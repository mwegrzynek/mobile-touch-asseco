<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class db_filter {
	
	function __construct()
	{
		$this->CI = & get_instance();	
	}
		
	function setFilters($filter)
	{
	   //filters settings
		if(count($filter))
		{				
			if(isset($filter['customFilter']))
			{
				foreach($filter['customFilter'] as $customFilter)
				{
					if(isset($customFilter['in']))
					{							
						$this->CI->db->where_in($customFilter['in']['field'], $customFilter['in']['value']);													
					}
					elseif(isset($customFilter['notin']))
					{							
						$this->CI->db->where_not_in($customFilter['notin']['field'], $customFilter['notin']['value']);
					}
					elseif(isset($customFilter['text']) && trim($customFilter['text']) != '')
					{
						$text = mb_strtolower($customFilter['text']);
						if(isset($customFilter['fields'][0]))
						{
							foreach($customFilter['fields'] as $field)
							{
								if(is_array($field)) 
								{
									if($field[0] = 'left') 
									{
										$queryPart[] = 'lower('.$field[1].') LIKE "'.$text.'%"';
									}
									else
									{
										$queryPart[] = 'lower('.$field[1].') LIKE "%'.$text.'"';	
									}
								}
								else
								{
									$queryPart[] = 'lower('.$field.') LIKE "%'.$text.'%"';
								}
							}
							$queryPartStr = implode(' OR ', $queryPart);
							$this->CI->db->where('('.$queryPartStr.')');					
						}
						//$this->CI->db->where('(lower(title) LIKE "%'.$text.'%" OR lower(content) LIKE "%'.$text.'%")');					
					}
					elseif(isset($customFilter['text-left']) && trim($customFilter['text-left']) != '')
					{
						$text = mb_strtolower($customFilter['text-left']);
						if(isset($customFilter['fields'][0]))
						{
							foreach($customFilter['fields'] as $field)
							{
								$queryPart[] = 'lower('.$field.') LIKE "'.$text.'%"';
							}
							$queryPartStr = implode(' OR ', $queryPart);
							$this->CI->db->where('('.$queryPartStr.')');					
						}
						//$this->CI->db->where('(lower(title) LIKE "%'.$text.'%" OR lower(content) LIKE "%'.$text.'%")');					
					}
					elseif(isset($customFilter['date']) && trim($customFilter['date']) != '')						
					{								
						$this->CI->db->where('UNIX_TIMESTAMP('.$customFilter['field'].') >=', $customFilter['from']);
						$this->CI->db->where('UNIX_TIMESTAMP('.$customFilter['field'].') <', $customFilter['to']);							
					}						
					/*elseif(isset($customFilter['valuefrom']) && trim($customFilter['valuefrom']) != '')						
					{								
						$this->CI->db->where('op_post.'.$customFilter['field'].' >=', $customFilter['valuefrom']);							
						$this->CI->db->where('op_post.'.$customFilter['field'].' <=', $customFilter['valueto']);							
					}*/
					else
					{
						if(isset($customFilter['field']) && isset($customFilter['value']))
						{								
							$this->CI->db->where($customFilter['field'], $customFilter['value']);						
						}
					}
				}
			}
		}
	}		
}