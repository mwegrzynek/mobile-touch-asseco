<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Video_vm {

	var $api_url			= 'http://vimeo.com/api/v2/video/'; // url of API Feed
	var $video_id			= ''; // id of movie in You Tube
	var $video_url			= ''; // url of movie in You Tube
	var $duration_format	= 'minutes';
	
	private $xml  			= false;
	private $errors  		= array();
	private $is_xml_get	= false;
	/**
	 * Constructor
	 *
	 * @access	public
	 * @param	array	initialization parameters
	 */
	function __construct($params = array())
	{
		if(count($params) > 0)
		{
			$this->initialize($params);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Initialize Preferences
	 *
	 * @access	public
	 * @param	array	initialization parameters
	 * @return	void
	 */
	function initialize($params = array())
	{
		if(count($params) > 0)
		{
			foreach($params as $key => $val)
			{
				if (isset($this->$key))
				{
					$this->$key = $val;
				}
			}

			if($this->video_id == '')
			{
				$this->video_id = $this->getMovieIdFromLink($this->video_url);
			}
		}
	}

	// --------------------------------------------------------------------

	//SET	
	function setUrl($link)
	{
		$this->video_url = $link;
		$this->video_id = $this->getMovieIdFromLink($link);
	}

	function setId($id)
	{
		$this->video_id = $id;
	}
	
	function setError($msg)
	{				
		$this->errors[] = $msg;	
	}
	
	function displayErrors()
	{	
		return $this->errors;
	}
	
	//GET
	function getXmlInfo()
	{		
		if($this->video_id == '')
		{
			$this->setError('You must provide Video ID or Video URL');
			return false;
		}
		
		if(!$this->xml = @simplexml_load_file($this->api_url.$this->video_id.'.xml'))
		{
			$this->setError('Cannot connect to Vimeo with this data');
			$this->is_xml_get = true;
			return false;			
		}

		return true;
	}
	
	function getMovieInfo()
	{			
	   if($this->checkIsXml())
		{
			$info['title']     = $this->getTitle();
			$info['content']   = $this->getContent();
			$info['thumbnail'] = $this->getThumbnail();
			return $info;
		}
		return false;
	}

	/*private function getDuration()
	{				
		$media 	= $this->xml->children('http://search.yahoo.com/mrss/');
		$yt 		= $media->children('http://gdata.youtube.com/schemas/2007');
		
		$attrs = $yt->duration->attributes();      
		$duration = (int) $attrs['duration'];
		
		if(!$duration)
		{
			return "";
		}
		
		switch($this->duration_format) 
		{
			case 'minutes':
				$duration = $this->secondsToMinutes($duration);
				break;
		}
		return $duration;
	}*/

	private function getTitle()
	{		
		$title = (string) $this->xml->video->title;
		return $title;		
	}
	
	private function getContent()
	{
		$this->getXmlInfo();
		$content = (string) $this->xml->video->description;
		return $content;
	}	
	
	private function getThumbnail()
	{		
		$title = (string) $this->xml->video->thumbnail_large;
		return $title;		
	}
	
	//UTILS	
	function getMovieIdFromLink($link)
	{
		$linkArray = explode('/', $link);		
      return end($linkArray);
	}

	function secondsToMinutes($seconds)
	{
		$mins = floor ($seconds / 60);
		$secs = $seconds % 60;
		return sprintf ("%d:%02d", $mins, $secs);
	}
	
	function checkIsXml()
	{
	   if(!$this->xml)
		{
			if(!$this->is_xml_get)
			{
				if($this->getXmlInfo())
				{
					return true;
				}
				else
				{			
					return false;
				}
			}
		}
		return true;
	}
}
// END Video Youtube Class
