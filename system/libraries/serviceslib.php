<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//This library is common for backend and frontend

class serviceslib {
	
	/**
	 * Get all services of given user
	 *
	 * @access	public
	 * @param	int $userId
	 * @return	array of user services
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2011, Maciej Węgrzynek
	 */ 	 
	function getUserServices($userId)
	{
		$CI = & get_instance();	
		$CI->load->model('../../models/ServiceModel');		
				
		//set user id
		$filter['customFilter'][0] = array(					
											'value' => $userId,
											'field' => 'user_id'
										);				
		
		$services = $CI->ServiceModel->getAllServices(100, 0, 'add_date desc', $filter);		
		return $services;		
	}
	
	// --------------------------------------------------------------------	
	
	/**
	 * Add new service
	 *
	 * @access	public
	 * @param	array $data
	 * @return	int $serviceId
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2011, Maciej Węgrzynek
	 */
	function addService($data, $serviceTypeInfo)
	{
		$CI = & get_instance();	
		$CI->load->model('../../models/ServiceModel');
		
		$sliderId = 0;
		if(isset($data['entry_slider_id'])) 
		{
			$sliderId = $data['entry_slider_id'];
			unset($data['entry_slider_id']);
		}
		
		$data['id']           = '';		
		$data['type_id']      = $serviceTypeInfo->id;
		$data['entry_id']     = $data['entry_id'];
		
		
		//If service is "Entry Edit", get active languages from entry
		if($data['type_id'] == 86) 
		{
			$serviceLanguages = $CI->entrieslib->getEntryActiveLanguages($data['entry_id']);
			$serialized_data = $CI->entrieslib->getEntryCurrentTextData($data['entry_id']);			
		}
		elseif($data['type_id'] == 120)
		{
			$serviceLanguages = $CI->entrieslib->getEntryActiveLanguages($data['entry_id']);
			$serialized_data = '';
		}
		elseif($data['type_id'] == 530)
		{
			$serviceLanguages = array();
			$serialized_data = '';	
		}
		else
		{
			$serviceLanguages = $data['language_id'];	
			$serialized_data = '';
		}
		
		//price info
		$priceInfo = $this->calculatePrice($serviceLanguages, $serviceTypeInfo);
		
		//remove 'language_id' from data set
		unset($data['language_id']);
		
		$data['price']        = $priceInfo['netto'];
		$data['fv_id']        = 0;
		$data['is_paid']      = 0;
		
		$data['public_id']    = substr(md5(rand(100000, 999999)), 0, 5).date('ymd');
		$data['tax_value']    = $priceInfo['tax'];
		$data['order_date']   = '0000-00-00 00:00:00';
		$data['valid_thru']   = '0000-00-00 00:00:00';
		$data['currency_id']  = $priceInfo['currency'];
		$data['description']  = '';
		$data['price_brutto'] = $priceInfo['brutto'];
		
		//var_dump($data);
		
		$serviceId = $CI->ServiceModel->addService($data);
		
		$serializedServiceLanguages = $this->getLanguageArrayForService($serviceLanguages);
		
		$serviceMetaData['id']                = '';
		$serviceMetaData['service_id']        = $serviceId;
		$serviceMetaData['languages']         = $serializedServiceLanguages;
		$serviceMetaData['entry_slider_id']   = $sliderId;
		$serviceMetaData['entry_category_id'] = '';		
		$serviceMetaData['serialized_data']   = $serialized_data;
		
		$CI->ServiceModel->addServiceMetaData($serviceMetaData);
		
		return $serviceId;
	}

	// --------------------------------------------------------------------	
	
	/**
	 * Add new service
	 *
	 * @access	public
	 * @param	array $data
	 * @return	int $serviceId
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2011, Maciej Węgrzynek
	 */
	function updateService($serviceId, $postedData, $serviceTypeInfo)
	{
		$CI = & get_instance();	
		$CI->load->model('../../models/ServiceModel');
		
		$data['id']           = $serviceId;
		$data['entry_id']     = $postedData['entry_id'];		
		
		//If service is "Entry Edit", get active languages from entry		
		if($serviceTypeInfo->id == 86) 
		{
			$serviceLanguages = $CI->entrieslib->getEntryActiveLanguages($postedData['entry_id']);
			
			$dataToSerialize['entry_title']   = $postedData['entry_title'];
			$dataToSerialize['entry_content'] = $postedData['entry_content'];
			$dataToSerialize['entry_tags']    = $postedData['entry_tags'];			
			$serialized_data = serialize($dataToSerialize);
		}
		elseif($serviceTypeInfo->id == 120)
		{
			$serviceLanguages = $CI->entrieslib->getEntryActiveLanguages($postedData['entry_id']);
			$serialized_data = '';
		}
		else
		{
			$serviceLanguages = $postedData['language_id'];	
			$serialized_data = '';
		}
		
		//price info
		$priceInfo = $this->calculatePrice($serviceLanguages, $serviceTypeInfo);
				
		$data['price']        = $priceInfo['netto'];
		$data['tax_value']    = $priceInfo['tax'];
		$data['currency_id']  = $priceInfo['currency'];
		$data['price_brutto'] = $priceInfo['brutto'];
		
		//var_dump($data);
		
		$serviceId = $CI->ServiceModel->updateService($data);
		
		$serializedServiceLanguages = $this->getLanguageArrayForService($serviceLanguages);
		
		$serviceMetaData['service_id']        = $serviceId;
		$serviceMetaData['languages']         = $serializedServiceLanguages;
		//$serviceMetaData['entry_slider_id']   = '';
		
		//promo in categories
		if(isset($postedData['category_id'])) 
		{
			$categoryid = $postedData['category_id'];
		}
		else
		{
			$categoryid = 0;
		}		
		$serviceMetaData['entry_category_id'] = $categoryid;
		
		//chenge entry service
		$serviceMetaData['serialized_data']   = $serialized_data;
		
		$CI->ServiceModel->updateServiceMetaData($serviceMetaData);
		
		return $serviceId;
	}
	
	// --------------------------------------------------------------------
	
	
	/**
	 * Calculate price depend on number of language, current language and type of service
	 *
	 * @access	public
	 * @param	array $languages
	 * @return	array $priceInfo
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2011, Maciej Węgrzynek
	 */
	function calculatePrice($languages, $serviceTypeInfo, $serviceId = null)
	{
		$CI = & get_instance();	
		
		$langNum = count($languages);
				
		$taxValue = $serviceTypeInfo->li_num_data_2;
		
		//get prices
		if($CI->languageid == 1) 
		{
			//PLN
			$servicePriceBrutto = $serviceTypeInfo->price_data_1 + ($serviceTypeInfo->price_data_1 * $taxValue / 100);		
			$priceNextLangBrutto = $serviceTypeInfo->price_data_3 + ($serviceTypeInfo->price_data_3 * $taxValue / 100);	
			
			$servicePriceNetto = $serviceTypeInfo->price_data_1;		
			$priceNextLangNetto = $serviceTypeInfo->price_data_3;	
			
			$currencyId = 1;	
		}
		else
		{
			//EUR
			$servicePriceBrutto = $serviceTypeInfo->price_data_2 + ($serviceTypeInfo->price_data_2 * $taxValue / 100);		
			$priceNextLangBrutto = $serviceTypeInfo->price_data_4 + ($serviceTypeInfo->price_data_4 * $taxValue / 100);
			
			$servicePriceNetto = $serviceTypeInfo->price_data_2;		
			$priceNextLangNetto = $serviceTypeInfo->price_data_4;	
			
			$currencyId = 2;	
		}
		
		
		$priceBrutto = $servicePriceBrutto + ($priceNextLangBrutto * ($langNum - 1));
		$priceNetto = $servicePriceNetto + ($priceNextLangNetto * ($langNum - 1));
		
		if($langNum == 0) 
		{
			$priceBrutto = 0;
			$priceNetto = 0;
		}
		
		return array('brutto' => $priceBrutto, 'netto' => $priceNetto, 'currency' => $currencyId, 'tax' => $taxValue);		
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * 
	 *
	 * @access	public
	 * @param	integer (void)
	 * @return	array (void)
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2011, Maciej Węgrzynek
	 */
	function getLanguageArrayForService($serviceLanguages)
	{
		$CI = & get_instance();	
		
		$languages = $CI->LanguageModel->getAllLanguages();
		foreach($languages as $key => $value) 
		{
			if(!in_array($value->id, $serviceLanguages)) 
			{
				unset($languages[$key]);
			}
		}
		return serialize($languages);
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Check if set languages are the same as possible languages. If not remove languages that are not possible for this service and update service.
	 * Return possible current language Ids
	 *
	 * @access	public
	 * @param	integer $serviceId
	 * @param	array $serviceLanguages
	 * @param	array $possibleServiceLanguages
	 * @return	array $output
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2011, Maciej Węgrzynek
	 */
	public function checkLanguageConsistency($serviceId, $serviceLanguages, $possibleServiceLangs)
	{
		$CI = & get_instance();	
		
		foreach($serviceLanguages as $key => $value) 
		{
			if(in_array($value->id, $possibleServiceLangs)) 
			{
				$langs[] = $value;
				$output[] = $value->id;
			}
		}
		
		$serviceMetaData['service_id']        = $serviceId;		
		$serviceMetaData['languages']         = serialize($langs);
		$CI->ServiceModel->updateServiceMetaData($serviceMetaData);
		return $output;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Check if set languages are the same as possible languages. If not remove languages that are not possible for this service and update service.
	 * Return possible current language Ids
	 *
	 * @access	public
	 * @param	integer $serviceId
	 * @param	array $serviceCurrentPossibleLanguages
	 * @param	array $serviceTypeInfo
	 * @return	array $output
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2011, Maciej Węgrzynek
	 */
	function checkPriceConsistency($serviceId, $serviceCurrentPossibleLanguages, $serviceTypeInfo)
	{
		$CI = & get_instance();	
		
		$priceInfo = $this->calculatePrice($serviceCurrentPossibleLanguages, $serviceTypeInfo, $serviceId);

		$data['id']           = $serviceId;
		$data['price']        = $priceInfo['netto'];		
		$data['price_brutto'] = $priceInfo['brutto'];		
		$CI->ServiceModel->updateService($data);
		
		return $priceInfo;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Get "Add / Translate Entry" service for given entryid
	 *
	 * @access	public
	 * @param	integer $entryId
	 * @return	array $serviceInfo
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2011, Maciej Węgrzynek
	 */
	function getMainServiceByEntryId($entryId)
	{
		$CI = & get_instance();	
		$CI->load->model('../../models/ServiceModel');		
				
		//set user id
		$filter['customFilter'][0] = array(					
											'value' => $entryId,
											'field' => 'entry_id'
										);				
		$filter['customFilter'][1] = array(					
											'value' => 87,
											'field' => 'type_id'
										);
		
		$service = $CI->ServiceModel->getAllServices(1, 0, 'add_date desc', $filter);		
		return $service;		
	}
	
	// --------------------------------------------------------------------
	
	
	/**
	 * setPaid() - Set service as paid
	 *
	 * @access	public
	 * @param	integer $serviceId
	 * @return	array (void)
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2011, Maciej Węgrzynek
	 */
	function setPaid($serviceId)
	{
		$CI = & get_instance();	
		$CI->load->model('../../models/ServiceModel');
		
		$serviceInfo = $CI->ServiceModel->getService($serviceId);
		if(!$serviceInfo[0]->is_paid)
		{
			//set service and entry (if needed) as paid						
			$userInfo = $CI->UserModel->getUser($serviceInfo[0]->user_id);
			
			$this->doSetServicePaid($serviceInfo, $userInfo);			
			
			//SEND MESSAGES			
			//send notice to admin					
			$CI->load->library('Notices');
			
			$noticeConfig['noticeType'] = 'paymentValidNoticeAdmin';					
			$noticeConfig['link']       = 'http://'.$_SERVER['HTTP_HOST'].'/admin/service/showfull/'.$serviceInfo[0]->id;
			$noticeConfig['topic']      = 'User has paid for service in Eurovistas';
			$noticeConfig['permission'] = 'getsystemnotices-9';
			$CI->notices->SendAdminNotice($noticeConfig);
			
			//to user			
			//$userInfo = $CI->UserModel->getUser($serviceInfo[0]->user_id);
			
			//get user lang
			$userLang = $userInfo[0]->lang_id;
			$userId = $userInfo[0]->id;
			
			$noticeConfig = '';
			//send message (notification) about new service to user			
			if($serviceInfo[0]->type_id == 87) 
			{
				$link = 'http://'.$_SERVER['HTTP_HOST'].'/user/showentry/'.$serviceInfo[0]->entry_id;
			}
			else
			{
				$link = 'http://'.$_SERVER['HTTP_HOST'].'/user/showservice/'.$serviceInfo[0]->id;
			}
			$noticeConfig['link'] = $link;
			$noticeConfig['noticeType'] = 'paymentValidNotice';
			$noticeConfig['topicid']    = 543;
			$noticeConfig['userid']     = $userId;
			$noticeConfig['lang_id']    = $userLang;	
			$CI->notices->SendNotice($noticeConfig);
							
			log_message('info', 'Status usługi ['.$serviceInfo[0]->id.'] zmieniony');					
		}	
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * make all settings to set service/entry paid
	 *
	 * @access	public
	 * @param	array $serviceInfo
	 * @return	bool true
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2011, Maciej Węgrzynek
	 */
	function doSetServicePaid($serviceInfo, $userInfo)
	{
		$CI = & get_instance();	
		$CI->load->model('../../models/ItemsModel');
		$CI->load->model('../../models/ServiceModel');
		
		$userLang = $userInfo[0]->lang_id;
		
		$CI->ItemsModel->setDefaultLanguage($userLang);
		
		//set service paid, set expiration date
		$data['id']         = $serviceInfo[0]->id;
		$data['is_paid']    = 1;
		$data['order_date'] = date('Y-m-d H:i:s');		
		
		$entryId       = $serviceInfo[0]->entry_id;
		$serviceTypeId = $serviceInfo[0]->type_id;
		$serviceTypes  = $CI->ServiceModel->getServiceTypes();
		
		log_message('info', 'Check ServiceType Duration Days: '.$userLang);
		if($serviceTypes[$serviceTypeId]->li_num_data_1) 
		{
			$durationDays       = $serviceTypes[$serviceTypeId]->li_num_data_1;
			$timestamp          = strtotime("+".$durationDays." days");
			$data['valid_thru'] = date('Y-m-d H:i:s', $timestamp);
			log_message('info', 'Set ValidThru Date');
		}
		
		$CI->ServiceModel->updateService($data);
		//var_dump($data);
		
		//special settings for services
		switch($serviceTypeId) 
		{
			//add entry
			case 87:
				//set entry paid, and expiration data				
				$entryData['exp_date']      = $data['valid_thru'];
				$entryData['li_num_data_2'] = 1;
				$CI->ItemsModel->updateItem($entryId, $entryData);
				break;
				
			//add new language to entry
			case 530:
				$serviceMetaData = $CI->ServiceModel->getServiceMetaData($serviceInfo[0]->id);
				$languages = unserialize($serviceMetaData[0]->languages);
				if(is_array($languages)) 
				{
					foreach($languages as $key => $value) 
					{
						$langEntryData['id']          = $entryId;
						$langEntryData['is_active']   = 1;
						$langEntryData['language_id'] = $value->id;
						$CI->ItemsModel->changeItem($langEntryData);
					}
				}
				break;
				
			//edit entry
			case 86:
				//set				
				$serviceMetaData = $CI->ServiceModel->getServiceMetaData($serviceInfo[0]->id);
				$textData = unserialize($serviceMetaData[0]->serialized_data);
				
				$userInfo = $CI->UserModel->getUser($serviceInfo[0]->user_id);
				$userLang = $userInfo[0]->lang_id;
				
				if(isset($textData['entry_title']) && $textData['entry_title'] != '') 
				{				
					$langChangeEntryData['id']          = $entryId;
					$langChangeEntryData['language_id'] = $userLang;
					$langChangeEntryData['title']       = $textData['entry_title'];
					$langChangeEntryData['content']     = $textData['entry_content'];
					$langChangeEntryData['entry_tags']  = $textData['entry_tags'];
					$CI->ItemsModel->changeItem($langChangeEntryData);				
				}
				break;	
				
			//entry expiration date extension
			case 120:					
				$entryExtensionData['exp_date'] = $data['valid_thru'];				
				$CI->ItemsModel->updateItem($entryId, $entryExtensionData);
				break;
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Check if service is present in current language
	 *
	 * @access	public
	 * @param	string $languages
	 * @return	bool true/false
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2011, Maciej Węgrzynek
	 */
	function checkIfServiceInCurrentLang($languages)
	{
		$CI = & get_instance();	
		
		$serviceLanguages = unserialize($languages);
		if($serviceLanguages != NULL) 
		{
			foreach($serviceLanguages as $key => $value) 
			{
				if($value->id == $CI->languageid)
				{
					return true;
				}			
			}
		}
		return false;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Get paid, active, not expired premium services
	 * 118 - Home page sidebar
	 * 119 - Home page slider
	 *
	 * @access	public	
	 * @return	array $premiumServices
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2011, Maciej Węgrzynek
	 */
	function getValidPremiumServices($service_type)
	{
		$CI = & get_instance();	
		$CI->load->model('../../models/ServiceModel');
		
		//get premium services
		$filter['customFilter'][0] = array(
													'field' => 'is_paid',
													'value' => 1
												);
												
		$filter['customFilter'][1] = array(
														'field' => '(UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(valid_thru)) <',
														'value' => -3600
													);												
		
		$filter['customFilter'][2] = array(
														'field' => 'type_id',
														'value' => $service_type
													);		
		
		$premiumServices = $CI->ServiceModel->getAllServices(50, 0, 'add_date desc', $filter);
		return $premiumServices;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Get service meta data
	 *
	 * @access	public
	 * @param	integer $serviceid
	 * @return	array $serviceData
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2011, Maciej Węgrzynek
	 */
	function getServiceMetaData($service_id)
	{
		$CI = & get_instance();	
		$CI->load->model('../../models/ServiceModel');
		
		$serviceData = $CI->ServiceModel->getServiceMetaData($service_id);
		return $serviceData[0];
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Get paid, active, not expired premium services related with category or subcategory
	 * 116 - premium in category
	 * 117 - premium in subcategory
	 *
	 * @access	public
	 * @param	integer $categoryId
	 * @param	integer $service_type
	 * @return	array $premiumServices
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2011, Maciej Węgrzynek
	 */
	function getValidPremiumServicesForCategory($service_type, $categoryId)
	{
		$CI = & get_instance();	
		$CI->load->model('../../models/ServiceModel');
		
		//get premium services
		$filter['customFilter'][0] = array(
													'field' => 'is_paid',
													'value' => 1
												);
												
		$filter['customFilter'][1] = array(
														'field' => '(UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(valid_thru)) <',
														'value' => -3600
													);												
		
		$filter['customFilter'][2] = array(
														'field' => 'type_id',
														'value' => $service_type
													);		
													
		$filter['customFilter'][3] = array(
														'field' => 'entry_category_id',
														'value' => $categoryId
													);
		
		$premiumServices = $CI->ServiceModel->getAllServices(50, 0, 'add_date desc', $filter);
		return $premiumServices;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Get paid, active, not expired entry services
	 *
	 * @access	public	
	 * @return	array $premiumServices
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2011, Maciej Węgrzynek
	 */
	function getValidEntryServices($entryId)
	{
		$CI = & get_instance();	
		$CI->load->model('../../models/ServiceModel');
		
		//get premium services
		$filter['customFilter'][0] = array(
													'field' => 'is_paid',
													'value' => 1
												);
		
		$filter['customFilter'][2] = array(
														'field' => 'entry_id',
														'value' => $entryId
													);		
		
		$filter['customFilter'][3]['in'] = array(
														'field' => 'type_id',
														'value' => array(86, 87, 120)
													);		
		
		$premiumServices = $CI->ServiceModel->getAllServices(50, 0, 'add_date desc', $filter);
		return $premiumServices;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Get paid, active, not expired entry services
	 *
	 * @access	public	
	 * @return	array $premiumServices
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2011, Maciej Węgrzynek
	 */
	function getEntryPremiumServices($entryId, $additionalFilter = array())
	{
		$CI = & get_instance();	
		$CI->load->model('../../models/ServiceModel');
		
		//get premium services
		$filter['customFilter'][1] = array(
														'field' => 'entry_id',
														'value' => $entryId
													);		
		
		$filter['customFilter'][2]['notin'] = array(
														'field' => 'type_id',
														'value' => array(87)
													);	
													
		$filter['customFilter'] = array_merge($filter['customFilter'],  $additionalFilter);	
													
		$premiumServices = $CI->ServiceModel->getAllServices(50, 0, 'add_date desc', $filter);
		return $premiumServices;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Get filters and set new filters for entry services list
	 *
	 * @access	public	 
	 * @return	array $filters
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2011, Maciej Węgrzynek
	 */
	function getUserFilters()
	{
		$CI = & get_instance();	
		
		if(isset($_POST['service-status-filter']))
		{
			$statusFilter = $CI->input->post('service-status-filter');
			$CI->session->set_userdata('service-status-filter', $statusFilter);
		}
		
		if($CI->session->userdata('service-status-filter')) 
		{
			$statusFilter = $CI->session->userdata('service-status-filter');			
		}
		
		if(!isset($statusFilter)) 
		{
			$statusFilter = -1;
		}
		
		switch($statusFilter) 
		{
			case 0:
				$filter[0] = array(
											'field' => 'is_paid',
											'value' => 1
									);	
			
				$filter[1] = array(
											'field' => '(UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(valid_thru)) <',
											'value' => -3600
									);					
				break;
				
			case 1:
				$filter[0] = array(
											'field' => '(UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(valid_thru)) >',
											'value' => 0
									);			
				break;
				
			case 2:
				$filter[0] = array(
										'field' => 'is_paid',
										'value' => 0
									);	
				break;		
			
			default:
				$filter = array();
				break;
		}
		
		return array($filter, $statusFilter);		
	}
	
	// --------------------------------------------------------------------

	/**
	 * Get categories or subcategories of given entry.
	 *
	 * @access	public
	 * @param	integer $entryId
	 * @param	bool $sub
	 * @return	array $output (categories or subcategories with parents)
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2011, Maciej Węgrzynek
	 */
	public function getEntryCategoriesInfo($entryId, $sub = false)
	{
		$CI = & get_instance();
		
		$categories = $CI->entrieslib->getEntryCategoriesInfo($entryId);		
		
		$output = false;
		
		if(!isset($categories[0])) 
		{
			return $output;
		}
		
		foreach($categories as $key => $value) 
		{
			if(!$sub) 
			{
				//get main categories only
				if(is_object($value->parent_info)) 
				{
					$output[$value->parent_info->id] = $value->parent_info;
				}
				else
				{
					$output[$value->id] = $value;
				}
			}
			else
			{
				//get subcategories only
				if(is_object($value->parent_info)) 
				{
					$output[] = $value;
				}				
			}
		}
		return $output;		
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Get languages ids
	 *
	 * @access	private
	 * @param	array $languages
	 * @return	array $languagesIds
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2011, Maciej Węgrzynek
	 */ 	 
	function getLanguagesIds($languages)
	{
		//get active languages
		$languagesIds = array();		
		
		//default language always first			
		if(count($languages)) 
		{
			foreach($languages as $key => $value) 
			{
				$languagesIds[] = $value->id;
			}
		}		
		return $languagesIds;	
	}
	
	// --------------------------------------------------------------------
}
