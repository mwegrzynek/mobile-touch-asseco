<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 4.3.2 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2006, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Pagination Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Pagination
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/pagination.html
 */
class CI_Pagination2 {

	var $base_url			= ''; // The page we are linking to
	var $total_rows  		= ''; // Total number of items (database results)
	var $per_page	 		= 10; // Max number of items you want shown per page
	var $num_links			= 2; // Number of "digit" links to show before/after the currently viewed page
	var $cur_page	 		= 0; // The current page being viewed
	var $first_link   	= '';
	var $next_link			= '';
	var $prev_link			= '';
	var $last_link			= '';
	var $uri_segment		= 2;
	

	/**
	 * Constructor
	 *
	 * @access	public
	 * @param	array	initialization parameters
	 */
	function CI_Pagination($params = array())
	{
		if (count($params) > 0)
		{
			$this->initialize($params);		
		}
		
		log_message('debug', "Pagination Class2 Initialized");
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Initialize Preferences
	 *
	 * @access	public
	 * @param	array	initialization parameters
	 * @return	void
	 */
	function initialize($params = array())
	{
		if (count($params) > 0)
		{
			foreach ($params as $key => $val)
			{
				if (isset($this->$key))
				{
					$this->$key = $val;
				}
			}		
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Generate the pagination links
	 *
	 * @access	public
	 * @return	string
	 */	
	function create_links()
	{
		// If our item count or per-page total is zero there is no need to continue.
		if ($this->total_rows == 0 OR $this->per_page == 0)
		{
		   return '';
		}

		// Calculate the total number of pages
		$num_pages = ceil($this->total_rows / $this->per_page);

		// Is there only one page? Hm... nothing more to do here then.
		if ($num_pages == 1)
		{
			return '';
		}

		// Determine the current page number.
      if($this->cur_page == 0)
      {
         $CI =& get_instance();         
   		if ($CI->uri->segment($this->uri_segment) != 0)
   		{
   			$this->cur_page = $CI->uri->segment($this->uri_segment);
   			
   			// Prep the current page - no funny business!
   			$this->cur_page = (int) $this->cur_page;
   		}
      }
		

		$this->num_links = (int)$this->num_links;
		
		if ($this->num_links < 1)
		{
			show_error('Your number of links must be a positive number.');
		}
				
		if ( ! is_numeric($this->cur_page))
		{
			$this->cur_page = 0;
		}
		
		// Is the page number beyond the result range?
		// If so we show the last page
		if ($this->cur_page > $this->total_rows)
		{
			$this->cur_page = ($num_pages - 1) * $this->per_page;
		}
		
		$uri_page_number = $this->cur_page;
		$this->cur_page = floor(($this->cur_page/$this->per_page) + 1);

		// Calculate the start and end numbers. These determine
		// which number to start and end the digit links with
		$start = (($this->cur_page - $this->num_links) > 0) ? $this->cur_page - ($this->num_links - 1) : 1;
		$end   = (($this->cur_page + $this->num_links) < $num_pages) ? $this->cur_page + $this->num_links : $num_pages;

		// Add a trailing slash to the base URL if needed
		$this->base_url = rtrim($this->base_url, '/') .'/';

  		// And here we go...
		$output = array();

		// Render the "First" link
		if  ($this->cur_page > $this->num_links + 1)
		{
			$output['first'] = array(
                                 'url'   => $this->base_url.'0',
                                 'link'  => $this->first_link
                              );
		}

		// Render the "previous" link
		if  ($this->cur_page != 1)
		{
			$i = $uri_page_number - $this->per_page;
			//if ($i == 0) $i = '';
         
         $output['previous'] = array(
                                 'url'   => $this->base_url.$i,
                                 'link'  => $this->prev_link
                              );			
		}

		// Write the digit links
		for ($loop = $start -1; $loop <= $end; $loop++)
		{
			$i = ($loop * $this->per_page) - $this->per_page;
					
			if ($i >= 0)
			{
				if ($this->cur_page == $loop)
				{
					$output['pages'][] = array(
                                 'url'   => '',
                                 'link'  => $loop
                              );
               $cur_page = $loop;
				}
				else
				{
					//$n = ($i == 0) ? '' : $i;
               $n=$i;
					
               $output['pages'][] = array(
                                 'url'   => $this->base_url.$n,
                                 'link'  => $loop
                              );
				}
			}
		}

		// Render the "next" link
		if ($this->cur_page < $num_pages)
		{
			$output['next'] = array(
                                 'url'   => $this->base_url.($this->cur_page * $this->per_page),
                                 'link'  => $this->next_link
                              );
		}

		// Render the "Last" link
		if (($this->cur_page + $this->num_links) < $num_pages)
		{
			$i = (($num_pages * $this->per_page) - $this->per_page);
         
         $output['last'] = array(
                                 'url'   => $this->base_url.$i,
                                 'link'  => $this->last_link
                              );	
		}
      
      $output['num_pages'] = $num_pages;
      $output['cur_page']  = $this->cur_page;
		
		return $output;		
	}
}
// END Pagination Class

/* End of file Pagination.php */
/* Location: ./system/libraries/Pagination.php */