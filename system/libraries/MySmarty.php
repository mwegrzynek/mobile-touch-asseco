<?php
   require_once('smarty/Smarty.class.php');
      
   class MySmarty extends Smarty
   {
      function __construct()
      {
         parent::__construct();

         if(function_exists('date_default_timezone_set')) 
            date_default_timezone_set('Europe/Warsaw');
         
      	$this->compile_dir     = APPPATH . "views/templates_c";
         $this->template_dir    = APPPATH . "views/templates";
         $this->cache_dir       = $_SERVER['DOCUMENT_ROOT'].'/system/cache/smarty';         
         $this->error_reporting = E_ALL ^ E_NOTICE;         
         
         //$this->plugins_dir[] = 'plugins'; this is default localization, no need to redeclare
         
         log_message('debug', "Smarty Class Initialized");
      }
   }
   
