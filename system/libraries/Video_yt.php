<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Video_yt {

	var $api_url			= 'http://gdata.youtube.com/feeds/api/videos/'; // url of API Feed
	var $video_id			= ''; // id of movie in You Tube
	var $video_url			= ''; // url of movie in You Tube
	var $duration_format	= 'minutes';
	
	private $xml  			= false;
	private $errors  		= array();
	private $is_xml_get	= false;
	/**
	 * Constructor
	 *
	 * @access	public
	 * @param	array	initialization parameters
	 */
	function Video_yt($params = array())
	{
		if(count($params) > 0)
		{
			$this->initialize($params);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Initialize Preferences
	 *
	 * @access	public
	 * @param	array	initialization parameters
	 * @return	void
	 */
	function initialize($params = array())
	{
		if(count($params) > 0)
		{
			foreach($params as $key => $val)
			{
				if (isset($this->$key))
				{
					$this->$key = $val;
				}
			}

			if($this->video_id == '')
			{
				$this->video_id = $this->getMovieIdFromLink($this->video_url);
			}
		}
	}

	// --------------------------------------------------------------------

	//SET	
	function setUrl($link)
	{
		$this->video_url = $link;
		$this->video_id = $this->getMovieIdFromLink($link);
	}

	function setId($id)
	{
		$this->video_id = $id;
	}
	
	function setError($msg)
	{				
		$this->errors[] = $msg;	
	}
	
	function displayErrors()
	{	
		return $this->errors;
	}
	
	//GET
	function getXmlInfo()
	{		
		if($this->video_id == '')
		{
			$this->setError('You must provide Video ID or Video URL');
			return false;
		}

		//var_dump($this->video_id);
		if(!$this->xml = simplexml_load_file($this->api_url.$this->video_id))
		{
			$this->setError('Nie mozna pobrać danych XML z podanego linku do klipu YouTube. Sprawdź poprawność adresu URL linka.');
			$this->is_xml_get = true;
			return false;			
		}
			//print_r($this->xml);
		return true;
	}
	
	function getMovieInfo()
	{			
	   if($this->checkIsXml())
		{
			$info['title'] = $this->getTitle();
			$info['content'] = $this->getContent();
			$info['duration'] = $this->getDuration();
			$info['thumbnail'] = '';
			return $info;
		}
		return false;
	}

	private function getDuration()
	{				
		$media 	= $this->xml->children('http://search.yahoo.com/mrss/');
		$yt 		= $media->children('http://gdata.youtube.com/schemas/2007');
		
		$attrs = $yt->duration->attributes();      
		$duration = (int) $attrs['duration'];
		
		if(!$duration)
		{
			return "";
		}
		
		switch($this->duration_format) 
		{
			case 'minutes':
				$duration = $this->secondsToMinutes($duration);
				break;
		}
		return $duration;
	}

	private function getTitle()
	{		
		$title = (string) $this->xml->title;
		return $title;		
	}
	
	private function getContent()
	{
		$this->getXmlInfo();
		$title = (string) $this->xml->content;
		return $title;
	}	
	
	//UTILS	
	function getMovieIdFromLink($link)
	{
		parse_str(parse_url($link, PHP_URL_QUERY), $params);		
		return $params['v'];
	}

	function secondsToMinutes($seconds)
	{
		$mins = floor ($seconds / 60);
		$secs = $seconds % 60;
		return sprintf ("%d:%02d", $mins, $secs);
	}
	
	function checkIsXml()
	{
	   if(!$this->xml)
		{
			if(!$this->is_xml_get)
			{
				if($this->getXmlInfo())
				{
					return true;
				}
				else
				{			
					return false;
				}
			}
		}
		return true;
	}
}
// END Video Youtube Class
