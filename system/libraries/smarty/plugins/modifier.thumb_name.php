<?php

function smarty_modifier_thumb_name($string, $counter = 0)
{
	if($string != '')
	{
		if(!$counter) 
		{
			$counter = '';
		}
		else
		{
			$counter = '_'.$counter;
		}
		
		$path_parts = pathinfo($string);
		$file_extension = $path_parts['extension'];
		$thumbFileName = str_replace('.'.$file_extension, '', $string).$counter.'_thumb.'.$file_extension;
		return $thumbFileName;
	}
	else
	{
		return false;
	}
}

/* vim: set expandtab: */

?>