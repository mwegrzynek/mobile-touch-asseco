<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty mb_truncate modifier plugin
 *
 * Type:     modifier<br>
 * Name:     mb_truncate<br>
 * Purpose:  Truncate a string to a certain length if necessary,
 *           optionally splitting in the middle of a word, and
 *           appending the $etc string. (MultiByte version)
 * @link http://smarty.php.net/manual/en/language.modifier.truncate.php
 *          truncate (Smarty online manual)
 * @param string
 * @param string
 * @param integer
 * @param string
 * @param boolean
 * @return string
 */
function smarty_modifier_mb_truncate($string, $intEncode='UTF8', $length = 80, $etc = '...', $break_words = false)
{
    if ($length == 0)
        return '';

   // Set the internal encoding
   if ( !$intEncode ) $intEncode = "UTF8";
   mb_internal_encoding($intEncode);

	if(mb_strlen($string) > $length) 
	{
		$length -= mb_strlen($etc);
		if(!$break_words)
		{	
			$string = preg_replace('/\s+?(\S+)?$/', '', mb_substr($string, 0, $length+1));
		}
		$string =  mb_substr($string, 0, $length).$etc;
	} 	
	
		 
	$string = str_replace('&amp;', '&', $string);
	$string = str_replace('&', '&amp;', $string);
	//var_dump($string);
   return $string;
}

/* vim: set expandtab: */

?> 