<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notices {

	function __construct()
	{
		$this->CI = & get_instance();

		$this->CI->load->library('commonareas');
		$this->CI->load->model('../../models/NoticesModel');

	}

	// --------------------------------------------------------------------

	var $noticeTypes = array(
			'changePasswordNotice'				=> 189,
			'createAccountNotice'				=> 100,
			'orderConfirmation'					=> 192,
			'sendMessageNotice'					=> 115,
			'forgottenPassword'					=> 103,
			'resetPassword'						=> 597,
			'entryActivated'						=> 536,
			'paymentValidNotice'					=> 544,
			'newAccountByAdminNotice'			=> 595
		);

   // --------------------------------------------------------------------

   private function getTemplatePath()
   {
   	return 'file:'.BASEPATH.'application/backend/views/templates/notices/';
   }

   // --------------------------------------------------------------------

	function SendMailNotice($mail, $topic, $message, $maitype = 'text')
	{
		$CI = & get_instance();

		if($mail == 'maciej@hitmo.pl' || $mail == 'admin@asseco.com')
		{
			return true;
		}

		$CI->load->library('email');
		$config['protocol'] = 'sendmail';
		$config['mailtype'] = $maitype;

		$CI->email->initialize($config);
		$CI->email->from('notices@'.$_SERVER['HTTP_HOST'], $this->getNotification(105));
		//for tests only
		//$mail = 'user@localhost.com';
		// $mail = 'user@preview-dev.com';
		$CI->email->to($mail);
		$CI->email->subject($topic);
		$CI->email->message($message);

		// var_dump($mail);

		if(!$CI->email->send())
		{
			// var_dump($CI->email->print_debugger());
			// exit();
			return false;
		}
		else
		{
			//var_dump($CI->email->print_debugger());
			// var_dump($CI->email->print_debugger());
			// exit();
			return true;
		}
		/*var_dump($mailContent);
		var_dump($tempPassword);*/

	}

	// --------------------------------------------------------------------

	function getNotification($notificationId)
	{
		return $this->notificationTemplates[$notificationId]->content;
	}

	// --------------------------------------------------------------------

	function getNotificationTexts($lang_id = false)
	{
	 	if(!$lang_id)
	 	{
	 		$lang_id = $this->CI->session->userdata('languageid');
	 		if(!$lang_id)
	 		{
	 			$lang_id = $this->CI->OptionsModel->getDefaultLanguageId();
	 		}
	 	}

	 	$default_lang_id = $this->CI->session->userdata('languageid');
	 	if(!$default_lang_id)
	 	{
	 		$default_lang_id = $this->CI->OptionsModel->getDefaultLanguageId();
	 	}

		$this->CI->ItemsModel->setDefaultLanguage($lang_id);

		$this->notificationTemplates = $this->CI->commonareas->getNotificationTexts();
		$this->CI->ItemsModel->setDefaultLanguage($default_lang_id);
	}

	// --------------------------------------------------------------------

	function SendNotice($config)
	{
		if(!isset($config['lang_id']))
		{
			$config['lang_id'] = $this->CI->session->userdata('languageid');
		}

		$this->getNotificationTexts($config['lang_id']);

		$this->conf = $config;
		if($noticeid = $this->CheckNoticeType($config['noticeType']))
		{
			$message = $this->getNotification($noticeid);
			$bottomMessage = $this->GetAdditionalMessage($config['noticeType']);

			$data                       = array();
			$data['title']              = $this->getNotification($config['topicid']);
			$data['notificationHeader'] = $this->getNotification(5);
			$data['link']  				 = $config['link'];

			$mailtype = 'text';
			if(isset($config['maitype']))
			{
				$mailtype = $config['maitype'];

				if($mailtype == 'html')
				{
					$data['message']            = nl2br($message);
					$data['additionalMessage']  = nl2br($bottomMessage);
					$data['link']  				 = $config['link'];

					$template = 'bodyHtml.tpl';
					//$this->CI->mysmarty->assign('noticeData', $data);
					//$message = $this->CI->mysmarty->fetch($this->getTemplatePath().'bodyHtml.tpl');
					//$message = str_replace($this->footer, '<hr/>', $message);
				}
			}

			if($mailtype == 'text')
			{
				$data['message']            = $message;
				$data['additionalMessage']  = strip_tags($bottomMessage);
				//$message = $this->getNotification(5)."\n\n".$message."\n\n".$bottomMessage.$config['link']."\n".$this->footer;
				$template = 'bodyTxt.tpl';
			}

			$this->CI->mysmarty->assign('noticeData', $data);
			$message = $this->CI->mysmarty->fetch($this->getTemplatePath().$template);


			if($mail = $this->GetUserMailById($config['userid']))
			{
				$result = $this->SendMailNotice($mail, $this->getNotification($config['topicid']), $message, $mailtype);
				return $result;
			}
		}
		else
		{
			return false;
		}
	}

	// --------------------------------------------------------------------

	function CheckNoticeType($noticeType)
	{
		if(isset($this->noticeTypes[$noticeType]))
		{
			return $this->noticeTypes[$noticeType];
		}
		return false;
	}

	// --------------------------------------------------------------------

	function AddAdditionalInfo()
	{
		# code...
	}

	// --------------------------------------------------------------------

	function GetUserMailById($userid)
	{
		$CI = & get_instance();
		$CI->load->model('../../models/UserModel');
		$mail = $CI->UserModel->getUserMail($userid);
		return $mail;
	}

	// --------------------------------------------------------------------

	function GetAdditionalMessage($noticeType)
	{
		return true;
	}

	// --------------------------------------------------------------------

	//ADMIN NOTICES
	function SendAdminNotice($config)
	{
		$CI = & get_instance();
		$CI->load->model('../../models/OptionsModel');
		$CI->load->model('../../models/UserModel');

		$this->getNotificationTexts();

		if(isset($config['userid']))
		{
			$userInfo = $CI->UserModel->getUser($config['userid']);
			//$fName = $userInfo[0]->first_name;
			//$lName = $userInfo[0]->last_name;
			$config['userInfo'] = $userInfo[0];
		}


		$message = $this->getAdminMessage($config);
		$mails = $this->getAdminMails($config);

		if(is_array($config['additionalMails']))
		{
			if(isset($config['forceAdditionalMails']) && $config['forceAdditionalMails'] == true)
			{
				$mails = $config['additionalMails'];
			}
			else
			{
				$mails = array_merge($mails, $config['additionalMails']);
			}

			$mails = array_unique($mails);
		}

		//send mails
		if($mails)
		{
			foreach($mails as $key => $mail)
			{

				if($CI->languageid != '32' || $mail != 'implementations@assecobs.pl')
				{
					// var_dump($CI->languageid);
					// var_dump($mail);

					$this->SendMailNotice($mail, $config['topic'], $message);
				}
				else
				{

				}
			}
			return true;
		}
	}

	// --------------------------------------------------------------------

	function getAdminMails($config)
	{
		//get all super admin mails
		$mails = $this->CI->NoticesModel->getSuperAdminsMails();

		//get all admin mails with privileges to get this info
		if($groups = $this->CI->NoticesModel->getGroupsByPermission($config['permission']))
		{
			$mails1 = $this->CI->NoticesModel->getAdminsMailsByGroups($groups);
			if($mails1)
			{
				$mails = array_merge($mails, $mails1);
			}
			$mails = array_unique($mails);
		}

		return $mails;
	}


	function getAdminMessage($config)
	{
		$noticeType = $config['noticeType'];

		if($noticeType == 'newContactFormMessage')
		{
			if(!isset($config['data']['client_name']))
			{
				$config['data']['client_name'] = '';
			}
			if(!isset($config['data']['company']))
			{
				$config['data']['company'] = '';
			}
			if(!isset($config['data']['email']))
			{
				$config['data']['email'] = '';
			}
			if(!isset($config['data']['phone']))
			{
				$config['data']['phone'] = '';
			}
			if(!isset($config['data']['message']))
			{
				$config['data']['message'] = '';
			}

			$message = '---------------------------------------------------------------
A message has been send by contact form.
---------------------------------------------------------------
Date: '.date('Y-m-d').'
By: '.$config['data']['client_name'].'
Company: '.$config['data']['company'].'
Email: '.$config['data']['email'].'
Contact phone: '.$config['data']['phone'].'
Message:
'.$config['data']['message'].'
---------------------------------------------------------------';

		}

		return $message;
	}
}
