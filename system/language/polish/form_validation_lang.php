<?php

$lang['required'] 			= "Pole <strong>%s</strong> jest wymagane.";
$lang['isset']				= "Pole <strong>%s</strong> musi mieć wartość.";
$lang['valid_email']		= "Pole <strong>%s</strong> musi zawierać poprawny adres email.";
$lang['valid_emails'] 		= "Pole <strong>%s</strong> musi zawierać poprawne adresy email.";
$lang['valid_url'] 			= "Pole <strong>%s</strong> musi zawierać poprawny URL.";
$lang['valid_ip'] 			= "Pole <strong>%s</strong> musi zawierać poprawny IP.";
$lang['min_length']			= "Pole <strong>%s</strong> musi zawierać co najmniej <strong>%s</strong> znaków.";
$lang['max_length']			= "Pole <strong>%s</strong> nie może zawierać więcej niż <strong>%s</strong> znaków.";
$lang['exact_length']		= "Pole <strong>%s</strong> musi zawierać dokładnie <strong>%s</strong> znaków.";
$lang['alpha']				= "Pole <strong>%s</strong> może zawierać tylko litery.";
$lang['alpha_numeric']		= "Pole <strong>%s</strong> może zawierać tylko litery i cyfry.";
$lang['alpha_dash']			= "Pole <strong>%s</strong> może zawierać tylko litery, cyfry, podkreślenia i myślniki.";
$lang['numeric']			= "Pole <strong>%s</strong> musi zawierać liczbę.";
$lang['is_numeric']			= "Pole <strong>%s</strong> musi zawierać liczbę.";
$lang['integer']			= "Pole <strong>%s</strong> musi zawierać liczbę całkowitą.";
$lang['matches']			= "Pole <strong>%s</strong> ma inną wartość niż pole <strong>%s</strong>.";
$lang['is_natural']			= "Pole <strong>%s</strong> musi zawierać liczbę naturalną.";
$lang['is_natural_no_zero']	= "Pole <strong>%s</strong> musi zawierać liczbę większą od zera.";


/* End of file form_validation_lang.php */
/* Location: ./system/language/polish/form_validation_lang.php */