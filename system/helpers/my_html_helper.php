<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function getAttribute($attrib, $tag)
{
	//get attribute from html tag
	$re = '/' . preg_quote($attrib) . '=([\'"])?((?(1).+?|[^\s>]+))(?(1)\1)/is';
	if(preg_match($re, $tag, $match)) 
	{
		return urldecode($match[2]);
	}
	return false;
}