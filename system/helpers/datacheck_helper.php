<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


	function nipToArray($nip)
	{
		if(preg_match_all("/\d/", $nip, $digits))
		{
			$nip = $digits[0];
			return $nip;
		}
		return false;
	}
	
	function clearNip($nip)
	{
		if($nip = nipToArray($nip))
		{
			$nip = implode($nip);
			return $nip;
		}
		return false;
	}
	
	function formatNip($nip)
	{
		if($nip = nipToArray($nip))
		{		
			$slicedNip[0] = implode(array_slice($nip, 0, 3));
			$slicedNip[1] = implode(array_slice($nip, 3, 3));
			$slicedNip[2] = implode(array_slice($nip, 6, 2));
			$slicedNip[3] = implode(array_slice($nip, 8, 2));
			
			$nip = implode($slicedNip, '-');
			return $nip;
		}
		return false;
	}
	
	
	function is_evidence_nr($nr, $type=0)
	{
		if ($nr === NULL || empty($nr)) return false;
		$types = array(0 => "NIP", 1 => "REGON", 2 => "PESEL");
		
		
		if ( !array_key_exists($type, $types) ) return false;
		else $type = (string) $types[$type];
	
		$weights["NIP"]  = array (6, 5, 7, 2, 3, 4, 5, 6, 7) ;
		$weights["REGON_7"] = array (2, 3, 4, 5, 6, 7) ;
		$weights["REGON_9"] = array (8, 9, 2, 3, 4, 5, 6, 7) ;
		$weights["PESEL"] = array (1, 3, 7, 9, 1, 3, 7, 9, 1, 3);
	
		$mods["NIP"] = 11;
		$mods["REGON"] = 11;
		$mods["PESEL"] = 10;
	
		if(preg_match_all("/\d/", $nr, $digits))
		{		
			$digits  = $digits[0];
			if ($type == "REGON") 
			{
				$digits_count = count($digits);
				if ($digits_count != 9 && $digits_count != 7) return false;
				$idx = (string) sprintf("%s_%d", $type, $digits_count);
			}
			else $idx = (string) $type;
	
	 
			$weights  = $weights[$idx];
			$mods = $mods[$type];
			
			if ( count($digits)-1 != count($weights) ) return false;
			$ctrl_digit = (int) array_pop($digits);
			$all_sum  = (int) 0;
			foreach ( $digits as $digit )
			{
				$weight  = (int) current($weights);
				$all_sum += (int) $digit * $weight;
				next($weights);
			}
	
			$calc_ctrl = (int) ($all_sum % $mods == 10) ? 0 : $all_sum % $mods;
			if ($type == "PESEL") $calc_ctrl = (int) (10 - ($all_sum % $mods) == 10) ? 0 : 10 - ($all_sum % $mods) ;
			if ($calc_ctrl == $ctrl_digit) return true;		
		}	 
		return false; 
	}