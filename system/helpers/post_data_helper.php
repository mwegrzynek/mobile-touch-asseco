<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	function getPostDataToArray()
	{
		if(is_array($_POST))
		{
			$CI = & get_instance();
			
			$keys = array_keys($_POST);
			
			foreach ($keys as $key => $value) 
			{
				$value = preg_replace('/[^a-zA-Z0-9_-]/', '', $value);
				$data[$value] = $CI->input->post($value);				
			}	
			return $data;
		}	
		return false;	
	}