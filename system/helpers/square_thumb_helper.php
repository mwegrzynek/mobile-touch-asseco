<?php

	/**
   * function resizePhotoToSquare()   
   *
   * Make a thumbnail sqare sized but no deformed.<br/>
   * Return true.
   *   
   * @param string $path path to Image (with filename)
   * @param integer $newSize size of square
   * @param string $newPath path where thumbnail will be saved
   * @return bool true
   */
   function resizePhotoToSquare($path, $newSize, $newPath)
   {
      $info =  getimagesize($path);
      
      $width = $info[0];
      $height = $info[1];
		
      if($width<=$height)
      {
         $wymiar = $width;
         $przesuniecie = -($height-$width)/2;
         $przesuniecie1 = 0;
      }
      else
      {
         $wymiar = $height;
         $przesuniecie = 0;
         $przesuniecie1 = -($width-$height)/2;
      }
		
     
     
      if($info['mime'] == 'image/png')
      {
	      $obrazek = imagecreatefrompng($path);
      }
      else
	   {
	   	$obrazek = imagecreatefromjpeg($path);
	   }      
      
      
      $obrazekTmp		= imagecreatetruecolor($wymiar, $wymiar);
      imagecopymerge($obrazekTmp, $obrazek, $przesuniecie1, $przesuniecie, 0, 0, $wymiar-$przesuniecie1, $wymiar-$przesuniecie, 100);
      $miniaturka		= imagecreatetruecolor($newSize, $newSize);
		
      imagecopyresampled($miniaturka, $obrazekTmp, 0, 0, 0, 0, $newSize, $newSize, $wymiar, $wymiar);
      
      
      if($info['mime'] == 'image/png')
      {
	      imagepng($miniaturka, $newPath, 0);
      }
      else
	   {
	   	imagejpeg($miniaturka, $newPath, 70);
	   }      
      
      imagedestroy($miniaturka);
      imagedestroy($obrazek);
      imagedestroy($obrazekTmp);
      return true;
   }
	
?>