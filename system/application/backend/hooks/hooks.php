<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class Hooks
	{
		function Hooks()
		{
			$this->Engine = & get_instance();
			//$this->Engine->mysmarty->autoload_filters = array('output' => array('tidyoutput'));
			$this->Engine->mysmarty->assign('baseurl', base_url());
			$this->Engine->mysmarty->assign('langcode', 'pl');
		}

		// --------------------------------------------------------------------

		function authorization()
		{
			if($this->Engine->uri->segment(3) !== 'uploadmore')
			{
				if(!$this->Engine->ion_auth->logged_in())
				{
					if($this->Engine->uri->segment(2)!='login' && $this->Engine->uri->segment(2)!='forgot_password' && $this->Engine->uri->segment(2)!='reset_password')
					{
						redirect('auth/login');
					}
				}
				else
				{
					$userInfo = $this->Engine->ion_auth->get_user();

					$userData['userid']   = $userInfo->id;
					$userData['username'] = $userInfo->username;
					$userData['group']    = $userInfo->group;


					$permissions = $this->Engine->ion_auth->get_permissions_for_group();
					$this->Engine->permissions = $permissions["parsedvalues"];
					if(!isset($this->Engine->permissions[0]))
					{
						$this->Engine->permissions = array();
					}
					$this->Engine->mysmarty->assign('userData', $userData);

					$this->makeMainMenu();
				}
			}
		}

		// --------------------------------------------------------------------

		function makeMainMenu()
		{
			$this->Engine->load->library('form_validation');
			$this->Engine->form_validation->set_error_delimiters('', '###');

			$aMainNavig=array(
						1 => array(
								"name"   => "Menu",
								"link"   => "#",
								"submenu"   => array(
														0 => array(
																"name"   => "Main menu",
																"link"   => "menu/mainmenu",
																"class"  => "",
																"permission"  => "mainmenu-4",
															),
														1 => array(
																"name"   => "Footer menu",
																"link"   => "menu/footermenu",
																"permission"   => "footermenu-4",
																"class"  => ""
															)
                                    )
						),
						2 => array(
								"name"   => "Mobile Touch Pages",
								"link"   => "items/mtpages",
								"permission"  => "mtpages-4",
								"submenu"   => array(
														0 => array(
																"name"   => "Features Pages",
																"link"   => "items/featurespages",
																"permission"  => "featurespages-4"

															),
														1 => array(
																"name"   => "Services Pages",
																"link"   => "items/servicespages",
																"permission"  => "servicespages-4"

															),
														2 => array(
																"name"   => "Concepts Pages",
																"link"   => "items/conceptspages",
																"permission"  => "conceptspages-4"

															),
														3 => array(
																"name"   => "Overview Pages",
																"link"   => "items/overviewpages",
																"permission"  => "overviewpages-4"

															),
														4 => array(
																"name"   => "Main sections",
																"link"   => "items/mtpages",
																"class"  => "folder",
																"permission"  => "mtpages-4"
															)
                                    )
						),
						3 => array(
								"name"   => "PC Pages",
								"link"   => "items/pconnectorpages",
								"permission"  => "pconnectorpages-4",
								"submenu"   => array(
														0 => array(
																"name"   => "Features Pages",
																"link"   => "items/pcfeaturespages",
																"permission"  => "pcfeaturespages-4"

															),
														1 => array(
																"name"   => "Services Pages",
																"link"   => "items/pcservicespages",
																"permission"  => "pcservicespages-4"

															),
														2 => array(
																"name"   => "About Pages",
																"link"   => "items/pcaboutpages",
																"permission"  => "pcaboutpages-4"

															),
														3 => array(
																"name"   => "Main sections",
																"link"   => "items/pconnectorpages",
																"class"  => "folder",
																"permission"  => "pconnectorpages-4"
															)
                                    )
						),
						/*3 => array(
								"name"   => "Other textpages",
								"link"   => "items/pages",
								"permission"  => "pages-4",
						),*/
						4 => array(
								"name"   => "Other sections",
								"link"   => "items/mainpages",
								"permission"  => "pages-4",
						),
						5 => array(
								"name"   => "News",
								"link"   => "items/news",
								"permission"  => "news-4",
								"endSection"    => true
						),
						6 => array(
								"name"   => "Clients",
								"link"   => "items/clients",
								"permission"  => "clients-4",
								"submenu"   => array(
														0 => array(
																"name"   => "Manage clients",
																"link"   => "items/clients",
																"permission"  => "clients-4",
															),
														1 => array(
																"name"   => "Manage client testimonials",
																"link"   => "items/testimonials",
																"permission"  => "testimonials-4"
															),
														2 => array(
																"name"   => "Manage clients case studies",
																"link"   => "items/casestudies",
																"permission"  => "casestudies-4"
															),
														3 => array(
																"name"   => "Manage clients references",
																"link"   => "items/references",
																"permission"  => "references-4"
															)
                                       )
						),
						7 => array(
								"name"   => "Company",
								"link"   => "items/companypages",
								"permission"  => "companypages-4",
								"submenu"   => array(
														0 => array(
																"name"   => "Manage company subpages",
																"link"   => "items/companypages",
																"permission"  => "companypages-4",
															),
														1 => array(
																"name"   => "Manage raport testimonials",
																"link"   => "items/raporttestimonials",
																"permission"  => "raporttestimonials-4"
															),
														2 => array(
																"name"   => "Manage partners testimonials",
																"link"   => "items/partnerstestimonials",
																"permission"  => "partnerstestimonials-4"
															)
                                       )
						),
						8 => array(
								"name"   => "Team leaders",
								"link"   => "items/teamleaders",
								"permission"  => "teamleaders-4",
								"endSection"    => true,
                  ),
						10 => array(
								"name"   => "Languages",
								"link"   => "language",
								"permission"  => "lang-4",
								"startSection"    => true
						),
						15 => array(
								"name"   => "Options",
								"link"   => "#",
								"endSection"    => true,
								"submenu"   => array(
															0 => array(
																"name"   => "Labels and texts",
																"link"   => "items/sitetexts",
																"permission" => 'sitetexts-4'
															),
															1 => array(
																"name"   => "Home page slider",
																"link"   => "items/hpsliders",
																"permission" => 'hpsliders-4'
															),
															2 => array(
																"name"   => "Home page video slider",
																"link"   => "items/sliders",
																"permission" => 'sliders-4'
															),
															3 => array(
																"name"   => "Partners",
																"link"   => "partners",
																"permission" => 'partners-4'
															),
															4 => array(
																"name"   => "Contact Entries",
																"link"   => "contacts",
																"permission" => 'contacts-4'
															),
															5 => array(
																"name"   => "Contact form countries list",
																"link"   => "items/countries",
																"permission" => 'countries-4'
															),
															6 => array(
																"name"   => "Home page logo slider",
																"link"   => "items/logoslider",
																"permission" => 'logoslider-4'
															),
															7 => array(
																"name"   => "Why us section",
																"link"   => "items/whyus",
																"permission" => 'whyus-4'
															),
															8 => array(
																"name"   => "Why Mobile Touch section",
																"link"   => "items/whymobiletouch",
																"permission" => 'whymobiletouch-4'
															),
															9 => array(
																	"name"   => "Industries",
																	"link"   => "category/industries",
																	"permission"  => "industries-4"
															)
											)
						),

						20 => array(
								"name"            => "Administrators",
								"link"            => "auth",
								"startSection"    => true,
								"submenu"   => array(
															0 => array(
																"name"   => "Manage accounts",
																"link"   => "auth"
															)
													)
						)
				);

			if($this->Engine->ion_auth->is_admin())
			{
				$aMainNavig[20]['submenu'][1] = array(
																"name"   => "Admin groups",
																"link"   => "authgroups"
																	);
			}

			$aMainNavig = $this->checkMenuPermissions($aMainNavig);

			$this->Engine->mysmarty->assign('mainNavig', $aMainNavig);
		}

		// --------------------------------------------------------------------

		function checkMenuPermissions($menu)
		{
			if($this->Engine->ion_auth->is_admin())
			{
				return $menu;
			}
			return $this->getMenu($menu);
		}

		// --------------------------------------------------------------------

		function getMenu($menu)
		{
			$res = $menu;

			foreach($res as $key => $value)
			{
				if(isset($value['permission']) && !in_array($value['permission'], $this->Engine->permissions))
				{
					unset($res[$key]);
				}
				if(isset($value['submenu']))
				{
					foreach($value['submenu'] as $subkey => $subvalue)
					{
						if(isset($subvalue['permission']))
						{

							if(!in_array($subvalue['permission'], $this->Engine->permissions))
							{
								unset($res[$key]['submenu'][$subkey]);
							}
						}
					}
				}
			}

			foreach($res as $key => $value)
			{
				if($value['link'] == '#')
				{
					if(!isset($value['submenu']) || count($value['submenu']) < 1)
					{
						unset($res[$key]);
					}
				}
			}

			return $res;
		}

		// --------------------------------------------------------------------

		function setPaginationSettings()
		{
			$perPageItems = array(
										30,
										50,
										100,
										200
									);

			$pagination_config  = array();


			$pagination = array(
								'config'       => $pagination_config,
								'itemsPerPage' => $perPageItems
			);

			$this->Engine->mysmarty->assign('perPageItems', $perPageItems);

			$this->Engine->paginationConfig = $pagination;
		}
		/*function getBenchmark()
		{
			var_dump(xdebug_time_index());
		}*/
	}
