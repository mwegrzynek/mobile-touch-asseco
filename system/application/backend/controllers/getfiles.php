<?php

	class getfiles extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
		}


		function index()
		{
			$fileName = $this->uri->segment(3, 0);	
			
			
			switch($this->uri->segment(2, 0)) 
			{
				case 'frdownload':
					$path = '../pictures/frdownload/';
					break;
					
				case 'frnews':
					$path = '../pictures/frnews/';
					break;
				
				default:
					show_error('Wrong file type.');
					exit();
					break;
			}
			
			if(is_file($path.$fileName)) 
			{
				$this->load->helper('file');
				
				$file = file_get_contents($path.$fileName);
				$fileInfo = get_file_info($path.$fileName);
				$fileSize = $fileInfo['size'];
				$fileMime = get_mime_by_extension($fileName);
				
				header('Content-Type: '.$fileMime);
				header('Content-Disposition: attachment; filename='.$fileName);
				header("Content-Length: ".$fileSize);
				
				echo $file;
			}
			else
			{
				show_error('File doesn\'t exist');
				exit();	
			}
		}
	}//end class
	
	
	