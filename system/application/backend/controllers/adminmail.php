<?php

class adminmail extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
      
      $this->mysmarty->assign('activeItem', 15);
      $this->load->model('../../models/OptionsModel');
      $this->mysmarty->assign('link', 'adminmail');
		
		$this->secondMenu[0] = array(
                     "name"   => "Mail kontaktowy",
                     "link"   => "adminmail"
                  );
	}
	
	function index()
	{      
      $result = $this->OptionsModel->getAdminMail(); 
      $aBackData['adminMail'] = $result[0]->global_admin_mail; 
		
      //set header
      $this->mysmarty->assign('header', 'Edytuj adres e-mail administratora');      
      $this->form_validation->set_rules('adminMail', 'Mail', 'trim|valid_email|required'); // Set the validation rules.		

      if($this->form_validation->run()) // Validation Passed
      { 
         $change = $this->OptionsModel->setAdminMail
         (                 
            $this->input->post('adminMail')                 
         );            
         
         if($change)            
         {               
            $this->mysmarty->assign('header', 'Dane zostały pomyślnie zapisane.');
         }           
       
         $aBackData['adminMail'] = $this->input->post('adminMail');
      }
      elseif(validation_errors() != '') // Validation Failed
      {      			
         $this->mysmarty->assign('errorHeader', 'Dane nie zostały zapisane.');
         
         //prepare for smarty as array         
         $aErrors = explode("###", substr(trim(validation_errors()), 0, -3)); 
         
         if(isset($aErrors[0]) && $aErrors[0]!='')
         {            
            $this->mysmarty->assign('errors', $aErrors);
         }
         
         $aBackData['adminMail'] = $this->input->post('adminMail');
      }  
      
      $this->mysmarty->assign('backData', $aBackData);
		$this->mysmarty->assign('secondmenu', $this->secondMenu);
      $this->mysmarty->display('options/adminMail.tpl');
   }
}