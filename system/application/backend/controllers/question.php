<?php

class question extends CI_Controller
{
	function __construct()
	{
		parent::__construct();	
		
		//$this->output->enable_profiler(TRUE);
		$this->mysmarty->assign('activeItem', 8);
		$this->load->model('../../models/QuestionsModel');
		$this->load->model('../../models/OfferModel');
		$this->load->model('../../models/OptionsModel');
		$this->load->model('../../models/CategoryModel');     
		$this->load->model('../../models/UserModel');     
		$this->load->model('../../models/ItemsModel');     
		$this->load->model('../../models/LanguageModel');     

		$this->defaultLangId = $this->OptionsModel->getDefaultLanguageId();
		$this->ItemsModel->setDefaultLanguage($this->defaultLangId);
		$this->CategoryModel->setDefaultLanguage($this->defaultLangId);

		$this->secondMenu[0]=array(
							"name"   => "Pokaż wszystkie",
							"link"   => "question"
						);

		/*$this->secondMenu[1]=array(
							"name"   => "Add new",
							"link"   => "question/addnew"
						);
		*/
		
		//permissions					
		if(!$this->ion_auth->is_admin())
		{			
			if(!in_array('question-4', $this->_getPermissions())) 
			{
				redirect('nopermission');
			}
		}
		

		if($lastEdited = $this->session->userdata('lastEditedQuestion'))
		{
			$this->secondMenu[2]=array(
								"name"   => "Last edited: [".$lastEdited['name']."]",
								"link"   => "question/showfull/".$lastEdited['id']
							);
		}


		$this->mysmarty->assign('link', 'question');
	}
	
	// --------------------------------------------------------------------

	function index()
	{
		$this->mysmarty->assign('css', array('uitheme/cupertino/jquery-ui-1.7.1.custom.css'));
		
		//FILTERS

		//text search filter
		if($this->session->userdata('questionSearchFilter'))
		{
			$filter['textSearchFilter'] = $this->session->userdata('questionSearchFilter');
			$filter['customFilter'][] = array(					
													'text' => $filter['textSearchFilter'],
													'fields' => array('public_id')													
												);						
		}
		else
		{
			$filter['textSearchFilter'] = '';
		}

		//show by status filter
		if($this->session->userdata('questionShowFilter') && $this->session->userdata('questionShowFilter') != 1)
		{
			$filter['showFilter'] = $this->session->userdata('questionShowFilter');
		
			if($filter['showFilter'] == 2 || $filter['showFilter'] == 3)
			{
				if($filter['showFilter'] == 2)
				{
					$filterValue = 1;
				}
				if($filter['showFilter'] == 3)
				{
					$filterValue = 0;
				}
	
				$filter['customFilter'][] = array(
													'field' => 'is_closed',
													'value' => $filterValue
												);
			}
			/*elseif($filter['showFilter'] == 4)
			{
				$filter['customFilter'][] = array(
													'field' => 'spam_reported',
													'value' => 1
				);						
			}*/
		}
		else
		{
			$filter['showFilter'] = 1;
		}


		//show by type (SMR, WSE) filter
		if($this->session->userdata('questionShowFilter1') && $this->session->userdata('questionShowFilter1') != 1)
		{
			$filter['showFilter1'] = $this->session->userdata('questionShowFilter1');

			if($filter['showFilter1'] == 2)
			{
				$filterValue = 0;
			}
			if($filter['showFilter1'] == 3)
			{
				$filterValue = 1;
			}

			$filter['customFilter'][] = array(
												'field' => 'question_type',
												'value' => $filterValue
											);
		}
		else
		{
			$filter['showFilter1'] = 1;
		}
		
		//show by client acceptation
		if($this->session->userdata('questionShowFilter2') && $this->session->userdata('questionShowFilter2') != 1)
		{
			$filter['showFilter2'] = $this->session->userdata('questionShowFilter2');

			if($filter['showFilter2'] == 2)
			{
				$filterValue = 2;
			}
			if($filter['showFilter2'] == 3)
			{
				$filterValue = 1;
			}

			$filter['customFilter'][] = array(
												'field' => 'is_accepted',
												'value' => $filterValue
											);
		}
		else
		{
			$filter['showFilter2'] = 1;
		}

		//state filter
		if($this->session->userdata('questionStateFilter') && $this->session->userdata('questionStateFilter') != -1)
		{
			$filter['stateFilter'] = $this->session->userdata('questionStateFilter');
			$filter['customFilter'][] = array(
												'field' => 'state_id',
												'value' => $filter['stateFilter']
											);
		}

		//category filter (not active now)
		/*if($this->session->userdata('questionCategoryFilter') === 0 || $this->session->userdata('questionCategoryFilter') > -1)
		{
			$filter['categoryFilter'] = $this->session->userdata('questionCategoryFilter');
			$filter['customFilter'][] = array(
												'field' => 'type_id',
												'value' => $filter['categoryFilter']
											);			
		}
		else
		{
			 $filter['categoryFilter'] = -1;
		}*/
		
		//date filter
		if($this->session->userdata('filterDateFrom') && $this->session->userdata('filterDateTo'))
		{
			$filter['filterDateTo'] = $this->session->userdata('filterDateTo');
			
			
			$filter['filterDateFrom'] = $this->session->userdata('filterDateFrom');
			
			if($filter['filterDateTo'] != '' && $filter['filterDateFrom'] != '')
			{
				$dateFrom = $this->check_date($filter['filterDateFrom']);
				$dateTo = $this->check_date($filter['filterDateTo']);
				if($dateFrom && $dateTo)
				{
					$dateFromArray = explode('-', $dateFrom);
					$dateToArray = explode('-', $dateTo);
					$from = mktime(0, 0, 0, $dateFromArray[1], $dateFromArray[2], $dateFromArray[0]);
					$to = mktime(23, 59, 59, $dateToArray[1], $dateToArray[2], $dateToArray[0]);
					
					$filter['customFilter'][] = array(
							'date' => $filter['filterDateFrom'],
							'field' => 'add_date',
							'from' => $from,
							'to' => $to
					); 
				}
				else
				{
					$this->mysmarty->assign('subheader', 'Invalid date given.');					
				}
			}
		}
		else
		{
			$filter['filterDateTo'] = '';
			$filter['filterDateFrom'] = '';
		}

		$this->mysmarty->assign('filter', $filter);

		if($itemsCount = $this->QuestionsModel->getCountQuestions($filter))
		{
			if($this->uri->segment(3))
			{
				$offset = $this->uri->segment(3);
				$this->mysmarty->assign('offset', $offset);
			}
			else
			{
				$offset = 0;
			}

			$this->load->library('pagination2');

			$this->paginationConfig['config']['base_url'] = base_url().'question/index/';
			$this->paginationConfig['config']['total_rows'] = $itemsCount;



			if($this->session->userdata('perPage'))
			{
				$this->paginationConfig['config']['per_page'] = $this->session->userdata('perPage');
			}
			else
			{
				$this->paginationConfig['config']['per_page'] = $this->paginationConfig['itemsPerPage'][0];
			}


			$this->pagination2->initialize($this->paginationConfig['config']);

			$this->mysmarty->assign('pagination', $this->pagination2->create_links());
			$this->mysmarty->assign('currentPerPage', $this->paginationConfig['config']['per_page']);
			$this->mysmarty->assign('perPageLink', 'question/setItemsPerPage');
			$this->mysmarty->assign('allItems', $itemsCount);

			$result = $this->QuestionsModel->getAllQuestions($this->paginationConfig['config']['per_page'], $offset, 'add_date desc', $filter);
			
			foreach($result as $key => $res) 
			{
				$result[$key]->user_info = $this->UserModel->getUser($res->user_id);						
				$result[$key]->offers_info = $this->getCountOffers($res->id);						
			}			
		}
		else
		{
			$result = array();
		}

		
		//if question/ad has been deleted set subheader
		if($this->session->flashdata('deleted'))
		{
			$this->mysmarty->assign('subheader', 'Pytanie zostało usunięte.');		
		}

		$this->mysmarty->assign('categories', $this->CategoryModel->getTree($itemType = 1, 'name asc'));		
		$this->mysmarty->assign('allItemsCount', $itemsCount);
		$this->mysmarty->assign('itemsList', $result);
		$this->mysmarty->assign('secondmenu', $this->secondMenu);
		$this->mysmarty->assign('activeSecondItem', 0);
		$this->mysmarty->display('question/showQuestions.tpl');
	}
	
	// --------------------------------------------------------------------

	function filter()
	{
		if($this->input->post('filter'))
		{
			$this->session->set_userdata('questionSearchFilter', $this->input->post('filter'));
		}
		else
		{
			$this->session->set_userdata('questionSearchFilter', '');
		}

		if($this->input->post('showFilter'))
		{
			$this->session->set_userdata('questionShowFilter', $this->input->post('showFilter'));
		}
		else
		{
			$this->session->set_userdata('questionShowFilter', '');
		}

		if($this->input->post('stateFilter'))
		{
			$this->session->set_userdata('questionStateFilter', $this->input->post('stateFilter'));
		}
		else
		{
			$this->session->set_userdata('questionStateFilter', '');
		}


		if($this->input->post('categoryFilter') || $this->input->post('categoryFilter') === 0)
		{
			$this->session->set_userdata('questionCategoryFilter', $this->input->post('categoryFilter'));
		}
		else
		{
			$this->session->set_userdata('questionCategoryFilter', '');
		}



		if($this->input->post('showFilter1'))
		{
			$this->session->set_userdata('questionShowFilter1', $this->input->post('showFilter1'));
		}
		else
		{
			$this->session->set_userdata('questionShowFilter1', '');
		}
		
		if($this->input->post('showFilter2'))
		{
			$this->session->set_userdata('questionShowFilter2', $this->input->post('showFilter2'));
		}
		else
		{
			$this->session->set_userdata('questionShowFilter2', '');
		}
		
		if($this->input->post('filterDateFrom'))
		{
			$this->session->set_userdata('filterDateFrom', $this->input->post('filterDateFrom'));
		}
		else
		{
			$this->session->set_userdata('filterDateFrom', '');
		}		
		
		if($this->input->post('filterDateTo'))
		{
			$this->session->set_userdata('filterDateTo', $this->input->post('filterDateTo'));
		}
		else
		{
			$this->session->set_userdata('filterDateTo', '');
		}		

		$this->session->unset_userdata('questionOffset');


		redirect('question');
	}
	
	// --------------------------------------------------------------------	
	
	function showfull()
	{
		if(!$questionid = $this->uri->segment(3))
		{
			redirect('question');
		}
		else
		{
			$this->mysmarty->assign('formLink', 'showfull/'.$questionid);						
			$this->mysmarty->assign('css', array('uitheme/cupertino/jquery-ui-1.7.1.custom.css'));
			//get question data
			$mainData = $this->QuestionsModel->getQuestion($questionid);
						
			
			$questionName = substr(strip_tags($mainData[0]->content), 0, 80);
						
			//set header
			$this->mysmarty->assign('header', 'Zobacz pytanie: <em>'.$questionName.'</em>');

			//set second menu edit item
			$this->secondMenu[2]=array(
							"name"   => "Zobacz pytanie: [".$questionName."]",
							"link"   => "question/showfull/".$questionid
						);

			$lastEdited = array(
							"name"   => $questionName,
							"id"     => $questionid
						);
			
			//GET OFFERS for this QUESTION
			$this->getOffers($questionid);
						
			if(isset($_POST['change_status']))
			{					
				$config = array(
					array(
							'field'   => 'is_closed',
							'label'   => 'Czy zakończone',
							'rules'   => 'trim|strip_tags'
						)/*,
					array(
							'field'   => 'spam_reported',
							'label'   => 'Czy oznaczony jako spam',
							'rules'   => 'trim|strip_tags'
								)*/
				);
	
				$this->form_validation->set_rules($config);
				
				if($this->form_validation->run())
				{						
					$this->QuestionsModel->setStatus($questionid, $this->input->post('is_closed'));
					//$this->QuestionsModel->setSpamStatus($questionid, $this->input->post('spam_reported'));
					// not put form again when page reload
					redirect(str_replace('/admin', '', $_SERVER['REDIRECT_URL']), 303);
				}
				elseif(validation_errors() != '') // Validation Failed
				{
					$this->mysmarty->assign('errorHeader', 'Data has not been changed.');
	
					//prepare for smarty as array
					$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));
	
					if(isset($aErrors[0]) && $aErrors[0]!='')
					{
						$this->mysmarty->assign('errors', $aErrors);
					}	
					$aBackData->is_active = $this->input->post('is_active');					
				}				
			}			


			$mainData[0]->user_info = $this->UserModel->getUser($mainData[0]->user_id);
			$aBackData = $mainData[0];

			$langs = $this->LanguageModel->getAllLanguages();
		
			
			$this->session->set_userdata('lastEditedQuestion', $lastEdited);

			$this->mysmarty->assign('backData', $aBackData);
			$this->mysmarty->assign('secondmenu', $this->secondMenu);
			$this->mysmarty->assign('activeSecondItem', 2);

			$this->mysmarty->display('question/showInfo.tpl');
		}
	}
	
	// --------------------------------------------------------------------

	function removeQuestion()
	{
		//permissions					
		if(!$this->ion_auth->is_admin())
		{			
			if(!in_array('question-3', $this->_getPermissions())) 
			{
				redirect('nopermission');
			}
		}
		
		if(!$questionid = $this->uri->segment(3))
		{
			redirect('question');
		}
		else
		{
			$this->QuestionsModel->removeQuestion($questionid);
			$this->_checkLastEdited($questionid);
			$this->session->set_flashdata('deleted', $questionid);
			redirect('question');
		}
	}
	
	// --------------------------------------------------------------------

	function setItemsPerPage()
	{
		if(!$items = $this->uri->segment(3))
		{
			redirect('question');
		}
		else
		{
			$this->session->set_userdata('perPage', $items);
			redirect('question');
		}
	}
	
	// --------------------------------------------------------------------

	function status()
	{
		$redirectAddress = 'question';
		if(isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] !='')
		{
			$path_parts = pathinfo($_SERVER['HTTP_REFERER']);
			if(isset($path_parts['basename']))
			{
				if(strpos($path_parts['dirname'], 'showfull'))
				{
					$redirectAddress .= '/showfull/'.$path_parts['basename'];
				}
				else
				{
					$redirectAddress .= '/index/'.$path_parts['basename'];
				}
			}
		}

		if(!$questionid = $this->uri->segment(3))
		{
			redirect($redirectAddress);
		}
		else
		{
			$this->_changeStatus($questionid);
			redirect($redirectAddress);
		}
	}
	
	// --------------------------------------------------------------------
	
	function spamstatus()
	{
		$redirectAddress = 'question';
		if(isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] !='')
		{
			$path_parts = pathinfo($_SERVER['HTTP_REFERER']);
			if(isset($path_parts['basename']))
			{
				if(strpos($path_parts['dirname'], 'showfull'))
				{
					$redirectAddress .= '/showfull/'.$path_parts['basename'];
				}
				else
				{
					$redirectAddress .= '/index/'.$path_parts['basename'];
				}
			}
		}

		if(!$questionid = $this->uri->segment(3))
		{
			redirect($redirectAddress);
		}
		else
		{
			$this->_changeSpamStatus($questionid);
			redirect($redirectAddress);
		}
	}
	
	// --------------------------------------------------------------------	

	function _changeStatus($questionid)
	{
		if($this->QuestionsModel->getStatus($questionid))
		{
			$this->QuestionsModel->setStatus($questionid, 0);
		}
		else
		{
			$this->QuestionsModel->setStatus($questionid, 1);
		}
	}
	
	// --------------------------------------------------------------------
	
	function _changeSpamStatus($questionid)
	{
		if($this->QuestionsModel->getSpamStatus($questionid))
		{
			$this->QuestionsModel->setSpamStatus($questionid, 0);
		}
		else
		{
			$this->QuestionsModel->setSpamStatus($questionid, 1);
		}
	}
	
	// --------------------------------------------------------------------

	function decode_html(&$input)
	{
		$input = html_entity_decode($input, ENT_QUOTES, 'UTF-8');
		return $input;
	}

	// --------------------------------------------------------------------
	
	function _uploadFile($config, $field_name)
	{
		$this->load->library('upload', $config);
		if(!$this->upload->do_upload($field_name))
		{
			$warnings = $this->upload->display_errors('', '###');
			$aErrors = explode("###", substr(trim($warnings), 0, -3));
			return array(false, $aErrors);
		}
		else
		{
			$uploadData=$this->upload->data();
			$fileName=$uploadData['file_name'];
			return array(true, array($uploadData['file_name'], $uploadData['full_path']));
		}
	}
	
	// --------------------------------------------------------------------
	
	function check_date($input)
	{
		$dateArray = explode('-', $input);
		if(isset($dateArray[0]) && isset($dateArray[1]) && isset($dateArray[2]))
		{
			return date('Y-m-d', mktime(0, 0, 0, $dateArray[1], $dateArray[2], $dateArray[0]));
		}
		else
		{
			//$this->form_validation->set_message('check_date', "<strong>Data: '".$input."' jest niepoprawna</strong>. Proszę wprowadzić datę w formacie 'YYYY-MM-DD'.");
			return false;
		}
	}
	
	// --------------------------------------------------------------------
	
	function _checkLastEdited($itemid)
	{
		if($lastEdited = $this->session->userdata('lastEditedQuestion'))
		{
			if($lastEdited['id'] == $itemid)
			{
				$this->session->unset_userdata(array('lastEditedQuestion' => ''));
			}
		}
	}	
	
	// --------------------------------------------------------------------
	
	function _getMessageTemplates()
	{
		$filter['customFilter'][0] = array(
								'field' => 'is_active',
								'value' => 1
			);
		
		$this->defaultLangId = $this->OptionsModel->getDefaultLanguageId();
		$this->ItemsModel->setDefaultLanguage($this->defaultLangId);	
		$result = $this->ItemsModel->getAllItems(
				6,
				10000000, 
				0, 
				'title', 
				'asc',
				-1,
				$filter
			);	
			
		return $result;	
	}
	
	// --------------------------------------------------------------------
	
	function _getPermissions()
	{
		$permissions = $this->ion_auth->get_permissions_for_group();
		$this->permissions = $permissions["parsedvalues"];
		if(!isset($this->permissions[0])) 
		{
			$this->permissions = array();
		}
		return $this->permissions;
	}
	
	// --------------------------------------------------------------------
	
	function getOffers($question_id)
	{
		$filter['customFilter'][] = array(
													'field' => 'question_id',
													'value' => $question_id
												);
		$result = $this->OfferModel->getAllOffers(1000, 0, 'add_date desc', $filter);
		
		$this->mysmarty->assign('offers', $result);
		
		return $result;
	}
	
	// --------------------------------------------------------------------
	
	function getCountOffers($question_id)
	{
		$filter['customFilter'][] = array(
													'field' => 'question_id',
													'value' => $question_id
												);
		$result = $this->OfferModel->getCountOffers($filter);
		return $result;
	}
}