<?php defined('BASEPATH') OR exit('No direct script access allowed');

class contacts extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		//$this->output->enable_profiler(TRUE);
		$this->mysmarty->assign('activeItem', 15);
		$this->load->model('../../models/ContactsModel');
		$this->load->model('../../models/ItemsModel');
		$this->load->model('../../models/OptionsModel');
		$this->load->model('../../models/LanguageModel');

		$this->defaultLangId = $this->OptionsModel->getDefaultLanguageId();
		$this->ItemsModel->setDefaultLanguage($this->defaultLangId);

		$this->secondMenu[0]=array(
							"name"   => "See all",
							"link"   => "contacts"
						);

		/*$this->secondMenu[1]=array(
							"name"   => "Add new",
							"link"   => "contacts/addnew"
						);
		*/

		//permissions
		if(!$this->ion_auth->is_admin())
		{
			if(!in_array('contacts-4', $this->_getPermissions()))
			{
				redirect('nopermission');
			}
		}


		if($lastEdited = $this->session->userdata('lastEditedContact'))
		{
			$this->secondMenu[2]=array(
								"name"   => "Last viewed: [".$lastEdited['name']."]",
								"link"   => "contacts/showfull/".$lastEdited['id']
							);
		}


		$this->mysmarty->assign('link', 'contacts');
	}

	// --------------------------------------------------------------------

	function index()
	{
		$this->mysmarty->assign('css', array('uitheme/cupertino/jquery-ui-1.7.1.custom.css'));

		//FILTERS

		//text search filter
		if($this->session->userdata('contactsSearchFilter'))
		{
			$filter['textSearchFilter'] = $this->session->userdata('contactsSearchFilter');
			$filter['customFilter'][] = array(
													'text' => $filter['textSearchFilter'],
													'fields' => array('client_name', 'content', 'mail', 'company')
												);
		}
		else
		{
			$filter['textSearchFilter'] = '';
		}

		//show by status filter
		if($this->session->userdata('contactsShowFilter') && $this->session->userdata('contactsShowFilter') != 1)
		{
			$filter['showFilter'] = $this->session->userdata('contactsShowFilter');

			if($filter['showFilter'] == 2 || $filter['showFilter'] == 3)
			{
				if($filter['showFilter'] == 2)
				{
					$filterValue = 0;
				}
				if($filter['showFilter'] == 3)
				{
					$filterValue = 1;
				}

				$filter['customFilter'][] = array(
													'field' => 'is_active',
													'value' => $filterValue
												);
			}
			elseif($filter['showFilter'] == 4)
			{
				$filter['customFilter'][] = array(
													'field' => 'spam_reported',
													'value' => 1
				);
			}
		}
		else
		{
			$filter['showFilter'] = 1;
		}

		if($this->session->userdata('contactsShowFilter1') && $this->session->userdata('contactsShowFilter1') != 1)
		{
			$filter['showFilter1'] = $this->session->userdata('contactsShowFilter1');

			if($filter['showFilter1'] == 2)
			{
				$filterValue = 0;
			}
			if($filter['showFilter1'] == 3)
			{
				$filterValue = 1;
			}

			$filter['customFilter'][] = array(
												'field' => 'contacts_type',
												'value' => $filterValue
											);
		}
		else
		{
			$filter['showFilter1'] = 1;
		}

		//show by client acceptation
		if($this->session->userdata('contactsShowFilter2') && $this->session->userdata('contactsShowFilter2') != 1)
		{
			$filter['showFilter2'] = $this->session->userdata('contactsShowFilter2');

			if($filter['showFilter2'] == 2)
			{
				$filterValue = 2;
			}
			if($filter['showFilter2'] == 3)
			{
				$filterValue = 1;
			}

			$filter['customFilter'][] = array(
												'field' => 'is_accepted',
												'value' => $filterValue
											);
		}
		else
		{
			$filter['showFilter2'] = 1;
		}

		//state filter
		/*if($this->session->userdata('contactsStateFilter') && $this->session->userdata('contactsStateFilter') != -1)
		{
			$filter['stateFilter'] = $this->session->userdata('contactsStateFilter');
			$filter['customFilter'][] = array(
												'field' => 'state_id',
												'value' => $filter['stateFilter']
											);
		}*/

		//category filter (not active now)
		if($this->session->userdata('commentCategoryFilter') === 0 || $this->session->userdata('commentCategoryFilter') > -1)
		{
			$filter['categoryFilter'] = $this->session->userdata('commentCategoryFilter');
			$filter['customFilter'][] = array(
												'field' => 'country',
												'value' => $filter['categoryFilter']
											);
		}
		else
		{
			 $filter['categoryFilter'] = -1;
		}

		//date filter
		if($this->session->userdata('filterDateFrom') && $this->session->userdata('filterDateTo'))
		{
			$filter['filterDateTo'] = $this->session->userdata('filterDateTo');


			$filter['filterDateFrom'] = $this->session->userdata('filterDateFrom');

			if($filter['filterDateTo'] != '' && $filter['filterDateFrom'] != '')
			{
				$dateFrom = $this->check_date($filter['filterDateFrom']);
				$dateTo = $this->check_date($filter['filterDateTo']);
				if($dateFrom && $dateTo)
				{
					$dateFromArray = explode('-', $dateFrom);
					$dateToArray = explode('-', $dateTo);
					$from = mktime(0, 0, 0, $dateFromArray[1], $dateFromArray[2], $dateFromArray[0]);
					$to = mktime(23, 59, 59, $dateToArray[1], $dateToArray[2], $dateToArray[0]);

					$filter['customFilter'][] = array(
							'date' => $filter['filterDateFrom'],
							'field' => 'add_date',
							'from' => $from,
							'to' => $to
					);
				}
				else
				{
					$this->mysmarty->assign('subheader', 'Invalid date given.');
				}
			}
		}
		else
		{
			$filter['filterDateTo'] = '';
			$filter['filterDateFrom'] = '';
		}


		$this->mysmarty->assign('filter', $filter);

		if($itemsCount = $this->ContactsModel->getCountContacts($filter))
		{
			if($this->uri->segment(3))
			{
				$offset = $this->uri->segment(3);
				$this->mysmarty->assign('offset', $offset);
			}
			else
			{
				$offset = 0;
			}

			$this->load->library('pagination2');

			$this->paginationConfig['config']['base_url'] = base_url().'contacts/index/';
			$this->paginationConfig['config']['total_rows'] = $itemsCount;



			if($this->session->userdata('perPage'))
			{
				$this->paginationConfig['config']['per_page'] = $this->session->userdata('perPage');
			}
			else
			{
				$this->paginationConfig['config']['per_page'] = $this->paginationConfig['itemsPerPage'][0];
			}


			$this->pagination2->initialize($this->paginationConfig['config']);

			$this->mysmarty->assign('pagination', $this->pagination2->create_links());
			$this->mysmarty->assign('currentPerPage', $this->paginationConfig['config']['per_page']);
			$this->mysmarty->assign('perPageLink', 'contacts/setItemsPerPage');
			$this->mysmarty->assign('allItems', $itemsCount);

			$result = $this->ContactsModel->getAllContacts($this->paginationConfig['config']['per_page'], $offset, 'add_date desc', $filter);
		}
		else
		{
			$result = array();
		}


		//if contacts/ad has been deleted set subheader
		if($this->session->flashdata('deleted'))
		{
			$this->mysmarty->assign('subheader', 'Contact has been deleted.');
		}

		$this->mysmarty->assign('countries', $this->getCountries());
		$this->mysmarty->assign('allItemsCount', $itemsCount);
		$this->mysmarty->assign('itemsList', $result);
		$this->mysmarty->assign('secondmenu', $this->secondMenu);
		$this->mysmarty->assign('activeSecondItem', 0);
		$this->mysmarty->display('contacts/showItems.tpl');
	}

	// --------------------------------------------------------------------

	function filter()
	{
		if($this->input->post('filter'))
		{
			$this->session->set_userdata('contactsSearchFilter', $this->input->post('filter'));
		}
		else
		{
			$this->session->set_userdata('contactsSearchFilter', '');
		}

		if($this->input->post('showFilter'))
		{
			$this->session->set_userdata('contactsShowFilter', $this->input->post('showFilter'));
		}
		else
		{
			$this->session->set_userdata('contactsShowFilter', '');
		}

		if($this->input->post('stateFilter'))
		{
			$this->session->set_userdata('contactsStateFilter', $this->input->post('stateFilter'));
		}
		else
		{
			$this->session->set_userdata('contactsStateFilter', '');
		}

		if($this->input->post('categoryFilter') || $this->input->post('categoryFilter') === 0)
		{
			$this->session->set_userdata('commentCategoryFilter', $this->input->post('categoryFilter'));
		}
		else
		{
			$this->session->set_userdata('commentCategoryFilter', -1);
		}

		if($this->input->post('showFilter1'))
		{
			$this->session->set_userdata('contactsShowFilter1', $this->input->post('showFilter1'));
		}
		else
		{
			$this->session->set_userdata('contactsShowFilter1', '');
		}

		if($this->input->post('showFilter2'))
		{
			$this->session->set_userdata('contactsShowFilter2', $this->input->post('showFilter2'));
		}
		else
		{
			$this->session->set_userdata('contactsShowFilter2', '');
		}

		if($this->input->post('filterDateFrom'))
		{
			$this->session->set_userdata('filterDateFrom', $this->input->post('filterDateFrom'));
		}
		else
		{
			$this->session->set_userdata('filterDateFrom', '');
		}

		if($this->input->post('filterDateTo'))
		{
			$this->session->set_userdata('filterDateTo', $this->input->post('filterDateTo'));
		}
		else
		{
			$this->session->set_userdata('filterDateTo', '');
		}

		$this->session->unset_userdata('contactsOffset');


		redirect('contacts');
	}

	// --------------------------------------------------------------------

	function showfull()
	{
		if(!$contactsid = $this->uri->segment(3))
		{
			redirect('contacts');
		}
		else
		{
			$this->mysmarty->assign('formLink', 'showfull/'.$contactsid);
			$this->mysmarty->assign('css', array('uitheme/cupertino/jquery-ui-1.7.1.custom.css'));
			//get contacts data
			$mainData = $this->ContactsModel->getContact($contactsid);

			$mainData[0]->countries = $this->getCountries();

			//set header
			$this->mysmarty->assign('header', 'See contact: <em>'.$mainData[0]->client_name.'</em>');

			//set second menu edit item
			$this->secondMenu[2]=array(
							"name"   => "See contact: [".$mainData[0]->client_name."]",
							"link"   => "contacts/showfull/".$contactsid
						);

			$lastEdited = array(
							"name"   => $mainData[0]->client_name,
							"id"     => $contactsid
						);

			$aBackData = $mainData[0];

			$this->session->set_userdata('lastEditedContact', $lastEdited);

			$this->mysmarty->assign('backData', $aBackData);
			$this->mysmarty->assign('secondmenu', $this->secondMenu);
			$this->mysmarty->assign('activeSecondItem', 2);

			$this->mysmarty->display('contacts/showItemInfo.tpl');
		}
	}

	// --------------------------------------------------------------------

	function removeContact()
	{
		//permissions
		if(!$this->ion_auth->is_admin())
		{
			if(!in_array('contacts-3', $this->_getPermissions()))
			{
				redirect('nopermission');
			}
		}

		if(!$contactsid = $this->uri->segment(3))
		{
			redirect('contacts');
		}
		else
		{
			$this->ContactsModel->removeContact($contactsid);
			$this->_checkLastEdited($contactsid);
			$this->session->set_flashdata('deleted', $contactsid);
			redirect('contacts');
		}
	}

	// --------------------------------------------------------------------

	function removeSelected()
	{
		if(isset($_POST['id']))
		{
			if(is_array($_POST['id']))
			{
				foreach($_POST['id'] as $contactsid)
				{
					$this->ContactsModel->removeContact($contactsid);
					$this->_checkLastEdited($contactsid);
				}
			}
		}

		redirect('contacts');
	}

	// --------------------------------------------------------------------

	function setItemsPerPage()
	{
		if(!$items = $this->uri->segment(3))
		{
			redirect('contacts');
		}
		else
		{
			$this->session->set_userdata('perPage', $items);
			redirect('contacts');
		}
	}

	// --------------------------------------------------------------------

	function _getPermissions()
	{
		$permissions = $this->ion_auth->get_permissions_for_group();
		$this->permissions = $permissions["parsedvalues"];
		if(!isset($this->permissions[0]))
		{
			$this->permissions = array();
		}
		return $this->permissions;
	}

	// --------------------------------------------------------------------

	function _checkLastEdited($itemid)
	{
		if($lastEdited = $this->session->userdata('lastEditedComment'))
		{
			if($lastEdited['id'] == $itemid)
			{
				$this->session->unset_userdata(array('lastEditedComment' => ''));
			}
		}
	}

	// --------------------------------------------------------------------

	function getCountries()
	{
		/*$filter['customFilter'][0] = array(
												'field' => 'is_active',
												'value' => 1
													);*/

		$result = $this->ItemsModel->getAllItems(9, 100, 0, 'title', 'asc', $filter = array());
		foreach($result as $key => $value)
		{
			$idsResult[$value->id] = $value;
		}

		if(isset($result[0]))
		{
			return $idsResult;
		}

		return false;
	}

	// --------------------------------------------------------------------

	function check_date(&$input)
	{
		$dateArray = explode('-', $input);
		if(isset($dateArray[0]) && isset($dateArray[1]) && isset($dateArray[2]))
		{
			return date('Y-m-d', mktime(0, 0, 0, $dateArray[1], $dateArray[2], $dateArray[0]));
		}
		else
		{
			$this->form_validation->set_message('check_date', "<strong>Date: '".$input."' is incorrect</strong>. Please enter correct date in 'YYYY-MM-DD' format.");
			return false;
		}
	}
}