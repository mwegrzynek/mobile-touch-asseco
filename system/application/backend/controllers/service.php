<?php defined('BASEPATH') OR exit('No direct script access allowed');

class service extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		
		
		//$this->output->enable_profiler(TRUE);
		$this->mysmarty->assign('activeItem', 7);
		$this->load->model('../../models/ServiceModel');
		$this->load->model('../../models/OptionsModel');
		$this->load->model('../../models/CategoryModel');     
		$this->load->model('../../models/InvoiceModel');     
		$this->load->model('../../models/UserModel');     
		$this->load->model('../../models/ItemsModel');     
		$this->load->model('../../models/LanguageModel');     

		$this->defaultLangId = $this->OptionsModel->getDefaultLanguageId();
		$this->ItemsModel->setDefaultLanguage($this->defaultLangId);

		$this->secondMenu[0]=array(
							"name"   => "Pokaż wszystkie",
							"link"   => "service"
						);

		/*$this->secondMenu[1]=array(
							"name"   => "Add new",
							"link"   => "service/addnew"
						);
		*/
		
		//permissions					
		if(!$this->ion_auth->is_admin())
		{			
			if(!in_array('service-4', $this->_getPermissions())) 
			{
				redirect('nopermission');
			}
		}

		if($lastEdited = $this->session->userdata('lastEditedService'))
		{
			$this->secondMenu[2]=array(
								"name"   => "Ostatnio przeglądane: [".$lastEdited['name']."]",
								"link"   => "service/showfull/".$lastEdited['id']
							);
		}


		$this->mysmarty->assign('link', 'service');
	}

	function index()
	{
		
		$this->mysmarty->assign('css', array('uitheme/cupertino/jquery-ui-1.7.1.custom.css'));
		
		//FILTERS

		//text search filter
		if($this->session->userdata('serviceSearchFilter'))
		{
			$filter['textSearchFilter'] = $this->session->userdata('serviceSearchFilter');
			$filter['customFilter'][] = array(					
													'text' => $filter['textSearchFilter'],
													'fields' => array('public_id')													
												);						
		}
		else
		{
			$filter['textSearchFilter'] = '';
		}

		//show by status (payed, not payed) filter
		if($this->session->userdata('serviceShowFilter') && $this->session->userdata('serviceShowFilter') != 1)
		{
			$filter['showFilter'] = $this->session->userdata('serviceShowFilter');

			if($filter['showFilter'] == 2)
			{
				$filterValue = 0;
			}
			if($filter['showFilter'] == 3)
			{
				$filterValue = 1;
			}

			$filter['customFilter'][] = array(
												'field' => 'is_paid',
												'value' => $filterValue
											);
		}
		else
		{
			$filter['showFilter'] = 1;
		}


		//show by type (SMR, WSE) filter
		if($this->session->userdata('serviceShowFilter1') && $this->session->userdata('serviceShowFilter1') != 1)
		{
			$filter['showFilter1'] = $this->session->userdata('serviceShowFilter1');

			if($filter['showFilter1'] == 2)
			{
				$filterValue = 0;
			}
			if($filter['showFilter1'] == 3)
			{
				$filterValue = 1;
			}

			$filter['customFilter'][] = array(
												'field' => 'service_type',
												'value' => $filterValue
											);
		}
		else
		{
			$filter['showFilter1'] = 1;
		}
		
		//show by client acceptation
		if($this->session->userdata('serviceShowFilter2') && $this->session->userdata('serviceShowFilter2') != 1)
		{
			$filter['showFilter2'] = $this->session->userdata('serviceShowFilter2');

			if($filter['showFilter2'] == 2)
			{
				$filterValue = 2;
			}
			if($filter['showFilter2'] == 3)
			{
				$filterValue = 1;
			}

			$filter['customFilter'][] = array(
												'field' => 'is_accepted',
												'value' => $filterValue
											);
		}
		else
		{
			$filter['showFilter2'] = 1;
		}

		//state filter
		if($this->session->userdata('serviceStateFilter') && $this->session->userdata('serviceStateFilter') != -1)
		{
			$filter['stateFilter'] = $this->session->userdata('serviceStateFilter');
			$filter['customFilter'][] = array(
												'field' => 'state_id',
												'value' => $filter['stateFilter']
											);
		}

		//category filter (not active now)
		if($this->session->userdata('serviceCategoryFilter') === 0 || $this->session->userdata('serviceCategoryFilter') > -1)
		{
			$filter['categoryFilter'] = $this->session->userdata('serviceCategoryFilter');
			$filter['customFilter'][] = array(
												'field' => 'type_id',
												'value' => $filter['categoryFilter']
											);			
		}
		else
		{
			 $filter['categoryFilter'] = -1;
		}
		
		//date filter
		if($this->session->userdata('filterDateFrom') && $this->session->userdata('filterDateTo'))
		{
			$filter['filterDateTo'] = $this->session->userdata('filterDateTo');
			
			
			$filter['filterDateFrom'] = $this->session->userdata('filterDateFrom');
			
			if($filter['filterDateTo'] != '' && $filter['filterDateFrom'] != '')
			{
				$dateFrom = $this->check_date($filter['filterDateFrom']);
				$dateTo = $this->check_date($filter['filterDateTo']);
				if($dateFrom && $dateTo)
				{
					$dateFromArray = explode('-', $dateFrom);
					$dateToArray = explode('-', $dateTo);
					$from = mktime(0, 0, 0, $dateFromArray[1], $dateFromArray[2], $dateFromArray[0]);
					$to = mktime(23, 59, 59, $dateToArray[1], $dateToArray[2], $dateToArray[0]);
					
					$filter['customFilter'][] = array(
							'date' => $filter['filterDateFrom'],
							'field' => 'add_date',
							'from' => $from,
							'to' => $to
					); 
				}
				else
				{
					$this->mysmarty->assign('subheader', 'Invalid date given.');					
				}
			}
		}
		else
		{
			$filter['filterDateTo'] = '';
			$filter['filterDateFrom'] = '';
		}

		$this->mysmarty->assign('filter', $filter);


		$this->mysmarty->assign('serviceTypes', $this->ServiceModel->getServiceTypes());

		if($itemsCount = $this->ServiceModel->getCountServices($filter))
		{
			$this->mysmarty->assign('currencies', $this->ServiceModel->getCurrencies());
			
			if($this->uri->segment(3))
			{
				$offset = $this->uri->segment(3);
				$this->mysmarty->assign('offset', $offset);
			}
			else
			{
				$offset = 0;
			}

			$this->load->library('pagination2');

			$this->paginationConfig['config']['base_url'] = base_url().'service/index/';
			$this->paginationConfig['config']['total_rows'] = $itemsCount;



			if($this->session->userdata('perPage'))
			{
				$this->paginationConfig['config']['per_page'] = $this->session->userdata('perPage');
			}
			else
			{
				$this->paginationConfig['config']['per_page'] = $this->paginationConfig['itemsPerPage'][0];
			}


			$this->pagination2->initialize($this->paginationConfig['config']);

			$this->mysmarty->assign('pagination', $this->pagination2->create_links());
			$this->mysmarty->assign('currentPerPage', $this->paginationConfig['config']['per_page']);
			$this->mysmarty->assign('perPageLink', 'service/setItemsPerPage');
			$this->mysmarty->assign('allItems', $itemsCount);

			$result = $this->ServiceModel->getAllServices($this->paginationConfig['config']['per_page'], $offset, 'add_date desc', $filter);
			
			foreach($result as $key => $res) 
			{
				$result[$key]->user_info = $this->UserModel->getUser($res->user_id);
				$result[$key]->entry_info = $this->ItemsModel->getItemById($res->entry_id, $this->defaultLangId);
			}
			
		}
		else
		{
			$result = array();
		}

		
		//if service/ad has been deleted set subheader
		if($this->session->flashdata('deleted'))
		{
			$this->mysmarty->assign('subheader', 'Usługa została usunięta.');		
		}

		//$this->mysmarty->assign('categories', $this->CategoryModel->getTree(/*category type here*/, 'name asc'));		
		$this->mysmarty->assign('allItemsCount', $itemsCount);
		$this->mysmarty->assign('itemsList', $result);
		$this->mysmarty->assign('secondmenu', $this->secondMenu);
		$this->mysmarty->assign('activeSecondItem', 0);
		$this->mysmarty->display('service/showServices.tpl');
	}


	function filter()
	{
		if($this->input->post('filter'))
		{
			$this->session->set_userdata('serviceSearchFilter', $this->input->post('filter'));
		}
		else
		{
			$this->session->set_userdata('serviceSearchFilter', '');
		}

		if($this->input->post('showFilter'))
		{
			$this->session->set_userdata('serviceShowFilter', $this->input->post('showFilter'));
		}
		else
		{
			$this->session->set_userdata('serviceShowFilter', '');
		}

		if($this->input->post('stateFilter'))
		{
			$this->session->set_userdata('serviceStateFilter', $this->input->post('stateFilter'));
		}
		else
		{
			$this->session->set_userdata('serviceStateFilter', '');
		}


		if($this->input->post('categoryFilter') || $this->input->post('categoryFilter') === 0)
		{
			$this->session->set_userdata('serviceCategoryFilter', $this->input->post('categoryFilter'));
		}
		else
		{
			$this->session->set_userdata('serviceCategoryFilter', '');
		}



		if($this->input->post('showFilter1'))
		{
			$this->session->set_userdata('serviceShowFilter1', $this->input->post('showFilter1'));
		}
		else
		{
			$this->session->set_userdata('serviceShowFilter1', '');
		}
		
		if($this->input->post('showFilter2'))
		{
			$this->session->set_userdata('serviceShowFilter2', $this->input->post('showFilter2'));
		}
		else
		{
			$this->session->set_userdata('serviceShowFilter2', '');
		}
		
		if($this->input->post('filterDateFrom'))
		{
			$this->session->set_userdata('filterDateFrom', $this->input->post('filterDateFrom'));
		}
		else
		{
			$this->session->set_userdata('filterDateFrom', '');
		}		
		
		if($this->input->post('filterDateTo'))
		{
			$this->session->set_userdata('filterDateTo', $this->input->post('filterDateTo'));
		}
		else
		{
			$this->session->set_userdata('filterDateTo', '');
		}		

		$this->session->unset_userdata('serviceOffset');


		redirect('service');
	}
	
	function sentconfirmation()
	{
		if(!$serviceid = $this->uri->segment(3))
		{
			redirect('service');
		}
		else
		{
			$this->ItemsModel->setDefaultLanguage($this->defaultLangId);
			
			//get service data
			$mainData = $this->ServiceModel->getService($serviceid);
			
			
			if($mainData[0]->fv_info != '' && $mainData[0]->waybill_num != '') 
			{
				$noticeConfig = '';
				//send message (notification) about new service to user
				$this->load->library('Notices');
				$noticeConfig['noticeType'] = 'orderSentConfirmation';						
				$noticeConfig['link'] = 'http://'.$_SERVER['HTTP_HOST'].'/showorder/'.$mainData[0]->public_id;
				$noticeConfig['usermail'] = $mainData[0]->customer_mail;
				$noticeConfig['topicid'] = 26;			
				$noticeConfig['maitype'] = 'text';			
				$noticeConfig['public_id'] = $mainData[0]->public_id;		
				$noticeConfig['fv_info'] = $mainData[0]->fv_info;		
				$noticeConfig['waybill_num'] = $mainData[0]->waybill_num;		
				$this->notices->SendNotice($noticeConfig);
				
				$data['id'] = $serviceid;
				$data['if_service_sent'] = 1;				
				$this->ServiceModel->updateService($data);
				$this->session->set_flashdata('confirmMailSent',1);
			}
			else
			{
				$this->session->set_flashdata('noInfoData',1);
			}
			
			redirect('service/showfull/'.$serviceid, 303);
		}
	}


	function showfull()
	{
		if(!$serviceid = $this->uri->segment(3))
		{
			redirect('service');
		}
		else
		{
			$this->mysmarty->assign('formLink', 'showfull/'.$serviceid);
			
			if($this->session->flashdata('fileUploaded'))
			{         
				$this->mysmarty->assign($this->session->flashdata('fileUploaded'), 1);
			}
			
			if($this->session->flashdata('noInfoData'))
			{         
				$this->mysmarty->assign('errors1', 1);
			}
			
			if($this->session->flashdata('confirmMailSent'))
			{         
				$this->mysmarty->assign('confirmMailSent', 1);
			}
			
			//get service data
			$mainData = $this->ServiceModel->getService($serviceid);			
			//$mainData[0]->products = $this->ServiceModel->parseProducts($mainData[0]->description);
			
			//get service MetaData
			$metaData = $this->ServiceModel->getServiceMetaData($serviceid);
			$metaData[0]->languages = unserialize($metaData[0]->languages);			
			$metaData[0]->serialized_data = unserialize($metaData[0]->serialized_data);			
			
			
			$this->mysmarty->assign('serviceTypes', $this->ServiceModel->getServiceTypes());
			$this->mysmarty->assign('currencies', $this->ServiceModel->getCurrencies());
			
			$entryInfo = $this->ItemsModel->getItemById($mainData[0]->entry_id, $this->defaultLangId);
			$this->mysmarty->assign('entryInfo', $entryInfo[0]);
					
		  
			//if slider
			if($mainData[0]->type_id == 119) 
			{
				$itemInfo = $this->ItemsModel->getItemById($metaData[0]->entry_slider_id, $this->defaultLangId);
				if(isset($itemInfo[0])) 
				{
					$metaData[0]->sliderInfo = $itemInfo;
				}
				else
				{
					$metaData[0]->sliderInfo = false;
				}
			}
			
			
			//if category / subcategory service
			if($mainData[0]->type_id == 116 || $mainData[0]->type_id == 117) 
			{
				$this->CategoryModel->setDefaultLanguage($this->defaultLangId);
				$categoryInfo = $this->CategoryModel->getCategoryFullInfo($metaData[0]->entry_category_id);				
				if(isset($categoryInfo[0])) 
				{
					$metaData[0]->categoryInfo = $categoryInfo;
				}
				else
				{
					$metaData[0]->categoryInfo = false;
				}
			}
			
			$this->mysmarty->assign('metaData', $metaData[0]);
		  
			
			//set header
			$this->mysmarty->assign('header', 'View service: <em>'.$mainData[0]->public_id.'</em>');

			//set second menu edit item
			$this->secondMenu[2]=array(
							"name"   => "Zobacz usługę: [".$mainData[0]->public_id."]",
							"link"   => "service/showfull/".$serviceid
						);

			$lastEdited = array(
							"name"   => $mainData[0]->public_id,
							"id"     => $serviceid
						);


			$mainData[0]->user_info = $this->UserModel->getUser($mainData[0]->user_id);
			$aBackData = $mainData[0];

			$langs = $this->LanguageModel->getAllLanguages();
			
			


			//var_dump($aBackData);
			$this->mysmarty->assign('backData', $aBackData);
						
			if(isset($_POST['change_status']))
			{					
				$config = array(
					array(
							'field'   => 'is_paid',
							'label'   => 'Is paid',
							'rules'   => 'trim|strip_tags'
						)
				);
	
				$this->form_validation->set_rules($config);
				
				if($this->form_validation->run())
				{						
					$data['id'] = $serviceid;
					$data['is_paid'] = $this->input->post('is_paid');
					$data['valid_thru'] = $this->input->post('valid_thru');
					$this->ServiceModel->updateService($data);
					// not put form again when page reload
					redirect(str_replace('/admin', '', $_SERVER['REDIRECT_URL']), 303);
				}
				elseif(validation_errors() != '') // Validation Failed
				{
					$this->mysmarty->assign('errorHeader', 'Data has not been changed.');
	
					//prepare for smarty as array
					$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));
	
					if(isset($aErrors[0]) && $aErrors[0]!='')
					{
						$this->mysmarty->assign('errors', $aErrors);
					}	
					$aBackData->is_active = $this->input->post('is_paid');
					
				}				
			}			
			
			
			$this->session->set_userdata('lastEditedService', $lastEdited);

			$this->mysmarty->assign('backData', $aBackData);
			$this->mysmarty->assign('secondmenu', $this->secondMenu);
			$this->mysmarty->assign('activeSecondItem', 2);

			$this->mysmarty->display('service/showServiceInfo.tpl');
		}
	}


	function removeService()
	{
		//permissions					
		if(!$this->ion_auth->is_admin())
		{			
			if(!in_array('service-3', $this->_getPermissions())) 
			{
				redirect('nopermission');
			}
		}
		
		
		if(!$serviceid = $this->uri->segment(3))
		{
			redirect('service');
		}
		else
		{
			$this->ServiceModel->removeService($serviceid);
			$this->_checkLastEdited($serviceid);
			$this->session->set_flashdata('deleted', $serviceid);
			redirect('service');
		}
	}

	function setItemsPerPage()
	{
		if(!$items = $this->uri->segment(3))
		{
			redirect('service');
		}
		else
		{
			$this->session->set_userdata('perPage', $items);
			redirect('service');
		}
	}

	function status()
	{
		$redirectAddress = 'service';
		if(isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] !='')
		{
			$path_parts = pathinfo($_SERVER['HTTP_REFERER']);
			if(isset($path_parts['basename']))
			{
				if(strpos($path_parts['dirname'], 'showfull'))
				{
					$redirectAddress .= '/showfull/'.$path_parts['basename'];
				}
				else
				{
					$redirectAddress .= '/index/'.$path_parts['basename'];
				}
			}
		}

		if(!$serviceid = $this->uri->segment(3))
		{
			redirect($redirectAddress);
		}
		else
		{
			$this->_changeStatus($serviceid);
			redirect($redirectAddress);
		}
	}

	function _changeStatus($serviceId)
	{
		if($this->ServiceModel->getStatus($serviceId))
		{
			//this sets service as not paid but will not back entry settings
			$data['id'] = $serviceId;
			$data['order_date'] = '0000-00-00 00:00:00';
			$data['valid_thru'] = '0000-00-00 00:00:00'; 	 				
			$this->ServiceModel->updateService($data);
		}
		else
		{
			$this->load->library('serviceslib');
			$this->serviceslib->setPaid($serviceId);
		}
	}

	function decode_html(&$input)
	{
		$input = html_entity_decode($input, ENT_QUOTES, 'UTF-8');
		return $input;
	}

	
	function _uploadFile($config, $field_name)
	{
		$this->load->library('upload', $config);
		if(!$this->upload->do_upload($field_name))
		{
			$warnings = $this->upload->display_errors('', '###');
			$aErrors = explode("###", substr(trim($warnings), 0, -3));
			return array(false, $aErrors);
		}
		else
		{
			$uploadData=$this->upload->data();
			$fileName=$uploadData['file_name'];
			return array(true, array($uploadData['file_name'], $uploadData['full_path']));
		}
	}
	
	function check_date($input)
	{
		$dateArray = explode('-', $input);
		if(isset($dateArray[0]) && isset($dateArray[1]) && isset($dateArray[2]))
		{
			return date('Y-m-d', mktime(0, 0, 0, $dateArray[1], $dateArray[2], $dateArray[0]));
		}
		else
		{
			//$this->form_validation->set_message('check_date', "<strong>Data: '".$input."' jest niepoprawna</strong>. Proszę wprowadzić datę w formacie 'YYYY-MM-DD'.");
			return false;
		}
	}
	
	function _checkLastEdited($itemid)
	{
		if($lastEdited = $this->session->userdata('lastEditedService'))
		{
			if($lastEdited['id'] == $itemid)
			{
				$this->session->unset_userdata(array('lastEditedService' => ''));
			}
		}
	}	
	
	function _getMessageTemplates()
	{
		$filter['customFilter'][0] = array(
								'field' => 'is_active',
								'value' => 1
			);
		
		$this->defaultLangId = $this->OptionsModel->getDefaultLanguageId();
		$this->ItemsModel->setDefaultLanguage($this->defaultLangId);	
		$result = $this->ItemsModel->getAllItems(
				6,
				10000000, 
				0, 
				'title', 
				'asc',
				-1,
				$filter
			);	
			
		return $result;	
	}
	
	
	function getInvoiceOriginal($serviceid)
	{
		$this->_getInvoice($serviceid);
	}
	
	function getInvoiceCopy($serviceid)
	{
		$this->_getInvoice($serviceid, true);
	}
	
	function _getInvoice($serviceid, $mode = false)
	{
		if(!$serviceid)
		{
			redirect('service');
		}
		else
		{
			$serviceInfo = $this->ServiceModel->getService($serviceid);
			if(isset($serviceInfo[0]))
			{
				$invoiceInfo = $this->InvoiceModel->getInvoicesByServiceId($serviceid);
				if(!$invoiceInfo)
				{
					$invoiceId = $this->_addInvoice($serviceInfo);
				}
				else
				{
					$invoiceId = $invoiceInfo->id;
				}
				
				if($mode) 
				{
					$this->InvoiceModel->getCopy($invoiceId);
				}
				else
				{
					$this->InvoiceModel->getOriginal($invoiceId);					
				}				
			}
			else
			{
				redirect('service');
			}
		}
	}
	
	function _addInvoice($serviceInfo)
	{
		$data['file_name'] = $serviceInfo[0]->public_id.'.pdf';		
		$data['service_id'] = $serviceInfo[0]->id;		
		return $this->InvoiceModel->addInvoice($data);
	}	
	
	function _getPermissions()
	{
		$permissions = $this->ion_auth->get_permissions_for_group();
		$this->permissions = $permissions["parsedvalues"];
		if(!isset($this->permissions[0])) 
		{
			$this->permissions = array();
		}
		return $this->permissions;
	}
}