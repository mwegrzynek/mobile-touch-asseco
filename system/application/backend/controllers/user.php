<?php defined('BASEPATH') OR exit('No direct script access allowed');

class user extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->output->enable_profiler(TRUE);
		
		$this->mysmarty->assign('activeItem', 6);
		$this->load->model('../../models/UserModel');      
		$this->load->model('../../models/OptionsModel');
		$this->load->model('../../models/ServiceModel');
		$this->load->model('../../models/ItemsModel');
		$this->load->model('../../models/MessagesModel');
		$this->load->model('../../models/CommentsModel');
		
		//load helpers
		$this->load->helper('post_data');
		
		$this->defaultLangId = $this->OptionsModel->getDefaultLanguageId();
		$this->ItemsModel->setDefaultLanguage($this->defaultLangId);

		//permissions					
		if(!$this->ion_auth->is_admin())
		{			
			if(!in_array('user-4', $this->_getPermissions())) 
			{
				redirect('nopermission');
			}
		}


		$this->secondMenu[0]=array(
							"name"   => "Wszyscy użytkownicy",
							"link"   => "user"
						);
						
		$this->secondMenu[1]=array(
							"name"   => "Dodaj użytkownika",
							"link"   => "user/addnew"
						);				

		if($lastEdited = $this->session->userdata('lastEditedUser'))
		{
			$this->secondMenu[2]=array(
								"name"   => "Ostatnio edytowany: [".$lastEdited['name']."]",
								"link"   => "user/showfull/".$lastEdited['id']
							);
		}


		$this->mysmarty->assign('scripts', array(
													'jquery.cookie.js',
													'sectiontools.js'
												));
											
		$this->mysmarty->assign('link', 'user');
	}
	
	// --------------------------------------------------------------------
	
	function index()
	{
		$this->mysmarty->assign('css', array('uitheme/cupertino/jquery-ui-1.7.1.custom.css'));
		
		$this->filter = array();		
		$this->setFilters();
		$this->getUserParents();

		if($itemsCount = $this->UserModel->getCountUsers($this->filter))
		{
			if($this->uri->segment(3))
			{
				$offset = $this->uri->segment(3);
				$this->mysmarty->assign('offset', $offset);
			}
			else
			{
				$offset = 0;
			}

			$this->load->library('pagination2');

			$this->paginationConfig['config']['base_url'] = base_url().'user/index/';
			$this->paginationConfig['config']['total_rows'] = $itemsCount;
			$this->paginationConfig['config']['uri_segment'] = 3;

			if($this->session->userdata('perPage'))
			{
				$this->paginationConfig['config']['per_page'] = $this->session->userdata('perPage');
			}
			else
			{
				$this->paginationConfig['config']['per_page'] = $this->paginationConfig['itemsPerPage'][0];
			}

			$this->pagination2->initialize($this->paginationConfig['config']);
			$this->mysmarty->assign('pagination', $this->pagination2->create_links());
			$this->mysmarty->assign('currentPerPage', $this->paginationConfig['config']['per_page']);
			$this->mysmarty->assign('perPageLink', 'user/setItemsPerPage');
			$this->mysmarty->assign('allItems', $itemsCount);

			$result = $this->UserModel->getAllUsers($this->paginationConfig['config']['per_page'], $offset, 'last_name asc, '.$this->db->dbprefix.'users.id asc', $this->filter);
			
			foreach($result as $key => $value) 
			{				
				if((int)$value->group_id === 4 && (int)$value->parent_id !== 0)
				{
					$result[$key]->parentInfo = $this->UserModel->getUser($value->parent_id);
				}
			}
		}
		else
		{
			$result = array();
		}      

		//if user has been deleted set subheader
		if($this->session->flashdata('deleted'))
		{
			$this->mysmarty->assign('subheader', 'Użytkownik oraz wszystkie jego usługi, komentarze i profil zostały usunięte');		
		}		
		
		$this->mysmarty->assign('allItemsCount', $itemsCount);
		$this->mysmarty->assign('itemsList', $result);
		$this->mysmarty->assign('secondmenu', $this->secondMenu);
		$this->mysmarty->assign('activeSecondItem', 0);
		$this->mysmarty->display('user/showUsers.tpl');
	}
		
	// --------------------------------------------------------------------

	function filter()
	{
		if($this->input->post('userTextFilter'))
		{
			$this->session->set_userdata('userTextFilter', $this->input->post('userTextFilter'));
		}
		else
		{
			$this->session->set_userdata('userTextFilter', '');
		}
		
		if($this->input->post('groupFilter'))
		{
			$this->session->set_userdata('userGroupFilter', $this->input->post('groupFilter'));
		}
		else
		{
			$this->session->set_userdata('userGroupFilter', '');
		}
		
		if($this->input->post('userParentFilter') || ((int) $this->input->post('userParentFilter')) === 0)
		{							
			$this->session->set_userdata('userParentFilter', $this->input->post('userParentFilter'));
		}
		else
		{
			$this->session->unset_userdata('userParentFilter');
		}

		$this->session->unset_userdata('userOffset');

		redirect('user');
	}

	// --------------------------------------------------------------------

	function addnew()
	{
		$this->load->model('../../models/ion_auth_front_model', 'ion_front');		
		//$res = $this->ion_front->get_user(9);
		//var_dump($res->row());
		
		
		
		//set header
		$this->mysmarty->assign('header', 'Dodaj nowego użytkownika.');
		
		$this->getUserParents();
		
		
		
		if(isset($_POST['first_name']))
		{
			$config = $this->getValidationConfig($add = true, $mailCheckFlag = true);				
			$this->form_validation->set_rules($config);
			
			if($this->form_validation->run())
			{									
				$uploadResults = $this->uploadAction();					
				$errors = $uploadResults['errors'];
				$fileName = $uploadResults['fileName'];					
				
				if(!isset($errors[0]))
				{
					$data = getPostDataToArray();				
					
					if($userId = $this->createAccount($data)) 
					{
						$this->mysmarty->assign('header', 'Nowe konto zostało utworzone: (<a href="user/showfull/'.$userId.'">'.$this->input->post('first_name').' '.$this->input->post('last_name').'</a>)');
						$this->mysmarty->assign('userId', $userId);
						
						//send message to user
					}					
				}
				else
				{
					$this->mysmarty->assign('errors', $errors);
				}				
			}
			elseif(validation_errors() != '') // Validation Failed
			{
				$this->mysmarty->assign('errorHeader', 'Dane nie zostały poprawnie zapisane.');	
				//prepare for smarty as array
				$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));	
				if(isset($aErrors[0]) && $aErrors[0]!='')
				{
					$this->mysmarty->assign('errors', $aErrors);
				}		
				$aBackData = getPostDataToArray();					
				
				$this->mysmarty->assign('backData', $aBackData);	
			}	
		}
		
			
		
		$this->mysmarty->assign('secondmenu', $this->secondMenu);
		$this->mysmarty->assign('activeSecondItem', 1);
		
		$this->mysmarty->assign('css', array('uitheme/cupertino/jquery-ui-1.7.1.custom.css'));
		$this->mysmarty->display('user/addUser.tpl');
		
	}

	// --------------------------------------------------------------------
	

	function showfull()
	{
		if(!$userid = $this->uri->segment(3))
		{
			redirect('user');
		}
		else
		{
			if($this->session->flashdata('passReset')) 
			{
				$this->mysmarty->assign('passReset', 1);
			}
			
			$this->load->model('../../models/ion_auth_front_model', 'ion_front');		
			
			$this->userid = $userid;			
			$this->mysmarty->assign('scripts', array(
												'jquery.cookie.js',
												'ui.tabs.js',
												'ui.tabs.ext.js',
												'tabs.js'
											));
											
			$this->mysmarty->assign('css', array('uitheme/cupertino/jquery-ui-1.7.1.custom.css'));

			$mainData = $this->UserModel->getUser($userid);
			//$this->userLangId = $mainData[0]->lang_id;			
			
			$lastEdited = array(
								"name"   => $mainData[0]->first_name." ".$mainData[0]->last_name,
								"id"     => $userid
							);
	
			$this->session->set_userdata('lastEditedUser', $lastEdited);
			$this->mysmarty->assign('formLink', 'showfull/'.$userid);         

			//set header
			$this->mysmarty->assign('header', 'Użytkownik: <em>'.$mainData[0]->first_name.' '.$mainData[0]->last_name.'</em>');

			
			//actions
			if(isset($_POST['message']))
			{
				$this->_sendMessage();
			}
			
			if(isset($_POST['add_topic']))
			{
				$this->_addTopic();
			}
			
			if(isset($_POST['change_status']))
			{
				$this->_changeStatus();
			}
			
			if(isset($_POST['change_filter']))
			{
				$this->_changeTopicFilter();
			}
			
			if(isset($_POST['reset_pass']))
			{
				$this->_resetPassword();
			}
			
			//get user data
			$this->getUserParents($userid);
			$this->getUserProfile($userid);	
			$this->getUserServices($userid);			
			$this->getUserMessages($userid);
			$this->getUserComments($userid);
			$this->getUserArticles($userid);

			//set second menu edit item
			$this->secondMenu[2]=array(
							"name"   => "Pokaż: [".$mainData[0]->first_name." ".$mainData[0]->last_name."]",
							"link"   => "user/showfull/".$userid
						);

			$aBackData = get_object_vars($mainData[0]);
			$aBackData['id'] = $userid;
			$this->mysmarty->assign('backData', $aBackData);
			
			if(isset($_POST['first_name']))
			{
				$mailCheckFlag = false;
				if($this->input->post('email') !== $aBackData['email']) 
				{
					$mailCheckFlag = true;
				}
				
				$config = $this->getValidationConfig($add = false, $mailCheckFlag);				
				$this->form_validation->set_rules($config);
	
				if($this->form_validation->run())
				{					
					
					$uploadResults = $this->uploadAction();					
					$errors = $uploadResults['errors'];
					$fileName = $uploadResults['fileName'];					
					
					if(!isset($errors[0]))
					{
						$data = getPostDataToArray();
						
						//main data
						$mainUserData['id']     = $userid;
						$mainUserData['email']  = $this->input->post('email');						
						$mainUserData['active'] = (int)$this->input->post('active');
						if($this->input->post('group_id') != $aBackData['group_id']) 
						{							
							$mainUserData['group_id'] = $this->input->post('group_id');														
						}						
						
						$this->UserModel->updateAccount($mainUserData);
						
						//change parent
						if($this->input->post('group_id') != 4)
						{
							$data['parent_id'] = 0;
							//unset($data['parent_id']);
						}						
						
						unset($data['email']);
						unset($data['active']);
						unset($data['group_id']);
						unset($data['MAX_FILE_SIZE']);
						
						$data['user_id'] = $userid;						
						
						if(isset($path))
						{
							$data['logo'] = $fileName;
						}
	
						if(!$this->UserModel->updateAccountMeta($data))
						{
							$aErrors[] = 'System nie mógł zmienić danych użytkownika. Proszę spróbuj później. Jeśli problem będzie się powtarzał skontaktuj się z administratorem systemu.';
							$this->mysmarty->assign('errors', $aErrors);
						}
					}
					else
					{
						$this->mysmarty->assign('errors', $errors);
					}
				}
				elseif(validation_errors() != '') // Validation Failed
				{
					$this->mysmarty->assign('errorHeader', 'Dane nie zostały poprawnie zapisane.');	
					//prepare for smarty as array
					$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));	
					if(isset($aErrors[0]) && $aErrors[0]!='')
					{
						$this->mysmarty->assign('errors', $aErrors);
					}				
				}
				
				if($this->input->post('first_name'))
				{
					$aBackData = getPostDataToArray();
					if(isset($path))
					{				
						$aBackData['logo'] = $fileName;
					}
					else
					{
						$aBackData['logo'] = $mainData[0]->logo;
					}
				}		
				
				$aBackData['id'] = $userid;				
				$this->mysmarty->assign('backData', $aBackData);	
				$lastEdited = array(
								"name"   => $aBackData["first_name"]." ".$aBackData["last_name"],
								"id"     => $userid
							);
	
				$this->session->set_userdata('lastEditedUser', $lastEdited);
			}		
					 
			$this->mysmarty->assign('secondmenu', $this->secondMenu);
			$this->mysmarty->assign('activeSecondItem', 2);

			//if password has been reset
			if($this->session->flashdata('passReset'))
			{
				$this->mysmarty->assign('passReset', 1);
			}
			
			$this->mysmarty->display('user/showUserInfo.tpl');
		}
	}
	
	// --------------------------------------------------------------------

	function removeUser()
	{
		if(!$this->ion_auth->is_admin())
		{
			if(!in_array('user-3', $this->_getPermissions())) 
			{
				unset($this->secondMenu[1]);
			}
		}
		
		if(!$userid = $this->uri->segment(3))
		{
			redirect('user');
		}
		else
		{
			$this->UserModel->deleteUser($userid);
			log_message('info', 'Usunięto usera o id = '.$userid);				
			
			$this->_checkLastEdited($userid);
			
			$this->session->set_flashdata('deleted', $userid);
			redirect('user');
		}
	}
	
	// --------------------------------------------------------------------

	function setItemsPerPage()
	{
		if(!$items = $this->uri->segment(3))
		{
			redirect('user');
		}
		else
		{
			$this->session->set_userdata('perPage', $items);
			redirect('user');
		}
	}
	
	// --------------------------------------------------------------------
		
	function deletetopic($topicid)
	{
		if($topicid) 
		{
			$this->MessagesModel->deletetopic($topicid);
		}
		header('Location: '.$_SERVER['HTTP_REFERER']);
		
		//redirect(str_replace('/admin', '', $_SERVER['REDIRECT_URL']), 303);
	}
	
	// --------------------------------------------------------------------
	
	function deleteComment($comment_id)
	{
		//permissions					
		if(!$this->ion_auth->is_admin())
		{			
			if(!in_array('comment-3', $this->_getPermissions())) 
			{
				redirect('nopermission');
			}
		}		
		
		if($comment_id) 
		{
			$this->CommentsModel->removeComment($comment_id);
		}
		header('Location: '.$_SERVER['HTTP_REFERER']);
		
		//redirect(str_replace('/admin', '', $_SERVER['REDIRECT_URL']), 303);
	}
	
	// --------------------------------------------------------------------

	function _uploadFile($config, $ifResize = null)
	{
		$this->load->library('upload', $config);

		$field_name = "thefile";

		if(!$this->upload->do_upload($field_name))
		{
			$warnings = $this->upload->display_errors('', '###');
			$aErrors = explode("###", substr(trim($warnings), 0, -3));
			return array(false, $aErrors);
		}
		else
		{
			$uploadData=$this->upload->data();
			$fileName=$uploadData['file_name'];

			if(is_array($ifResize))
			{
				if($uploadData['image_width'] > $ifResize['image_width'] || $uploadData['image_height'] > $ifResize['image_height'])
				{
					$config['image_library']   = 'GD2';
					$config['source_image']    = $uploadData['full_path'];
					//$config['create_thumb']  = TRUE;
					$config['maintain_ratio']  = false;
					$config['width']           = 140;
					$config['height']          = 80;

					$this->load->library('image_lib', $config);

					if(!$this->image_lib->resize())
					{
						//echo $this->image_lib->display_errors();
					}
				}
			}

			return array(true, array($uploadData['file_name'], $uploadData['full_path']));
		}
	}	
	
	// --------------------------------------------------------------------
	
	function removeLogo()
	{
		if(!$userid = $this->uri->segment(3))
		{
			redirect('user');
		}
		else
		{
			$this->UserModel->removeLogo($userid);			
			redirect('user/showfull/'.$userid);			
		}
	}
	
	// --------------------------------------------------------------------
	
	function _resetPassword()
	{
		if(!$userid = $this->uri->segment(3))
		{
			redirect('user');
		}
		else
		{
			$this->_resetPassAction($userid);
			$this->session->set_flashdata('passReset', '1');
			redirect('user/showfull/'.$userid.'#usrPasswordReset');			
		}
	}
	
	// --------------------------------------------------------------------
	
	function _resetPassAction($userid)
	{
		$userInfo = $this->UserModel->getUser($userid);		
		$mail     = $userInfo[0]->email;	
		
		//var_dump($userInfo);
		
		$randomPasswd = $this->generatePassword();
		//$randomPasswd = 666;	//for test	

		//send message (notification) to user			
		$this->load->library('Notices');
		$noticeConfig['maitype']    = 'html';
		$noticeConfig['noticeType'] = 'resetPassword';					
		$noticeConfig['link']       = 'http://'.$_SERVER['HTTP_HOST'].'/user/';
		$noticeConfig['userid']     = $userInfo[0]->id;
		$noticeConfig['topicid']    = 598;
		//$noticeConfig['lang_id']    = $this->languageid;			
		$noticeConfig['password']   = $randomPasswd;			
		
		if($this->input->post('reset_pass'))
		{			
			$this->notices->SendNotice($noticeConfig);
		}
		
		if($this->ion_front->change_password_as_admin($mail, $randomPasswd)) 
		{
			return true;
		}
		
		return false;
	}
	
	// --------------------------------------------------------------------
	
	function nip_check(&$nip)
	{			
		if($nip != '') 
		{			
			$this->load->helper('datacheck');		
			if(!is_evidence_nr($nip, 0))
			{
				$this->form_validation->set_message('nip_check', 'NIP nie jest poprawny. Wpisz poprawny numer NIP');	
				return false;
			}
			else
			{
				$nip = formatNip($nip);				
			}
		}
		
		return true;
	}
	
	// --------------------------------------------------------------------
	
	function group_check(&$parent_id)
	{		
		$group_id = $this->input->post('group_id');		
		if((int)$group_id != 4 && (int)$parent_id != -1)
		{
			$this->form_validation->set_message('group_check', 'Użytkownik musi należeć do grupy "Członek kancelarii", żeby mozna go było przyporządkować do którejś z kancelarii.');
			return false;
		}
		else
		{			
			return true;	
		}
	}
	
	// --------------------------------------------------------------------
	
	function mail_avail_check($mail)
	{			
		if($this->ion_front->email_check($mail))
		{
			$this->form_validation->set_message('mail_avail_check', 'Adres mail nie może zostać użyty. Mail jest zajęty przez innego użytkownika.');
			return false;
		}
		
		return true;	
	}
	
	
	// --------------------------------------------------------------------
	
	function _checkLastEdited($itemid)
	{
		if($lastEdited = $this->session->userdata('lastEditedUser'))
		{
			if($lastEdited['id'] == $itemid)
			{
				$this->session->unset_userdata(array('lastEditedUser' => ''));
			}
		}
	}
	
	// --------------------------------------------------------------------
	
	function _getMessageTemplates()
	{
		$filter['customFilter'][0] = array(
								'field' => 'is_active',
								'value' => 0
			);
		
		$this->defaultLangId = $this->OptionsModel->getDefaultLanguageId();
		$this->ItemsModel->setDefaultLanguage($this->defaultLangId);	
		$result = $this->ItemsModel->getAllItems(
				6,
				10000000, 
				0, 
				'title', 
				'asc',
				-1,
				$filter
			);	
			
		return $result;	
	}
	
	// --------------------------------------------------------------------
	
	function _getPermissions()
	{
		$permissions = $this->ion_auth->get_permissions_for_group();
		$this->permissions = $permissions["parsedvalues"];
		if(!isset($this->permissions[0])) 
		{
			$this->permissions = array();
		}
		return $this->permissions;
	}	
		
	// --------------------------------------------------------------------	
	
	function _sendMessage()
	{
		$data = array();
		$config = array(
			array(
					'field'   => 'message',
					'label'   => 'Wiadomość',
					'rules'   => 'trim|strip_tags|required'
				)
		);

		$this->form_validation->set_rules($config);
		
				
		if($this->form_validation->run())
		{
			$data['service_id']    = 0;
			$data['is_admin_send'] = 1;
			$data['user_id']       = $this->userid;
			$data['is_read']       = 0;
			$data['msg_text']      = $this->input->post('message');
			$data['topic_id']      = $this->input->post('topic_id');
			$this->MessagesModel->addUserMessage($data);	
			
			//send message (notification) to user			
			$this->load->library('Notices');
			$noticeConfig['noticeType'] = 'sendMessageNotice';					
			$noticeConfig['link']       = 'http://'.$_SERVER['HTTP_HOST'].'/user/messages/';
			$noticeConfig['userid']     = $this->userid;
			$noticeConfig['topicid']    = 114;
			//$noticeConfig['lang_id']  = $this->userLangId;			
			$this->notices->SendNotice($noticeConfig);			
			
			//update topic date
			$topicData['id'] = $data['topic_id'];
			$topicData['last_changed_date'] = date('Y-m-d H:i:s');
			$this->MessagesModel->updateTopic($topicData);
			
			// not put form again when page reload
			redirect(str_replace('/admin', '', $_SERVER['REDIRECT_URL']), 303);
			
		}
		elseif(validation_errors() != '') // Validation Failed
		{			
			$this->mysmarty->assign('messageErrorHeader'.$this->input->post('topic_id'), 'Message has not been sent.');
			//prepare for smarty as array
			$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));

			if(isset($aErrors[0]) && $aErrors[0]!='')
			{
				$this->mysmarty->assign('messageErrors'.$this->input->post('topic_id'), $aErrors);
			}
		}
	}
	
	// --------------------------------------------------------------------
	
	function _addTopic()
	{
		$data = array();
		$config = array(
			array(
					'field'   => 'topic_name',
					'label'   => 'Topic name',
					'rules'   => 'trim|strip_tags|required'
				)
		);

		$this->form_validation->set_rules($config);
		
		if($this->form_validation->run())
		{
			$data['name'] = $this->input->post('topic_name');
			$data['user_id'] = $this->userid;
			$data['add_date'] = date('Y-m-d H:i:s');
			$data['last_changed_date'] = $data['add_date'];
			$data['status_id'] = 1;
			
			$this->MessagesModel->addTopic($data);			
			
			// not put form again when page reload
			redirect(str_replace('/admin', '', $_SERVER['REDIRECT_URL']), 303);					
			
		}
		elseif(validation_errors() != '') // Validation Failed
		{
			$this->mysmarty->assign('topicEerrorHeader', 'Topic has not been added.');

			//prepare for smarty as array
			$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));

			if(isset($aErrors[0]) && $aErrors[0]!='')
			{
				$this->mysmarty->assign('topicErrors', $aErrors);
			}
		}
	}
	
	// --------------------------------------------------------------------
	
	function _changeStatus()
	{
		if($this->input->post('status_id'))
		{
			$data['id'] = $this->input->post('topic_id');
			$data['status_id'] = $this->input->post('status_id');
			
			$this->MessagesModel->updateTopic($data);		
			
			// not put form again when page reload
			redirect(str_replace('/admin', '', $_SERVER['REDIRECT_URL']), 303);					
		}		
	}
	
	// --------------------------------------------------------------------
	
	function _changeTopicFilter()
	{
		if($this->input->post('topicStatus'))
		{
			$this->session->set_userdata('topicStatus', $this->input->post('topicStatus'));
		}
		else
		{
			$this->session->set_userdata('topicStatus', '');
		}
		redirect(str_replace('/admin', '', $_SERVER['REDIRECT_URL']), 303);
	}
	
	// --------------------------------------------------------------------
	
	function getValidationConfig($add = false, $mailCheckFlag = false)
	{
		$config = array(
				0 =>	array(
							'field'   => 'first_name',
							'label'   => 'Imię',
							'rules'   => 'trim|strip_tags|required'
						),
				1 =>	array(
							'field'   => 'last_name',
							'label'   => 'Nazwisko',
							'rules'   => 'trim|strip_tags|required'
						),
				2 =>	array(
							'field'   => 'email',
							'label'   => 'Email',
							'rules'   => 'trim|valid_email|required'
						),
				3 =>	array(
							'field'   => 'phone',
							'label'   => 'Telefon',
							'rules'   => 'trim|strip_tags'
						),
				4 =>	array(
							'field'   => 'company',
							'label'   => 'Nazwa Kancelarii',
							'rules'   => 'trim|strip_tags'
						),
				5 =>	array(
							'field'   => 'invoice_data_name',
							'label'   => 'Nazwa',
							'rules'   => 'trim'
						),	
				6 =>	array(
							'field'   => 'invoice_data_city',
							'label'   => 'Miasto',
							'rules'   => 'trim|strip_tags'
						),
				7 =>	array(
							'field'   => 'invoice_data_address',
							'label'   => 'Adres',
							'rules'   => 'trim'
						),					
				8 =>	array(
							'field'   => 'invoice_data_zipcode',
							'label'   => 'Kod pocztowy',
							'rules'   => 'trim|strip_tags'
						),
				9 =>	array(
							'field'   => 'company_web_address',
							'label'   => 'Adres www',
							'rules'   => 'trim|strip_tags|prep_url'
						),
				10 =>	array(
							'field'   => 'invoice_data_taxid',
							'label'   => 'NIP',
							'rules'   => 'trim|callback_nip_check'
						)	
				);
				
		if($add) 
		{
			$config[] = array(
				'field'   => 'group_id',
				'label'   => 'Grupa',
				'rules'   => 'trim|required|is_natural'
			);
			
			$config[] = array(
				'field'   => 'parent_id',
				'label'   => 'Kancelaria do której należy',
				'rules'   => 'trim|required|numeric|callback_group_check'
			);
			
			$config[] = array(
				'field'   => 'password',
				'label'   => 'Hasło',
				'rules'   => 'trim|min_length[5]'
			);			
		}
		
		if($mailCheckFlag) 
		{
			$config[2]['rules'] .= '|callback_mail_avail_check';
		}
		
		return $config;
	}
	
	// --------------------------------------------------------------------
	
	function getUserParents($userid = -1)
	{		
		$filter['customFilter'][0] = array(					
													'field' => 'group_id !=',
													'value' => '4'
												);			
		$filter['customFilter'][1] = array(					
													'field' => 'group_id !=',
													'value' => '3'
												);														
		$filter['customFilter'][2] = array(					
													'field' => 'user_id !=',
													'value' => $userid
												);
		
		$result = $this->UserModel->getAllUsers(1000, 0, 'company asc, '.$this->db->dbprefix.'users.id asc', $filter);		
		$this->mysmarty->assign('userParents', $result);		
		
		return $result;		
	}
	
	// --------------------------------------------------------------------
	
	function getUserProfile($userid)
	{		
		$filter['customFilter'][0] = array(
											'field' => 'li_num_data_1',
											'value' => $userid
										);			
		//$result = $this->ItemsModel->getAllItems(4,	1000, 0, 'add_date', 'desc', $filter);
		$result = $this->ItemsModel->getAllItems(4,	1, 0, 'add_date', 'desc', $filter);
		$this->mysmarty->assign('userEntries', $result);

		return $result;
	}
	
	// --------------------------------------------------------------------
	
	function getUserServices($userid)
	{
		$filter['customFilter'][0] = array(
												'field' => 'user_id',
												'value' => $userid
											);			
		//get user services
		$result = $this->ServiceModel->getAllServices(1000, 0, 'add_date desc', $filter);
		$this->mysmarty->assign('userServices', $result);
		
		$this->mysmarty->assign('serviceTypes', $this->ServiceModel->getServiceTypes());
		$this->mysmarty->assign('currencies', $this->ServiceModel->getCurrencies());
		
		return $result;
	}
	
	// --------------------------------------------------------------------
	
	function getUserMessages($userid)
	{
		//filter topics				
		if($this->session->userdata('topicStatus') && $this->session->userdata('topicStatus') != -1)
		{
			$filter['topicFilter'] = $this->session->userdata('topicStatus');				
			$this->topicFilter = $filter['topicFilter'];
		}
		else
		{
			$filter['topicFilter'] = -1;
			$this->topicFilter = false;
		}	
		$this->mysmarty->assign('messagesFilter', $filter);
		
		$messagesCount = 0;		
		$userTopics = $this->MessagesModel->getAllUserTopics($userid, $this->topicFilter);
		if(isset($userTopics[0])) 
		{
			foreach($userTopics as $key => $value) 
			{
				$userTopics[$key]->messages = $this->MessagesModel->getAllUserMessages($userid, $value->id);
				$messagesCount += count($userTopics[$key]->messages);
			}
		}
		$this->mysmarty->assign('userTopics', $userTopics);
		$this->mysmarty->assign('userMessagesCount', $messagesCount);
		
		$statuses = $this->MessagesModel->getAllStatuses();
		$this->mysmarty->assign('statuses', $statuses);
		
		return array(
			'userTopics'        => $userTopics,
			'userMessagesCount' => $messagesCount,
			'statuses'          => $statuses
		);
	}
	
	// --------------------------------------------------------------------
	
	function getUserComments($userid)	
	{
		$filter['customFilter'][0] = array(
										'field' => 'user_id',
										'value' => $userid
									);
		$result = $this->CommentsModel->getAllComments(1000, 0, 'add_date desc', $filter);
		$this->mysmarty->assign('userComments', $result);
		
		return $result;
	}
	
	// --------------------------------------------------------------------
	
	function getUserArticles($userid)
	{
		$filter['customFilter'][0] = array(
											'field' => 'li_num_data_1',
											'value' => $userid
										);
		$result = $this->ItemsModel->getAllItems(15,	1000, 0, 'add_date', 'desc', $filter);
		$this->mysmarty->assign('userArticles', $result);

		return $result;
	}
	
	// --------------------------------------------------------------------
	
	function setFilters()
	{
		//filters
		//text search filter
		if($this->session->userdata('userTextFilter'))
		{
			$filter['userTextFilter'] = $this->session->userdata('userTextFilter');
			$this->filter['customFilter'][] = array(					
													'text' => $filter['userTextFilter'],
													'fields' => array('first_name', 'company', 'email', 'last_name')													
												);													
		}
		else
		{
			$filter['userTextFilter'] = '';
		}
		
		
		//show by group filter
		if($this->session->userdata('userGroupFilter') && $this->session->userdata('userGroupFilter') != 1)
		{
			$filter['groupFilter'] = $this->session->userdata('userGroupFilter');

			if($filter['groupFilter'] == 2)
			{
				$filterValue = 1;
			}
			if($filter['groupFilter'] == 3)
			{
				$filterValue = 2;
			}
			if($filter['groupFilter'] == 4)
			{
				$filterValue = 3;
			}
			if($filter['groupFilter'] == 5)
			{
				$filterValue = 4;
			}			

			$this->filter['customFilter'][] = array(
												'field' => 'group_id',
												'value' => $filterValue
											);
		}
		else
		{
			$filter['groupFilter'] = 1;
		}
		
		//parent
		$parentFilter = $this->session->userdata('userParentFilter');
		if(($parentFilter === 0 || $parentFilter > -1 ) && is_numeric($parentFilter))
		{
			$filter['parentFilter'] = $parentFilter;				
			$this->filter['customFilter'][] = array(					
													'value' => $parentFilter,
													'field' => 'parent_id'
												);				
		}
		else
		{
			$filter['parentFilter'] = -1;
		}

		//$this->UserModel->getUsersFromClassifieds();

		$this->mysmarty->assign('filter', $filter);
	}
	
	// --------------------------------------------------------------------
	
	function createAccount($data)
	{
		//change parent
		if($data['group_id'] != 4)
		{
			$data['parent_id'] = 0;			
		}
		
		//if password empty generate random
		if($data['password'] == '') 
		{			
			$data['password'] = $this->generatePassword();
		}
		
		if(!isset($data['auto_send'])) 
		{
			$data['auto_send'] = false;
		}
		
		$username  = strtolower($data['first_name'].'_'.$data['last_name']);
		$email     = $data['email'];
		$active    = $data['active'];
		$password  = $data['password'];
		$auto_send = $data['auto_send'];
		
		
		$tempData = $data;
		unset($data['email']);
		unset($data['auto_send']);
		unset($data['password']);
		unset($data['active']);
		unset($data['MAX_FILE_SIZE']);
		
		$userId = $this->ion_front->register($username, $password, $email, $data);
		
		if(!$active) 
		{
			$this->ion_front->deactivate($userId);
		}
		
		if($auto_send) 
		{
			$tempData['user_id'] = $userId;
			$this->sendAccountInfo($tempData);
		}
		
		return $userId;
	}
	
	// --------------------------------------------------------------------
	
	function sendAccountInfo($data)
	{
		//send message (notification) to user			
		$this->load->library('Notices');
		$noticeConfig['noticeType'] = 'newAccountByAdminNotice';
		$noticeConfig['email']      = $data['email'];
		$noticeConfig['maitype']    = 'html';
		$noticeConfig['password']   = $data['password'];
		$noticeConfig['topicid']    = 594;
		$noticeConfig['userid']     = $data['user_id'];
		$noticeConfig['link']       = 'http://'.$_SERVER['HTTP_HOST'].'/user/show/'.$data['user_id'];
		
		$this->notices->SendNotice($noticeConfig);	
		return true;
	}
	
	// --------------------------------------------------------------------
	
	
	function uploadAction($currentFileName = '')
	{
		$errors = array();
		$fileName = '';
		
		if(isset($_FILES['thefile']['name']) && $_FILES['thefile']['name'] != '')
		{			
			//file save
			$config['upload_path']     = '../pictures/user/logos';
			$config['allowed_types']   = 'jpg|gif|png';
			$config['max_size']	      = '150000';
			$config['overwrite']	      = FALSE;
			$config['max_width']       = '0';
			$config['max_height']      = '0';
			$config['remove_spaces']   = TRUE;

			/*$ifResize['image_width'] 	= 140;
			$ifResize['image_height'] 	= 80;*/
			$uploadResult = $this->_uploadFile($config);

			if($uploadResult[0])
			{
				//get uploaded file path
				$path = str_replace($uploadResult[1][0], '', $uploadResult[1][1]);
				$fileName = $uploadResult[1][0];
				
				//delete old logo if exists
				if($currentFileName != '')
				{							
					//var_dump($aBackData->company_logo);
					$this->load->helper('file');
					@unlink($config['upload_path'].'/'.$currentFileName);
				}
			}
			else
			{
				$errors = $uploadResult[1];
				$this->mysmarty->assign('errors1', $errors);
			}					
			
		}
		
		return array(
					'errors' => $errors,
					'fileName' => $fileName
				);
	}
	
	// --------------------------------------------------------------------
	
	function generatePassword()
	{
		$this->load->helper('string');
		return random_string('alnum', 8);
	}
}
