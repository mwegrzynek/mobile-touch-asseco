<?php

	class itemsvideos extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			
			$this->load->model('../../models/ItemsModel');
			$this->load->model('../../models/VideosModel');
			$this->load->model('../../models/LanguageModel');
			$this->load->model('../../models/MetaDataModel');
			$this->load->model('../../models/OptionsModel');


			// Check item type by name
			$itemName = $this->uri->segment(2, 0);	
			if(!$itemName)
			{
				show_error('Error checking item type. Invalid URL');
			}

			// Get item congfiguration
			$this->config->load('items_config', FALSE, TRUE);
			$this->itemConfig = $this->config->item($itemName);
			//var_dump($this->itemConfig);	
			
			//permissions					
			if(!$this->ion_auth->is_admin())
			{			
				if(!in_array($this->itemConfig['mainName'].'-2', $this->_getPermissions())) 
				{
					redirect('nopermission');
				}
			}
			
			$this->mysmarty->assign('activeItem', $this->itemConfig['activeMenuItem']);

			//alt tags and picture title are done by meta tags system
			$this->MetaDataModel->setType(99);

			$this->mysmarty->assign('link', 'itemsvideos/'.$this->itemConfig['mainName']);
			$this->mysmarty->assign('itemMainName', $this->itemConfig['mainName']);

			$this->defaultLangId = $this->OptionsModel->getDefaultLanguageId();

			$this->ItemsModel->setDefaultLanguage($this->defaultLangId);

			$this->secondMenu[0]=array(
								"name"   => "All movies",
								"link"   => "itemsvideos/".$this->itemConfig['mainName']
							);

			$this->secondMenu[1]=array(
								"name"   => "Add new movie",
								"link"   => "itemsvideos/".$this->itemConfig['mainName']."/addnew"
							);

			if($lastEdited = $this->session->userdata($this->itemConfig['menuLastEditedVarName'].'Picture'))
			{
				$this->secondMenu[2]=array(
									"name"   => "Last edited: [".$lastEdited['name']."]",
									"link"   => "itemsvideos/".$this->itemConfig['mainName']."/edit/".$lastEdited['id']
								);
			}
			
			$this->load->library('getdatalib');
		}

		function _chooseItem()
		{
		   if($this->input->post('itemid'))
			{
				$this->session->set_userdata('itemid', $this->input->post('itemid'));
			}
			elseif(is_numeric($this->uri->segment(4)))
			{
				$this->session->set_userdata('itemid', $this->uri->segment(4));
			}
			else
			{
				$this->session->unset_userdata('itemid');	
			}
			redirect('itemsvideos/'.$this->itemConfig['mainName']);
		}

		function index()
		{
			$this->_checkMethodName();
			
			$this->mysmarty->assign('css', array(
													'uitheme/cupertino/jquery-ui-1.7.1.custom.css',
													'jquery.fancybox.css'
												));

			if($itemsCount = $this->ItemsModel->getCountItems($this->itemConfig['itemType']))
			{
				$this->itemsTree = $this->ItemsModel->getTree($this->itemConfig['itemType'], $this->itemConfig['indexItemsOrderBy'], $this->itemConfig['indexItemsOrder']);
			}
			else
			{
				$this->itemsTree = array();
			}			
			$this->mysmarty->assign('itemsTree', $this->itemsTree);


			$itemid = $this->session->userdata('itemid');
			$this->mysmarty->assign('itemId', $itemid);

			$videosCount = 0;
			$result = array();

			if($itemid)
			{
				
				//parent Item title
				$itemName = $this->ItemsModel->getItemTitle($itemid, $this->defaultLangId);
				$this->mysmarty->assign('itemId', $itemid);
				$this->mysmarty->assign('itemName', $itemName);


				//get all videos in parent gallery				
				if($videosCount = $this->VideosModel->getCountVideos($itemid))
				{					

					if($this->uri->segment(3))
					{
						$offset = $this->uri->segment(3);
						$this->mysmarty->assign('offset', $offset);
					}
					else
					{
						$offset = 0;
					}

					$this->load->library('pagination2');

					$this->paginationConfig['config']['base_url'] = base_url().'itemsvideos/'.$this->itemConfig['mainName'].'/';
					$this->paginationConfig['config']['uri_segment'] = 3;
					$this->paginationConfig['config']['total_rows'] = $videosCount;

					if($this->session->userdata('perPage'))
					{
						$this->paginationConfig['config']['per_page'] = $this->session->userdata('perPage');
					}
					else
					{
						$this->paginationConfig['config']['per_page'] = $this->paginationConfig['itemsPerPage'][0];
					}

					$this->pagination2->initialize($this->paginationConfig['config']);

					$this->mysmarty->assign('pagination', $this->pagination2->create_links());
					$this->mysmarty->assign('currentPerPage', $this->paginationConfig['config']['per_page']);
					$this->mysmarty->assign('perPageLink', 'itemsvideos/'.$this->itemConfig['mainName'].'/setItemsPerPage');

					$result = $this->VideosModel->getVideos($this->paginationConfig['config']['per_page'], $offset, 'position asc', $itemid);
				
					foreach($result as $key => $res)
					{
						$meta = $this->MetaDataModel->getMetaData($res->id, $this->defaultLangId);
						//var_dump($meta);
						if(isset($meta[0]))
						{
							$result[$key]->title = $meta[0]->title;
						}
					}
				}
				else
				{
					$result = array();
				}
			}

			$this->mysmarty->assign('allVideosCount', $videosCount);
			$this->mysmarty->assign('videosList', $result);
			$this->mysmarty->assign('secondmenu', $this->secondMenu);
			$this->mysmarty->assign('activeSecondItem', 0);
			$this->mysmarty->display('itemsvideos/showVideos.tpl');
		}

		function _setItemsPerPage()
		{
			if(!$items = $this->uri->segment(3))
			{
				redirect('itemsvideos');
			}
			else
			{
				$this->session->set_userdata('perPage', $items);
				redirect('itemsvideos');
			}
		}


		function _addnew()
		{
			$itemid = $this->session->userdata('itemid');
			$this->mysmarty->assign('itemId', $itemid);

			if(!$itemid)
			{
				redirect('itemsvideos/'.$this->itemConfig['mainName']);
			}

			$itemName = $this->ItemsModel->getItemTitle($itemid, $this->defaultLangId);
			$this->mysmarty->assign('itemId', $itemid);
			$this->mysmarty->assign('itemName', $itemName);


			//set default
			$aBackData['position'] = 1;

			//get default lang name
			$result = $this->LanguageModel->getLanguage($this->defaultLangId);

			$this->mysmarty->assign('header', 'Add new video to <em>'.$itemName.'</em>');			
			//$this->mysmarty->assign('subheader', 'Najpierw dodaj w domyślnym języku: [<em>'.$result[0]->name.'</em>]. Potem edytuj w innych językach.');


			//set second menu
			$this->mysmarty->assign('secondmenu', $this->secondMenu);
			$this->mysmarty->assign('activeSecondItem', 1);

			//set second menu
			$this->mysmarty->assign('secondmenu', $this->secondMenu);
			$this->mysmarty->assign('activeSecondItem', 1);

			//set validation rules
			$config = array(
				array(
						'field'   => 'link',
						'label'   => 'Link to video',
						'rules'   => 'trim|required'
					)			
			);

			$this->form_validation->set_rules($config);

			if($this->form_validation->run())
			{
				$error = false;
				$result = $this->_checkVideoFrom($this->input->post('link'));
				
				if($result == 'yt') 
				{
					//get You Tube video info
					$this->load->library('video_yt');
					$this->video_yt->setUrl($this->input->post('link'));
					$typeId = 1;					
					if(!$movieInfo = $this->video_yt->getMovieInfo())
					{
						$aErrors = $this->video_yt->displayErrors();
						$error = true;							
					}
				}
				else
				{
					//get Vimeo video info
					$this->load->library('video_vm');
					$this->video_vm->setUrl($this->input->post('link'));
					$typeId = 2;
					if(!$movieInfo = $this->video_vm->getMovieInfo())
					{
						$aErrors = $this->video_vm->displayErrors();
						$error = true;							
					}						
				}					
				

				if(!$error)
				{
					$movieTitle = $movieInfo['title'];
					//$movieDuration = $movieInfo['duration'];

					$meta = $this->input->post('meta');
					if($meta['title'] == '')
					{
						$title = $movieTitle;
					}
					else
					{
						$title = $meta['title'];
					}

					$videoid = $this->VideosModel->addVideo
					(
						array(
								'link'        		=> $this->input->post('link'),
								'position'        => $this->input->post('position'),
								'item_id'     		=> $itemid,
								'add_date'       	=> date("Y-m-d H:i:s"),
								'duration'       	=> '0:00',
								'type_id'       	=> $typeId,
								'thumbnail_url'   => $movieInfo['thumbnail']
							)
					);

					if($videoid)
					{
						$metaData = $this->getdatalib->fillTableDataWithPost($this->MetaDataModel->getMetaInfoTableFields($except = array('id', 'meta_data_id', 'language_id')), $sub = 'meta');
						$metaData['title'] = $title;
						$languages = $this->LanguageModel->getAllLanguages();		
						foreach($languages as $key => $value) 
						{							
							$this->MetaDataModel->addMetaData($videoid, $value->id, $metaData);
						}					

						$this->mysmarty->assign('header', 'Data has been successfully saved.');
						$this->mysmarty->assign('subheader', '<a href="itemsvideos/'.$this->itemConfig['mainName'].'/edit/'.$videoid.'">Edit '.$title.'</a>');
					}
				}
				else
				{
					$title = $this->input->post('title');
					$this->mysmarty->assign('errorHeader', 'Video has not been saved.');
					//$aErrors = $this->video_yt->displayErrors();
					$this->mysmarty->assign('errors', $aErrors);
				}
			}
			elseif(validation_errors() != '') // Validation Failed
			{
				$this->mysmarty->assign('errorHeader', 'Video has not been saved.');

				//prepare for smarty as array
				$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));

				if(isset($aErrors[0]) && $aErrors[0] != '')
				{
					$this->mysmarty->assign('errors', $aErrors);
				}
			}

			//there were errors
			if(isset($_POST['position']) && !isset($add))
			{
				$aBackData['link']				= $this->input->post('link');
				$aBackData['title'] 				= $title;
				$aBackData['position']			= $this->input->post('position');

				$meta = $this->input->post('meta');
				$aBackData['metaInfo'] = $meta;		
			}

			$this->mysmarty->assign('backData', $aBackData);
			$this->mysmarty->display('itemsvideos/videoAddAction.tpl');
		}

		/* EDIT */
		function _edit()
		{
			$this->mysmarty->assign('css', array(
													'jquery.fancybox.css',
												));

			if(!$videoid = $this->uri->segment(4))
			{
				redirect('itemsvideos/'.$this->itemConfig['mainName']);
			}
			else
			{
				if(!$this->VideosModel->checkVideo($videoid))
				{
					redirect('itemsvideos/'.$this->itemConfig['mainName']);
					exit();
				}
				else
				{
					$this->videoid = $videoid;
				}

				if($itemsCount = $this->ItemsModel->getCountItems($this->itemConfig['itemType']))
				{
					$this->itemsTree = $this->ItemsModel->getTree($this->itemConfig['itemType'], $this->itemConfig['indexItemsOrderBy'], $this->itemConfig['indexItemsOrder']);
				}
				else
				{
					$this->itemsTree = array();
				}			
				$this->mysmarty->assign('itemsTree', $this->itemsTree);

				//get simple info
				$simpleInfo = $this->VideosModel->getVideo($videoid);
				$this->mysmarty->assign('video', $simpleInfo[0]);

				$aBackData['link'] 		= $simpleInfo[0]->link;
				$aBackData['videoid'] 	= $this->videoid;
				$aBackData['position'] 	= $simpleInfo[0]->position;
				

				$itemid = $this->session->userdata('itemid');
				
				if(!isset($_POST['langid']) || !isset($_POST['position']))
				{					
					$itemid = $simpleInfo[0]->item_id;					
				}				
			
				$this->mysmarty->assign('itemId', $itemid);

				if(!$itemid)
				{
					redirect('itemsvideos'.$this->itemConfig['mainName']);
				}

				$itemName = $this->ItemsModel->getItemTitle($itemid, $this->defaultLangId);
				$this->mysmarty->assign('itemId', $itemid);
				$this->mysmarty->assign('itemName', $itemName);
				
				
				$aBackData['itemid'] = $itemid;
				$aBackData['languages'] = $this->_getAllLangDependedData($videoid);
				$title = $aBackData['languages'][$this->defaultLangId]['metaInfo']['title'];				
				
				//set header
				if($this->session->flashdata('changed'))
				{
					//$header = 'Dane zostały poprawnie zapisane. Zdjęcie zostało przeniesione do produktu: <em>'.$itemName.'</em>';
					$header = 'Data has been successfully saved. <br/> Video has been moved to <em>'.$itemName.'</em>';
				}
				else
				{
					$header = 'Edit video <em>'.$title.'</em> in <em>'.$itemName.'</em>';
				}
				
				
				if(!isset($_POST['itemid']))
				{
					$itemid = $simpleInfo[0]->item_id;
				}
				
				//$this->mysmarty->assign('galleryId', $itemid);

				if(!$itemid)
				{
					redirect('itemsvideos');
				}


				$this->mysmarty->assign('header', $header);


				//set second menu edit item
				$this->secondMenu[2]=array(
								"name"   => "Edit: [".$title."]",
								"link"   => 'itemsvideos/'.$this->itemConfig['mainName'].'/edit/'.$videoid
							);

				$lastEdited = array(
								"name"   => $title,
								"id"     => $videoid
							);

				$this->session->set_userdata('lastEditeditemsvideos', $lastEdited);

				$this->mysmarty->assign('secondmenu', $this->secondMenu);
				$this->mysmarty->assign('activeSecondItem', 2);

				

				if(isset($_POST['langid']))
				{
					$langid = $_POST['langid'];
					$metaData = $this->getdatalib->fillTableDataWithPost($this->MetaDataModel->getMetaInfoTableFields($except = array('id', 'meta_data_id', 'language_id')), $sub = 'meta');
					$this->MetaDataModel->addMetaData($videoid, $langid, $metaData);
					if($this->input->post('submit-and-next'))
					{
						$nextLangCode = $aBackData['languages'][$langid]['next_lang_code'];
						redirect('itemsvideos/'.$this->itemConfig['mainName'].'/edit/'.$videoid.'#'.$nextLangCode);						
					}
				}
				else
				{
					$config = array(
						array(
							'field'   => 'link',
							'label'   => 'Link to video',
							'rules'   => 'trim|required'
						)					
					);
					$this->form_validation->set_rules($config);	

					$error = false;	
					if($this->form_validation->run())
					{
						//get You Tube video info
						$result = $this->_checkVideoFrom($this->input->post('link'));
					
						if($result == 'yt') 
						{
							//get You Tube video info
							$this->load->library('video_yt');
							$this->video_yt->setUrl($this->input->post('link'));
							$typeId = 1;					
							if(!$movieInfo = $this->video_yt->getMovieInfo())
							{
								$aErrors = $this->video_yt->displayErrors();
								$error = true;							
							}
						}
						else
						{
							//get Vimeo video info
							$this->load->library('video_vm');
							$this->video_vm->setUrl($this->input->post('link'));
							$typeId = 2;
							if(!$movieInfo = $this->video_vm->getMovieInfo())
							{
								$aErrors = $this->video_vm->displayErrors();
								$error = true;							
							}						
						}					
						
		
						if(!$error)
						{
							//$movieTitle = $movieInfo['title'];
							//$movieDuration = $movieInfo['duration'];
	
							/*if(!$this->input->post('metaTitle'))
							{
								$title = $movieTitle;
							}
							else
							{
								$title = $this->input->post('metaTitle');
							}*/
	
							$data = array(
										'id'            => $videoid,
										'link'          => $this->input->post('link'),
										'item_id'       => $this->input->post('itemid'),
										'position'      => $this->input->post('position'),
										'duration'      => '0:00',
										'type_id'       => $typeId,
										'thumbnail_url' => $movieInfo['thumbnail']
								);					
	
							$change = $this->VideosModel->changeVideo($data);
							$this->mysmarty->assign('header', 'Data has been successfully changed.');
	
													
							if($itemid != $this->input->post('itemid'))
							{
								$this->session->set_userdata('itemid', $this->input->post('itemid'));
								$this->session->set_flashdata('changed', 1);
								redirect('itemsvideos/'.$this->itemConfig['mainName'].'/edit/'.$videoid);
							}
						}
						else
						{
							$title = $this->input->post('title');
							$this->mysmarty->assign('errorHeader', 'Data has not been changed.');
							$aErrors = $this->video_yt->displayErrors();
							$this->mysmarty->assign('errors', $aErrors);
						}
					}
					else
					{
						$this->mysmarty->assign('errorHeader', 'Data has not been changed.');
	
						//prepare for smarty as array
						$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));
						if(isset($aErrors[0]) && $aErrors[0]!='')
						{
							$this->mysmarty->assign('errors', $aErrors);
						}
					}
				}

				if(isset($_POST['itemid']))
				{
					$aBackData['link']     = $this->input->post('link');
					$aBackData['itemid']   = $this->input->post('itemid');
					$aBackData['position'] = $this->input->post('position');
					
				}
				elseif(isset($_POST['langid']))
				{
					$meta = $this->input->post('meta');
					$aBackData['languages'][$langid]['metaInfo'] = $meta;		
				}

				$this->mysmarty->assign('backData', $aBackData);
				$this->mysmarty->display('itemsvideos/videoEditAction.tpl');
			}	
		}
		
		// --------------------------------------------------------------------

		function _removeVideo()
		{
			if(!$videoid = $this->uri->segment(4))
			{
				redirect('itemsvideos/'.$this->itemConfig['mainName']);
			}
			else
			{
				if($this->VideosModel->checkVideo($videoid))
				{
					//$this->VideosModel->deleteVideo($videoid);
					$this->VideosModel->removeVideo($videoid);
					$this->MetaDataModel->removeMetaData($videoid);
					$this->_checkLastEdited($videoid);
				}
				redirect('itemsvideos/'.$this->itemConfig['mainName']);
			}
		}
		
		// --------------------------------------------------------------------
		
		function _checkLastEdited($pictureid)
		{
			if($lastEdited = $this->session->userdata($this->itemConfig['menuLastEditedVarName'].'Picture'))
			{
				if($lastEdited['id'] == $pictureid)
				{
					$this->session->unset_userdata(array($this->itemConfig['menuLastEditedVarName'].'Picture' => ''));
				}
			}
		}

		// --------------------------------------------------------------------

		function _uploadFile($config, $ifResize = null)
		{
			$this->load->library('upload', $config);
			$field_name = "thefile";

			if(!$this->upload->do_upload($field_name))
			{
				$warnings = $this->upload->display_errors('', '###');
				$aErrors = explode("###", substr(trim($warnings), 0, -3));
				return array(false, $aErrors);
			}
			else
			{
				$uploadData=$this->upload->data();
				$fileName=$uploadData['file_name'];

				$ifResize = false;
				if(is_array($ifResize))
				{
					if($uploadData['image_width'] > $ifResize['image_width']/* || $uploadData['image_height'] > $ifResize['image_height']*/)
					{						
						$config['image_library']   = 'GD2';
						$config['source_image']    = $uploadData['full_path'];						
						$config['master_dim']  		= 'width';
						$config['maintain_ratio']  = TRUE;
						$config['width']           = $ifResize['image_width'];
						$config['height']          = 400;

						$this->load->library('image_lib', $config);

						if(!$this->image_lib->resize())
						{
							$warnings = $this->image_lib->display_errors('', '###');
							$aErrors = explode("###", substr(trim($warnings), 0, -3));
							return array(false, $aErrors);
						}
					}
				}
				return array(true, array($uploadData['file_name'], $uploadData['full_path']));
			}
		}
		
		function _checkMethodName()
		{
			$methodName = $this->uri->segment(3, 0);			
			if($methodName === 'addnew')
			{
				$this->_addnew();
				exit();				
			}
			elseif($methodName === 'edit')
			{
				$this->_edit();
				exit();				
			}
			elseif($methodName === 'setItemsPerPage')
			{
				$this->_setItemsPerPage();
				exit();				
			}
			elseif($methodName === 'removeVideo')
			{
				$this->_removeVideo();
				exit();				
			}
			elseif($methodName === 'chooseitem')
			{				
				$this->_chooseItem();
				exit();				
			}
			return true;				
		}
		
		function _getPermissions()
		{
			$permissions = $this->ion_auth->get_permissions_for_group();
			$this->permissions = $permissions["parsedvalues"];
			if(!isset($this->permissions[0])) 
			{
				$this->permissions = array();
			}
			return $this->permissions;
		}
		
		
		function _checkVideoFrom($link)
		{
			$pos = strpos($link, 'vimeo.com/');
			if ($pos === false) 
			{
				return 'yt';
			} 
			else 
			{
			   return 'vm';
			}			
		}
		
		// --------------------------------------------------------------------
		
		function _getAllLangDependedData($itemid)
		{
			$languages = $this->getLanguages();

			$metaDefaultInfo = null;
			
			if($this->itemConfig['metaDataType'] != -1)
			{
				$metaDefaultInfo = $this->MetaDataModel->getMetaData($itemid, $this->defaultLangId);
			}		

			foreach($languages as $key => $lang)
			{				
				$metaInfo = array();				
				if($this->defaultLangId == $lang->id)
				{
					$metaInfo = $metaDefaultInfo;
				}
				else
				{
					$metaInfo = $this->MetaDataModel->getMetaData($itemid, $lang->id);						
				}
				
				if(!count($metaInfo))
				{
					$metaInfo = $this->_makeEmptyStructure($this->MetaDataModel->getEmptyStructure());					
				}
						
								
				if(isset($languages[$key+1]))
				{
					$nextItem = $languages[$key+1];
				}
				else
				{
					$nextItem = $languages[0];
				}

				$backData[$lang->id] = array(
										'id'             => $lang->id,
										'lang_code'      => $lang->lang_code,
										'name'           => $lang->name,
										'next_lang_code' => $nextItem->lang_code
									);
									
								
				$backData[$lang->id]['metaInfo'] = get_object_vars($metaInfo[0]);
				
			}		
			
			return $backData;
		}
		
		
		// --------------------------------------------------------------------
		
		
		function getLanguages()
		{
			$languages = $this->LanguageModel->getAllLanguages();			
			
			//default language always first			
			if(count($languages) > 1) 
			{
				foreach($languages as $key => $value) 
				{
					if(($value->id == $this->defaultLangId) && $key != 0) 
					{
						unset($languages[$key]);
						array_unshift($languages, $value);						
					}
				}
			}
			
			return $languages;
		}
		
		// --------------------------------------------------------------------
		
		function _makeEmptyStructure($structure)
		{
			$defaults = array(
				'position' => 1,
				'is_active' => 1
			);
			
			if(isset($structure[0])) 
			{
				$objVars	= get_object_vars($structure[0]);
				foreach($objVars as $key => $value) 
				{
					$newValue = '';
					if(isset($defaults[$key])) 
					{
						$newValue = $defaults[$key];
					}
					
					$structure[0]->$key = $newValue;
				}
				return $structure;
			}
			return false;
		}
	}
