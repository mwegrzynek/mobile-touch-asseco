<?php

class offer extends CI_Controller
{
	function __construct()
	{
		parent::__construct();	
		
		//$this->output->enable_profiler(TRUE);
		$this->mysmarty->assign('activeItem', 8);
		$this->load->model('../../models/OfferModel');
		$this->load->model('../../models/QuestionsModel');
		$this->load->model('../../models/OptionsModel');
		$this->load->model('../../models/CategoryModel');     
		$this->load->model('../../models/UserModel');
		$this->load->model('../../models/ItemsModel');     
		$this->load->model('../../models/LanguageModel');     

		$this->defaultLangId = $this->OptionsModel->getDefaultLanguageId();
		$this->ItemsModel->setDefaultLanguage($this->defaultLangId);
		$this->CategoryModel->setDefaultLanguage($this->defaultLangId);

		$this->secondMenu[0]=array(
							"name"   => "Pokaż wszystkie",
							"link"   => "offer"
						);

		/*$this->secondMenu[1]=array(
							"name"   => "Add new",
							"link"   => "offer/addnew"
						);
		*/
		
		//permissions					
		if(!$this->ion_auth->is_admin())
		{			
			if(!in_array('offer-4', $this->_getPermissions())) 
			{
				redirect('nopermission');
			}
		}
		

		if($lastEdited = $this->session->userdata('lastEditedOffer'))
		{
			$this->secondMenu[2]=array(
								"name"   => "Last edited: [".$lastEdited['name']."]",
								"link"   => "offer/showfull/".$lastEdited['id']
							);
		}


		$this->mysmarty->assign('link', 'offer');
	}
	
	// --------------------------------------------------------------------

	function index()
	{
		$this->mysmarty->assign('css', array('uitheme/cupertino/jquery-ui-1.7.1.custom.css'));
		
		//FILTERS

		//text search filter
		if($this->session->userdata('offerSearchFilter'))
		{
			$filter['textSearchFilter'] = $this->session->userdata('offerSearchFilter');
			$filter['customFilter'][] = array(					
													'text' => $filter['textSearchFilter'],
													'fields' => array('public_id')													
												);						
		}
		else
		{
			$filter['textSearchFilter'] = '';
		}

		//show by status filter
		if($this->session->userdata('offerShowFilter') && $this->session->userdata('offerShowFilter') != 1)
		{
			$filter['showFilter'] = $this->session->userdata('offerShowFilter');
		
			if($filter['showFilter'] == 2 || $filter['showFilter'] == 3)
			{
				if($filter['showFilter'] == 2)
				{
					$filterValue = 1;
				}
				if($filter['showFilter'] == 3)
				{
					$filterValue = 0;
				}
	
				$filter['customFilter'][] = array(
													'field' => 'is_accepted',
													'value' => $filterValue
												);
			}
			/*elseif($filter['showFilter'] == 4)
			{
				$filter['customFilter'][] = array(
													'field' => 'spam_reported',
													'value' => 1
				);						
			}*/
		}
		else
		{
			$filter['showFilter'] = 1;
		}


		//show by type (SMR, WSE) filter
		if($this->session->userdata('offerShowFilter1') && $this->session->userdata('offerShowFilter1') != 1)
		{
			$filter['showFilter1'] = $this->session->userdata('offerShowFilter1');

			if($filter['showFilter1'] == 2)
			{
				$filterValue = 0;
			}
			if($filter['showFilter1'] == 3)
			{
				$filterValue = 1;
			}

			$filter['customFilter'][] = array(
												'field' => 'offer_type',
												'value' => $filterValue
											);
		}
		else
		{
			$filter['showFilter1'] = 1;
		}
		
		//show by client acceptation
		if($this->session->userdata('offerShowFilter2') && $this->session->userdata('offerShowFilter2') != 1)
		{
			$filter['showFilter2'] = $this->session->userdata('offerShowFilter2');

			if($filter['showFilter2'] == 2)
			{
				$filterValue = 2;
			}
			if($filter['showFilter2'] == 3)
			{
				$filterValue = 1;
			}

			$filter['customFilter'][] = array(
												'field' => 'is_accepted',
												'value' => $filterValue
											);
		}
		else
		{
			$filter['showFilter2'] = 1;
		}

		//state filter
		if($this->session->userdata('offerStateFilter') && $this->session->userdata('offerStateFilter') != -1)
		{
			$filter['stateFilter'] = $this->session->userdata('offerStateFilter');
			$filter['customFilter'][] = array(
												'field' => 'state_id',
												'value' => $filter['stateFilter']
											);
		}

		//category filter (not active now)
		/*if($this->session->userdata('offerCategoryFilter') === 0 || $this->session->userdata('offerCategoryFilter') > -1)
		{
			$filter['categoryFilter'] = $this->session->userdata('offerCategoryFilter');
			$filter['customFilter'][] = array(
												'field' => 'type_id',
												'value' => $filter['categoryFilter']
											);			
		}
		else
		{
			 $filter['categoryFilter'] = -1;
		}*/
		
		//date filter
		if($this->session->userdata('filterDateFrom') && $this->session->userdata('filterDateTo'))
		{
			$filter['filterDateTo'] = $this->session->userdata('filterDateTo');
			
			
			$filter['filterDateFrom'] = $this->session->userdata('filterDateFrom');
			
			if($filter['filterDateTo'] != '' && $filter['filterDateFrom'] != '')
			{
				$dateFrom = $this->check_date($filter['filterDateFrom']);
				$dateTo = $this->check_date($filter['filterDateTo']);
				if($dateFrom && $dateTo)
				{
					$dateFromArray = explode('-', $dateFrom);
					$dateToArray = explode('-', $dateTo);
					$from = mktime(0, 0, 0, $dateFromArray[1], $dateFromArray[2], $dateFromArray[0]);
					$to = mktime(23, 59, 59, $dateToArray[1], $dateToArray[2], $dateToArray[0]);
					
					$filter['customFilter'][] = array(
							'date' => $filter['filterDateFrom'],
							'field' => 'add_date',
							'from' => $from,
							'to' => $to
					); 
				}
				else
				{
					$this->mysmarty->assign('subheader', 'Invalid date given.');					
				}
			}
		}
		else
		{
			$filter['filterDateTo'] = '';
			$filter['filterDateFrom'] = '';
		}

		$this->mysmarty->assign('filter', $filter);

		if($itemsCount = $this->OfferModel->getCountOffers($filter))
		{
			if($this->uri->segment(3))
			{
				$offset = $this->uri->segment(3);
				$this->mysmarty->assign('offset', $offset);
			}
			else
			{
				$offset = 0;
			}

			$this->load->library('pagination2');

			$this->paginationConfig['config']['base_url'] = base_url().'offer/index/';
			$this->paginationConfig['config']['total_rows'] = $itemsCount;



			if($this->session->userdata('perPage'))
			{
				$this->paginationConfig['config']['per_page'] = $this->session->userdata('perPage');
			}
			else
			{
				$this->paginationConfig['config']['per_page'] = $this->paginationConfig['itemsPerPage'][0];
			}


			$this->pagination2->initialize($this->paginationConfig['config']);

			$this->mysmarty->assign('pagination', $this->pagination2->create_links());
			$this->mysmarty->assign('currentPerPage', $this->paginationConfig['config']['per_page']);
			$this->mysmarty->assign('perPageLink', 'offer/setItemsPerPage');
			$this->mysmarty->assign('allItems', $itemsCount);

			$result = $this->OfferModel->getAllOffers($this->paginationConfig['config']['per_page'], $offset, 'add_date desc', $filter);
			
			foreach($result as $key => $res) 
			{
				$result[$key]->user_info = $this->UserModel->getUser($res->user_id);						
				$result[$key]->question_info = $this->QuestionsModel->getQuestion($res->question_id);						
			}			
		}
		else
		{
			$result = array();
		}

		
		//if offer/ad has been deleted set subheader
		if($this->session->flashdata('deleted'))
		{
			$this->mysmarty->assign('subheader', 'Pytanie zostało usunięte.');		
		}

		$this->mysmarty->assign('categories', $this->CategoryModel->getTree($itemType = 1, 'name asc'));		
		$this->mysmarty->assign('allItemsCount', $itemsCount);
		$this->mysmarty->assign('itemsList', $result);
		$this->mysmarty->assign('secondmenu', $this->secondMenu);
		$this->mysmarty->assign('activeSecondItem', 0);
		$this->mysmarty->display('offer/showOffers.tpl');
	}
	
	// --------------------------------------------------------------------

	function filter()
	{
		if($this->input->post('filter'))
		{
			$this->session->set_userdata('offerSearchFilter', $this->input->post('filter'));
		}
		else
		{
			$this->session->set_userdata('offerSearchFilter', '');
		}

		if($this->input->post('showFilter'))
		{
			$this->session->set_userdata('offerShowFilter', $this->input->post('showFilter'));
		}
		else
		{
			$this->session->set_userdata('offerShowFilter', '');
		}

		if($this->input->post('stateFilter'))
		{
			$this->session->set_userdata('offerStateFilter', $this->input->post('stateFilter'));
		}
		else
		{
			$this->session->set_userdata('offerStateFilter', '');
		}


		if($this->input->post('categoryFilter') || $this->input->post('categoryFilter') === 0)
		{
			$this->session->set_userdata('offerCategoryFilter', $this->input->post('categoryFilter'));
		}
		else
		{
			$this->session->set_userdata('offerCategoryFilter', '');
		}



		if($this->input->post('showFilter1'))
		{
			$this->session->set_userdata('offerShowFilter1', $this->input->post('showFilter1'));
		}
		else
		{
			$this->session->set_userdata('offerShowFilter1', '');
		}
		
		if($this->input->post('showFilter2'))
		{
			$this->session->set_userdata('offerShowFilter2', $this->input->post('showFilter2'));
		}
		else
		{
			$this->session->set_userdata('offerShowFilter2', '');
		}
		
		if($this->input->post('filterDateFrom'))
		{
			$this->session->set_userdata('filterDateFrom', $this->input->post('filterDateFrom'));
		}
		else
		{
			$this->session->set_userdata('filterDateFrom', '');
		}		
		
		if($this->input->post('filterDateTo'))
		{
			$this->session->set_userdata('filterDateTo', $this->input->post('filterDateTo'));
		}
		else
		{
			$this->session->set_userdata('filterDateTo', '');
		}		

		$this->session->unset_userdata('offerOffset');


		redirect('offer');
	}
	
	// --------------------------------------------------------------------	
	
	function showfull()
	{
		if(!$offerid = $this->uri->segment(3))
		{
			redirect('offer');
		}
		else
		{
			$this->mysmarty->assign('formLink', 'showfull/'.$offerid);						
			$this->mysmarty->assign('css', array('uitheme/cupertino/jquery-ui-1.7.1.custom.css'));
			//get offer data
			$mainData = $this->OfferModel->getOffer($offerid);
						
			
			$offerName = substr(strip_tags($mainData[0]->content), 0, 80);
						
			//set header
			$this->mysmarty->assign('header', 'Zobacz pytanie: <em>'.$offerName.'</em>');

			//set second menu edit item
			$this->secondMenu[2]=array(
							"name"   => "Zobacz pytanie: [".$offerName."]",
							"link"   => "offer/showfull/".$offerid
						);

			$lastEdited = array(
							"name"   => $offerName,
							"id"     => $offerid
						);
						
						
			if(isset($_POST['change_status']))
			{					
				$config = array(
					array(
							'field'   => 'is_closed',
							'label'   => 'Czy zakończone',
							'rules'   => 'trim|strip_tags'
						)/*,
					array(
							'field'   => 'spam_reported',
							'label'   => 'Czy oznaczony jako spam',
							'rules'   => 'trim|strip_tags'
								)*/
				);
	
				$this->form_validation->set_rules($config);
				
				if($this->form_validation->run())
				{						
					$this->OfferModel->setStatus($offerid, $this->input->post('is_closed'));
					//$this->OfferModel->setSpamStatus($offerid, $this->input->post('spam_reported'));
					// not put form again when page reload
					redirect(str_replace('/admin', '', $_SERVER['REDIRECT_URL']), 303);
				}
				elseif(validation_errors() != '') // Validation Failed
				{
					$this->mysmarty->assign('errorHeader', 'Data has not been changed.');
	
					//prepare for smarty as array
					$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));
	
					if(isset($aErrors[0]) && $aErrors[0]!='')
					{
						$this->mysmarty->assign('errors', $aErrors);
					}	
					$aBackData->is_active = $this->input->post('is_active');					
				}				
			}			


			$mainData[0]->user_info = $this->UserModel->getUser($mainData[0]->user_id);
			$aBackData = $mainData[0];

			$langs = $this->LanguageModel->getAllLanguages();
		
			
			$this->session->set_userdata('lastEditedOffer', $lastEdited);

			$this->mysmarty->assign('backData', $aBackData);
			$this->mysmarty->assign('secondmenu', $this->secondMenu);
			$this->mysmarty->assign('activeSecondItem', 2);

			$this->mysmarty->display('offer/showInfo.tpl');
		}
	}
	
	// --------------------------------------------------------------------

	function removeOffer()
	{
		//permissions					
		if(!$this->ion_auth->is_admin())
		{			
			if(!in_array('offer-3', $this->_getPermissions())) 
			{
				redirect('nopermission');
			}
		}
		
		if(!$offerid = $this->uri->segment(3))
		{
			redirect('offer');
		}
		else
		{
			$this->OfferModel->removeOffer($offerid);
			$this->_checkLastEdited($offerid);
			$this->session->set_flashdata('deleted', $offerid);
			redirect('offer');
		}
	}
	
	// --------------------------------------------------------------------

	function setItemsPerPage()
	{
		if(!$items = $this->uri->segment(3))
		{
			redirect('offer');
		}
		else
		{
			$this->session->set_userdata('perPage', $items);
			redirect('offer');
		}
	}
	
	// --------------------------------------------------------------------

	function status()
	{
		$redirectAddress = 'offer';
		if(isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] !='')
		{
			$path_parts = pathinfo($_SERVER['HTTP_REFERER']);
			if(isset($path_parts['basename']))
			{
				if(strpos($path_parts['dirname'], 'showfull'))
				{
					$redirectAddress .= '/showfull/'.$path_parts['basename'];
				}
				else
				{
					$redirectAddress .= '/index/'.$path_parts['basename'];
				}
			}
		}

		if(!$offerid = $this->uri->segment(3))
		{
			redirect($redirectAddress);
		}
		else
		{
			$this->_changeStatus($offerid);
			redirect($redirectAddress);
		}
	}
	
	// --------------------------------------------------------------------
	
	function spamstatus()
	{
		$redirectAddress = 'offer';
		if(isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] !='')
		{
			$path_parts = pathinfo($_SERVER['HTTP_REFERER']);
			if(isset($path_parts['basename']))
			{
				if(strpos($path_parts['dirname'], 'showfull'))
				{
					$redirectAddress .= '/showfull/'.$path_parts['basename'];
				}
				else
				{
					$redirectAddress .= '/index/'.$path_parts['basename'];
				}
			}
		}

		if(!$offerid = $this->uri->segment(3))
		{
			redirect($redirectAddress);
		}
		else
		{
			$this->_changeSpamStatus($offerid);
			redirect($redirectAddress);
		}
	}
	
	// --------------------------------------------------------------------	

	function _changeStatus($offerid)
	{
		if($this->OfferModel->getStatus($offerid))
		{
			$this->OfferModel->setStatus($offerid, 0);
		}
		else
		{
			$this->OfferModel->setStatus($offerid, 1);
		}
	}
	
	// --------------------------------------------------------------------
	
	function _changeSpamStatus($offerid)
	{
		if($this->OfferModel->getSpamStatus($offerid))
		{
			$this->OfferModel->setSpamStatus($offerid, 0);
		}
		else
		{
			$this->OfferModel->setSpamStatus($offerid, 1);
		}
	}
	
	// --------------------------------------------------------------------

	function decode_html(&$input)
	{
		$input = html_entity_decode($input, ENT_QUOTES, 'UTF-8');
		return $input;
	}

	// --------------------------------------------------------------------
	
	function _uploadFile($config, $field_name)
	{
		$this->load->library('upload', $config);
		if(!$this->upload->do_upload($field_name))
		{
			$warnings = $this->upload->display_errors('', '###');
			$aErrors = explode("###", substr(trim($warnings), 0, -3));
			return array(false, $aErrors);
		}
		else
		{
			$uploadData=$this->upload->data();
			$fileName=$uploadData['file_name'];
			return array(true, array($uploadData['file_name'], $uploadData['full_path']));
		}
	}
	
	// --------------------------------------------------------------------
	
	function check_date($input)
	{
		$dateArray = explode('-', $input);
		if(isset($dateArray[0]) && isset($dateArray[1]) && isset($dateArray[2]))
		{
			return date('Y-m-d', mktime(0, 0, 0, $dateArray[1], $dateArray[2], $dateArray[0]));
		}
		else
		{
			//$this->form_validation->set_message('check_date', "<strong>Data: '".$input."' jest niepoprawna</strong>. Proszę wprowadzić datę w formacie 'YYYY-MM-DD'.");
			return false;
		}
	}
	
	// --------------------------------------------------------------------
	
	function _checkLastEdited($itemid)
	{
		if($lastEdited = $this->session->userdata('lastEditedOffer'))
		{
			if($lastEdited['id'] == $itemid)
			{
				$this->session->unset_userdata(array('lastEditedOffer' => ''));
			}
		}
	}	
	
	// --------------------------------------------------------------------
	
	function _getMessageTemplates()
	{
		$filter['customFilter'][0] = array(
								'field' => 'is_active',
								'value' => 1
			);
		
		$this->defaultLangId = $this->OptionsModel->getDefaultLanguageId();
		$this->ItemsModel->setDefaultLanguage($this->defaultLangId);	
		$result = $this->ItemsModel->getAllItems(
				6,
				10000000, 
				0, 
				'title', 
				'asc',
				-1,
				$filter
			);	
			
		return $result;	
	}
	
	// --------------------------------------------------------------------
	
	function _getPermissions()
	{
		$permissions = $this->ion_auth->get_permissions_for_group();
		$this->permissions = $permissions["parsedvalues"];
		if(!isset($this->permissions[0])) 
		{
			$this->permissions = array();
		}
		return $this->permissions;
	}
}