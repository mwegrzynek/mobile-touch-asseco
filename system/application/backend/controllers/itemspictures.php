<?php

	class itemspictures extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			
			$this->load->model('../../models/ItemsModel');
			$this->load->model('../../models/PicturesModel');
			$this->load->model('../../models/LanguageModel');
			$this->load->model('../../models/MetaDataModel');
			$this->load->model('../../models/OptionsModel');


			// Check item type by name
			$itemName = $this->uri->segment(2, 0);	
			if(!$itemName)
			{
				show_error('Error checking item type. Invalid URL');
			}

			// Get item congfiguration
			$this->config->load('items_config', FALSE, TRUE);
			$this->itemConfig = $this->config->item($itemName);
			//var_dump($this->itemConfig);	
			
			//for swf upload function
			$this->gphash = sha1('mohitmosd1314'.date('Y-m-d'));	
			//$this->gphash = '12345';	
			
			//permissions			
			if(isset($_POST['hash']) && ($_POST['hash'] !== $this->gphash))
			{				
				if(!$this->ion_auth->is_admin())
				{			
					if(!in_array($this->itemConfig['mainName'].'-2', $this->_getPermissions())) 
					{
						redirect('nopermission');
					}
				}
			}
			
			$this->mysmarty->assign('activeItem', $this->itemConfig['activeMenuItem']);

			//alt tags and picture title are done by meta tags system
			$this->MetaDataModel->setType(98);

			$this->mysmarty->assign('link', 'itemspictures/'.$this->itemConfig['mainName']);
			$this->mysmarty->assign('itemMainName', $this->itemConfig['mainName']);

			$this->defaultLangId = $this->OptionsModel->getDefaultLanguageId();

			$this->ItemsModel->setDefaultLanguage($this->defaultLangId);

			$this->secondMenu[0]=array(
								"name"   => "Show all pictures",
								"link"   => "itemspictures/".$this->itemConfig['mainName']
							);

			$this->secondMenu[1]=array(
								"name"   => "Add new picture",
								"link"   => "itemspictures/".$this->itemConfig['mainName']."/addnew"
							);
							
			$this->secondMenu[4]=array(
								"name"   => "Add more new pictures at one time",
								"link"   => "itemspictures/".$this->itemConfig['mainName']."/addmore"
							);

			if($lastEdited = $this->session->userdata($this->itemConfig['menuLastEditedVarName'].'Picture'))
			{
				$this->secondMenu[2]=array(
									"name"   => "Last edited: [".$lastEdited['name']."]",
									"link"   => "itemspictures/".$this->itemConfig['mainName']."/edit/".$lastEdited['id']
								);
			}	
			
			$this->load->library('getdatalib');				
		}

		function _chooseItem()
		{
		   if($this->input->post('itemid'))
			{
				$this->session->set_userdata('itemid', $this->input->post('itemid'));
			}
			elseif(is_numeric($this->uri->segment(4)))
			{
				$this->session->set_userdata('itemid', $this->uri->segment(4));
			}
			else
			{
				$this->session->unset_userdata('itemid');	
			}
			redirect('itemspictures/'.$this->itemConfig['mainName']);
		}

		function index()
		{
									
			log_message('info', 'Index wywolano'.current_url());
			$this->_checkMethodName();
			
			$this->mysmarty->assign('css', array(
													'uitheme/cupertino/jquery-ui-1.7.1.custom.css',
													'jquery.fancybox.css'
												));


			if($itemsCount = $this->ItemsModel->getCountItems($this->itemConfig['itemType']))
			{
				$this->itemsTree = $this->ItemsModel->getTree($this->itemConfig['itemType'], $this->itemConfig['indexItemsOrderBy'], $this->itemConfig['indexItemsOrder']);
			}
			else
			{
				$this->itemsTree = array();
			}
					
			$this->mysmarty->assign('itemsTree', $this->itemsTree);


			//$itemid = $this->session->userdata('itemid');
			$itemid = $this->session->userdata('itemid');
			$this->mysmarty->assign('itemId', $itemid);

			$picturesCount = 0;
			$result = array();

			if($itemid)
			{
				//parent Item title
				$itemName = $this->ItemsModel->getItemTitle($itemid, $this->defaultLangId);
				$this->mysmarty->assign('itemId', $itemid);
				$this->mysmarty->assign('itemName', $itemName);


				//get all pictures in parent gallery				
				if($picturesCount = $this->PicturesModel->getCountPictures($itemid))
				{					

					if($this->uri->segment(3))
					{
						$offset = $this->uri->segment(3);
						$this->mysmarty->assign('offset', $offset);
					}
					else
					{
						$offset = 0;
					}

					$this->load->library('pagination2');

					$this->paginationConfig['config']['base_url'] = base_url().'itemspictures/'.$this->itemConfig['mainName'].'/';
					$this->paginationConfig['config']['uri_segment'] = 3;
					$this->paginationConfig['config']['total_rows'] = $picturesCount;

					if($this->session->userdata('perPage'))
					{
						$this->paginationConfig['config']['per_page'] = $this->session->userdata('perPage');
					}
					else
					{
						$this->paginationConfig['config']['per_page'] = $this->paginationConfig['itemsPerPage'][0];
					}

					$this->pagination2->initialize($this->paginationConfig['config']);

					$this->mysmarty->assign('pagination', $this->pagination2->create_links());
					$this->mysmarty->assign('currentPerPage', $this->paginationConfig['config']['per_page']);
					$this->mysmarty->assign('perPageLink', 'itemspictures/setItemsPerPage');

					$result = $this->PicturesModel->getPictures($this->paginationConfig['config']['per_page'], $offset, 'position asc, file_name asc', $itemid);
					
					if(isset($this->itemConfig['isRestrictedContent']) && $this->itemConfig['isRestrictedContent']) 
					{
						$this->mysmarty->assign('isRestrictedContent', 1);
					}
					
				}
				else
				{
					$result = array();
				}
			}

			$this->mysmarty->assign('picturePath', $this->itemConfig['galleryPicturesPath']);
			$this->mysmarty->assign('allPicturesCount', $picturesCount);
			$this->mysmarty->assign('picturesList', $result);
			$this->mysmarty->assign('secondmenu', $this->secondMenu);
			$this->mysmarty->assign('activeSecondItem', 0);
			$this->mysmarty->display('itemspictures/showPictures.tpl');
		}

		function _setItemsPerPage()
		{
			if(!$items = $this->uri->segment(3))
			{
				redirect('itemspictures');
			}
			else
			{
				$this->session->set_userdata('perPage', $items);
				redirect('itemspictures');
			}
		}


		function _addnew()
		{
			$itemid = $this->session->userdata('itemid');
			$this->mysmarty->assign('itemId', $itemid);

			if(!$itemid)
			{
				redirect('itemspictures/'.$this->itemConfig['mainName']);
			}

			$itemName = $this->ItemsModel->getItemTitle($itemid, $this->defaultLangId);
			$this->mysmarty->assign('itemId', $itemid);
			$this->mysmarty->assign('itemName', $itemName);


			//set default
			$aBackData['position'] = 1;

			//get default lang name
			$result = $this->LanguageModel->getLanguage($this->defaultLangId);

			$this->mysmarty->assign('header', 'Add new picture to <em>'.$itemName.'</em>');
			//$this->mysmarty->assign('subheader', 'Najpierw dodaj w domyślnym języku: [<em>'.$result[0]->name.'</em>]. Potem edytuj w innych językach.');


			//set second menu
			$this->mysmarty->assign('secondmenu', $this->secondMenu);
			$this->mysmarty->assign('activeSecondItem', 1);

			//set validation rules
			$config = array(
				array(
						'field'   => 'position',
						'label'   => 'Position',
						'rules'   => 'trim|required|numeric'
					)
			);

			$this->form_validation->set_rules($config);

			if($this->form_validation->run())
			{
				$aBackData['picture'] = '';
				$aBackData['picture_thumb'] = '';
				$files['thefile'] = array($aBackData['picture'], $aBackData['picture_thumb']);

				//var_dump($_FILES);
				$errors = array();
				//file save
				$config['upload_path']     = $this->itemConfig['galleryPicturesPath'];
				$config['allowed_types']   = 'jpg';
				$config['max_size']	      = '1512000';
				$config['overwrite']	      = FALSE;
				//$config['max_width']       = '600';
				//$config['max_height']      = '600';
				$config['remove_spaces']   = TRUE;

				$ifResize['image_width'] 	= 1024;
				//$ifResize = false;
				//$ifResize['image_height'] 	= 80;
				$uploadResult = $this->_uploadFile($config, $ifResize);

				if($uploadResult[0])
				{
					//get uploaded file path
					$fileName = $uploadResult[1][0];
					
					$thumbsConfig['file_name'] = $fileName;
					$thumbsConfig['upload_path'] = $this->itemConfig['galleryPicturesPath'];
					$thumbsConfig['thumbs_info'] = $this->itemConfig['galleryPicturesThumbsInfo'];
					$this->load->library('picturesactionslib');
					$this->picturesactionslib->makeThumbs($thumbsConfig);
				}
				else
				{
					$errors = $uploadResult[1];
				}
				

				if(!isset($errors[0]))
				{
					$pictureid = $this->PicturesModel->addPicture
					(
						array(
								'position'     => $this->input->post('position'),
								'file_name'    => $fileName,
								'item_id'     	=> $itemid
							)
					);

					if($pictureid)
					{
						$metaData = $this->getdatalib->fillTableDataWithPost($this->MetaDataModel->getMetaInfoTableFields($except = array('id', 'meta_data_id', 'language_id')), $sub = 'meta');						
						$languages = $this->LanguageModel->getAllLanguages();		
						foreach($languages as $key => $value) 
						{							
							$this->MetaDataModel->addMetaData($pictureid, $value->id, $metaData);
						}

						$this->mysmarty->assign('header', 'Data has been successfully saved.');						
					}
				}
				else
				{
					$this->mysmarty->assign('errorHeader', 'Picture has not been saved.');
					$this->mysmarty->assign('errors', $errors);
				}
			}
			elseif(validation_errors() != '') // Validation Failed
			{
				$this->mysmarty->assign('errorHeader', 'Picture has not been saved.');

				//prepare for smarty as array
				$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));

				if(isset($aErrors[0]) && $aErrors[0] != '')
				{
					$this->mysmarty->assign('errors', $aErrors);
				}
			}

			//there were errors
			if(isset($_POST['position']) && !isset($add))
			{
				$aBackData['position'] = $this->input->post('position');
				$meta = $this->input->post('meta');
				$aBackData['metaInfo'] = $meta;
			}

			$this->mysmarty->assign('picturePath', $this->itemConfig['galleryPicturesPath']);
			$this->mysmarty->assign('backData', $aBackData);
			$this->mysmarty->display('itemspictures/pictureAddAction.tpl');
		}

		/* EDIT */
		function _edit()
		{
			$this->mysmarty->assign('css', array(
													'jquery.fancybox.css',
												));

			if(!$pictureid = $this->uri->segment(4))
			{
				redirect('itemspictures/'.$this->itemConfig['mainName']);
			}
			else
			{
				if(!$this->PicturesModel->checkPicture($pictureid))
				{
					redirect('itemspictures/'.$this->itemConfig['mainName']);
					exit();
				}
				else
				{
					$this->pictureid = $pictureid;
				}

				if($itemsCount = $this->ItemsModel->getCountItems($this->itemConfig['itemType']))
				{
					$this->itemsTree = $this->ItemsModel->getTree($this->itemConfig['itemType'], $this->itemConfig['indexItemsOrderBy'], $this->itemConfig['indexItemsOrder']);
				}
				else
				{
					$this->itemsTree = array();
				}

				$this->mysmarty->assign('itemsTree', $this->itemsTree);

				//get simple info
				$simpleInfo = $this->PicturesModel->getPicture($pictureid);


				$pictureName = $simpleInfo[0]->file_name;
				$aBackData['picture'] 			= $pictureName;
				$aBackData['pictureid'] 		= $this->pictureid;
				$aBackData['picture_thumb']	= $this->PicturesModel->makeThumbName($pictureName);
				$aBackData['position'] 			= $simpleInfo[0]->position;				

				$itemid = $this->session->userdata('itemid');
				
				if(!isset($_POST['langid']) || !isset($_POST['position']))
				{					
					$itemid = $simpleInfo[0]->item_id;					
				}				
			
				$this->mysmarty->assign('itemId', $itemid);

				if(!$itemid)
				{
					redirect('itemspictures/'.$this->itemConfig['mainName']);
				}

				$itemName = $this->ItemsModel->getItemTitle($itemid, $this->defaultLangId);
				$this->mysmarty->assign('itemId', $itemid);
				$this->mysmarty->assign('itemName', $itemName);

				//set header
				if($this->session->flashdata('changed'))
				{
					$header = 'Data has been successfully saved. Picture has been moved to gallery: <em>'.$itemName.'</em>';
				}
				else
				{
					$header = 'Edit picture <em>'.$pictureName.'</em> in gallery <em>'.$itemName.'</em>';
				}

				$this->mysmarty->assign('header', $header);


				//set second menu edit item
				$this->secondMenu[2]=array(
								"name"   => "Edit: [".$pictureName."]",
								"link"   => "itemspictures/".$this->itemConfig['mainName']."/edit/".$pictureid
							);

				$lastEdited = array(
								"name"   => $pictureName,
								"id"     => $pictureid
							);

				$this->session->set_userdata($this->itemConfig['menuLastEditedVarName'].'Picture', $lastEdited);

				$this->mysmarty->assign('secondmenu', $this->secondMenu);
				$this->mysmarty->assign('activeSecondItem', 2);

				//get data for all languages
				$aBackData['languages'] = $this->_getAllData($pictureid);

				if(isset($_POST['langid']))
				{
					$langid = $_POST['langid'];

					$config = array(
						array(
							'field'   => 'metaTitle',
							'label'   => '',
							'rules'   => 'trim|strip_tags'
						),
						array(
							'field'   => 'metaKeywords',
							'label'   => '',
							'rules'   => 'trim|strip_tags'
						),
						array(
							'field'   => 'metaDescription',
							'label'   => '',
							'rules'   => 'trim|strip_tags'
						)
					);
					$this->form_validation->set_rules($config);

					if($this->form_validation->run())
					{
						$metaData = $this->getdatalib->fillTableDataWithPost($this->MetaDataModel->getMetaInfoTableFields($except = array('id', 'meta_data_id', 'language_id')), $sub = 'meta');
						$this->MetaDataModel->addMetaData($this->pictureid, $langid, $metaData);

						///submit and edit next
						if($this->input->post('submit-and-next'))
						{
							$nextLangCode = $aBackData['languages'][$langid]['next_lang_code'];
							redirect("itemspictures/".$this->itemConfig['mainName'].'/edit/'.$this->pictureid.'#'.$nextLangCode);
						}

						$this->mysmarty->assign('header', 'Dane zostały poprawnie zapisane.');
					}
					else
					{
						$this->mysmarty->assign('errorHeader', array($langid => 'Dane nie zostały poprawnie zapisane.'));

						//prepare for smarty as array
						$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));
						if(isset($aErrors[0]) && $aErrors[0]!='')
						{
							$aSubErrors[$langid] = $aErrors;
							$this->mysmarty->assign('subErrors', $aSubErrors);
						}
					}

					$meta = $this->input->post('meta');
					$aBackData['languages'][$langid]['metaInfo'] = $meta;
				}
				elseif(isset($_POST['position']))
				{
					$config = array(
						array(
							'field'   => 'position',
							'label'   => 'Position',
							'rules'   => 'trim|required|numeric'
						)
					);
					$this->form_validation->set_rules($config);

					if($this->form_validation->run())
					{
						if(isset($_FILES['thefile']['name']) && $_FILES['thefile']['name'] != '')
						{
							$files['thefile'] = array($aBackData['picture'], $aBackData['picture_thumb']);
							$errors = array();

							//file save
							$config['upload_path']     = $this->itemConfig['galleryPicturesPath'];
							$config['allowed_types']   = 'jpg';
							$config['max_size']	      = '1512000';
							$config['overwrite']	      = FALSE;
							//$config['max_width']       = '600';
							//$config['max_height']      = '600';
							$config['remove_spaces']   = TRUE;

							$ifResize['image_width'] 	= 1024;
							//$ifResize['image_height'] 	= 80;
							$uploadResult = $this->_uploadFile($config, $ifResize);

							if($uploadResult[0])
							{
								//get uploaded file path
								$fileName = $uploadResult[1][0];
								$thumbFileName = $this->PicturesModel->makeThumbName($fileName);

								$thumbsConfig['file_name'] = $fileName;
								$thumbsConfig['upload_path'] = $this->itemConfig['galleryPicturesPath'];
								$thumbsConfig['thumbs_info'] = $this->itemConfig['galleryPicturesThumbsInfo'];
								$thumbsConfig['old_file_name'] = $files['thefile'][0];
							
								$this->load->library('picturesactionslib');
								$this->picturesactionslib->makeThumbs($thumbsConfig);
								$this->picturesactionslib->removeThumbs($thumbsConfig);
								
								$aBackData['picture'] = $fileName;
								$aBackData['picture_thumb'] = $thumbFileName;
							}
							else
							{
								$errors = $uploadResult[1];
							}
						}

						if(!isset($errors[0]))
						{
							$data = array(
											'id'			=> $pictureid,
											'position'	=> $this->input->post('position'),
											'item_id'	=> $this->input->post('itemid')
									);

							if(isset($fileName))
							{
								$data['file_name'] = $fileName;
								$lastEdited['name'] =  $fileName;
							}

							$change = $this->PicturesModel->changePicture($data);
							$this->mysmarty->assign('header', 'Data has been changed.');

							if($itemid != $this->input->post('itemid'))
							{
								$this->session->set_userdata('itemid', $this->input->post('itemid'));
								$this->session->set_flashdata('changed', 1);
								redirect('itemspictures/'.$this->itemConfig['mainName'].'/edit/'.$this->pictureid);
							}
						}
						else
						{
							$this->mysmarty->assign('errors', $errors);
							$this->mysmarty->assign('errorHeader', 'Data has not been changed.');
						}
					}
					else
					{
						$this->mysmarty->assign('errorHeader', 'Data has not been changed.');

						//prepare for smarty as array
						$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));
						if(isset($aErrors[0]) && $aErrors[0]!='')
						{
							$this->mysmarty->assign('errors', $aErrors);
						}
					}
					$aBackData['position'] = $this->input->post('position');
					$aBackData['item_id'] = $this->input->post('itemid');
				}


				$this->mysmarty->assign('picturePath', $this->itemConfig['galleryPicturesPath']);
				$this->mysmarty->assign('backData', $aBackData);
				$this->mysmarty->display('itemspictures/pictureEditAction.tpl');
			}
		}

		function _removeImage()
		{
			if(!$pictureid = $this->uri->segment(4))
			{
				redirect('itemspictures/'.$this->itemConfig['mainName']);
			}
			else
			{
				if($pictureInfo = $this->PicturesModel->checkPicture($pictureid))
				{
					//$this->PicturesModel->deletePicture($pictureid, $this->itemConfig['galleryPicturesPath'].'/');
					$this->PicturesModel->removePicture($pictureid);
					
					$thumbsConfig['file_name'] = $pictureInfo->file_name;
					$thumbsConfig['upload_path'] = $this->itemConfig['galleryPicturesPath'];
					$thumbsConfig['thumbs_info'] = $this->itemConfig['galleryPicturesThumbsInfo'];
					$thumbsConfig['old_file_name'] = $pictureInfo->file_name;
				
					$this->load->library('picturesactionslib');
					//$this->picturesactionslib->makeThumbs($thumbsConfig);
					$this->picturesactionslib->removeThumbs($thumbsConfig);
					
					$this->MetaDataModel->removeMetaData($pictureid);
					$this->_checkLastEdited($pictureid);
				}
				redirect('itemspictures/'.$this->itemConfig['mainName']);
			}
		}
		
		function _checkLastEdited($pictureid)
		{
			if($lastEdited = $this->session->userdata($this->itemConfig['menuLastEditedVarName'].'Picture'))
			{
				if($lastEdited['id'] == $pictureid)
				{
					$this->session->unset_userdata(array($this->itemConfig['menuLastEditedVarName'].'Picture' => ''));
				}
			}
		}

		function _getAllData($pictureid)
		{
			$languages = $this->LanguageModel->getAllLanguages();

			//info for default language
			$metaDefaultInfo = $this->MetaDataModel->getMetaData($pictureid, $this->defaultLangId);


			if(!count($metaDefaultInfo))
			{
				$metaDefaultInfo[0]->title = '';
				$metaDefaultInfo[0]->keywords = '';
				$metaDefaultInfo[0]->description = '';
			}

			foreach($languages as $key => $lang)
			{
				if($this->defaultLangId == $lang->id)
				{
					$metaInfo = $metaDefaultInfo;
				}
				else
				{
					$metaInfo = $this->MetaDataModel->getMetaData($pictureid, $lang->id);
					if(!count($metaInfo))
					{
						$metaInfo = $metaDefaultInfo;
					}
				}

				if(isset($languages[$key+1]))
				{
					$nextItem = $languages[$key+1];
				}
				else
				{
					$nextItem = $languages[0];
				}

				$backData[$lang->id] = array(
										'id'        => $lang->id,
										'lang_code' => $lang->lang_code,
										'name'      => $lang->name,

										'next_lang_code' => $nextItem->lang_code,

										'metaInfo'     => array(
																		'title'        => $metaInfo[0]->title,
																		'keywords'     => $metaInfo[0]->keywords,
																		'description'  => $metaInfo[0]->description
																	)
									);
			}
			return $backData;
		}


		function _uploadFile($config, $ifResize = null, $field_name = 'thefile')
		{
			$this->load->library('upload', $config);
			

			if(!$this->upload->do_upload($field_name))
			{
				$warnings = $this->upload->display_errors('', '###');
				$aErrors = explode("###", substr(trim($warnings), 0, -3));
				return array(false, $aErrors);
			}
			else
			{
				$uploadData=$this->upload->data();
				$fileName=$uploadData['file_name'];

				$ifResize = false;
				if(is_array($ifResize))
				{
					if($uploadData['image_width'] > $ifResize['image_width']/* || $uploadData['image_height'] > $ifResize['image_height']*/)
					{						
						$config['image_library']   = 'GD2';
						$config['source_image']    = $uploadData['full_path'];						
						$config['master_dim']  		= 'width';
						$config['maintain_ratio']  = TRUE;
						$config['width']           = $ifResize['image_width'];
						$config['height']          = 400;

						$this->load->library('image_lib', $config);

						if(!$this->image_lib->resize())
						{
							$warnings = $this->image_lib->display_errors('', '###');
							$aErrors = explode("###", substr(trim($warnings), 0, -3));
							return array(false, $aErrors);
						}
					}
				}
				return array(true, array($uploadData['file_name'], $uploadData['full_path']));
			}
		}
		
		
		function _addmore()
		{
			$this->mysmarty->assign('css', array(
													'swfupload.css',
												));
			
			
			$this->mysmarty->assign('scripts', array(
													'swfupload/swfupload.queue.js',
													'swfupload/fileprogress.js',
													'swfupload/handlers.js'
												));


			$this->mysmarty->assign('outerscripts', array(
															array(
																'name' => 'swfupload.js',
																'src'  => 'swfupload'
															)
													));
			
			$itemid = $this->session->userdata('itemid');
			$this->mysmarty->assign('itemId', $itemid);

			if(!$itemid)
			{
				redirect('itemspictures/'.$this->itemConfig['mainName']);
			}

			$itemName = $this->ItemsModel->getItemTitle($itemid, $this->defaultLangId);
			$this->mysmarty->assign('itemId', $itemid);
			$this->mysmarty->assign('itemName', $itemName);
			
			//set default
			$aBackData['position'] = 1;

			//get default lang name
			$result = $this->LanguageModel->getLanguage($this->defaultLangId);

			$this->mysmarty->assign('header', 'Add new pictures to <em>'.$itemName.'</em>');
			$this->mysmarty->assign('subheader', 'Add more pictures at one time.');


			//set second menu
			$this->mysmarty->assign('hash', $this->gphash);
			$this->mysmarty->assign('itemid', $itemid);
			$this->mysmarty->assign('secondmenu', $this->secondMenu);
			$this->mysmarty->assign('activeSecondItem', 4);

			
			$this->mysmarty->display('itemspictures/pictureAddMoreAction.tpl');
		}
		
		function _uploadmore()
		{			
			log_message('info', 'Upload More wywolano');
			if($_POST['hash'] === $this->gphash)
			{				
				if(isset($_POST['itemid']))
				{
					$itemid = $_POST['itemid'];
					
					//file save
					$config['upload_path']     = $this->itemConfig['galleryPicturesPath'];
					$config['allowed_types']   = 'jpg';
					$config['max_size']	      = '1512000';
					$config['overwrite']	      = FALSE;
					//$config['max_width']       = '600';
					//$config['max_height']      = '600';
					$config['remove_spaces']   = TRUE;
	
					$ifResize['image_width'] 	= 1024;
					//$ifResize = false;
					//$ifResize['image_height'] 	= 80;
					$uploadResult = $this->_uploadFile($config, $ifResize, 'file');
	
					if($uploadResult[0])
					{
						//get uploaded file path
						$fileName = $uploadResult[1][0];
						
						$thumbsConfig['file_name'] = $fileName;
						$thumbsConfig['upload_path'] = $this->itemConfig['galleryPicturesPath'];
						$thumbsConfig['thumbs_info'] = $this->itemConfig['galleryPicturesThumbsInfo'];
						$this->load->library('picturesactionslib');
						$this->picturesactionslib->makeThumbs($thumbsConfig);
						
					}
					else
					{
						$errors = $uploadResult[1];
					}
						//var_dump($uploadResult);
	
					if(!isset($errors[0]))
					{
						$add = $this->PicturesModel->addPicture
						(
							array(
								'position'     => 1,
								'file_name'    => $fileName,
								'item_id'     	=> $itemid
							)
						);
						
						echo 0;
						return true;	
						
					}
					else
					{
						foreach ($errors as $key => $value) 
						{
							echo $value;
						}						
					}
					return false;
										
				}
			}			
			echo 'Bląd: brak poprawnego ciągu hash';			
		}
		
		
		function _checkMethodName()
		{
			$methodName = $this->uri->segment(3, 0);			
			if($methodName === 'addnew')
			{
				$this->_addnew();
				exit();				
			}
			elseif($methodName === 'edit')
			{
				$this->_edit();
				exit();				
			}
			elseif($methodName === 'setItemsPerPage')
			{
				$this->_setItemsPerPage();
				exit();				
			}
			elseif($methodName === 'removeImage')
			{
				$this->_removeImage();
				exit();				
			}
			elseif($methodName === 'chooseitem')
			{				
				$this->_chooseItem();
				exit();				
			}
			elseif($methodName === 'addmore')
			{				
				$this->_addmore();
				exit();				
			}
			elseif($methodName === 'uploadmore')
			{				
				$this->_uploadmore();
				exit();				
			}
			return true;				
		}
		
		function _getPermissions()
		{
			$permissions = $this->ion_auth->get_permissions_for_group();
			$this->permissions = $permissions["parsedvalues"];
			if(!isset($this->permissions[0])) 
			{
				$this->permissions = array();
			}
			return $this->permissions;
		}	
		
	}
