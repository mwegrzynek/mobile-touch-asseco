<?php

	class importdata extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();

			$this->load->model('../../models/ItemsModel');
			$this->load->model('../../models/PicturesModel');
			$this->load->model('../../models/VideosModel');
			$this->load->model('../../models/LanguageModel');
			$this->load->model('../../models/CategoryModel');
			$this->load->model('../../models/OptionsModel');
			$this->load->model('../../models/ServiceModel');
			$this->load->model('../../models/UserModel');
		}


		function index()
		{
			//$this->importTranslations();
		}


		function importTranslations()
		{
			$fileName = "../temp/translate_notifications_en.csv";
			$row = 1;
			if(($handle = fopen($fileName, "r")) !== FALSE)
			{
				//$csvdata = fgetcsv($handle, 1000, ",");

				while(($csvdata = fgetcsv($handle, 1000, ",")) !== FALSE)
				{
					$id = $csvdata[0];
					$content = $csvdata[2];

					if($id !== '')
					{
						$itemDefault = $this->ItemsModel->getItemById($id, 1);

						$data['title']       = $itemDefault[0]->title;
						$data['slug']        = $itemDefault[0]->slug;

						$data['id']          = $id;
						$data['content']     = $content;
						$data['language_id'] = 29;
						$data['is_active']   = $itemDefault[0]->is_active;

						echo $this->ItemsModel->changeItem($data);
						//echo $id.' '.$content."\n";
					}
				}
				fclose($handle);
			}
		}

		function addCategories()
		{
			/*$fileName = "../temp/categories.csv";
			$row = 1;
			if(($handle = fopen($fileName, "r")) !== FALSE)
			{
				while(($csvdata = fgetcsv($handle, 5000, ",")) !== FALSE)
				{
					$id = $csvdata[0];

					if($id !== '')
					{
						$data['id'] = $id;
						$data['title_pl'] = $csvdata[1];
						$data['title_dk'] = $csvdata[2];
						$data['title_en'] = $csvdata[3];

						echo $data['id'].' '.$data['title_pl'].' '.$data['title_dk'].' '.$data['title_en']."\n";


						//PL
						$categorySlug = $this->_makeSlug($data['title_pl'], 1);
						$add = $this->CategoryModel->addCategory
						(
							array(
								'type'    		=> 1,
								'parent_id'    => $id,
								'language_id'  => 1,
								'name'         => $data['title_pl'],
								'slug'         => $categorySlug,
								'position'     => 1,
								'file_name'    => '',
								'is_active'    => 1,
								'description'  => ''
								)
						);

						if($add)
						{
							//DK
							$categorySlug = $this->_makeSlug($data['title_dk'], 2);
							$change = $this->CategoryModel->changeCategory
							(
								array(
											'id'           => $add,
											'language_id'  => 2,
											'name'         => $data['title_dk'],
											'slug'         => $categorySlug,
											'position'     => 1,
											'is_active'    => 1,
											'description'  => ''
									)
							);


							//EN
							$categorySlug = $this->_makeSlug($data['title_en'], 29);
							$change = $this->CategoryModel->changeCategory
							(
								array(
											'id'           => $add,
											'language_id'  => 29,
											'name'         => $data['title_en'],
											'slug'         => $categorySlug,
											'position'     => 1,
											'is_active'    => 1,
											'description'  => ''
									)
							);
						}
					}
				}
				fclose($handle);
			}*/
		}

		function _makeSlug($title, $langId)
		{
			$this->load->helper('slug_helper');
			$slug = makeSlugs($title);
			$slug = $this->_checkSlug($slug, $langId);
			return $slug;
		}

		function _checkSlug($slug, $langId)
		{
			if($slug == '')
			{
				//get default slug
				$info = $this->CategoryModel->getCategoryInfo($this->categoryid, $this->defaultLangId);
				$slug = $info[0]->slug;
				if($slug == '')
				{
					$slug = 'please-write-slug-manually';
				}
			}

			$counter = 1;
			do
			{
				if($result = $this->CategoryModel->checkSlug($slug, $langId, 1))
				{
					$slug = $slug.$counter;
				}

				$counter++;
				if($counter > 100)
				{
					show_error('_checkSlug() loop error');
					break;
				}
			}
			while($result);
			return $slug;
		}
	}//end class