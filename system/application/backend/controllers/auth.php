<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}


class Auth extends Controller {

	function __construct()
	{
		parent::__construct();		
		$this->mysmarty->assign('activeItem', 20);
		$this->mysmarty->assign('isAdmin', $this->ion_auth->is_admin());
		
		
		if($this->ion_auth->is_admin()) 
		{
			
			$this->secondMenu[0]=array(
								"name"   => "Admin list",
								"link"   => "auth"
							);
	
			$this->secondMenu[1]=array(
								"name"   => "Add new admin",
								"link"   => "auth/addnew"
									);
		}

		if($lastEdited = $this->session->userdata('lastEditedAdmin'))
		{
			$this->secondMenu[2]=array(
								"name"   => "Last edited: [".$lastEdited['name']."]",
								"link"   => "auth/edit/".$lastEdited['id']
							);
		}
	}

	//redirect if needed, otherwise display the user list
	function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			//redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin())
		{
			//redirect them to the home page because they must be an administrator to view this
			$userInfo = $this->ion_auth->get_user();
			$userId = $userInfo->id;
			redirect('auth/edit/'.$userId, 'refresh');
		}
		else
		{
			if($this->session->flashdata('userDeleted')) 
			{
				$this->mysmarty->assign('subheader', 'Account has been deleted');
			} 

			$this->mysmarty->assign('css', array('uitheme/cupertino/jquery-ui-1.7.1.custom.css'));
			
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->data['users'] = $this->ion_auth->get_users_array();
			
						
			$this->mysmarty->assign('itemsList', $this->data['users']);
			
			
			$this->mysmarty->assign('secondmenu', $this->secondMenu);
			$this->mysmarty->assign('activeSecondItem', 0);
			$this->mysmarty->display('admin/showAdmins.tpl');
		}
	}
	
	
	
	
	function addnew()
	{
		if(!$this->ion_auth->is_admin())
		{			
			$userInfo = $this->ion_auth->get_user();
			$userId = $userInfo->id;
			redirect('auth/edit/'.$userId, 'refresh');
		}
		
		$this->mysmarty->assign('formLink', 'addnew');
		
		//set second menu
		$this->mysmarty->assign('secondmenu', $this->secondMenu);
		$this->mysmarty->assign('activeSecondItem', 1);
		
		$aBackData['group_id']	= 2;
		$aBackData['groups']		= $this->ion_auth->get_groups();
		
		$config = array(
				array(
						'field'   => 'username',
						'label'   => 'User name',
						'rules'   => 'trim|strip_tags|required|callback_check_username'
					),
				array(
						'field'   => 'email',
						'label'   => 'E-mail',
						'rules'   => 'trim|required|valid_email|callback_check_email'
					),		
				array(
						'field'   => 'first_name',
						'label'   => 'First name',
						'rules'   => 'trim|strip_tags|required'
					),
				array(
						'field'   => 'last_name',
						'label'   => 'Last name',
						'rules'   => 'trim|strip_tags|required'
					),
				array(
						'field'   => 'email',
						'label'   => 'E-mail',
						'rules'   => 'trim|required|valid_email|callback_check_email'
					),
				array(
						'field'   => 'new',
						'label'   => 'Password',
						'rules'   => 'trim|required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]'
					),   
				array(
						'field'   => 'new_confirm',
						'label'   => 'Confirm password',
						'rules'   => 'trim|required'
					)
			);

		$this->form_validation->set_rules($config);	

		//set header
		$this->mysmarty->assign('header', 'Add new admin.');


		if($this->form_validation->run())
		{
			$additional_data['first_name'] = $this->input->post('first_name');
			$additional_data['last_name'] = $this->input->post('last_name');
			
			$group = $this->ion_auth->get_group($this->input->post('group_id'));		
			$group_name = $group->name;
		
			$register = $this->ion_auth->register($this->input->post('username'), $this->input->post('new'), $this->input->post('email'), $additional_data, $group_name);			
			
			if($register)
			{
				$this->mysmarty->assign('header', 'Data has been successfully saved. Admin has been added.');
			}
			else
			{
				$this->mysmarty->assign('header', 'Data has not been saved.');
				$aErrors = array($this->ion_auth->errors());
				$this->mysmarty->assign('errors', $aErrors);
			}

			/*$aBackData['email']    		= $this->input->post('email');
			$aBackData['username'] 		= $this->input->post('username');
			$aBackData['first_name'] 	= $this->input->post('first_name');
			$aBackData['last_name'] 	= $this->input->post('last_name');*/
			$aBackData['group_id'] 		= $this->input->post('group_id');
		}
		elseif(validation_errors() != '') // Validation Failed
		{
			$this->mysmarty->assign('errorHeader', 'Data has not been saved.');

			//prepare for smarty as array
			$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));

			if(isset($aErrors[0]) && $aErrors[0]!='')
			{
				$this->mysmarty->assign('errors', $aErrors);
			}

			$aBackData['email']    		= $this->input->post('email');
			$aBackData['username'] 		= $this->input->post('username');
			$aBackData['first_name'] 	= $this->input->post('first_name');
			$aBackData['last_name'] 	= $this->input->post('last_name');
			$aBackData['group_id'] 		= $this->input->post('group_id');
		}
		
		$this->mysmarty->assign('backData', $aBackData);		
		$this->mysmarty->display('admin/addAction.tpl');		
	}
	
	
	
	
	
	function edit()
	{
		if(!$this->adminid = $this->uri->segment(3))
		{
			redirect('auth');
		}
		else
		{
			//get admin data
			$this->mysmarty->assign('formLink', 'edit/'.$this->adminid);
			$result = $this->ion_auth->get_user($this->adminid);
			
			$aBackData = get_object_vars($result);			
			$aBackData['groups']	= $this->ion_auth->get_groups();
			
			//var_dump($aBackData);
			
	
			$this->mysmarty->assign('header', 'Edit account <em>'.$aBackData['username'].'</em>');
			
			//set second menu edit item only for superuser
			$this->secondMenu[2]=array(
							"name"   => "Edit: [".$aBackData['username']."]",
							"link"   => "auth/edit/".$this->adminid
						);
	
			$lastEdited = array(
							"name"   => $aBackData['username'],
							"id"     => $this->adminid
						);
	
			$this->session->set_userdata('lastEditedAdmin', $lastEdited);
	
			$this->mysmarty->assign('secondmenu', $this->secondMenu);
			$this->mysmarty->assign('activeSecondItem', 2);
				
			
			if($this->input->post('change-data'))
			{
				$aBackData = $this->_editChangeData($aBackData);
			}
			elseif($this->input->post('change-pass'))
			{
				$this->_editChangePassword();				
			}
	
			$lastEdited = array(
							"name"   => $aBackData['username'],
							"id"     => $this->adminid
						);
			$this->session->set_userdata('lastEditedAdmin', $lastEdited);
	
			$this->mysmarty->assign('backData', $aBackData);
			$this->mysmarty->display('admin/editAction.tpl');
		}
	}
	
	
	function remove()
	{
		if(!$this->ion_auth->is_admin())
		{			
			$userInfo = $this->ion_auth->get_user();
			$userId = $userInfo->id;
			redirect('auth/edit/'.$userId, 'refresh');
		}
		
		
		
		if(!$adminid = $this->uri->segment(3))
		{
			redirect('auth');
		}
		else
		{
			$userData = $this->session->userdata('userData');
			//var_dump($adminid);
			//var_dump($userData['userid']);
			
			if($userData['userid'] != $adminid)
			{
				
				if($result = $this->ion_auth->delete_user($adminid)) 
				{
					$this->session->set_flashdata('userDeleted', 1);
				}
			}
			redirect('auth');
		}				
	}
	
	
	function _editChangeData($aBackData)
	{
		if($this->input->post('email') != $aBackData['email'])
		{					
			$this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email|callback_check_email');
		}

		if($this->input->post('username') != $aBackData['username'])
		{					
			$this->form_validation->set_rules('username', 'User name', 'trim|strip_tags|required|callback_check_username');
		}
		
		$this->form_validation->set_rules('first_name', 'First name', 'trim|required');
		$this->form_validation->set_rules('last_name', 'Last name', 'trim|required');
		
		if($this->form_validation->run()) // Validation Passed
		{
			//update_user($id, $data)
			$data = array(
				'email' 			=> $this->input->post('email'),
				'username' 		=> $this->input->post('username'),
				'first_name' 	=> $this->input->post('first_name'),
				'last_name'	 	=> $this->input->post('last_name')				
			);
						
			if($this->ion_auth->is_admin())
			{
				$data['group_id']	= $this->input->post('group_id');
			}
			
			$change = $this->ion_auth->update_user($this->adminid, $data);

			if(!$change)
			{
				$aErrors = array($this->ion_auth->errors());
				$this->mysmarty->assign('errors', $aErrors);
			}
			else
			{
				$this->mysmarty->assign('header', 'Data has been successfully saved.');
			}
			
			$aBackData['email']    		= $this->input->post('email');
			$aBackData['username'] 		= $this->input->post('username');
			$aBackData['first_name'] 	= $this->input->post('first_name');
			$aBackData['last_name'] 	= $this->input->post('last_name');
			$aBackData['group_id'] 		= $this->input->post('group_id');
		}
		elseif(validation_errors() != '') // Validation Failed
		{
			$this->mysmarty->assign('errorHeader', 'Data has not been saved.');

			//prepare for smarty as array
			$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));

			if(isset($aErrors[0]) && $aErrors[0]!='')
			{
				$this->mysmarty->assign('errors', $aErrors);
			}
			$aBackData['email']    		= $this->input->post('email');
			$aBackData['username'] 		= $this->input->post('username');
			$aBackData['first_name'] 	= $this->input->post('first_name');
			$aBackData['last_name'] 	= $this->input->post('last_name');
			$aBackData['group_id'] 		= $this->input->post('group_id');
		}
		
		return $aBackData;
	}
	
	
	
	
	function _editChangePassword()
	{
		//$this->form_validation->set_rules('old', 'Stare hasło', 'required');
		$this->form_validation->set_rules('new', 'New password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
		$this->form_validation->set_rules('new_confirm', 'Confirm new password', 'required');

		$user = $this->ion_auth->get_user($this->adminid);
		$identity_key = $this->config->item('identity', 'ion_auth');
		$identity = $user->$identity_key;
		
		if($this->form_validation->run()) // Validation Passed
		{
			$change = $this->ion_auth->change_password_as_admin($identity, $this->input->post('new'));

			if(!$change)
			{ 
				$this->mysmarty->assign('errorHeader', 'Password has not been changed.');
				$aErrors = array($this->ion_auth->errors());
				$this->mysmarty->assign('errors', $aErrors);
			}
			else
			{
				$this->mysmarty->assign('header', 'Password has been changed.');				
				//$this->logout();
			}
		}
		elseif(validation_errors() != '') // Validation Failed
		{
			$this->mysmarty->assign('errorHeader', 'Password has not been changed.');

			//prepare for smarty as array
			$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));

			if(isset($aErrors[0]) && $aErrors[0]!='')
			{
				$this->mysmarty->assign('errors', $aErrors);
			}			
		}
	}
	

	//log the user in
	function login()
	{
		$this->data['title'] = "Login";

		//validate form input
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == true)
		{ //check to see if the user is logging in
			//check for "remember me"
			$remember = (bool) $this->input->post('remember');

			if ($this->ion_auth->login($this->input->post('email'), $this->input->post('password'), $remember))
			{ //if the login is successful
				//redirect them back to the home page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect($this->config->item('base_url'), 'refresh');
			}
			else
			{ //if the login was un-successful
				//redirect them back to the login page
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('auth/login', 'refresh'); //use redirects instead of loading views for compatibility with MY_Controller libraries
			}
		}
		else
		{  //the user is not logging in so display the login page
			//set the flash data error message if there is one
			$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));	
			$message = $this->session->flashdata('message');


			if(isset($aErrors[0]) && $aErrors[0]!='')
			{				
				$this->mysmarty->assign('errors', $aErrors);
			}
			elseif($message)
			{
				$this->mysmarty->assign('errors', array($message));
			}

			$backData['email'] = $this->form_validation->set_value('email');

			//$this->load->view('auth/login', $this->data);
			$this->mysmarty->display('login.tpl');
		}
	}

	//log the user out
	function logout()
	{
		$this->data['title'] = "Logout";

		//log the user out
		$logout = $this->ion_auth->logout();

		//redirect them back to the page they came from
		redirect('auth', 'refresh');
	}

	//change password
	function change_password()
	{
		$this->form_validation->set_rules('old', 'Old password', 'required');
		$this->form_validation->set_rules('new', 'New Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
		$this->form_validation->set_rules('new_confirm', 'Confirm New Password', 'required');

		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login', 'refresh');
		}
		$user = $this->ion_auth->get_user($this->session->userdata('user_id'));

		if ($this->form_validation->run() == false)
		{ //display the form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['old_password'] = array('name' => 'old',
				'id' => 'old',
				'type' => 'password',
			);
			$this->data['new_password'] = array('name' => 'new',
				'id' => 'new',
				'type' => 'password',
			);
			$this->data['new_password_confirm'] = array('name' => 'new_confirm',
				'id' => 'new_confirm',
				'type' => 'password',
			);
			$this->data['user_id'] = array('name' => 'user_id',
				'id' => 'user_id',
				'type' => 'hidden',
				'value' => $user->id,
			);

			//render
			$this->load->view('auth/change_password', $this->data);
		}
		else
		{
			$identity = $this->session->userdata($this->config->item('identity', 'ion_auth'));

			$change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

			if ($change)
			{ //if the password was successfully changed
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				$this->logout();
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('auth/change_password', 'refresh');
			}
		}
	}

	//forgot password
	function forgot_password()
	{
	
		$this->form_validation->set_rules('email', 'Adres email', 'required');
		if ($this->form_validation->run() == false)
		{
			//setup the input
			$this->data['email'] = array('name' => 'email',
				'id' => 'email',
			);
			//set any errors and display the form
			$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));	
			$message = $this->session->flashdata('message');

			if(isset($aErrors[0]) && $aErrors[0]!='')
			{				
				$this->mysmarty->assign('errors1', $aErrors);
			}
			elseif($message)
			{
				$this->mysmarty->assign('errors1', array($message));
			}
			
			$this->mysmarty->assign('forgottenPass', 1);			
			$this->mysmarty->display('login.tpl');
		}
		else
		{
			
			//run the forgotten password method to email an activation code to the user
			$forgotten = $this->ion_auth->forgotten_password($this->input->post('email'));

			if ($forgotten)
			{ //if there were no errors
				$this->session->set_flashdata('message', $this->ion_auth->messages());				
				redirect("auth/login", 'refresh'); //we should display a confirmation page here instead of the login page
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect("auth/forgot_password", 'refresh');
			}
		}
	}

	//reset password - final step for forgotten password
	public function reset_password($code)
	{
		$reset = $this->ion_auth->forgotten_password_complete($code);

		if ($reset)
		{  //if the reset worked then send them to the login page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("auth/login", 'refresh');
		}
		else
		{ //if the reset didnt work then send them back to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("auth/forgot_password", 'refresh');
		}
	}

	//activate the user
	function activate($id, $code=false)
	{
		if ($code !== false)
			$activation = $this->ion_auth->activate($id, $code);
		else if ($this->ion_auth->is_admin())
			$activation = $this->ion_auth->activate($id);


		if ($activation)
		{
			//redirect them to the auth page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("auth", 'refresh');
		}
		else
		{
			//redirect them to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("auth/forgot_password", 'refresh');
		}
	}

	//deactivate the user
	function deactivate($id = NULL)
	{
		// no funny business, force to integer
		$id = (int) $id;

		$this->load->library('form_validation');
		$this->form_validation->set_rules('confirm', 'confirmation', 'required');
		$this->form_validation->set_rules('id', 'user ID', 'required|is_natural');

		if ($this->form_validation->run() == FALSE)
		{
			// insert csrf check
			$this->data['csrf'] = $this->_get_csrf_nonce();
			$this->data['user'] = $this->ion_auth->get_user($id);
			$this->load->view('auth/deactivate_user', $this->data);
		}
		else
		{
			// do we really want to deactivate?
			if ($this->input->post('confirm') == 'yes')
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id'))
				{
					show_404();
				}

				// do we have the right userlevel?
				if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin())
				{
					$this->ion_auth->deactivate($id);
				}
			}

			//redirect them back to the auth page
			redirect('auth', 'refresh');
		}
	}

	//create a new user
	function create_user()
	{
		$this->data['title'] = "Create User";

		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('auth', 'refresh');
		}

		//validate form input
		$this->form_validation->set_rules('first_name', 'First Name', 'required|xss_clean');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required|xss_clean');
		$this->form_validation->set_rules('email', 'Email Address', 'required|valid_email');
		$this->form_validation->set_rules('phone1', 'First Part of Phone', 'required|xss_clean|min_length[3]|max_length[3]');
		$this->form_validation->set_rules('phone2', 'Second Part of Phone', 'required|xss_clean|min_length[3]|max_length[3]');
		$this->form_validation->set_rules('phone3', 'Third Part of Phone', 'required|xss_clean|min_length[4]|max_length[4]');
		$this->form_validation->set_rules('company', 'Company Name', 'required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
		$this->form_validation->set_rules('password_confirm', 'Password Confirmation', 'required');

		if ($this->form_validation->run() == true)
		{
			$username = strtolower($this->input->post('first_name')) . ' ' . strtolower($this->input->post('last_name'));
			$email = $this->input->post('email');
			$password = $this->input->post('password');

			$additional_data = array('first_name' => $this->input->post('first_name'),
				'last_name' => $this->input->post('last_name'),
				'company' => $this->input->post('company'),
				'phone' => $this->input->post('phone1') . '-' . $this->input->post('phone2') . '-' . $this->input->post('phone3'),
			);
		}
		if ($this->form_validation->run() == true && $this->ion_auth->register($username, $password, $email, $additional_data))
		{ //check to see if we are creating the user
			//redirect them back to the admin page
			$this->session->set_flashdata('message', "User Created");
			redirect("auth", 'refresh');
		}
		else
		{ //display the create user form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$this->data['first_name'] = array('name' => 'first_name',
				'id' => 'first_name',
				'type' => 'text',
				'value' => $this->form_validation->set_value('first_name'),
			);
			$this->data['last_name'] = array('name' => 'last_name',
				'id' => 'last_name',
				'type' => 'text',
				'value' => $this->form_validation->set_value('last_name'),
			);
			$this->data['email'] = array('name' => 'email',
				'id' => 'email',
				'type' => 'text',
				'value' => $this->form_validation->set_value('email'),
			);
			$this->data['company'] = array('name' => 'company',
				'id' => 'company',
				'type' => 'text',
				'value' => $this->form_validation->set_value('company'),
			);
			$this->data['phone1'] = array('name' => 'phone1',
				'id' => 'phone1',
				'type' => 'text',
				'value' => $this->form_validation->set_value('phone1'),
			);
			$this->data['phone2'] = array('name' => 'phone2',
				'id' => 'phone2',
				'type' => 'text',
				'value' => $this->form_validation->set_value('phone2'),
			);
			$this->data['phone3'] = array('name' => 'phone3',
				'id' => 'phone3',
				'type' => 'text',
				'value' => $this->form_validation->set_value('phone3'),
			);
			$this->data['password'] = array('name' => 'password',
				'id' => 'password',
				'type' => 'password',
				'value' => $this->form_validation->set_value('password'),
			);
			$this->data['password_confirm'] = array('name' => 'password_confirm',
				'id' => 'password_confirm',
				'type' => 'password',
				'value' => $this->form_validation->set_value('password_confirm'),
			);
			$this->load->view('auth/create_user', $this->data);
		}
	}

	function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}

	function _valid_csrf_nonce()
	{
		if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
				$this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	/**
	* check_username
	*
	* @return bool
	* @author Mathew Davies
	**/

	function check_username($username)
	{
		if($this->ion_auth->username_check($username))
		{
			$this->form_validation->set_message('check_username', 'User name: '.$username.' is not available.');
			return false;
		}
		else
		{
			return true;
		}
	}

	/**
	* check_email
	*
	* @return bool
	* @author Mathew Davies
	**/

	function check_email($email)
	{
		if ($this->ion_auth->email_check($email))
		{
			$this->form_validation->set_message('check_email', 'E-mail: ' . $email . ' is not available.');
			return false;
		}
		else
		{
			return true;
		}
	}

}
