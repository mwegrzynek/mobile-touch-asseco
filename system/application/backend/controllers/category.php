<?php

	class category extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();
			
			//$this->output->enable_profiler(TRUE);
			
			$this->mysmarty->assign('activeItem', 7);			
			$this->load->model('../../models/ItemsModel');
			$this->load->model('../../models/CategoryModel');
			$this->load->model('../../models/OptionsModel');
			$this->load->model('../../models/LanguageModel');
			$this->load->model('../../models/PicturesModel');
			$this->load->model('../../models/MetaDataModel');

			$this->defaultLangId = $this->OptionsModel->getDefaultLanguageId();
			$this->CategoryModel->setDefaultLanguage($this->defaultLangId);
			
			$this->load->helper('post_data');
			$this->load->library('getdatalib');

			// Check item type by name
			$itemName = $this->uri->segment(2, 0);	
			if(!$itemName)
			{
				show_error('Error checking item type. Invalid URL.');
			}
			$this->itemName = $itemName;
			
			// Get item congfiguration
			$this->config->load('category_config', FALSE, TRUE);
			if(!$this->itemConfig = $this->config->item($itemName))
			{
				show_error('Item "'.$itemName.'" is not present in config file.');
			}
			
			
			//permissions					
			if(!$this->ion_auth->is_admin())
			{			
				if(!in_array($this->itemConfig['mainName'].'-4', $this->_getPermissions())) 
				{
					redirect('nopermission');
				}
			}			
			
			// Set item configuration			
			if(isset($this->itemConfig['models'][0]))
			{
				//load additional models		
				foreach ($this->itemConfig['models'] as $key => $value) 
				{
					$this->load->model($value);
				}
			}			

			// If -1 then Item doesn't use MetaData
			if($this->itemConfig['metaDataType'] != -1)
			{
				$this->load->model('../../models/MetaDataModel');
				$this->MetaDataModel->setType($this->itemConfig['metaDataType']);
			}			
			
			// Smarty config
			$this->mysmarty->assign('activeItem', $this->itemConfig['activeMenuItem']);
			$this->mysmarty->assign('link', $this->itemConfig['mainLink']);
			$this->mysmarty->assign('mainName', $this->itemConfig['mainName']);

			
			// Second menu settings
			if(isset($this->itemConfig['secondMenu'][0]))
			{
				$this->secondMenu = $this->itemConfig['secondMenu'];
			}

			if($this->session->userdata($this->itemConfig['menuLastEditedVarName']) && ($this->ion_auth->is_admin() || in_array($this->itemConfig['mainName'].'-2', $this->_getPermissions())))
			{
				$lastEdited = $this->session->userdata($this->itemConfig['menuLastEditedVarName']);
				// 99 to always be last
				$this->secondMenu[99]=array(
									"name"   => $this->itemConfig['menuLastEditedText']." [".$lastEdited['name']."]",
									"link"   => $this->itemConfig['menuLastEditedLink'].$lastEdited['id']
								);
			}
			
			if(!$this->ion_auth->is_admin())
			{
				if(!in_array($this->itemConfig['mainName'].'-1', $this->_getPermissions())) 
				{
					unset($this->secondMenu[1]);
				}
			}
		}

		// --------------------------------------------------------------------

		/*function makeAllSlugs()
		{
			$this->load->helper('slug_helper');


			$cats = $this->CategoryModel->getAllCategoryInfo(2);

			foreach($cats as $c)
			{
				$slug = makeSlugs($c->name);
				$this->CategoryModel->saveSlug($c->id, $slug);
			}

			//var_dump($cats);

			//$slug = makeSlugs($title);
		}*/

		function index()
		{
			
			// check method name from URL			
			$this->_checkMethodName();
			
			if($this->session->flashdata('catDeleted'))
			{
				$this->mysmarty->assign('subheader', $this->itemConfig['removeSuccessHeader']);
			}
			
			$this->_loadAdditionalScripts($this->itemConfig['indexAdditional']);
						
			$this->filter = array();						
			if($this->itemConfig['ifFiltered'])
			{
				$this->_setFilters();				
			}

			if($itemsCount = $this->CategoryModel->getCountCategories($this->itemConfig['itemType'], $this->filter))
			{
				//pagination
				$offset = $this->_setPagination($itemsCount);
				$result = $this->CategoryModel->getCategories($this->itemConfig['itemType'], $this->paginationConfig['config']['per_page'], $offset, $this->itemConfig['indexItemsOrder'], $this->filter);
				
				$parentBuffer = array();
				foreach($result as $key => $res)
				{					
					if(isset($parentBuffer[$res->parent_id])) 
					{
						$parent = $parentBuffer[$res->parent_id];
					}
					else
					{
						$parent = $this->CategoryModel->getCategoryParentsInfo($res->parent_id);
						$parentBuffer[$res->parent_id] = $parent;	
					}
					$result[$key]->parents = $parent;
				}	   
			}
			else
			{
				$this->mysmarty->assign('allItems', 0);
				$result = array();
			}
			
			if($this->itemConfig['nestedNodes'])
			{		
				$this->categoryTree = $this->CategoryModel->getTree($this->itemConfig['itemType'], 'name asc');
				$this->mysmarty->assign('itemsTree', $this->categoryTree);
			}
			
			$this->mysmarty->assign('itemsList', $result);
			$this->mysmarty->assign('allItems', $itemsCount);
			$this->mysmarty->assign('secondmenu', $this->secondMenu);
			$this->mysmarty->assign('activeSecondItem', 0);
			$this->mysmarty->display($this->itemConfig['indexTemplate']);
		}
		
		
		// --------------------------------------------------------------------


		function _filter()
		{
			if($this->input->post('filter'))
			{
				$this->session->set_userdata('catSearchFilter', $this->input->post('filter'));
			}
			else
			{
				$this->session->set_userdata('catSearchFilter', '');
			}



			if($this->input->post('parentFilter'))
			{
				$this->session->set_userdata('catCategoryFilter', $this->input->post('parentFilter'));
			}
			else
			{
				$this->session->set_userdata('catCategoryFilter', '');
			}

			$this->session->unset_userdata('catOffset');


			redirect($this->itemConfig['mainLink']);
		}
		
		
		// --------------------------------------------------------------------
		
		
		function _loadAdditionalScripts($input)
		{
			if(isset($input['css'][0]))
			{
				$this->mysmarty->assign('css', $input['css']);				
			}
			
			if(isset($input['js'][0]))
			{
				$this->mysmarty->assign('scripts', $input['js']);				
			}
			
			if(isset($input['outerjs'][0]))
			{
				$this->mysmarty->assign('outerscripts', $input['outerjs']);				
			}
		}
		
		
		// --------------------------------------------------------------------


		function _addnew()
		{
			//permissions					
			if(!$this->ion_auth->is_admin())
			{			
				if(!in_array($this->itemConfig['mainName'].'-1', $this->_getPermissions())) 
				{
					redirect('nopermission');
				}
			}	
			
			
			//set aditional css and	JS
			$this->_loadAdditionalScripts($this->itemConfig['addNewAdditional']);


			//set default			
			$aBackData['position']  = 1;
			$aBackData['is_active'] = 1;
			$this->categorySlug     = '';

			//get default lang name
			$result = $this->LanguageModel->getLanguage($this->defaultLangId);
			
			$languages = $this->getLanguages();
			$this->mysmarty->assign('languages', $languages);

			$this->mysmarty->assign('header', $this->itemConfig['addNewDefaultHeader']);
			$this->mysmarty->assign('subheader', $this->itemConfig['addNewDefaultSubHeader']);
			
			//set second menu			
			$this->mysmarty->assign('secondmenu', $this->secondMenu);
			$this->mysmarty->assign('activeSecondItem', 1);


			//set validation rules
			$this->form_validation->set_rules($this->itemConfig['addNewValidationConfig']);	


			if($this->form_validation->run())
			{
				$this->_clearSmartyCache();
				//make slug
				if(!$this->input->post('slug'))
				{
					$this->categorySlug = $this->CategoryModel->makeSlug($this->input->post('name'), $this->defaultLangId, $this->itemConfig['itemType']);
				}
				else
				{
					$this->categorySlug = $this->CategoryModel->makeSlug($this->input->post('slug'), $this->defaultLangId, $this->itemConfig['itemType']);
				}
				
				$fileName = '';
								
				if(isset($_FILES['thefile']['name']) && $_FILES['thefile']['name'] != '')
				{
					//$files['thefile'] = array($aBackData['picture']);
					$errors = array();
					//file save
					$config['upload_path']     = $this->itemConfig['mainPicturePath'];
					$config['allowed_types']   = $this->itemConfig['allowedTypes'];
					$config['max_size']	      = '2097152';
					$config['overwrite']	      = FALSE;
					$config['max_width']       = $this->itemConfig['mainPictureMaxWidth'];
					$config['max_height']      = $this->itemConfig['mainPictureMaxHeight'];
					$config['min_width']       = $this->itemConfig['mainPictureMinWidth'];
					$config['min_height']      = $this->itemConfig['mainPictureMinWidth'];
					$config['remove_spaces']   = TRUE;
	
					if($this->itemConfig['mainPictureThumbWidth'])
					{							
						$ifResize['image_width'] = $this->itemConfig['mainPictureThumbWidth'];
					}
					else
					{
						$ifResize = false;
					}
					//$ifResize['image_height'] 	= 80;
					$uploadResult = $this->_uploadFile($config, $ifResize);
	
					if($uploadResult[0])
					{
						//get uploaded file path
						$fileName = $uploadResult[1][0];
	
						//delete old file if exists
						/*if($files['thefile'][0] != '')
						{
							unlink($config['upload_path'].'/'.$files['thefile'][0]);							
						}
						$aBackData['picture'] = $fileName;	*/					
					}
					else
					{
						$errors = $uploadResult[1];
					}
				}
				elseif($this->itemConfig['mainPictureMandatory'])
				{
					$errors[0] = $this->itemConfig['mainPictureMandatoryError'];
				}

				if(!isset($errors[0]))
				{
					$data = array_merge($this->getdatalib->fillTableDataWithPost($this->CategoryModel->getContentTableFields()), $this->getdatalib->fillTableDataWithPost($this->CategoryModel->getTableFields()));

					$data['slug']        = $this->categorySlug;
					$data['type']        = $this->itemConfig['itemType'];
					$data['file_name']   = $fileName;
					$data['language_id'] = $this->defaultLangId;


					$add = $this->CategoryModel->addCategory($data);						
	
					if($add)
					{
						$metaData = $this->getdatalib->fillTableDataWithPost($this->MetaDataModel->getMetaInfoTableFields($except = array('id', 'meta_data_id', 'language_id')), $sub = 'meta');						
						$this->MetaDataModel->addMetaData($add, $this->defaultLangId, $metaData);
						
						if($this->input->post('submit-and-edit-next'))
						{
							$nextLangCode = $this->input->post('next');
							redirect('category/'.$this->itemName.'/edit/'.$add.'#'.$nextLangCode);
							exit();
						}
										
						$this->mysmarty->assign('header', $this->itemConfig['addNewSuccessHeader'].' (<a href="'.$this->itemConfig['mainLink'].'/edit/'.$add.'">'.$this->input->post('name').'</a>)');
					}
				}
				else
				{									
					$aBackData = $this->_addNewGetBackData();
					
					$this->mysmarty->assign('errorHeader',  $this->itemConfig['addNewErrorHeader']);
					$this->mysmarty->assign('errors', $errors);
				}

			}
			elseif(validation_errors() != '') // Validation Failed
			{
				$this->mysmarty->assign('errorHeader',  $this->itemConfig['addNewErrorHeader']);

				//prepare for smarty as array
				$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));

				if(isset($aErrors[0]) && $aErrors[0]!='')
				{
					$this->mysmarty->assign('errors', $aErrors);
				}

				$aBackData = $this->_addNewGetBackData();
			}
			
			//TODO if nested_nodes
			if($this->itemConfig['nestedNodes'])
			{
				$this->categoryTree = $this->CategoryModel->getTree($this->itemConfig['itemType'], 'name asc');
				$this->mysmarty->assign('itemsTree', $this->categoryTree);
			}

			$this->mysmarty->assign('backData', $aBackData);
			$this->mysmarty->display($this->itemConfig['addNewTemplate']);
		}
		
		
		// --------------------------------------------------------------------
		
		
		function _addNewGetBackData()
		{			
			$aBackData = getPostDataToArray();
			
			$aBackData['slug'] = $this->categorySlug;		
			return $aBackData;			
		}
		
		
		// --------------------------------------------------------------------
		

		function _edit()
		{
			//permissions					
			if(!$this->ion_auth->is_admin())
			{			
				if(!in_array($this->itemConfig['mainName'].'-2', $this->_getPermissions())) 
				{
					redirect('nopermission');
				}
			}
			
			if(!$categoryid = $this->uri->segment(4))
			{
				redirect($this->itemConfig['mainLink']);
			}
			else
			{
				if(!$this->CategoryModel->ifCategoryExists($categoryid, $this->itemConfig['itemType']))
				{
					$this->session->unset_userdata(array($this->itemConfig['menuLastEditedVarName'] => ''));
					redirect($this->itemConfig['mainLink']);
					exit();
				}
				else
				{
					$this->categoryid = $categoryid;
				}

				$this->_loadAdditionalScripts($this->itemConfig['editAdditional']);

				//get simple info (parentid)
				$simpleInfo = $this->CategoryModel->getCategorySimpleInfo($categoryid);
				$categoryName = $this->CategoryModel->getCategoryName($categoryid, $this->defaultLangId);
				$extendedInfo = $this->CategoryModel->getCategoryInfo($categoryid, $this->defaultLangId);

				$aBackData['categoryid']     = $categoryid;		
				$aImageBackData['main_file'] = $simpleInfo[0]->file_name;
				$aBackData['parent_id']      = $simpleInfo[0]->parent_id;				
				$aBackData['is_default']     = $simpleInfo[0]->is_default;				
				$aBackData['name']           = $categoryName;							
				

				//set header
				$this->mysmarty->assign('header', $this->itemConfig['editDefaultHeader']);
				$this->mysmarty->assign('picturePath', $this->itemConfig['mainPicturePath']);


				
				//get data for all languages
				$aBackData['languages'] = $this->_getAllData($categoryid);



				if(isset($_POST['parent_id']))
				{				
					$this->_clearSmartyCache();
					if($this->itemConfig['mainPicture'])
					{						
						if(isset($_FILES['thefile']['name']) && $_FILES['thefile']['name'] != '')
						{							
							$files['thefile'] = array($aImageBackData['main_file']);
							$errors = array();
			
							//file save
							$config['upload_path']     = $this->itemConfig['mainPicturePath'];
							$config['allowed_types']   = $this->itemConfig['allowedTypes'];
							$config['max_size']	      = '2097152';
							$config['overwrite']	      = FALSE;
							$config['max_width']       = $this->itemConfig['mainPictureMaxWidth'];
							$config['max_height']      = $this->itemConfig['mainPictureMaxHeight'];
							$config['min_width']       = $this->itemConfig['mainPictureMinWidth'];
							$config['min_height']      = $this->itemConfig['mainPictureMinWidth'];
							$config['remove_spaces']   = TRUE;
			
							$ifResize = false;
							if($this->itemConfig['mainPictureThumbWidth'])
							{							
								$ifResize['image_width'] = $this->itemConfig['mainPictureThumbWidth'];
							}							
							$uploadResult = $this->_uploadFile($config, $ifResize);
			
							//var_dump($uploadResult);
							
							
							if($uploadResult[0])
							{							
								//get uploaded file path
								$fileName = $uploadResult[1][0];
			
								//delete old file if exists
								if($files['thefile'][0] != '' && $fileName != $files['thefile'][0])
								{
									unlink($config['upload_path'].'/'.$files['thefile'][0]);
									if($this->itemConfig['mainPictureThumbWidth'])
									{
										unlink($config['upload_path'].'/'.$this->PicturesModel->makeThumbName($files['thefile'][0]));
									}
								}
								$aImageBackData['main_file'] = $fileName;								
							}
							else
							{
								$errors = $uploadResult[1];
								$this->mysmarty->assign('errorHeader',  $this->itemConfig['addNewErrorHeader']);
								$this->mysmarty->assign('errors', $errors[0]);	
							}						
						}						
					}
					
					if(!isset($errors[0]))
					{					
						
						$this->CategoryModel->changeMainData(
							array(
								'id' 					=> $categoryid,
								'parent_id' 		=> $this->input->post('parent_id'),
								'file_name' 		=> $aImageBackData['main_file'],
								'is_default' 		=> $this->input->post('is_default')
							)
						);
	
						$this->mysmarty->assign('header', $this->itemConfig['editSuccessHeader']);
					}
					else
					{
						$this->mysmarty->assign('errors', $errors);
						$this->mysmarty->assign('errorHeader', $this->itemConfig['editErrorHeader']);	
					}
					$aBackData['parent_id'] = $this->input->post('parent_id');	
					$aBackData['is_default'] = $this->input->post('is_default');	
				}
				elseif(isset($_POST['langid']))
				{
					$this->_clearSmartyCache();
					
					$langid = $_POST['langid'];

					//set validation rules
					$this->form_validation->set_rules($this->itemConfig['editValidationConfig']);

					$this->categorySlug = $aBackData['languages'][$langid]['categoryInfo']['slug'];

					if($this->form_validation->run())
					{
						if($this->categorySlug != $this->input->post('slug') || $this->categorySlug == '')
						{
							if(!$this->input->post('slug'))
							{
								$this->categorySlug = $this->CategoryModel->makeSlug($this->input->post('name'), $langid, $this->itemConfig['itemType']);
							}
							else
							{
								$this->categorySlug = $this->CategoryModel->makeSlug($this->input->post('slug'), $langid, $this->itemConfig['itemType']);
							}						
						}
						
						$data = $this->getdatalib->fillTableDataWithPost($this->CategoryModel->getContentTableFields());
						//var_dump($data);
						
						$data['category_id']  = $categoryid;
						$data['language_id']  = $langid;
						$data['slug']         = $this->categorySlug;

						$change = $this->CategoryModel->changeCategory($data);

						//metadata
						$metaData = $this->getdatalib->fillTableDataWithPost($this->MetaDataModel->getMetaInfoTableFields($except = array('id', 'meta_data_id', 'language_id')), $sub = 'meta');
						
						if($this->itemConfig['metaDataType'] != -1)
						{
							$this->MetaDataModel->addMetaData($categoryid, $langid, $metaData);
						}

						$this->mysmarty->assign('header', $this->itemConfig['editSuccessHeader']);

						//clear cache;
						//$this->clearCache($categoryid, $langid);

						///submit and edit next
						if($this->input->post('submit-and-next'))
						{
							$nextLangCode = $aBackData['languages'][$langid]['next_lang_code'];
							redirect('category/'.$this->itemName.'/edit/'.$this->categoryid.'#'.$nextLangCode);
						}

					}
					else
					{
						$this->mysmarty->assign('errorHeader', array($langid => $this->itemConfig['editErrorHeader']));

						//prepare for smarty as array
						$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));

						if(isset($aErrors[0]) && $aErrors[0]!='')
						{
							$aSubErrors[$langid] = $aErrors;
							$this->mysmarty->assign('subErrors', $aSubErrors);
						}
					}				
					
					$aBackData = $this->_editGetDataBack($langid, $aBackData);
					
				}
				
				
				if($this->itemConfig['nestedNodes'])
				{
					$categoryChildren = $this->CategoryModel->getCategoryChildren($categoryid);
					$forbiddenIds = $categoryChildren;
					$forbiddenIds[] = $categoryid;
					
					$filter['customFilter'][]['notin'] = array(					
															'value' => $forbiddenIds,
															'field' => $this->db->dbprefix.'category.id'
														);				
					
					$this->categoryTree = $this->CategoryModel->getTree($this->itemConfig['itemType'], 'name asc', $filter);
					$this->mysmarty->assign('itemsTree', $this->categoryTree);
				}
				
				//second menu settings
				unset($this->secondMenu[99]);
				$this->secondMenu[2]=array(
								"name"   => $this->itemConfig['editDefaultTabName']."[".$categoryName."]",
								"link"   => $this->itemConfig['mainLink']."/edit/".$categoryid
							);				
				$this->_setLastEdited($categoryid, $categoryName);	
				$this->mysmarty->assign('secondmenu', $this->secondMenu);
				$this->mysmarty->assign('activeSecondItem', 2);

				$this->mysmarty->assign('imageBackData', $aImageBackData);
				$this->mysmarty->assign('backData', $aBackData);				
				$this->mysmarty->display($this->itemConfig['editTemplate']);
			}
		}
		
		
		// --------------------------------------------------------------------
		
		
		function _editGetDataBack($langid, $aBackData)
		{
			$aBackDataLang = getPostDataToArray();
			
			$aBackData['languages'][$langid]['categoryInfo']         = $aBackDataLang;
			$aBackData['languages'][$langid]['categoryInfo']['slug'] = $this->categorySlug;
			
			$meta = $this->input->post('meta');
			$aBackData['languages'][$langid]['metaInfo'] = $meta;
			return $aBackData;
		}
		
		
		// --------------------------------------------------------------------	
		

		function _removeCategory()
		{
			//permissions					
			if(!$this->ion_auth->is_admin())
			{			
				if(!in_array($this->itemConfig['mainName'].'-3', $this->_getPermissions())) 
				{
					redirect('nopermission');
				}
			}	
			
			
			if(!$categoryid = $this->uri->segment(4))
			{
				redirect($this->itemConfig['mainLink']);
			}
			else
			{
				if(!$this->CategoryModel->ifCategoryExists($categoryid, $this->itemConfig['itemType']))
				{
					redirect($this->itemConfig['mainLink']);
					exit();
				}
				
				$this->mysmarty->assign('header', $this->itemConfig['removeDefaultHeader']);
				
				$this->secondMenu[0]=array(
								"name"   => $this->itemConfig['removeDefaultTabName'],
								"link"   => $this->itemConfig['mainLink']."/removeCategory/".$categoryid
							);

				$this->secondMenu[1]=array(
									"name"   => "Anuluj",
									"link"   => $this->itemConfig['mainLink']
								);
				$this->mysmarty->assign('secondmenu', $this->secondMenu);
				
				$this->ItemsModel->setDefaultLanguage($this->defaultLangId);
				$categoryName = $this->CategoryModel->getCategoryName($categoryid, $this->defaultLangId);
				
				$parentid = $this->CategoryModel->getParent($categoryid);
				
			
				$categoryChildren = $this->CategoryModel->getCategoryChildren($categoryid);
				$forbiddenIds = $categoryChildren;
				$forbiddenIds[] = $categoryid;
				
				$filter['customFilter'][]['notin'] = array(					
														'value' => $forbiddenIds,
														'field' => $this->db->dbprefix.'category.id'
													);				
				
				$this->categoryTree = $this->CategoryModel->getTree($this->itemConfig['itemType'], 'name asc', $filter);
				$this->mysmarty->assign('itemsTree', $this->categoryTree);


				$this->mysmarty->assign('parent_id', $parentid);
				$this->mysmarty->assign('categoryid', $categoryid);
				$this->mysmarty->assign('categoryName', $categoryName);				

								
				if(isset($_POST['parent_id']))
				{
					$this->_clearSmartyCache();
					
					$destinationCategory = $this->input->post('parent_id');
					
					$this->ItemsModel->remapCategoryItems($categoryid, $destinationCategory);
					//$this->ItemsModel->remapCategory1Items($categoryid, $destinationCategory);
					$this->CategoryModel->remapCategories($categoryid, $destinationCategory);
				
					$this->CategoryModel->removeImage($categoryid, $this->itemConfig['mainPicturePath']);
					$this->CategoryModel->removeCategory($categoryid);
					$this->MetaDataModel->removeMetaData($categoryid);	
					
					//if deleted category is in Last Edited
					if($lastEdited = $this->session->userdata('lastEditedCategory'))
					{
						if($lastEdited['id'] == $categoryid)
						{
							$this->session->unset_userdata(array('lastEditedCategory' => ''));
						}
					}

					$this->session->set_flashdata('catDeleted', '1');
					redirect($this->itemConfig['mainLink']);
				}
			}
			$this->mysmarty->display($this->itemConfig['removeTemplate']);
		}
		
		
		// --------------------------------------------------------------------
		

		function _getAllData($categoryid)
		{
			$languages = $this->getLanguages();

			//info for default language
			$categoryDefaultInfo = $this->CategoryModel->getCategoryInfo($categoryid, $this->defaultLangId);
			$metaDefaultInfo = $this->MetaDataModel->getMetaData($categoryid, $this->defaultLangId);


			foreach($languages as $key => $lang)
			{
				if($this->defaultLangId == $lang->id || !count($categoryInfo))
				{
					$categoryInfo = $categoryDefaultInfo;
				}
				else
				{
					$categoryInfo = $this->CategoryModel->getCategoryInfo($categoryid, $lang->id);
					if(!count($categoryInfo))
					{
						$categoryInfo = $this->_makeEmptyStructure($categoryDefaultInfo);
					}
				}

				if($this->defaultLangId == $lang->id)
				{
					$metaInfo = $metaDefaultInfo;
				}
				else
				{
					//var_dump($lang->id);
					$metaInfo = $this->MetaDataModel->getMetaData($categoryid, $lang->id);
					if(!count($metaInfo))
					{
						$metaInfo = $this->_makeEmptyStructure($this->MetaDataModel->getEmptyStructure());
					}
				}

				if(isset($languages[$key+1]))
				{
					$nextItem = $languages[$key+1];
				}
				else
				{
					$nextItem = $languages[0];
				}



				$backData[$lang->id] = array(
										'id'        => $lang->id,
										'lang_code' => $lang->lang_code,
										'name'      => $lang->name,
										'next_lang_code' => $nextItem->lang_code,
										
										'categoryInfo' => get_object_vars($categoryInfo[0])
									);
									
				if($this->itemConfig['metaDataType'] != -1)
				{					
					$backData[$lang->id]['metaInfo'] = get_object_vars($metaInfo[0]);
				}					
			}
			//var_dump($backData);
			return $backData;			
		}
		
		
		// --------------------------------------------------------------------
		
		
		function getLanguages()
		{
			$languages = $this->LanguageModel->getAllLanguages();			
			
			//default language always first			
			if(count($languages) > 1) 
			{
				foreach($languages as $key => $value) 
				{
					if(($value->id == $this->defaultLangId) && $key != 0) 
					{
						unset($languages[$key]);
						array_unshift($languages, $value);						
					}
				}
			}
			
			return $languages;
		}
		
		
		// --------------------------------------------------------------------
		
		
		function _makeEmptyStructure($structure)
		{
			$defaults = array(
				'position' => 1,
				'is_active' => 1
			);
			
			if(isset($structure[0])) 
			{
				$objVars	= get_object_vars($structure[0]);
				foreach($objVars as $key => $value) 
				{
					$newValue = '';
					if(isset($defaults[$key])) 
					{
						$newValue = $defaults[$key];
					}
					
					$structure[0]->$key = $newValue;
				}
				return $structure;
			}
			return false;
		}
		
		
		// --------------------------------------------------------------------
		

		/*
		obsolete
		function _makeSlug($title, $langId)
		{
			$this->load->helper('slug_helper');
			$slug = makeSlugs($title);
			$slug = $this->_checkSlug($slug, $langId);
			return $slug;
		}*/
		
		
		// --------------------------------------------------------------------		
		

		function _checkSlug($slug, $langId)
		{
			if($slug == '')
			{
				//get default slug
				$info = $this->CategoryModel->getCategoryInfo($this->categoryid, $this->defaultLangId);
				$slug = $info[0]->slug;
				if($slug == '')
				{
					$slug = 'please-write-slug-manually';
				}
			}

			$counter = 1;
			do
			{
				if($result = $this->CategoryModel->checkSlug($slug, $langId, $this->itemConfig['itemType']))
				{
					$slug = $slug.$counter;
				}
				
				$counter++;				
				if($counter > 100)
				{
					show_error('_checkSlug() loop error');
					break;
				}
			}
			while($result);
			return $slug;
		}
		

		// --------------------------------------------------------------------
		

		function _setItemsPerPage()
		{
			if(!$items = $this->uri->segment(4))
			{
				redirect($this->itemConfig['mainLink']);
			}
			else
			{
				$this->session->set_userdata('perPage', $items);
				redirect($this->itemConfig['mainLink']);
			}
		}
		
		function _setFilters()
		{
			//$this->makeAllSlugs();
			/*$this->filter['customFilter'][] = array(
												'field' => 'language_id',
												'value' => $this->defaultLangId
													);*/

			

			if($this->session->userdata('catSearchFilter'))
			{
				$filter['textSearchFilter'] = $this->session->userdata('catSearchFilter');
				$this->filter['customFilter'][] = array(
												'fields' => array('name'),
												'text' 	=> $filter['textSearchFilter']
											);
			}
			else
			{
				$filter['textSearchFilter'] = '';
			}


			//category filter
			if($this->session->userdata('catCategoryFilter'))
			{
				$filter['parentFilter'] = $this->session->userdata('catCategoryFilter');

				if($filter['parentFilter'] != -1)
				{
					$this->filter['customFilter'][] = array(
													'field' => 'category.parent_id',
													'value' => $filter['parentFilter']
												);
				}
			}
			else
			{
				$filter['parentFilter'] = -1;
			}

			//var_dump($filter);

			$this->mysmarty->assign('filter', $filter);
		}
		
		
		// --------------------------------------------------------------------
		
		
		function _setPagination($itemsCount)
		{
			if($this->uri->segment(2))
			{					
				$offset = $this->uri->segment(2);
				$this->mysmarty->assign('offset', $offset);
			}
			else
			{
				$offset = 0;
			}			

			$this->load->library('pagination2');

			$this->paginationConfig['config']['base_url'] = base_url().$this->itemConfig['mainLink'].'/';
			$this->paginationConfig['config']['uri_segment'] = 3;
			$this->paginationConfig['config']['total_rows'] = $itemsCount;

			if($this->session->userdata('perPage'))
			{
				$this->paginationConfig['config']['per_page'] = $this->session->userdata('perPage');
			}
			else
			{
				$this->paginationConfig['config']['per_page'] = $this->paginationConfig['itemsPerPage'][0];
			}

			$this->pagination2->initialize($this->paginationConfig['config']);

			$this->mysmarty->assign('pagination', $this->pagination2->create_links());
			$this->mysmarty->assign('currentPerPage', $this->paginationConfig['config']['per_page']);
			$this->mysmarty->assign('perPageLink', $this->itemConfig['mainLink'].'/setItemsPerPage');	
			
			return $offset;			
		}
		
		
		// --------------------------------------------------------------------
		
		
		function _checkMethodName()
		{
			$methodName = $this->uri->segment(3, 0);			
			if($methodName === 'addnew')
			{
				$this->_addnew();
				exit();				
			}
			elseif($methodName === 'edit')
			{
				$this->_edit();
				exit();				
			}
			elseif($methodName === 'setItemsPerPage')
			{
				$this->_setItemsPerPage();
				exit();				
			}
			elseif($methodName === 'removeCategory')
			{
				$this->_removeCategory();
				exit();				
			}
			elseif($methodName === 'filter')
			{
				$this->_filter();
				exit();				
			}
			elseif($methodName === 'removeImage')
			{
				$this->_removeImage();
				exit();				
			}
			return true;						
			
		}
		
		
		// --------------------------------------------------------------------
		
		
		function _setLastEdited($itemid, $itemTitle)
		{
			$lastEdited = array(
								"name"   => $itemTitle,
								"id"     => $itemid
							);											
			$this->session->set_userdata($this->itemConfig['menuLastEditedVarName'], $lastEdited);
		}
		
		
		// --------------------------------------------------------------------
		
		
		function decode_html(&$input)
		{
			$input = html_entity_decode($input, ENT_QUOTES, 'UTF-8');         
			return $input;
		}	
		
		/*function _uploadFile($config, $ifResize = null)
		{
			$this->load->library('upload', $config);
			$field_name = "thefile";

			if(!$this->upload->do_upload($field_name))
			{
				$warnings = $this->upload->display_errors('', '###');
				$aErrors = explode("###", substr(trim($warnings), 0, -3));
				return array(false, $aErrors);
			}
			else
			{
				$uploadData=$this->upload->data();
				$fileName=$uploadData['file_name'];

				$ifResize = false;
				if(is_array($ifResize))
				{
					if($uploadData['image_width'] > $ifResize['image_width']) // || $uploadData['image_height'] > $ifResize['image_height']
					{						
						$config['image_library']   = 'GD2';
						$config['source_image']    = $uploadData['full_path'];						
						$config['master_dim']  		= 'width';
						$config['maintain_ratio']  = TRUE;
						$config['width']           = $ifResize['image_width'];
						$config['height']          = 400;

						$this->load->library('image_lib', $config);

						if(!$this->image_lib->resize())
						{
							$warnings = $this->image_lib->display_errors('', '###');
							$aErrors = explode("###", substr(trim($warnings), 0, -3));
							return array(false, $aErrors);
						}
					}
				}
				return array(true, array($uploadData['file_name'], $uploadData['full_path']));
			}
		}*/
		
		
		// --------------------------------------------------------------------
		
		
		function _uploadFile($config, $ifResize = null)
		{
			$this->load->library('upload', $config);
			$field_name = "thefile";

			if(!$this->upload->do_upload($field_name))
			{
				$warnings = $this->upload->display_errors('', '###');
				$aErrors = explode("###", substr(trim($warnings), 0, -3));
				return array(false, $aErrors);
			}
			else
			{
				$uploadData=$this->upload->data();
				$fileName=$uploadData['file_name'];

				//$ifResize = false;
				if(is_array($ifResize))
				{
					//if($uploadData['image_width'] > $ifResize['image_width']/* || $uploadData['image_height'] > $ifResize['image_height']*/)
					//{						
					$config['image_library']   = 'GD2';
					$config['source_image']    = $uploadData['full_path'];						
					$config['master_dim']  		= 'width';
					$config['maintain_ratio']  = TRUE;
					$config['width']           = $ifResize['image_width'];
					$config['height']          = 400;
					$config['create_thumb']    = TRUE;

					$this->load->library('image_lib', $config);

					if(!$this->image_lib->resize())
					{
						$warnings = $this->image_lib->display_errors('', '###');
						$aErrors = explode("###", substr(trim($warnings), 0, -3));
						return array(false, $aErrors);
					}
					//}
				}
				return array(true, array($uploadData['file_name'], $uploadData['full_path']));
			}
		}
		
		
		// --------------------------------------------------------------------
		
		
		function _removeImage()
		{
			if(!$categoryid = $this->uri->segment(4))
			{
				redirect($this->itemConfig['mainLink']);
			}
			else
			{
				if(!$this->CategoryModel->ifCategoryExists($categoryid))
				{
					redirect($this->itemConfig['mainLink']);
					exit();
				}
				
				$this->CategoryModel->removeImage($categoryid, $this->itemConfig['mainPicturePath']);  
				redirect($this->itemConfig['mainLink'].'/edit/'.$categoryid);
			}
		}
		
		
		// --------------------------------------------------------------------
		
		
		function _clearSmartyCache()
		{
			if($this->itemConfig['mainName'] == 'prodcats') 
			{
				$this->mysmarty->clear_cache(null,'products');
			}
			
			if($this->itemConfig['mainName'] == 'trademarks') 
			{
				$this->mysmarty->clear_cache(null,'trademarks');
			}
		}	
		
		
		// --------------------------------------------------------------------
		
		
		function _getPermissions()
		{
			$permissions = $this->ion_auth->get_permissions_for_group();
			$this->permissions = $permissions["parsedvalues"];
			if(!isset($this->permissions[0])) 
			{
				$this->permissions = array();
			}
			return $this->permissions;
		}
	}
