<?php

class invoices extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		
		//$this->output->enable_profiler(TRUE);
		$this->mysmarty->assign('activeItem', 7);
		$this->load->model('../../models/UserModel'); 
		$this->load->model('../../models/ServiceModel');      
		$this->load->model('../../models/InvoiceModel');      
				
		$this->secondMenu[0]=array(
							"name"   => "Pokaż wszystkie",
							"link"   => "invoices"
						);

		/*$this->secondMenu[1]=array(
							"name"   => "Add new",
							"link"   => "user/addnew"
						);
		*/

		//permissions					
		if(!$this->ion_auth->is_admin())
		{			
			if(!in_array('invoices-4', $this->_getPermissions())) 
			{
				redirect('nopermission');
			}
		}

								  
		$this->mysmarty->assign('link', 'invoices');
	}
	
	function index()
	{
		$this->filter = array();
		
		$this->mysmarty->assign('css', array('uitheme/cupertino/jquery-ui-1.7.1.custom.css'));
		//date filter
		if($this->session->userdata('filterDateFrom') && $this->session->userdata('filterDateTo'))
		{
			$filter['filterDateTo'] = $this->session->userdata('filterDateTo');
			
			
			$filter['filterDateFrom'] = $this->session->userdata('filterDateFrom');
			
			if($filter['filterDateTo'] != '' && $filter['filterDateFrom'] != '')
			{
				$dateFrom = $this->check_date($filter['filterDateFrom']);
				$dateTo = $this->check_date($filter['filterDateTo']);
				if($dateFrom && $dateTo)
				{
					$dateFromArray = explode('-', $dateFrom);
					$dateToArray = explode('-', $dateTo);
					$from = mktime(0, 0, 0, $dateFromArray[1], $dateFromArray[2], $dateFromArray[0]);
					$to = mktime(23, 59, 59, $dateToArray[1], $dateToArray[2], $dateToArray[0]);
					
					$this->filter['customFilter'][] = array(
							'date' => $filter['filterDateFrom'],
							'field' => 'add_date',
							'from' => $from,
							'to' => $to
					); 
				}
				else
				{
					$this->mysmarty->assign('subheader', 'Invalid date given.');					
				}
			}
		}
		else
		{
			$filter['filterDateTo'] = '';
			$filter['filterDateFrom'] = '';
		}
		
		if($this->session->userdata('invoiceShowFilter1') && $this->session->userdata('invoiceShowFilter1') != 1)
		{
			$filter['showFilter1'] = $this->session->userdata('invoiceShowFilter1');

			if($filter['showFilter1'] == 2)
			{
				$filterValue = 1;
			}
			if($filter['showFilter1'] == 3)
			{
				$filterValue = 2;
			}

			$this->filter['customFilter'][] = array(
												'field' => 'type_id',
												'value' => $filterValue
											);
		}
		else
		{
			$filter['showFilter1'] = 1;
		}

		$this->mysmarty->assign('filter', $filter);
	
		
		if($itemsCount = $this->InvoiceModel->getCountInvoices($this->filter))
		{         
			if($this->uri->segment(3))
			{            
				$offset = $this->uri->segment(3);           
				$this->mysmarty->assign('offset', $offset);
			}        
			else
			{
				$offset = 0;
			}
			
			$this->load->library('pagination2');
							
			$this->paginationConfig['config']['base_url'] = base_url().'invoices/index/';
			$this->paginationConfig['config']['total_rows'] = $itemsCount;  
			
			if($this->session->userdata('perPage'))
			{
				$this->paginationConfig['config']['per_page'] = $this->session->userdata('perPage');
			}
			else
			{
				$this->paginationConfig['config']['per_page'] = $this->paginationConfig['itemsPerPage'][0];
			}
			
			$this->pagination2->initialize($this->paginationConfig['config']);         
			$this->mysmarty->assign('pagination', $this->pagination2->create_links());
			$this->mysmarty->assign('currentPerPage', $this->paginationConfig['config']['per_page']);
			$this->mysmarty->assign('perPageLink', 'invoices/setItemsPerPage');
			$this->mysmarty->assign('allItems', $itemsCount);
			
			$result = $this->InvoiceModel->getAllInvoices($this->paginationConfig['config']['per_page'], $offset, 'add_date desc', $this->filter);
			
			
			foreach($result as $key=>$res)
			{
				if($serviceInfo = $this->ServiceModel->getService($res->service_id))
				{
					//var_dump($serviceInfo);
					
					$pubId = $serviceInfo[0]->public_id;
					$result[$key]->serviceInfo = $serviceInfo[0];
				}
				else
				{
					$pubId = 'This service doesn\'t exist anymore'; 
					$result[$key]->service_id = -1;
				}
								
				/*$result[$key]->userDetails = 'Taki użytkownik nie istnieje';
				$result[$key]->user_id = -1;
				if(isset($serviceInfo->user_id))
				{
					if($userInfo = $this->UserModel->getUser($serviceInfo->user_id))
					{
						$userDetails = $userInfo[0]->first_name.' '.$userInfo[0]->last_name;
						$result[$key]->user_id = $serviceInfo->user_id;
						$result[$key]->userDetails = $userDetails;
					}
				}*/									
			}
		}
		else
		{
			$result = array();
		}     
				
		$this->mysmarty->assign('allItemsCount', $itemsCount);         
		$this->mysmarty->assign('itemsList', $result);         
		$this->mysmarty->assign('secondmenu', $this->secondMenu);         
		$this->mysmarty->assign('activeSecondItem', 0);
		$this->mysmarty->display('invoices/showInvoices.tpl');     
	}
	
	
	function filter()
	{

		if($this->input->post('showFilter1'))
		{
			$this->session->set_userdata('invoiceShowFilter1', $this->input->post('showFilter1'));
		}
		else
		{
			$this->session->set_userdata('invoiceShowFilter1', '');
		}		
		
		if($this->input->post('filterDateFrom'))
		{
			$this->session->set_userdata('filterDateFrom', $this->input->post('filterDateFrom'));
		}
		else
		{
			$this->session->set_userdata('filterDateFrom', '');
		}		
		
		if($this->input->post('filterDateTo'))
		{
			$this->session->set_userdata('filterDateTo', $this->input->post('filterDateTo'));
		}
		else
		{
			$this->session->set_userdata('filterDateTo', '');
		}		

		
		$this->session->unset_userdata('transOffset');
		
		redirect('invoices');
	}
	
		
	function removeInvoice()
	{
		//permissions					
		if(!$this->ion_auth->is_admin())
		{			
			if(!in_array('invoices-3', $this->_getPermissions())) 
			{
				redirect('nopermission');
			}
		}
		
		if(!$id = $this->uri->segment(3))
		{
			redirect('invoices');
		}
		else
		{        
			$this->InvoiceModel->deleteInvoice($id);              
			redirect('invoices');
		}
	}
	
	function getOriginal($id)
	{
		if(!$id)
		{
			redirect('invoices');
		}
		else
		{        
			if(!$this->InvoiceModel->getOriginal($id))
			{
				redirect('invoices');
			}
		}
	}
	
	function getCopy($id)
	{
		if(!$id)
		{
			redirect('invoices');
		}
		else
		{        
			if(!$this->InvoiceModel->getCopy($id))
			{
				redirect('invoices');
			}
		}
	}
	
	function setItemsPerPage()
	{
		if(!$items = $this->uri->segment(3))
		{
			redirect('invoices');
		}
		else
		{  
			$this->session->set_userdata('perPage', $items); 
			redirect('invoices');
		}
	}
	
	function check_date($input)
	{
		$dateArray = explode('-', $input);
		if(isset($dateArray[0]) && isset($dateArray[1]) && isset($dateArray[2]))
		{
			return date('Y-m-d', mktime(0, 0, 0, $dateArray[1], $dateArray[2], $dateArray[0]));
		}
		else
		{
			//$this->form_validation->set_message('check_date', "<strong>Data: '".$input."' jest niepoprawna</strong>. Proszę wprowadzić datę w formacie 'YYYY-MM-DD'.");
			return false;
		}
	}
}