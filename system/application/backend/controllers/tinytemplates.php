<?php defined('BASEPATH') OR exit('No direct script access allowed');

class tinytemplates extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
	}
	
	// --------------------------------------------------------------------

	function index($template = '')
	{
		if($template != '') 
		{			
			$this->mysmarty->display('tiny_templates/'.$template.'.tpl');
		}
		else
		{
			echo 'no template name';
		}
	}
}