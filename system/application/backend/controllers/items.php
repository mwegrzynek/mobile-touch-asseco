<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class items extends CI_Controller
	{
		function __construct()
		{
			parent::__construct();

			//$this->output->enable_profiler(TRUE);

			//load models
			$this->load->model('../../models/ItemsModel');
			$this->load->model('../../models/PicturesModel');
			$this->load->model('../../models/VideosModel');
			$this->load->model('../../models/LanguageModel');
			$this->load->model('../../models/CategoryModel');
			$this->load->model('../../models/OptionsModel');
			$this->load->model('../../models/ServiceModel');
			$this->load->model('../../models/UserModel');

			//load helpers
			$this->load->helper('post_data');

			//load libraries
			$this->load->library('getdatalib');
			$this->load->library('itemslib');

			//set default language
			$this->defaultLangId = $this->OptionsModel->getDefaultLanguageId();
			$this->ItemsModel->setDefaultLanguage($this->defaultLangId);
			$this->CategoryModel->setDefaultLanguage($this->defaultLangId);



			// Check item type by name
			$itemName = $this->uri->segment(2, 0);
			if(!$itemName)
			{
				show_error('Error checking item type. Invalid URL.');
			}
			$this->itemName = $itemName;


			// Get item congfiguration
			$this->config->load('items_config', FALSE, TRUE);
			if(!$this->itemConfig = $this->config->item($itemName))
			{
				show_error('Item "'.$itemName.'" is not present in config file.');
			}

			//permissions
			if(!$this->ion_auth->is_admin())
			{
				if(!in_array($this->itemConfig['mainName'].'-4', $this->_getPermissions()))
				{
					redirect('nopermission');
				}
			}

			// Set item configuration
			/*if(isset($this->itemConfig['models'][0]))
			{
				//load additional models
				foreach ($this->itemConfig['models'] as $key => $value)
				{
					$this->load->model($value);
				}
			}	*/

			// If -1 then Item doesn't use MetaData
			if($this->itemConfig['metaDataType'] != -1)
			{
				$this->load->model('../../models/MetaDataModel');
				$this->MetaDataModel->setType($this->itemConfig['metaDataType']);
			}

			// Smarty config
			$this->mysmarty->assign('activeItem', $this->itemConfig['activeMenuItem']);
			$this->mysmarty->assign('link', $this->itemConfig['mainLink']);
			$this->mysmarty->assign('mainName', $this->itemConfig['mainName']);


			// Second menu settings
			if(isset($this->itemConfig['secondMenu'][0]))
			{
				$this->secondMenu = $this->itemConfig['secondMenu'];
			}

			if($this->session->userdata($this->itemConfig['menuLastEditedVarName']) && ($this->ion_auth->is_admin() || in_array($this->itemConfig['mainName'].'-2', $this->_getPermissions())))
			{
				$lastEdited = $this->session->userdata($this->itemConfig['menuLastEditedVarName']);
				// 99 to always be last
				$this->secondMenu[99]=array(
									"name"   => $this->itemConfig['menuLastEditedText']." [".$lastEdited['name']."]",
									"link"   => $this->itemConfig['menuLastEditedLink'].$lastEdited['id']
								);
			}

			if(!$this->ion_auth->is_admin())
			{
				if(!in_array($this->itemConfig['mainName'].'-1', $this->_getPermissions()))
				{
					unset($this->secondMenu[1]);
				}
			}

			if($this->itemConfig['mainName'] == 'entries')
			{
				$this->_getContactLanguages();
			}
		}

		// --------------------------------------------------------------------

		function index()
		{
			// check method name from URL
			$this->_checkMethodName();

			//index page

			//set aditional css and	JS
			$this->_loadAdditionalScripts($this->itemConfig['indexAdditional']);

			$allPictures = 0;
			$allVideos = 0;

			//filter settings
			  // defaults
			$this->filter = array();
			//$this->categoryid = -1;

			if($this->itemConfig['ifFiltered'])
			{
				$this->_setFilters();
			}

			//var_dump($this->filter);


			//get items
			if($itemsCount = $this->ItemsModel->getCountItems($this->itemConfig['itemType'], $this->filter))
			{
				//pagination
				$offset = $this->_setPagination($itemsCount);

				//get result
				$result = $this->ItemsModel->getAllItems(
					$this->itemConfig['itemType'],
					$this->paginationConfig['config']['per_page'],
					$offset,
					$this->itemConfig['indexItemsOrderBy'],
					$this->itemConfig['indexItemsOrder'],
					$this->filter
				);

				foreach($result as $key => $res)
				{
					$result[$key]->videos_num = $this->VideosModel->getCountVideos($res->id);
					$allVideos += $result[$key]->videos_num;

					$result[$key]->pictures_num = $this->PicturesModel->getCountPictures($res->id);
					$allPictures += $result[$key]->pictures_num;


					if($this->itemConfig['categorized'])
					{
						$categories = $this->ItemsModel->getItemsCategories($res->id);
						if(isset($categories[0]))
						{
							foreach($categories as $value)
							{
								$result[$key]->categories[] = $this->CategoryModel->getCategoryParentsInfo($value);
							}
						}
					}

					if($this->itemConfig['nestedNodes'])
					{
						$result[$key]->parents = $this->ItemsModel->getItemParents($res->parent_id);
					}

					$result[$key] = $this->_indexLoopHook($result[$key]);
				}
			}
			else
			{
				$result = array();
				$this->session->unset_userdata($this->itemConfig['menuLastEditedVarName']);
			}


			if($this->session->flashdata('itemDeleted'))
			{
				$this->mysmarty->assign('subheader', $this->itemConfig['removeSuccessHeader'].' '.$this->session->flashdata('itemDeleted'));
			}


			$this->mysmarty->assign('allPicturesCount', $allPictures);
			$this->mysmarty->assign('allItemsCount', $itemsCount);
			$this->mysmarty->assign('itemsList', $result);
			$this->mysmarty->assign('secondmenu', $this->secondMenu);
			$this->mysmarty->assign('activeSecondItem', 0);

			if($this->itemConfig['categorized'])
			{
				$this->categoryTree = $this->CategoryModel->getTree($this->itemConfig['categoryType'], 'name asc');
				$this->mysmarty->assign('categoryTree', $this->categoryTree);
				$this->mysmarty->assign('categoryLink', $this->itemConfig['categoryLink']);
			}

			if($this->itemConfig['nestedNodes'])
			{
				$this->itemsTree = $this->ItemsModel->getTree($this->itemConfig['itemType'], $this->itemConfig['indexItemsOrderBy'], $this->itemConfig['indexItemsOrder']);
				$this->mysmarty->assign('itemsTree', $this->itemsTree);
			}

			if(isset($this->itemConfig['connectedWithItems']))
			{
				foreach($this->itemConfig['connectedWithItems'] as $key => $value)
				{
					$connectedItems[$value['id']] = $this->ItemsModel->getTree($value['id'], $value['itemsorderby'], $value['itemsorder']);
					$this->mysmarty->assign('connectedItems', $connectedItems);
				}
			}

			$this->mysmarty->display($this->itemConfig['indexTemplate']);
		}



		// --------------------------------------------------------------------



		/* AddNew Item
		 *
		 */
		function _addnew()
		{
			//permissions
			if(!$this->ion_auth->is_admin())
			{
				if(!in_array($this->itemConfig['mainName'].'-1', $this->_getPermissions()))
				{
					redirect('nopermission');
				}
			}


			//addnew item - page

			//set aditional css and	JS
			$this->_loadAdditionalScripts($this->itemConfig['addNewAdditional']);

			//set default
			$aBackData['position'] 		= 1;
			$aBackData['is_active'] 	= true;
			$aBackData['add_date'] 		= date('Y-m-d');
			$this->itemSlug = '';

			//get default lang name
			//$result = $this->LanguageModel->getLanguage($this->defaultLangId);

			$languages = $this->getLanguages();
			$this->mysmarty->assign('languages', $languages);


			$this->mysmarty->assign('header', $this->itemConfig['addNewDefaultHeader']);
			$this->mysmarty->assign('subheader', $this->itemConfig['addNewDefaultSubHeader']);


			//set secondary menu
			$this->mysmarty->assign('secondmenu', $this->secondMenu);
			$this->mysmarty->assign('activeSecondItem', 1);

			//set validation rules
			$this->form_validation->set_rules($this->itemConfig['addNewValidationConfig']);

			//var_dump($_POST);



			if($this->form_validation->run())
			{
				if(!$this->input->post('slug'))
				{
					$this->itemSlug = $this->ItemsModel->makeSlug($this->input->post('title'), $this->defaultLangId, $this->itemConfig['itemType']);
				}
				else
				{
					$this->itemSlug = $this->ItemsModel->makeSlug($this->input->post('slug'), $this->defaultLangId, $this->itemConfig['itemType']);
				}

				$uploadResult = $this->itemslib->uploadFile($this->itemConfig);

				$errors = $uploadResult['errors'];
				$fileName = $uploadResult['fileName'];

				//SPECIAL CACHE DELETE
				//$this->_clearSmartyCache();
				//END SPECIAL




				if(!isset($errors[0]))
				{
					$data = array_merge($this->getdatalib->fillTableDataWithPost($this->ItemsModel->getContentTableFields()), $this->getdatalib->fillTableDataWithPost($this->ItemsModel->getTableFields()));

					if($this->input->post('add_date'))
					{
						$addDate = $this->input->post('add_date');
					}
					else
					{
						$addDate = date("Y-m-d H:i:s");
					}

					$data['add_date']			= $addDate;
					$data['main_image']     = $fileName;
					$data['slug']           = $this->itemSlug;
					$data['language_id']    = $this->defaultLangId;
					$data['type']          	= $this->itemConfig['itemType'];

					//$data = $this->_addPreDataHook($data);

					//Add new item
					$newItemId = $this->ItemsModel->addItem($data);

					//Bind new item to categories
					$this->itemslib->updateCategories($newItemId, $this->input->post('category_id'));

					//Meta data
					$metaData = array();
					if($newItemId && ($this->itemConfig['metaDataType'] != -1))
					{
						$metaData = $this->getdatalib->fillTableDataWithPost($this->MetaDataModel->getMetaInfoTableFields($except = array('id', 'meta_data_id', 'language_id')), $sub = 'meta');
						$this->MetaDataModel->addMetaData($newItemId, $this->defaultLangId, $metaData);
					}

					//$this->_addPostDataHook($newItemId);

					$successMessage = $this->itemConfig['addNewSuccessHeader'];

					//Add to all languages at once
					if($this->input->post('submit-and-fill-langs'))
					{
						$this->itemslib->addToAllLanguages($data, $newItemId, $this->itemConfig, $metaData);
						$successMessage = $this->itemConfig['addNewSuccessHeaderAllLangs'];
						//"Dane zostały pomyślnie zapisane dla wszystkich języków";
					}

					//Add and go to edit next language
					if($this->input->post('submit-and-edit-next'))
					{
						$nextLangCode = $this->input->post('next');
						redirect('items/'.$this->itemName.'/edit/'.$newItemId.'#'.$nextLangCode);
						exit();
					}

					if($this->input->post('parent_id'))
					{
						$aBackData['parent_id'] = $this->input->post('parent_id');
					}

					if($this->input->post('category_id'))
					{
						$aBackData['category_id'] = $this->input->post('category_id');
					}

					$this->mysmarty->assign('header', $this->itemConfig['addNewSuccessHeader'].' (<a href="'.$this->itemConfig['mainLink'].'/edit/'.$newItemId.'">'.$this->input->post('title').'</a>)');
					$this->mysmarty->assign('itemId', $newItemId);
				}
				else
				{
					$aBackData = $this->_addNewGetBackData();
					$this->mysmarty->assign('errorHeader',  $this->itemConfig['addNewErrorHeader']);
					$this->mysmarty->assign('errors', $errors[0]);
				}
			}
			elseif(validation_errors() != '') // Validation Failed
			{
				$this->mysmarty->assign('errorHeader', $this->itemConfig['addNewErrorHeader']);

				//prepare for smarty as array
				$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));

				if(isset($aErrors[0]) && $aErrors[0]!='')
				{
					$this->mysmarty->assign('errors', $aErrors);
				}

				$aBackData = $this->_addNewGetBackData();
			}

			if($this->itemConfig['nestedNodes'])
			{
				$this->itemsTree = $this->ItemsModel->getTree($this->itemConfig['itemType'], $this->itemConfig['indexItemsOrderBy'], $this->itemConfig['indexItemsOrder']);
				$this->mysmarty->assign('itemsTree', $this->itemsTree);
			}

			if($this->itemConfig['categorized'])
			{
				$this->categoryTree = $this->CategoryModel->getTree($this->itemConfig['categoryType'], 'name asc');
				$this->mysmarty->assign('categoryTree', $this->categoryTree);
			}

			if(isset($this->itemConfig['connectedWithItems']))
			{
				foreach($this->itemConfig['connectedWithItems'] as $key => $value)
				{
					$connectedItems[$value['id']] = $this->ItemsModel->getTree($value['id'], $value['itemsorderby'], $value['itemsorder']);
					$this->mysmarty->assign('connectedItems', $connectedItems);
				}
			}


			$this->mysmarty->assign('backData', $aBackData);
			$this->mysmarty->display($this->itemConfig['addNewTemplate']);
		}


		// --------------------------------------------------------------------


		/* Edit Item
		 *
		 */
		function _edit()
		{
			//permissions
			if(!$this->ion_auth->is_admin())
			{
				if(!in_array($this->itemConfig['mainName'].'-2', $this->_getPermissions()))
				{
					redirect('nopermission');
				}
			}


			/*var_dump($_SERVER['DOCUMENT_ROOT']);
			var_dump($_SERVER['HTTP_HOST']);*/

			//set aditional css and	JS
			$this->_loadAdditionalScripts($this->itemConfig['editAdditional']);


			if(!$itemid = $this->uri->segment(4))
			{
				redirect($this->itemConfig['mainLink']);
			}
			else
			{
				if(!$this->ItemsModel->ifItemOfTypeExists($itemid, $this->itemConfig['itemType']))
				{
					redirect($this->itemConfig['mainLink']);
					exit();
				}
				else
				{
					$this->itemid = $itemid;
				}

				//$itemTitle = $this->ItemsModel->getItemTitle($itemid, $this->defaultLangId);
				$itemInfo = $this->ItemsModel->getItemById($itemid,  $this->defaultLangId);
				$itemTitle = $itemInfo[0]->title;


				$aImageBackData['main_image'] = $itemInfo[0]->main_image;

				$this->mysmarty->assign('itemTitle', $itemTitle);
				$this->mysmarty->assign('header', $this->itemConfig['editDefaultHeader']);
				$this->mysmarty->assign('itemid', $itemid);

				if(isset($_POST['lang_independent']))
				{
					$this->form_validation->set_rules($this->itemConfig['editValidationConfigLangIndependent']);

					if($this->form_validation->run())
					{
						$aBackData = getPostDataToArray();

						$aBackData['li_num_data_2'] = $this->input->post('li_num_data_2');

						//var_dump($data);
						unset($aBackData['lang_independent']);
						unset($aBackData['MAX_FILE_SIZE']);
						unset($aBackData['category_id']);
						$data = $this->_editHookGetDataFromForm($aBackData);
						$this->ItemsModel->updateItem($itemid, $data);

						if(isset($_POST['category_id']))
						{
							$this->itemslib->updateCategories($this->itemid, $this->input->post('category_id'));
							$aBackData['category_id'] = $this->input->post('category_id');
						}
					}
					else
					{
						$aBackData = getPostDataToArray();
						$this->mysmarty->assign('errorHeader', $this->itemConfig['editErrorHeader']);


						//prepare for smarty as array
						$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));

						if(isset($aErrors[0]) && $aErrors[0]!='')
						{
							$this->mysmarty->assign('errors', $aErrors);
						}
					}
				}
				elseif(isset($_POST['category_id']))
				{
					$this->itemslib->updateCategories($this->itemid, $this->input->post('category_id'));
					$aBackData['category_id'] = $this->input->post('category_id');
					//SPECIAL FOR ENTRIES
					//$this->_clearSmartyCache();
					//END SPECIAL FOR ENTRIES
					if($this->input->post('add_date'))
					{
						$aBackData['add_date'] = $this->input->post('add_date');
					}

					if($this->input->post('li_num_data_1'))
					{
						$aBackData['li_num_data_1'] = $this->input->post('li_num_data_1');
						$aBackData = $this->_editHookGetDataFromForm($aBackData);
					}
					$this->ItemsModel->updateItem($this->itemid, array('li_num_data_1' => $this->input->post('li_num_data_1')));

					if($this->input->post('parent_id'))
					{
						$this->ItemsModel->changeParent($itemid, $this->input->post('parent_id'));
						$aBackData['parent_id'] = $this->input->post('parent_id');
					}
				}
				elseif(isset($_POST['parent_id']))
				{
					$this->ItemsModel->changeParent($itemid, $this->input->post('parent_id'));
					$aBackData['parent_id'] = $this->input->post('parent_id');
					$aBackData['li_num_data_1'] = $this->input->post('li_num_data_1');
					$this->ItemsModel->updateItem($this->itemid, array('li_num_data_1' => $this->input->post('li_num_data_1')));
				}
				elseif(isset($_POST['add_date']))
				{
					$this->ItemsModel->changeAddDate($itemid, $this->input->post('add_date'));
					$this->mysmarty->assign('subheader', 'Data została zmieniona');
					$aBackData['add_date'] = $this->input->post('add_date');
				}
				else
				{
					//get simple info
					$simpleInfo = $this->ItemsModel->getItemSimpleInfo($itemid);
					$aBackData = get_object_vars($simpleInfo[0]);
					$aBackData = $this->_editHookGetDataFromForm($aBackData);
					$aBackData['parent_id'] = $simpleInfo[0]->parent_id;
					$aBackData = $this->_editHookGetDataFromDB($aBackData);
					$aBackData['category_id'] = $this->ItemsModel->getItemsCategories($this->itemid);
				}

				//get data for all languages
				$aBackData['languages'] = $this->_getAllLangDependedData($itemid);



				if($this->itemConfig['mainPicture'])
				{
					$uploadResult = $this->itemslib->uploadFile($this->itemConfig, $currentFileName = $aImageBackData['main_image']);

					$errors = $uploadResult['errors'];
					$fileName = $uploadResult['fileName'];


					if(!isset($errors[0]))
					{
						$this->ItemsModel->changeMainPicture($itemid, $fileName);
						$aImageBackData['main_image'] = $fileName;
					}
					else
					{
						$this->mysmarty->assign('errorHeader',  $this->itemConfig['addNewErrorHeader']);
						$this->mysmarty->assign('errors', $errors[0]);
					}
				}

				if(isset($_POST['langid']))
				{
					$langid = $_POST['langid'];

					//form validation set rules
					$this->form_validation->set_rules($this->itemConfig['editValidationConfig']);

					$this->itemSlug = $aBackData['languages'][$langid]['itemInfo']['slug'];

					if($this->form_validation->run())
					{
						if($this->itemSlug != $this->input->post('slug') || !$this->input->post('slug'))
						{
							if(!$this->input->post('slug'))
							{
								$this->itemSlug = $this->ItemsModel->makeSlug($this->input->post('title'), $langid, $this->itemConfig['itemType']);
							}
							else
							{
								$this->itemSlug = $this->ItemsModel->makeSlug($this->input->post('slug'), $langid, $this->itemConfig['itemType']);
							}
						}

						$data = $this->getdatalib->fillTableDataWithPost($this->ItemsModel->getContentTableFields());
						//var_dump($data);

						$data['id']           = $itemid;
						$data['language_id']  = $langid;
						$data['slug']         = $this->itemSlug;

						$change = $this->ItemsModel->changeItem($data);

						//MetaData
						if($this->itemConfig['metaDataType'] != -1)
						{
							$metaData = $this->getdatalib->fillTableDataWithPost($this->MetaDataModel->getMetaInfoTableFields($except = array('id', 'meta_data_id', 'language_id')), $sub = 'meta');
							$this->MetaDataModel->addMetaData($itemid, $langid, $metaData);
						}

						//submit and edit next
						if($this->input->post('submit-and-next'))
						{
							$nextLangCode = $aBackData['languages'][$langid]['next_lang_code'];
							redirect($this->itemConfig['mainLink'].'/edit/'.$itemid.'#'.$nextLangCode);
						}

						$this->mysmarty->assign('header', $this->itemConfig['editSuccessHeader']);
						$itemTitle = $this->input->post('title');
					}
					else
					{
						$this->mysmarty->assign('errorHeader', array($langid => $this->itemConfig['editErrorHeader']));

						//prepare for smarty as array
						$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));
						if(isset($aErrors[0]) && $aErrors[0]!='')
						{
							$aSubErrors[$langid] = $aErrors;
							$this->mysmarty->assign('subErrors', $aSubErrors);
						}
					}

					//SPECIAL FOR ENTRIES
					$this->_clearSmartyCache();
					//END SPECIAL FOR ENTRIES

					$aBackData = $this->_editGetDataBack($langid, $aBackData);
				}

				//set header
				$this->mysmarty->assign('title', $itemTitle);

				//second menu settings
				unset($this->secondMenu[99]);
				$this->secondMenu[2]=array(
								"name"   => $this->itemConfig['editDefaultTabName']."[".$itemTitle."]",
								"link"   => $this->itemConfig['mainLink']."/edit/".$itemid
							);
				$this->_setLastEdited($itemid, $itemTitle);

				$this->mysmarty->assign('secondmenu', $this->secondMenu);
				$this->mysmarty->assign('activeSecondItem', 2);


				if($this->itemConfig['categorized'])
				{
					$this->categoryTree = $this->CategoryModel->getTree($this->itemConfig['categoryType'], 'name asc');
					$this->mysmarty->assign('categoryTree', $this->categoryTree);
				}

				if($this->itemConfig['nestedNodes'])
				{
					$itemChildren = $this->ItemsModel->getItemChildren($itemid);
					//var_dump($itemChildren);
					$forbiddenIds = $itemChildren;
					$forbiddenIds[] = $itemid;

					$filter['customFilter'][]['notin'] = array(
															'value' => $forbiddenIds,
															'field' => $this->db->dbprefix.'items.id'
														);

					$this->itemsTree = $this->ItemsModel->getTree($this->itemConfig['itemType'], $this->itemConfig['indexItemsOrderBy'], $this->itemConfig['indexItemsOrder'], $filter);
					$this->mysmarty->assign('itemsTree', $this->itemsTree);

					//get all parents
					$parentid = $aBackData['parent_id'];
					if($parentid == -1)
					{
						$parentid = 0;
					}

					//$parents = $this->ItemsModel->getItemParents($parentid);

					$languages = $this->getLanguages();
					foreach($languages as $key => $lang)
					{
						$parents[$lang->id] = $this->ItemsModel->getItemParents($parentid, $lang->id);
					}

					$this->mysmarty->assign('parents', $parents);
				}

				if(isset($this->itemConfig['connectedWithItems']))
				{
					foreach($this->itemConfig['connectedWithItems'] as $key => $value)
					{
						$connectedItems[$value['id']] = $this->ItemsModel->getTree($value['id'], $value['itemsorderby'], $value['itemsorder']);
						$this->mysmarty->assign('connectedItems', $connectedItems);
					}
				}

				$this->mysmarty->assign('imageBackData', $aImageBackData);
				$this->mysmarty->assign('backData', $aBackData);
				$this->mysmarty->assign('picturePath', $this->itemConfig['mainPicturePath']);
				$this->mysmarty->display($this->itemConfig['editTemplate']);
			}
		}


		// --------------------------------------------------------------------


		function _removeItem()
		{
			//permissions
			if(!$this->ion_auth->is_admin())
			{
				if(!in_array($this->itemConfig['mainName'].'-3', $this->_getPermissions()))
				{
					redirect('nopermission');
				}
			}

			if(!$itemid = $this->uri->segment(4))
			{
				redirect($this->itemConfig['mainLink']);
				exit();
			}
			else
			{
				if(!$this->ItemsModel->ifItemExists($itemid))
				{
					redirect($this->itemConfig['mainLink']);
					exit();
				}

				$itemName = $this->ItemsModel->getItemTitle($itemid, $this->defaultLangId);

				// if simple remove is on
				if(!$this->itemConfig['advancedRemove'])
				{
					$this->_simpleRemoveItem($itemid, $itemName);
					exit();
				}

				//advanced remove (+ pictures, movies, maps)
				$this->mysmarty->assign('header', $this->itemConfig['removeDefaultHeader']);

				$this->secondMenu[0]=array(
								"name"   => $this->itemConfig['removeDefaultTabName'],
								"link"   => $this->itemConfig['mainLink']."/removeItem/".$itemid
							);

				$this->secondMenu[1]=array(
									"name"   => "Cancel",
									"link"   => $this->itemConfig['mainLink']
								);

				$this->mysmarty->assign('secondmenu', $this->secondMenu);
				$this->mysmarty->assign('activeSecondItem', 0);


				if($itemsCount = $this->ItemsModel->getCountItems($this->itemConfig['itemType']))
				{
					$itemChildren = $this->ItemsModel->getItemChildren($itemid);
					//var_dump($itemChildren);
					$forbiddenIds = $itemChildren;
					$forbiddenIds[] = $itemid;

					$filter['customFilter'][]['notin'] = array(
															'value' => $forbiddenIds,
															'field' => $this->db->dbprefix.'items.id'
														);

					$this->itemsTree = $this->ItemsModel->getTree($this->itemConfig['itemType'], $this->itemConfig['indexItemsOrderBy'], $this->itemConfig['indexItemsOrder'], $filter);
					$this->mysmarty->assign('itemsTree', $this->itemsTree);
				}
				else
				{
					$result = array();
					redirect($this->itemConfig['mainLink']);
					exit();
				}
				$this->mysmarty->assign('itemId', $itemid);
				$this->mysmarty->assign('itemName', $itemName);

				//if deleted item is in Last Edited
				$this->_checkLastEdited($itemid);

				if(isset($_POST['deleteOptions']))
				{
					$deleteOptions = $this->input->post('deleteOptions');
					if($deleteOptions == 1)
					{
						$picturesInfo = $this->PicturesModel->getPictures(10000000, 0, 'position asc', $itemid);

						if(isset($picturesInfo[0]))
						{
							//set meta system to pictures
							$this->MetaDataModel->setType(99);
							foreach($picturesInfo as $picture)
							{
								//var_dump($offer->id);
								$thumbsConfig['upload_path'] = $this->itemConfig['galleryPicturesPath'];
								$thumbsConfig['thumbs_info'] = $this->itemConfig['galleryPicturesThumbsInfo'];

								$this->PicturesModel->deletePicture($picture->id, $thumbsConfig);
								$this->PicturesModel->removePicture($picture->id);
								$this->MetaDataModel->removeMetaData($picture->id);
							}
						}

						$this->VideosModel->removeItemVideos($itemid);
						$this->ItemsModel->remapItems($itemid, 0);
					}
					else
					{
						$destinationItem = $this->input->post('parentid');
						$this->ItemsModel->remapItems($itemid, $destinationItem);
						$this->ItemsModel->remapPictures($itemid, $destinationItem);
						$this->ItemsModel->remapVideos($itemid, $destinationItem);
					}

					$this->_simpleRemoveItem($itemid, $itemName);
				}
			}
			$this->mysmarty->display($this->itemConfig['removeTemplate']);
			//redirect('news');
		}

		// --------------------------------------------------------------------

		function _removeSelected()
		{
			if(isset($_POST['id']))
			{
				if(is_array($_POST['id']))
				{
					foreach($_POST['id'] as $itemid)
					{
						$this->ItemsModel->remapItems($itemid, 0);
						$this->ItemsModel->remapPictures($itemid, 0);
						$this->ItemsModel->remapVideos($itemid, 0);
						$this->remove($itemid);
					}
				}
			}

			redirect($this->itemConfig['mainLink']);
		}

		// --------------------------------------------------------------------


		function _simpleRemoveItem($itemid, $itemName)
		{
			$this->_clearSmartyCache();
			$this->remove($itemid);
			$this->session->set_flashdata('itemDeleted', $itemName);
			redirect($this->itemConfig['mainLink']);
		}

		// --------------------------------------------------------------------


		function remove($itemid)
		{
			//set meta system to Items
			if($this->itemConfig['metaDataType'] != -1)
			{
				$this->MetaDataModel->setType($this->itemConfig['itemType']);
				$this->MetaDataModel->removeMetaData($itemid);
			}

			if($this->itemConfig['mainPicture'])
			{
				$thumbsConfig['upload_path'] = $this->itemConfig['mainPicturePath'];
				$thumbsConfig['thumbs_info'] = $this->itemConfig['mainPictureThumbsInfo'];
				$this->ItemsModel->removeMainPicture($itemid, $thumbsConfig);
			}

			$this->_checkLastEdited($itemid);
			$this->ItemsModel->removeItem($itemid);

			$this->_removeHook($itemid);

			return true;
		}


		// --------------------------------------------------------------------

		private function _export()
		{
			//permissions
			if(!$this->ion_auth->is_admin())
			{
				// same pewrmissions as for reading / viewing
				if(!in_array($this->itemConfig['mainName'].'-1', $this->_getPermissions()))
				{
					redirect('nopermission');
				}
			}
			$languages = $this->getLanguages();
			$this->mysmarty->assign('languages', $languages);
			$this->mysmarty->assign('config', $this->itemConfig);
			$this->load->library('importlib');

			//set validation rules
			$this->form_validation->set_rules(array(
															array(
																	'field'   => 'language',
																	'label'   => 'Language',
																	'rules'   => 'trim|strip_tags|required|is_natural'
																))
														);

			$secondMenu = array(
									0 => array(	"name"   => "Export",
													"link"   => "items/".$this->itemConfig['mainName']."/export")

									// 1 => array(	"name"   => "Import",
									// 				"link"   => "items/".$this->itemConfig['mainName']."/import")
								);
			$this->mysmarty->assign('secondmenu', $secondMenu);

			if($this->form_validation->run())
			{
				$this->importlib->setSectionType('items');
				$this->importlib->export($this->itemConfig['itemType'], $this->input->post('language'), $this->itemConfig['import'], $this->itemConfig['showName']);
				exit();
			}
			$this->mysmarty->display('common/export.tpl');
		}

		// --------------------------------------------------------------------


		function _getAllLangDependedData($itemid)
		{
			$languages = $this->getLanguages();

			//info for default language
			$itemDefaultInfo = $this->ItemsModel->getItemById($itemid, $this->defaultLangId);
			//$itemDefaultInfo = $this->ItemsModel->getItemInfo($itemid, $this->defaultLangId);

			$metaDefaultInfo = null;

			if($this->itemConfig['metaDataType'] != -1)
			{
				$metaDefaultInfo = $this->MetaDataModel->getMetaData($itemid, $this->defaultLangId);
			}

			foreach($languages as $key => $lang)
			{
				if($this->defaultLangId == $lang->id)
				{
					$itemInfo = $itemDefaultInfo;
				}
				else
				{
					$itemInfo = $this->ItemsModel->getItemById($itemid, $lang->id);

					if(!isset($itemInfo[0]->title))
					{
						$itemInfo = $this->_makeEmptyStructure($itemDefaultInfo);
					}
				}

				$metaInfo = array();
				if($this->itemConfig['metaDataType'] != -1)
				{
					if($this->defaultLangId == $lang->id)
					{
						$metaInfo = $metaDefaultInfo;
					}
					else
					{
						$metaInfo = $this->MetaDataModel->getMetaData($itemid, $lang->id);
					}

					if(!count($metaInfo))
					{
						$metaInfo = $this->_makeEmptyStructure($this->MetaDataModel->getEmptyStructure());
					}
				}


				if(isset($languages[$key+1]))
				{
					$nextItem = $languages[$key+1];
				}
				else
				{
					$nextItem = $languages[0];
				}

				$backData[$lang->id] = array(
										'id'        => $lang->id,
										'lang_code' => $lang->lang_code,
										'name'      => $lang->name,

										'next_lang_code' => $nextItem->lang_code,

										'itemInfo' => get_object_vars($itemInfo[0])
									);

				if($this->itemConfig['metaDataType'] != -1)
				{
					$backData[$lang->id]['metaInfo'] = get_object_vars($metaInfo[0]);
				}
			}

			return $backData;
		}


		// --------------------------------------------------------------------


		function getLanguages()
		{
			$languages = $this->LanguageModel->getAllLanguages();

			//default language always first
			if(count($languages) > 1)
			{
				foreach($languages as $key => $value)
				{
					if(($value->id == $this->defaultLangId) && $key != 0)
					{
						unset($languages[$key]);
						array_unshift($languages, $value);
					}
				}
			}

			return $languages;
		}


		// --------------------------------------------------------------------


		function _makeEmptyStructure($structure)
		{
			$defaults = array(
				'position' => 1,
				'is_active' => 1
			);

			if(isset($structure[0]))
			{
				$objVars	= get_object_vars($structure[0]);
				foreach($objVars as $key => $value)
				{
					$newValue = '';
					if(isset($defaults[$key]))
					{
						$newValue = $defaults[$key];
					}

					$structure[0]->$key = $newValue;
				}
				return $structure;
			}
			return false;
		}

		// --------------------------------------------------------------------

		function _addNewGetBackData()
		{
			$aBackData = getPostDataToArray();
			$aBackData['slug'] = $this->itemSlug;
			$meta = $this->input->post('meta');
			$aBackData['metaInfo'] = $meta;
			return $aBackData;
		}

		// --------------------------------------------------------------------

		function _editGetDataBack($langid, $aBackData)
		{
			//var_dump($aBackData);


			$aBackDataLang = getPostDataToArray();
			$aBackData['languages'][$langid]['itemInfo']         	= $aBackDataLang;
			$aBackData['languages'][$langid]['itemInfo']['slug']  = $this->itemSlug;
			$meta = $this->input->post('meta');
			$aBackData['languages'][$langid]['metaInfo'] = $meta;

			return $aBackData;
		}

		// --------------------------------------------------------------------

		function _setItemsPerPage()
		{
			if(!$items = $this->uri->segment(4))
			{
				redirect($this->itemConfig['mainLink']);
			}
			else
			{
				$this->session->set_userdata('perPage', $items);
				redirect($this->itemConfig['mainLink']);
			}
		}

		// --------------------------------------------------------------------

		function _filter()
		{
			if($this->input->post('searchFilterTxt'))
			{
				$this->session->set_userdata('searchFilterTxt'.$this->itemName, $this->input->post('searchFilterTxt'));
			}
			else
			{
				$this->session->unset_userdata('searchFilterTxt'.$this->itemName);
			}


			if($this->input->post('categoryFilter') || ((int) $this->input->post('categoryFilter')) === 0)
			{
				$this->session->set_userdata('categoryFilter'.$this->itemName, $this->input->post('categoryFilter'));
			}
			else
			{
				$this->session->unset_userdata('categoryFilter'.$this->itemName);
			}


			if($this->input->post('category1Filter') || ((int) $this->input->post('category1Filter')) === 0)
			{
				$this->session->set_userdata('category1Filter'.$this->itemName, $this->input->post('category1Filter'));
			}
			else
			{
				$this->session->unset_userdata('category1Filter'.$this->itemName);
			}


			if($this->input->post('parentFilter') || ((int) $this->input->post('parentFilter')) === 0)
			{
				$this->session->set_userdata('parentFilter'.$this->itemName, $this->input->post('parentFilter'));
			}
			else
			{
				$this->session->unset_userdata('parentFilter'.$this->itemName);
			}


			if($this->input->post('filterDateFrom'))
			{
				$this->session->set_userdata('filterDateFrom'.$this->itemName, $this->input->post('filterDateFrom'));
			}
			else
			{
				$this->session->unset_userdata('filterDateFrom'.$this->itemName);
			}


			if($this->input->post('filterDateTo'))
			{
				$this->session->set_userdata('filterDateTo'.$this->itemName, $this->input->post('filterDateTo'));
			}
			else
			{
				$this->session->unset_userdata('filterDateTo'.$this->itemName);
			}


			if($this->input->post('showFilter1'))
			{
				$this->session->set_userdata('serviceShowFilter1'.$this->itemName, $this->input->post('showFilter1'));
				//var_dump($this->input->post('showFilter1'));
			}
			else
			{
				$this->session->set_userdata('serviceShowFilter1'.$this->itemName, '');
			}

			if($this->input->post('showFilter2'))
			{
				$this->session->set_userdata('articleFilter'.$this->itemName, $this->input->post('showFilter2'));
				//var_dump($this->input->post('showFilter1'));
			}
			else
			{
				$this->session->set_userdata('articleFilter'.$this->itemName, '');
			}

			redirect($this->itemConfig['mainLink']);
		}


		// --------------------------------------------------------------------

		/*function decode_html(&$input)
		{
			$input = html_entity_decode($input, ENT_QUOTES, 'UTF-8');
			return $input;
		}*/

		// --------------------------------------------------------------------

		function check_date(&$input)
		{
			$dateArray = explode('-', $input);
			if(isset($dateArray[0]) && isset($dateArray[1]) && isset($dateArray[2]))
			{
				return date('Y-m-d', mktime(0, 0, 0, $dateArray[1], $dateArray[2], $dateArray[0]));
			}
			else
			{
				$this->form_validation->set_message('check_date', "<strong>Date: '".$input."' is incorrect</strong>. Please enter correct date in 'YYYY-MM-DD' format.");
				return false;
			}
		}

		// --------------------------------------------------------------------

		function commasToDots(&$input)
		{
			$input = str_replace(',', '.', $input);
			return $input;
		}

		// --------------------------------------------------------------------

		function check_if_entry_exists(&$input)
		{
			if($this->ItemsModel->ifItemOfTypeExists($input, 4))
			{
				return true;
			}
			else
			{
				$this->form_validation->set_message('check_if_entry_exists', "<strong>Entry with given ID: '".$input."' doesn't exist</strong>.");
				return false;
			}
		}

		// --------------------------------------------------------------------

		function check_if_user_exists(&$input)
		{
			if($this->UserModel->checkIfUserExists($input))
			{
				return true;
			}
			else
			{
				$this->form_validation->set_message('check_if_user_exists', "<strong>Użytkownik z podanym ID: '".$input."' nie istnieje</strong>.");
				return false;
			}
		}

		// --------------------------------------------------------------------

		function _loadAdditionalScripts($input)
		{
			if(isset($input['css'][0]))
			{
				$this->mysmarty->assign('css', $input['css']);
			}

			if(isset($input['js'][0]))
			{
				$this->mysmarty->assign('scripts', $input['js']);
			}

			if(isset($input['outerjs'][0]))
			{
				$this->mysmarty->assign('outerscripts', $input['outerjs']);
			}
		}

		// --------------------------------------------------------------------

		function _setPagination($itemsCount)
		{
			if($this->uri->segment(3))
			{
				$offset = $this->uri->segment(3);
				$this->mysmarty->assign('offset', $offset);
			}
			else
			{
				$offset = 0;
			}

			$this->load->library('pagination2');

			$this->paginationConfig['config']['base_url'] = base_url().$this->itemConfig['mainLink'].'/';
			$this->paginationConfig['config']['uri_segment'] = 3;
			$this->paginationConfig['config']['total_rows'] = $itemsCount;

			if($this->session->userdata('perPage'))
			{
				$this->paginationConfig['config']['per_page'] = $this->session->userdata('perPage');
			}
			else
			{
				$this->paginationConfig['config']['per_page'] = $this->paginationConfig['itemsPerPage'][0];
			}

			$this->pagination2->initialize($this->paginationConfig['config']);

			$this->mysmarty->assign('pagination', $this->pagination2->create_links());
			$this->mysmarty->assign('currentPerPage', $this->paginationConfig['config']['per_page']);
			$this->mysmarty->assign('perPageLink', $this->itemConfig['mainLink'].'/setItemsPerPage');

			return $offset;
		}

		// --------------------------------------------------------------------

		function _checkMethodName()
		{
			$methodName = $this->uri->segment(3, 0);
			if($methodName === 'addnew')
			{
				$this->_addnew();
				exit();
			}
			elseif($methodName === 'edit')
			{
				$this->_edit();
				exit();
			}
			elseif($methodName === 'setItemsPerPage')
			{
				$this->_setItemsPerPage();
				exit();
			}
			elseif($methodName === 'removeItem')
			{
				$this->_removeItem();
				exit();
			}
			elseif($methodName === 'filter')
			{
				$this->_filter();
				exit();
			}
			elseif($methodName === 'removeImage')
			{
				$this->_removeImage();
				exit();
			}
			elseif($methodName === 'activate')
			{
				$this->_activateEntry();
				exit();
			}
			elseif($methodName === 'removeSelected')
			{
				$this->_removeSelected();
				exit();
			}
			elseif($methodName === 'export')
			{
				$this->_export();
				exit();
			}
			elseif($methodName === 'import')
			{
				$this->_import();
				exit();
			}
			return true;
		}

		function _setLastEdited($itemid, $itemTitle)
		{
			$lastEdited = array(
								"name"   => $itemTitle,
								"id"     => $itemid
							);
			$this->session->set_userdata($this->itemConfig['menuLastEditedVarName'], $lastEdited);
		}

		function _checkLastEdited($itemid)
		{
			if($lastEdited = $this->session->userdata($this->itemConfig['menuLastEditedVarName']))
			{
				if($lastEdited['id'] == $itemid)
				{
					$this->session->unset_userdata(array($this->itemConfig['menuLastEditedVarName'] => ''));
				}
			}
		}

		// --------------------------------------------------------------------

		function _setFilters()
		{
			//text search filter
			if($this->session->userdata('searchFilterTxt'.$this->itemName))
			{
				$filter['searchFilterTxt'] = $this->session->userdata('searchFilterTxt'.$this->itemName);
				$this->filter['customFilter'][] = array(
														'text' => $filter['searchFilterTxt'],
														'fields' => array('title', 'content')
													);

			}
			else
			{
				$filter['searchFilterTxt'] = '';
			}

			//category filter
			$categoryFilter = $this->session->userdata('categoryFilter'.$this->itemName);
			if(($categoryFilter === 0 || $categoryFilter > -1 ) && is_numeric($categoryFilter))
			{
				$filter['categoryFilter'] = $categoryFilter;


				$itemsIds = $this->ItemsModel->getItemsIdsByCategoryId($categoryFilter);
				$this->filter['customFilter'][]['in'] = array(
															'value' => $itemsIds,
															'field' => $this->db->dbprefix.'items.id'
														);

				/*if(isset($this->itemConfig['multicategorized'])  && $this->itemConfig['multicategorized'] === TRUE )
				{
					//multicategories
					$itemsIds = $this->ItemsModel->getItemsIdsByCategoryId($categoryFilter);
					$this->filter['customFilter'][]['in'] = array(
																'value' => $itemsIds,
																'field' => $this->db->dbprefix.'items.id'
															);
				}
				else
				{
					//regular category
					if($this->itemConfig['indexCategoryFilterWithChildren'])
					{
						$itemChildren = $this->CategoryModel->getCategoryChildren($categoryFilter);
						//var_dump($itemChildren);
						$childIds = $itemChildren;
						$childIds[] = $categoryFilter;

						$this->filter['customFilter'][]['in'] = array(
																'value' => $childIds,
																'field' => 'category_id'
															);
					}
					else
					{
						$this->filter['customFilter'][] = array(
																'value' => $categoryFilter,
																'field' => 'category_id'
															);
					}
				}	*/
			}
			else
			{
				$filter['categoryFilter'] = -1;
			}



			//category1 filter
			/*$category1Filter = $this->session->userdata('category1Filter'.$this->itemName);
			if(($category1Filter === 0 || $category1Filter > -1 ) && is_numeric($category1Filter))
			{
				$filter['category1Filter'] = $category1Filter;
				if($this->itemConfig['indexCategorySecondFilterWithChildren'])
				{
					$itemChildren = $this->CategoryModel->getCategoryChildren($category1Filter);
					//var_dump($itemChildren);
					$childIds = $itemChildren;
					$childIds[] = $category1Filter;

					$this->filter['customFilter'][]['in'] = array(
															'value' => $childIds,
															'field' => 'category1_id'
														);
				}
				else
				{
					$this->filter['customFilter'][] = array(
															'value' => $category1Filter,
															'field' => 'category1_id'
														);
				}
			}
			else
			{
				$filter['category1Filter'] = -1;
			}*/


			//parent filter
			$parentFilter = $this->session->userdata('parentFilter'.$this->itemName);
			if(($parentFilter === 0 || $parentFilter > -1 ) && is_numeric($parentFilter))
			{
				$filter['parentFilter'] = $parentFilter;
				$this->filter['customFilter'][] = array(
														'value' => $parentFilter,
														'field' => 'parent_id'
													);
			}
			else
			{
				$filter['parentFilter'] = -1;
			}

			//show by type filter
			if($this->session->userdata('serviceShowFilter1'.$this->itemName) && $this->session->userdata('serviceShowFilter1'.$this->itemName) != 1)
			{
				$filter['showFilter1'] = $this->session->userdata('serviceShowFilter1'.$this->itemName);

				if($filter['showFilter1'] == 2)
				{
					$filterField = 'li_num_data_1';
				}
				if($filter['showFilter1'] == 3)
				{
					$filterField = 'li_num_data_2';
				}

				$this->filter['customFilter'][] = array(
													'field' => $filterField,
													'value' => 1
												);
			}
			else
			{
				$filter['showFilter1'] = 1;
			}


			//show by type filter
			if($this->session->userdata('articleFilter'.$this->itemName) && $this->session->userdata('articleFilter'.$this->itemName) != 1)
			{
				$filter['showFilter2'] = $this->session->userdata('articleFilter'.$this->itemName);

				if($filter['showFilter2'] == 2)
				{
					$value = 1;
				}
				if($filter['showFilter2'] == 3)
				{
					$value = 0;
				}

				$this->filter['customFilter'][] = array(
													'field' => 'li_num_data_1',
													'value' => $value
												);
			}
			else
			{
				$filter['showFilter2'] = 1;
			}

			//DATE RANGE FILTER
			if($this->session->userdata('filterDateFrom'.$this->itemName) && $this->session->userdata('filterDateTo'.$this->itemName))
			{
				$filter['filterDateTo'] = $this->session->userdata('filterDateTo'.$this->itemName);
				$filter['filterDateFrom'] = $this->session->userdata('filterDateFrom'.$this->itemName);

				if($filter['filterDateTo'] != '' && $filter['filterDateFrom'] != '')
				{
					$dateFrom = $this->check_date($filter['filterDateFrom']);
					$dateTo = $this->check_date($filter['filterDateTo']);
					if($dateFrom && $dateTo)
					{
						$dateFromArray = explode('-', $dateFrom);
						$dateToArray = explode('-', $dateTo);
						$from = mktime(0, 0, 0, $dateFromArray[1], $dateFromArray[2], $dateFromArray[0]);
						$to = mktime(23, 59, 59, $dateToArray[1], $dateToArray[2], $dateToArray[0]);

						$this->filter['customFilter'][] = array(
								'date' => $filter['filterDateFrom'],
								'field' => 'add_date',
								'from' => $from,
								'to' => $to
						);
					}
					else
					{
						$this->mysmarty->assign('subheader', 'Wpisano niepoprawny zakres dat.');
					}
				}
			}
			else
			{
				$filter['filterDateTo'] = '';
				$filter['filterDateFrom'] = '';
			}

			$this->mysmarty->assign('filter', $filter);
		}

		// --------------------------------------------------------------------


		function _removeImage()
		{
			if(!$itemid = $this->uri->segment(4))
			{
				redirect($this->itemConfig['mainLink']);
			}
			else
			{
				if(!$this->ItemsModel->ifItemExists($itemid))
				{
					redirect($this->itemConfig['mainLink']);
					exit();
				}

				$thumbsConfig['upload_path'] = $this->itemConfig['mainPicturePath'];
				$thumbsConfig['thumbs_info'] = $this->itemConfig['mainPictureThumbsInfo'];

				$this->ItemsModel->removeMainPicture($itemid, $thumbsConfig);
				redirect($this->itemConfig['mainLink'].'/edit/'.$itemid);
			}
		}


		function _clearSmartyCache()
		{
			if($this->itemConfig['mainName'] == 'products')
			{
				$this->mysmarty->clear_cache(null,'products');
			}
		}


		function _getPermissions()
		{
			$permissions = $this->ion_auth->get_permissions_for_group();
			$this->permissions = $permissions["parsedvalues"];
			if(!isset($this->permissions[0]))
			{
				$this->permissions = array();
			}
			return $this->permissions;
		}


		//HOOKS

		//add hooks
		function _addPreDataHook($data)
		{
			if($this->itemConfig['mainName'] == 'entries')
			{
				return $this->_addHookEntries($data);
			}
			return $data;
		}

		function _addPostDataHook($newItemId)
		{
			if($this->itemConfig['mainName'] == 'entries')
			{
				return $this->_addHookPostEntries($newItemId);
			}

			return $newItemId;
		}

		function _indexLoopHook($item)
		{
			if($this->itemConfig['mainName'] == 'entries' || $this->itemConfig['mainName'] == 'articles')
			{
				$item = $this->_indexUserLoopHook($item);
			}
			elseif($this->itemConfig['mainName'] == 'sliders')
			{
				$item->entry_info = $this->ItemsModel->getItemById($item->li_num_data_1, $this->defaultLangId);
			}

			return $item;
		}

		//edit hooks
		function _editHookGetDataFromForm($aBackData)
		{
			if($this->itemConfig['mainName'] == 'entries')
			{
				return $this->_editEntryFilterDataFromForm($aBackData);
			}
			elseif($this->itemConfig['mainName'] == 'sliders')
			{
				return $this->_editSliderFilterDataFromForm($aBackData);
			}
			elseif($this->itemConfig['mainName'] == 'clients')
			{
				return $this->_editClientGetDataHook($aBackData);
			}
			return $aBackData;
		}

		function _editHookGetDataFromDB($aBackData)
		{
			if($this->itemConfig['mainName'] == 'entries')
			{
				return $this->_editEntryGetDataFromDB($aBackData);
			}
			return $aBackData;
		}


		//remove hooks
		function _removeHook($itemid)
		{
			if($this->itemConfig['mainName'] == 'articles')
			{
				return $this->_removeHookArticles($itemid);
			}
			return $itemid;
		}


		// ENTRY HOOKS AND SPECIAL FUNCTIONS

		function _activateEntry()
		{
			//permissions
			if(!$this->ion_auth->is_admin())
			{
				if(!in_array($this->itemConfig['mainName'].'-2', $this->_getPermissions()))
				{
					redirect('nopermission');
				}
			}

			if(!$itemid = $this->uri->segment(4))
			{
				redirect($this->itemConfig['mainLink']);
			}
			else
			{
				if(!$this->ItemsModel->ifItemOfTypeExists($itemid, $this->itemConfig['itemType']))
				{
					redirect($this->itemConfig['mainLink']);
					exit();
				}
				else
				{
					$serviceTypes  = $this->ServiceModel->getServiceTypes();
					if($serviceTypes[87]->li_num_data_1)
					{
						$durationDays     = $serviceTypes[87]->li_num_data_1;
						$timestamp        = strtotime("+".$durationDays." days");
						$data['exp_date'] = date('Y-m-d H:i:s', $timestamp);
					}

					$data['li_num_data_2'] = 2;
					$this->ItemsModel->updateItem($itemid, $data);

					//send notification
					$this->load->library('Notices');
					$itemInfo = $this->ItemsModel->getItemById($itemid, $this->defaultLangId);
					$userInfo = $this->UserModel->getUser($itemInfo[0]->li_num_data_1);

					//get user lang
					$userLang = $userInfo[0]->lang_id;
					$userId = $userInfo[0]->id;

					//configure message
					$noticeConfig = array();
					$noticeConfig['noticeType'] = 'entryActivated';
					$noticeConfig['link'] = 'http://'.$_SERVER['HTTP_HOST'].'/user/showentry/'.$itemid;
					$noticeConfig['userid'] = $userId;
					$noticeConfig['topicid'] = 535;
					$noticeConfig['lang_id'] = $userLang;
					$noticeConfig['entry_id'] = $itemInfo[0]->universal_id;

					//send message
					$this->notices->SendNotice($noticeConfig);

					redirect($this->itemConfig['mainLink'].'/edit/'.$itemid);
				}
			}
		}



		//add hooks
		function _addHookEntries($data)
		{
			//categories
			$categories = $data['category_id'];
			$data['category_id'] = 0;

			if(isset($categories[0]))
			{
				foreach($categories as $key => $value)
				{
					if($value < 1)
					{
						unset($categories[$key]);
					}
				}
				$categories = array_unique($categories);
			}
			$this->multiCategories = $categories;


			//contact data
			$data['li_text_data_2'] = $this->_clearContactDataArray($data['li_text_data_2']);
			$data['li_text_data_2'] = serialize($data['li_text_data_2']);


			//original data
			$data['li_text_data_1'] = array(
													'original_title' 			=> $data['title'],
													'original_content' 		=> $data['content'],
													'original_entry_tags' 	=> $data['entry_tags']
												);
			$data['li_text_data_1'] = serialize($data['li_text_data_1']);


			//unique ID
			$data['universal_id'] = substr(md5(rand(100000, 999999)), 0, 5).date('ymd');

			return $data;
		}


		function _addHookPostEntries($newItemId)
		{
			if(isset($this->multiCategories))
			{
				$this->ItemsModel->updateItemCategories($newItemId, $this->multiCategories);
			}
		}


		function _getContactLanguages()
		{
			//get result
			$result = $this->ItemsModel->getAllItems(
				5,
				50,
				0,
				'title',
				'asc'
			);

			$this->mysmarty->assign('contactLanguages', $result);
		}

		function _clearContactDataArray($array)
		{
			$langIds = array();
			foreach($array as $key => $value)
			{
				if($value['lang_id'] != -1 && !in_array($value['lang_id'], $langIds))
				{

					$output[] = $value;
					$langIds[] = $value['lang_id'];
				}
			}
			return $output;
		}

		function _indexUserLoopHook($item)
		{
			$userData = $this->UserModel->getUser($item->li_num_data_1);

			$item->userData = $userData[0];
			return $item;
		}

		//editHooks
		function _editEntryFilterDataFromForm($aBackData)
		{
			$originalData['original_content'] = $aBackData['original_content'];
			$originalData['original_title'] = $aBackData['original_title'];
			$originalData['original_entry_tags'] = $aBackData['original_entry_tags'];
			$this->mysmarty->assign('originalData', $originalData);
			unset($aBackData['original_content']);
			unset($aBackData['original_title']);
			unset($aBackData['original_entry_tags']);


			//categories
			$categories = $aBackData['category_id'];
			$aBackData['category_id'] = 0;

			if(isset($categories[0]))
			{
				foreach($categories as $key => $value)
				{
					if($value < 1)
					{
						unset($categories[$key]);
					}
				}
				$categories = array_unique($categories);
			}

			//contact data
			$aBackData['li_text_data_2'] = $this->_clearContactDataArray($aBackData['li_text_data_2']);
			$aBackData['li_text_data_2'] = serialize($aBackData['li_text_data_2']);


			//user data
			$this->mysmarty->assign('userData', $this->UserModel->getUser($aBackData['li_num_data_1']));


			//services
			$filter['customFilter'][0] = array(
												'field' => 'entry_id',
												'value' => $this->itemid
											);
			//get user services
			$services = $this->ServiceModel->getAllServices(1000, 0, 'add_date desc', $filter);
			$this->mysmarty->assign('entryServices', $services);


			//put additional data to db
			if(isset($categories[0]))
			{
				$this->ItemsModel->updateItemCategories($this->itemid, $categories);
			}
			return $aBackData;
		}


		function _editEntryGetDataFromDB($aBackData)
		{
			$aBackData['li_text_data_1'] = unserialize($aBackData['li_text_data_1']);
			$this->mysmarty->assign('originalData', $aBackData['li_text_data_1']);


			$aBackData['li_text_data_2'] = unserialize($aBackData['li_text_data_2']);
			$aBackData['category_id'] = $this->ItemsModel->getItemsCategories($this->itemid);

			//var_dump($aBackData);
			$this->mysmarty->assign('userData', $this->UserModel->getUser($aBackData['li_num_data_1']));

			//get entry services
			$filter['customFilter'][0] = array(
												'field' => 'entry_id',
												'value' => $this->itemid
											);

			$services = $this->ServiceModel->getAllServices(1000, 0, 'add_date desc', $filter);
			$this->mysmarty->assign('entryServices', $services);

			return $aBackData;
		}


		//SLIDERS HOOKS AND SPECIAL FUNCTIONS
		function _editSliderFilterDataFromForm($aBackData)
		{
			$this->mysmarty->assign('entryData', $this->ItemsModel->getItemById($aBackData['li_num_data_1'], $this->defaultLangId));
			return $aBackData;
		}

		//CLIENTS HOOKS
		function _removeHookArticles($itemid)
		{
			$this->load->model('../../models/CommentsModel');
			$this->CommentsModel->removeItemComments($itemid);
		}

		function _editClientGetDataHook($aBackData)
		{
			if($this->input->post('category_id'))
			{
				$this->ItemsModel->updateItem($this->itemid, array('li_num_data_1' => $this->input->post('li_num_data_1')));
			}
			return $aBackData;
		}

	}//end class



