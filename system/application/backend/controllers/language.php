<?php

class language extends CI_Controller
{

	function __construct()
	{
		parent::__construct();

		$this->mysmarty->assign('activeItem', 10);
		$this->load->model('../../models/LanguageModel');
		$this->load->model('../../models/OptionsModel');

		//permissions
		if(!$this->ion_auth->is_admin())
		{
			if(!in_array('lang-4', $this->_getPermissions()))
			{
				redirect('nopermission');
			}
		}


		$this->secondMenu[0]=array(
							"name"   => "See all",
							"link"   => "language"
						);

		$this->secondMenu[1]=array(
							"name"   => "Add new",
							"link"   => "language/addnew"
						);


		$this->load->library('langlib');


		if($this->session->userdata('lastEditedLanguage') && ($this->ion_auth->is_admin() || in_array('lang-2', $this->_getPermissions())))
		{
			$lastEdited = $this->session->userdata('lastEditedLanguage');
			$this->secondMenu[2]=array(
								"name"   => "Last edited: [".$lastEdited['name']."]",
								"link"   => "language/edit/".$lastEdited['id']
							);
		}

		/*$this->secondMenu[99]=array(
							"name"   => "Ustaw język domyślny",
							"link"   => "language/setdefault"
								);*/

		$this->mysmarty->assign('scripts', array(
												'jquery.cookie.js',
												'ui.tabs.js',
												'ui.tabs.ext.js',
												'tabs.js'
											));
		if(!$this->ion_auth->is_admin())
		{
			if(!in_array('lang-1', $this->_getPermissions()))
			{
				unset($this->secondMenu[1]);
			}
		}
	}

	function index()
	{
		//get all languages
		$result = $this->LanguageModel->getAllLanguages();

		if($this->session->flashdata('itemDefault'))
		{
			$this->mysmarty->assign('subheader', 'You cannot delete default language.');
		}

		if($this->session->flashdata('itemDeleted'))
		{
			$this->mysmarty->assign('subheader', 'Language <em>'.$this->session->flashdata('itemDeleted').'</em> has been deleted');
		}

		$this->mysmarty->assign('css', array('uitheme/cupertino/jquery-ui-1.7.1.custom.css'));

		$this->mysmarty->assign('itemsList', $result);

		$this->mysmarty->assign('secondmenu', $this->secondMenu);
		$this->mysmarty->assign('activeSecondItem', 0);


		$this->mysmarty->display('languages/showLanguages.tpl');
	}


	function addnew()
	{
		//permissions
		if(!$this->ion_auth->is_admin())
		{
			if(!in_array('lang-1', $this->_getPermissions()))
			{
				redirect('nopermission');
			}
		}

		//$this->LanguageModel->copyLanguage(0, 1);

		//set header
		$this->mysmarty->assign('formLink', 'addnew');
		$this->mysmarty->assign('header', 'Add new language');
		$this->mysmarty->assign('add', true);

		//set defaults
		$aBackData['isLangActive'] = true;

		$this->mysmarty->assign('secondmenu', $this->secondMenu);
		$this->mysmarty->assign('activeSecondItem', 1);


		$langs = $this->LanguageModel->getAllLanguages();
		$this->mysmarty->assign('langs', $langs);

		$fields['langName']     = 'Language name';
		$fields['langCode']     = 'Language code';
		$fields['subDomain']    = 'Language domain / subdomain';

		$config = array(
					array(
							'field'   => 'copy_lang_id',
							'label'   => 'Language to copy from',
							'rules'   => 'trim|required'
						),
					array(
							'field'   => 'langName',
							'label'   => 'Language name',
							'rules'   => 'trim|required|strip_tags|callback_check_langname'
						),
					array(
							'field'   => 'langCode',
							'label'   => 'Language code',
							'rules'   => 'trim|required|alpha|max_length[2]|callback_check_langcode'
						),
					array(
							'field'   => 'subDomain',
							'label'   => 'Language domain / subdomain',
							'rules'   => 'trim|required'
						)
				);
		$this->form_validation->set_rules($config);


		if($this->form_validation->run())
		{

			$uploadResult = $this->langlib->uploadFile($this->input->post('langCode'));
			$errors       = $uploadResult['errors'];
			$fileName     = $uploadResult['fileName'];

			if(!isset($errors[0]))
			{

				$langId = $this->LanguageModel->addLanguage//($name, $code, $subdomain, $filename)
				(
					$this->input->post('langName'),
					$this->input->post('langCode'),
					$this->input->post('subDomain'),
					$fileName,
					$this->input->post('is_visible'),
					$this->input->post('isLangActive')
				);

				$this->LanguageModel->copyLanguage($from = $this->input->post('copy_lang_id'), $to = $langId);
			}
			else
			{
				$this->mysmarty->assign('errorHeader', 'Language has not been added');
				$this->mysmarty->assign('errors', $errors[0]);

				$aBackData['langName']     = $this->input->post('langName');
				$aBackData['langCode']     = $this->input->post('langCode');
				$aBackData['subDomain']    = $this->input->post('subDomain');
				$aBackData['direction']    = $this->input->post('direction');
				$aBackData['is_visible']   = $this->input->post('is_visible');
				$aBackData['isLangActive'] = $this->input->post('isLangActive');
			}


			//clear cache
			//$this->mysmarty->clear_all_cache();

			/*$aBackData['langName']     = $this->input->post('langName');
			$aBackData['langCode']     = $this->input->post('langCode');
			$aBackData['subDomain']    = $this->input->post('subDomain');
			$aBackData['isLangActive'] = $this->input->post('isLangActive'); */
		}
		elseif(validation_errors() != '') // Validation Failed
		{
			$this->mysmarty->assign('errorHeader', 'Data has not been changed.');

			//prepare for smarty as array
			$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));

			if(isset($aErrors[0]) && $aErrors[0]!='')
			{
				$this->mysmarty->assign('errors', $aErrors);
			}
			$aBackData['langName']     = $this->input->post('langName');
			$aBackData['langCode']     = $this->input->post('langCode');
			$aBackData['subDomain']    = $this->input->post('subDomain');
			$aBackData['is_visible']   = $this->input->post('is_visible');
			$aBackData['isLangActive'] = $this->input->post('isLangActive');
		}
		$this->mysmarty->assign('backData', $aBackData);
		$this->mysmarty->display('languages/languageActions.tpl');
	}



	function edit()
	{
		//permissions
		if(!$this->ion_auth->is_admin())
		{
			if(!in_array('lang-2', $this->_getPermissions()))
			{
				redirect('nopermission');
			}
		}

		if(!$langid = $this->uri->segment(3))
		{
			redirect('language');
		}
		else
		{
			$this->mysmarty->assign('formLink', 'edit/'.$langid);
			//get language data
			$result = $this->LanguageModel->getLanguage($langid);

			$aBackData['langName']     = $result[0]->name;
			$aBackData['langCode']     = $result[0]->lang_code;
			$aBackData['subDomain']    = $result[0]->subdomain;
			$aBackData['isLangActive'] = $result[0]->is_active;
			$aBackData['is_visible']   = $result[0]->is_visible;


			//set header
			$this->mysmarty->assign('header', 'Edit language: <em>'.$aBackData['langName'].'</em>');


			//set second menu edit item
			$this->secondMenu[2]=array(
							"name"   => "Edytuj: [".$aBackData['langName']."]",
							"link"   => "language/edit/".$langid
						);

			$lastEdited = array(
							"name"   => $aBackData['langName'],
							"id"     => $langid
						);


			$this->session->set_userdata('lastEditedLanguage', $lastEdited);

			$this->mysmarty->assign('secondmenu', $this->secondMenu);
			$this->mysmarty->assign('activeSecondItem', 2);



			//validation rules
			if($this->input->post('langName') != $aBackData['langName'])
			{
				$this->form_validation->set_rules('langName', 'Language name',  'trim|required|strip_tags|callback_check_langname');
			}

			if($this->input->post('langCode') != $aBackData['langCode'])
			{
				$this->form_validation->set_rules('langCode', 'Language code', 'trim|required|alpha|max_length[2]|callback_check_langcode');
			}

			$this->form_validation->set_rules('subDomain', 'Language domain / subdomain', 'trim');


			if($this->form_validation->run()) // Validation Passed
			{
				$uploadResult = $this->langlib->uploadFile($this->input->post('langCode'));
				$errors       = $uploadResult['errors'];
				$fileName     = $uploadResult['fileName'];

				if(!isset($errors[0]))
				{
					$change = $this->LanguageModel->changeLanguage//($id, $name, $code, $subdomain, $filename, $is_active, $is_active)
					(
						$langid,
						$this->input->post('langName'),
						$this->input->post('langCode'),
						$this->input->post('subDomain'),
						'',
						$this->input->post('is_visible'),
						$this->input->post('isLangActive')
					);


					if(!$change)
					{
						$aErrors = array('Some Error');
						$this->mysmarty->assign('errors', $aErrors);
					}
				}
				else
				{
					$this->mysmarty->assign('errorHeader',  'Data has not been updated');
					$this->mysmarty->assign('errors', $errors[0]);
				}

				$aBackData['langName']     = $this->input->post('langName');
				$aBackData['langCode']     = $this->input->post('langCode');
				$aBackData['subDomain']    = $this->input->post('subDomain');
				$aBackData['is_visible']   = $this->input->post('is_visible');
				$aBackData['isLangActive'] = $this->input->post('isLangActive');
			}
			elseif(validation_errors() != '') // Validation Failed
			{
				$this->mysmarty->assign('errorHeader', 'Dane nie zostały zmienione.');

				//prepare for smarty as array
				$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));

				if(isset($aErrors[0]) && $aErrors[0]!='')
				{
					$this->mysmarty->assign('errors', $aErrors);
				}
				$aBackData['langName']     = $this->input->post('langName');
				$aBackData['langCode']     = $this->input->post('langCode');
				$aBackData['subDomain']    = $this->input->post('subDomain');
				$aBackData['is_visible']   = $this->input->post('is_visible');
				$aBackData['isLangActive'] = $this->input->post('isLangActive');
			}

			$this->mysmarty->assign('backData', $aBackData);
			$this->mysmarty->display('languages/languageActions.tpl');
		}
	}


	function setdefault()
	{
		//permissions
		if(!$this->ion_auth->is_admin())
		{
			if(!in_array('lang-5', $this->_getPermissions()))
			{
				redirect('nopermission');
			}
		}


		$this->mysmarty->assign('header', 'Set default language');

		$langs = $this->LanguageModel->getAllLanguages();
		$defaultLang = $this->OptionsModel->getDefaultLanguageId();



		$this->mysmarty->assign('secondmenu', $this->secondMenu);
		$this->mysmarty->assign('activeSecondItem', 99);

		$config = array(
					array(
							'field'   => 'langid',
							'label'   => 'Language',
							'rules'   => 'trim|required'
						)
				);
		$this->form_validation->set_rules($config);


		if($this->form_validation->run())
		{
			$defaultLang = $this->input->post('langid');
			$this->OptionsModel->setDefaultLanguage($defaultLang);
			$this->mysmarty->assign('header', 'Default language has been changed');
		}


		$this->mysmarty->assign('backData', $langs);
		$this->mysmarty->assign('defaultLang', $defaultLang);

		$this->mysmarty->display('languages/setDefault.tpl');
	}


	function removelang()
	{
		//permissions
		if(!$this->ion_auth->is_admin())
		{
			if(!in_array('lang-3', $this->_getPermissions()))
			{
				redirect('nopermission');
			}
		}


		if(!$langid = $this->uri->segment(3))
		{
			redirect('language');
		}
		else
		{
			$this->load->model('../../models/OptionsModel');
			$defaultLangId = $this->OptionsModel->getDefaultLanguageId();

			if($defaultLangId != $langid)
			{
				$result = $this->LanguageModel->getLanguage($langid);

				$this->session->set_flashdata('itemDeleted', $result[0]->name);
				$this->LanguageModel->removeLanguage($langid);
			}
			else
			{
				$this->session->set_flashdata('itemDefault', '1');
			}

			redirect('language');
		}
	}


	function status()
	{
		//permissions
		if(!$this->ion_auth->is_admin())
		{
			if(!in_array('lang-2', $this->_getPermissions()))
			{
				redirect('nopermission');
			}
		}

		if(!$langid = $this->uri->segment(4))
		{
			redirect('language');
		}
		else
		{
			$this->changeStatus($langid);

			//clear cache
			//$this->mysmarty->clear_all_cache();

			redirect('language');
		}
	}


	function changeStatus($langid)
	{
		$statusType = $this->checkStatus();

		if($this->LanguageModel->getStatus($langid, $statusType))
		{
			$this->LanguageModel->setStatus($langid, $statusType, 0);
		}
		else
		{
			$this->LanguageModel->setStatus($langid, $statusType, 1);
		}
	}

	// --------------------------------------------------------------------

	public function checkStatus()
	{
		switch ($this->uri->segment(3)) {
			case 'visibility':
				$statusType = 'is_visible';
				break;

			default:
				$statusType = 'is_active';
				break;
		}

		return $statusType;
	}


	function check_langname($langname)
	{
		if ($this->LanguageModel->checkLanguageName($langname))
		{
			$this->form_validation->set_message('check_langname', 'Name ' . $langname . ' is not available.');
			return false;
		}
		else
		{
			return true;
		}
	}


	function check_langcode($code)
	{
		if($this->LanguageModel->checkLanguageCode($code))
		{
			$this->form_validation->set_message('check_langcode', 'Code ' . $code . ' is not available.');
			return false;
		}
		else
		{
			return true;
		}
	}

	function _getPermissions()
	{
		$permissions = $this->ion_auth->get_permissions_for_group();
		$this->permissions = $permissions["parsedvalues"];
		if(!isset($this->permissions[0]))
		{
			$this->permissions = array();
		}
		return $this->permissions;
	}
}