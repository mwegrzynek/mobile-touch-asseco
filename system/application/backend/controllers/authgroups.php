<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! class_exists('Controller'))
{
	class Controller extends CI_Controller {}
}

class Authgroups extends Controller {

	function __construct()
	{
		parent::__construct();		
		$this->mysmarty->assign('activeItem', 20);
		$this->mysmarty->assign('isAdmin', $this->ion_auth->is_admin());
		
		
		if(!$this->ion_auth->is_admin())
		{			
			$userInfo = $this->ion_auth->get_user();
			$userId = $userInfo->id;
			redirect('auth/edit/'.$userId, 'refresh');
		}

		$this->secondMenu[0]=array(
								"name"   => "Groups list",
								"link"   => "authgroups"
							);
	
		$this->secondMenu[1]=array(
								"name"   => "Add new group",
								"link"   => "authgroups/addnew"
							);

		if($lastEdited = $this->session->userdata('lastEditedGroup'))
		{
			$this->secondMenu[2]=array(
								"name"   => "Last edited: [".$lastEdited['name']."]",
								"link"   => "authgroups/edit/".$lastEdited['id']
							);
		}
	}

	//redirect if needed, otherwise display the user list
	function index()
	{
		if($this->session->flashdata('groupDeleted')) 
		{
			$this->mysmarty->assign('subheader', 'Group has been deleted');
		} 
	
		$this->mysmarty->assign('css', array('uitheme/cupertino/jquery-ui-1.7.1.custom.css'));
		
		//set the flash data error message if there is one
		$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
	
		//list the users
		$groups = $this->ion_auth->get_groups();
								
		$this->mysmarty->assign('itemsList', $groups);
		
		
		$this->mysmarty->assign('secondmenu', $this->secondMenu);
		$this->mysmarty->assign('activeSecondItem', 0);
		$this->mysmarty->display('admin/groups/showGroups.tpl');
	}
	
	
	
	
	function addnew()
	{
		$this->mysmarty->assign('permissions', $this->ion_auth->get_permissions_types());
		
		
		$this->mysmarty->assign('formLink', 'addnew');
		
		//set second menu
		$this->mysmarty->assign('secondmenu', $this->secondMenu);
		$this->mysmarty->assign('activeSecondItem', 1);
		
		$aBackData = array();
		
		$config = array(
				array(
						'field'   => 'description',
						'label'   => 'Group name',
						'rules'   => 'trim|strip_tags|required'
					)
			);

		$this->form_validation->set_rules($config);	

		//set header
		$this->mysmarty->assign('header', 'Add new group.');
		
		if($this->form_validation->run())
		{
			//$aBackData['description'] = $this->input->post('description');			
			$data['name'] = $this->_makeSlug($this->input->post('description'));
			$data['description'] = $this->input->post('description');
			$group_id = $this->ion_auth->add_group($data);
			
			$aBackData['permissions']	= $this->input->post('permissions');		
			if(isset($aBackData['permissions'][0])) 
			{
				foreach($aBackData['permissions'] as $key => $value) 
				{
					$data = array();
					$pArray = $this->_parseActions($value);
					$data['group_id'] = $group_id;
					$data['type_id'] = $pArray[0];
					$data['action_id'] = $pArray[1];
					$data['permission_value'] = 1;
					$this->ion_auth->add_permission($data);
				}
			}
			
			$this->mysmarty->assign('header', 'Group has been added (<a href="authgroups/edit/'.$group_id.'">Edit group '.$aBackData['description'].'</a>)');
		}
		elseif(validation_errors() != '') // Validation Failed
		{
			$this->mysmarty->assign('errorHeader', 'Data has not been saved.');

			//prepare for smarty as array
			$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));

			if(isset($aErrors[0]) && $aErrors[0]!='')
			{
				$this->mysmarty->assign('errors', $aErrors);
			}

			$aBackData['description']	= $this->input->post('description');		
			$aBackData['permissions']	= $this->input->post('permissions');		
		}
		
		$this->mysmarty->assign('backData', $aBackData);		
		$this->mysmarty->display('admin/groups/addAction.tpl');		
	}
	
	
	function edit()
	{
		if(!$this->groupid = $this->uri->segment(3))
		{
			redirect('authgroups');
		}
		else
		{
			if($this->groupid == 1)
			{
				redirect('authgroups');
			}
			
			
			$this->mysmarty->assign('permissions', $this->ion_auth->get_permissions_types());
			
			$permissionsValues = $this->ion_auth->get_permissions_for_group($this->groupid);
			
			//get admin data
			$this->mysmarty->assign('formLink', 'edit/'.$this->groupid);
			$result = $this->ion_auth->get_group($this->groupid);
			
			$aBackData = get_object_vars($result);			
			$aBackData['permissions'] = $permissionsValues['parsedvalues'];
			
			//var_dump($aBackData);
			
	
			$this->mysmarty->assign('header', 'Edit group <em>'.$aBackData['description'].'</em>');
			
			//set second menu edit item only for superuser
			$this->secondMenu[2]=array(
							"name"   => "Edit: [".$aBackData['description']."]",
							"link"   => "authgroups/edit/".$this->groupid
						);
	
			$lastEdited = array(
							"name"   => $aBackData['description'],
							"id"     => $this->groupid
						);
	
			$this->session->set_userdata('lastEditedAdmin', $lastEdited);
	
			$this->mysmarty->assign('secondmenu', $this->secondMenu);
			$this->mysmarty->assign('activeSecondItem', 2);
				
			
			$config = array(
				array(
						'field'   => 'description',
						'label'   => 'Group name',
						'rules'   => 'trim|strip_tags|required'
					)
			);
			
			$this->form_validation->set_rules($config);	
			
			if($this->form_validation->run()) // Validation Passed
			{
				//$aBackData['description'] = $this->input->post('description');			
				$data['name'] = $this->_makeSlug($this->input->post('description'));
				$data['description'] = $this->input->post('description');
				$this->ion_auth->update_group($this->groupid, $data);
				
				$aBackData['permissions']	= $this->input->post('permissions');
				$aBackData['description']	= $this->input->post('description');
									
				$this->ion_auth->delete_group_permissions($this->groupid);
				if(isset($aBackData['permissions'][0])) 
				{					
					foreach($aBackData['permissions'] as $key => $value) 
					{
						$data = array();
						$pArray = $this->_parseActions($value);
						$data['group_id'] = $this->groupid;
						$data['type_identity'] = $pArray[0];
						$data['action_id'] = $pArray[1];
						$data['permission_value'] = 1;
						$this->ion_auth->add_permission($data);
					}
				}
				
				$this->mysmarty->assign('header', 'Group data has been changed');
			}
			elseif(validation_errors() != '') // Validation Failed
			{
				$this->mysmarty->assign('errorHeader', 'Data has not been saved.');
	
				//prepare for smarty as array
				$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));
	
				if(isset($aErrors[0]) && $aErrors[0]!='')
				{
					$this->mysmarty->assign('errors', $aErrors);
				}
				$aBackData['email']    		= $this->input->post('email');
				$aBackData['username'] 		= $this->input->post('username');
				$aBackData['first_name'] 	= $this->input->post('first_name');
				$aBackData['last_name'] 	= $this->input->post('last_name');
				$aBackData['group_id'] 		= $this->input->post('group_id');
			}
	
			$lastEdited = array(
							"name"   => $aBackData['description'],
							"id"     => $this->groupid
						);
			$this->session->set_userdata('lastEditedGroup', $lastEdited);
	
			$this->mysmarty->assign('backData', $aBackData);
			$this->mysmarty->display('admin/groups/editAction.tpl');
		}
	}
	
	
	function remove()
	{
		if(!$this->ion_auth->is_admin())
		{			
			$userInfo = $this->ion_auth->get_user();
			$userId = $userInfo->id;
			redirect('auth/edit/'.$userId, 'refresh');
		}		
		
		if(!$this->groupid = $this->uri->segment(3))
		{
			redirect('auth');
		}
		else
		{
			if($this->groupid == 1)
			{
				redirect('authgroups');
			}
			
			if($result = $this->ion_auth->remove_group($this->groupid)) 
			{
				$this->session->set_flashdata('groupDeleted', 1);
			}
			
			redirect('authgroups');
		}				
	}
	
	
	
	
	function _makeSlug($title)
	{
		$this->load->helper('slug_helper');
		$slug = makeSlugs($title);
		return $slug;
	}
	
	function _parseActions($string)
	{
		$output = explode('-', $string);
		return $output;
	}
}
