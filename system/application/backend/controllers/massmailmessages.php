<?php defined('BASEPATH') OR exit('No direct script access allowed');

class massmailmessages extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		
		//$this->output->enable_profiler(TRUE);
		
		$this->load->model('../../models/UserModel'); 
		$this->load->model('../../models/ServiceModel');
		$this->load->model('../../models/ItemsModel');
		$this->load->model('../../models/OptionsModel');
		$this->load->library('Notices');
		
		$this->defaultLangId = $this->OptionsModel->getDefaultLanguageId();
		$this->ItemsModel->setDefaultLanguage($this->defaultLangId);
	}
	
	function index()
	{
		
	}

	function init($passwd = null)
	{
		$this->_log('initialized');
		
		if($passwd !== '1234567890') 
		{
			die('wrong password');
		}
		
		
		$this->_promoInfo();
		$this->_entryActualizationReminder(-60, 'reminderMinus60', 161);
		$this->_entryActualizationReminder(-30, 'reminderMinus30', 159);
		$this->_entryActualizationReminder(-10, 'reminderMinus10', 163);
		$this->_entryActualizationReminder(-3, 'reminderMinus3', 171);
		$this->_entryActualizationReminder(1, 'reminderToday', 165);
		$this->_entryActualizationReminder(6, 'reminderPlus5', 167);
		$this->_entryActualizationReminder(11, 'reminderPlus10', 169);
		
		
		echo 'ok';
	}
	
	function _promoInfo()
	{
		$this->_log('initialized');
		
		//get users
		$term = 90; //days
		$termSeconds = $term * 24 * 60 * 60;
		
		$filterValue = '(UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(last_promo_mail_date)) > ';
		$filter['customFilter'][0] = array(
											'field' => $filterValue,
											'value' => $termSeconds
										);
		//only clients
		$filter['customFilter'][1] = array(
											'field' => 'group_id',
											'value' => 1
										);
										
										
		$result = $this->UserModel->getAllUsers(1000, 0, 'last_name asc, id asc', $filter);
										
		//set loop
		foreach($result as $key => $value) 
		{
			//get user lang
			$userLang = $value->lang_id;
			
			//configure message  
			$noticeConfig = array();
			$noticeConfig['noticeType'] = 'sendPeriodPromoNotice';					
			$noticeConfig['link'] = 'http://'.$_SERVER['HTTP_HOST'].'/user/';
			$noticeConfig['userid'] = $value->id;
			$noticeConfig['topicid'] = 145;
			$noticeConfig['lang_id'] = $userLang;	
			
			//send message
			$this->notices->SendNotice($noticeConfig);		
				
			//set new date in user account
			$this->_setNewUserDate($value->id);
		}	
		$this->_log('send '.count($result).' messages');
	}
	
	
	function _entryActualizationReminder($time = 0, $noticeType, $topicid = 0)
	{
		$oneDay = 24 * 60 * 60;
		
		
		if($time >= 0) 
		{
			$timeRangeTo = $time * $oneDay;
			$timeRangeFrom = $timeRangeTo - $oneDay;
		}
		else
		{
			$timeRangeFrom = $time * $oneDay;
			$timeRangeTo = $timeRangeFrom + $oneDay;
		}
		
		$filter = array();
		
		$filterValue = '(UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(exp_date)) > ';
		$filter['customFilter'][0] = array(
											'field' => $filterValue,
											'value' => $timeRangeFrom
										);
										
		$filterValue = '(UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(exp_date)) < ';
		$filter['customFilter'][1] = array(
											'field' => $filterValue,
											'value' => $timeRangeTo
												);
								
										
		$entries = $this->ItemsModel->getAllItems(4,	100000, 0, 'add_date', 'desc', $filter);
		
		//var_dump($entries);
		
		
		if(isset($entries[0])) 
		{
			foreach($entries as $key => $value) 
			{
				$userInfo = $this->UserModel->getUser($value->li_num_data_1);
				
				//get user lang
				$userLang = $userInfo[0]->lang_id;
				$userId = $userInfo[0]->id;
				
				//configure message  
				$noticeConfig = array();
				$noticeConfig['noticeType'] = $noticeType;					
				$noticeConfig['link'] = 'http://'.$_SERVER['HTTP_HOST'].'/user/showentry/'.$value->id;
				$noticeConfig['userid'] = $userId;
				$noticeConfig['topicid'] = $topicid;
				$noticeConfig['lang_id'] = $userLang;	
				$noticeConfig['entry_id'] = $value->universal_id;	
				
				//send message
				$this->notices->SendNotice($noticeConfig);				
			}
		}
		
		echo 'Done function '.$noticeType.' with '.count($entries).' messages sent'."<br/>\n";
	}
	
	
	function _setNewUserDate($userId)
	{
		$data['user_id'] = $userId;		
		$data['last_promo_mail_date'] = date('Y-m-d H:i:s');
		$this->UserModel->updateAccountMeta($data);
	}
	
	
	function _log($message)
	{
		log_message('info', __FUNCTION__.' '.$message);
	}
	
}