<?php

class partners extends CI_Controller
{
	function __construct()
	{
		parent::__construct();	
		
		//$this->output->enable_profiler(TRUE);
		$this->mysmarty->assign('activeItem', 15);
		$this->load->model('../../models/PartnersModel');
		$this->load->model('../../models/OptionsModel');		
		$this->load->model('../../models/ItemsModel');     
		$this->load->model('../../models/LanguageModel');     

		$this->defaultLangId = $this->OptionsModel->getDefaultLanguageId();
		$this->ItemsModel->setDefaultLanguage($this->defaultLangId);		

		$this->secondMenu[0]=array(
							"name"   => "See all",
							"link"   => "partners"
						);

		$this->secondMenu[1]=array(
							"name"   => "Add new",
							"link"   => "partners/addnew"
						);
		
		//permissions					
		if(!$this->ion_auth->is_admin())
		{			
			if(!in_array('partner-4', $this->_getPermissions())) 
			{
				redirect('nopermission');
			}
		}
		

		if($lastEdited = $this->session->userdata('lastEditedPartner'))
		{
			$this->secondMenu[2]=array(
								"name"   => "Last edited: [".$lastEdited['name']."]",
								"link"   => "partners/showfull/".$lastEdited['id']
							);
		}
		
		$this->load->helper('post_data');

		$this->mysmarty->assign('link', 'partners');
	}
	
	// --------------------------------------------------------------------

	function index()
	{
		$this->mysmarty->assign('css', array('uitheme/cupertino/jquery-ui-1.7.1.custom.css'));
		
		//FILTERS

		//text search filter
		if($this->session->userdata('partnerSearchFilter'))
		{
			$filter['textSearchFilter'] = $this->session->userdata('partnerSearchFilter');
			$filter['customFilter'][] = array(					
													'text' => $filter['textSearchFilter'],
													'fields' => array('public_id')													
												);						
		}
		else
		{
			$filter['textSearchFilter'] = '';
		}

		//show by status filter
		if($this->session->userdata('partnerShowFilter') && $this->session->userdata('partnerShowFilter') != 1)
		{
			$filter['showFilter'] = $this->session->userdata('partnerShowFilter');
		
			if($filter['showFilter'] == 2 || $filter['showFilter'] == 3)
			{
				if($filter['showFilter'] == 2)
				{
					$filterValue = 0;
				}
				if($filter['showFilter'] == 3)
				{
					$filterValue = 1;
				}
	
				$filter['customFilter'][] = array(
													'field' => 'is_active',
													'value' => $filterValue
												);
			}
			elseif($filter['showFilter'] == 4)
			{
				$filter['customFilter'][] = array(
													'field' => 'spam_reported',
													'value' => 1
				);						
			}
		}
		else
		{
			$filter['showFilter'] = 1;
		}


		//show by type (SMR, WSE) filter
		if($this->session->userdata('partnerShowFilter1') && $this->session->userdata('partnerShowFilter1') != 1)
		{
			$filter['showFilter1'] = $this->session->userdata('partnerShowFilter1');

			if($filter['showFilter1'] == 2)
			{
				$filterValue = 0;
			}
			if($filter['showFilter1'] == 3)
			{
				$filterValue = 1;
			}

			$filter['customFilter'][] = array(
												'field' => 'partner_type',
												'value' => $filterValue
											);
		}
		else
		{
			$filter['showFilter1'] = 1;
		}
		
		//show by client acceptation
		if($this->session->userdata('partnerShowFilter2') && $this->session->userdata('partnerShowFilter2') != 1)
		{
			$filter['showFilter2'] = $this->session->userdata('partnerShowFilter2');

			if($filter['showFilter2'] == 2)
			{
				$filterValue = 2;
			}
			if($filter['showFilter2'] == 3)
			{
				$filterValue = 1;
			}

			$filter['customFilter'][] = array(
												'field' => 'is_accepted',
												'value' => $filterValue
											);
		}
		else
		{
			$filter['showFilter2'] = 1;
		}

		//state filter
		if($this->session->userdata('partnerStateFilter') && $this->session->userdata('partnerStateFilter') != -1)
		{
			$filter['stateFilter'] = $this->session->userdata('partnerStateFilter');
			$filter['customFilter'][] = array(
												'field' => 'state_id',
												'value' => $filter['stateFilter']
											);
		}

		//category filter (not active now)
		if($this->session->userdata('partnerCategoryFilter') === 0 || $this->session->userdata('partnerCategoryFilter') > -1)
		{
			$filter['categoryFilter'] = $this->session->userdata('partnerCategoryFilter');
			$filter['customFilter'][] = array(
												'field' => 'language_id',
												'value' => $filter['categoryFilter']
											);			
		}
		else
		{
			 $filter['categoryFilter'] = -1;
		}
		
		//date filter
		if($this->session->userdata('filterDateFrom') && $this->session->userdata('filterDateTo'))
		{
			$filter['filterDateTo'] = $this->session->userdata('filterDateTo');
			
			
			$filter['filterDateFrom'] = $this->session->userdata('filterDateFrom');
			
			if($filter['filterDateTo'] != '' && $filter['filterDateFrom'] != '')
			{
				$dateFrom = $this->check_date($filter['filterDateFrom']);
				$dateTo = $this->check_date($filter['filterDateTo']);
				if($dateFrom && $dateTo)
				{
					$dateFromArray = explode('-', $dateFrom);
					$dateToArray = explode('-', $dateTo);
					$from = mktime(0, 0, 0, $dateFromArray[1], $dateFromArray[2], $dateFromArray[0]);
					$to = mktime(23, 59, 59, $dateToArray[1], $dateToArray[2], $dateToArray[0]);
					
					$filter['customFilter'][] = array(
							'date' => $filter['filterDateFrom'],
							'field' => 'add_date',
							'from' => $from,
							'to' => $to
					); 
				}
				else
				{
					$this->mysmarty->assign('subheader', 'Invalid date given.');					
				}
			}
		}
		else
		{
			$filter['filterDateTo'] = '';
			$filter['filterDateFrom'] = '';
		}


		$this->mysmarty->assign('filter', $filter);

		if($itemsCount = $this->PartnersModel->getCountPartners($filter))
		{
			if($this->uri->segment(3))
			{
				$offset = $this->uri->segment(3);
				$this->mysmarty->assign('offset', $offset);
			}
			else
			{
				$offset = 0;
			}

			$this->load->library('pagination2');

			$this->paginationConfig['config']['base_url'] = base_url().'partner/index/';
			$this->paginationConfig['config']['total_rows'] = $itemsCount;



			if($this->session->userdata('perPage'))
			{
				$this->paginationConfig['config']['per_page'] = $this->session->userdata('perPage');
			}
			else
			{
				$this->paginationConfig['config']['per_page'] = $this->paginationConfig['itemsPerPage'][0];
			}


			$this->pagination2->initialize($this->paginationConfig['config']);

			$this->mysmarty->assign('pagination', $this->pagination2->create_links());
			$this->mysmarty->assign('currentPerPage', $this->paginationConfig['config']['per_page']);
			$this->mysmarty->assign('perPageLink', 'partner/setItemsPerPage');
			$this->mysmarty->assign('allItems', $itemsCount);

			$result = $this->PartnersModel->getAllPartners($this->paginationConfig['config']['per_page'], $offset, 'name asc', $filter);			
			
		}
		else
		{
			$result = array();
		}

		
		//if partner/ad has been deleted set subheader
		if($this->session->flashdata('deleted'))
		{
			$this->mysmarty->assign('subheader', 'Partner has been deleted.');		
		}
		
		$this->mysmarty->assign('languages', $this->getLanguages());
		$this->mysmarty->assign('allItemsCount', $itemsCount);
		$this->mysmarty->assign('itemsList', $result);
		$this->mysmarty->assign('secondmenu', $this->secondMenu);
		$this->mysmarty->assign('activeSecondItem', 0);
		$this->mysmarty->display('partners/listItems.tpl');
	}
	
	// --------------------------------------------------------------------

	function filter()
	{
		if($this->input->post('filter'))
		{
			$this->session->set_userdata('partnerSearchFilter', $this->input->post('filter'));
		}
		else
		{
			$this->session->set_userdata('partnerSearchFilter', '');
		}

		if($this->input->post('showFilter'))
		{
			$this->session->set_userdata('partnerShowFilter', $this->input->post('showFilter'));
		}
		else
		{
			$this->session->set_userdata('partnerShowFilter', '');
		}

		if($this->input->post('stateFilter'))
		{
			$this->session->set_userdata('partnerStateFilter', $this->input->post('stateFilter'));
		}
		else
		{
			$this->session->set_userdata('partnerStateFilter', '');
		}


		if($this->input->post('categoryFilter') || $this->input->post('categoryFilter') === 0)
		{
			$this->session->set_userdata('partnerCategoryFilter', $this->input->post('categoryFilter'));
		}
		else
		{
			$this->session->set_userdata('partnerCategoryFilter', -1);
		}



		if($this->input->post('showFilter1'))
		{
			$this->session->set_userdata('partnerShowFilter1', $this->input->post('showFilter1'));
		}
		else
		{
			$this->session->set_userdata('partnerShowFilter1', '');
		}
		
		if($this->input->post('showFilter2'))
		{
			$this->session->set_userdata('partnerShowFilter2', $this->input->post('showFilter2'));
		}
		else
		{
			$this->session->set_userdata('partnerShowFilter2', '');
		}
		
		if($this->input->post('filterDateFrom'))
		{
			$this->session->set_userdata('filterDateFrom', $this->input->post('filterDateFrom'));
		}
		else
		{
			$this->session->set_userdata('filterDateFrom', '');
		}		
		
		if($this->input->post('filterDateTo'))
		{
			$this->session->set_userdata('filterDateTo', $this->input->post('filterDateTo'));
		}
		else
		{
			$this->session->set_userdata('filterDateTo', '');
		}		

		$this->session->unset_userdata('partnerOffset');


		redirect('partners');
	}
	
	// --------------------------------------------------------------------	
	
	function addnew()
	{
		//set header
		$this->mysmarty->assign('header', 'Add new partner.');
		
		$this->mysmarty->assign('css', array('uitheme/cupertino/jquery-ui-1.7.1.custom.css'));
		$this->mysmarty->assign('scripts', array('tiny.init.js'));
		$this->mysmarty->assign('outerscripts',  array(
												array(
													'name' => 'tiny_mce.js',
													'src'  => 'tinymce'
												)));
		
		if(isset($_POST['name']))
		{
			$config = $this->getValidationConfig();
			$this->form_validation->set_rules($config);
			
			if($this->form_validation->run())
			{				
				if(!isset($errors[0]))
				{
					$data = getPostDataToArray();				
					
					if($partnerId = $this->PartnersModel->addPartner($data)) 
					{
						$this->mysmarty->assign('header', 'New partner has been added: (<a href="partners/showfull/'.$partnerId.'">'.$this->input->post('name').'</a>)');
						$this->mysmarty->assign('partnerId', $partnerId);						
						//send message to user
					}					
				}
				else
				{
					$this->mysmarty->assign('errors', $errors);
				}				
			}
			elseif(validation_errors() != '') // Validation Failed
			{
				$this->mysmarty->assign('errorHeader', 'Data has not been saved.');	
				//prepare for smarty as array
				$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));	
				if(isset($aErrors[0]) && $aErrors[0]!='')
				{
					$this->mysmarty->assign('errors', $aErrors);
				}		
				$aBackData = getPostDataToArray();
				
				$this->mysmarty->assign('backData', $aBackData);	
			}	
		}
		
		$this->mysmarty->assign('languages', $this->getLanguages());
		$this->mysmarty->assign('secondmenu', $this->secondMenu);
		$this->mysmarty->assign('activeSecondItem', 1);
		
		$this->mysmarty->assign('css', array('uitheme/cupertino/jquery-ui-1.7.1.custom.css'));
		$this->mysmarty->display('partners/addAction.tpl');
		
	}	
	
	// --------------------------------------------------------------------
	
	
	function showfull()
	{
		if(!$partnerid = $this->uri->segment(3))
		{
			redirect('partner');
		}
		else
		{
			$this->mysmarty->assign('formLink', 'showfull/'.$partnerid);						
			$this->mysmarty->assign('css', array('uitheme/cupertino/jquery-ui-1.7.1.custom.css'));
			$this->mysmarty->assign('scripts', array('tiny.init.js'));
			$this->mysmarty->assign('outerscripts',  array(
													array(
														'name' => 'tiny_mce.js',
														'src'  => 'tinymce'
													)));

			//get partner data
			$mainData = $this->PartnersModel->getPartner($partnerid);
			
			
			$partnerName = $mainData[0]->name;
						
			//set header
			$this->mysmarty->assign('header', 'See partner: <em>'.$partnerName.'</em>');

			//set second menu edit item
			$this->secondMenu[2]=array(
							"name"   => "See partner: [".$partnerName."]",
							"link"   => "partners/showfull/".$partnerid
						);

			$lastEdited = array(
							"name"   => $partnerName,
							"id"     => $partnerid
						);
						
			$aBackData = get_object_vars($mainData[0]);
									
			if(isset($_POST['name']))
			{					
				$config = $this->getValidationConfig();
	
				$this->form_validation->set_rules($config);
				
				if($this->form_validation->run())
				{						
					$data = getPostDataToArray();	
					$data['id'] = $partnerid;
					$data['is_default'] = $this->input->post('is_default');
					$this->PartnersModel->updatePartner($data);
					// not put form again when page reload
					//redirect(str_replace('/admin', '', $_SERVER['REDIRECT_URL']), 303);
				}
				elseif(validation_errors() != '') // Validation Failed
				{
					$this->mysmarty->assign('errorHeader', 'Data has not been changed.');
	
					//prepare for smarty as array
					$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));
	
					if(isset($aErrors[0]) && $aErrors[0]!='')
					{
						$this->mysmarty->assign('errors', $aErrors);
					}	
					
				}
				$aBackData = getPostDataToArray();				
			}

			//$langs = $this->LanguageModel->getAllLanguages();
		
			
			$this->session->set_userdata('lastEditedPartner', $lastEdited);

			$this->mysmarty->assign('languages', $this->getLanguages());
			$this->mysmarty->assign('backData', $aBackData);
			$this->mysmarty->assign('secondmenu', $this->secondMenu);
			$this->mysmarty->assign('activeSecondItem', 2);

			$this->mysmarty->display('partners/editAction.tpl');
		}
	}
	
	// --------------------------------------------------------------------

	function removePartner()
	{
		//permissions					
		if(!$this->ion_auth->is_admin())
		{			
			if(!in_array('partner-3', $this->_getPermissions())) 
			{
				redirect('nopermission');
			}
		}
		
		if(!$partnerid = $this->uri->segment(3))
		{
			redirect('partner');
		}
		else
		{
			$this->PartnersModel->removePartner($partnerid);
			$this->_checkLastEdited($partnerid);
			$this->session->set_flashdata('deleted', $partnerid);
			redirect('partner');
		}
	}
	
	// --------------------------------------------------------------------

	function setItemsPerPage()
	{
		if(!$items = $this->uri->segment(3))
		{
			redirect('partner');
		}
		else
		{
			$this->session->set_userdata('perPage', $items);
			redirect('partner');
		}
	}

	
	// --------------------------------------------------------------------

	
	function _uploadFile($config, $field_name)
	{
		$this->load->library('upload', $config);
		if(!$this->upload->do_upload($field_name))
		{
			$warnings = $this->upload->display_errors('', '###');
			$aErrors = explode("###", substr(trim($warnings), 0, -3));
			return array(false, $aErrors);
		}
		else
		{
			$uploadData=$this->upload->data();
			$fileName=$uploadData['file_name'];
			return array(true, array($uploadData['file_name'], $uploadData['full_path']));
		}
	}
	
	// --------------------------------------------------------------------
	
	function check_date($input)
	{
		$dateArray = explode('-', $input);
		if(isset($dateArray[0]) && isset($dateArray[1]) && isset($dateArray[2]))
		{
			return date('Y-m-d', mktime(0, 0, 0, $dateArray[1], $dateArray[2], $dateArray[0]));
		}
		else
		{
			//$this->form_validation->set_message('check_date', "<strong>Data: '".$input."' jest niepoprawna</strong>. Proszę wprowadzić datę w formacie 'YYYY-MM-DD'.");
			return false;
		}
	}
	
	// --------------------------------------------------------------------
	
	function _checkLastEdited($itemid)
	{
		if($lastEdited = $this->session->userdata('lastEditedPartner'))
		{
			if($lastEdited['id'] == $itemid)
			{
				$this->session->unset_userdata(array('lastEditedPartner' => ''));
			}
		}
	}	
	
	// --------------------------------------------------------------------
	
	function _getMessageTemplates()
	{
		$filter['customFilter'][0] = array(
								'field' => 'is_active',
								'value' => 1
			);
		
		$this->defaultLangId = $this->OptionsModel->getDefaultLanguageId();
		$this->ItemsModel->setDefaultLanguage($this->defaultLangId);	
		$result = $this->ItemsModel->getAllItems(
				6,
				10000000, 
				0, 
				'title', 
				'asc',
				-1,
				$filter
			);	
			
		return $result;	
	}
	
	// --------------------------------------------------------------------
	
	function _getPermissions()
	{
		$permissions = $this->ion_auth->get_permissions_for_group();
		$this->permissions = $permissions["parsedvalues"];
		if(!isset($this->permissions[0])) 
		{
			$this->permissions = array();
		}
		return $this->permissions;
	}
	
	// --------------------------------------------------------------------
	
	function getLanguages()
	{
		$languages = $this->LanguageModel->getAllLanguages();			
		
		//default language always first			
		if(count($languages) > 1) 
		{
			foreach($languages as $key => $value) 
			{
				if(($value->id == $this->defaultLangId) && $key != 0) 
				{
					unset($languages[$key]);
					array_unshift($languages, $value);						
				}
			}
		}
		
		return $languages;
	}
	
	// --------------------------------------------------------------------
	
	function getValidationConfig()
	{
		$config = array(
					array(
							'field'   => 'company_name',
							'label'   => 'Company name',
							'rules'   => 'trim|strip_tags|required'
						),
					array(
							'field'   => 'language_id',
							'label'   => 'Language',
							'rules'   => 'trim|strip_tags|required'
						),
					array(
							'field'   => 'county_name',
							'label'   => 'Country name',
							'rules'   => 'trim|strip_tags'
						),
					array(
							'field'   => 'phone',
							'label'   => 'Phone',
							'rules'   => 'trim|strip_tags'
						),
					array(
							'field'   => 'mail',
							'label'   => 'Mail',
							'rules'   => 'trim|strip_tags'
						),
					array(
							'field'   => 'info',
							'label'   => 'Info',
							'rules'   => 'trim'
						),
					array(
							'field'   => 'is_default',
							'label'   => 'Default',
							'rules'   => 'trim'
						)
			);
				
		return $config;		
	}
}