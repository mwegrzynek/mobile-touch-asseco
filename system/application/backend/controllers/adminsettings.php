<?php

class adminsettings extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->mysmarty->assign('activeItem', 20);
		$this->load->model('Admin');



		$this->secondMenu[0]=array(
							"name"   => "Ustawienia",
							"link"   => "adminsettings"
						);

	/*	$this->secondMenu[1]=array(
							"name"   => "Dodaj nowego administratora",
							"link"   => "adminsettings/addnew"
								);*/

		if($lastEdited = $this->session->userdata('lastEditedAdmin'))
		{
			$this->secondMenu[2]=array(
								"name"   => "Edit last edited: [".$lastEdited['name']."]",
								"link"   => "adminsettings/edit/".$lastEdited['id']
							);
		}
	}

	function index()
	{
		//if user belogs to group "Root" show all users to manage
		//if belongs to other group (regular admin) show only his data to change

		if($this->redux_auth->get_group($this->session->userdata('adminid')) == 'Root')
		{
			$result = $this->Admin->getAllAdmins();
			$this->mysmarty->assign('itemsList', $result);

			$this->mysmarty->assign('secondmenu', $this->secondMenu);
			$this->mysmarty->assign('activeSecondItem', 0);
			$this->mysmarty->display('admin/showAdmins.tpl');
		}
		else
		{
			redirect('adminsettings/edit/'.$this->session->userdata('adminid'));
		}
	}
	
	function edit()
	{

		if($this->redux_auth->get_group($this->session->userdata('adminid')) == 'Root' || $this->uri->segment(3) == $this->session->userdata('adminid'))
		{
			if(!$adminid = $this->uri->segment(3))
			{
				redirect('adminsettings');
			}
			else
			{
				//get admin data
				$this->mysmarty->assign('formLink', 'edit/'.$adminid);
				$result = $this->Admin->getAdmin($adminid);
				$aBackData['username']  = $result[0]->username;
				$aBackData['email']     = $result[0]->email;

				$this->mysmarty->assign('header', 'Edytuj konto <em>'.$aBackData['username'].'</em>');


				if($this->redux_auth->get_group($this->session->userdata('adminid')) == 'Root')
				{
					//set second menu edit item only for superuser
					$this->secondMenu[2]=array(
									"name"   => "Edytuj: [".$aBackData['username']."]",
									"link"   => "adminsettings/edit/".$adminid
								);

					$lastEdited = array(
									"name"   => $aBackData['username'],
									"id"     => $adminid
								);

					$this->session->set_userdata('lastEditedAdmin', $lastEdited);

					$this->mysmarty->assign('secondmenu', $this->secondMenu);
					$this->mysmarty->assign('activeSecondItem', 2);
				}

				//$this->load->library('form_validation');

				//validation rules
				if($this->input->post('email') != $aBackData['email'])
				{					
					$this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email|callback_check_email');
				}

				if($this->input->post('username') != $aBackData['username'])
				{					
					$this->form_validation->set_rules('username', 'Nazwa użytkownika', 'trim|strip_tags|required|callback_check_username');
				}

				$this->form_validation->set_rules('password', 'Hasło', 'trim|matches[password2]');
				$this->form_validation->set_rules('password2', 'Potwierdzenie hasła', 'trim');				

				if($this->form_validation->run()) // Validation Passed
				{
					$change = $this->redux_auth->changeData
					(
						$adminid,
						$this->input->post('username'),
						$this->input->post('password'),
						$this->input->post('email')
					);

					//var_dump($login);
					if(!$change)
					{
						//var_dump('assddd');
						$aErrors = array('Wystąpił błąd podczas dodawania danych do bazu. Proszę spróbować ponownie później.');
						$this->mysmarty->assign('errors', $aErrors);
					}
					else
					{
						$this->mysmarty->assign('header', 'Zmiany zostały wprowadzone.');
					}
					$aBackData['username'] = $this->input->post('username');
					$aBackData['email']    = $this->input->post('email');
					
					$lastEdited = array(
									"name"   => $aBackData['username'],
									"id"     => $adminid
								);
				}
				elseif(validation_errors() != '') // Validation Failed
				{
					$this->mysmarty->assign('errorHeader', 'Zmiany nie zostały wprowadzone.');

					//prepare for smarty as array
					$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));

					if(isset($aErrors[0]) && $aErrors[0]!='')
					{
						$this->mysmarty->assign('errors', $aErrors);
					}
					$aBackData['username'] = $this->input->post('username');
					$aBackData['email']    = $this->input->post('email');
				}

				$this->mysmarty->assign('backData', $aBackData);
				$this->mysmarty->display('admin/adminActions.tpl');
			}
		}
		else
		{
			redirect('adminsettings/edit/'.$this->session->userdata('adminid'));
		}
	}
	
	function addnew()
	{
		$this->mysmarty->assign('formLink', 'addnew');
		if($this->redux_auth->get_group($this->session->userdata('adminid')) == 'Root')
		{
			//set second menu
			$this->mysmarty->assign('secondmenu', $this->secondMenu);
			$this->mysmarty->assign('activeSecondItem', 1);

			
			
			$config = array(
					array(
							'field'   => 'username',
							'label'   => 'Nazwa użytkownika',
							'rules'   => 'trim|strip_tags|required|callback_check_username'
						),
					array(
							'field'   => 'email',
							'label'   => 'E-mail',
							'rules'   => 'trim|required|valid_email|callback_check_email'
						),						
					array(
							'field'   => 'password',
							'label'   => 'Hasło',
							'rules'   => 'trim|required|matches[password2]'
						),   
					array(
							'field'   => 'password2',
							'label'   => 'Potwierdź hasło',
							'rules'   => 'trim|required'
						)
				);

			$this->form_validation->set_rules($config);	

			//set header
			$this->mysmarty->assign('header', 'Dodaj nowego Administratora.');


			if($this->form_validation->run())
			{
				$register = $this->redux_auth->register
				(
					$this->input->post('username'),
					$this->input->post('password'),
					$this->input->post('email')
				);

				if($register)
				{
					$this->mysmarty->assign('header', 'Dane zostały poprawnie zapisane.');
				}

				$aBackData['username'] = $this->input->post('username');
				$aBackData['email']    = $this->input->post('email');
				$this->mysmarty->assign('backData', $aBackData);
			}
			elseif(validation_errors() != '') // Validation Failed
			{
				$this->mysmarty->assign('errorHeader', 'Dane nie zostały zapisane.');

				//prepare for smarty as array
				$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));

				if(isset($aErrors[0]) && $aErrors[0]!='')
				{
					$this->mysmarty->assign('errors', $aErrors);
				}

				$aBackData['username'] = $this->input->post('username');
				$aBackData['email']    = $this->input->post('email');
				$this->mysmarty->assign('backData', $aBackData);
			}
			$this->mysmarty->display('admin/adminActions.tpl');
		}
		else
		{
			redirect('adminsettings/edit/'.$this->session->userdata('adminid'));
		}
	}



	function removeadmin()
	{
		if($this->redux_auth->get_group($this->session->userdata('adminid')) == 'Root')
		{
			if(!$adminid = $this->uri->segment(3))
			{
				redirect('adminsettings');
			}
			else
			{
				if($this->session->userdata('adminid') != $adminid)
				{
					$result = $this->Admin->removeAdmin($adminid);
				}
				redirect('adminsettings');
			}
		}
		else
		{
			redirect('adminsettings/edit/'.$this->session->userdata('adminid'));
		}
	}

	/**
	* check_username
	*
	* @return bool
	* @author Mathew Davies
	**/

	function check_username($username)
	{
		if($this->redux_auth->check_username($username))
		{
			$this->form_validation->set_message('check_username', 'Nazwa użytkownika: '.$username.' jest niedostępna.');
			return false;
		}
		else
		{
			return true;
		}
	}

	/**
	* check_email
	*
	* @return bool
	* @author Mathew Davies
	**/

	function check_email($email)
	{
		if ($this->redux_auth->check_email($email))
		{
			$this->form_validation->set_message('check_email', 'E-mail: ' . $email . ' jest niedostępny.');
			return false;
		}
		else
		{
			return true;
		}
	}

	/*
	|--------------------------------------------------------------------------
	| Login
	|--------------------------------------------------------------------------
	|
	| Performs the login action.
	|
	| @param void
	| @return bool
	| @author Mathew Davies
	|
	*/

	function login()
	{
		if($this->redux_auth->logged_in())
		{
			redirect('');
			exit();
		}
		
		$config = array(               
					array(
							'field'   => 'email',
							'label'   => 'E-mail',
							'rules'   => 'trim|required|valid_email'
						),						
					array(
							'field'   => 'password',
							'label'   => 'Hasło',
							'rules'   => 'trim|required'
						)              
			);
		$this->form_validation->set_rules($config);	
		
		if($this->form_validation->run()) // Validation Passed
		{
			$login = $this->redux_auth->login
			(
				$this->input->post('email'),
				$this->input->post('password')
			);

			//var_dump($login);
			if(!$login)
			{
				//var_dump('assddd');
				$aErrors = array('Niepoprawne hasło lub e-mail. Proszę wpisać ponownie');
			}
			elseif($login === 'BANNED')
			{
				$aErrors = array('Konto jest zablokowane. skontaktuj się z administratorem.');
			}
			elseif($login === 'NOT_ACTIVATED')
			{
				$aErrors = array('Konto jest nieaktywne.');
			}
			else
			{				
				/*if($this->input->post('rememberme'))
				{				
					$this->load->helper('cookie');				
					$hash = $this->redux_auth->getUserHash($this->session->userdata('adminid'));
					
					$cookie = array(
	                   'name'   => 'logMeIn',
	                   'value'  => $hash,
	                   'expire' => '1209600'                   
	            );			
					set_cookie($cookie);				
				}*/	
				redirect('');
			}

			$this->mysmarty->assign('errors', $aErrors);
			$this->mysmarty->display('login.tpl');
		}
		else // Validation Failed
		{
			//prepare for smarty as array
			$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));
			
			$flash['logout'] = $this->session->flashdata('logout');
			$flash['status'] = $this->session->flashdata('status');

			if(isset($aErrors[0]) && $aErrors[0]!='')
			{
				$this->mysmarty->assign('errors', $aErrors);
				$this->mysmarty->assign('errors', $aErrors);
			}

			$this->mysmarty->display('login.tpl');
		}
	}	
	
	function forgottenPass()
	{
		$this->mysmarty->assign('forgottenPass', 1);
		$config = array(               
					array(
							'field'   => 'email',
							'label'   => 'E-mail',
							'rules'   => 'trim|required|valid_email|callback_check_if_email_exists'
						)       
			);
		$this->form_validation->set_rules($config);			
		
		
		if($this->form_validation->run()) // Validation Passed
		{
			if($this->_forgottenPassAction($this->input->post('email')))
			{
				$this->mysmarty->assign('forgottenPassSuccess', 1);
			}
		}
		else // Validation Failed
		{
			//prepare for smarty as array
			$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));
			
			$flash['logout'] = $this->session->flashdata('logout');
			$flash['status'] = $this->session->flashdata('status');

			if(isset($aErrors[0]) && $aErrors[0]!='')
			{
				$this->mysmarty->assign('errors1', $aErrors);
			}			
		}
		$this->mysmarty->display('login.tpl');	
	}
	
	

	
	function _forgottenPassAction($mail)
	{
	   $userInfo = $this->redux_auth->getUserByMail($mail);
		
		$userId = $userInfo[0]->id;
		$userHash = $userInfo[0]->hash;
		$mail = $userInfo[0]->email;
		
		$randomPasswd = rand(100000, 999999);	
		//$randomPasswd = 666;	//for test

		
		$tempPassword = $this->redux_auth->generatePassword($randomPasswd, $userHash);		
		
		
		$this->load->library('email');
	
		$config['protocol'] = 'mail';
		$config['mailtype'] = 'text';
		$this->email->initialize($config);
		
		
		$this->email->from('password-recovery@fastpr.pl', 'System odzyskiwania hasła - FastPR, panel administracyjny');
		
		//tests only
		//$mail = 'userreceiver@localhost.com';
		$this->email->to($mail);
		$this->email->subject('FastPR. Odzyskiwanie hasła w panelu administracyjnym.');

		$mailContent = "FastPR. Odzyskiwanie hasła w panelu administracyjnym.
--------------------------------------

Twoje nowe hasło tymczasowe:

".$randomPasswd."

Zaloguj się i zmień swoje hasło tymczasowe.


--------------------------------------
To jest automatycznie wygenerowana wiadomość.
Proszę na nią nie odpowiadać.";
//var_dump($mailContent);
		$this->email->message($mailContent);
		if(!$this->email->send())
		{
			//var_dump($this->email->print_debugger());
			//$this->UserModel->changePassword($userId, $tempPassword);
			return false;
		}
		else
		{
			$this->redux_auth->changePassword($userId, $tempPassword);
			return true;
		}
		/*var_dump($mailContent);
		var_dump($tempPassword);*/
	}
	
	function check_if_email_exists(&$input)
	{ 		
		if(!$this->redux_auth->checkIfUserExistsByMail($input))
		{
			$this->form_validation->set_message('check_if_email_exists', "Użytkownik z podanym adresem e-mail nie istnieje w bazie.");
			return false;
		}
		else
		{
			return true;
		}
	}
	
	/*
	|--------------------------------------------------------------------------
	| Logout
	|--------------------------------------------------------------------------
	|
	| Logout the visitor
	|
	| @param void
	| @return void
	| @author Mathew Davies
	|
	*/

	function logout()
	{
		$this->load->helper('cookie');
		delete_cookie('logMeIn');
		$this->redux_auth->logout(); // Destroy the session variables.
		redirect('adminsettings/login');
	}
}