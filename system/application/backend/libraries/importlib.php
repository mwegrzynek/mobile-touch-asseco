<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Importlib {

	function __construct()
	{
		$this->CI = & get_instance();
		$this->sectionType = 'items';
	}


	/*
	* Export funtions
	*
	*
	*/

	public function export($itemType, $lang, $fieldconfig, $name = '')
	{
		$filename = $this->sectionType.'_'.$itemType.'_'.$lang.'_'.date("Y-m-d").'.csv';
		header( 'Content-Type: text/csv' );
      header( 'Content-Disposition: attachment;filename='.$filename);
      $fp = fopen('php://output', 'w');

		$result = $this->getItems($itemType, $lang);

		// Name
		fputcsv($fp, array($name));

		// Headers
		fputcsv($fp, $this->getHeaders($fieldconfig));

		// Values
		foreach($result as $key => $value)
		{
			$val = array();
			$val  = get_object_vars($value);
			$val  = $this->filterResultByConfig($fieldconfig['fields'], $val);
			if($fieldconfig['meta'])
			{
				$meta = $this->filterResultByConfig($fieldconfig['meta'], $this->getMeta($itemType, $value->id, $lang));
			}
			else
			{
				$meta = array();
			}

			$val = array_merge($val, $meta);
			fputcsv($fp, $val);
		}
		fclose($fp);
		// ob_end_clean();
	}

	// --------------------------------------------------------------------

	private function getItems($itemType, $lang)
	{
		$CI = & get_instance();


		switch ($this->sectionType) {
			case 'items':
				$CI->ItemsModel->setDefaultLanguage($lang);
				$result = $CI->ItemsModel->getAllItems($itemType, 100000, 0, 'add_date', 'desc', array());
				break;

			case 'categories':
				$CI->CategoryModel->setDefaultLanguage($lang);
				$result = $CI->CategoryModel->getCategories($itemType, 100000, 0, 'name desc', array());
				break;

			case 'menus':
				$CI->MenuModel->setDefaultLanguage($lang);
				$result = $CI->MenuModel->getMenus($itemType, 100000, 0, 'name desc', array());
				break;

			default:
				die('Wrong section type provided');
				break;
		}
		return $result;
	}

	// --------------------------------------------------------------------

	private function getMeta($itemType, $itemId, $lang)
	{
		$CI = & get_instance();
		$CI->MetaDataModel->setType($itemType);
		$metaInfo = $CI->MetaDataModel->getMetaToFront($itemId, $lang, $lang);
		if(isset($metaInfo[0]))
		{
			return get_object_vars($metaInfo[0]);
		}
		return array();
	}

	// --------------------------------------------------------------------

	private function filterResultByConfig($config, $line)
	{
		$filtered = array();
		if($config)
		{
			foreach($config as $key => $value)
			{
				if(isset($line[$key]))
				{
					$filtered[] = str_replace(array("\n", "\n\n", "\n\n\n", "\n\n\n\n", "\n\n\n\n\n", "\n\n\n\n\n\n", "\n\n\n\n\n\n\n", "\n\n\n\n\n\n\n\n"), "\n", strip_tags($line[$key]));
				}
				else
				{
					$filtered[] = '';
				}
			}
		}
		return $filtered;
	}

	// --------------------------------------------------------------------

	private function getHeaders($fieldconfig)
	{
		$output = array();
		foreach((array)$fieldconfig['fields'] as $key => $value)
		{
			$output[] = $value;
		}

		foreach((array)$fieldconfig['meta'] as $key => $value)
		{
			$output[] = $value;
		}

		return $output;
	}




	/*
	* Import funtions
	*
	*
	*/

	public function import($itemType, $lang, $fieldconfig, $file)
	{
		// $fileName = "../temp/translate_notifications_en.csv";

		if(!$this->checkFile($file, $itemType))
		{
			return false;
		}

		$row = 0;
		if(($handle = fopen($file, "r")) !== FALSE)
		{
			while(($csvdata = fgetcsv($handle, 1000, ",")) !== FALSE)
			{
				$id = $csvdata[0];
				if($id !== '' && is_numeric($id) && $row > 1)
				{
					$data = $this->filterCSVFieldsByConfig($fieldconfig['fields'], $csvdata);
					$data['language_id'] = $lang;
					$this->updateData($id, $data);

					if($fieldconfig['meta'])
					{
						$meta = $this->getMetaFromCSV($fieldconfig, $csvdata);
						$this->addMeta($itemType, $id, $lang, $meta);
					}
				}
				$row++;
			}
			fclose($handle);
			return $row - 2;
		}
		else
		{
			die('Wrong file name');
		}
	}

	// --------------------------------------------------------------------

	private function filterCSVFieldsByConfig($config, $line, $counter = 0)
	{
		$data = array();
		if($config)
		{
			// $counter = 0;
			foreach($config as $key => $value)
			{
				$data[$key] = $line[$counter];
				$counter++;
			}
		}

		return $data;
	}

	// --------------------------------------------------------------------

	private function updateData($id, $data)
	{
		switch ($this->sectionType) {
			case 'items':
				$this->CI->ItemsModel->changeItem($data);
				break;

			case 'categories':
				$data['category_id'] = $id;
				$this->CI->CategoryModel->changeCategory($data);
				break;

			case 'menus':
				$data['menu_id'] = $id;
				$this->CI->MenuModel->changeMenu($data);
				break;

			default:
				die('Wrong section type provided');
				break;
		}
	}

	// --------------------------------------------------------------------

	private function getMetaFromCSV($fieldconfig, $csvdata)
	{
		$counter = count($fieldconfig['fields']);
		$data = $this->filterCSVFieldsByConfig($fieldconfig['meta'], $csvdata, $counter);
		return $data;
	}

	// --------------------------------------------------------------------

	private function addMeta($type, $id, $lang, $data)
	{
		$this->CI->MetaDataModel->setType($type);
		$this->CI->MetaDataModel->addMetaData($id, $lang, $data);
	}

	// --------------------------------------------------------------------

	public function getSectionType()
	{
		return $this->sectionType;
	}

	// --------------------------------------------------------------------

	public function setSectionType($type)
	{
		$this->sectionType = $type;
		return true;
	}

	// --------------------------------------------------------------------

	private function checkFile($file, $itemType)
	{
		$path_parts = pathinfo($file);
		$nameArray = explode('_', $path_parts['filename']);

		if(isset($nameArray[0]) && isset($nameArray[1]) && $nameArray[0] == $this->sectionType && $nameArray[1] == $itemType)
		{
			return true;
		}
		return false;
	}


	/**
	 * Upload file
	 *
	 * @access	public
	 * @return	array
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2012, Maciej Węgrzynek
	 */
	function doUpload()
	{
		//file save
		$config['upload_path']     = '../temp';
		$config['allowed_types']   = 'csv';
		$config['max_size']	      = '2097152';
		$config['overwrite']	      = FALSE;
		$config['remove_spaces']   = TRUE;

		$this->CI->load->library('upload', $config);
		$field_name = "thefile";

		if(!$this->CI->upload->do_upload($field_name))
		{
			$warnings = $this->CI->upload->display_errors('', '###');
			$aErrors = explode("###", substr(trim($warnings), 0, -3));
			return array(false, $aErrors);
		}
		else
		{
			$uploadData = $this->CI->upload->data();
			$fileName = $uploadData['file_name'];
			return array(true, array($uploadData['file_name'], $uploadData['full_path']));
		}
	}
}