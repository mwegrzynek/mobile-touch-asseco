<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Itemslib {

	function __construct()
	{
		$this->CI = & get_instance();		
	}
	
	/**
	 * Update multicategories for added / edited item
	 *
	 * @access	public
	 * @param	$categories (array)
	 * @return	bool
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2012, Maciej Węgrzynek
	 */
	function updateCategories($itemId, $categories)
	{
		//categories
		if(isset($categories[0])) 
		{
			foreach($categories as $key => $value) 
			{
				if($value < 1) 
				{
					unset($categories[$key]);
				}
			}
			$categories = array_unique($categories);
		}
		$this->CI->ItemsModel->updateItemCategories($itemId, $categories);
		
		return true;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Add information to all languages at once
	 *
	 * @access	public
	 * @param	$data (array), $itemId (integer), $itemConfig (array)
	 * @return	bool
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2012, Maciej Węgrzynek
	 */
	function addToAllLanguages($data, $itemId, $itemConfig, $metaData)
	{
		$langData = $this->CI->ItemsModel->unsetTableData($this->CI->ItemsModel->getTableFields(), $data);
		$langData['id'] = $itemId;					
		
		$languages = $this->CI->LanguageModel->getAllLanguages();		
		
		if(count($languages) > 1) 
		{
			foreach($languages as $key => $value) 
			{
				if(($value->id != $this->CI->defaultLangId))
				{
					$langData['slug']        = $this->CI->ItemsModel->makeSlug($langData['title'], $value->id, $itemConfig['itemType']);
					$langData['language_id'] = $value->id;
					$this->CI->ItemsModel->changeItem($langData);
				}				
				
				if($itemId && ($itemConfig['metaDataType'] != -1))
				{
					$this->CI->MetaDataModel->addMetaData($itemId, $value->id, $metaData);								
				}
			}			
		}
		
		return true;
	}
	
	// --------------------------------------------------------------------
	
	
	/**
	 * Upload Action, make thumbnails according to item config
	 *
	 * @access	public
	 * @param	$itemConfig (array), $currentFileName (string)
	 * @return	array (void)
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2012, Maciej Węgrzynek
	 */
	function uploadFile($itemConfig, $currentFileName = '')
	{
		$fileName = $currentFileName;
		$errors = array();
				
		if(isset($_FILES['thefile']['name']) && $_FILES['thefile']['name'] != '')
		{			
			$files['thefile'] = array($fileName);
			
			//file save
			$config['upload_path']     = $itemConfig['mainPicturePath'];
			$config['allowed_types']   = $itemConfig['allowedTypes'];
			$config['max_size']	      = '2097152';
			$config['overwrite']	      = FALSE;
			$config['max_width']       = $itemConfig['mainPictureMaxWidth'];
			$config['max_height']      = $itemConfig['mainPictureMaxHeight'];
			$config['min_width']       = $itemConfig['mainPictureMinWidth'];
			$config['min_height']      = $itemConfig['mainPictureMinHeight'];
			$config['remove_spaces']   = TRUE;
			
			$uploadResult = $this->doUpload($config);
			
			if($uploadResult[0])
			{
				//get uploaded file path
				$fileName = $uploadResult[1][0];
				
				if(isset($itemConfig['mainPictureThumbsInfo'][0])) 
				{
					$thumbsConfig['file_name']     = $fileName;
					$thumbsConfig['upload_path']   = $itemConfig['mainPicturePath'];
					$thumbsConfig['thumbs_info']   = $itemConfig['mainPictureThumbsInfo'];
					$thumbsConfig['old_file_name'] = $files['thefile'][0];
					
					$this->CI->load->library('picturesactionslib');
					$this->CI->picturesactionslib->makeThumbs($thumbsConfig);
					
					//when editing item
					if($currentFileName != '') 
					{
						$this->CI->picturesactionslib->removeThumbs($thumbsConfig);
					}
				}				
			}
			else
			{
				$errors = $uploadResult[1];
			}
		}
		elseif((isset($_FILES['thefile']['name']) && $_FILES['thefile']['name'] != '') && $itemConfig['mainPictureMandatory'])
		{
			$errors[0] = $itemConfig['mainPictureMandatoryError'];
		}
		
		return array(
				'errors'   => $errors,
				'fileName' => $fileName
			);
	}	
	
	// --------------------------------------------------------------------
	
	/**
	 * Upload file and resize original image if needed
	 *
	 * @access	public
	 * @param	$config (array), $ifResize (integer)
	 * @return	array
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2012, Maciej Węgrzynek
	 */
	function doUpload($config)
	{
		$this->CI->load->library('upload', $config);
		$field_name = "thefile";

		if(!$this->CI->upload->do_upload($field_name))
		{
			$warnings = $this->CI->upload->display_errors('', '###');
			$aErrors = explode("###", substr(trim($warnings), 0, -3));
			return array(false, $aErrors);
		}
		else
		{
			$uploadData = $this->CI->upload->data();
			$fileName = $uploadData['file_name'];			
			return array(true, array($uploadData['file_name'], $uploadData['full_path']));
		}
	}
	
	// --------------------------------------------------------------------
	
}
