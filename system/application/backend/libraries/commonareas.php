<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Commonareas {

	function __construct()
	{
		$this->CI = & get_instance();		
	}
		
	function getNotificationTexts()
	{
		$result = $this->CI->ItemsModel->getAllItems(14, 1000, 0, 'title', 'asc');
		foreach ($result as $key => $value) 
		{			
			$idsResult[$value->id] = $value;
		}
		
		//$this->CI->mysmarty->assign('siteTexts', $idsResult);		
		
		return $idsResult;
	}
	
	
	function getSiteTexts()
	{
		$result = $this->CI->ItemsModel->getAllItems(12, 1000, 0, 'title', 'asc');
		foreach ($result as $key => $value) 
		{			
			$idsResult[$value->id] = $value;
		}
		return $idsResult;
	}
}