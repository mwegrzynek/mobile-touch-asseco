<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Langlib {

	function __construct()
	{
		$this->CI = & get_instance();
	}

	// --------------------------------------------------------------------

	/**
	 * Upload Action
	 *
	 * @access	public
	 * @param	$itemConfig (array), $currentFileName (string)
	 * @return	array (void)
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2012, Maciej Węgrzynek
	 */
	function uploadFile($fileName)
	{
		$errors = array();
		$fileName = $fileName.'.png';

		if(isset($_FILES['thefile']['name']) && $_FILES['thefile']['name'] != '')
		{
			//$files['thefile'] = array($fileName);
var_dump($fileName);
			//file save
			$config['upload_path']     = '../userfiles/languages/';
			$config['allowed_types']   = 'png';
			$config['max_size']	      = '2097152';
			$config['file_name']	      = $fileName;
			$config['overwrite']	      = TRUE;
			$config['max_width']       = 14;
			$config['max_height']      = 11;
			$config['min_width']       = 14;
			$config['min_height']      = 11;
			// $config['remove_spaces']   = TRUE;

			$uploadResult = $this->doUpload($config);

			if($uploadResult[0])
			{

			}
			else
			{
				$errors = $uploadResult[1];
			}
		}
		elseif((isset($_FILES['thefile']['name']) && $_FILES['thefile']['name'] != ''))
		{
			$errors[0] = '';
		}

		return array(
				'errors'   => $errors,
				'fileName' => $fileName
			);
	}

	// --------------------------------------------------------------------

	/**
	 * Upload file and resize original image if needed
	 *
	 * @access	public
	 * @param	$config (array), $ifResize (integer)
	 * @return	array
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2012, Maciej Węgrzynek
	 */
	function doUpload($config)
	{
		$this->CI->load->library('upload', $config);
		$field_name = "thefile";

		if(!$this->CI->upload->do_upload($field_name))
		{
			$warnings = $this->CI->upload->display_errors('', '###');
			$aErrors = explode("###", substr(trim($warnings), 0, -3));
			return array(false, $aErrors);
		}
		else
		{
			$uploadData = $this->CI->upload->data();
			$fileName = $uploadData['file_name'];
			return array(true, array($uploadData['file_name'], $uploadData['full_path']));
		}
	}

	// --------------------------------------------------------------------

}

/* End of file langlib.php */
/* Location: ./application/backend/libraries/langlib.php */
