<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Picturesactionslib {

	function __construct()
	{
		$this->CI = & get_instance();		
	}
	
	
	// --------------------------------------------------------------------
	
		
	function makeThumbs($config)
	{
		$this->CI->load->model('../../models/PicturesModel');
		$this->CI->load->library('zebraimage');			
		
		
		$this->CI->zebraimage->source_path = $config['upload_path'].'/'.$config['file_name'];	
		$this->CI->zebraimage->jpeg_quality = 90;
		
		$this->CI->load->library('image_lib');	
		if(isset($config['thumbs_info'][0])) 
		{
			
			foreach($config['thumbs_info'] as $key => $value) 
			{
				$thumbFileName = $this->CI->PicturesModel->makeThumbName($config['file_name'], $key);				
				$this->CI->zebraimage->target_path = $config['upload_path'].'/'.$thumbFileName;				
		
				if($value['width'] == 0 || $value['height'] == 0) 
				{					
					$resizeConfig = array(
						'source_image'   => $config['upload_path'].'/'.$config['file_name'],
						'width'          => $value['width'],						
						'height'         => 400,						
						'new_image'      => $config['upload_path'].'/'.$thumbFileName,
						'maintain_ratio' => TRUE,
						'master_dim'  	  => 'width'
					);					
					
					$this->CI->image_lib->initialize($resizeConfig);
					if(!$this->CI->image_lib->resize())
					{
						var_dump($resizeConfig);
						echo $this->CI->image_lib->display_errors();
					}					
					
					if(isset($value['grey']) && $value['grey'] == true) 
					{						
						$this->desaturate($config['upload_path'].'/'.$thumbFileName);
					}
					
				}
				else
				{
						
					if (!$this->CI->zebraimage->resize($value['width'], $value['height'], ZEBRA_IMAGE_CROP_TOPCENTER)) 
					{
						// if there was an error, let's see what the error is about
						switch ($this->CI->zebraimage->error) 
						{
							case 1:
								$error = 'Source file could not be found!';
							break;
							case 2:
								$error = 'Source file is not readable!';
							break;
							case 3:
								$error = 'Could not write target file!';
							break;
							case 4:
								$error = 'Unsupported source file format!';
							break;
							case 5:
								$error = 'Unsupported target file format!';
							break;
							case 6:
								$error = 'GD library version does not support target file format!';
							break;
							case 7:
								$error = 'GD library is not installed!';
							break;						
						}						
						return $error;
					} 
					else 
					{
						
					}
				}	
			}
			return false;
		}
	}
	
	
	// --------------------------------------------------------------------
	
	
	function removeThumbs($config)
	{
		$this->removeFile($config['upload_path'].'/'.$config['old_file_name']);
		if(isset($config['thumbs_info'][0])) 
		{
			foreach($config['thumbs_info'] as $key => $value) 
			{
				$thumbFileName = $this->CI->PicturesModel->makeThumbName($config['old_file_name'], $key);
				$this->removeFile($config['upload_path'].'/'.$thumbFileName);
			}
		}		
	}
	
	// --------------------------------------------------------------------	
	
	function desaturate($imgSrc)
	{
		$image = imagecreatefrompng($imgSrc);
		imagealphablending( $image, false );
		imagesavealpha( $image, true );

		imagefilter($image, IMG_FILTER_GRAYSCALE);
		imagepng($image, $imgSrc);
		imagedestroy($image);
	}	
	
	// --------------------------------------------------------------------
		
	
	private function removeFile($filePath)
	{
		if(is_file($filePath)) 
		{
			unlink($filePath);
		}
	}	
}
