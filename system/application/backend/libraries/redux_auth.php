<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

	/**
	 * redux_auth
	 *
	 * @author Mathew Davies <leveldesign.info@gmail.com>
	 * @copyright Copyright (c) 1 June 2008, Mathew Davies
	 * @license http://www.opensource.org/licenses/mit-license.php The MIT License
	 * @version 1.3
	 * @since 1.0
	**/
	class redux_auth extends redux_auth_db
	{
		/**
		 * __construct
		 *
		 * @access public
		 * @param void
		 * @return void
		**/
		function __construct()
		{
			$this->ci =& get_instance();
	
			$this->ci->config->load('redux_auth');
			$auth = $this->ci->config->item('auth');
			
			foreach($auth as $key => $value)
			{
				$this->$key = $value;
			}
	
			$libraries = array('session','email');
			
			foreach ($libraries as $key) 
			{
				$this->ci->load->library($key);
			}
	
			$this->ci->email->initialize($this->mail);
		}

		/**
		 * register
		 *
		 * @access public
		 * @param string $username Username
		 * @param string $password Password
		 * @param string $email Valid email address
		 * @param string $question Secret question
		 * @param string $answer Answer to secret question
		 * @return mixed
		**/
		public function register ($username, $password, $email, $group_id = 2,  $question='', $answer='')
		{
			# Hash secret answer
			//$answer = sha1($this->salt.$answer); 
			
			# Insert question and answer into the questions table
			//$this->ci->db->insert($this->questions_table, array('question' => $question, 'answer' => $answer)); 
			
			# Retrieve the question_id
			//$question_id = $this->ci->db->insert_id();
			
			# Generate dynamic salt
			$hash = sha1(microtime()); 
			
			# Insert the salt into database
			$this->ci->db->set('hash', $hash); 
			
			# Hash password
			$password_enc = sha1($this->salt.$hash.$password);
			
			$data = array
			(
				'username' 	=> $username,
				'password' 	=> $password_enc,
				'email'    	=> $email,
				'group_id'	=> $group_id,
				'question_id' => 0
			);
	
			$this->ci->db->set($data);
			
			/**
			 * Optional Columns Start
			**/
			if (!empty($this->optional_columns))
			{
				foreach($this->optional_columns as $key)
				{
					if (!$this->ci->input->post($key))
					{
						$optional[$key] = null;
					}
					else
					{
						$optional[$key] = $this->ci->input->post($key);
					}
				}
	
				$this->ci->db->set($optional);
			}
			/**
			 * Optional Columns Finish
			**/
	
			if (!$this->email_activation)
			{
				# Insert information into the users table
            $this->ci->db->set('activation_code', 1);
				$this->ci->db->insert($this->users_table);
	
				# Automatic login
				//$this->login($email, $password);
				
				return 'REGISTRATION_SUCCESS';
			}
	
			else
			{
				# Generate activation code
				$key = sha1(microtime());
	
				$this->ci->db->set('activation_code', $key);
				$this->ci->db->insert($this->users_table);
	
				$this->ci->email->from($this->mail_from_email, $this->mail_from_name);
				$this->ci->email->to($email);
				$this->ci->email->subject($this->email_activation_subject);
				$this->ci->email->message($this->email_activation_message);
				$this->ci->email->send();
				
				return 'REGISTRATION_SUCCESS_EMAIL';
			}	
	
			return false;
		}
		
		
      public function generatePassword($passwd,$hash)
		{
			$password_enc = sha1($this->salt.$hash.$passwd);
			return $password_enc;
		}   		
		
		function getUserByMail($mail)
      {
         $query = $this->ci->db->get_where($this->users_table, array('email' => $mail));
         return $query->result();
      }	
		
		
		function getUserHash($id)
      {
         $query = $this->ci->db->select('hash')->where($this->users_table.'.id', $id)->limit(1)->get($this->users_table);
         $result = $query->result();

         if(count($result))
         {
            return $result[0]->hash;
         }
         return false;
      }
		
		function getUserByHash($hash)
      {
         $query = $this->ci->db->get_where($this->users_table, array('hash' => $hash));
         return $query->result();
      }
		
		function checkIfUserExistsByMail($mail)
		{
			$this->ci->db->from($this->users_table);
			$this->ci->db->where('email', $mail);				
			return $this->ci->db->count_all_results();
		}


		function changePassword($id, $passwd)
      {
         $data = array(               
               'password'  => $passwd         
            );
            
         $this->ci->db->where('id', $id);
         $this->ci->db->update($this->users_table, $data);
         return true;
      }
		

		public function changeData($id, $username, $password, $email)
      {
         if($password != '')
         {
            $hash = sha1(microtime()); # New hash
            $password = sha1($this->salt.$hash.$password); # New hashed password
            $this->ci->db->set('password', $password);
            $this->ci->db->set('hash', $hash);
         }
          
         
         $data = array(
               'username'  => $username,               
               'email'     => $email               
            );

         $this->ci->db->where('id', $id);
         $this->ci->db->update($this->users_table, $data);
         
         return true;
      }
		
		/**
		 * login
		 *
		 * @access public
		 * @param string $email Vaild email address
		 * @param string $password Password
		 * @return mixed
		**/
		public function login ($email, $password)
		{
			# Grab hash, password, id, activation_code and banned_id from database.
			$result = $this->_login($this->users_table, $this->banned_table, $email);
   
			if($result)
			{	
				if(empty($result->activation_code))
				{					
               $this->ci->session->set_flashdata('login', 'Your Account is Not Activated.');
					$this->logout();
					return 'NOT_ACTIVATED';
				}
				elseif(!empty($result->banned_id))
				{
					$this->ci->session->set_flashdata('login', 'Your Account has been banned for the following reason : '.$result->reason);
					$this->logout();
					return 'BANNED';
				}
				else
				{
					$password = sha1($this->salt.$result->hash.$password); 
					
					//echo $password .'<br>'. $result->password;
	
					if ($password === $result->password)
					{
						$this->ci->session->set_userdata(array('adminid'=> $result->id));	
						return true;
					}
				}
			}
			
			return false;
		}
				
		
		function checkIfUserExists($userid)
		{   
         $query = $this->ci->db->from($this->users_table)->where('id', $userid)->limit(1)->get();         
         return $this->ci->db->count_all_results(); 
		}
		
		
		/**
		 * logged_in
		 *
		 * @access public
		 * @param void
		 * @return bool
		**/
		public function logged_in()
		{
			return $var = ($this->ci->session->userdata('adminid')) ? true : false; 
		}

		/**
		 * logout
		 *
		 * @access public
		 * @param void
		 * @return void
		**/
		public function logout ()
		{
			$this->ci->session->unset_userdata('adminid');
			$this->ci->session->sess_destroy();
		}

		/**
		 * activate
		 *
		 * @access public
		 * @param string $activation_code
		 * @return bool
		**/
		public function activate ($activation_code)
		{
			$activate = $this->_activate($this->users_table, $activation_code);
		
			return $var = ($activate) ? true : false;
		}
		
		/**
		 * change_password
		 *
		 * @access public
		 * @param
		 * @return
		**/
		
		
	 	/**
	 	 * forgotten_begin
	 	 *
	 	 * @access public
	 	 * @param string $email Valid email address
	 	 * @return bool
	 	**/
		public function forgotten_begin ($email)
		{
			if($this->_check_forgotten_code($this->users_table, $email))
			{
				# Verification code
				$key = sha1(microtime()); 
				
				# Inserts verification code into the users table
				$this->_insert_forgotten_code($this->users_table, $key, $email); 
	
				# Replace {key} with verification code
				$message = preg_replace('/({key})/', $key, $this->forgotten_password_message);
				
				$this->ci->email->from($this->mail_from_email, $this->mail_from_name);
				$this->ci->email->to($email);
				$this->ci->email->subject($this->forgotten_password_subject);	
				$this->ci->email->message($message);
				$this->ci->email->send();
				
				return true;
			}
	
			return false;
		}
		
		/**
		 * forgotten_process
		 *
		 * @access public
		 * @param string $key
		 * @return void
		**/
		public function forgotten_process($key)
		{
			# Select question_id and question
			$question = $this->_select_question($this->users_table, $this->questions_table, $key);
				
			$data = array
			(
				'question_id' => $question->id
			);
			
			$this->ci->session->set_userdata($data);
				
			# Remove the forgotten code from the users table
			$this->_remove_forgotten_code($this->users_table, $key); 
			
			# Return the question to be used in a template
			return $data = array('question' => $question->question);
		}	
		
		/**
		 * forgotten_end
		 *
		 * @access public
		 * @param string $answer Secret question answer
		 * @return bool
		**/
		public function forgotten_end ($answer)
		{
			# Retrieve question_id
			$question_id = $this->ci->session->userdata('question_id'); 
			
			# Destroy question_id
			$this->ci->session->unset_userdata('question_id'); 
			
			# Retrieve questions answer
			$dbanswer = $this->_select_answer($question_id); 
	
			if($dbanswer->answer === sha1($this->salt.$answer))
			{
				# New password
				$password = substr(sha1(microtime()), 0, 10); 
				
				# Insert new password
				$this->_insert_new_password($this->users_table, $password, $question_id); 
	
				# Replace {password} with new password
				$message = preg_replace('/({password})/', $password, $this->new_password_message); 
				
				$this->ci->email->from($this->mail_from_email, $this->mail_from_name); 
				$this->ci->email->to($dbanswer->email);
				$this->ci->email->subject($this->new_password_subject);
				$this->ci->email->message($message);
				$this->ci->email->send();
	
				return true;
			}
	
			return false;
		}
		
		/**
		 * get_group
		 *
		 * @access public
		 * @param string $id Users id
		 * @return string
		**/
		public function get_group($id){return $this->_get_group($this->groups_table, $this->users_table, $id);}
		
		/**
		 * check_username
		 *
		 * @access public
		 * @param string $username Username
		 * @return bool
		**/
		public function check_username($username){return $this->_check_username($this->users_table, $username);}
		
		/**
		 * check_email
		 *
		 * @access public
		 * @param string $email Valid email address
		 * @return bool
		**/
		public function check_email($email){return $this->_check_email($this->users_table, $email);}
      
      
      public function get_username($id)
      {        
         $i = $this->ci->db->select('username')->from($this->users_table)->where('id', $id)->limit(1)->get();
         $row = $i->row();                                          
         return $row->username;
      } 
	
	}
	
	/**
	 * redux_auth_db
	 *
	 * @author Mathew Davies <leveldesign.info@gmail.com>
	 * @copyright Copyright (c) 1 June 2008, Mathew Davies
	 * @license http://www.opensource.org/licenses/mit-license.php The MIT License
	 * @version 1.3
	 * @since 1.0
	**/
	class redux_auth_db
	{
		
		/**
		 * _activate
		 *
		 * @access protected
		 * @param string $users Users table
		 * @param string $code Email activation code
		 * @return bool
		**/
		protected function _activate ($users, $code)
		{
			$this->ci->db->where($users.'.activation_code', $code)->update($users, array($users.'.activation_code' => 0));
		
			return $var = ($this->ci->db->affected_rows() > 0) ? true : false;
		}

		/**
		 * _insert_new_password
		 *
		 * @access protected
		 * @param string $users Users table
		 * @param string $password New password
		 * @param integer $question_id Question id
		 * @return bool/mixed/array/resource/void
		**/
		protected function _insert_new_password($users, $password, $question_id)
		{
	
			$hash = sha1(microtime()); # New hash

			$password = sha1($this->salt.$hash.$password); # New hashed password

			$data = array(
	           $users.'.forgotten_password_code' => '0',
	           $users.'.password' => $password,
	           $users.'.hash' => $hash
	        );
	
			$this->ci->db->where($users.'.question_id', $question_id)->update($users, $data); 
			
			return $var = ($this->ci->db->affected_rows() > 0) ? true : false;
		}

		/**
		 * _insert_forgotten_code
		 *
		 * @access protected
		 * @param string $users Users table
		 * @param string $key Email verification code
		 * @param string $email Users email address
		 * @return bool
		**/
		protected function _insert_forgotten_code($users, $key, $email)
		{
			$this->ci->db->where($users.'.email', $email)->update($users, array('forgotten_password_code' => $key)); 
			
			return $var = ($this->ci->db->affected_rows() > 0) ? true : false;
		}

		/**
		 * _check_forgotten_code
		 *
		 * @access protected
		 * @param string $users Users table
		 * @param string $email Valid email address
		 * @return bool
		**/
		protected function _check_forgotten_code($users, $email)
		{
		 	$i = $this->ci->db->select($users.'.forgotten_password_code')->from($users)->where($users.'.email', $email)->get();
		 	
		 	return $var = ($i->num_rows() > 0) ? true : false;
		}

		/**
		 * _remove_forgotten_code
		 *
		 * @access protected
		 * @param string $users Users table
		 * @param string $key Email verification code
		 * @return bool
		**/
		protected function _remove_forgotten_code($users, $key)
		{
		 	$this->ci->db->where($users.'.forgotten_password_code', $key)->update($users, array($users.'.forgotten_password_code' => 0));
		
			return $var = ($this->ci->db->affected_rows() > 0) ? true : false;
		}
		
		/**
		 * _check_username
		 *
		 * @access protected
		 * @param string $users Users table
		 * @param string $username Username
		 * @return bool
		**/
		protected function _check_username ($users, $username)
		{
			$i = $this->ci->db->select($users.'.username')->from($users)->where($users.'.username', $username)->get();
			
			return $var = ($i->num_rows() > 0) ? true : false;
		}
		
		/**
		 * _check_email
		 *
		 * @access protected
		 * @param string $users Users table
		 * @param string $email Valid email address
		 * @return bool
		**/
		protected function _check_email ($users, $email)
		{
			$i = $this->ci->db->select($users.'.email')->from($users)->where($users.'.email', $email)->get();
			
			return $var = ($i->num_rows() > 0) ? true : false;
		}
		
		/**
		 * _get_group
		 *
		 * @access protected
		 * @param void
		 * @return bool/mixed/array/resource/void
		**/
		protected function _get_group ($groups, $users, $where)
		{
			$i = $this->ci->db->select($groups.'.title')
			->from($groups)
			->join($users, $groups.'.id = '.$users.'.group_id', 'left')
			->where($users.'.id', $where)
			->limit(1)
			->get();
	
			return $var = ($i->num_rows() > 0) ? $i->row()->title : false;
		}
		
		/**
		 * _select_question
		 *
		 * @access protected
		 * @param string $users Users table
		 * @param string $questions Questions table
		 * @param string $forgotten_password_code Forgotten password code
		 * @return bool/mixed/array/resource/void
		**/
		protected function _select_question ($users, $questions, $forgotten_password_code)
		{
			$i = $this->ci->db->select($users.'.question_id, '.
									   $questions.'.question')
			->from($users)
			->join($questions, $users.'.question_id = '.$questions.'.id', 'left')
			->where($users.'.forgotten_password_code', $forgotten_password_code)
			->limit(1)
			->get();
			
			return $var = ($i->num_rows() > 0) ? $i->row() : false;
		}
		
		/**
		 * _select_answer
		 *
		 * @access protected
		 * @param string $questions Questions table
		 * @param integer $id Questions id
		 * @return mixed
		**/
		protected function _select_answer ($questions, $id)
		{
			$i = $this->ci->db->select($questions.'.answer')
			->from($questions)
			->where($questions.'.id', $id)
			->limit(1)
			->get();
			
			return $var = ($i->num_rows() > 0) ? $i->row() : false;
		}
		
		/**
		 * _login
		 *
		 * @access protected
		 * @param string $users Users table
		 * @param string $banned Banned table
		 * @param string $email Valid email address
		 * @return mixed
		**/
		protected function _login ($users, $banned, $email)
		{			
         $i = $this->ci->db->select($users.'.password, '.
									   $users.'.hash, '.
									   $users.'.id, '.
									   $users.'.activation_code, '.
									   $banned.'.reason')	
			->from($users)
			->join($banned, $users.'.banned_id = '.$banned.'.id', 'left')
			->where($users.'.email', $email)
			->limit(1)
			->get();
	
			return $var = ($i->num_rows() > 0) ? $i->row() : false;
		}
	}