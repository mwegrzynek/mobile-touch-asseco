<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Getdatalib {

	function __construct()
	{
		$this->CI = & get_instance();		
	}
	
	
	// --------------------------------------------------------------------
	
	
	function fillTableDataWithPost($fieldsTable, $sub = false)
	{
		$CI = & get_instance();	
		if(isset($fieldsTable[0])) 
		{			
			$allPost = $CI->input->post();
			if($sub) 
			{				
				$postFields = $this->getSubBranch($allPost, $sub);
			}
			else
			{
				$postFields = $allPost;
			}			
			
			foreach($fieldsTable as $key => $value) 
			{
				if(!isset($postFields[$value])) 
				{
					$val = '';
				}
				else
				{
					$val = $postFields[$value];	
				}
				$output[$value] = $val;
			}
		}
		return $output;
	}
	
	
	// --------------------------------------------------------------------
	
	
	private function getSubBranch($array, $subname)
	{	
		
		if(isset($array[$subname])) 
		{
			return $array[$subname];
		}
		
		return false;
	}	
	
	
	// --------------------------------------------------------------------
	
	
	function fillTableData($tableFields, $data)
	{
		if(isset($tableFields[0])) 
		{
			foreach($tableFields as $key => $value) 
			{
				if(!isset($data[$value])) 
				{
					$data[$value] = '';
				}
				$output[$value] = $data[$value];
			}
			return $output;
		}
		return false;
	}
	
}