<?
   class Admin extends CI_Model 
   {
      function __construct()
      {
         parent::__construct();
      }
      
      function getAllAdmins()
      {
         
         $query = $this->db->select(
                              'admin.id, '.
									   'admin.username, '.
									   'admin.email, '.
									   'admin_groups.title as group_title'
									   )	
			->from('admin')
			->join('admin_groups', 'admin.group_id = admin_groups.id', 'left')			
			->get();
         
         return $query->result();
      }
      
      
      function getAdmin($id)
      {         
         $query = $this->db->from('admin')->where('id', $id)->limit(1)->get();         
         return $query->result();
      }
      
      
      function removeAdmin($id)
      {
         $tables = array('admin');
         $this->db->where('id', $id);
         $this->db->delete($tables);
      }
     
   }