<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*
 * mainmenu
 *
 *
 *
*/

//general settings
$config['mainmenu']['itemType'] = 1;
$config['mainmenu']['mainName'] = 'mainmenu';
$config['mainmenu']['activeMenuItem'] = 1;
$config['mainmenu']['mainLink'] 		= 'menu/mainmenu';
$config['mainmenu']['models'] 		= null;
$config['mainmenu']['ifFiltered'] = TRUE;
$config['mainmenu']['secondMenu'] = array(
											0 => array(	"name"   => "See all",
															"link"   => "menu/mainmenu"),
														
											1 => array(	"name"   => "Add item",
															"link"   => "menu/mainmenu/addnew")
										);
$config['mainmenu']['menuLastEditedVarName'] 	= 'lastEditedMainMenu';
$config['mainmenu']['menuLastEditedText'] 		= 'Last edited item:';
$config['mainmenu']['menuLastEditedLink'] 		= 'menu/mainmenu/edit/';
$config['mainmenu']['metaDataType'] = -1;
$config['mainmenu']['nestedNodes'] = TRUE;

//extended field [filedname => default]
$config['mainmenu']['extendedFields'] = null;

//main picture settings
$config['mainmenu']['allowedTypes'] 					= 'jpg|png';
$config['mainmenu']['mainPicture'] 						= 0;
$config['mainmenu']['mainPictureMandatory'] 			= 0;
$config['mainmenu']['mainPictureMandatoryError'] 	= 'Field <strong>attachment</strong> is mandatory.';
$config['mainmenu']['mainPicturePath'] 				= '../pictures/menus/mainmenu';
$config['mainmenu']['mainPictureMaxWidth'] 			= 0;
$config['mainmenu']['mainPictureMaxHeight'] 			= 0;
$config['mainmenu']['mainPictureMinWidth'] 			= 0;
$config['mainmenu']['mainPictureMinWidth'] 			= 0;
$config['mainmenu']['mainPictureThumbWidth'] 		= 0;


//index page
$config['mainmenu']['indexAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['mainmenu']['indexAdditional']['js'] 		= null;
$config['mainmenu']['indexAdditional']['outerjs'] 	= null;
$config['mainmenu']['indexItemsOrder'] = 'position asc';
$config['mainmenu']['indexTemplate'] = 'menus/mainmenu/showAll.tpl';

//addnew page
$config['mainmenu']['addNewAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['mainmenu']['addNewAdditional']['js'] 		= array('tiny.init.js');
$config['mainmenu']['addNewAdditional']['outerjs'] = array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																			));
$config['mainmenu']['addNewTemplate'] 					= 'menus/mainmenu/addAction.tpl';
$config['mainmenu']['addNewDefaultHeader'] 			= 'Add item';
$config['mainmenu']['addNewDefaultSubHeader'] 		= '';
$config['mainmenu']['addNewSuccessHeader'] 			= 'Data has been successfully saved';
$config['mainmenu']['addNewSuccessSubHeader'] 		= '';
$config['mainmenu']['addNewErrorHeader'] 				= 'Data has not been saved';
$config['mainmenu']['addNewErrorSubHeader'] 			= '';
$config['mainmenu']['addNewValidationConfig']	= array(
																		array(
																				'field'   => 'menuName',
																				'label'   => 'Item name',
																				'rules'   => 'trim|strip_tags|required'
																			),
																		array(
																				'field'   => 'position',
																				'label'   => 'Position',
																				'rules'   => 'trim|required|numeric'
																			),
																		array(
																				'field'   => 'link',
																				'label'   => 'Link',
																				'rules'   => 'trim|strip_tags|required'
																			),
																		array(
																				'field'   => 'class',
																				'label'   => 'Html Class',
																				'rules'   => 'trim'
																			),
																		array(
																				'field'   => 'description',
																				'label'   => 'Short description',
																				'rules'   => 'trim'
																			)
																);
																
//edit page
$config['mainmenu']['editAdditional']['css'] 		= array('jquery.fancybox.css');//array('uitheme/cupertino/jquery-ui-1.7.1.custom.css', 'jquery.fancybox.css');
$config['mainmenu']['editAdditional']['js'] 			= array('tiny.init.js');
$config['mainmenu']['editAdditional']['outerjs'] 	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config['mainmenu']['editTemplate'] 				= 'menus/mainmenu/editAction.tpl';
$config['mainmenu']['editDefaultTabName']			= 'Edit: ';
$config['mainmenu']['editDefaultHeader'] 			= 'Edit item';
$config['mainmenu']['editDefaultSubHeader'] 		= '';
$config['mainmenu']['editSuccessHeader'] 			= 'Data has been successfully saved';
$config['mainmenu']['editSuccessSubHeader'] 		= '';
$config['mainmenu']['editErrorHeader'] 			= 'Data has not been saved.';
$config['mainmenu']['editErrorSubHeader'] 		= '';
$config['mainmenu']['editValidationConfig']		= array(
																		array(
																				'field'   => 'menuName',
																				'label'   => 'Item name',
																				'rules'   => 'trim|strip_tags|required'
																			),
																		array(
																				'field'   => 'position',
																				'label'   => 'Position',
																				'rules'   => 'trim|required|numeric'
																			),
																		array(
																				'field'   => 'link',
																				'label'   => 'Link',
																				'rules'   => 'trim|strip_tags|required'
																			),
																		array(
																				'field'   => 'class',
																				'label'   => 'Html Class',
																				'rules'   => 'trim'
																			),
																		array(
																				'field'   => 'description',
																				'label'   => 'Short description',
																				'rules'   => 'trim'
																			)
																);																
//remove page
$config['mainmenu']['advancedRemove']					= true;	
$config['mainmenu']['removeAdditional']['css']		= array();
$config['mainmenu']['removeAdditional']['js']		= array();
$config['mainmenu']['removeAdditional']['outerjs']	= array();
$config['mainmenu']['removeTemplate']					= 'menus/mainmenu/removeAction.tpl';
$config['mainmenu']['removeDefaultTabName']			= 'Delete item';
$config['mainmenu']['removeDefaultHeader']			= 'Delete item';
$config['mainmenu']['removeDefaultSubHeader'] 		= '';
$config['mainmenu']['removeSuccessHeader']			= 'Item has been deleted:';
$config['mainmenu']['removeSuccessSubHeader']		= '';
$config['mainmenu']['removeErrorHeader']				= 'Item has not been deleted.';
$config['mainmenu']['removeErrorSubHeader']			= '';				



/*
 * footermenu
 *
 *
 *
*/

//general settings
$config['footermenu']['itemType'] = 2;
$config['footermenu']['mainName'] = 'footermenu';
$config['footermenu']['activeMenuItem'] = 1;
$config['footermenu']['mainLink'] 		= 'menu/footermenu';
$config['footermenu']['models'] 		= null;
$config['footermenu']['ifFiltered'] = TRUE;
$config['footermenu']['secondMenu'] = array(
											0 => array(	"name"   => "See all",
															"link"   => "menu/footermenu"),
														
											1 => array(	"name"   => "Add item",
															"link"   => "menu/footermenu/addnew")
										);
$config['footermenu']['menuLastEditedVarName'] 	= 'lastEditedFooterMenu';
$config['footermenu']['menuLastEditedText'] 		= 'Last edited item:';
$config['footermenu']['menuLastEditedLink'] 		= 'menu/footermenu/edit/';
$config['footermenu']['metaDataType'] = -1;
$config['footermenu']['nestedNodes'] = TRUE;

//extended field [filedname => default]
$config['footermenu']['extendedFields'] = null;

//main picture settings
$config['footermenu']['allowedTypes'] 						= 'jpg|png';
$config['footermenu']['mainPicture'] 						= 0;
$config['footermenu']['mainPictureMandatory'] 			= 0;
$config['footermenu']['mainPictureMandatoryError'] 	= 'Pole <strong>załącznik</strong> jest wymagane.';
$config['footermenu']['mainPicturePath'] 				= '../pictures/menus/footermenu';
$config['footermenu']['mainPictureMaxWidth'] 			= 0;
$config['footermenu']['mainPictureMaxHeight'] 			= 0;
$config['footermenu']['mainPictureMinWidth'] 			= 0;
$config['footermenu']['mainPictureMinWidth'] 			= 0;
$config['footermenu']['mainPictureThumbWidth'] 			= 0;


//index page
$config['footermenu']['indexAdditional']['css'] 	= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['footermenu']['indexAdditional']['js'] 		= null;
$config['footermenu']['indexAdditional']['outerjs'] 	= null;
$config['footermenu']['indexItemsOrder'] = 'position asc';
$config['footermenu']['indexTemplate'] = 'menus/footermenu/showAll.tpl';

//addnew page
$config['footermenu']['addNewAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['footermenu']['addNewAdditional']['js'] 		= array('tiny.init.js');
$config['footermenu']['addNewAdditional']['outerjs'] = array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																			));
$config['footermenu']['addNewTemplate'] 					= 'menus/footermenu/addAction.tpl';
$config['footermenu']['addNewDefaultHeader'] 			= 'Add item';
$config['footermenu']['addNewDefaultSubHeader'] 		= '';
$config['footermenu']['addNewSuccessHeader'] 			= 'Data has been successfully saved';
$config['footermenu']['addNewSuccessSubHeader'] 		= '';
$config['footermenu']['addNewErrorHeader'] 				= 'Data has not been saved';
$config['footermenu']['addNewErrorSubHeader'] 			= '';
$config['footermenu']['addNewValidationConfig']	= array(
																		array(
																				'field'   => 'menuName',
																				'label'   => 'Item name',
																				'rules'   => 'trim|strip_tags|required'
																			),
																		array(
																				'field'   => 'position',
																				'label'   => 'Position',
																				'rules'   => 'trim|required|numeric'
																			),
																		array(
																				'field'   => 'link',
																				'label'   => 'Link',
																				'rules'   => 'trim|strip_tags|required'
																			),
																		array(
																				'field'   => 'class',
																				'label'   => 'Html Class',
																				'rules'   => 'trim'
																			),
																		array(
																				'field'   => 'description',
																				'label'   => 'Short description',
																				'rules'   => 'trim'
																			)
																);
																
//edit page
$config['footermenu']['editAdditional']['css'] 			= array('jquery.fancybox.css');//array('uitheme/cupertino/jquery-ui-1.7.1.custom.css', 'jquery.fancybox.css');
$config['footermenu']['editAdditional']['js'] 			= array('tiny.init.js');
$config['footermenu']['editAdditional']['outerjs'] 	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config['footermenu']['editTemplate'] 					= 'menus/footermenu/editAction.tpl';
$config['footermenu']['editDefaultTabName']			= 'Edit: ';
$config['footermenu']['editDefaultHeader'] 			= 'Edit item';
$config['footermenu']['editDefaultSubHeader'] 		= '';
$config['footermenu']['editSuccessHeader'] 			= 'Data has been successfully saved';
$config['footermenu']['editSuccessSubHeader'] 		= '';
$config['footermenu']['editErrorHeader'] 				= 'Data has not been saved.';
$config['footermenu']['editErrorSubHeader'] 			= '';
$config['footermenu']['editValidationConfig']		= array(
																		array(
																				'field'   => 'menuName',
																				'label'   => 'Item name',
																				'rules'   => 'trim|strip_tags|required'
																			),
																		array(
																				'field'   => 'position',
																				'label'   => 'Position',
																				'rules'   => 'trim|required|numeric'
																			),
																		array(
																				'field'   => 'link',
																				'label'   => 'Link',
																				'rules'   => 'trim|strip_tags|required'
																			),
																		array(
																				'field'   => 'class',
																				'label'   => 'Html Class',
																				'rules'   => 'trim'
																			),
																		array(
																				'field'   => 'description',
																				'label'   => 'Short description',
																				'rules'   => 'trim'
																			)
																);																
//remove page
$config['footermenu']['advancedRemove']					= true;	
$config['footermenu']['removeAdditional']['css']		= array();
$config['footermenu']['removeAdditional']['js']			= array();
$config['footermenu']['removeAdditional']['outerjs']	= array();
$config['footermenu']['removeTemplate']					= 'menus/footermenu/removeAction.tpl';
$config['footermenu']['removeDefaultTabName']			= 'Delete item';
$config['footermenu']['removeDefaultHeader']				= 'Delete item';
$config['footermenu']['removeDefaultSubHeader'] 		= '';
$config['footermenu']['removeSuccessHeader']				= 'Item has been deleted:';
$config['footermenu']['removeSuccessSubHeader']			= '';
$config['footermenu']['removeErrorHeader']				= 'Item has not been deleted.';
$config['footermenu']['removeErrorSubHeader']			= '';	


/*
 * topmenu
 *
 *
 *
*/


//general settings
$config['topmenu']['itemType'] = 3;
$config['topmenu']['mainName'] = 'topmenu';
$config['topmenu']['activeMenuItem'] = 1;
$config['topmenu']['mainLink'] 		= 'menu/topmenu';
$config['topmenu']['models'] 		= null;
$config['topmenu']['ifFiltered'] = TRUE;
$config['topmenu']['secondMenu'] = array(
											0 => array(	"name"   => "See all",
															"link"   => "menu/topmenu"),
														
											1 => array(	"name"   => "Add item",
															"link"   => "menu/topmenu/addnew")
										);
$config['topmenu']['menuLastEditedVarName'] 	= 'lastEditedtopmenu';
$config['topmenu']['menuLastEditedText'] 		= 'Last edited item:';
$config['topmenu']['menuLastEditedLink'] 		= 'menu/topmenu/edit/';
$config['topmenu']['metaDataType'] = -1;
$config['topmenu']['nestedNodes'] = TRUE;

//extended field [filedname => default]
$config['topmenu']['extendedFields'] = null;

//main picture settings
$config['topmenu']['allowedTypes'] 					= 'jpg|png';
$config['topmenu']['mainPicture'] 						= 0;
$config['topmenu']['mainPictureMandatory'] 			= 0;
$config['topmenu']['mainPictureMandatoryError'] 	= 'Field <strong>attachment</strong> is mandatory.';
$config['topmenu']['mainPicturePath'] 				= '../pictures/menus/topmenu';
$config['topmenu']['mainPictureMaxWidth'] 			= 0;
$config['topmenu']['mainPictureMaxHeight'] 			= 0;
$config['topmenu']['mainPictureMinWidth'] 			= 0;
$config['topmenu']['mainPictureMinWidth'] 			= 0;
$config['topmenu']['mainPictureThumbWidth'] 		= 0;


//index page
$config['topmenu']['indexAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['topmenu']['indexAdditional']['js'] 		= null;
$config['topmenu']['indexAdditional']['outerjs'] 	= null;
$config['topmenu']['indexItemsOrder'] = 'position asc';
$config['topmenu']['indexTemplate'] = 'menus/topmenu/showAll.tpl';

//addnew page
$config['topmenu']['addNewAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['topmenu']['addNewAdditional']['js'] 		= array('tiny.init.js');
$config['topmenu']['addNewAdditional']['outerjs'] = array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																			));
$config['topmenu']['addNewTemplate'] 					= 'menus/topmenu/addAction.tpl';
$config['topmenu']['addNewDefaultHeader'] 			= 'Add item';
$config['topmenu']['addNewDefaultSubHeader'] 		= '';
$config['topmenu']['addNewSuccessHeader'] 			= 'Data has been successfully saved';
$config['topmenu']['addNewSuccessSubHeader'] 		= '';
$config['topmenu']['addNewErrorHeader'] 				= 'Data has not been saved';
$config['topmenu']['addNewErrorSubHeader'] 			= '';
$config['topmenu']['addNewValidationConfig']	= array(
																		array(
																				'field'   => 'menuName',
																				'label'   => 'Item name',
																				'rules'   => 'trim|strip_tags|required'
																			),
																		array(
																				'field'   => 'position',
																				'label'   => 'Position',
																				'rules'   => 'trim|required|numeric'
																			),
																		array(
																				'field'   => 'link',
																				'label'   => 'Link',
																				'rules'   => 'trim|strip_tags|required'
																			),
																		array(
																				'field'   => 'class',
																				'label'   => 'Html Class',
																				'rules'   => 'trim'
																			),
																		array(
																				'field'   => 'description',
																				'label'   => 'Short description',
																				'rules'   => 'trim'
																			)
																);
																
//edit page
$config['topmenu']['editAdditional']['css'] 		= array('jquery.fancybox.css');//array('uitheme/cupertino/jquery-ui-1.7.1.custom.css', 'jquery.fancybox.css');
$config['topmenu']['editAdditional']['js'] 			= array('tiny.init.js');
$config['topmenu']['editAdditional']['outerjs'] 	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config['topmenu']['editTemplate'] 				= 'menus/topmenu/editAction.tpl';
$config['topmenu']['editDefaultTabName']			= 'Edit: ';
$config['topmenu']['editDefaultHeader'] 			= 'Edit item';
$config['topmenu']['editDefaultSubHeader'] 		= '';
$config['topmenu']['editSuccessHeader'] 			= 'Data has been successfully saved';
$config['topmenu']['editSuccessSubHeader'] 		= '';
$config['topmenu']['editErrorHeader'] 			= 'Data has not been saved.';
$config['topmenu']['editErrorSubHeader'] 		= '';
$config['topmenu']['editValidationConfig']		= array(
																		array(
																				'field'   => 'menuName',
																				'label'   => 'Item name',
																				'rules'   => 'trim|strip_tags|required'
																			),
																		array(
																				'field'   => 'position',
																				'label'   => 'Position',
																				'rules'   => 'trim|required|numeric'
																			),
																		array(
																				'field'   => 'link',
																				'label'   => 'Link',
																				'rules'   => 'trim|strip_tags|required'
																			),
																		array(
																				'field'   => 'class',
																				'label'   => 'Html Class',
																				'rules'   => 'trim'
																			),
																		array(
																				'field'   => 'description',
																				'label'   => 'Short description',
																				'rules'   => 'trim'
																			)
																);																
//remove page
$config['topmenu']['advancedRemove']					= true;	
$config['topmenu']['removeAdditional']['css']		= array();
$config['topmenu']['removeAdditional']['js']		= array();
$config['topmenu']['removeAdditional']['outerjs']	= array();
$config['topmenu']['removeTemplate']					= 'menus/topmenu/removeAction.tpl';
$config['topmenu']['removeDefaultTabName']			= 'Delete item';
$config['topmenu']['removeDefaultHeader']			= 'Delete item';
$config['topmenu']['removeDefaultSubHeader'] 		= '';
$config['topmenu']['removeSuccessHeader']			= 'Item has been deleted:';
$config['topmenu']['removeSuccessSubHeader']		= '';
$config['topmenu']['removeErrorHeader']				= 'Item has not been deleted.';
$config['topmenu']['removeErrorSubHeader']			= '';		