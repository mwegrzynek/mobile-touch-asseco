<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*
 * Features pages
 *
 *
 *
*/

//general settings
$config['featurespages']['itemType']       = 3;
$config['featurespages']['mainName']       = 'featurespages';
$config['featurespages']['activeMenuItem'] = 2;
$config['featurespages']['showName']       = 'Mobile Touch Features Pages';
$config['featurespages']['mainLink']       = 'items/featurespages';
$config['featurespages']['models']         = null;
$config['featurespages']['ifFiltered']     = true;
$config['featurespages']['secondMenu']     = array(
											0 => array(	"name"   => "See all",
															"link"   => "items/featurespages"),

											1 => array(	"name"   => "Add new page",
															"link"   => "items/featurespages/addnew")
										);
$config['featurespages']['menuLastEditedVarName'] = 'lastEditedSite';
$config['featurespages']['menuLastEditedText'] 	= 'Edit last edited:';
$config['featurespages']['menuLastEditedLink'] 	= 'items/featurespages/edit/';
$config['featurespages']['metaDataType'] = 3;
$config['featurespages']['nestedNodes'] = TRUE;
$config['featurespages']['categorized'] = TRUE;
$config['featurespages']['categoryType'] = 1;
$config['featurespages']['categoryLink'] = 'category/industries';
$config['featurespages']['indexCategoryFilterWithChildren'] = FALSE;
$config['featurespages']['indexParentFilterWithChildren'] 	= FALSE;


$config['featurespages']['categorizedSecond'] = FALSE;
$config['featurespages']['categorySecondType'] = -1;
$config['featurespages']['categorySecondLink'] = 'category/';
$config['featurespages']['indexCategorySecondFilterWithChildren'] = FALSE;





//extended field [filedname => default]
$config['featurespages']['extendedFields'] = null;

$config['featurespages']['isRestrictedContent'] = FALSE;

//main picture settings
$config['featurespages']['allowedTypes'] 					= 'jpg|png';
$config['featurespages']['mainPicture'] 						= 1;
$config['featurespages']['mainPictureMandatory'] 			= 0;
$config['featurespages']['mainPictureMandatoryError'] 	= 'Field <strong>image</strong> is mandatory.';
$config['featurespages']['mainPicturePath'] 				= '../userfiles/featurespages';
$config['featurespages']['mainPictureMaxWidth'] 			= 980;
$config['featurespages']['mainPictureMaxHeight'] 			= 1200;
$config['featurespages']['mainPictureMinWidth'] 			= 30;
$config['featurespages']['mainPictureMinHeight'] 			= 44;

//gallery pictures
$config['featurespages']['galleryPicturesPath']   = '../pictures/featurespages';
$config['featurespages']['galleryPicturesThumbsInfo'] = array(
																	0 => array( 'width' => 100, 'height' => 100 ),
																	1 => array( 'width' => 100, 'height' => 50 )
																);


//index page
$config['featurespages']['indexAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['featurespages']['indexAdditional']['js'] 		= null;
$config['featurespages']['indexAdditional']['outerjs'] 	= null;
$config['featurespages']['indexItemsOrderBy'] 				= 'position';
$config['featurespages']['indexItemsOrder'] 					= 'asc';
$config['featurespages']['indexTemplate'] 					= 'items/featurespages/showItems.tpl';

//addnew page
$config['featurespages']['addNewAdditional']['css'] 		= array(); //array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['featurespages']['addNewAdditional']['js'] 		= array('tiny.init.js');
$config['featurespages']['addNewAdditional']['outerjs'] = array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																			));
$config['featurespages']['addNewTemplate'] 				= 'items/featurespages/itemsAddAction.tpl';
$config['featurespages']['addNewDefaultHeader'] 		= 'Add new page';
$config['featurespages']['addNewDefaultSubHeader'] 	= '';
$config['featurespages']['addNewSuccessHeaderAllLangs']	= 'Data has been successfully saved in all languages.';
$config['featurespages']['addNewSuccessHeader'] 		= 'Data has been successfully saved.';
$config['featurespages']['addNewSuccessSubHeader'] 	= '';
$config['featurespages']['addNewErrorHeader'] 			= 'Data has not been saved';
$config['featurespages']['addNewErrorSubHeader'] 		= '';
$config['featurespages']['addNewValidationConfig']	= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',
																			'rules'   => 'trim|required|is_natural'
																		)
																);

//edit page
$config['featurespages']['editAdditional']['css'] 		= array('jquery.fancybox.css');//array('uitheme/cupertino/jquery-ui-1.7.1.custom.css', 'jquery.fancybox.css');
$config['featurespages']['editAdditional']['js'] 			= array('tiny.init.js');
$config['featurespages']['editAdditional']['outerjs'] 	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config['featurespages']['editTemplate'] 				= 'items/featurespages/itemsEditAction.tpl';
$config['featurespages']['editDefaultTabName']			= 'Edit: ';
$config['featurespages']['editDefaultHeader'] 			= 'Edit page';
$config['featurespages']['editDefaultSubHeader'] 		= '';
$config['featurespages']['editSuccessHeader'] 			= 'Data has been successfully saved.';
$config['featurespages']['editSuccessSubHeader'] 		= '';
$config['featurespages']['editErrorHeader'] 			= 'Data has not been saved.';
$config['featurespages']['editErrorSubHeader'] 		= '';
$config['featurespages']['editValidationConfig']		= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',
																			'rules'   => 'trim|required|is_natural'
																		)
																);


//remove page
$config['featurespages']['advancedRemove']              = true;
$config['featurespages']['removeAdditional']['css']     = array();
$config['featurespages']['removeAdditional']['js']      = array();
$config['featurespages']['removeAdditional']['outerjs'] = array();
$config['featurespages']['removeTemplate']              = 'items/featurespages/removeItem.tpl';
$config['featurespages']['removeDefaultTabName']        = 'Remove page';
$config['featurespages']['removeDefaultHeader']         = 'Remove page';
$config['featurespages']['removeDefaultSubHeader']      = '';
$config['featurespages']['removeSuccessHeader']         = 'Page has been deleted:';
$config['featurespages']['removeSuccessSubHeader']      = '';
$config['featurespages']['removeErrorHeader']           = 'Page has not been deleted.';
$config['featurespages']['removeErrorSubHeader']        = '';

// import / export settings
$config['featurespages']['import']  = array(
														'fields' => array(
															'id'      => 'Id',
															'title'   => 'Title',
															'lead'    => 'Lead',
															'content' => 'Content'
														),
														'meta' => false
													);