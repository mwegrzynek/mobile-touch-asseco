<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*
 * logoslider
 *
 *
 *
*/

//general settings
$config['logoslider']['itemType']       = 21;
$config['logoslider']['mainName']       = 'logoslider';
$config['logoslider']['activeMenuItem'] = 15;
$config['logoslider']['mainLink']       = 'items/logoslider';
$config['logoslider']['models']         = null;
$config['logoslider']['ifFiltered']     = true;
$config['logoslider']['secondMenu']     = array(
											0 => array(	"name"   => "See all",
															"link"   => "items/logoslider"),
														
											1 => array(	"name"   => "Add new slider",
															"link"   => "items/logoslider/addnew")
										);
$config['logoslider']['menuLastEditedVarName'] = 'lastEditedSite';
$config['logoslider']['menuLastEditedText'] 	= 'Edit last edited:';
$config['logoslider']['menuLastEditedLink'] 	= 'items/logoslider/edit/';
$config['logoslider']['metaDataType'] = -1;
$config['logoslider']['nestedNodes'] = FALSE;
$config['logoslider']['categorized'] = FALSE;
$config['logoslider']['categoryType'] = 1;
$config['logoslider']['categoryLink'] = 'category/industries';
$config['logoslider']['indexCategoryFilterWithChildren'] = FALSE;
$config['logoslider']['indexParentFilterWithChildren'] 	= FALSE;


$config['logoslider']['categorizedSecond'] = FALSE;
$config['logoslider']['categorySecondType'] = -1;
$config['logoslider']['categorySecondLink'] = 'category/';
$config['logoslider']['indexCategorySecondFilterWithChildren'] = FALSE;





//extended field [filedname => default]
$config['logoslider']['extendedFields'] = null;

$config['logoslider']['isRestrictedContent'] = FALSE;

//main picture settings
$config['logoslider']['allowedTypes'] 					= 'jpg|png';
$config['logoslider']['mainPicture'] 					= 1;
$config['logoslider']['mainPictureMandatory'] 		= 1;
$config['logoslider']['mainPictureMandatoryError'] = 'Field <strong>image</strong> is mandatory.';
$config['logoslider']['mainPicturePath'] 				= '../userfiles/logoslider';
$config['logoslider']['mainPictureMaxWidth'] 		= 128;
$config['logoslider']['mainPictureMaxHeight'] 		= 97;
$config['logoslider']['mainPictureMinWidth'] 		= 128;
$config['logoslider']['mainPictureMinHeight'] 		= 97;
$config['logoslider']['mainPictureThumbsInfo'] 		= FALSE;
																
/*$config['logoslider']['mainPictureThumbsInfo'] 		= array(
																	0 => array( 'width' => 100, 'height' => 100 ),
																	1 => array( 'width' => 100, 'height' => 50 )
																);	*/									


//gallery pictures
$config['logoslider']['galleryPicturesPath']   = '../pictures/logoslider';
$config['logoslider']['galleryPicturesThumbsInfo'] = array(
																	0 => array( 'width' => 100, 'height' => 100 ),
																	1 => array( 'width' => 100, 'height' => 50 )
																);


//index page
$config['logoslider']['indexAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['logoslider']['indexAdditional']['js'] 		= null;
$config['logoslider']['indexAdditional']['outerjs'] 	= null;
$config['logoslider']['indexItemsOrderBy'] 				= 'position';
$config['logoslider']['indexItemsOrder'] 					= 'asc';
$config['logoslider']['indexTemplate'] 					= 'items/logoslider/showItems.tpl';

//addnew page
$config['logoslider']['addNewAdditional']['css'] 		= array(); //array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['logoslider']['addNewAdditional']['js'] 		= array('tiny.init.js');
$config['logoslider']['addNewAdditional']['outerjs'] = array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																			));
$config['logoslider']['addNewTemplate'] 				= 'items/logoslider/itemsAddAction.tpl';
$config['logoslider']['addNewDefaultHeader'] 		= 'Add new logo';
$config['logoslider']['addNewDefaultSubHeader'] 	= '';
$config['logoslider']['addNewSuccessHeaderAllLangs']	= 'Data has been successfully saved in all languages.';
$config['logoslider']['addNewSuccessHeader'] 		= 'Data has been successfully saved.';
$config['logoslider']['addNewSuccessSubHeader'] 	= '';
$config['logoslider']['addNewErrorHeader'] 			= 'Data has not been saved';
$config['logoslider']['addNewErrorSubHeader'] 		= '';
$config['logoslider']['addNewValidationConfig']	= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',																			
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',																			
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',																			
																			'rules'   => 'trim|required|is_natural'
																		)	
																);
																
//edit page
$config['logoslider']['editAdditional']['css'] 		= array('jquery.fancybox.css');//array('uitheme/cupertino/jquery-ui-1.7.1.custom.css', 'jquery.fancybox.css');
$config['logoslider']['editAdditional']['js'] 			= array('tiny.init.js');
$config['logoslider']['editAdditional']['outerjs'] 	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config['logoslider']['editTemplate'] 				= 'items/logoslider/itemsEditAction.tpl';
$config['logoslider']['editDefaultTabName']		= 'Edit: ';
$config['logoslider']['editDefaultHeader'] 		= 'Edit logo';
$config['logoslider']['editDefaultSubHeader'] 	= '';
$config['logoslider']['editSuccessHeader'] 		= 'Data has been successfully saved.';
$config['logoslider']['editSuccessSubHeader'] 	= '';
$config['logoslider']['editErrorHeader'] 			= 'Data has not been saved.';
$config['logoslider']['editErrorSubHeader'] 		= '';
$config['logoslider']['editValidationConfig']		= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',																			
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',																			
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',																			
																			'rules'   => 'trim|required|is_natural'
																		)	
																);
																
																
//remove page
$config['logoslider']['advancedRemove']					= true;	
$config['logoslider']['removeAdditional']['css']		= array();
$config['logoslider']['removeAdditional']['js']			= array();
$config['logoslider']['removeAdditional']['outerjs']	= array();
$config['logoslider']['removeTemplate']					= 'items/logoslider/removeItem.tpl';
$config['logoslider']['removeDefaultTabName']			= 'Remove logo';
$config['logoslider']['removeDefaultHeader']				= 'Remove logo';
$config['logoslider']['removeDefaultSubHeader'] 		= '';
$config['logoslider']['removeSuccessHeader']				= 'Logo has been deleted:';
$config['logoslider']['removeSuccessSubHeader']			= '';
$config['logoslider']['removeErrorHeader']				= 'Logo has not been deleted.';
$config['logoslider']['removeErrorSubHeader']			= '';		
