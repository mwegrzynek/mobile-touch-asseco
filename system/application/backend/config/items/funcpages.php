<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * funcpages
 *
 *
 *
*/

//general settings
$config['funcpages']['itemType']       = 13;
$config['funcpages']['mainName']       = 'funcpages';
$config['funcpages']['activeMenuItem'] = 15;
$config['funcpages']['mainLink']       = 'items/funcpages';
$config['funcpages']['models'] = null;
$config['funcpages']['ifFiltered'] = true;
$config['funcpages']['secondMenu'] = array(
											0 => array(	"name"   => "See all",
															"link"   => "items/funcpages"),

											/*1 => array(	"name"   => "Add new page func page",
															"link"   => "items/funcpages/addnew")	*/
										);
$config['funcpages']['menuLastEditedVarName'] 	= 'lastEditedFuncpage';
$config['funcpages']['menuLastEditedText'] 		= 'Last edited:';
$config['funcpages']['menuLastEditedLink'] 		= 'items/funcpages/edit/';
$config['funcpages']['metaDataType'] = -1;
$config['funcpages']['nestedNodes'] = FALSE;
$config['funcpages']['categorized'] = FALSE;
$config['funcpages']['categoryType'] = 3;
$config['funcpages']['categoryLink'] = 'category/catfuncpages';

$config['funcpages']['categorizedSecond'] = FALSE;
$config['funcpages']['categorySecondType'] = -1;
$config['funcpages']['categorySecondLink'] = 'category/';
$config['funcpages']['indexCategorySecondFilterWithChildren'] = FALSE;

//extended field [filedname => default]
$config['funcpages']['extendedFields'] = null;

$config['funcpages']['isRestrictedContent'] = FALSE;


//main picture settings
$config['funcpages']['allowedTypes']              = 'jpg|png';
$config['funcpages']['mainPicture']               = 0;
$config['funcpages']['mainPictureMandatory']      = 0;
$config['funcpages']['mainPictureMandatoryError'] = 'Pole <strong>obrazek</strong> jest wymagane.';
$config['funcpages']['mainPicturePath']           = '../pictures/funcpages';
$config['funcpages']['mainPictureMaxWidth']       = 1000;
$config['funcpages']['mainPictureMaxHeight']      = 1000;
$config['funcpages']['mainPictureMinWidth']       = 10;
$config['funcpages']['mainPictureMinHeight']      = 10;
$config['funcpages']['mainPictureThumbWidth']     = 0;

//gallery pictures
$config['funcpages']['galleryPicturesPath']   = '../pictures/funcpages';


//index page
$config['funcpages']['indexAdditional']['css']			= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['funcpages']['indexAdditional']['js']			= null;
$config['funcpages']['indexAdditional']['outerjs']	= null;
$config['funcpages']['indexItemsOrderBy'] 				= 'title';
$config['funcpages']['indexItemsOrder'] 					= 'asc';
$config['funcpages']['indexTemplate'] 					= 'items/funcpages/showItems.tpl';

//addnew page
$config['funcpages']['addNewAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['funcpages']['addNewAdditional']['js'] 			= array('tiny.init.js');
$config['funcpages']['addNewAdditional']['outerjs']	= array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																			));
$config['funcpages']['addNewTemplate'] 				= 'items/funcpages/itemsAddAction.tpl';
$config['funcpages']['addNewDefaultHeader'] 		= 'Add new page';
$config['funcpages']['addNewDefaultSubHeader'] 	= '';
$config['funcpages']['addNewSuccessHeader'] 		= 'Data has been successfully saved.';
$config['funcpages']['addNewSuccessSubHeader'] 	= '';
$config['funcpages']['addNewErrorHeader'] 			= 'Data has not been saved.';
$config['funcpages']['addNewErrorSubHeader'] 		= '';
$config['funcpages']['addNewValidationConfig']	= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Name',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Text',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',
																			'rules'   => 'trim'
																		)
																);

//edit page
$config['funcpages']['editAdditional']['css']	= array('jquery.fancybox.css');//array('uitheme/cupertino/jquery-ui-1.7.1.custom.css', 'jquery.fancybox.css');
$config['funcpages']['editAdditional']['js']		= array('tiny.init.js');
$config['funcpages']['editAdditional']['outerjs']	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config['funcpages']['editTemplate']					= 'items/funcpages/itemsEditAction.tpl';
$config['funcpages']['editDefaultTabName']			= 'Edit address';
$config['funcpages']['editDefaultHeader'] 			= 'Edit address';
$config['funcpages']['editDefaultSubHeader'] 		= '';
$config['funcpages']['editSuccessHeader'] 			= 'Data has been successfully saved.';
$config['funcpages']['editSuccessSubHeader'] 		= '';
$config['funcpages']['editErrorHeader']				= 'Data has not been saved.';
$config['funcpages']['editErrorSubHeader']			= '';
$config['funcpages']['editValidationConfig']		= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Name',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Text',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',
																			'rules'   => 'trim'
																		)
																);

//remove page
$config['funcpages']['advancedRemove']					= FALSE;
$config['funcpages']['removeAdditional']['css']		= array();
$config['funcpages']['removeAdditional']['js']			= array();
$config['funcpages']['removeAdditional']['outerjs']	= array();
$config['funcpages']['removeTemplate']					= 'items/funcpages/removeItem.tpl';
$config['funcpages']['removeDefaultTabName']			= 'Delete address';
$config['funcpages']['removeDefaultHeader']			= 'Delete address';
$config['funcpages']['removeDefaultSubHeader']		= '';
$config['funcpages']['removeSuccessHeader']			= 'Address has been deleted:';
$config['funcpages']['removeSuccessSubHeader']		= '';
$config['funcpages']['removeErrorHeader']				= 'Address has been deleted.';
$config['funcpages']['removeErrorSubHeader']			= '';
