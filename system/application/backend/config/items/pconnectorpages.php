<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*
 * Platforma Connector main pconnectorpages
 *
 *
 *
*/

//general settings
$config['pconnectorpages']['itemType']       = 22;
$config['pconnectorpages']['mainName']       = 'pconnectorpages';
$config['pconnectorpages']['activeMenuItem'] = 3;
$config['pconnectorpages']['mainLink']       = 'items/pconnectorpages';
$config['pconnectorpages']['models']         = null;
$config['pconnectorpages']['ifFiltered']     = true;
$config['pconnectorpages']['secondMenu']     = array(
											0 => array(	"name"   => "See all",
															"link"   => "items/pconnectorpages")/*,
														
											1 => array(	"name"   => "Add new page",
												"link"   => "items/pconnectorpages/addnew")*/
										);
$config['pconnectorpages']['menuLastEditedVarName'] = 'lastEditedSite';
$config['pconnectorpages']['menuLastEditedText'] 	= 'Edit last edited:';
$config['pconnectorpages']['menuLastEditedLink'] 	= 'items/pconnectorpages/edit/';
$config['pconnectorpages']['metaDataType'] = 22;
$config['pconnectorpages']['nestedNodes'] = FALSE;
$config['pconnectorpages']['categorized'] = FALSE;
$config['pconnectorpages']['categoryType'] = 1;
$config['pconnectorpages']['categoryLink'] = 'category/industries';
$config['pconnectorpages']['indexCategoryFilterWithChildren'] = FALSE;
$config['pconnectorpages']['indexParentFilterWithChildren'] 	= FALSE;


$config['pconnectorpages']['categorizedSecond'] = FALSE;
$config['pconnectorpages']['categorySecondType'] = -1;
$config['pconnectorpages']['categorySecondLink'] = 'category/';
$config['pconnectorpages']['indexCategorySecondFilterWithChildren'] = FALSE;





//extended field [filedname => default]
$config['pconnectorpages']['extendedFields'] = null;

$config['pconnectorpages']['isRestrictedContent'] = FALSE;

//main picture settings
$config['pconnectorpages']['allowedTypes']              = 'jpg|png';
$config['pconnectorpages']['mainPicture']               = 1;
$config['pconnectorpages']['mainPictureMandatory']      = 0;
$config['pconnectorpages']['mainPictureMandatoryError'] = 'Field <strong>image</strong> is mandatory.';
$config['pconnectorpages']['mainPicturePath']           = '../userfiles/pconnectorpages';
$config['pconnectorpages']['mainPictureMaxWidth'] 		= 700;
$config['pconnectorpages']['mainPictureMaxHeight'] 		= 1000;
$config['pconnectorpages']['mainPictureMinWidth'] 		= 50;
$config['pconnectorpages']['mainPictureMinHeight'] 		= 50;
$config['pconnectorpages']['mainPictureThumbWidth'] 		= 0;


//gallery pictures
$config['pconnectorpages']['galleryPicturesPath']   = '../pictures/pconnectorpages';
$config['pconnectorpages']['galleryPicturesThumbsInfo'] = array(
																	0 => array( 'width' => 100, 'height' => 100 ),
																	1 => array( 'width' => 100, 'height' => 50 )
																);


//index page
$config['pconnectorpages']['indexAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['pconnectorpages']['indexAdditional']['js'] 			= null;
$config['pconnectorpages']['indexAdditional']['outerjs'] 	= null;
$config['pconnectorpages']['indexItemsOrderBy'] 				= 'position';
$config['pconnectorpages']['indexItemsOrder'] 					= 'asc';
$config['pconnectorpages']['indexTemplate'] 					= 'items/pconnectorpages/showItems.tpl';

//addnew page
$config['pconnectorpages']['addNewAdditional']['css'] 		= array(); //array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['pconnectorpages']['addNewAdditional']['js'] 		= array('tiny.init.js');
$config['pconnectorpages']['addNewAdditional']['outerjs'] = array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																			));
$config['pconnectorpages']['addNewTemplate'] 				= 'items/pconnectorpages/itemsAddAction.tpl';
$config['pconnectorpages']['addNewDefaultHeader'] 		= 'Add new page';
$config['pconnectorpages']['addNewDefaultSubHeader'] 	= '';
$config['pconnectorpages']['addNewSuccessHeaderAllLangs']	= 'Data has been successfully saved in all languages.';
$config['pconnectorpages']['addNewSuccessHeader'] 		= 'Data has been successfully saved.';
$config['pconnectorpages']['addNewSuccessSubHeader'] 	= '';
$config['pconnectorpages']['addNewErrorHeader'] 			= 'Data has not been saved';
$config['pconnectorpages']['addNewErrorSubHeader'] 		= '';
$config['pconnectorpages']['addNewValidationConfig']	= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',																			
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',																			
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',																			
																			'rules'   => 'trim'
																		)	
																);
																
//edit page
$config['pconnectorpages']['editAdditional']['css'] 		= array('jquery.fancybox.css');//array('uitheme/cupertino/jquery-ui-1.7.1.custom.css', 'jquery.fancybox.css');
$config['pconnectorpages']['editAdditional']['js'] 			= array('tiny.init.js');
$config['pconnectorpages']['editAdditional']['outerjs'] 	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config['pconnectorpages']['editTemplate'] 				= 'items/pconnectorpages/itemsEditAction.tpl';
$config['pconnectorpages']['editDefaultTabName']			= 'Edit: ';
$config['pconnectorpages']['editDefaultHeader'] 			= 'Edit page';
$config['pconnectorpages']['editDefaultSubHeader'] 		= '';
$config['pconnectorpages']['editSuccessHeader'] 			= 'Data has been successfully saved.';
$config['pconnectorpages']['editSuccessSubHeader'] 		= '';
$config['pconnectorpages']['editErrorHeader'] 			= 'Data has not been saved.';
$config['pconnectorpages']['editErrorSubHeader'] 		= '';
$config['pconnectorpages']['editValidationConfig']		= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',																			
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',																			
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',																			
																			'rules'   => 'trim'
																		)	
																);
																
																
//remove page
$config['pconnectorpages']['advancedRemove']						= true;	
$config['pconnectorpages']['removeAdditional']['css']			= array();
$config['pconnectorpages']['removeAdditional']['js']			= array();
$config['pconnectorpages']['removeAdditional']['outerjs']		= array();
$config['pconnectorpages']['removeTemplate']					= 'items/pconnectorpages/removeItem.tpl';
$config['pconnectorpages']['removeDefaultTabName']			= 'Remove page';
$config['pconnectorpages']['removeDefaultHeader']			= 'Remove page';
$config['pconnectorpages']['removeDefaultSubHeader'] 		= '';
$config['pconnectorpages']['removeSuccessHeader']			= 'Page has been deleted:';
$config['pconnectorpages']['removeSuccessSubHeader']		= '';
$config['pconnectorpages']['removeErrorHeader']				= 'Page has not been deleted.';
$config['pconnectorpages']['removeErrorSubHeader']			= '';		
