<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * main pages
 *
 *
 *
*/

//general settings
$config['mainpages']['itemType']       = 16;
$config['mainpages']['mainName']       = 'mainpages';
$config['mainpages']['activeMenuItem'] = 4;
$config['mainpages']['showName']       = 'Mobile Touch Main Sections';
$config['mainpages']['mainLink']       = 'items/mainpages';
$config['mainpages']['models'] = null;
$config['mainpages']['ifFiltered'] = true;
$config['mainpages']['secondMenu'] = array(
											0 => array(	"name"   => "See all main pages and sections",
															"link"   => "items/mainpages"),

											/*1 => array(	"name"   => "Add new main page or section",
												"link"   => "items/mainpages/addnew")*/
										);
$config['mainpages']['menuLastEditedVarName'] 	= 'lastEditedMainpages';
$config['mainpages']['menuLastEditedText'] 		= 'Last edited:';
$config['mainpages']['menuLastEditedLink'] 		= 'items/mainpages/edit/';
$config['mainpages']['metaDataType'] = 16;
$config['mainpages']['nestedNodes'] = FALSE;
$config['mainpages']['categorized'] = FALSE;
$config['mainpages']['categoryType'] = -1;
$config['mainpages']['categoryLink'] = 'category/catmainpages';

$config['mainpages']['categorizedSecond'] = FALSE;
$config['mainpages']['categorySecondType'] = -1;
$config['mainpages']['categorySecondLink'] = 'category/';
$config['mainpages']['indexCategorySecondFilterWithChildren'] = FALSE;

//extended field [filedname => default]
$config['mainpages']['extendedFields'] = null;

$config['mainpages']['isRestrictedContent'] = FALSE;


//main picture settings
$config['mainpages']['allowedTypes'] 					= 'jpg|png';
$config['mainpages']['mainPicture'] 					= 1;
$config['mainpages']['mainPictureMandatory'] 		= 0;
$config['mainpages']['mainPictureMandatoryError'] 	= 'Pole <strong>obrazek</strong> jest wymagane.';
$config['mainpages']['mainPicturePath'] 				= '../userfiles/mainpages';
$config['mainpages']['mainPictureMaxWidth'] 			= 600;
$config['mainpages']['mainPictureMaxHeight'] 		= 1000;
$config['mainpages']['mainPictureMinWidth'] 			= 50;
$config['mainpages']['mainPictureMinHeight'] 		= 50;
$config['mainpages']['mainPictureThumbWidth'] 		= 0;

//gallery pictures
$config['mainpages']['galleryPicturesPath']   = '../userfiles/mainpages';


//index page
$config['mainpages']['indexAdditional']['css']			= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['mainpages']['indexAdditional']['js']			= null;
$config['mainpages']['indexAdditional']['outerjs']		= null;
$config['mainpages']['indexItemsOrderBy'] 				= 'title';
$config['mainpages']['indexItemsOrder'] 					= 'asc';
$config['mainpages']['indexTemplate'] 						= 'items/mainpages/showItems.tpl';

//addnew page
$config['mainpages']['addNewAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['mainpages']['addNewAdditional']['js'] 			= array('tiny.init.js');
$config['mainpages']['addNewAdditional']['outerjs']	= array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																			));
$config['mainpages']['addNewTemplate'] 					= 'items/mainpages/itemsAddAction.tpl';
$config['mainpages']['addNewDefaultHeader'] 				= 'Add new';
$config['mainpages']['addNewDefaultSubHeader'] 			= '';
$config['mainpages']['addNewSuccessHeader'] 				= 'Data has been successfully saved.';
$config['mainpages']['addNewSuccessSubHeader'] 			= '';
$config['mainpages']['addNewErrorHeader'] 				= 'Data has not been saved.';
$config['mainpages']['addNewErrorSubHeader'] 			= '';
$config['mainpages']['addNewValidationConfig']	= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',
																			'rules'   => 'trim'
																		)
																);

//edit page
$config['mainpages']['editAdditional']['css']		= array('jquery.fancybox.css');//array('uitheme/cupertino/jquery-ui-1.7.1.custom.css', 'jquery.fancybox.css');
$config['mainpages']['editAdditional']['js']		= array('tiny.init.js');
$config['mainpages']['editAdditional']['outerjs']	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config['mainpages']['editTemplate']					= 'items/mainpages/itemsEditAction.tpl';
$config['mainpages']['editDefaultTabName']			= 'Edit';
$config['mainpages']['editDefaultHeader'] 			= 'Edit';
$config['mainpages']['editDefaultSubHeader'] 		= '';
$config['mainpages']['editSuccessHeader'] 			= 'Data has been successfully saved.';
$config['mainpages']['editSuccessSubHeader'] 		= '';
$config['mainpages']['editErrorHeader']				= 'Data has not been saved.';
$config['mainpages']['editErrorSubHeader']			= '';
$config['mainpages']['editValidationConfig']		= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',
																			'rules'   => 'trim'
																		)
																);


//remove page
$config['mainpages']['advancedRemove']						= true;
$config['mainpages']['removeAdditional']['css']			= array();
$config['mainpages']['removeAdditional']['js']			= array();
$config['mainpages']['removeAdditional']['outerjs']	= array();
$config['mainpages']['removeTemplate']						= 'items/mainpages/removeItem.tpl';
$config['mainpages']['removeDefaultTabName']				= 'Delete';
$config['mainpages']['removeDefaultHeader']				= 'Delete';
$config['mainpages']['removeDefaultSubHeader']			= '';
$config['mainpages']['removeSuccessHeader']				= 'Page / section has been deleted:';
$config['mainpages']['removeSuccessSubHeader']			= '';
$config['mainpages']['removeErrorHeader']					= 'Page / section has been deleted.';
$config['mainpages']['removeErrorSubHeader']				= '';
// import / export settings
$config['mainpages']['import']  		= array(
														'fields' => array(
															'id'          => 'Id',
															'title'       => 'Title',
															'text_data_1' => 'Text Data 1',
															'lead'        => 'Lead',
															'content'     => 'Content',
															'text_data_2' => 'Text Data 2',
															'text_data_3' => 'Text Data 3',
															'text_data_4' => 'Text Data 4'
														),
														'meta' => false
													);
