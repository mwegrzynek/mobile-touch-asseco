<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Testimonials
 *
 *
 *
*/

//general settings
$config['testimonials']['itemType']       = 6;
$config['testimonials']['mainName']       = 'testimonials';
$config['testimonials']['activeMenuItem'] = 6;
$config['testimonials']['showName']       = 'Mobile Touch Testimonials';
$config['testimonials']['mainLink']       = 'items/testimonials';
$config['testimonials']['models']         = null;
$config['testimonials']['ifFiltered']     = true;
$config['testimonials']['secondMenu'] = array(
											0 => array(	"name"   => "See all",
															"link"   => "items/testimonials"),

											1 => array(	"name"   => "Add new testimonial",
															"link"   => "items/testimonials/addnew")
										);
$config['testimonials']['menuLastEditedVarName'] 	= 'lastEditedtestimonials';
$config['testimonials']['menuLastEditedText'] 		= 'Edit testimonial';
$config['testimonials']['menuLastEditedLink'] 		= 'items/testimonials/edit/';
$config['testimonials']['metaDataType'] = -1;
$config['testimonials']['nestedNodes'] = FALSE;
$config['testimonials']['categorized'] = FALSE;
$config['testimonials']['categoryType'] = -1;
$config['testimonials']['categoryLink'] = 'category/cattestimonials';

$config['testimonials']['connectedWithItems'] = array(
																	array(
																		'id'           =>  5,
																		'itemsorderby' => 'title',
																		'itemsorder'   => 'asc'
																	)
															); //item type ID array

/*
$config['testimonials']['categorizedSecond'] = FALSE;
$config['testimonials']['categorySecondType'] = -1;
$config['testimonials']['categorySecondLink'] = 'category/';
$config['testimonials']['indexCategorySecondFilterWithChildren'] = FALSE;*/

//extended field [filename => default]
//$config['testimonials']['extendedFields'] = null;

//$config['testimonials']['isRestrictedContent'] = FALSE;

//main picture settings
$config['testimonials']['allowedTypes'] 					= 'jpg|png';
$config['testimonials']['mainPicture'] 					= 1;
$config['testimonials']['mainPictureMandatory'] 		= 1;
$config['testimonials']['mainPictureMandatoryError'] 	= 'Field <strong>image</strong> is mandatory.';
$config['testimonials']['mainPicturePath'] 				= '../pictures/testimonials';
$config['testimonials']['mainPictureMaxWidth'] 			= 2000;
$config['testimonials']['mainPictureMaxHeight'] 		= 2000;
$config['testimonials']['mainPictureMinWidth'] 			= 140;
$config['testimonials']['mainPictureMinHeight'] 		= 190;
$config['testimonials']['mainPictureThumbWidth'] 		= 140;
$config['testimonials']['mainPictureThumbsInfo'] 		= array(
																			0 => array( 'width' => 100, 'height' => 100 ),
																			1 => array( 'width' => 100, 'height' => 50 )
																		);

//gallery pictures
$config['testimonials']['galleryPicturesPath']   	 = '../pictures/testimonials';
$config['testimonials']['galleryPicturesThumbsInfo'] = array(
																		0 => array( 'width' => 100, 'height' => 100 ),
																		1 => array( 'width' => 100, 'height' => 50 )
																	);

//index page
$config['testimonials']['indexAdditional']['css']     = array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['testimonials']['indexAdditional']['js']      = null;
$config['testimonials']['indexAdditional']['outerjs'] = null;
$config['testimonials']['indexItemsOrderBy']          = 'title';
$config['testimonials']['indexItemsOrder']            = 'asc';
$config['testimonials']['indexTemplate']              = 'items/testimonials/showItems.tpl';

//addnew page
$config['testimonials']['addNewAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['testimonials']['addNewAdditional']['js'] 		= array('tiny.init.js');
$config['testimonials']['addNewAdditional']['outerjs']	= array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																	));
$config['testimonials']['addNewTemplate'] 				= 'items/testimonials/itemsAddAction.tpl';
$config['testimonials']['addNewDefaultHeader'] 		= 'Add new Testimonial';
$config['testimonials']['addNewDefaultSubHeader'] 	= '';
$config['testimonials']['addNewSuccessHeader'] 		= 'Data has been successfully saved.';
$config['testimonials']['addNewSuccessHeaderAllLangs'] = 'Data has been successfully saved in all languages';
$config['testimonials']['addNewSuccessSubHeader'] 	= '';
$config['testimonials']['addNewErrorHeader'] 		= 'Data has not been saved.';
$config['testimonials']['addNewErrorSubHeader'] 	= '';
$config['testimonials']['addNewValidationConfig']	= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'First and last name',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Testimonial',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Description',
																			'rules'   => 'trim'
																		)
																);

//edit page
$config['testimonials']['editAdditional']['css']		= array('jquery.fancybox.css');//array('uitheme/cupertino/jquery-ui-1.7.1.custom.css', 'jquery.fancybox.css');
$config['testimonials']['editAdditional']['js']		= array('tiny.init.js');
$config['testimonials']['editAdditional']['outerjs']	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config['testimonials']['editTemplate']					= 'items/testimonials/itemsEditAction.tpl';
$config['testimonials']['editDefaultTabName']			= 'Edit testimonial';
$config['testimonials']['editDefaultHeader'] 			= 'Edit testimonial';
$config['testimonials']['editDefaultSubHeader'] 		= '';
$config['testimonials']['editSuccessHeader'] 			= 'Data has been successfully saved.';
$config['testimonials']['editSuccessSubHeader'] 		= '';
$config['testimonials']['editErrorHeader']				= 'Data has not been saved.';
$config['testimonials']['editErrorSubHeader']			= '';
$config['testimonials']['editValidationConfig']		= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'First and last name',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Testimonial',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Description',
																			'rules'   => 'trim'
																		)
																);


//remove page
$config['testimonials']['advancedRemove']              = FALSE;
$config['testimonials']['removeAdditional']['css']     = array();
$config['testimonials']['removeAdditional']['js']      = array();
$config['testimonials']['removeAdditional']['outerjs'] = array();
$config['testimonials']['removeTemplate']              = 'items/testimonials/removeItem.tpl';
$config['testimonials']['removeDefaultTabName']        = 'Delete team leader';
$config['testimonials']['removeDefaultHeader']         = 'Delete team leader';
$config['testimonials']['removeDefaultSubHeader']      = '';
$config['testimonials']['removeSuccessHeader']         = 'Testimonial has been removed:';
$config['testimonials']['removeSuccessSubHeader']      = '';
$config['testimonials']['removeErrorHeader']           = 'Testimonial has not been removed.';
$config['testimonials']['removeErrorSubHeader']        = '';
// import / export settings
$config['testimonials']['import']  		= array(
														'fields' => array(
															'id'          => 'Id',
															'title'       => 'Title',
															'text_data_1' => 'Company position',
															'lead'        => 'Testimonial'
														),
														'meta' => false
													);
