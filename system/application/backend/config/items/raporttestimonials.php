<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Team Leaders
 *
 *
 *
*/

//general settings
$config['raporttestimonials']['itemType']       = 29;
$config['raporttestimonials']['mainName']       = 'raporttestimonials';
$config['raporttestimonials']['activeMenuItem'] = 7;
$config['raporttestimonials']['showName']       = 'Raport Testimonials';
$config['raporttestimonials']['mainLink']       = 'items/raporttestimonials';
$config['raporttestimonials']['models']         = null;
$config['raporttestimonials']['ifFiltered'] = true;
$config['raporttestimonials']['secondMenu'] = array(
											0 => array(	"name"   => "See all",
															"link"   => "items/raporttestimonials"),

											1 => array(	"name"   => "Add new testimonial",
															"link"   => "items/raporttestimonials/addnew")
										);
$config['raporttestimonials']['menuLastEditedVarName'] 	= 'lastEditedRaporttestimonials';
$config['raporttestimonials']['menuLastEditedText'] 		= 'Edit testimonial';
$config['raporttestimonials']['menuLastEditedLink'] 		= 'items/raporttestimonials/edit/';
$config['raporttestimonials']['metaDataType'] = -1;
$config['raporttestimonials']['nestedNodes'] = FALSE;
$config['raporttestimonials']['categorized'] = FALSE;
$config['raporttestimonials']['categoryType'] = -1;
$config['raporttestimonials']['categoryLink'] = 'category/catraporttestimonials';

$config['raporttestimonials']['categorizedSecond'] = FALSE;
$config['raporttestimonials']['categorySecondType'] = -1;
$config['raporttestimonials']['categorySecondLink'] = 'category/';
$config['raporttestimonials']['indexCategorySecondFilterWithChildren'] = FALSE;

//extended field [filedname => default]
$config['raporttestimonials']['extendedFields'] = null;

$config['raporttestimonials']['isRestrictedContent'] = FALSE;

//main picture settings
$config['raporttestimonials']['allowedTypes']              = 'jpg|png';
$config['raporttestimonials']['mainPicture']               = 1;
$config['raporttestimonials']['mainPictureMandatory']      = 1;
$config['raporttestimonials']['mainPictureMandatoryError'] = 'Field <strong>image</strong> is mandatory.';
$config['raporttestimonials']['mainPicturePath']           = '../userfiles/raporttestimonials';
$config['raporttestimonials']['mainPictureMaxWidth']       = 139;
$config['raporttestimonials']['mainPictureMaxHeight']      = 124;
$config['raporttestimonials']['mainPictureMinWidth']       = 139;
$config['raporttestimonials']['mainPictureMinHeight']      = 124;
/*$config['raporttestimonials']['mainPictureThumbsInfo'] 		= array(
																			0 => array( 'width' => 100, 'height' => 100 ),
																			1 => array( 'width' => 100, 'height' => 50 )
																		);*/

//gallery pictures
$config['raporttestimonials']['galleryPicturesPath']   	 = '../pictures/raporttestimonials';
$config['raporttestimonials']['galleryPicturesThumbsInfo'] = array(
																		0 => array( 'width' => 100, 'height' => 100 ),
																		1 => array( 'width' => 100, 'height' => 50 )
																	);

//index page
$config['raporttestimonials']['indexAdditional']['css']     = array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['raporttestimonials']['indexAdditional']['js']      = null;
$config['raporttestimonials']['indexAdditional']['outerjs'] = null;
$config['raporttestimonials']['indexItemsOrderBy']          = 'title';
$config['raporttestimonials']['indexItemsOrder']            = 'asc';
$config['raporttestimonials']['indexTemplate']              = 'items/raporttestimonials/showItems.tpl';

//addnew page
$config['raporttestimonials']['addNewAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['raporttestimonials']['addNewAdditional']['js'] 		= array('tiny.init.js');
$config['raporttestimonials']['addNewAdditional']['outerjs']	= array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																	));
$config['raporttestimonials']['addNewTemplate']              = 'items/raporttestimonials/itemsAddAction.tpl';
$config['raporttestimonials']['addNewDefaultHeader']         = 'Add new testimonial';
$config['raporttestimonials']['addNewDefaultSubHeader']      = '';
$config['raporttestimonials']['addNewSuccessHeader']         = 'Data has been successfully saved.';
$config['raporttestimonials']['addNewSuccessHeaderAllLangs'] = 'Data has been successfully saved in all languages.';
$config['raporttestimonials']['addNewSuccessSubHeader']      = '';
$config['raporttestimonials']['addNewErrorHeader']           = 'Data has not been saved.';
$config['raporttestimonials']['addNewErrorSubHeader']        = '';
$config['raporttestimonials']['addNewValidationConfig']	= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'First and last name',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Testimonial',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Description',
																			'rules'   => 'trim'
																		)
																);

//edit page
$config['raporttestimonials']['editAdditional']['css']		= array('jquery.fancybox.css');//array('uitheme/cupertino/jquery-ui-1.7.1.custom.css', 'jquery.fancybox.css');
$config['raporttestimonials']['editAdditional']['js']		= array('tiny.init.js');
$config['raporttestimonials']['editAdditional']['outerjs']	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config['raporttestimonials']['editTemplate']					= 'items/raporttestimonials/itemsEditAction.tpl';
$config['raporttestimonials']['editDefaultTabName']			= 'Edit testimonial';
$config['raporttestimonials']['editDefaultHeader'] 			= 'Edit testimonial';
$config['raporttestimonials']['editDefaultSubHeader'] 		= '';
$config['raporttestimonials']['editSuccessHeader'] 			= 'Data has been successfully saved.';
$config['raporttestimonials']['editSuccessSubHeader'] 		= '';
$config['raporttestimonials']['editErrorHeader']				= 'Data has not been saved.';
$config['raporttestimonials']['editErrorSubHeader']			= '';
$config['raporttestimonials']['editValidationConfig']		= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'First and last name',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Testimonial',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Description',
																			'rules'   => 'trim'
																		)
																);


//remove page
$config['raporttestimonials']['advancedRemove']              = FALSE;
$config['raporttestimonials']['removeAdditional']['css']     = array();
$config['raporttestimonials']['removeAdditional']['js']      = array();
$config['raporttestimonials']['removeAdditional']['outerjs'] = array();
$config['raporttestimonials']['removeTemplate']              = 'items/raporttestimonials/removeItem.tpl';
$config['raporttestimonials']['removeDefaultTabName']        = 'Delete testimonial';
$config['raporttestimonials']['removeDefaultHeader']         = 'Delete testimonial';
$config['raporttestimonials']['removeDefaultSubHeader']      = '';
$config['raporttestimonials']['removeSuccessHeader']         = 'Testimonial has been removed:';
$config['raporttestimonials']['removeSuccessSubHeader']      = '';
$config['raporttestimonials']['removeErrorHeader']           = 'Testimonial has not been removed.';
$config['raporttestimonials']['removeErrorSubHeader']        = '';

// import / export settings
$config['raporttestimonials']['import']  		= array(
														'fields' => array(
															'id'          => 'Id',
															'title'       => 'Name and position',
															'lead'        => 'Testimonial'
														),
														'meta' => false
													);
