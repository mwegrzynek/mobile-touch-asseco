<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*
 * Concepts pages
 *
 *
 *
*/

//general settings
$config['conceptspages']['itemType']       = 15;
$config['conceptspages']['mainName']       = 'conceptspages';
$config['conceptspages']['activeMenuItem'] = 2;
$config['conceptspages']['showName']       = 'Mobile Touch Concepts';
$config['conceptspages']['mainLink']       = 'items/conceptspages';
$config['conceptspages']['models']         = null;
$config['conceptspages']['ifFiltered']     = true;
$config['conceptspages']['secondMenu'] = array(
											0 => array(	"name"   => "See all",
															"link"   => "items/conceptspages"),

											1 => array(	"name"   => "Add new page",
															"link"   => "items/conceptspages/addnew")
										);
$config['conceptspages']['menuLastEditedVarName'] = 'lastEditedSite';
$config['conceptspages']['menuLastEditedText'] 	= 'Edit last edited:';
$config['conceptspages']['menuLastEditedLink'] 	= 'items/conceptspages/edit/';
$config['conceptspages']['metaDataType'] = 15;
$config['conceptspages']['nestedNodes'] = FALSE;
$config['conceptspages']['categorized'] = TRUE;
$config['conceptspages']['categoryType'] = 1;
$config['conceptspages']['categoryLink'] = 'category/industries';
$config['conceptspages']['indexCategoryFilterWithChildren'] = FALSE;
$config['conceptspages']['indexParentFilterWithChildren'] 	= FALSE;


$config['conceptspages']['categorizedSecond'] = FALSE;
$config['conceptspages']['categorySecondType'] = -1;
$config['conceptspages']['categorySecondLink'] = 'category/';
$config['conceptspages']['indexCategorySecondFilterWithChildren'] = FALSE;





//extended field [filedname => default]
$config['conceptspages']['extendedFields'] = null;

$config['conceptspages']['isRestrictedContent'] = FALSE;

//main picture settings
$config['conceptspages']['allowedTypes'] 					= 'jpg|png';
$config['conceptspages']['mainPicture'] 						= 1;
$config['conceptspages']['mainPictureMandatory'] 			= 0;
$config['conceptspages']['mainPictureMandatoryError'] 	= 'Field <strong>image</strong> is mandatory.';
$config['conceptspages']['mainPicturePath'] 				= '../userfiles/conceptspages';
$config['conceptspages']['mainPictureMaxWidth'] 			= 630;
$config['conceptspages']['mainPictureMaxHeight'] 			= 1100;
$config['conceptspages']['mainPictureMinWidth'] 			= 30;
$config['conceptspages']['mainPictureMinHeight'] 			= 44;
$config['conceptspages']['mainPictureThumbsInfo'] 		= array(
																	0 => array( 'width' => 100, 'height' => 100 ),
																	1 => array( 'width' => 100, 'height' => 50 )
																);


//gallery pictures
$config['conceptspages']['galleryPicturesPath']   = '../pictures/conceptspages';
$config['conceptspages']['galleryPicturesThumbsInfo'] = array(
																	0 => array( 'width' => 100, 'height' => 100 ),
																	1 => array( 'width' => 100, 'height' => 50 )
																);


//index page
$config['conceptspages']['indexAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['conceptspages']['indexAdditional']['js'] 		= null;
$config['conceptspages']['indexAdditional']['outerjs'] 	= null;
$config['conceptspages']['indexItemsOrderBy'] 				= 'position';
$config['conceptspages']['indexItemsOrder'] 					= 'asc';
$config['conceptspages']['indexTemplate'] 					= 'items/conceptspages/showItems.tpl';

//addnew page
$config['conceptspages']['addNewAdditional']['css'] 		= array(); //array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['conceptspages']['addNewAdditional']['js'] 		= array('tiny.init.js');
$config['conceptspages']['addNewAdditional']['outerjs'] = array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																			));
$config['conceptspages']['addNewTemplate'] 				= 'items/conceptspages/itemsAddAction.tpl';
$config['conceptspages']['addNewDefaultHeader'] 		= 'Add new page';
$config['conceptspages']['addNewDefaultSubHeader'] 	= '';
$config['conceptspages']['addNewSuccessHeaderAllLangs']	= 'Data has been successfully saved in all languages.';
$config['conceptspages']['addNewSuccessHeader'] 		= 'Data has been successfully saved.';
$config['conceptspages']['addNewSuccessSubHeader'] 	= '';
$config['conceptspages']['addNewErrorHeader'] 			= 'Data has not been saved';
$config['conceptspages']['addNewErrorSubHeader'] 		= '';
$config['conceptspages']['addNewValidationConfig']	= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',
																			'rules'   => 'trim|required|is_natural'
																		)
																);

//edit page
$config['conceptspages']['editAdditional']['css'] 		= array('jquery.fancybox.css');//array('uitheme/cupertino/jquery-ui-1.7.1.custom.css', 'jquery.fancybox.css');
$config['conceptspages']['editAdditional']['js'] 			= array('tiny.init.js');
$config['conceptspages']['editAdditional']['outerjs'] 	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config['conceptspages']['editTemplate'] 				= 'items/conceptspages/itemsEditAction.tpl';
$config['conceptspages']['editDefaultTabName']			= 'Edit: ';
$config['conceptspages']['editDefaultHeader'] 			= 'Edit page';
$config['conceptspages']['editDefaultSubHeader'] 		= '';
$config['conceptspages']['editSuccessHeader'] 			= 'Data has been successfully saved.';
$config['conceptspages']['editSuccessSubHeader'] 		= '';
$config['conceptspages']['editErrorHeader'] 			= 'Data has not been saved.';
$config['conceptspages']['editErrorSubHeader'] 		= '';
$config['conceptspages']['editValidationConfig']		= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',
																			'rules'   => 'trim|required|is_natural'
																		)
																);


//remove page
$config['conceptspages']['advancedRemove']						= true;
$config['conceptspages']['removeAdditional']['css']			= array();
$config['conceptspages']['removeAdditional']['js']			= array();
$config['conceptspages']['removeAdditional']['outerjs']		= array();
$config['conceptspages']['removeTemplate']					= 'items/conceptspages/removeItem.tpl';
$config['conceptspages']['removeDefaultTabName']			= 'Remove page';
$config['conceptspages']['removeDefaultHeader']			= 'Remove page';
$config['conceptspages']['removeDefaultSubHeader'] 		= '';
$config['conceptspages']['removeSuccessHeader']			= 'Page has been deleted:';
$config['conceptspages']['removeSuccessSubHeader']		= '';
$config['conceptspages']['removeErrorHeader']				= 'Page has not been deleted.';
$config['conceptspages']['removeErrorSubHeader']			= '';
// import / export settings
$config['conceptspages']['import']  		= array(
														'fields' => array(
															'id'      => 'Id',
															'title'   => 'Title',
															'lead'    => 'Lead',
															'content' => 'Content'
														),
														'meta' => false
													);