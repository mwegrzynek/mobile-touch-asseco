<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*
 * Mobile Touch main mtpages
 *
 *
 *
*/

//general settings
$config['mtpages']['itemType']       = 2;
$config['mtpages']['mainName']       = 'mtpages';
$config['mtpages']['showName']       = 'Mobile Touch Pages';
$config['mtpages']['activeMenuItem'] = 2;
$config['mtpages']['mainLink']       = 'items/mtpages';
$config['mtpages']['models']         = null;
$config['mtpages']['ifFiltered']     = true;
$config['mtpages']['secondMenu']     = array(
											0 => array(	"name"   => "See all",
															"link"   => "items/mtpages")/*,

											1 => array(	"name"   => "Add new page",
												"link"   => "items/mtpages/addnew")*/
										);
$config['mtpages']['menuLastEditedVarName'] = 'lastEditedSite';
$config['mtpages']['menuLastEditedText'] 	= 'Edit last edited:';
$config['mtpages']['menuLastEditedLink'] 	= 'items/mtpages/edit/';
$config['mtpages']['metaDataType'] = 2;
$config['mtpages']['nestedNodes'] = FALSE;
$config['mtpages']['categorized'] = FALSE;
$config['mtpages']['categoryType'] = 1;
$config['mtpages']['categoryLink'] = 'category/industries';
$config['mtpages']['indexCategoryFilterWithChildren'] = FALSE;
$config['mtpages']['indexParentFilterWithChildren'] 	= FALSE;


$config['mtpages']['categorizedSecond'] = FALSE;
$config['mtpages']['categorySecondType'] = -1;
$config['mtpages']['categorySecondLink'] = 'category/';
$config['mtpages']['indexCategorySecondFilterWithChildren'] = FALSE;





//extended field [filedname => default]
$config['mtpages']['extendedFields'] = null;

$config['mtpages']['isRestrictedContent'] = FALSE;

//main picture settings
$config['mtpages']['allowedTypes']              = 'jpg|png';
$config['mtpages']['mainPicture']               = 1;
$config['mtpages']['mainPictureMandatory']      = 0;
$config['mtpages']['mainPictureMandatoryError'] = 'Field <strong>image</strong> is mandatory.';
$config['mtpages']['mainPicturePath']           = '../userfiles/mtpages';
$config['mtpages']['mainPictureMaxWidth'] 		= 700;
$config['mtpages']['mainPictureMaxHeight'] 		= 1000;
$config['mtpages']['mainPictureMinWidth'] 		= 50;
$config['mtpages']['mainPictureMinHeight'] 		= 50;
$config['mtpages']['mainPictureThumbWidth'] 		= 0;


//gallery pictures
$config['mtpages']['galleryPicturesPath']   = '../pictures/mtpages';
$config['mtpages']['galleryPicturesThumbsInfo'] = array(
																	0 => array( 'width' => 100, 'height' => 100 ),
																	1 => array( 'width' => 100, 'height' => 50 )
																);


//index page
$config['mtpages']['indexAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['mtpages']['indexAdditional']['js'] 			= null;
$config['mtpages']['indexAdditional']['outerjs'] 	= null;
$config['mtpages']['indexItemsOrderBy'] 				= 'position';
$config['mtpages']['indexItemsOrder'] 					= 'asc';
$config['mtpages']['indexTemplate'] 					= 'items/mtpages/showItems.tpl';

//addnew page
$config['mtpages']['addNewAdditional']['css'] 		= array(); //array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['mtpages']['addNewAdditional']['js'] 		= array('tiny.init.js');
$config['mtpages']['addNewAdditional']['outerjs'] = array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																			));
$config['mtpages']['addNewTemplate'] 				= 'items/mtpages/itemsAddAction.tpl';
$config['mtpages']['addNewDefaultHeader'] 		= 'Add new page';
$config['mtpages']['addNewDefaultSubHeader'] 	= '';
$config['mtpages']['addNewSuccessHeaderAllLangs']	= 'Data has been successfully saved in all languages.';
$config['mtpages']['addNewSuccessHeader'] 		= 'Data has been successfully saved.';
$config['mtpages']['addNewSuccessSubHeader'] 	= '';
$config['mtpages']['addNewErrorHeader'] 			= 'Data has not been saved';
$config['mtpages']['addNewErrorSubHeader'] 		= '';
$config['mtpages']['addNewValidationConfig']	= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',
																			'rules'   => 'trim'
																		)
																);

//edit page
$config['mtpages']['editAdditional']['css'] 		= array('jquery.fancybox.css');//array('uitheme/cupertino/jquery-ui-1.7.1.custom.css', 'jquery.fancybox.css');
$config['mtpages']['editAdditional']['js'] 			= array('tiny.init.js');
$config['mtpages']['editAdditional']['outerjs'] 	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config['mtpages']['editTemplate'] 				= 'items/mtpages/itemsEditAction.tpl';
$config['mtpages']['editDefaultTabName']			= 'Edit: ';
$config['mtpages']['editDefaultHeader'] 			= 'Edit page';
$config['mtpages']['editDefaultSubHeader'] 		= '';
$config['mtpages']['editSuccessHeader'] 			= 'Data has been successfully saved.';
$config['mtpages']['editSuccessSubHeader'] 		= '';
$config['mtpages']['editErrorHeader'] 			= 'Data has not been saved.';
$config['mtpages']['editErrorSubHeader'] 		= '';
$config['mtpages']['editValidationConfig']		= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',
																			'rules'   => 'trim'
																		)
																);


//remove page
$config['mtpages']['advancedRemove']              = true;
$config['mtpages']['removeAdditional']['css']     = array();
$config['mtpages']['removeAdditional']['js']      = array();
$config['mtpages']['removeAdditional']['outerjs'] = array();
$config['mtpages']['removeTemplate']              = 'items/mtpages/removeItem.tpl';
$config['mtpages']['removeDefaultTabName']        = 'Remove page';
$config['mtpages']['removeDefaultHeader']         = 'Remove page';
$config['mtpages']['removeDefaultSubHeader']      = '';
$config['mtpages']['removeSuccessHeader']         = 'Page has been deleted:';
$config['mtpages']['removeSuccessSubHeader']      = '';
$config['mtpages']['removeErrorHeader']           = 'Page has not been deleted.';
$config['mtpages']['removeErrorSubHeader']        = '';

// import / export settings
$config['mtpages']['import']  		= array(
														'fields' => array(
															'id'          => 'Id',
															'title'       => 'Title',
															'text_data_2' => 'Lead',
															'text_data_3' => 'Text Data',
															'lead'        => 'Text Data',
															'text_data_1' => 'Text Data'
														),
														'meta' => false
													);
