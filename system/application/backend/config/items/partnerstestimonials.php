<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Team Leaders
 *
 *
 *
*/

//general settings
$config['partnerstestimonials']['itemType']       = 30;
$config['partnerstestimonials']['mainName']       = '';
$config['partnerstestimonials']['activeMenuItem'] = 7;
$config['partnerstestimonials']['showName']       = 'Raport Testimonials';
$config['partnerstestimonials']['mainLink']       = 'items/partnerstestimonials';
$config['partnerstestimonials']['models']         = null;
$config['partnerstestimonials']['ifFiltered'] = true;
$config['partnerstestimonials']['secondMenu'] = array(
											0 => array(	"name"   => "See all",
															"link"   => "items/partnerstestimonials"),

											1 => array(	"name"   => "Add new testimonial",
															"link"   => "items/partnerstestimonials/addnew")
										);
$config['partnerstestimonials']['menuLastEditedVarName'] 	= 'lastEditedRaporttestimonials';
$config['partnerstestimonials']['menuLastEditedText'] 		= 'Edit testimonial';
$config['partnerstestimonials']['menuLastEditedLink'] 		= 'items/partnerstestimonials/edit/';
$config['partnerstestimonials']['metaDataType'] = -1;
$config['partnerstestimonials']['nestedNodes'] = FALSE;
$config['partnerstestimonials']['categorized'] = FALSE;
$config['partnerstestimonials']['categoryType'] = -1;
$config['partnerstestimonials']['categoryLink'] = 'category/catpartnerstestimonials';

$config['partnerstestimonials']['categorizedSecond'] = FALSE;
$config['partnerstestimonials']['categorySecondType'] = -1;
$config['partnerstestimonials']['categorySecondLink'] = 'category/';
$config['partnerstestimonials']['indexCategorySecondFilterWithChildren'] = FALSE;

//extended field [filedname => default]
$config['partnerstestimonials']['extendedFields'] = null;

$config['partnerstestimonials']['isRestrictedContent'] = FALSE;

//main picture settings
$config['partnerstestimonials']['allowedTypes']              = 'jpg|png';
$config['partnerstestimonials']['mainPicture']               = 1;
$config['partnerstestimonials']['mainPictureMandatory']      = 1;
$config['partnerstestimonials']['mainPictureMandatoryError'] = 'Field <strong>image</strong> is mandatory.';
$config['partnerstestimonials']['mainPicturePath']           = '../userfiles/partnerstestimonials';
$config['partnerstestimonials']['mainPictureMaxWidth']       = 139;
$config['partnerstestimonials']['mainPictureMaxHeight']      = 124;
$config['partnerstestimonials']['mainPictureMinWidth']       = 139;
$config['partnerstestimonials']['mainPictureMinHeight']      = 124;
/*$config['partnerstestimonials']['mainPictureThumbsInfo'] 		= array(
																			0 => array( 'width' => 100, 'height' => 100 ),
																			1 => array( 'width' => 100, 'height' => 50 )
																		);*/

//gallery pictures
$config['partnerstestimonials']['galleryPicturesPath']   	 = '../pictures/partnerstestimonials';
$config['partnerstestimonials']['galleryPicturesThumbsInfo'] = array(
																		0 => array( 'width' => 100, 'height' => 100 ),
																		1 => array( 'width' => 100, 'height' => 50 )
																	);

//index page
$config['partnerstestimonials']['indexAdditional']['css']     = array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['partnerstestimonials']['indexAdditional']['js']      = null;
$config['partnerstestimonials']['indexAdditional']['outerjs'] = null;
$config['partnerstestimonials']['indexItemsOrderBy']          = 'title';
$config['partnerstestimonials']['indexItemsOrder']            = 'asc';
$config['partnerstestimonials']['indexTemplate']              = 'items/partnerstestimonials/showItems.tpl';

//addnew page
$config['partnerstestimonials']['addNewAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['partnerstestimonials']['addNewAdditional']['js'] 		= array('tiny.init.js');
$config['partnerstestimonials']['addNewAdditional']['outerjs']	= array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																	));
$config['partnerstestimonials']['addNewTemplate']              = 'items/partnerstestimonials/itemsAddAction.tpl';
$config['partnerstestimonials']['addNewDefaultHeader']         = 'Add new testimonial';
$config['partnerstestimonials']['addNewDefaultSubHeader']      = '';
$config['partnerstestimonials']['addNewSuccessHeader']         = 'Data has been successfully saved.';
$config['partnerstestimonials']['addNewSuccessHeaderAllLangs'] = 'Data has been successfully saved in all languages.';
$config['partnerstestimonials']['addNewSuccessSubHeader']      = '';
$config['partnerstestimonials']['addNewErrorHeader']           = 'Data has not been saved.';
$config['partnerstestimonials']['addNewErrorSubHeader']        = '';
$config['partnerstestimonials']['addNewValidationConfig']	= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'First and last name',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Testimonial',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Description',
																			'rules'   => 'trim'
																		)
																);

//edit page
$config['partnerstestimonials']['editAdditional']['css']		= array('jquery.fancybox.css');//array('uitheme/cupertino/jquery-ui-1.7.1.custom.css', 'jquery.fancybox.css');
$config['partnerstestimonials']['editAdditional']['js']		= array('tiny.init.js');
$config['partnerstestimonials']['editAdditional']['outerjs']	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config['partnerstestimonials']['editTemplate']					= 'items/partnerstestimonials/itemsEditAction.tpl';
$config['partnerstestimonials']['editDefaultTabName']			= 'Edit testimonial';
$config['partnerstestimonials']['editDefaultHeader'] 			= 'Edit testimonial';
$config['partnerstestimonials']['editDefaultSubHeader'] 		= '';
$config['partnerstestimonials']['editSuccessHeader'] 			= 'Data has been successfully saved.';
$config['partnerstestimonials']['editSuccessSubHeader'] 		= '';
$config['partnerstestimonials']['editErrorHeader']				= 'Data has not been saved.';
$config['partnerstestimonials']['editErrorSubHeader']			= '';
$config['partnerstestimonials']['editValidationConfig']		= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'First and last name',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Testimonial',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Description',
																			'rules'   => 'trim'
																		)
																);


//remove page
$config['partnerstestimonials']['advancedRemove']              = FALSE;
$config['partnerstestimonials']['removeAdditional']['css']     = array();
$config['partnerstestimonials']['removeAdditional']['js']      = array();
$config['partnerstestimonials']['removeAdditional']['outerjs'] = array();
$config['partnerstestimonials']['removeTemplate']              = 'items/partnerstestimonials/removeItem.tpl';
$config['partnerstestimonials']['removeDefaultTabName']        = 'Delete testimonial';
$config['partnerstestimonials']['removeDefaultHeader']         = 'Delete testimonial';
$config['partnerstestimonials']['removeDefaultSubHeader']      = '';
$config['partnerstestimonials']['removeSuccessHeader']         = 'Testimonial has been removed:';
$config['partnerstestimonials']['removeSuccessSubHeader']      = '';
$config['partnerstestimonials']['removeErrorHeader']           = 'Testimonial has not been removed.';
$config['partnerstestimonials']['removeErrorSubHeader']        = '';

// import / export settings
$config['partnerstestimonials']['import']  		= array(
														'fields' => array(
															'id'          => 'Id',
															'title'       => 'Name and position',
															'lead'        => 'Testimonial'
														),
														'meta' => false
													);
