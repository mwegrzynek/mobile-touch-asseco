<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Clients
 *
 *
 *
*/

//general settings
$config['clients']['itemType'] = 5;
$config['clients']['mainName'] = 'clients';
$config['clients']['activeMenuItem'] = 6;
$config['clients']['showName']       = 'Mobile Touch Clients';
$config['clients']['mainLink'] = 'items/clients';
$config['clients']['models'] = null;
$config['clients']['ifFiltered'] = true;
$config['clients']['secondMenu'] = array(
											0 => array(	"name"   => "See all",
															"link"   => "items/clients"),

											1 => array(	"name"   => "Add client",
															"link"   => "items/clients/addnew")
										);
$config['clients']['menuLastEditedVarName'] 	= 'lastEditedclients';
$config['clients']['menuLastEditedText'] 		= 'Edit client';
$config['clients']['menuLastEditedLink'] 		= 'items/clients/edit/';
$config['clients']['metaDataType'] = 5;
$config['clients']['nestedNodes'] = FALSE;
$config['clients']['categorized'] = TRUE;
$config['clients']['categoryType'] = 1;
$config['clients']['categoryLink'] = 'category/industries';

$config['clients']['categorizedSecond'] = FALSE;
$config['clients']['categorySecondType'] = -1;
$config['clients']['categorySecondLink'] = 'category/';
$config['clients']['indexCategorySecondFilterWithChildren'] = FALSE;

//extended field [filedname => default]
$config['clients']['extendedFields'] = null;

$config['clients']['isRestrictedContent'] = FALSE;

//main picture settings
$config['clients']['allowedTypes'] 					= 'png';
$config['clients']['mainPicture'] 						= 1;
$config['clients']['mainPictureMandatory'] 			= 1;
$config['clients']['mainPictureMandatoryError'] 	= 'Field <strong>image</strong> is mandatory.';
$config['clients']['mainPicturePath'] 				= '../userfiles/clients';
$config['clients']['mainPictureMaxWidth'] 		= 220;
$config['clients']['mainPictureMaxHeight'] 		= 500;
$config['clients']['mainPictureMinWidth'] 		= 120;
$config['clients']['mainPictureMinHeight'] 		= 100;
$config['clients']['mainPictureThumbWidth'] 		= 120;
$config['clients']['mainPictureThumbsInfo'] 		= array(
																			0 => array( 'width' => 120, 'height' => 0 ),
																			1 => array( 'width' => 120, 'height' => 0, 'grey' => true )
																		);

//gallery pictures
$config['clients']['galleryPicturesPath']   	 = '../pictures/clients';
$config['clients']['galleryPicturesThumbsInfo'] = array(
																		0 => array( 'width' => 100, 'height' => 100 ),
																		1 => array( 'width' => 100, 'height' => 50 )
																	);

//index page
$config['clients']['indexAdditional']['css']     = array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['clients']['indexAdditional']['js']      = null;
$config['clients']['indexAdditional']['outerjs'] = null;
$config['clients']['indexItemsOrderBy']          = 'title';
$config['clients']['indexItemsOrder']            = 'asc';
$config['clients']['indexTemplate']              = 'items/clients/showItems.tpl';

//addnew page
$config['clients']['addNewAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['clients']['addNewAdditional']['js'] 		= array('tiny.init.js');
$config['clients']['addNewAdditional']['outerjs']	= array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																	));
$config['clients']['addNewTemplate'] 				= 'items/clients/itemsAddAction.tpl';
$config['clients']['addNewDefaultHeader'] 		= 'Add new client';
$config['clients']['addNewDefaultSubHeader'] 	= '';
$config['clients']['addNewSuccessHeader'] 		= 'Data has been successfully saved.';
$config['clients']['addNewSuccessSubHeader'] 	= '';
$config['clients']['addNewErrorHeader'] 			= 'Data has not been saved.';
$config['clients']['addNewErrorSubHeader'] 		= '';
$config['clients']['addNewValidationConfig']	= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Name',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Description',
																			'rules'   => 'trim'
																		)
																);

//edit page
$config['clients']['editAdditional']['css']		= array('jquery.fancybox.css');//array('uitheme/cupertino/jquery-ui-1.7.1.custom.css', 'jquery.fancybox.css');
$config['clients']['editAdditional']['js']		= array('tiny.init.js');
$config['clients']['editAdditional']['outerjs']	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config['clients']['editTemplate']					= 'items/clients/itemsEditAction.tpl';
$config['clients']['editDefaultTabName']			= 'Edit client';
$config['clients']['editDefaultHeader'] 			= 'Edit client';
$config['clients']['editDefaultSubHeader'] 		= '';
$config['clients']['editSuccessHeader'] 			= 'Data has been successfully saved.';
$config['clients']['editSuccessSubHeader'] 		= '';
$config['clients']['editErrorHeader']				= 'Data has not been saved.';
$config['clients']['editErrorSubHeader']			= '';
$config['clients']['editValidationConfig']		= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Name',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Description',
																			'rules'   => 'trim'
																		)
																);


//remove page
$config['clients']['advancedRemove']              = TRUE;
$config['clients']['removeAdditional']['css']     = array();
$config['clients']['removeAdditional']['js']      = array();
$config['clients']['removeAdditional']['outerjs'] = array();
$config['clients']['removeTemplate']              = 'items/clients/removeItem.tpl';
$config['clients']['removeDefaultTabName']        = 'Delete client';
$config['clients']['removeDefaultHeader']         = 'Delete client';
$config['clients']['removeDefaultSubHeader']      = '';
$config['clients']['removeSuccessHeader']         = 'Client has been removed:';
$config['clients']['removeSuccessSubHeader']      = '';
$config['clients']['removeErrorHeader']           = 'Client has not been removed.';
$config['clients']['removeErrorSubHeader']        = '';
// import / export settings
$config['clients']['import']  		= array(
														'fields' => array(
															'id'      => 'Id',
															'title'   => 'Title',
															'lead'    => 'Lead',
															'content' => 'Content'
														),
														'meta' => false
													);
