<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Team Leaders
 *
 *
 *
*/

$indexName = "whyus";

//general settings
$config[$indexName]['itemType'] = 26;
$config[$indexName]['mainName'] = $indexName;
$config[$indexName]['activeMenuItem'] = 15;
$config[$indexName]['showName']       = 'Mobile Touch Pages - Why Us';
$config[$indexName]['mainLink'] = 'items/'.$indexName;
$config[$indexName]['models'] = null;
$config[$indexName]['ifFiltered'] = true;
$config[$indexName]['secondMenu'] = array(
											0 => array(	"name"   => "See all",
															"link"   => 'items/'.$indexName),

											1 => array(	"name"   => "Add new item",
															"link"   => "items/".$indexName."/addnew")
										);
$config[$indexName]['menuLastEditedVarName'] 	= 'lastEdited'.$indexName;
$config[$indexName]['menuLastEditedText'] 		= 'Edit item';
$config[$indexName]['menuLastEditedLink'] 		= 'items/'.$indexName.'/edit/';
$config[$indexName]['metaDataType'] = -1;
$config[$indexName]['nestedNodes'] = FALSE;
$config[$indexName]['categorized'] = FALSE;
$config[$indexName]['categoryType'] = -1;
$config[$indexName]['categoryLink'] = '';

$config[$indexName]['categorizedSecond'] = FALSE;
$config[$indexName]['categorySecondType'] = -1;
$config[$indexName]['categorySecondLink'] = 'category/';
$config[$indexName]['indexCategorySecondFilterWithChildren'] = FALSE;

//extended field [filedname => default]
$config[$indexName]['extendedFields'] = null;

$config[$indexName]['isRestrictedContent'] = FALSE;

//main picture settings
$config[$indexName]['allowedTypes'] 					= 'jpg|png';
$config[$indexName]['mainPicture'] 						= 0;
$config[$indexName]['mainPictureMandatory'] 			= 0;
$config[$indexName]['mainPictureMandatoryError'] 	= 'Field <strong>image</strong> is mandatory.';
$config[$indexName]['mainPicturePath'] 				= '../userfiles/'.$indexName;
$config[$indexName]['mainPictureMaxWidth'] 			= 139;
$config[$indexName]['mainPictureMaxHeight'] 			= 124;
$config[$indexName]['mainPictureMinWidth'] 			= 139;
$config[$indexName]['mainPictureMinHeight'] 			= 124;
/*$config[$indexName]['mainPictureThumbsInfo'] 		= array(
																			0 => array( 'width' => 100, 'height' => 100 ),
																			1 => array( 'width' => 100, 'height' => 50 )
																		);*/

//gallery pictures
$config[$indexName]['galleryPicturesPath']   	 = '../pictures/'.$indexName;
$config[$indexName]['galleryPicturesThumbsInfo'] = array(
																		0 => array( 'width' => 100, 'height' => 100 ),
																		1 => array( 'width' => 100, 'height' => 50 )
																	);

//index page
$config[$indexName]['indexAdditional']['css']     = array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config[$indexName]['indexAdditional']['js']      = null;
$config[$indexName]['indexAdditional']['outerjs'] = null;
$config[$indexName]['indexItemsOrderBy']          = 'position';
$config[$indexName]['indexItemsOrder']            = 'asc';
$config[$indexName]['indexTemplate']              = 'items/'.$indexName.'/showItems.tpl';

//addnew page
$config[$indexName]['addNewAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config[$indexName]['addNewAdditional']['js'] 		= array('tiny.init.js');
$config[$indexName]['addNewAdditional']['outerjs']	= array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																	));
$config[$indexName]['addNewTemplate'] 				= 'items/'.$indexName.'/itemsAddAction.tpl';
$config[$indexName]['addNewDefaultHeader'] 		= 'Add new item';
$config[$indexName]['addNewDefaultSubHeader'] 	= '';
$config[$indexName]['addNewSuccessHeader'] 		= 'Data has been successfully saved.';
$config[$indexName]['addNewSuccessHeaderAllLangs'] = 'Data has been successfully saved in all languages.';
$config[$indexName]['addNewSuccessSubHeader'] 	= '';
$config[$indexName]['addNewErrorHeader'] 			= 'Data has not been saved.';
$config[$indexName]['addNewErrorSubHeader'] 		= '';
$config[$indexName]['addNewValidationConfig']	= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'First and last name',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Testimonial',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Description',
																			'rules'   => 'trim'
																		)
																);

//edit page
$config[$indexName]['editAdditional']['css']		= array('jquery.fancybox.css');//array('uitheme/cupertino/jquery-ui-1.7.1.custom.css', 'jquery.fancybox.css');
$config[$indexName]['editAdditional']['js']		= array('tiny.init.js');
$config[$indexName]['editAdditional']['outerjs']	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config[$indexName]['editTemplate']					= 'items/'.$indexName.'/itemsEditAction.tpl';
$config[$indexName]['editDefaultTabName']			= 'Edit item';
$config[$indexName]['editDefaultHeader'] 			= 'Edit item';
$config[$indexName]['editDefaultSubHeader'] 		= '';
$config[$indexName]['editSuccessHeader'] 			= 'Data has been successfully saved.';
$config[$indexName]['editSuccessSubHeader'] 		= '';
$config[$indexName]['editErrorHeader']				= 'Data has not been saved.';
$config[$indexName]['editErrorSubHeader']			= '';
$config[$indexName]['editValidationConfig']		= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'First and last name',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Testimonial',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Description',
																			'rules'   => 'trim'
																		)
																);


//remove page
$config[$indexName]['advancedRemove']              = FALSE;
$config[$indexName]['removeAdditional']['css']     = array();
$config[$indexName]['removeAdditional']['js']      = array();
$config[$indexName]['removeAdditional']['outerjs'] = array();
$config[$indexName]['removeTemplate']              = 'items/'.$indexName.'/removeItem.tpl';
$config[$indexName]['removeDefaultTabName']        = 'Delete item';
$config[$indexName]['removeDefaultHeader']         = 'Delete item';
$config[$indexName]['removeDefaultSubHeader']      = '';
$config[$indexName]['removeSuccessHeader']         = 'Item has been removed:';
$config[$indexName]['removeSuccessSubHeader']      = '';
$config[$indexName]['removeErrorHeader']           = 'Item has not been removed.';
$config[$indexName]['removeErrorSubHeader']        = '';

// import / export settings
$config[$indexName]['import']  		= array(
														'fields' => array(
															'id'      => 'Id',
															'title'   => 'Title',
															'content' => 'Content'
														),
														'meta' => false
													);
