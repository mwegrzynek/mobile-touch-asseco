<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*
 * Platforma Connector Services pages
 *
 *
 *
*/

//general settings
$config['pcservicespages']['itemType'] = 24;
$config['pcservicespages']['mainName'] = 'pcservicespages';
$config['pcservicespages']['activeMenuItem'] = 3;
$config['pcservicespages']['mainLink'] = 'items/pcservicespages';
$config['pcservicespages']['models'] = null;
$config['pcservicespages']['ifFiltered'] = true;
$config['pcservicespages']['secondMenu'] = array(
											0 => array(	"name"   => "See all",
															"link"   => "items/pcservicespages"),
														
											1 => array(	"name"   => "Add new page",
															"link"   => "items/pcservicespages/addnew")
										);
$config['pcservicespages']['menuLastEditedVarName'] = 'lastEditedSite';
$config['pcservicespages']['menuLastEditedText'] 	= 'Edit last edited:';
$config['pcservicespages']['menuLastEditedLink'] 	= 'items/pcservicespages/edit/';
$config['pcservicespages']['metaDataType'] = 24;
$config['pcservicespages']['nestedNodes'] = FALSE;
$config['pcservicespages']['categorized'] = TRUE;
$config['pcservicespages']['categoryType'] = 1;
$config['pcservicespages']['categoryLink'] = 'category/industries';
$config['pcservicespages']['indexCategoryFilterWithChildren'] = FALSE;
$config['pcservicespages']['indexParentFilterWithChildren'] 	= FALSE;


$config['pcservicespages']['categorizedSecond'] = FALSE;
$config['pcservicespages']['categorySecondType'] = -1;
$config['pcservicespages']['categorySecondLink'] = 'category/';
$config['pcservicespages']['indexCategorySecondFilterWithChildren'] = FALSE;





//extended field [filedname => default]
$config['pcservicespages']['extendedFields'] = null;

$config['pcservicespages']['isRestrictedContent'] = FALSE;

//main picture settings
$config['pcservicespages']['allowedTypes'] 					= 'jpg|png';
$config['pcservicespages']['mainPicture'] 						= 1;
$config['pcservicespages']['mainPictureMandatory'] 			= 0;
$config['pcservicespages']['mainPictureMandatoryError'] 	= 'Field <strong>image</strong> is mandatory.';
$config['pcservicespages']['mainPicturePath'] 				= '../userfiles/pcservicespages';
$config['pcservicespages']['mainPictureMaxWidth'] 			= 630;
$config['pcservicespages']['mainPictureMaxHeight'] 		= 144;
$config['pcservicespages']['mainPictureMinWidth'] 			= 30;
$config['pcservicespages']['mainPictureMinHeight'] 		= 30;


//gallery pictures
$config['pcservicespages']['galleryPicturesPath']   = '../pictures/pcservicespages';
$config['pcservicespages']['galleryPicturesThumbsInfo'] = array(
																	0 => array( 'width' => 100, 'height' => 100 ),
																	1 => array( 'width' => 100, 'height' => 50 )
																);


//index page
$config['pcservicespages']['indexAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['pcservicespages']['indexAdditional']['js'] 		= null;
$config['pcservicespages']['indexAdditional']['outerjs'] 	= null;
$config['pcservicespages']['indexItemsOrderBy'] 				= 'position';
$config['pcservicespages']['indexItemsOrder'] 					= 'asc';
$config['pcservicespages']['indexTemplate'] 					= 'items/pcservicespages/showItems.tpl';

//addnew page
$config['pcservicespages']['addNewAdditional']['css'] 		= array(); //array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['pcservicespages']['addNewAdditional']['js'] 		= array('tiny.init.js');
$config['pcservicespages']['addNewAdditional']['outerjs'] = array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																			));
$config['pcservicespages']['addNewTemplate'] 				= 'items/pcservicespages/itemsAddAction.tpl';
$config['pcservicespages']['addNewDefaultHeader'] 		= 'Add new page';
$config['pcservicespages']['addNewDefaultSubHeader'] 	= '';
$config['pcservicespages']['addNewSuccessHeaderAllLangs']	= 'Data has been successfully saved in all languages.';
$config['pcservicespages']['addNewSuccessHeader'] 		= 'Data has been successfully saved.';
$config['pcservicespages']['addNewSuccessSubHeader'] 	= '';
$config['pcservicespages']['addNewErrorHeader'] 			= 'Data has not been saved';
$config['pcservicespages']['addNewErrorSubHeader'] 		= '';
$config['pcservicespages']['addNewValidationConfig']	= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',																			
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',																			
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',																			
																			'rules'   => 'trim|required|is_natural'
																		)	
																);
																
//edit page
$config['pcservicespages']['editAdditional']['css'] 		= array('jquery.fancybox.css');//array('uitheme/cupertino/jquery-ui-1.7.1.custom.css', 'jquery.fancybox.css');
$config['pcservicespages']['editAdditional']['js'] 			= array('tiny.init.js');
$config['pcservicespages']['editAdditional']['outerjs'] 	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config['pcservicespages']['editTemplate'] 				= 'items/pcservicespages/itemsEditAction.tpl';
$config['pcservicespages']['editDefaultTabName']			= 'Edit: ';
$config['pcservicespages']['editDefaultHeader'] 			= 'Edit page';
$config['pcservicespages']['editDefaultSubHeader'] 		= '';
$config['pcservicespages']['editSuccessHeader'] 			= 'Data has been successfully saved.';
$config['pcservicespages']['editSuccessSubHeader'] 		= '';
$config['pcservicespages']['editErrorHeader'] 			= 'Data has not been saved.';
$config['pcservicespages']['editErrorSubHeader'] 		= '';
$config['pcservicespages']['editValidationConfig']		= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',																			
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',																			
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',																			
																			'rules'   => 'trim|required|is_natural'
																		)	
																);
																
																
//remove page
$config['pcservicespages']['advancedRemove']						= true;	
$config['pcservicespages']['removeAdditional']['css']			= array();
$config['pcservicespages']['removeAdditional']['js']			= array();
$config['pcservicespages']['removeAdditional']['outerjs']		= array();
$config['pcservicespages']['removeTemplate']					= 'items/pcservicespages/removeItem.tpl';
$config['pcservicespages']['removeDefaultTabName']			= 'Remove page';
$config['pcservicespages']['removeDefaultHeader']			= 'Remove page';
$config['pcservicespages']['removeDefaultSubHeader'] 		= '';
$config['pcservicespages']['removeSuccessHeader']			= 'Page has been deleted:';
$config['pcservicespages']['removeSuccessSubHeader']		= '';
$config['pcservicespages']['removeErrorHeader']				= 'Page has not been deleted.';
$config['pcservicespages']['removeErrorSubHeader']			= '';		