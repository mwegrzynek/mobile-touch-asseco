<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Sitetexts
 *
 *
 *
*/

//general settings
$config['sitetexts']['itemType'] = 12;
$config['sitetexts']['mainName'] = 'sitetexts';
$config['sitetexts']['activeMenuItem'] = 15;
$config['sitetexts']['showName']       = 'Mobile Touch Other Site Texts';
$config['sitetexts']['mainLink'] = 'items/sitetexts';
$config['sitetexts']['models'] = null;
$config['sitetexts']['ifFiltered'] = true;
$config['sitetexts']['secondMenu'] = array(
											0 => array(	"name"   => "See all",
															"link"   => "items/sitetexts"),

											/*1 => array(	"name"   => "Add new text",
												"link"   => "items/sitetexts/addnew")*/
										);
$config['sitetexts']['menuLastEditedVarName'] 	= 'lastEditedSiteText';
$config['sitetexts']['menuLastEditedText'] 		= 'Last edited:';
$config['sitetexts']['menuLastEditedLink'] 		= 'items/sitetexts/edit/';
$config['sitetexts']['metaDataType'] = -1;
$config['sitetexts']['nestedNodes'] = FALSE;
$config['sitetexts']['categorized'] = TRUE;
$config['sitetexts']['categoryType'] = 3;
$config['sitetexts']['categoryLink'] = 'category/catsitetexts';
$config['sitetexts']['indexCategoryFilterWithChildren'] = FALSE;

$config['sitetexts']['categorizedSecond'] = FALSE;
$config['sitetexts']['categorySecondType'] = -1;
$config['sitetexts']['categorySecondLink'] = 'category/';
$config['sitetexts']['indexCategorySecondFilterWithChildren'] = FALSE;

//extended field [filedname => default]
$config['sitetexts']['extendedFields'] = null;

$config['sitetexts']['isRestrictedContent'] = FALSE;


//main picture settings
$config['sitetexts']['allowedTypes'] 					= 'jpg|png';
$config['sitetexts']['mainPicture'] 					= 0;
$config['sitetexts']['mainPictureMandatory'] 		= 0;
$config['sitetexts']['mainPictureMandatoryError'] 	= 'Pole <strong>obrazek</strong> jest wymagane.';
$config['sitetexts']['mainPicturePath'] 				= '../pictures/sitetexts';
$config['sitetexts']['mainPictureMaxWidth'] 			= 1000;
$config['sitetexts']['mainPictureMaxHeight'] 		= 1000;
$config['sitetexts']['mainPictureMinWidth'] 			= 10;
$config['sitetexts']['mainPictureMinHeight'] 		= 10;
$config['sitetexts']['mainPictureThumbWidth'] 		= 0;

//gallery pictures
$config['sitetexts']['galleryPicturesPath']   = '../pictures/sitetexts';


//index page
$config['sitetexts']['indexAdditional']['css']			= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['sitetexts']['indexAdditional']['js']			= null;
$config['sitetexts']['indexAdditional']['outerjs']		= null;
$config['sitetexts']['indexItemsOrderBy'] 				= 'title';
$config['sitetexts']['indexItemsOrder'] 					= 'asc';
$config['sitetexts']['indexTemplate'] 						= 'items/sitetexts/showItems.tpl';

//addnew page
$config['sitetexts']['addNewAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['sitetexts']['addNewAdditional']['js'] 			= array('tiny.init.js');
$config['sitetexts']['addNewAdditional']['outerjs']	= array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																			));
$config['sitetexts']['addNewTemplate'] 				= 'items/sitetexts/itemsAddAction.tpl';
$config['sitetexts']['addNewDefaultHeader'] 			= 'Add new text.';
$config['sitetexts']['addNewDefaultSubHeader'] 		= '';
$config['sitetexts']['addNewSuccessHeader'] 			= 'Data has been successfully saved.';
$config['sitetexts']['addNewSuccessSubHeader'] 		= '';
$config['sitetexts']['addNewSuccessHeaderAllLangs'] = 'Data has been successfully saved in all languages.';
$config['sitetexts']['addNewErrorHeader'] 			= 'Data has not been saved.';
$config['sitetexts']['addNewErrorSubHeader'] 		= '';
$config['sitetexts']['addNewValidationConfig']	= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Name',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',
																			'rules'   => 'trim'
																		)
																);

//edit page
$config['sitetexts']['editAdditional']['css']		= array('jquery.fancybox.css');//array('uitheme/cupertino/jquery-ui-1.7.1.custom.css', 'jquery.fancybox.css');
$config['sitetexts']['editAdditional']['js']			= array('tiny.init.js');
$config['sitetexts']['editAdditional']['outerjs']	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config['sitetexts']['editTemplate']					= 'items/sitetexts/itemsEditAction.tpl';
$config['sitetexts']['editDefaultTabName']			= 'Edit text';
$config['sitetexts']['editDefaultHeader'] 			= 'Edit text';
$config['sitetexts']['editDefaultSubHeader'] 		= '';
$config['sitetexts']['editSuccessHeader'] 			= 'Data has been successfully saved.';
$config['sitetexts']['editSuccessSubHeader'] 		= '';
$config['sitetexts']['editErrorHeader']				= 'Data has not been saved.';
$config['sitetexts']['editErrorSubHeader']			= '';
$config['sitetexts']['editValidationConfig']		= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Name',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',
																			'rules'   => 'trim'
																		)
																);

//remove page
$config['sitetexts']['advancedRemove']						= FALSE;
$config['sitetexts']['removeAdditional']['css']			= array();
$config['sitetexts']['removeAdditional']['js']			= array();
$config['sitetexts']['removeAdditional']['outerjs']	= array();
$config['sitetexts']['removeTemplate']						= 'items/sitetexts/removeItem.tpl';
$config['sitetexts']['removeDefaultTabName']				= 'Delete text';
$config['sitetexts']['removeDefaultHeader']				= 'Delete text';
$config['sitetexts']['removeDefaultSubHeader']			= '';
$config['sitetexts']['removeSuccessHeader']				= 'Text has been deleted:';
$config['sitetexts']['removeSuccessSubHeader']			= '';
$config['sitetexts']['removeErrorHeader']					= 'Text has not been deleted.';
$config['sitetexts']['removeErrorSubHeader']				= '';

// import / export settings
$config['sitetexts']['import']  		= array(
														'fields' => array(
															'id'      => 'Id',
															'title'   => 'Label',
															'content' => 'Content'
														),
														'meta' => false
													);