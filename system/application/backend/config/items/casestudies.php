<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Case Studies
 *
 *
 *
*/

//general settings
$config['casestudies']['itemType']       = 8;
$config['casestudies']['mainName']       = 'casestudies';
$config['casestudies']['activeMenuItem'] = 6;
$config['casestudies']['showName']       = 'Mobile Touch Case Studies';
$config['casestudies']['mainLink']       = 'items/casestudies';
$config['casestudies']['models']         = null;
$config['casestudies']['ifFiltered'] = true;
$config['casestudies']['secondMenu'] = array(
											0 => array(	"name"   => "See all",
															"link"   => "items/casestudies"),

											1 => array(	"name"   => "Add new Case Study",
															"link"   => "items/casestudies/addnew")
										);
$config['casestudies']['menuLastEditedVarName'] 	= 'lastEditedcasestudies';
$config['casestudies']['menuLastEditedText'] 		= 'Edit case study';
$config['casestudies']['menuLastEditedLink'] 		= 'items/casestudies/edit/';
$config['casestudies']['metaDataType'] = -1;
$config['casestudies']['nestedNodes'] = FALSE;
$config['casestudies']['categorized'] = FALSE;
$config['casestudies']['categoryType'] = -1;
$config['casestudies']['categoryLink'] = 'category/catcasestudies';

$config['casestudies']['connectedWithItems'] = array(
																	array(
																		'id'           =>  5,
																		'itemsorderby' => 'title',
																		'itemsorder'   => 'asc'
																	)
															); //item type ID array

/*
$config['casestudies']['categorizedSecond'] = FALSE;
$config['casestudies']['categorySecondType'] = -1;
$config['casestudies']['categorySecondLink'] = 'category/';
$config['casestudies']['indexCategorySecondFilterWithChildren'] = FALSE;*/

//extended field [filename => default]
//$config['casestudies']['extendedFields'] = null;

//$config['casestudies']['isRestrictedContent'] = FALSE;

//main picture settings
$config['casestudies']['allowedTypes'] 					= 'pdf|jpg|png|doc|odt|xls';
$config['casestudies']['mainPicture'] 					= 1;
$config['casestudies']['mainPictureMandatory'] 		= 1;
$config['casestudies']['mainPictureMandatoryError'] 	= 'Field <strong>file</strong> is mandatory.';
$config['casestudies']['mainPicturePath'] 				= '../userfiles/casestudies';
$config['casestudies']['mainPictureMaxWidth'] 			= 2000;
$config['casestudies']['mainPictureMaxHeight'] 			= 2000;
$config['casestudies']['mainPictureMinWidth'] 			= 140;
$config['casestudies']['mainPictureMinHeight'] 			= 190;
$config['casestudies']['mainPictureThumbWidth'] 		= 140;
$config['casestudies']['mainPictureThumbsInfo'] 		= array(
																			0 => array( 'width' => 100, 'height' => 100 ),
																			1 => array( 'width' => 100, 'height' => 50 )
																		);

//gallery pictures
$config['casestudies']['galleryPicturesPath']   	 = '../userfiles/casestudies';
$config['casestudies']['galleryPicturesThumbsInfo'] = array(
																		0 => array( 'width' => 100, 'height' => 100 ),
																		1 => array( 'width' => 100, 'height' => 50 )
																	);

//index page
$config['casestudies']['indexAdditional']['css']     = array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['casestudies']['indexAdditional']['js']      = null;
$config['casestudies']['indexAdditional']['outerjs'] = null;
$config['casestudies']['indexItemsOrderBy']          = 'title';
$config['casestudies']['indexItemsOrder']            = 'asc';
$config['casestudies']['indexTemplate']              = 'items/casestudies/showItems.tpl';

//addnew page
$config['casestudies']['addNewAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['casestudies']['addNewAdditional']['js'] 		= array('tiny.init.js');
$config['casestudies']['addNewAdditional']['outerjs']	= array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																	));
$config['casestudies']['addNewTemplate'] 				= 'items/casestudies/itemsAddAction.tpl';
$config['casestudies']['addNewDefaultHeader'] 		= 'Add new Case Study';
$config['casestudies']['addNewDefaultSubHeader'] 	= '';
$config['casestudies']['addNewSuccessHeader'] 		= 'Data has been successfully saved.';
$config['casestudies']['addNewSuccessHeaderAllLangs'] = 'Data has been successfully saved in all languages';
$config['casestudies']['addNewSuccessSubHeader'] 	= '';
$config['casestudies']['addNewErrorHeader'] 		= 'Data has not been saved.';
$config['casestudies']['addNewErrorSubHeader'] 	= '';
$config['casestudies']['addNewValidationConfig']	= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Study name',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Description',
																			'rules'   => 'trim'
																		)
																);

//edit page
$config['casestudies']['editAdditional']['css']		= array('jquery.fancybox.css');//array('uitheme/cupertino/jquery-ui-1.7.1.custom.css', 'jquery.fancybox.css');
$config['casestudies']['editAdditional']['js']		= array('tiny.init.js');
$config['casestudies']['editAdditional']['outerjs']	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config['casestudies']['editTemplate']					= 'items/casestudies/itemsEditAction.tpl';
$config['casestudies']['editDefaultTabName']			= 'Edit case study';
$config['casestudies']['editDefaultHeader'] 			= 'Edit case study';
$config['casestudies']['editDefaultSubHeader'] 		= '';
$config['casestudies']['editSuccessHeader'] 			= 'Data has been successfully saved.';
$config['casestudies']['editSuccessSubHeader'] 		= '';
$config['casestudies']['editErrorHeader']				= 'Data has not been saved.';
$config['casestudies']['editErrorSubHeader']			= '';
$config['casestudies']['editValidationConfig']		= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Study name',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Description',
																			'rules'   => 'trim'
																		)
																);


//remove page
$config['casestudies']['advancedRemove']              = FALSE;
$config['casestudies']['removeAdditional']['css']     = array();
$config['casestudies']['removeAdditional']['js']      = array();
$config['casestudies']['removeAdditional']['outerjs'] = array();
$config['casestudies']['removeTemplate']              = 'items/casestudies/removeItem.tpl';
$config['casestudies']['removeDefaultTabName']        = 'Delete case study';
$config['casestudies']['removeDefaultHeader']         = 'Delete case study';
$config['casestudies']['removeDefaultSubHeader']      = '';
$config['casestudies']['removeSuccessHeader']         = 'Case Study has been removed:';
$config['casestudies']['removeSuccessSubHeader']      = '';
$config['casestudies']['removeErrorHeader']           = 'Case Study has not been removed.';
$config['casestudies']['removeErrorSubHeader']        = '';

// import / export settings
$config['casestudies']['import']  		= array(
														'fields' => array(
															'id'      => 'Id',
															'title'   => 'Title',
															'lead'    => 'Lead',
															'content' => 'Content'
														),
														'meta' => false
													);
