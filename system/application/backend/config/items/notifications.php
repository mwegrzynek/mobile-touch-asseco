<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*
 * Notifications
 *
 *
 *
*/

//general settings
$config['notifications']['itemType'] = 14;
$config['notifications']['mainName'] = 'notifications';
$config['notifications']['activeMenuItem'] = 15;
$config['notifications']['mainLink'] = 'items/notifications';
$config['notifications']['models'] = null;
$config['notifications']['ifFiltered'] = true;
$config['notifications']['secondMenu'] = array(
											0 => array(	"name"   => "Zobacz wszystkie",
															"link"   => "items/notifications"),
															
											1 => array(	"name"   => "Dodaj nowe",
															"link"   => "items/notifications/addnew")
										);
$config['notifications']['menuLastEditedVarName'] 	= 'lastEditedNotification';
$config['notifications']['menuLastEditedText'] 		= 'Ostatnio edytowane:';
$config['notifications']['menuLastEditedLink'] 		= 'items/notifications/edit/';
$config['notifications']['metaDataType'] = -1;
$config['notifications']['nestedNodes'] = FALSE;
$config['notifications']['categorized'] = FALSE;
$config['notifications']['categoryType'] = 3;
$config['notifications']['categoryLink'] = 'category/catnotifications';

$config['notifications']['categorizedSecond'] = FALSE;

//extended field [filedname => default]
$config['notifications']['extendedFields'] = null;

$config['notifications']['isRestrictedContent'] = FALSE;


//main picture settings
$config['notifications']['allowedTypes'] 					= 'jpg|png';
$config['notifications']['mainPicture'] 					= 0;
$config['notifications']['mainPictureMandatory'] 		= 0;
$config['notifications']['mainPictureMandatoryError'] = 'Pole <strong>obrazek</strong> jest wymagane.';
$config['notifications']['mainPicturePath'] 				= '../pictures/notifications';
$config['notifications']['mainPictureMaxWidth'] 		= 1000;
$config['notifications']['mainPictureMaxHeight'] 		= 1000;
$config['notifications']['mainPictureMinWidth'] 		= 10;
$config['notifications']['mainPictureMinHeight'] 		= 10;
$config['notifications']['mainPictureThumbWidth'] 		= 0;

//gallery pictures
$config['notifications']['galleryPicturesPath']   = '../pictures/notifications';


//index page
$config['notifications']['indexAdditional']['css']			= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['notifications']['indexAdditional']['js']			= null;
$config['notifications']['indexAdditional']['outerjs']	= null;
$config['notifications']['indexItemsOrderBy'] 				= 'title';
$config['notifications']['indexItemsOrder'] 					= 'asc';
$config['notifications']['indexTemplate'] 					= 'items/notifications/showItems.tpl';

//addnew page
$config['notifications']['addNewAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['notifications']['addNewAdditional']['js'] 			= array('tiny.init.js');
$config['notifications']['addNewAdditional']['outerjs']	= array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																			));
$config['notifications']['addNewTemplate'] 				= 'items/notifications/itemsAddAction.tpl';
$config['notifications']['addNewDefaultHeader'] 		= 'Dodaj nowe';
$config['notifications']['addNewDefaultSubHeader'] 	= '';
$config['notifications']['addNewSuccessHeader'] 		= 'Dane zostały pomyślnie zapisane.';
$config['notifications']['addNewSuccessSubHeader'] 	= '';
$config['notifications']['addNewErrorHeader'] 			= 'Dane nie zostały zapisane.';
$config['notifications']['addNewErrorSubHeader'] 		= '';
$config['notifications']['addNewValidationConfig']	= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Nazwa',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Treść',																			
																			'rules'   => 'trim|strip_tags'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',																			
																			'rules'   => 'trim'
																		)	
																);
																
//edit page
$config['notifications']['editAdditional']['css']		= array('jquery.fancybox.css');
$config['notifications']['editAdditional']['js']		= array('tiny.init.js');
$config['notifications']['editAdditional']['outerjs']	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config['notifications']['editTemplate']					= 'items/notifications/itemsEditAction.tpl';
$config['notifications']['editDefaultTabName']			= 'Edytuj powiadomienie';
$config['notifications']['editDefaultHeader'] 			= 'Edytuj powiadomienie';
$config['notifications']['editDefaultSubHeader'] 		= '';
$config['notifications']['editSuccessHeader'] 			= 'Dane zostały pomyślnie zapisane.';
$config['notifications']['editSuccessSubHeader'] 		= '';
$config['notifications']['editErrorHeader']				= 'Dane nie zostały zapisane.';
$config['notifications']['editErrorSubHeader']			= '';
$config['notifications']['editValidationConfig']		= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Text',																			
																			'rules'   => 'trim|strip_tags'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',																			
																			'rules'   => 'trim'
																		)	
																);
																
//remove page
$config['notifications']['advancedRemove']					= FALSE;	
$config['notifications']['removeAdditional']['css']		= array();
$config['notifications']['removeAdditional']['js']			= array();
$config['notifications']['removeAdditional']['outerjs']	= array();
$config['notifications']['removeTemplate']					= 'items/notifications/removeItem.tpl';
$config['notifications']['removeDefaultTabName']			= 'Usuń powiadomienie';
$config['notifications']['removeDefaultHeader']				= 'Usuń powiadomienie';
$config['notifications']['removeDefaultSubHeader']			= '';
$config['notifications']['removeSuccessHeader']				= 'Powiadomienie zostało usuniete';
$config['notifications']['removeSuccessSubHeader']			= '';
$config['notifications']['removeErrorHeader']				= 'Powiadomienie nie zostało usunięte';
$config['notifications']['removeErrorSubHeader']			= '';