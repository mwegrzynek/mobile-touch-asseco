<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Countries
 *
 *
 *
*/

//general settings
$config['countries']['itemType'] = 9;
$config['countries']['mainName'] = 'countries';
$config['countries']['activeMenuItem'] = 6;
$config['countries']['mainLink'] = 'items/countries';
$config['countries']['models'] = null;
$config['countries']['ifFiltered'] = true;
$config['countries']['secondMenu'] = array(
											0 => array(	"name"   => "See all",
															"link"   => "items/countries"),
															
											1 => array(	"name"   => "Add country",
															"link"   => "items/countries/addnew")
										);
$config['countries']['menuLastEditedVarName'] 	= 'lastEditedcountries';
$config['countries']['menuLastEditedText'] 		= 'Edit country';
$config['countries']['menuLastEditedLink'] 		= 'items/countries/edit/';
$config['countries']['metaDataType'] = 5;
$config['countries']['nestedNodes'] = FALSE;
$config['countries']['categorized'] = FALSE;
$config['countries']['categoryType'] = 1;
$config['countries']['categoryLink'] = 'category/';


//extended field [filedname => default]
$config['countries']['extendedFields'] = null;

$config['countries']['isRestrictedContent'] = FALSE;

//main picture settings
$config['countries']['allowedTypes'] 					= 'jpg|png';
$config['countries']['mainPicture'] 					= 0;
$config['countries']['mainPictureMandatory'] 		= 0;
$config['countries']['mainPictureMandatoryError'] 	= 'Field <strong>image</strong> is mandatory.';
$config['countries']['mainPicturePath'] 				= '../pictures/countries';
$config['countries']['mainPictureMaxWidth'] 			= 2000;
$config['countries']['mainPictureMaxHeight'] 		= 2000;
$config['countries']['mainPictureMinWidth'] 			= 140;
$config['countries']['mainPictureMinHeight'] 		= 190;
$config['countries']['mainPictureThumbWidth'] 		= 140;
$config['countries']['mainPictureThumbsInfo'] 		= array(
																			0 => array( 'width' => 100, 'height' => 100 ),
																			1 => array( 'width' => 100, 'height' => 50 )
																		);

//gallery pictures
$config['countries']['galleryPicturesPath']   	 = '../pictures/countries';
$config['countries']['galleryPicturesThumbsInfo'] = array(
																		0 => array( 'width' => 100, 'height' => 100 ),
																		1 => array( 'width' => 100, 'height' => 50 )
																	);

//index page
$config['countries']['indexAdditional']['css']     = array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['countries']['indexAdditional']['js']      = null;
$config['countries']['indexAdditional']['outerjs'] = null;
$config['countries']['indexItemsOrderBy']          = 'title';
$config['countries']['indexItemsOrder']            = 'asc';
$config['countries']['indexTemplate']              = 'items/countries/showItems.tpl';

//addnew page
$config['countries']['addNewAdditional']['css'] 	= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['countries']['addNewAdditional']['js'] 		= array('tiny.init.js');
$config['countries']['addNewAdditional']['outerjs']	= array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																	));
$config['countries']['addNewTemplate'] 			= 'items/countries/itemsAddAction.tpl';
$config['countries']['addNewDefaultHeader'] 		= 'Add new country';
$config['countries']['addNewDefaultSubHeader'] 	= '';
$config['countries']['addNewSuccessHeader'] 		= 'Data has been successfully saved.';
$config['countries']['addNewSuccessHeaderAllLangs'] = 'Data has been successfully saved in all languages.';
$config['countries']['addNewSuccessSubHeader'] 	= '';
$config['countries']['addNewErrorHeader'] 		= 'Data has not been saved.';
$config['countries']['addNewErrorSubHeader'] 	= '';
$config['countries']['addNewValidationConfig']	= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Name',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',																			
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Description',																			
																			'rules'   => 'trim'
																		)
																);
																
//edit page
$config['countries']['editAdditional']['css']	= array('jquery.fancybox.css');//array('uitheme/cupertino/jquery-ui-1.7.1.custom.css', 'jquery.fancybox.css');
$config['countries']['editAdditional']['js']		= array('tiny.init.js');
$config['countries']['editAdditional']['outerjs']	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config['countries']['editTemplate']					= 'items/countries/itemsEditAction.tpl';
$config['countries']['editDefaultTabName']			= 'Edit country';
$config['countries']['editDefaultHeader'] 			= 'Edit country';
$config['countries']['editDefaultSubHeader'] 		= '';
$config['countries']['editSuccessHeader'] 			= 'Data has been successfully saved.';
$config['countries']['editSuccessSubHeader'] 		= '';
$config['countries']['editErrorHeader']				= 'Data has not been saved.';
$config['countries']['editErrorSubHeader']			= '';
$config['countries']['editValidationConfig']		= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Name',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',																			
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Description',																			
																			'rules'   => 'trim'
																		)
																);
																
																
//remove page
$config['countries']['advancedRemove']              = FALSE;	
$config['countries']['removeAdditional']['css']     = array();
$config['countries']['removeAdditional']['js']      = array();
$config['countries']['removeAdditional']['outerjs'] = array();
$config['countries']['removeTemplate']              = 'items/countries/removeItem.tpl';
$config['countries']['removeDefaultTabName']        = 'Delete country';
$config['countries']['removeDefaultHeader']         = 'Delete country';
$config['countries']['removeDefaultSubHeader']      = '';
$config['countries']['removeSuccessHeader']         = 'Country has been removed:';
$config['countries']['removeSuccessSubHeader']      = '';
$config['countries']['removeErrorHeader']           = 'Country has not been removed.';
$config['countries']['removeErrorSubHeader']        = '';	
