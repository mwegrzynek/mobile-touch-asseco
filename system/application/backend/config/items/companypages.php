<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*
 * Services pages
 *
 *
 *
*/

//general settings
$config['companypages']['itemType']       = 28;
$config['companypages']['mainName']       = 'companypages';
$config['companypages']['activeMenuItem'] = 7;
$config['companypages']['showName']       = 'Company Pages';
$config['companypages']['mainLink']       = 'items/companypages';
$config['companypages']['models']         = null;
$config['companypages']['ifFiltered']     = true;
$config['companypages']['secondMenu'] = array(
											0 => array(	"name"   => "See all",
															"link"   => "items/companypages"),

											// 1 => array(	"name"   => "Add new page",
											// 				"link"   => "items/companypages/addnew")
										);
$config['companypages']['menuLastEditedVarName'] = 'lastEditedCompanyPage';
$config['companypages']['menuLastEditedText'] 	= 'Edit last edited:';
$config['companypages']['menuLastEditedLink'] 	= 'items/companypages/edit/';
$config['companypages']['metaDataType'] = 4;
$config['companypages']['nestedNodes'] = FALSE;
$config['companypages']['categorized'] = TRUE;
$config['companypages']['categoryType'] = 1;
$config['companypages']['categoryLink'] = 'category/industries';
$config['companypages']['indexCategoryFilterWithChildren'] = FALSE;
$config['companypages']['indexParentFilterWithChildren'] 	= FALSE;


$config['companypages']['categorizedSecond'] = FALSE;
$config['companypages']['categorySecondType'] = -1;
$config['companypages']['categorySecondLink'] = 'category/';
$config['companypages']['indexCategorySecondFilterWithChildren'] = FALSE;





//extended field [filedname => default]
$config['companypages']['extendedFields'] = null;

$config['companypages']['isRestrictedContent'] = FALSE;

//main picture settings
$config['companypages']['allowedTypes']              = 'jpg|png';
$config['companypages']['mainPicture']               = 0;
$config['companypages']['mainPictureMandatory']      = 0;
$config['companypages']['mainPictureMandatoryError'] = 'Field <strong>image</strong> is mandatory.';
$config['companypages']['mainPicturePath']           = '../userfiles/companypages';
$config['companypages']['mainPictureMaxWidth']       = 630;
$config['companypages']['mainPictureMaxHeight']      = 144;
$config['companypages']['mainPictureMinWidth']       = 30;
$config['companypages']['mainPictureMinHeight']      = 30;


//gallery pictures
$config['companypages']['galleryPicturesPath']   = '../pictures/companypages';
$config['companypages']['galleryPicturesThumbsInfo'] = array(
																	0 => array( 'width' => 100, 'height' => 100 ),
																	1 => array( 'width' => 100, 'height' => 50 )
																);


//index page
$config['companypages']['indexAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['companypages']['indexAdditional']['js'] 		= null;
$config['companypages']['indexAdditional']['outerjs'] 	= null;
$config['companypages']['indexItemsOrderBy'] 				= 'position';
$config['companypages']['indexItemsOrder'] 					= 'asc';
$config['companypages']['indexTemplate'] 					= 'items/companypages/showItems.tpl';

//addnew page
$config['companypages']['addNewAdditional']['css'] 		= array(); //array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['companypages']['addNewAdditional']['js'] 		= array('tiny.init.js');
$config['companypages']['addNewAdditional']['outerjs'] = array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																			));
$config['companypages']['addNewTemplate'] 				= 'items/companypages/itemsAddAction.tpl';
$config['companypages']['addNewDefaultHeader'] 		= 'Add new page';
$config['companypages']['addNewDefaultSubHeader'] 	= '';
$config['companypages']['addNewSuccessHeaderAllLangs']	= 'Data has been successfully saved in all languages.';
$config['companypages']['addNewSuccessHeader'] 		= 'Data has been successfully saved.';
$config['companypages']['addNewSuccessSubHeader'] 	= '';
$config['companypages']['addNewErrorHeader'] 			= 'Data has not been saved';
$config['companypages']['addNewErrorSubHeader'] 		= '';
$config['companypages']['addNewValidationConfig']	= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',
																			'rules'   => 'trim|required|is_natural'
																		)
																);

//edit page
$config['companypages']['editAdditional']['css'] 		= array('jquery.fancybox.css');//array('uitheme/cupertino/jquery-ui-1.7.1.custom.css', 'jquery.fancybox.css');
$config['companypages']['editAdditional']['js'] 			= array('tiny.init.js');
$config['companypages']['editAdditional']['outerjs'] 	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config['companypages']['editTemplate'] 				= 'items/companypages/itemsEditAction.tpl';
$config['companypages']['editDefaultTabName']			= 'Edit: ';
$config['companypages']['editDefaultHeader'] 			= 'Edit page';
$config['companypages']['editDefaultSubHeader'] 		= '';
$config['companypages']['editSuccessHeader'] 			= 'Data has been successfully saved.';
$config['companypages']['editSuccessSubHeader'] 		= '';
$config['companypages']['editErrorHeader'] 			= 'Data has not been saved.';
$config['companypages']['editErrorSubHeader'] 		= '';
$config['companypages']['editValidationConfig']		= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',
																			'rules'   => 'trim|required|is_natural'
																		)
																);


//remove page
$config['companypages']['advancedRemove']						= true;
$config['companypages']['removeAdditional']['css']			= array();
$config['companypages']['removeAdditional']['js']			= array();
$config['companypages']['removeAdditional']['outerjs']		= array();
$config['companypages']['removeTemplate']					= 'items/companypages/removeItem.tpl';
$config['companypages']['removeDefaultTabName']			= 'Remove page';
$config['companypages']['removeDefaultHeader']			= 'Remove page';
$config['companypages']['removeDefaultSubHeader'] 		= '';
$config['companypages']['removeSuccessHeader']			= 'Page has been deleted:';
$config['companypages']['removeSuccessSubHeader']		= '';
$config['companypages']['removeErrorHeader']				= 'Page has not been deleted.';
$config['companypages']['removeErrorSubHeader']			= '';
// import / export settings
$config['companypages']['import']	= array(
														'fields' => array(
															'id'      => 'Id',
															'title'   => 'Title',
															'lead'    => 'Lead',
															'content' => 'Content',
															'text_data_1' => 'Text Data',
															'text_data_2' => 'Text Data'
														),
														'meta' => false
													);