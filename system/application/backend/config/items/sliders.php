<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*
 * Sliders
 *
 *
 *
*/

//general settings
$config['sliders']['itemType']       = 10;
$config['sliders']['mainName']       = 'sliders';
$config['sliders']['activeMenuItem'] = 15;
$config['sliders']['showName']       = 'Mobile Touch Home Page Video';
$config['sliders']['mainLink']       = 'items/sliders';
$config['sliders']['models']         = null;
$config['sliders']['ifFiltered']     = true;
$config['sliders']['secondMenu']     = array(
											0 => array(	"name"   => "See all",
															"link"   => "items/sliders"),

											1 => array(	"name"   => "Add new slider",
															"link"   => "items/sliders/addnew")
										);
$config['sliders']['menuLastEditedVarName'] = 'lastEditedSite';
$config['sliders']['menuLastEditedText'] 	= 'Edit last edited:';
$config['sliders']['menuLastEditedLink'] 	= 'items/sliders/edit/';
$config['sliders']['metaDataType'] = -1;
$config['sliders']['nestedNodes'] = FALSE;
$config['sliders']['categorized'] = FALSE;
$config['sliders']['categoryType'] = 1;
$config['sliders']['categoryLink'] = 'category/industries';
$config['sliders']['indexCategoryFilterWithChildren'] = FALSE;
$config['sliders']['indexParentFilterWithChildren'] 	= FALSE;


$config['sliders']['categorizedSecond'] = FALSE;
$config['sliders']['categorySecondType'] = -1;
$config['sliders']['categorySecondLink'] = 'category/';
$config['sliders']['indexCategorySecondFilterWithChildren'] = FALSE;





//extended field [filedname => default]
$config['sliders']['extendedFields'] = null;

$config['sliders']['isRestrictedContent'] = FALSE;

//main picture settings
$config['sliders']['allowedTypes'] 					= 'jpg|png';
$config['sliders']['mainPicture'] 					= 1;
$config['sliders']['mainPictureMandatory'] 		= 1;
$config['sliders']['mainPictureMandatoryError'] = 'Field <strong>image</strong> is mandatory.';
$config['sliders']['mainPicturePath'] 				= '../userfiles/sliders';
$config['sliders']['mainPictureMaxWidth'] 		= 220;
$config['sliders']['mainPictureMaxHeight'] 		= 124;
$config['sliders']['mainPictureMinWidth'] 		= 220;
$config['sliders']['mainPictureMinHeight'] 		= 124;
$config['sliders']['mainPictureThumbsInfo'] 		= FALSE;

/*$config['sliders']['mainPictureThumbsInfo'] 		= array(
																	0 => array( 'width' => 100, 'height' => 100 ),
																	1 => array( 'width' => 100, 'height' => 50 )
																);	*/


//gallery pictures
$config['sliders']['galleryPicturesPath']   = '../pictures/sliders';
$config['sliders']['galleryPicturesThumbsInfo'] = array(
																	0 => array( 'width' => 100, 'height' => 100 ),
																	1 => array( 'width' => 100, 'height' => 50 )
																);


//index page
$config['sliders']['indexAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['sliders']['indexAdditional']['js'] 		= null;
$config['sliders']['indexAdditional']['outerjs'] 	= null;
$config['sliders']['indexItemsOrderBy'] 				= 'position';
$config['sliders']['indexItemsOrder'] 					= 'asc';
$config['sliders']['indexTemplate'] 					= 'items/sliders/showItems.tpl';

//addnew page
$config['sliders']['addNewAdditional']['css'] 		= array(); //array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['sliders']['addNewAdditional']['js'] 		= array('tiny.init.js');
$config['sliders']['addNewAdditional']['outerjs'] = array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																			));
$config['sliders']['addNewTemplate'] 				= 'items/sliders/itemsAddAction.tpl';
$config['sliders']['addNewDefaultHeader'] 		= 'Add new slide';
$config['sliders']['addNewDefaultSubHeader'] 	= '';
$config['sliders']['addNewSuccessHeaderAllLangs']	= 'Data has been successfully saved in all languages.';
$config['sliders']['addNewSuccessHeader'] 		= 'Data has been successfully saved.';
$config['sliders']['addNewSuccessSubHeader'] 	= '';
$config['sliders']['addNewErrorHeader'] 			= 'Data has not been saved';
$config['sliders']['addNewErrorSubHeader'] 		= '';
$config['sliders']['addNewValidationConfig']	= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',
																			'rules'   => 'trim|required|is_natural'
																		)
																);

//edit page
$config['sliders']['editAdditional']['css'] 		= array('jquery.fancybox.css');//array('uitheme/cupertino/jquery-ui-1.7.1.custom.css', 'jquery.fancybox.css');
$config['sliders']['editAdditional']['js'] 			= array('tiny.init.js');
$config['sliders']['editAdditional']['outerjs'] 	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config['sliders']['editTemplate'] 				= 'items/sliders/itemsEditAction.tpl';
$config['sliders']['editDefaultTabName']		= 'Edit: ';
$config['sliders']['editDefaultHeader'] 		= 'Edit slide';
$config['sliders']['editDefaultSubHeader'] 	= '';
$config['sliders']['editSuccessHeader'] 		= 'Data has been successfully saved.';
$config['sliders']['editSuccessSubHeader'] 	= '';
$config['sliders']['editErrorHeader'] 			= 'Data has not been saved.';
$config['sliders']['editErrorSubHeader'] 		= '';
$config['sliders']['editValidationConfig']		= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',
																			'rules'   => 'trim|required|is_natural'
																		)
																);


//remove page
$config['sliders']['advancedRemove']					= true;
$config['sliders']['removeAdditional']['css']		= array();
$config['sliders']['removeAdditional']['js']			= array();
$config['sliders']['removeAdditional']['outerjs']	= array();
$config['sliders']['removeTemplate']					= 'items/sliders/removeItem.tpl';
$config['sliders']['removeDefaultTabName']			= 'Remove slide';
$config['sliders']['removeDefaultHeader']				= 'Remove slide';
$config['sliders']['removeDefaultSubHeader'] 		= '';
$config['sliders']['removeSuccessHeader']				= 'Slide has been deleted:';
$config['sliders']['removeSuccessSubHeader']			= '';
$config['sliders']['removeErrorHeader']				= 'Slide has not been deleted.';
$config['sliders']['removeErrorSubHeader']			= '';

// import / export settings
$config['sliders']['import']  		= array(
														'fields' => array(
															'id'      => 'Id',
															'title'   => 'Title'
														),
														'meta' => false
													);

