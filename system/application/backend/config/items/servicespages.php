<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*
 * Services pages
 *
 *
 *
*/

//general settings
$config['servicespages']['itemType']       = 4;
$config['servicespages']['mainName']       = 'servicespages';
$config['servicespages']['activeMenuItem'] = 2;
$config['servicespages']['showName']       = 'Mobile Touch Services';
$config['servicespages']['mainLink']       = 'items/servicespages';
$config['servicespages']['models']         = null;
$config['servicespages']['ifFiltered']     = true;
$config['servicespages']['secondMenu'] = array(
											0 => array(	"name"   => "See all",
															"link"   => "items/servicespages"),

											1 => array(	"name"   => "Add new page",
															"link"   => "items/servicespages/addnew")
										);
$config['servicespages']['menuLastEditedVarName'] = 'lastEditedSite';
$config['servicespages']['menuLastEditedText'] 	= 'Edit last edited:';
$config['servicespages']['menuLastEditedLink'] 	= 'items/servicespages/edit/';
$config['servicespages']['metaDataType'] = 4;
$config['servicespages']['nestedNodes'] = FALSE;
$config['servicespages']['categorized'] = TRUE;
$config['servicespages']['categoryType'] = 1;
$config['servicespages']['categoryLink'] = 'category/industries';
$config['servicespages']['indexCategoryFilterWithChildren'] = FALSE;
$config['servicespages']['indexParentFilterWithChildren'] 	= FALSE;


$config['servicespages']['categorizedSecond'] = FALSE;
$config['servicespages']['categorySecondType'] = -1;
$config['servicespages']['categorySecondLink'] = 'category/';
$config['servicespages']['indexCategorySecondFilterWithChildren'] = FALSE;





//extended field [filedname => default]
$config['servicespages']['extendedFields'] = null;

$config['servicespages']['isRestrictedContent'] = FALSE;

//main picture settings
$config['servicespages']['allowedTypes'] 					= 'jpg|png';
$config['servicespages']['mainPicture'] 						= 1;
$config['servicespages']['mainPictureMandatory'] 			= 0;
$config['servicespages']['mainPictureMandatoryError'] 	= 'Field <strong>image</strong> is mandatory.';
$config['servicespages']['mainPicturePath'] 				= '../userfiles/servicespages';
$config['servicespages']['mainPictureMaxWidth'] 			= 630;
$config['servicespages']['mainPictureMaxHeight'] 			= 144;
$config['servicespages']['mainPictureMinWidth'] 			= 30;
$config['servicespages']['mainPictureMinHeight'] 			= 30;


//gallery pictures
$config['servicespages']['galleryPicturesPath']   = '../pictures/servicespages';
$config['servicespages']['galleryPicturesThumbsInfo'] = array(
																	0 => array( 'width' => 100, 'height' => 100 ),
																	1 => array( 'width' => 100, 'height' => 50 )
																);


//index page
$config['servicespages']['indexAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['servicespages']['indexAdditional']['js'] 		= null;
$config['servicespages']['indexAdditional']['outerjs'] 	= null;
$config['servicespages']['indexItemsOrderBy'] 				= 'position';
$config['servicespages']['indexItemsOrder'] 					= 'asc';
$config['servicespages']['indexTemplate'] 					= 'items/servicespages/showItems.tpl';

//addnew page
$config['servicespages']['addNewAdditional']['css'] 		= array(); //array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['servicespages']['addNewAdditional']['js'] 		= array('tiny.init.js');
$config['servicespages']['addNewAdditional']['outerjs'] = array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																			));
$config['servicespages']['addNewTemplate'] 				= 'items/servicespages/itemsAddAction.tpl';
$config['servicespages']['addNewDefaultHeader'] 		= 'Add new page';
$config['servicespages']['addNewDefaultSubHeader'] 	= '';
$config['servicespages']['addNewSuccessHeaderAllLangs']	= 'Data has been successfully saved in all languages.';
$config['servicespages']['addNewSuccessHeader'] 		= 'Data has been successfully saved.';
$config['servicespages']['addNewSuccessSubHeader'] 	= '';
$config['servicespages']['addNewErrorHeader'] 			= 'Data has not been saved';
$config['servicespages']['addNewErrorSubHeader'] 		= '';
$config['servicespages']['addNewValidationConfig']	= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',
																			'rules'   => 'trim|required|is_natural'
																		)
																);

//edit page
$config['servicespages']['editAdditional']['css'] 		= array('jquery.fancybox.css');//array('uitheme/cupertino/jquery-ui-1.7.1.custom.css', 'jquery.fancybox.css');
$config['servicespages']['editAdditional']['js'] 			= array('tiny.init.js');
$config['servicespages']['editAdditional']['outerjs'] 	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config['servicespages']['editTemplate'] 				= 'items/servicespages/itemsEditAction.tpl';
$config['servicespages']['editDefaultTabName']			= 'Edit: ';
$config['servicespages']['editDefaultHeader'] 			= 'Edit page';
$config['servicespages']['editDefaultSubHeader'] 		= '';
$config['servicespages']['editSuccessHeader'] 			= 'Data has been successfully saved.';
$config['servicespages']['editSuccessSubHeader'] 		= '';
$config['servicespages']['editErrorHeader'] 			= 'Data has not been saved.';
$config['servicespages']['editErrorSubHeader'] 		= '';
$config['servicespages']['editValidationConfig']		= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',
																			'rules'   => 'trim|required|is_natural'
																		)
																);


//remove page
$config['servicespages']['advancedRemove']						= true;
$config['servicespages']['removeAdditional']['css']			= array();
$config['servicespages']['removeAdditional']['js']			= array();
$config['servicespages']['removeAdditional']['outerjs']		= array();
$config['servicespages']['removeTemplate']					= 'items/servicespages/removeItem.tpl';
$config['servicespages']['removeDefaultTabName']			= 'Remove page';
$config['servicespages']['removeDefaultHeader']			= 'Remove page';
$config['servicespages']['removeDefaultSubHeader'] 		= '';
$config['servicespages']['removeSuccessHeader']			= 'Page has been deleted:';
$config['servicespages']['removeSuccessSubHeader']		= '';
$config['servicespages']['removeErrorHeader']				= 'Page has not been deleted.';
$config['servicespages']['removeErrorSubHeader']			= '';
// import / export settings
$config['servicespages']['import']	= array(
														'fields' => array(
															'id'      => 'Id',
															'title'   => 'Title',
															'lead'    => 'Lead',
															'content' => 'Content'
														),
														'meta' => false
													);