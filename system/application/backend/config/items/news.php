<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * News
 *
 *
 *
*/

//general settings
$config['news']['itemType']       = 7;
$config['news']['mainName']       = 'news';
$config['news']['activeMenuItem'] = 5;
$config['news']['showName']       = 'News';
$config['news']['mainLink']       = 'items/news';
$config['news']['models']         = null;
$config['news']['ifFiltered']     = true;
$config['news']['secondMenu']     = array(
											0 => array(	"name"   => "See all",
															"link"   => "items/news"),

											1 => array(	"name"   => "Add new",
															"link"   => "items/news/addnew")
										);
$config['news']['menuLastEditedVarName'] 	= 'lastEditedNews';
$config['news']['menuLastEditedText'] 		= 'Last edited:';
$config['news']['menuLastEditedLink'] 		= 'items/news/edit/';
$config['news']['metaDataType'] = 7;
$config['news']['nestedNodes'] = FALSE;
$config['news']['categorized'] = FALSE;
$config['news']['categoryType'] = -1;
$config['news']['categoryLink'] = 'category/catnews';

$config['news']['categorizedSecond'] = FALSE;
$config['news']['categorySecondType'] = -1;
$config['news']['categorySecondLink'] = 'category/';
$config['news']['indexCategorySecondFilterWithChildren'] = FALSE;

//extended field [filedname => default]
$config['news']['extendedFields'] = null;

$config['news']['isRestrictedContent'] = FALSE;


//main picture settings
$config['news']['allowedTypes'] 						= 'jpg|png';
$config['news']['mainPicture'] 						= 1;
$config['news']['mainPictureMandatory'] 			= 0;
$config['news']['mainPictureMandatoryError'] 	= 'Field <strong>image</strong> is mandatory.';
$config['news']['mainPicturePath'] 					= '../userfiles/news';
$config['news']['mainPictureMaxWidth'] 			= 275;
$config['news']['mainPictureMaxHeight'] 			= 1000;
$config['news']['mainPictureMinWidth'] 			= 50;
$config['news']['mainPictureMinHeight'] 			= 50;
$config['news']['mainPictureThumbsInfo'] 			= array(
																	0 => array( 'width' => 50, 'height' => 0 )

																);

//gallery pictures
$config['news']['galleryPicturesPath']   = '../pictures/news';
$config['news']['galleryPicturesThumbsInfo'] = array(
																	0 => array( 'width' => 100, 'height' => 100 ),
																	1 => array( 'width' => 100, 'height' => 50 )
																);


//index page
$config['news']['indexAdditional']['css']			= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['news']['indexAdditional']['js']			= null;
$config['news']['indexAdditional']['outerjs']	= null;
$config['news']['indexItemsOrderBy'] 				= 'add_date';
$config['news']['indexItemsOrder'] 					= 'desc';
$config['news']['indexTemplate'] 					= 'items/news/showItems.tpl';

//addnew page
$config['news']['addNewAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['news']['addNewAdditional']['js'] 		= array('tiny.init.js');
$config['news']['addNewAdditional']['outerjs']	= array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																			));
$config['news']['addNewTemplate'] 					= 'items/news/itemsAddAction.tpl';
$config['news']['addNewDefaultHeader'] 			= 'Add new';
$config['news']['addNewDefaultSubHeader'] 		= '';
$config['news']['addNewSuccessHeader'] 			= 'Data has been successfully saved.';
$config['news']['addNewSuccessSubHeader'] 		= '';
$config['news']['addNewErrorHeader'] 				= 'Data has not been saved.';
$config['news']['addNewErrorSubHeader'] 			= '';
$config['news']['addNewValidationConfig']	= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',
																			'rules'   => 'trim'
																		)
																);

//edit page
$config['news']['editAdditional']['css']		= array('jquery.fancybox.css');//array('uitheme/cupertino/jquery-ui-1.7.1.custom.css', 'jquery.fancybox.css');
$config['news']['editAdditional']['js']		= array('tiny.init.js');
$config['news']['editAdditional']['outerjs']	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config['news']['editTemplate']					= 'items/news/itemsEditAction.tpl';
$config['news']['editDefaultTabName']			= 'Edit news';
$config['news']['editDefaultHeader'] 			= 'Edit news';
$config['news']['editDefaultSubHeader'] 		= '';
$config['news']['editSuccessHeader'] 			= 'Data has been successfully saved.';
$config['news']['editSuccessSubHeader'] 		= '';
$config['news']['editErrorHeader']				= 'Data has not been saved.';
$config['news']['editErrorSubHeader']			= '';
$config['news']['editValidationConfig']		= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',
																			'rules'   => 'trim'
																		)
																);


//remove page
$config['news']['advancedRemove']					= true;
$config['news']['removeAdditional']['css']		= array();
$config['news']['removeAdditional']['js']			= array();
$config['news']['removeAdditional']['outerjs']	= array();
$config['news']['removeTemplate']					= 'items/news/removeItem.tpl';
$config['news']['removeDefaultTabName']			= 'Delete news';
$config['news']['removeDefaultHeader']				= 'Delete news';
$config['news']['removeDefaultSubHeader']			= '';
$config['news']['removeSuccessHeader']				= 'News has been deleted';
$config['news']['removeSuccessSubHeader']			= '';
$config['news']['removeErrorHeader']				= 'News has not been deleted.';
$config['news']['removeErrorSubHeader']			= '';

// import / export settings
$config['news']['import']  		= array(
														'fields' => array(
															'id'          => 'Id',
															'title'       => 'Title',
															'lead'        => 'Lead',
															'content'     => 'Content'
														),
														'meta' => false
													);
