<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*
 * pages
 *
 *
 *
*/

//general settings
$config['pages']['itemType'] = 1;
$config['pages']['mainName'] = 'pages';
$config['pages']['activeMenuItem'] = 3;
$config['pages']['mainLink'] = 'items/pages';
$config['pages']['models'] = null;
$config['pages']['ifFiltered'] = true;
$config['pages']['secondMenu'] = array(
											0 => array(	"name"   => "See all",
															"link"   => "items/pages"),
														
											1 => array(	"name"   => "Add new page",
															"link"   => "items/pages/addnew")
										);
$config['pages']['menuLastEditedVarName'] = 'lastEditedSite';
$config['pages']['menuLastEditedText'] 	= 'Edit last edited:';
$config['pages']['menuLastEditedLink'] 	= 'items/pages/edit/';
$config['pages']['metaDataType'] = 1;
$config['pages']['nestedNodes'] = TRUE;
$config['pages']['categorized'] = TRUE;
$config['pages']['categoryType'] = 1;
$config['pages']['categoryLink'] = 'category/industries';
$config['pages']['indexCategoryFilterWithChildren'] = FALSE;
$config['pages']['indexParentFilterWithChildren'] 	= FALSE;


$config['pages']['categorizedSecond'] = FALSE;
$config['pages']['categorySecondType'] = -1;
$config['pages']['categorySecondLink'] = 'category/';
$config['pages']['indexCategorySecondFilterWithChildren'] = FALSE;





//extended field [filedname => default]
$config['pages']['extendedFields'] = null;

$config['pages']['isRestrictedContent'] = FALSE;

//main picture settings
$config['pages']['allowedTypes'] 					= 'jpg|png';
$config['pages']['mainPicture'] 						= 1;
$config['pages']['mainPictureMandatory'] 			= 0;
$config['pages']['mainPictureMandatoryError'] 	= 'Field <strong>image</strong> is mandatory.';
$config['pages']['mainPicturePath'] 				= '../pictures/pages';
$config['pages']['mainPictureMaxWidth'] 			= 630;
$config['pages']['mainPictureMaxHeight'] 			= 144;
$config['pages']['mainPictureMinWidth'] 			= 630;
$config['pages']['mainPictureMinHeight'] 			= 144;
$config['pages']['mainPictureThumbsInfo'] 		= array(
																	0 => array( 'width' => 100, 'height' => 100 ),
																	1 => array( 'width' => 100, 'height' => 50 )
																);


//gallery pictures
$config['pages']['galleryPicturesPath']   = '../pictures/pages';
$config['pages']['galleryPicturesThumbsInfo'] = array(
																	0 => array( 'width' => 100, 'height' => 100 ),
																	1 => array( 'width' => 100, 'height' => 50 )
																);


//index page
$config['pages']['indexAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['pages']['indexAdditional']['js'] 		= null;
$config['pages']['indexAdditional']['outerjs'] 	= null;
$config['pages']['indexItemsOrderBy'] 				= 'title';
$config['pages']['indexItemsOrder'] 				= 'asc';
$config['pages']['indexTemplate'] 					= 'items/pages/showItems.tpl';

//addnew page
$config['pages']['addNewAdditional']['css'] 		= array(); //array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['pages']['addNewAdditional']['js'] 		= array('tiny.init.js');
$config['pages']['addNewAdditional']['outerjs'] = array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																			));
$config['pages']['addNewTemplate'] 				= 'items/pages/itemsAddAction.tpl';
$config['pages']['addNewDefaultHeader'] 		= 'Add new page';
$config['pages']['addNewDefaultSubHeader'] 	= '';
$config['pages']['addNewSuccessHeaderAllLangs']	= 'Data has been successfully saved in all languages.';
$config['pages']['addNewSuccessHeader'] 		= 'Data has been successfully saved.';
$config['pages']['addNewSuccessSubHeader'] 	= '';
$config['pages']['addNewErrorHeader'] 			= 'Data has not been saved';
$config['pages']['addNewErrorSubHeader'] 		= '';
$config['pages']['addNewValidationConfig']	= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',																			
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',																			
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',																			
																			'rules'   => 'trim|required|is_natural'
																		)	
																);
																
//edit page
$config['pages']['editAdditional']['css'] 		= array('jquery.fancybox.css');//array('uitheme/cupertino/jquery-ui-1.7.1.custom.css', 'jquery.fancybox.css');
$config['pages']['editAdditional']['js'] 			= array('tiny.init.js');
$config['pages']['editAdditional']['outerjs'] 	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config['pages']['editTemplate'] 				= 'items/pages/itemsEditAction.tpl';
$config['pages']['editDefaultTabName']			= 'Edit: ';
$config['pages']['editDefaultHeader'] 			= 'Edit page';
$config['pages']['editDefaultSubHeader'] 		= '';
$config['pages']['editSuccessHeader'] 			= 'Data has been successfully saved.';
$config['pages']['editSuccessSubHeader'] 		= '';
$config['pages']['editErrorHeader'] 			= 'Data has not been saved.';
$config['pages']['editErrorSubHeader'] 		= '';
$config['pages']['editValidationConfig']		= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',																			
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',																			
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',																			
																			'rules'   => 'trim|required|is_natural'
																		)	
																);
																
																
//remove page
$config['pages']['advancedRemove']						= true;	
$config['pages']['removeAdditional']['css']			= array();
$config['pages']['removeAdditional']['js']			= array();
$config['pages']['removeAdditional']['outerjs']		= array();
$config['pages']['removeTemplate']					= 'items/pages/removeItem.tpl';
$config['pages']['removeDefaultTabName']			= 'Remove page';
$config['pages']['removeDefaultHeader']			= 'Remove page';
$config['pages']['removeDefaultSubHeader'] 		= '';
$config['pages']['removeSuccessHeader']			= 'Page has been deleted:';
$config['pages']['removeSuccessSubHeader']		= '';
$config['pages']['removeErrorHeader']				= 'Page has not been deleted.';
$config['pages']['removeErrorSubHeader']			= '';		