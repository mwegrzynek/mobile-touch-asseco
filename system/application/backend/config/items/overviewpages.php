<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*
 * Mobile Touch main overviewpages
 *
 *
 *
*/

//general settings
$config['overviewpages']['itemType']       = 19;
$config['overviewpages']['mainName']       = 'overviewpages';
$config['overviewpages']['activeMenuItem'] = 2;
$config['overviewpages']['showName']       = 'Mobile Touch Overview';
$config['overviewpages']['mainLink']       = 'items/overviewpages';
$config['overviewpages']['models']         = null;
$config['overviewpages']['ifFiltered']     = true;
$config['overviewpages']['secondMenu']     = array(
											0 => array(	"name"   => "See all",
															"link"   => "items/overviewpages")/*,

											1 => array(	"name"   => "Add new page",
												"link"   => "items/overviewpages/addnew")*/
										);
$config['overviewpages']['menuLastEditedVarName'] = 'lastEditedSite';
$config['overviewpages']['menuLastEditedText'] 	= 'Edit last edited:';
$config['overviewpages']['menuLastEditedLink'] 	= 'items/overviewpages/edit/';
$config['overviewpages']['metaDataType'] = 2;
$config['overviewpages']['nestedNodes'] = FALSE;
$config['overviewpages']['categorized'] = FALSE;
$config['overviewpages']['categoryType'] = 1;
$config['overviewpages']['categoryLink'] = 'category/industries';
$config['overviewpages']['indexCategoryFilterWithChildren'] = FALSE;
$config['overviewpages']['indexParentFilterWithChildren'] 	= FALSE;


$config['overviewpages']['categorizedSecond'] = FALSE;
$config['overviewpages']['categorySecondType'] = -1;
$config['overviewpages']['categorySecondLink'] = 'category/';
$config['overviewpages']['indexCategorySecondFilterWithChildren'] = FALSE;





//extended field [filedname => default]
$config['overviewpages']['extendedFields'] = null;

$config['overviewpages']['isRestrictedContent'] = FALSE;

//main picture settings
$config['overviewpages']['allowedTypes'] 					= 'jpg|png';
$config['overviewpages']['mainPicture'] 						= 1;
$config['overviewpages']['mainPictureMandatory'] 			= 0;
$config['overviewpages']['mainPictureMandatoryError'] 	= 'Field <strong>image</strong> is mandatory.';
$config['overviewpages']['mainPicturePath'] 				= '../pictures/overviewpages';
$config['overviewpages']['mainPictureMaxWidth'] 			= 630;
$config['overviewpages']['mainPictureMaxHeight'] 			= 144;
$config['overviewpages']['mainPictureMinWidth'] 			= 630;
$config['overviewpages']['mainPictureMinHeight'] 			= 144;
$config['overviewpages']['mainPictureThumbsInfo'] 		= array(
																	0 => array( 'width' => 100, 'height' => 100 ),
																	1 => array( 'width' => 100, 'height' => 50 )
																);


//gallery pictures
$config['overviewpages']['galleryPicturesPath']   = '../pictures/overviewpages';
$config['overviewpages']['galleryPicturesThumbsInfo'] = array(
																	0 => array( 'width' => 100, 'height' => 100 ),
																	1 => array( 'width' => 100, 'height' => 50 )
																);


//index page
$config['overviewpages']['indexAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['overviewpages']['indexAdditional']['js'] 			= null;
$config['overviewpages']['indexAdditional']['outerjs'] 	= null;
$config['overviewpages']['indexItemsOrderBy'] 				= 'position';
$config['overviewpages']['indexItemsOrder'] 					= 'asc';
$config['overviewpages']['indexTemplate'] 					= 'items/overviewpages/showItems.tpl';

//addnew page
$config['overviewpages']['addNewAdditional']['css'] 		= array(); //array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['overviewpages']['addNewAdditional']['js'] 		= array('tiny.init.js');
$config['overviewpages']['addNewAdditional']['outerjs'] = array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																			));
$config['overviewpages']['addNewTemplate'] 				= 'items/overviewpages/itemsAddAction.tpl';
$config['overviewpages']['addNewDefaultHeader'] 		= 'Add new page';
$config['overviewpages']['addNewDefaultSubHeader'] 	= '';
$config['overviewpages']['addNewSuccessHeaderAllLangs']	= 'Data has been successfully saved in all languages.';
$config['overviewpages']['addNewSuccessHeader'] 		= 'Data has been successfully saved.';
$config['overviewpages']['addNewSuccessSubHeader'] 	= '';
$config['overviewpages']['addNewErrorHeader'] 			= 'Data has not been saved';
$config['overviewpages']['addNewErrorSubHeader'] 		= '';
$config['overviewpages']['addNewValidationConfig']	= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',
																			'rules'   => 'trim'
																		)
																);

//edit page
$config['overviewpages']['editAdditional']['css'] 		= array('jquery.fancybox.css');//array('uitheme/cupertino/jquery-ui-1.7.1.custom.css', 'jquery.fancybox.css');
$config['overviewpages']['editAdditional']['js'] 			= array('tiny.init.js');
$config['overviewpages']['editAdditional']['outerjs'] 	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config['overviewpages']['editTemplate'] 				= 'items/overviewpages/itemsEditAction.tpl';
$config['overviewpages']['editDefaultTabName']			= 'Edit: ';
$config['overviewpages']['editDefaultHeader'] 			= 'Edit page';
$config['overviewpages']['editDefaultSubHeader'] 		= '';
$config['overviewpages']['editSuccessHeader'] 			= 'Data has been successfully saved.';
$config['overviewpages']['editSuccessSubHeader'] 		= '';
$config['overviewpages']['editErrorHeader'] 			= 'Data has not been saved.';
$config['overviewpages']['editErrorSubHeader'] 		= '';
$config['overviewpages']['editValidationConfig']		= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',
																			'rules'   => 'trim'
																		)
																);


//remove page
$config['overviewpages']['advancedRemove']						= true;
$config['overviewpages']['removeAdditional']['css']			= array();
$config['overviewpages']['removeAdditional']['js']			= array();
$config['overviewpages']['removeAdditional']['outerjs']		= array();
$config['overviewpages']['removeTemplate']					= 'items/overviewpages/removeItem.tpl';
$config['overviewpages']['removeDefaultTabName']			= 'Remove page';
$config['overviewpages']['removeDefaultHeader']			= 'Remove page';
$config['overviewpages']['removeDefaultSubHeader'] 		= '';
$config['overviewpages']['removeSuccessHeader']			= 'Page has been deleted:';
$config['overviewpages']['removeSuccessSubHeader']		= '';
$config['overviewpages']['removeErrorHeader']				= 'Page has not been deleted.';
$config['overviewpages']['removeErrorSubHeader']			= '';

// import / export settings
$config['overviewpages']['import']  		= array(
														'fields' => array(
															'id'      => 'Id',
															'title'   => 'Title',
															'lead'    => 'Lead',
															'content' => 'Content',
															'text_data_1' => 'No more need to...'
														),
														'meta' => false
													);
