<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Team Leaders
 *
 *
 *
*/

//general settings
$config['teamleaders']['itemType']       = 17;
$config['teamleaders']['mainName']       = 'teamleaders';
$config['teamleaders']['activeMenuItem'] = 8;
$config['teamleaders']['showName']       = 'Mobile Touch Team Leaders';
$config['teamleaders']['mainLink']       = 'items/teamleaders';
$config['teamleaders']['models']         = null;
$config['teamleaders']['ifFiltered'] = true;
$config['teamleaders']['secondMenu'] = array(
											0 => array(	"name"   => "See all",
															"link"   => "items/teamleaders"),

											1 => array(	"name"   => "Add new team leader",
															"link"   => "items/teamleaders/addnew")
										);
$config['teamleaders']['menuLastEditedVarName'] 	= 'lastEditedteamleaders';
$config['teamleaders']['menuLastEditedText'] 		= 'Edit team leader';
$config['teamleaders']['menuLastEditedLink'] 		= 'items/teamleaders/edit/';
$config['teamleaders']['metaDataType'] = -1;
$config['teamleaders']['nestedNodes'] = FALSE;
$config['teamleaders']['categorized'] = FALSE;
$config['teamleaders']['categoryType'] = -1;
$config['teamleaders']['categoryLink'] = 'category/catteamleaders';

$config['teamleaders']['categorizedSecond'] = FALSE;
$config['teamleaders']['categorySecondType'] = -1;
$config['teamleaders']['categorySecondLink'] = 'category/';
$config['teamleaders']['indexCategorySecondFilterWithChildren'] = FALSE;

//extended field [filedname => default]
$config['teamleaders']['extendedFields'] = null;

$config['teamleaders']['isRestrictedContent'] = FALSE;

//main picture settings
$config['teamleaders']['allowedTypes'] 					= 'jpg|png';
$config['teamleaders']['mainPicture'] 						= 1;
$config['teamleaders']['mainPictureMandatory'] 			= 1;
$config['teamleaders']['mainPictureMandatoryError'] 	= 'Field <strong>image</strong> is mandatory.';
$config['teamleaders']['mainPicturePath'] 				= '../userfiles/teamleaders';
$config['teamleaders']['mainPictureMaxWidth'] 			= 139;
$config['teamleaders']['mainPictureMaxHeight'] 			= 124;
$config['teamleaders']['mainPictureMinWidth'] 			= 139;
$config['teamleaders']['mainPictureMinHeight'] 			= 124;
/*$config['teamleaders']['mainPictureThumbsInfo'] 		= array(
																			0 => array( 'width' => 100, 'height' => 100 ),
																			1 => array( 'width' => 100, 'height' => 50 )
																		);*/

//gallery pictures
$config['teamleaders']['galleryPicturesPath']   	 = '../pictures/teamleaders';
$config['teamleaders']['galleryPicturesThumbsInfo'] = array(
																		0 => array( 'width' => 100, 'height' => 100 ),
																		1 => array( 'width' => 100, 'height' => 50 )
																	);

//index page
$config['teamleaders']['indexAdditional']['css']     = array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['teamleaders']['indexAdditional']['js']      = null;
$config['teamleaders']['indexAdditional']['outerjs'] = null;
$config['teamleaders']['indexItemsOrderBy']          = 'title';
$config['teamleaders']['indexItemsOrder']            = 'asc';
$config['teamleaders']['indexTemplate']              = 'items/teamleaders/showItems.tpl';

//addnew page
$config['teamleaders']['addNewAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['teamleaders']['addNewAdditional']['js'] 		= array('tiny.init.js');
$config['teamleaders']['addNewAdditional']['outerjs']	= array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																	));
$config['teamleaders']['addNewTemplate'] 				= 'items/teamleaders/itemsAddAction.tpl';
$config['teamleaders']['addNewDefaultHeader'] 		= 'Add new Team Leader';
$config['teamleaders']['addNewDefaultSubHeader'] 	= '';
$config['teamleaders']['addNewSuccessHeader'] 		= 'Data has been successfully saved.';
$config['teamleaders']['addNewSuccessHeaderAllLangs'] = 'Data has been successfully saved in all languages.';
$config['teamleaders']['addNewSuccessSubHeader'] 	= '';
$config['teamleaders']['addNewErrorHeader'] 			= 'Data has not been saved.';
$config['teamleaders']['addNewErrorSubHeader'] 		= '';
$config['teamleaders']['addNewValidationConfig']	= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'First and last name',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Testimonial',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Description',
																			'rules'   => 'trim'
																		)
																);

//edit page
$config['teamleaders']['editAdditional']['css']		= array('jquery.fancybox.css');//array('uitheme/cupertino/jquery-ui-1.7.1.custom.css', 'jquery.fancybox.css');
$config['teamleaders']['editAdditional']['js']		= array('tiny.init.js');
$config['teamleaders']['editAdditional']['outerjs']	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config['teamleaders']['editTemplate']					= 'items/teamleaders/itemsEditAction.tpl';
$config['teamleaders']['editDefaultTabName']			= 'Edit team leader';
$config['teamleaders']['editDefaultHeader'] 			= 'Edit team leader';
$config['teamleaders']['editDefaultSubHeader'] 		= '';
$config['teamleaders']['editSuccessHeader'] 			= 'Data has been successfully saved.';
$config['teamleaders']['editSuccessSubHeader'] 		= '';
$config['teamleaders']['editErrorHeader']				= 'Data has not been saved.';
$config['teamleaders']['editErrorSubHeader']			= '';
$config['teamleaders']['editValidationConfig']		= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'First and last name',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Testimonial',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Description',
																			'rules'   => 'trim'
																		)
																);


//remove page
$config['teamleaders']['advancedRemove']              = FALSE;
$config['teamleaders']['removeAdditional']['css']     = array();
$config['teamleaders']['removeAdditional']['js']      = array();
$config['teamleaders']['removeAdditional']['outerjs'] = array();
$config['teamleaders']['removeTemplate']              = 'items/teamleaders/removeItem.tpl';
$config['teamleaders']['removeDefaultTabName']        = 'Delete team leader';
$config['teamleaders']['removeDefaultHeader']         = 'Delete team leader';
$config['teamleaders']['removeDefaultSubHeader']      = '';
$config['teamleaders']['removeSuccessHeader']         = 'Team leader has been removed:';
$config['teamleaders']['removeSuccessSubHeader']      = '';
$config['teamleaders']['removeErrorHeader']           = 'Team leader has not been removed.';
$config['teamleaders']['removeErrorSubHeader']        = '';

// import / export settings
$config['teamleaders']['import']  		= array(
														'fields' => array(
															'id'          => 'Id',
															'title'       => 'First Name',
															'text_data_2' => 'Last Name',
															'text_data_1' => 'Company position',
															'lead'        => 'Testimonial',
															'content'     => 'Description'
														),
														'meta' => false
													);
