<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Case Studies
 *
 *
 *
*/

//general settings
$config['references']['itemType']       = 18;
$config['references']['mainName']       = 'references';
$config['references']['activeMenuItem'] = 6;
$config['references']['showName']       = 'Mobile Touch References';
$config['references']['mainLink']       = 'items/references';
$config['references']['models']         = null;
$config['references']['ifFiltered']     = true;
$config['references']['secondMenu'] = array(
											0 => array(	"name"   => "See all",
															"link"   => "items/references"),

											1 => array(	"name"   => "Add new Reference",
															"link"   => "items/references/addnew")
										);
$config['references']['menuLastEditedVarName'] 	= 'lastEditedreferences';
$config['references']['menuLastEditedText'] 		= 'Edit reference';
$config['references']['menuLastEditedLink'] 		= 'items/references/edit/';
$config['references']['metaDataType'] = -1;
$config['references']['nestedNodes'] = FALSE;
$config['references']['categorized'] = FALSE;
$config['references']['categoryType'] = -1;
$config['references']['categoryLink'] = 'category/catreferences';

$config['references']['connectedWithItems'] = array(
																	array(
																		'id'           =>  5,
																		'itemsorderby' => 'title',
																		'itemsorder'   => 'asc'
																	)
															); //item type ID array

/*
$config['references']['categorizedSecond'] = FALSE;
$config['references']['categorySecondType'] = -1;
$config['references']['categorySecondLink'] = 'category/';
$config['references']['indexCategorySecondFilterWithChildren'] = FALSE;*/

//extended field [filename => default]
//$config['references']['extendedFields'] = null;

//$config['references']['isRestrictedContent'] = FALSE;

//main picture settings
$config['references']['allowedTypes'] 					= 'pdf|jpg|png|doc|odt|xls';
$config['references']['mainPicture'] 					= 1;
$config['references']['mainPictureMandatory'] 		= 0;
$config['references']['mainPictureMandatoryError'] 	= 'Field <strong>file</strong> is mandatory.';
$config['references']['mainPicturePath'] 				= '../userfiles/references';
$config['references']['mainPictureMaxWidth'] 			= 2000;
$config['references']['mainPictureMaxHeight'] 			= 2000;
$config['references']['mainPictureMinWidth'] 			= 140;
$config['references']['mainPictureMinHeight'] 			= 190;
$config['references']['mainPictureThumbWidth'] 		= 140;
$config['references']['mainPictureThumbsInfo'] 		= array(
																			0 => array( 'width' => 100, 'height' => 100 ),
																			1 => array( 'width' => 100, 'height' => 50 )
																		);

//gallery pictures
$config['references']['galleryPicturesPath']   	 = '../userfiles/references';
$config['references']['galleryPicturesThumbsInfo'] = array(
																		0 => array( 'width' => 100, 'height' => 100 ),
																		1 => array( 'width' => 100, 'height' => 50 )
																	);

//index page
$config['references']['indexAdditional']['css']     = array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['references']['indexAdditional']['js']      = null;
$config['references']['indexAdditional']['outerjs'] = null;
$config['references']['indexItemsOrderBy']          = 'title';
$config['references']['indexItemsOrder']            = 'asc';
$config['references']['indexTemplate']              = 'items/references/showItems.tpl';

//addnew page
$config['references']['addNewAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['references']['addNewAdditional']['js'] 		= array('tiny.init.js');
$config['references']['addNewAdditional']['outerjs']	= array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																	));
$config['references']['addNewTemplate'] 				= 'items/references/itemsAddAction.tpl';
$config['references']['addNewDefaultHeader'] 		= 'Add new Reference';
$config['references']['addNewDefaultSubHeader'] 	= '';
$config['references']['addNewSuccessHeader'] 		= 'Data has been successfully saved.';
$config['references']['addNewSuccessHeaderAllLangs'] = 'Data has been successfully saved in all languages';
$config['references']['addNewSuccessSubHeader'] 	= '';
$config['references']['addNewErrorHeader'] 		= 'Data has not been saved.';
$config['references']['addNewErrorSubHeader'] 	= '';
$config['references']['addNewValidationConfig']	= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Reference name',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Description',
																			'rules'   => 'trim'
																		)
																);

//edit page
$config['references']['editAdditional']['css']		= array('jquery.fancybox.css');//array('uitheme/cupertino/jquery-ui-1.7.1.custom.css', 'jquery.fancybox.css');
$config['references']['editAdditional']['js']		= array('tiny.init.js');
$config['references']['editAdditional']['outerjs']	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config['references']['editTemplate']					= 'items/references/itemsEditAction.tpl';
$config['references']['editDefaultTabName']			= 'Edit reference';
$config['references']['editDefaultHeader'] 			= 'Edit reference';
$config['references']['editDefaultSubHeader'] 		= '';
$config['references']['editSuccessHeader'] 			= 'Data has been successfully saved.';
$config['references']['editSuccessSubHeader'] 		= '';
$config['references']['editErrorHeader']				= 'Data has not been saved.';
$config['references']['editErrorSubHeader']			= '';
$config['references']['editValidationConfig']		= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Study name',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Description',
																			'rules'   => 'trim'
																		)
																);


//remove page
$config['references']['advancedRemove']              = FALSE;
$config['references']['removeAdditional']['css']     = array();
$config['references']['removeAdditional']['js']      = array();
$config['references']['removeAdditional']['outerjs'] = array();
$config['references']['removeTemplate']              = 'items/references/removeItem.tpl';
$config['references']['removeDefaultTabName']        = 'Delete case study';
$config['references']['removeDefaultHeader']         = 'Delete case study';
$config['references']['removeDefaultSubHeader']      = '';
$config['references']['removeSuccessHeader']         = 'Case Study has been removed:';
$config['references']['removeSuccessSubHeader']      = '';
$config['references']['removeErrorHeader']           = 'Case Study has not been removed.';
$config['references']['removeErrorSubHeader']        = '';

// import / export settings
$config['references']['import']  		= array(
														'fields' => array(
															'id'      => 'Id',
															'title'   => 'Title',
															'lead'    => 'Lead',
															'content' => 'Content'
														),
														'meta' => false
													);
