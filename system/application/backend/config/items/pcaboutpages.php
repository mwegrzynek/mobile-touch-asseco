<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*
 * Platforma Connector main about pages
 *
 *
 *
*/

//general settings
$config['pcaboutpages']['itemType']       = 25;
$config['pcaboutpages']['mainName']       = 'pcaboutpages';
$config['pcaboutpages']['activeMenuItem'] = 3;
$config['pcaboutpages']['mainLink']       = 'items/pcaboutpages';
$config['pcaboutpages']['models']         = null;
$config['pcaboutpages']['ifFiltered']     = true;
$config['pcaboutpages']['secondMenu']     = array(
											0 => array(	"name"   => "See all",
															"link"   => "items/pcaboutpages")/*,
														
											1 => array(	"name"   => "Add new page",
												"link"   => "items/pcaboutpages/addnew")*/
										);
$config['pcaboutpages']['menuLastEditedVarName'] = 'lastEditedSite';
$config['pcaboutpages']['menuLastEditedText'] 	= 'Edit last edited:';
$config['pcaboutpages']['menuLastEditedLink'] 	= 'items/pcaboutpages/edit/';
$config['pcaboutpages']['metaDataType'] = 25;
$config['pcaboutpages']['nestedNodes'] = FALSE;
$config['pcaboutpages']['categorized'] = FALSE;
$config['pcaboutpages']['categoryType'] = 1;
$config['pcaboutpages']['categoryLink'] = 'category/industries';
$config['pcaboutpages']['indexCategoryFilterWithChildren'] = FALSE;
$config['pcaboutpages']['indexParentFilterWithChildren'] 	= FALSE;


$config['pcaboutpages']['categorizedSecond'] = FALSE;
$config['pcaboutpages']['categorySecondType'] = -1;
$config['pcaboutpages']['categorySecondLink'] = 'category/';
$config['pcaboutpages']['indexCategorySecondFilterWithChildren'] = FALSE;





//extended field [filedname => default]
$config['pcaboutpages']['extendedFields'] = null;

$config['pcaboutpages']['isRestrictedContent'] = FALSE;

//main picture settings
$config['pcaboutpages']['allowedTypes'] 					= 'jpg|png';
$config['pcaboutpages']['mainPicture'] 						= 1;
$config['pcaboutpages']['mainPictureMandatory'] 			= 0;
$config['pcaboutpages']['mainPictureMandatoryError'] 	= 'Field <strong>image</strong> is mandatory.';
$config['pcaboutpages']['mainPicturePath'] 				= '../pictures/pcaboutpages';
$config['pcaboutpages']['mainPictureMaxWidth'] 			= 630;
$config['pcaboutpages']['mainPictureMaxHeight'] 		= 144;
$config['pcaboutpages']['mainPictureMinWidth'] 			= 630;
$config['pcaboutpages']['mainPictureMinHeight'] 		= 144;
$config['pcaboutpages']['mainPictureThumbsInfo'] 		= array(
																	0 => array( 'width' => 100, 'height' => 100 ),
																	1 => array( 'width' => 100, 'height' => 50 )
																);


//gallery pictures
$config['pcaboutpages']['galleryPicturesPath']   = '../pictures/pcaboutpages';
$config['pcaboutpages']['galleryPicturesThumbsInfo'] = array(
																	0 => array( 'width' => 100, 'height' => 100 ),
																	1 => array( 'width' => 100, 'height' => 50 )
																);


//index page
$config['pcaboutpages']['indexAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['pcaboutpages']['indexAdditional']['js'] 			= null;
$config['pcaboutpages']['indexAdditional']['outerjs'] 	= null;
$config['pcaboutpages']['indexItemsOrderBy'] 				= 'position';
$config['pcaboutpages']['indexItemsOrder'] 					= 'asc';
$config['pcaboutpages']['indexTemplate'] 					= 'items/pcaboutpages/showItems.tpl';

//addnew page
$config['pcaboutpages']['addNewAdditional']['css'] 		= array(); //array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['pcaboutpages']['addNewAdditional']['js'] 		= array('tiny.init.js');
$config['pcaboutpages']['addNewAdditional']['outerjs'] = array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																			));
$config['pcaboutpages']['addNewTemplate'] 				= 'items/pcaboutpages/itemsAddAction.tpl';
$config['pcaboutpages']['addNewDefaultHeader'] 		= 'Add new page';
$config['pcaboutpages']['addNewDefaultSubHeader'] 	= '';
$config['pcaboutpages']['addNewSuccessHeaderAllLangs']	= 'Data has been successfully saved in all languages.';
$config['pcaboutpages']['addNewSuccessHeader'] 		= 'Data has been successfully saved.';
$config['pcaboutpages']['addNewSuccessSubHeader'] 	= '';
$config['pcaboutpages']['addNewErrorHeader'] 			= 'Data has not been saved';
$config['pcaboutpages']['addNewErrorSubHeader'] 		= '';
$config['pcaboutpages']['addNewValidationConfig']	= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',																			
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',																			
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',																			
																			'rules'   => 'trim'
																		)	
																);
																
//edit page
$config['pcaboutpages']['editAdditional']['css'] 		= array('jquery.fancybox.css');//array('uitheme/cupertino/jquery-ui-1.7.1.custom.css', 'jquery.fancybox.css');
$config['pcaboutpages']['editAdditional']['js'] 			= array('tiny.init.js');
$config['pcaboutpages']['editAdditional']['outerjs'] 	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config['pcaboutpages']['editTemplate'] 				= 'items/pcaboutpages/itemsEditAction.tpl';
$config['pcaboutpages']['editDefaultTabName']			= 'Edit: ';
$config['pcaboutpages']['editDefaultHeader'] 			= 'Edit page';
$config['pcaboutpages']['editDefaultSubHeader'] 		= '';
$config['pcaboutpages']['editSuccessHeader'] 			= 'Data has been successfully saved.';
$config['pcaboutpages']['editSuccessSubHeader'] 		= '';
$config['pcaboutpages']['editErrorHeader'] 			= 'Data has not been saved.';
$config['pcaboutpages']['editErrorSubHeader'] 		= '';
$config['pcaboutpages']['editValidationConfig']		= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',																			
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',																			
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',																			
																			'rules'   => 'trim'
																		)	
																);
																
																
//remove page
$config['pcaboutpages']['advancedRemove']						= true;	
$config['pcaboutpages']['removeAdditional']['css']			= array();
$config['pcaboutpages']['removeAdditional']['js']			= array();
$config['pcaboutpages']['removeAdditional']['outerjs']		= array();
$config['pcaboutpages']['removeTemplate']					= 'items/pcaboutpages/removeItem.tpl';
$config['pcaboutpages']['removeDefaultTabName']			= 'Remove page';
$config['pcaboutpages']['removeDefaultHeader']			= 'Remove page';
$config['pcaboutpages']['removeDefaultSubHeader'] 		= '';
$config['pcaboutpages']['removeSuccessHeader']			= 'Page has been deleted:';
$config['pcaboutpages']['removeSuccessSubHeader']		= '';
$config['pcaboutpages']['removeErrorHeader']				= 'Page has not been deleted.';
$config['pcaboutpages']['removeErrorSubHeader']			= '';		
