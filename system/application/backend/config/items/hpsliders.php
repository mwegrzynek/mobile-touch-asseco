<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*
 * hpsliders
 *
 *
 *
*/

//general settings
$config['hpsliders']['itemType']       = 20;
$config['hpsliders']['mainName']       = 'hpsliders';
$config['hpsliders']['activeMenuItem'] = 15;
$config['hpsliders']['showName']       = 'Mobile Touch Home Page Sliders';
$config['hpsliders']['mainLink']       = 'items/hpsliders';
$config['hpsliders']['models']         = null;
$config['hpsliders']['ifFiltered']     = true;
$config['hpsliders']['secondMenu']     = array(
											0 => array(	"name"   => "See all",
															"link"   => "items/hpsliders"),

											1 => array(	"name"   => "Add new slider",
															"link"   => "items/hpsliders/addnew")
										);
$config['hpsliders']['menuLastEditedVarName'] = 'lastEditedSite';
$config['hpsliders']['menuLastEditedText'] 	= 'Edit last edited:';
$config['hpsliders']['menuLastEditedLink'] 	= 'items/hpsliders/edit/';
$config['hpsliders']['metaDataType'] = -1;
$config['hpsliders']['nestedNodes'] = FALSE;
$config['hpsliders']['categorized'] = FALSE;
$config['hpsliders']['categoryType'] = 1;
$config['hpsliders']['categoryLink'] = 'category/industries';
$config['hpsliders']['indexCategoryFilterWithChildren'] = FALSE;
$config['hpsliders']['indexParentFilterWithChildren'] 	= FALSE;


$config['hpsliders']['categorizedSecond'] = FALSE;
$config['hpsliders']['categorySecondType'] = -1;
$config['hpsliders']['categorySecondLink'] = 'category/';
$config['hpsliders']['indexCategorySecondFilterWithChildren'] = FALSE;





//extended field [filedname => default]
$config['hpsliders']['extendedFields'] = null;

$config['hpsliders']['isRestrictedContent'] = FALSE;

//main picture settings
$config['hpsliders']['allowedTypes']              = 'jpg|png';
$config['hpsliders']['mainPicture']               = 0;
$config['hpsliders']['mainPictureMandatory']      = 0;
$config['hpsliders']['mainPictureMandatoryError'] = 'Field <strong>image</strong> is mandatory.';
$config['hpsliders']['mainPicturePath']           = '../userfiles/hpsliders';
$config['hpsliders']['mainPictureMaxWidth']       = 220;
$config['hpsliders']['mainPictureMaxHeight']      = 124;
$config['hpsliders']['mainPictureMinWidth']       = 220;
$config['hpsliders']['mainPictureMinHeight']      = 124;
$config['hpsliders']['mainPictureThumbsInfo']     = FALSE;

/*$config['hpsliders']['mainPictureThumbsInfo'] 		= array(
																	0 => array( 'width' => 100, 'height' => 100 ),
																	1 => array( 'width' => 100, 'height' => 50 )
																);	*/


//gallery pictures
$config['hpsliders']['galleryPicturesPath']   = '../pictures/hpsliders';
$config['hpsliders']['galleryPicturesThumbsInfo'] = array(
																	0 => array( 'width' => 100, 'height' => 100 ),
																	1 => array( 'width' => 100, 'height' => 50 )
																);


//index page
$config['hpsliders']['indexAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['hpsliders']['indexAdditional']['js'] 		= null;
$config['hpsliders']['indexAdditional']['outerjs'] 	= null;
$config['hpsliders']['indexItemsOrderBy'] 				= 'position';
$config['hpsliders']['indexItemsOrder'] 					= 'asc';
$config['hpsliders']['indexTemplate'] 					= 'items/hpsliders/showItems.tpl';

//addnew page
$config['hpsliders']['addNewAdditional']['css'] 		= array(); //array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['hpsliders']['addNewAdditional']['js'] 		= array('tiny.init.js');
$config['hpsliders']['addNewAdditional']['outerjs'] = array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																			));
$config['hpsliders']['addNewTemplate'] 				= 'items/hpsliders/itemsAddAction.tpl';
$config['hpsliders']['addNewDefaultHeader'] 		= 'Add new slide';
$config['hpsliders']['addNewDefaultSubHeader'] 	= '';
$config['hpsliders']['addNewSuccessHeaderAllLangs']	= 'Data has been successfully saved in all languages.';
$config['hpsliders']['addNewSuccessHeader'] 		= 'Data has been successfully saved.';
$config['hpsliders']['addNewSuccessSubHeader'] 	= '';
$config['hpsliders']['addNewErrorHeader'] 			= 'Data has not been saved';
$config['hpsliders']['addNewErrorSubHeader'] 		= '';
$config['hpsliders']['addNewValidationConfig']	= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',
																			'rules'   => 'trim|required|is_natural'
																		)
																);

//edit page
$config['hpsliders']['editAdditional']['css'] 		= array('jquery.fancybox.css');//array('uitheme/cupertino/jquery-ui-1.7.1.custom.css', 'jquery.fancybox.css');
$config['hpsliders']['editAdditional']['js'] 			= array('tiny.init.js');
$config['hpsliders']['editAdditional']['outerjs'] 	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config['hpsliders']['editTemplate'] 				= 'items/hpsliders/itemsEditAction.tpl';
$config['hpsliders']['editDefaultTabName']		= 'Edit: ';
$config['hpsliders']['editDefaultHeader'] 		= 'Edit slide';
$config['hpsliders']['editDefaultSubHeader'] 	= '';
$config['hpsliders']['editSuccessHeader'] 		= 'Data has been successfully saved.';
$config['hpsliders']['editSuccessSubHeader'] 	= '';
$config['hpsliders']['editErrorHeader'] 			= 'Data has not been saved.';
$config['hpsliders']['editErrorSubHeader'] 		= '';
$config['hpsliders']['editValidationConfig']		= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',
																			'rules'   => 'trim|required|is_natural'
																		)
																);


//remove page
$config['hpsliders']['advancedRemove']              = true;
$config['hpsliders']['removeAdditional']['css']     = array();
$config['hpsliders']['removeAdditional']['js']      = array();
$config['hpsliders']['removeAdditional']['outerjs'] = array();
$config['hpsliders']['removeTemplate']              = 'items/hpsliders/removeItem.tpl';
$config['hpsliders']['removeDefaultTabName']        = 'Remove slide';
$config['hpsliders']['removeDefaultHeader']         = 'Remove slide';
$config['hpsliders']['removeDefaultSubHeader']      = '';
$config['hpsliders']['removeSuccessHeader']         = 'Slide has been deleted:';
$config['hpsliders']['removeSuccessSubHeader']      = '';
$config['hpsliders']['removeErrorHeader']           = 'Slide has not been deleted.';
$config['hpsliders']['removeErrorSubHeader']        = '';

// import / export settings
$config['hpsliders']['import']  		= array(
														'fields' => array(
															'id'          => 'Id',
															'title'       => 'Title',
															'content'     => 'Content',
															'text_data_1' => 'Button Label',
															'text_data_2' => 'Button Link'
														),
														'meta' => false
													);
