<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*
 * Platforma Connector Features pages
 *
 *
 *
*/

//general settings
$config['pcfeaturespages']['itemType'] = 23;
$config['pcfeaturespages']['mainName'] = 'pcfeaturespages';
$config['pcfeaturespages']['activeMenuItem'] = 3;
$config['pcfeaturespages']['mainLink'] = 'items/pcfeaturespages';
$config['pcfeaturespages']['models'] = null;
$config['pcfeaturespages']['ifFiltered'] = true;
$config['pcfeaturespages']['secondMenu'] = array(
											0 => array(	"name"   => "See all",
															"link"   => "items/pcfeaturespages"),
														
											1 => array(	"name"   => "Add new page",
															"link"   => "items/pcfeaturespages/addnew")
										);
$config['pcfeaturespages']['menuLastEditedVarName'] = 'lastEditedSite';
$config['pcfeaturespages']['menuLastEditedText'] 	= 'Edit last edited:';
$config['pcfeaturespages']['menuLastEditedLink'] 	= 'items/pcfeaturespages/edit/';
$config['pcfeaturespages']['metaDataType'] = 23;
$config['pcfeaturespages']['nestedNodes'] = TRUE;
$config['pcfeaturespages']['categorized'] = TRUE;
$config['pcfeaturespages']['categoryType'] = 1;
$config['pcfeaturespages']['categoryLink'] = 'category/industries';
$config['pcfeaturespages']['indexCategoryFilterWithChildren'] = FALSE;
$config['pcfeaturespages']['indexParentFilterWithChildren'] 	= FALSE;


$config['pcfeaturespages']['categorizedSecond'] = FALSE;
$config['pcfeaturespages']['categorySecondType'] = -1;
$config['pcfeaturespages']['categorySecondLink'] = 'category/';
$config['pcfeaturespages']['indexCategorySecondFilterWithChildren'] = FALSE;





//extended field [filedname => default]
$config['pcfeaturespages']['extendedFields'] = null;

$config['pcfeaturespages']['isRestrictedContent'] = FALSE;

//main picture settings
$config['pcfeaturespages']['allowedTypes'] 					= 'jpg|png';
$config['pcfeaturespages']['mainPicture'] 					= 1;
$config['pcfeaturespages']['mainPictureMandatory'] 		= 0;
$config['pcfeaturespages']['mainPictureMandatoryError'] 	= 'Field <strong>image</strong> is mandatory.';
$config['pcfeaturespages']['mainPicturePath'] 				= '../userfiles/pcfeaturespages';
$config['pcfeaturespages']['mainPictureMaxWidth'] 			= 980;
$config['pcfeaturespages']['mainPictureMaxHeight'] 		= 1200;
$config['pcfeaturespages']['mainPictureMinWidth'] 			= 30;
$config['pcfeaturespages']['mainPictureMinHeight'] 		= 44;

//gallery pictures
$config['pcfeaturespages']['galleryPicturesPath']   = '../pictures/pcfeaturespages';
$config['pcfeaturespages']['galleryPicturesThumbsInfo'] = array(
																	0 => array( 'width' => 100, 'height' => 100 ),
																	1 => array( 'width' => 100, 'height' => 50 )
																);


//index page
$config['pcfeaturespages']['indexAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['pcfeaturespages']['indexAdditional']['js'] 		= null;
$config['pcfeaturespages']['indexAdditional']['outerjs'] 	= null;
$config['pcfeaturespages']['indexItemsOrderBy'] 				= 'position';
$config['pcfeaturespages']['indexItemsOrder'] 					= 'asc';
$config['pcfeaturespages']['indexTemplate'] 					= 'items/pcfeaturespages/showItems.tpl';

//addnew page
$config['pcfeaturespages']['addNewAdditional']['css'] 		= array(); //array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['pcfeaturespages']['addNewAdditional']['js'] 		= array('tiny.init.js');
$config['pcfeaturespages']['addNewAdditional']['outerjs'] = array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																			));
$config['pcfeaturespages']['addNewTemplate'] 				= 'items/pcfeaturespages/itemsAddAction.tpl';
$config['pcfeaturespages']['addNewDefaultHeader'] 		= 'Add new page';
$config['pcfeaturespages']['addNewDefaultSubHeader'] 	= '';
$config['pcfeaturespages']['addNewSuccessHeaderAllLangs']	= 'Data has been successfully saved in all languages.';
$config['pcfeaturespages']['addNewSuccessHeader'] 		= 'Data has been successfully saved.';
$config['pcfeaturespages']['addNewSuccessSubHeader'] 	= '';
$config['pcfeaturespages']['addNewErrorHeader'] 			= 'Data has not been saved';
$config['pcfeaturespages']['addNewErrorSubHeader'] 		= '';
$config['pcfeaturespages']['addNewValidationConfig']	= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',																			
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',																			
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',																			
																			'rules'   => 'trim|required|is_natural'
																		)	
																);
																
//edit page
$config['pcfeaturespages']['editAdditional']['css'] 		= array('jquery.fancybox.css');//array('uitheme/cupertino/jquery-ui-1.7.1.custom.css', 'jquery.fancybox.css');
$config['pcfeaturespages']['editAdditional']['js'] 			= array('tiny.init.js');
$config['pcfeaturespages']['editAdditional']['outerjs'] 	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config['pcfeaturespages']['editTemplate'] 				= 'items/pcfeaturespages/itemsEditAction.tpl';
$config['pcfeaturespages']['editDefaultTabName']			= 'Edit: ';
$config['pcfeaturespages']['editDefaultHeader'] 			= 'Edit page';
$config['pcfeaturespages']['editDefaultSubHeader'] 		= '';
$config['pcfeaturespages']['editSuccessHeader'] 			= 'Data has been successfully saved.';
$config['pcfeaturespages']['editSuccessSubHeader'] 		= '';
$config['pcfeaturespages']['editErrorHeader'] 			= 'Data has not been saved.';
$config['pcfeaturespages']['editErrorSubHeader'] 		= '';
$config['pcfeaturespages']['editValidationConfig']		= array(
																	array(
																			'field'   => 'title',
																			'label'   => 'Title',
																			'rules'   => 'trim|strip_tags|required'
																		),
																	array(
																			'field'   => 'slug',
																			'label'   => 'Slug',
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'content',
																			'label'   => 'Content',																			
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'lead',
																			'label'   => 'Lead',																			
																			'rules'   => 'trim'
																		),
																	array(
																			'field'   => 'position',
																			'label'   => 'Position',																			
																			'rules'   => 'trim|required|is_natural'
																		)	
																);
																
																
//remove page
$config['pcfeaturespages']['advancedRemove']						= true;	
$config['pcfeaturespages']['removeAdditional']['css']			= array();
$config['pcfeaturespages']['removeAdditional']['js']			= array();
$config['pcfeaturespages']['removeAdditional']['outerjs']		= array();
$config['pcfeaturespages']['removeTemplate']					= 'items/pcfeaturespages/removeItem.tpl';
$config['pcfeaturespages']['removeDefaultTabName']			= 'Remove page';
$config['pcfeaturespages']['removeDefaultHeader']			= 'Remove page';
$config['pcfeaturespages']['removeDefaultSubHeader'] 		= '';
$config['pcfeaturespages']['removeSuccessHeader']			= 'Page has been deleted:';
$config['pcfeaturespages']['removeSuccessSubHeader']		= '';
$config['pcfeaturespages']['removeErrorHeader']				= 'Page has not been deleted.';
$config['pcfeaturespages']['removeErrorSubHeader']			= '';		