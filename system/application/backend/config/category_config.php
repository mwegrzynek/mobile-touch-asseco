<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*
 * Site categories
 *
 *
 *
*/

//general settings
$config['industries']['itemType']       = 1;
$config['industries']['mainName']       = 'industries';
$config['industries']['activeMenuItem'] = 7;
$config['industries']['mainLink']       = 'category/industries';
$config['industries']['models']         = null;
$config['industries']['ifFiltered']     = TRUE;
$config['industries']['secondMenu']     = array(
											0 => array(	"name"   => "See all",
															"link"   => "category/industries"),
														
											1 => array(	"name"   => "Add new industry",
												"link"   => "category/industries/addnew")
										);
$config['industries']['menuLastEditedVarName'] 	= 'lastEditedEntryCat';
$config['industries']['menuLastEditedText'] 		= 'Last edited industry:';
$config['industries']['menuLastEditedLink'] 		= 'category/industries/edit/';
$config['industries']['metaDataType'] = 101;
$config['industries']['nestedNodes'] = TRUE;

//extended field [filedname => default]
$config['industries']['extendedFields'] = null;

//main picture settings
$config['industries']['allowedTypes']              = 'jpg|png';
$config['industries']['mainPicture']               = 1;
$config['industries']['mainPictureMandatory']      = 0;
$config['industries']['mainPictureMandatoryError'] = 'Field <strong>image</strong> is mandatory.';
$config['industries']['mainPicturePath']           = '../pictures/categories/industries';
$config['industries']['mainPictureMaxWidth']       = 314;
$config['industries']['mainPictureMaxHeight']      = 160;
$config['industries']['mainPictureMinWidth']       = 314;
$config['industries']['mainPictureMinWidth']       = 160;
$config['industries']['mainPictureThumbWidth']     = 0;


//index page
$config['industries']['indexAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['industries']['indexAdditional']['js'] 		= null;
$config['industries']['indexAdditional']['outerjs'] = null;
$config['industries']['indexItemsOrder'] = 'name asc';
$config['industries']['indexTemplate'] = 'categories/industries/showCategories.tpl';

//addnew page
$config['industries']['addNewAdditional']['css'] 	= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['industries']['addNewAdditional']['js'] 		= array('tiny.init.js');
$config['industries']['addNewAdditional']['outerjs'] = array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																			));
$config['industries']['addNewTemplate']         = 'categories/industries/categoryAddAction.tpl';
$config['industries']['addNewDefaultHeader']    = 'Add industry';
$config['industries']['addNewDefaultSubHeader'] = '';
$config['industries']['addNewSuccessHeader']    = 'Data has been successfully saved.';
$config['industries']['addNewSuccessSubHeader'] = '';
$config['industries']['addNewErrorHeader']      = 'Data has not been saved';
$config['industries']['addNewErrorSubHeader']   = '';
$config['industries']['addNewValidationConfig'] = array(
																		array(
																				'field'   => 'name',
																				'label'   => 'Name',
																				'rules'   => 'trim|strip_tags|required'
																			),
																		array(
																				'field'   => 'position',
																				'label'   => 'Position',
																				'rules'   => 'trim|required|numeric'
																			),
																		array(
																				'field'   => 'slug',
																				'label'   => 'Slug',
																				'rules'   => 'trim'
																			),
																		array(
																				'field'   => 'description',
																				'label'   => 'Description',
																				'rules'   => 'trim'
																			)
																);
																
//edit page
$config['industries']['editAdditional']['css'] 		= array('jquery.fancybox.css'); //array('uitheme/cupertino/jquery-ui-1.7.1.custom.css', 'jquery.fancybox.css');
$config['industries']['editAdditional']['js'] 		= array('tiny.init.js');
$config['industries']['editAdditional']['outerjs'] 	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config['industries']['editTemplate'] 					= 'categories/industries/categoryEditAction.tpl';
$config['industries']['editDefaultTabName']			= 'Edit: ';
$config['industries']['editDefaultHeader'] 			= 'Edit industry';
$config['industries']['editDefaultSubHeader'] 		= '';
$config['industries']['editSuccessHeader'] 			= 'Data has been successfully saved.';
$config['industries']['editSuccessSubHeader'] 		= '';
$config['industries']['editErrorHeader'] 				= 'Data has not been saved.';
$config['industries']['editErrorSubHeader'] 			= '';
$config['industries']['editValidationConfig']		= array(
																		array(
																				'field'   => 'name',
																				'label'   => 'Name',
																				'rules'   => 'trim|strip_tags|required'
																			),
																		array(
																				'field'   => 'position',
																				'label'   => 'Position',
																				'rules'   => 'trim|required|numeric'
																			),
																		array(
																				'field'   => 'slug',
																				'label'   => 'Slug',
																				'rules'   => 'trim'
																			),
																		array(
																				'field'   => 'description',
																				'label'   => 'Description',
																				'rules'   => 'trim'
																			)
																);
																
//remove page
$config['industries']['advancedRemove']              = true;	
$config['industries']['removeAdditional']['css']     = array();
$config['industries']['removeAdditional']['js']      = array();
$config['industries']['removeAdditional']['outerjs'] = array();
$config['industries']['removeTemplate']              = 'categories/industries/removeCategory.tpl';
$config['industries']['removeDefaultTabName']        = 'Delete industry';
$config['industries']['removeDefaultHeader']         = 'Delete industry';
$config['industries']['removeDefaultSubHeader']      = '';
$config['industries']['removeSuccessHeader']         = 'Industry has been deleted:';
$config['industries']['removeSuccessSubHeader']      = '';
$config['industries']['removeErrorHeader']           = 'Industry has not been deleted.';
$config['industries']['removeErrorSubHeader']        = '';				


/*
 * Catsitetexts
 *
 *
 *
*/

//general settings
$config['catsitetexts']['itemType'] = 3;
$config['catsitetexts']['mainName'] = 'catsitetexts';
$config['catsitetexts']['activeMenuItem'] = 15;
$config['catsitetexts']['mainLink'] = 'category/catsitetexts';
$config['catsitetexts']['models'] = null;
$config['catsitetexts']['ifFiltered'] = TRUE;
$config['catsitetexts']['secondMenu'] = array(
											0 => array(	"name"   => "See all",
															"link"   => "category/catsitetexts"),
														
											1 => array(	"name"   => "Dodaj nową kategorię",
												"link"   => "category/catsitetexts/addnew")
										);
$config['catsitetexts']['menuLastEditedVarName'] 	= 'lastEditedSiteTextsCat';
$config['catsitetexts']['menuLastEditedText'] 		= 'Ostatnio edytowana kategoria:';
$config['catsitetexts']['menuLastEditedLink'] 		= 'category/catsitetexts/edit/';
$config['catsitetexts']['metaDataType'] = -1;
$config['catsitetexts']['nestedNodes'] = TRUE;

//extended field [filedname => default]
$config['catsitetexts']['extendedFields'] = null;

//main picture settings
$config['catsitetexts']['allowedTypes'] 					= 'jpg|png';
$config['catsitetexts']['mainPicture'] 					= 1;
$config['catsitetexts']['mainPictureMandatory'] 		= 0;
$config['catsitetexts']['mainPictureMandatoryError'] 	= 'Pole <strong>załącznik</strong> jest wymagane.';
$config['catsitetexts']['mainPicturePath'] 				= '../pictures/categories/catsitetexts';
$config['catsitetexts']['mainPictureMaxWidth'] 			= 1000;
$config['catsitetexts']['mainPictureMaxHeight'] 		= 1000;
$config['catsitetexts']['mainPictureMinWidth'] 			= 100;
$config['catsitetexts']['mainPictureMinWidth'] 			= 100;
$config['catsitetexts']['mainPictureThumbWidth'] 		= 100;


//index page
$config['catsitetexts']['indexAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['catsitetexts']['indexAdditional']['js'] 		= null;
$config['catsitetexts']['indexAdditional']['outerjs'] = null;
$config['catsitetexts']['indexItemsOrder'] = 'name asc';
$config['catsitetexts']['indexTemplate'] = 'categories/catsitetexts/showCategories.tpl';

//addnew page
$config['catsitetexts']['addNewAdditional']['css'] 		= array('uitheme/cupertino/jquery-ui-1.7.1.custom.css');
$config['catsitetexts']['addNewAdditional']['js'] 			= array('tiny.init.js');
$config['catsitetexts']['addNewAdditional']['outerjs'] = array(
																			array(
																				'name' => 'tiny_mce.js',
																				'src'  => 'tinymce'
																			));
$config['catsitetexts']['addNewTemplate'] 				= 'categories/catsitetexts/categoryAddAction.tpl';
$config['catsitetexts']['addNewDefaultHeader'] 			= 'Add new category';
$config['catsitetexts']['addNewDefaultSubHeader'] 		= '';
$config['catsitetexts']['addNewSuccessHeader'] 			= 'Data has been successfully saved.';
$config['catsitetexts']['addNewSuccessSubHeader'] 		= '';
$config['catsitetexts']['addNewErrorHeader'] 			= 'Data has not been changed';
$config['catsitetexts']['addNewErrorSubHeader'] 		= '';
$config['catsitetexts']['addNewValidationConfig']	= array(
																		array(
																				'field'   => 'name',
																				'label'   => 'Nazwa',
																				'rules'   => 'trim|strip_tags|required'
																			),
																		array(
																				'field'   => 'position',
																				'label'   => 'Pozycja',
																				'rules'   => 'trim'
																			),
																		array(
																				'field'   => 'slug',
																				'label'   => 'Slug',
																				'rules'   => 'trim'
																			),
																		array(
																				'field'   => 'description',
																				'label'   => 'Opis kategorii',
																				'rules'   => 'trim'
																			)
																);
																
//edit page
$config['catsitetexts']['editAdditional']['css'] 		= array('jquery.fancybox.css'); //array('uitheme/cupertino/jquery-ui-1.7.1.custom.css', 'jquery.fancybox.css');
$config['catsitetexts']['editAdditional']['js'] 		= array('tiny.init.js');
$config['catsitetexts']['editAdditional']['outerjs'] 	= array(
																		array(
																			'name' => 'tiny_mce.js',
																			'src'  => 'tinymce'
																		));
$config['catsitetexts']['editTemplate'] 					= 'categories/catsitetexts/categoryEditAction.tpl';
$config['catsitetexts']['editDefaultTabName']			= 'Edytuj: ';
$config['catsitetexts']['editDefaultHeader'] 			= 'Edytuj kategorię';
$config['catsitetexts']['editDefaultSubHeader'] 		= '';
$config['catsitetexts']['editSuccessHeader'] 			= 'Data has been successfully saved.';
$config['catsitetexts']['editSuccessSubHeader'] 		= '';
$config['catsitetexts']['editErrorHeader'] 				= 'Data has not been changed.';
$config['catsitetexts']['editErrorSubHeader'] 			= '';
$config['catsitetexts']['editValidationConfig']		= array(
																		array(
																				'field'   => 'name',
																				'label'   => 'Nazwa',
																				'rules'   => 'trim|strip_tags|required'
																			),
																		array(
																				'field'   => 'position',
																				'label'   => 'Pozycja',
																				'rules'   => 'trim'
																			),
																		array(
																				'field'   => 'slug',
																				'label'   => 'Slug',
																				'rules'   => 'trim'
																			),
																		array(
																				'field'   => 'description',
																				'label'   => 'Opis kategorii',
																				'rules'   => 'trim'
																			)		
																);
																
//remove page
$config['catsitetexts']['advancedRemove']						= true;	
$config['catsitetexts']['removeAdditional']['css']			= array();
$config['catsitetexts']['removeAdditional']['js']			= array();
$config['catsitetexts']['removeAdditional']['outerjs']	= array();
$config['catsitetexts']['removeTemplate']					= 'categories/catsitetexts/removeCategory.tpl';
$config['catsitetexts']['removeDefaultTabName']			= 'Usuń stronę';
$config['catsitetexts']['removeDefaultHeader']			= 'Usuń stronę';
$config['catsitetexts']['removeDefaultSubHeader'] 		= '';
$config['catsitetexts']['removeSuccessHeader']			= 'Kategoria została usunięta:';
$config['catsitetexts']['removeSuccessSubHeader']		= '';
$config['catsitetexts']['removeErrorHeader']				= 'Kategoria nie została usunięta.';
$config['catsitetexts']['removeErrorSubHeader']			= '';	


