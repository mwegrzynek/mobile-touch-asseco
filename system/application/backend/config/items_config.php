<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH.'config/items/pages.php';

require_once APPPATH.'config/items/mtpages.php';
require_once APPPATH.'config/items/featurespages.php';
require_once APPPATH.'config/items/servicespages.php';
require_once APPPATH.'config/items/conceptspages.php';
require_once APPPATH.'config/items/overviewpages.php';

require_once APPPATH.'config/items/clients.php';
require_once APPPATH.'config/items/testimonials.php';
require_once APPPATH.'config/items/casestudies.php';

require_once APPPATH.'config/items/news.php';

require_once APPPATH.'config/items/sitetexts.php';
require_once APPPATH.'config/items/references.php';
require_once APPPATH.'config/items/sliders.php';
require_once APPPATH.'config/items/countries.php';

require_once APPPATH.'config/items/mainpages.php';
require_once APPPATH.'config/items/teamleaders.php';

require_once APPPATH.'config/items/hpsliders.php';
require_once APPPATH.'config/items/logoslider.php';

require_once APPPATH.'config/items/pconnectorpages.php';
require_once APPPATH.'config/items/pcfeaturespages.php';
require_once APPPATH.'config/items/pcservicespages.php';
require_once APPPATH.'config/items/pcaboutpages.php';

require_once APPPATH.'config/items/notifications.php';

require_once APPPATH.'config/items/whyus.php';
require_once APPPATH.'config/items/whymobiletouch.php';
require_once APPPATH.'config/items/companypages.php';

require_once APPPATH.'config/items/raporttestimonials.php';
require_once APPPATH.'config/items/partnerstestimonials.php';