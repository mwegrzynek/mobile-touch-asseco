{include file="header.tpl"}
		<h1>
		{if $norecords}
		No group found.
		{else}
		Manage admin groups
		{/if}
		</h1>
		{if $subheader}
		<h2>{$subheader}</h2>
		{/if}
		
		{if $itemsList}
		<div class="medium">			
			<table class="item-table">
				<thead>
					<tr>
						<th>Name</th>						   
						<th colspan="2">Options</th>
					</tr>               
				</thead>
				<tbody>
				{foreach from=$itemsList item="item"}
					<tr{cycle values=' class="diff",'}>
						<td>{if $item->name neq 'admin'}<a href="authgroups/edit/{$item->id}">{/if}{$item->description}{if $item->name eq 'admin'}</a>{/if}</td>
						<td>{if $item->name neq 'admin'}<a href="authgroups/edit/{$item->id}">Edit</a>{else}-{/if}</td>
						<td>{if $userData.group eq 'admin' and $item->name neq 'admin'}<a href="authgroups/remove/{$item->id}" class="del-opt" rel="dialog-groups">Delete</a>{else}-{/if}</td>
					</tr>               
				{/foreach}
				</tbody>
			</table>         
		</div><!-- .medium -->
		{/if} 
		<div class="dialog-box" id="dialog-groups" title="Delete group?">
			<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
				<span style="display:block; padding-left: 25px;">Group <em></em> Will be deleted.<br/> Accounts from this group will be moved to no-group (without any privileges). Are you sure?</span>
			</p>
		</div>     
{include file="footer.tpl"}