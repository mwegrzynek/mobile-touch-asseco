{include file="header.tpl"}
		<h1>{$header}{if $subheader} -&gt; <em>{$subheader}</em>{/if}</h1>
		<form action="authgroups/{$formLink}" method="post" class="form-b">
			{if $errors}
			<h3 class="warning">{$errorHeader}</h3>
			<ul class="warning">
			{foreach from=$errors item=error}
				<li>{$error}</li>
			{/foreach}
			</ul>
			{/if}

			<div class="tabs-container">
				<ul class="tabs">					
					<li><a href="#main-data"><span>Main data</span></a></li>					
				</ul>

				<div id="permissions-data" class="ui-tabs-panel">
					<div class="tab-content">
						
						<ul class="form-items">               
							<li>
								<label for="description">Group name <em>*</em>:</label> <input type="text" name="description" id="description" value="{$backData.description}"/>                  
							</li>							
						</ul>
						{foreach $permissions as $type}
							<div class="section-a{if $type@last} last{/if}">
								<div class="head-section"><h3 class="header-a">{$type->name}</h3><p><input type="checkbox" class="check_all" name="check_all" value="" id="check_all{$type->id}"/></p></div>
								{if is_array($type->actions)}
								<ul id="permissions{$type->id}" class="form-items1 frm-it-a{if $type@last} last{/if}">
									{foreach $type->actions as $action}
										{assign var=actionCode value=$type->identity|cat:'-'|cat:$action->id}
										<li>
											<input type="checkbox" {if is_array($backData.permissions) and in_array($actionCode, $backData.permissions)}checked="checked"{/if} name="permissions[]" id="{$action->action}-{$type->id}-{$action->id}" value="{$type->identity}-{$action->id}"/> <label for="{$action->action}-{$type->id}-{$action->id}">{$action->name}</label>
										</li>
									{/foreach}
								</ul>
								{/if}
							</div><!-- .section-a -->
						{/foreach}						
					</div>					
				</div>
			</div>
			<p class="btn">			
				<button type="submit">
					Change &gt;
				</button>
			</p>
		</form>
{include file="footer.tpl"}