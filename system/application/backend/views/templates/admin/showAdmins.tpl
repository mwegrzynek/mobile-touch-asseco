{include file="header.tpl"}
		<h1>
		{if $norecords}
		No admin found.
		{else}
		Mange admin accounts
		{/if}
		</h1>
		{if $subheader}
		<h2>{$subheader}</h2>
		{/if}
		
		{if $itemsList}
		<div class="medium">			
			<table class="item-table">
				<thead>
					<tr>
						<th>Name</th>
						<th>First name</th>                              
						<th>Last name</th>                              
						<th class="toleft">E-mail</th>                              
						<th class="toleft">Group</th>                     
						<th colspan="2">Options</th>
					</tr>               
				</thead>
				<tbody>
				{foreach from=$itemsList item="item"}
					<tr{cycle values=' class="diff",'}>
						<td><a href="auth/edit/{$item.id}">{$item.username}</a></td>
						<td>{$item.first_name}</td>
						<td>{$item.last_name}</td>
						<td class="toleft"><a href="mailto:{$item.email}">{$item.email}</a></td>                              
						<td class="toleft">{$item.group_description}</td>
						<td><a href="auth/edit/{$item.id}">Edit</a></td>
						<td>{if $userData.group eq 'admin' and $item.id neq $userData.userid}<a href="auth/remove/{$item.id}" class="del-opt" rel="dialog-admin">Delete</a>{else}-{/if}</td>
					</tr>               
				{/foreach}
				</tbody>
			</table>         
		</div><!-- .medium -->
		{/if} 
		<div class="dialog-box" id="dialog-admin" title="Delete administrator?">
			<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
				<span style="display:block; padding-left: 25px;">Admin <em></em> will be deleted.<br/> Are you sure?</span>
			</p>			
		</div>     
{include file="footer.tpl"}