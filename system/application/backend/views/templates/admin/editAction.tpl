{include file="header.tpl"}
		<h1>{$header}{if $subheader} -&gt; <em>{$subheader}</em>{/if}</h1>
		<form action="auth/{$formLink}" method="post" class="form-b">
			{if $errors}
			<h3 class="warning">{$errorHeader}</h3>
			<ul class="warning">
			{foreach from=$errors item=error}
				<li>{$error}</li>
			{/foreach}
			</ul>
			{/if}
			
			<div class="tabs-container">
				<ul class="tabs">					
					<li><a href="#main-data"><span>Main data</span></a></li>					
					<li><a href="#password-data"><span>Password change</span></a></li>					
				</ul>
			
				<div id="main-data" class="ui-tabs-panel">
					<div class="tab-content">
						<ul class="form-items">               
							<li>
								<label for="username">Admin name <em>*</em>:</label> <input type="text" name="username" id="username" value="{$backData.username}"/>                  
							</li>               
							<li class="diff">
								<label for="email">E-mail <em>*</em>:</label> <input type="text" name="email" id="email" value="{$backData.email}"/>                  
							</li>
							<li>
								<label for="first_name">First Name <em>*</em>:</label> <input type="text" name="first_name" id="first_name" value="{$backData.first_name}"/>                  
							</li>               
							<li class="diff">
								<label for="last_name">Last Name <em>*</em>:</label> <input type="text" name="last_name" id="last_name" value="{$backData.last_name}"/>                  
							</li>
							{if $isAdmin}
							<li>
								<label for="group_id">Group:</label> 
								<select name="group_id" id="group_id">
									{if $backData.group_id eq -1}
										<option value="-1" selected="selected">No group (no privileges)</option>
									{/if}							
									{foreach $backData.groups as $item}
										<option value="{$item->id}" {if $item->id eq $backData.group_id}selected="selected"{/if}>{$item->description}</option>
									{/foreach}
								</select>
							</li>
							{/if}
						</ul>
					</div>                     
					<p class="btn">			
						<button type="submit" name="change-data" value="1">
							Submit &gt;
						</button>
					</p>
				</div>
				
				<div id="password-data" class="ui-tabs-panel">
					<div class="tab-content">
						<ul class="form-items">							
							{*<li>
								<label for="old">Stare Hasło <em>*</em>:</label> <input type="password" name="old" id="old" value=""/>                  
							</li>	*}					
							
							<li class="diff">
								<label for="new">New password <em>*</em>:</label> <input type="password" name="new" id="new" value=""/>                  
							</li>
							<li>
								<label for="new_confirm">Confirm password <em>*</em>:</label> <input type="password" name="new_confirm" id="new_confirm" value=""/>                  
							</li>
						</ul>
					</div>                     
					<p class="btn">			
						<button type="submit" name="change-pass" value="1">
							Submit &gt;
						</button>
					</p>
				</div>
		</form>
{include file="footer.tpl"}