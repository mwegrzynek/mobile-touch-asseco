{include file="header.tpl"}
      <h1>
         {if !$itemsList|@count}
         No menu items found
         {else}
         All menu items ({$allItems})
         {/if}
      </h1>
      {if $subheader}
      <h2>{$subheader}</h2>
      {/if}
      
		
		<div id="filterBox">
			<form action="{$link}/filter" method="post">
				<p>
					<input type="text" id="filter" name="filter" value="{$filter.textSearchFilter}" title="Szukaj"/>
					<select name="parentFilter">
						<option value="-1">--All items--</option>						
						{if $itemsTree}
							{include file="common/menu_tree.tpl" scope=parent}
							{call menu data=$itemsTree currentItem=$filter.parentFilter forbiddenItem=$forbiddenItem}
						{/if}							
					</select>
				</p> 
				<p class="btn">
					<button type="submit">
						Szukaj &gt;
					</button>						
				</p>
			</form>             
		</div>
		
		
		
      {if $itemsList}		
		<div class="medium">
			{include file='pagination.tpl'}
         {include file='perPage.tpl'}
         <table class="item-table">
            <thead>
               <tr>
                  <th class="toleft">Name</th>                  
                  <th class="toleft">Parent</th>                  
                  <th colspan="2">Options</th>
               </tr>               
            </thead>
            <tbody>
            {foreach from=$itemsList item=item}
               <tr{cycle values=' class="diff",'}>                  
                  <td class="toleft"><a href="{$link}/edit/{$item->menu_id}">{$item->name}</a></td>                                                              
                  <td class="toleft">
                  	{if $item->parents}
								{foreach from=$item->parents item=parent key=key name=loop1}
									<a href="{$link}/edit/{$parent->id}">{$parent->name}</a> {if !$smarty.foreach.loop1.last}-> {/if}
								{/foreach}
							{/if}                  	
                  </td>                                                              
                  <td><a href="{$link}/edit/{$item->menu_id}">Edit</a></td>                  
                  <td><a href="{$link}/removeElement/{$item->menu_id}" class="del-opt" rel="dialog-menu">Delete</a></td>  
               </tr>               
            {/foreach}
            </tbody>
         </table>
			{include file='pagination.tpl'}
		</div>
      {/if}     
      <div class="dialog-box" id="dialog-menu" title="Delete item?">
			<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
				<span style="display:block; padding-left: 25px;">Item will be deleted<br/> Are You sure?</span>
			</p>
			<p>In next step You can decide where child elements will be moved.</p>
		</div> 
{include file="footer.tpl"}