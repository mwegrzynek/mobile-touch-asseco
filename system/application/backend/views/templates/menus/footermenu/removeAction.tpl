{include file="header.tpl"}

		<h1>Choose item where all items from current item will be moved: <em>{$menuName}</em></h1>
		<div id="tabContentContainer">         
			<form action="{$link}/removeElement/{$menuid}" method="post" id="stepForm">
				{if $errors}
				<h3 class="warning">{$errorHeader}</h3>
				<ul class="warning">
				{foreach from=$errors item=error}
					<li>{$error}</li>
				{/foreach}
				</ul>
				{/if}
			
				<ul class="form-items">
					<li>                     
						<label for="parentid">Choose item:</label> 
						<select name="parentid" id="parentid">
						 	<option value="0">Root</option>
							{*{if $itemsTree}
								{foreach from=$itemsTree item=item key=key}                        
								{if $item->id neq $menuid}
								<option value="{$item->id}" class="level-0">								
									{$item->name}
								</option>
								{/if}
								{if $item->children}
									{include file="common/menu_tree_recursion.tpl" element=$item->children level=1 forbiddenItem=$menuid}
								{/if}
								{/foreach}
							{/if}*}
							{include file="common/menu_tree.tpl" scope=parent}
							{call menu data=$itemsTree currentItem=$filter.parentFilter forbiddenItem=$menuid}
						</select>                  
					</li>
				</ul>				 
				<p class="btn">				
					<button type="submit">
						Delete item and confirm this options &gt;
					</button>
				</p>		
			</form>
		</div>
			
{include file="footer.tpl"}