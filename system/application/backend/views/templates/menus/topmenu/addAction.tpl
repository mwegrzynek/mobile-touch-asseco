{include file="header.tpl"}
		<h1>{$header}</h1>
		{if $subheader}<h2 class="helper">{$subheader}</h2>{/if}

		<div id="tabContentContainer">         
			<form action="{$link}/addnew" method="post" id="stepForm" enctype="multipart/form-data">
				{if $errors}
				<h3 class="warning">{$errorHeader}</h3>
				<ul class="warning">
				{foreach from=$errors item=error}
					<li>{$error}</li>
				{/foreach}
				</ul>
				{/if}
			
				<ul class="form-items">                
						 
					<li>                     
						<label for="parentid">Element nadrzędny:</label> 
						<select name="parentid" id="parentid">
							<option value="0"{if !$backData.parentid} selected="selected"{/if}>Root</option>							
							{if $itemsTree}
								{include file="common/menu_tree.tpl" scope=parent}								
								{call menu data=$itemsTree currentItem=$backData.parentid forbiddenItem=$forbiddenItem}
							{/if}				
						</select>	               
					</li>
					
					
					
					<li class="diff">
						<label for="menuName">Nazwa <em>*</em>:</label> <input type="text" name="menuName" id="menuName" value="{$backData.menuName}"/>     
						{*<input type="hidden" name="parentid" value="0" id="parentid"/>*}
						{*<input type="hidden" name="position" value="1" id="position"/>*}
					</li>
					
					<li>                     
						<label for="link">Link <em>*</em>:</label> <input type="text" name="link" id="link" value="{$backData.link}"/>                
					</li>
					
					<li class="diff">
						<label for="class">Klasa tml:</label> <input type="text" name="class" id="class" value="{$backData.class}"/>					
					</li>
					
					{*<li class="diff">
						<input type="hidden" name="MAX_FILE_SIZE" value="524288"/>
						<label for="thefile">Obrazek <span>(.jpg|.png)</span></label>
						<input type="file" id="thefile" name="thefile" size="47"/>
					</li>*}
					
					<li>                     
						<label for="position">Pozycja:</label> <input type="text" name="position" id="position" value="{$backData.content|default:1}" class="short-input"/> 
					</li>	     
					
					<li class="diff">
						<label for="description">Krótki opis:</label>
						<textarea rows="12" cols="70" id="description" name="description">{$backData.description|htmlspecialchars}</textarea>						
					</li>						
				</ul>      
				<ul class="form-items1">
					<li>
						<input type="checkbox" {if $backData.isActive}checked="checked"{/if} name="isActive" id="isActive" value="1"/> <label for="isActive">Czy aktywne</label>               
					</li>
				</ul>
				{include file="common/add_buttons.tpl"}
			</form>         
		</div>
			
{include file="footer.tpl"}