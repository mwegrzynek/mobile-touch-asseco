{include file="header.tpl"}
		<h1>{$header}: <em>{$backData.menuName}</em></h1>
		{if $subheader}<h2 class="helper">{$subheader}</h2>{/if}
		
		<form action="{$link}/edit/{$backData.menuid}" method="post" class="form-a" enctype="multipart/form-data">
			{if $errors}
			<h3 class="warning">{$errorHeader}</h3>
			<ul class="warning">
			{foreach from=$errors item=error}
				<li>{$error}</li>
			{/foreach}
			</ul>
			{/if}

			{if !$noImages}				
				{assign var=path value=$picturePath|cat:'/'|cat:$imageBackData.main_image}			
				{if $path|is_file}
					{assign var=isFile value=true}
					<div id="pictureContainer">
						<a href="{$path}" class="fancy"><img src="{$path}" alt="" width="175"/></a>
						<p class="imagesubtitle">
							<a href="{$link}/removeImage/{$backData.menuid}">Remove image</a>
						</p>
					</div>
				{/if}
			{/if}
			<div{if $isFile} class="withImage"{/if} id="independentSection">
				<ul class="form-items">
					<li>
						<label for="parentid">Parent item:</label>
						<select name="parentid" id="parentid">
							<option value="0"{if !$backData.parentid} selected="selected"{/if}>Root</option>							
							{if $itemsTree}								
								{include file="common/menu_tree.tpl" scope=parent}
								{call menu data=$itemsTree currentItem=$backData.parentid forbiddenItem=$forbiddenItem}
							{/if}			
						</select>
					</li>	
					{*<li class="diff">
						<input type="hidden" name="MAX_FILE_SIZE" value="524288"/>
						<label for="thefile">Obrazek <span>(.jpg|.png)</span></label>
						<input type="file" id="thefile" name="thefile" size="47"/>						
					</li>		*}		
				</ul>				
				<p class="btn">
					<button type="submit">
						Change data &gt;
					</button>
				</p>
			</div>
		</form>	

		<h2>Main data:</h2>
		{if $backData.languages|@count > 1}
		<div class="tabs-container">			
			<ul class="tabs">
				{foreach from=$backData.languages item=lang}
				<li><a href="#{$lang.lang_code}"><span>{$lang.name}</span></a></li>
				{/foreach}
			</ul>
		{/if}		
			<div class="tab-content">
				{foreach from=$backData.languages item=lang name=langLoop key=key}
				<div id="{$lang.lang_code}" class="ui-tabs-panel">
					<form action="{$link}/edit/{$backData.menuid}#{$lang.lang_code}" method="post" class="stepForm">
						{if $subErrors[$lang.id]}
						<h3 class="warning">{$errorHeader[$lang.id]}</h3>
						<ul class="warning">
						{foreach from=$subErrors[$lang.id] item=error}
							<li>{$error}</li>
						{/foreach}
						</ul>
						{/if}					
						<ul class="form-items">
							<li>
								<label for="menuName{$lang.id}">Name <em>*</em>:</label> <input type="text" name="menuName" id="menuName{$lang.id}" value="{$lang.menuInfo.name}"/>
								{*<input type="hidden" name="link" value="{$lang.menuInfo.link}" id="link{$lang.id}"/>*}
								{*<input type="hidden" name="position" value="{$lang.menuInfo.position}" id="position{$lang.id}"/>*}
							</li>
							<li class="diff">
								<label for="link{$lang.id}">Link <em>*</em>:</label> <input type="text" name="link" id="link{$lang.id}" value="{$lang.menuInfo.link}"/>
							</li>
							
							<li>
								<label for="class{$lang.id}">Html Class:</label> <input type="text" name="class" id="class{$lang.id}" value="{$lang.menuInfo.class}"/>
							</li>
							
							<li class="diff">
								<label for="position{$lang.id}">Position:</label> <input type="text" name="position" id="position{$lang.id}" value="{$lang.menuInfo.position}" class="short-input"/>							
							</li>
							
							<li>
								<label for="description{$lang.id}">Short description:</label>
								<textarea rows="12" cols="70" id="description{$lang.id}" name="description">{$lang.menuInfo.description|htmlspecialchars}</textarea>
							</li>
						</ul>
						<ul class="form-items1">
							<li>
								<input type="checkbox" {if $lang.menuInfo.isActive}checked="checked"{/if} name="isActive" id="isActive{$lang.id}" value="1"/> <label for="isActive{$lang.id}">Is active?</label>               
							</li>
						</ul>	
						{include	file="common/submit_buttons.tpl"}
					</form>
				</div>
				{/foreach}
			</div>
		{if $backData.languages|@count > 1}</div>{/if}
{include file="footer.tpl"}