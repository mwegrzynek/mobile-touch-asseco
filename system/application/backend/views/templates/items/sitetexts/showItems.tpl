{include file="header.tpl"}
		<h1>
			{if !$itemsList|@count}
			No text or option found
			{else}
			All additional texts and options ({$allItemsCount})
			{/if}
		</h1>
		{if $subheader}
		<h2>{$subheader}</h2>
		{/if}
		<div class="medium">
			<div id="filterBox">
				<form action="{$link}/filter/{$item->id}" method="post">
					<p class="field-a">
						<input type="text" id="searchFilterTxt" name="searchFilterTxt" value="{$filter.searchFilterTxt}" title="Szukaj w tytule i opisie"/>
						<select name="categoryFilter">
							<option value="-1">-- All categories --</option>
							{if $categoryTree}
								{include file="common/menu_tree.tpl" scope=parent}
								{call menu data=$categoryTree currentItem=$filter.categoryFilter forbiddenItem=$forbiddenItem}
							{/if}
						</select>
					</p>
					<p class="btn section-a">
						<button type="submit">
							Search
						</button>
					</p>
				</form>
			</div>

		{if $itemsList}
			{include file='pagination.tpl'}
			{include file='perPage.tpl'}
			<form action="{$link}/removeSelected" method="post" id="item-form">
				<table class="item-table">
					<thead>
						<tr>
							<th class="toleft">&nbsp;</th>
							<th class="toleft">ID</th>
							<th class="toleft">Name</th>
							<th class="toleft">Content</th>
							<th class="toleft">Category</th>
							{*<th class="toleft">Ilość obrazków</th>
							<th class="toleft">Ilość filmów</th>*}
							<th colspan="2">Options</th>
						</tr>
					</thead>
					<tbody>
					{foreach from=$itemsList item=item}
						<tr{cycle values=' class="diff",'}>
							<td><input type="checkbox" name="id[]" value="{$item->id}" id="id-{$item->id}"/></td>
							<td>{$item->id}</td>
							<td class="toleft"><a href="{$link}/edit/{$item->id}">{$item->title}</a></td>
							<td class="toleft">{$item->content|strip_tags|truncate:120:"&hellip;":false}</td>
							<td class="toleft">
								{if $item->categories}
									{foreach $item->categories as $categoryPath}
										{foreach $categoryPath as $category}
											<a href="{$categoryLink}/edit/{$category->category_id}">{$category->name}</a> {if !$category@last}-> {/if}
										{/foreach}
									{/foreach}
								{else}
								No category
								{/if}
							</td>
							<td><a href="{$link}/edit/{$item->id}">Edit</a></td>
							<td><a href="{$link}/removeItem/{$item->id}{if $offset}/{$offset}{/if}" class="del-opt" rel="dialog-sitetext" title="'{$item->title}'">Delete</a></td>
						</tr>
					{/foreach}
					</tbody>
				</table>
				<p class="btn">
					<button type="submit" class="del-opt" rel="dialog-sitetext">Remove selected</button>
					<a href="#item-form" id="mark-link">mark / unmark all</a>
				</p>
			</form>
			{include file='pagination.tpl'}
			{include file="common/importexport.tpl"}
		{/if}
		</div>
		<div class="dialog-box" id="dialog-sitetext" title="Delete text?">
			<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
				<span style="display:block; padding-left: 25px;">Text <em></em> will be deleted.<br/> Are you sure?</span>
			</p>
		</div>
{include file="footer.tpl"}
