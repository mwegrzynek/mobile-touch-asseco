{include file="header.tpl"}
		<h1>{$header}</h1>
		{if $subheader}<h2>{$subheader}</h2>{/if}

		<div class="tab-content">         
			<form action="{$link}/addnew" method="post" class="form-a" enctype="multipart/form-data">
				{if $errors}
				<h3 class="warning">{$errorHeader}</h3>
				<ul class="warning">
				{foreach from=$errors item=error}
					<li>{$error}</li>
				{/foreach}
				</ul>
				{/if}
				
				{include file="common/menu_tree.tpl" scope=parent}
				
				<ul class="form-items"> 	
					<li class="diff">                     
						{include file="common/menu_tree.tpl" scope=parent}
						{if is_array($backData.category_id)}
							{foreach $backData.category_id as $category_id}
								{if $category_id neq -1}								
								{assign var="loopCats" value="loop"}							
								<li class="category_container diff">                     
									<label for="categoryid{$category_id@index}">Category:</label> 
									<select name="category_id[]" id="categoryid{$category_id@index}">							
										<option value="-1">-- no category --</option>
										{call menu data=$categoryTree currentItem=$category_id forbiddenItem=$forbiddenItem}
									</select>	               
								</li>	
								{/if}
							{/foreach}						
						{/if}
						{if !is_array($backData.category_id) or !$loopCats}
							<li class="category_container diff">                     
								<label for="categoryid">Category:</label> 
								<select name="category_id[]" id="categoryid">							
									<option value="-1">-- no category --</option>
									{call menu data=$categoryTree currentItem=$backData.category_id forbiddenItem=$forbiddenItem}
								</select>	               
							</li>	
						{/if}    
					</li>				
					<li>
						<label for="title">Name <em>*</em>:</label> <input type="text" name="title" id="title" value="{$backData.title}"/>   
						<input type="hidden" name="parentid" value="0"/>
						<input type="hidden" name="lead" value=""/>
						<input type="hidden" name="position" value="0"/>
						
						{*<input type="hidden" name="categoryid" value="0"/> *}            
					</li>
					<li class="diff offset">                     
						<label for="slug">Slug:</label> <input type="text" name="slug" id="slug" value="{$backData.slug}"/> <span>If empty, generated automatically.</span>                  
					</li>
					
					{*<li>
						<input type="hidden" name="MAX_FILE_SIZE" value="524288"/>
						<label for="thefile">Obrazek <span>(.jpg|.png)</span></label>
						<input type="file" id="thefile" name="thefile" size="47"/>
					</li>*}
					
					{*<li>
						<label for="lead">Lead:</label>
						<textarea rows="8" cols="90" id="lead" name="lead">{$backData.lead}</textarea>                  
					</li> *}        
					<li class="diff">
						<label for="content-a">Content:</label>
						<textarea rows="6" cols="90" id="content-a" name="content">{$backData.content}</textarea>                  
					</li>
					{*<li class="diff">                     
						<label for="position">Pozycja:</label> <input type="text" name="position" id="position" value="{$backData.position|default:1}" class="short-input"/> 
					</li>	 *}              
				</ul>
				
				<ul class="form-items1">
					<li>
						<input type="checkbox" {*{if $backData.is_active}checked="checked"{/if}*} name="is_active" id="is_active" value="1"/> <label for="is_active">WYSIWYG turn on?</label>               
					</li>
				</ul>
				{*
				<h3 class="group-section">Meta Tagi</h3>
				<ul class="form-items">
					<li>                    
						<label for="metaTitle">Title:</label> <input type="text" name="metaTitle" id="metaTitle" value="{$backData.metaTitle}"/>                     
					</li>
					<li class="diff">
						<label class="type1" for="metaKeywords">Keywords:</label> <textarea rows="3" cols="80" id="metaKeywords" name="metaKeywords">{$backData.metaKeywords}</textarea>            
					</li>
					<li>
						<label class="type1" for="metaDescription">Description:</label> <textarea rows="3" cols="80" id="metaDescription" name="metaDescription">{$backData.metaDescription}</textarea>            
					</li>                               
				</ul> *}
				<p class="btn">					
					<button type="submit">
						Add &gt;
					</button>
					
					{if $languages|@count > 1}
					<input type="hidden" name="next" value="{$languages[1]->lang_code}"/>
					<button class="btn-a" type="submit" name="submit-and-edit-next" value="1">
						Add and edit next language &gt;
					</button>
					
					<button class="btn-a" type="submit" name="submit-and-fill-langs" value="1">
						Add to this and all other languages &gt;
					</button>
					{/if}
				</p>
			</form>         
		</div>         
{include file="footer.tpl"}