{include file="header.tpl"}
		<h1>{$header}: <em>{$itemTitle}</em></h1>
		{if $subheader}<h2>{$subheader}</h2>{/if}             
	  
		{if $errors}
		<h3 class="warning">{$errorHeader}</h3>
		<ul class="warning">
		{foreach from=$errors item=error}
			<li>{$error}</li>
		{/foreach}
		</ul>
		{/if}	
		
		
		{include file="common/menu_tree.tpl" scope=parent}
			
		<form action="{$link}/edit/{$itemid}" method="post" class="form-a fa-a" enctype="multipart/form-data">			
			{if !$noImages}
				{assign var=path value=$picturePath|cat:'/'|cat:$imageBackData.main_image}
				{if $path|is_file}
					{assign var=isFile value=true}
					<div id="pictureContainer">
						<a href="{$path}" class="fancy"><img src="{$path}" alt="" width="175"/></a>
						<p class="imagesubtitle">
							<a href="{$link}/removeImage/{$itemid}">Remove image</a>
						</p>
					</div>
				{/if}
			{/if}
			<div{if $isFile} class="withImage"{/if} id="independentSection">			
				<ul class="form-items">				
					{include file="common/menu_tree.tpl" scope=parent}
					{if is_array($backData.category_id)}
						{foreach $backData.category_id as $category_id}
							{if $category_id neq -1}								
							{assign var="loopCats" value="loop"}							
							<li class="category_container diff">                     
								<label for="categoryid{$category_id@index}">Category:</label> 
								<select name="category_id[]" id="categoryid{$category_id@index}">							
									<option value="-1">-- no category --</option>
									{call menu data=$categoryTree currentItem=$category_id forbiddenItem=$forbiddenItem}
								</select>	               
							</li>	
							{/if}
						{/foreach}						
					{/if}
					{if !is_array($backData.category_id) or !$loopCats}
						<li class="category_container diff">                     
							<label for="categoryid">Category:</label> 
							<select name="category_id[]" id="categoryid">							
								<option value="-1">-- no category --</option>
								{call menu data=$categoryTree currentItem=$backData.category_id forbiddenItem=$forbiddenItem}
							</select>	               
						</li>	
					{/if}    
				</ul>			
				<p class="btn">				
					<button type="submit">
						Zmień &gt;
					</button>
				</p>
			</div>
		</form>
		
		{if $backData.languages|count > 1}
		<div class="tabs-container">            
			<ul class="tabs">            
				{foreach from=$backData.languages item=lang}
				<li><a href="#{$lang.lang_code}"><span>{$lang.name}</span></a></li>
				{/foreach}                
			</ul>
		</div>      
		{/if}
		<div class="tab-content">                  
			{foreach from=$backData.languages item=lang name=langLoop key=key}
			<div id="{$lang.lang_code}" class="ui-tabs-panel">
				<form action="{$link}/edit/{$itemid}#{$lang.lang_code}" method="post" class="form-a">      
					{if $subErrors[$lang.id]}
					<h3 class="warning">{$errorHeader[$lang.id]}</h3>
					<ul class="warning">
					{foreach from=$subErrors[$lang.id] item=error}
						<li>{$error}</li>
					{/foreach}
					</ul>
					{/if}               
					
					<ul class="form-items">                
						<li>							
							<label for="title{$lang.id}">Tytuł <em>*</em>:</label> <input type="text" name="title" id="title{$lang.id}" value="{$lang.itemInfo.title}"/>
							{*<label for="title{$lang.id}">Name <em>*</em>:</label> <span class="content">{$lang.itemInfo.title}</span>
							<input type="hidden" name="title" id="title{$lang.id}" value="{$lang.itemInfo.title}"/>*}
						</li>
						<li class="diff offset">
							<label for="slug{$lang.id}">Slug:</label> <input type="text" name="slug" id="slug{$lang.id}" value="{$lang.itemInfo.slug}"/> <span>If empty, generated automatically.</span>                     
						</li>	
						{*<li>
							<label for="lead{$lang.id}">Lead:</label>
							<textarea rows="8" cols="90" id="lead{$lang.id}" name="lead">{$lang.itemInfo.lead}</textarea>                  
						</li> *}        						
						<li class="diff">
							<label for="content{$lang.id}">Content:</label>
							<textarea rows="6" cols="90" id="content{$lang.id}"{if $lang.itemInfo.is_active} class="mceEditorSimple"{/if} name="content">{$lang.itemInfo.content}</textarea>                     
						</li>
						{*<li class="diff">                     
							<label for="position{$lang.id}">Pozycja:</label> <input type="text" name="position" id="position{$lang.id}" value="{$lang.itemInfo.position}" class="short-input"/> 
						</li>	*}					
					</ul>	
					<ul class="form-items1">
						<li>
							<input type="checkbox" {if $lang.itemInfo.is_active}checked="checked"{/if} name="is_active" id="is_active{$lang.id}" value="1"/> <label for="is_active{$lang.id}">Czy włączyć edytor WYSIWYG?</label>               
						</li>
					</ul>			
					{include	file="common/submit_buttons.tpl"}
				</form>
			</div>
			{/foreach} 
		</div>
			
{include file="footer.tpl"}