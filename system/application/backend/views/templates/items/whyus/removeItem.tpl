{include file="header.tpl"}      
		<h1>{$header}: <em>{$itemName}</em></h1>
      <div class="tab-content">         
         <form action="{$link}/removeItem/{$itemId}" method="post" class="form-b">
            {if $errors}
            <h3 class="warning">{$errorHeader}</h3>
            <ul class="warning">
            {foreach from=$errors item=error}
               <li>{$error}</li>
            {/foreach}
            </ul>
            {/if}         
            		  
				<ul class="form-items1">
               <li>
                  <label for="deleteAll"><input type="radio" name="deleteOptions" value="1" id="deleteAll"/> Usuń wszystkie obrazki i video dla tego newsa: <strong>{$itemName}</strong>.</label>               
               </li>	
					<li>
                  <label for="moveAll"><input type="radio" name="deleteOptions" value="2" id="moveAll" checked="checked"/> Przenieś obrazki i video do innego newsa</label>
               </li>					
            </ul>
				<ul class="form-items">
					<li class="diff" id="select-container">					
						<label for="parentid">Wybierz news:</label>
						<select name="parentid" id="parentid">						
							{if $itemsTree}
								{*{foreach from=$itemsTree item=item key=key}
								{if $item->id neq $itemId}
								<option value="{$item->id}" class="level-0">								
									{$item->title}
								</option>
								{/if}
								{if $item->children}
									{include file="common/items_tree_recursion.tpl" element=$item->children level=1 currentItem=-1 forbiddenItem=$itemId}
								{/if}
								{/foreach}*}
								{include file="common/menu_tree.tpl" scope=parent}
								{call menu data=$itemsTree currentItem=$backData.categoryid forbiddenItem=$itemId}
							{/if}							
						</select>					
					</li>
            </ul>		
				<p class="btn">				
					<button type="submit">
						Usuń i potwierdź powyższe ustawienia &gt;
					</button>
				</p>		
         </form>         
      </div>         
{include file="footer.tpl"}