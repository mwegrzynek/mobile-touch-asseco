{include file="header.tpl"}
		<h1>
			{if !$itemsList|count}
			No page found
			{else}
			All pages ({$allItemsCount})
			{/if}
		</h1>

		{if !empty($subheader)}
		<h2>{$subheader}</h2>
		{/if}

		<div class="medium">
			<div id="filterBox">
				<form action="{$link}/filter" method="post">
					{include file="common/menu_tree.tpl" scope=parent}
					<p class="field-a">
						<input type="text" id="searchFilterTxt" name="searchFilterTxt" value="{$filter.searchFilterTxt}" title="Search in title and content"/>
						<select name="categoryFilter">
							<option value="-1">--All categories--</option>
							{call menu data=$categoryTree currentItem=$filter.categoryFilter forbiddenItem=$forbiddenItem}
						</select>

						<select name="parentFilter">
							<option value="-1">--All pages--</option>
							{call menu data=$itemsTree currentItem=$filter.parentFilter forbiddenItem=$forbiddenItem}
						</select>
					</p>
					<p class="btn section-a">
						<button type="submit">
							Search
						</button>
					</p>
				</form>
			</div>

		{if $itemsList}
			{include file='pagination.tpl'}
			{include file='perPage.tpl'}
			<table class="item-table">
				<thead>
					<tr>
						<th class="toleft">ID</th>
						<th class="toleft">Title</th>
						<th class="toleft">Content</th>
						<th class="toleft">Is subpage in</th>
						<th class="toleft">Category</th>
						<th class="toleft">Pictures count</th>
						<th class="toleft">Movies count</th>
						<th colspan="4">Options</th>
					</tr>
				</thead>
				<tbody>
				{foreach from=$itemsList item="item"}
					<tr{cycle values=' class="diff",'}>
						<td>{$item->id}</td>
						<td class="toleft"><a href="{$link}/edit/{$item->id}">{$item->title}</a></td>
						<td class="toleft">{$item->content|strip_tags|truncate:120:"&hellip;":false}</td>
							<td class="toleft">
							{if $item->parents}
								{foreach from=$item->parents item="parent" key="key" name="loop1"}
									<a href="{$link}/edit/{$parent[0]->item_id}">{$parent[0]->title}</a> {if !$smarty.foreach.loop1.last}-> {/if}
								{/foreach}
							{else}
							No parent pages
							{/if}
						</td>
						<td class="toleft">
							{if $item->categories}
								{foreach $item->categories as $categoryPath}
									{foreach $categoryPath as $category}
										<a href="{$categoryLink}/edit/{$category->category_id}">{$category->name}</a> {if !$category@last}-> {/if}
									{/foreach}
								{/foreach}
							{else}
							No category
							{/if}
						</td>
						<td class="toleft">{$item->pictures_num}</td>
						<td class="toleft">{$item->videos_num}</td>
						<td class="toleft"><a href="itemspictures/{$mainName}/chooseitem/{$item->id}">Manage pictures</a></td>
						<td class="toleft"><a href="itemsvideos/{$mainName}/chooseitem/{$item->id}">Manage movies</a></td>
						<td><a href="{$link}/edit/{$item->id}">Edit</a></td>
						<td><a rel="dialog-pages" href="{$link}/removeItem/{$item->id}{if $offset}/{$offset}{/if}" class="del-opt" title="'{$item->title}'">Delete</a></td>
					</tr>
				{/foreach}
				</tbody>
			</table>
			{include file='pagination.tpl'}
		{/if}
		</div>
		<div class="dialog-box" id="dialog-pages" title="Delete page?">
			<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
				<span style="display:block; padding-left: 25px;">Page <em></em> will be delteted.<br/> Are you sure?</span>
			</p>
		</div>
{include file="footer.tpl"}
