{include file="header.tpl"}
		<h1>{$header}</h1>
		{if $subheader}<h2>{$subheader}</h2>{/if}

		<div class="tab-content">         
			<form action="{$link}/addnew" method="post" class="form-a" enctype="multipart/form-data">
				{if $errors}
				<h3 class="warning">{$errorHeader}</h3>
				<ul class="warning">
				{foreach from=$errors item=error}
					<li>{$error}</li>
				{/foreach}
				</ul>
				{/if}
				{include file="common/menu_tree.tpl" scope=parent}
				<ul class="form-items"> 	
					{*<li class="diff">                     
						<label for="categoryid">Kategoria:</label> 
						<select name="categoryid" id="categoryid">							
							<option value="-1">-- Bez kategorii --</option>
							{call menu data=$categoryTree currentItem=$backData.categoryid forbiddenItem=$forbiddenItem}
						</select>	               
					</li>	*}			
					<li>
						<label for="title">Name <em>*</em>:</label> <input type="text" name="title" id="title" value="{$backData.title}"/>   
						<input type="hidden" name="parentid" value="0"/>
						<input type="hidden" name="position" value="1"/>
						{*<input type="hidden" name="lead" value=""/>*}
						
						<input type="hidden" name="categoryid" value="0"/>
					</li>
					<li>
						<label for="text_data_2">Last Name:</label> <input type="text" name="text_data_2" id="text_data_2" value="{$backData.text_data_2}"/>   						
					</li>
					<li class="diff">
						<input type="hidden" name="MAX_FILE_SIZE" value="524288"/>
						<label for="thefile">Image <em>*</em><span>(.jpg|.png)</span></label>
						<input type="file" id="thefile" name="thefile" size="47"/>
					</li>
					<li>
						<span><strong>Image:</strong> size exactly 139x124px, max 512KB, jpg or png</span>
					</li>
					<li>
						<label for="text_data_1{$lang.id}">Company position:</label>
						<textarea rows="3" cols="90" id="text_data_1{$lang.id}" name="text_data_1">{$lang.itemInfo.text_data_1}</textarea>                  
					</li>
					<li class="diff offset">                     
						<label for="slug">Slug:</label> <input type="text" name="slug" id="slug" value="{$backData.slug}"/> {include file="common/slug_info.tpl"}
					</li>	
					
					<li class="diff">
						<label for="lead">Testimonial:</label>
						<textarea rows="3" cols="90" id="lead" name="lead">{$backData.lead}</textarea>                  
					</li>        
					<li>
						<label for="content-a">Description:</label>
						<textarea rows="8" cols="90" id="content-a" name="content">{$backData.content}</textarea>                  
					</li>
					<li class="diff">                     
						<label for="position">Display position:</label> <input type="text" name="position" id="position" value="{$backData.position|default:1}" class="short-input"/> 
					</li>    
				</ul>
				
				<ul class="form-items1">
					<li>
						<input type="checkbox" {if $backData.is_active}checked="checked"{/if} name="is_active" id="is_active" value="1"/> <label for="is_active">Is active?</label>               
					</li>
				</ul>							
				
				{include file="common/add_buttons.tpl"}
			</form>         
		</div>         
{include file="footer.tpl"}