{include file="header.tpl"}
		<h1>
			{if !$itemsList|@count}
			Nie znaleziono powiadomień
			{else}
			Wszystkie powiadomienia ({$allItemsCount})
			{/if}
		</h1>
		{if $subheader}
		<h2>{$subheader}</h2>
		{/if}
		<div class="medium">
			<div id="filterBox">
				<form action="{$link}/filter/{$item->id}" method="post">
					<p class="field-a">
						<input type="text" id="searchFilterTxt" name="searchFilterTxt" value="{$filter.searchFilterTxt}" title="Szukaj w tytule i opisie"/>
					</p>
					<p class="btn section-a">
						<button type="submit">
							Szukaj
						</button>
					</p>
				</form>
			</div>

		{if $itemsList}
			{include file='pagination.tpl'}
			{include file='perPage.tpl'}
			<form action="{$link}/removeSelected" method="post" id="item-form">
				<table class="item-table">
					<thead>
						<tr>
							<th class="toleft">&nbsp;</th>
							<th class="toleft">ID</th>
							<th class="toleft">Nazwa</th>
							<th class="toleft">Treść</th>
							<th colspan="1">Opcje</th>
						</tr>
					</thead>
					<tbody>
					{foreach from=$itemsList item=item}
						<tr{cycle values=' class="diff",'}>
							<td><input type="checkbox" name="id[]" value="{$item->id}" id="id-{$item->id}"/></td>
							<td>{$item->id}</td>
							<td class="toleft"><a href="{$link}/edit/{$item->id}">{$item->title}</a></td>
							<td class="toleft">{$item->content|strip_tags|truncate:120:"&hellip;":false}</td>
							<td><a href="{$link}/edit/{$item->id}">Edit</a></td>
							<td><a href="{$link}/removeItem/{$item->id}{if $offset}/{$offset}{/if}" class="del-opt" rel="dialog-sitetext" title="'{$item->title}'">Delete</a></td>
						</tr>
					{/foreach}
					</tbody>
				</table>
				<p class="btn">
					<button type="submit" class="del-opt" rel="dialog-sitetext">Remove selected</button>
					<a href="#item-form" id="mark-link">mark / unmark all</a>
				</p>
			</form>
			{include file='pagination.tpl'}
		{/if}
		</div>
		<div class="dialog-box" id="dialog-sitetext" title="Delete text?">
			<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
				<span style="display:block; padding-left: 25px;">Text <em></em> will be deleted.<br/> Are you sure?</span>
			</p>
		</div>
{include file="footer.tpl"}
