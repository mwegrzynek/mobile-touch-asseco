{include file="header.tpl"}
		<h1>{$header}</h1>
		{if $subheader}<h2>{$subheader}</h2>{/if}

		<div class="tab-content">         
			<form action="{$link}/addnew" method="post" class="form-a" enctype="multipart/form-data">
				{if $errors}
				<h3 class="warning">{$errorHeader}</h3>
				<ul class="warning">
				{foreach from=$errors item=error}
					<li>{$error}</li>
				{/foreach}
				</ul>
				{/if}
			
				<ul class="form-items"> 	
					{*<li class="diff">                     
						<label for="categoryid">Kategorie:</label> 
						<select name="categoryid" id="categoryid">							
							<option value="-1">-- Ohne Kategorie --</option>
							{if $itemsTree}
								{foreach from=$itemsTree item=item key=key}                        
								{if $item->id eq $backData.categoryid}
								<option value="{$item->id}" selected="selected" class="level-0">
								{else}
								<option value="{$item->id}" class="level-0">
								{/if}
									{$item->name}
								</option>
								{if $item->children}
									{include file="common/category_tree_recursion.tpl" element=$item->children level=1 currentItem=$backData.categoryid}
								{/if}
								{/foreach}
							{/if}
						</select>	               
					</li>		*}		
					<li>
						<label for="title">Nazwa <em>*</em>:</label> <input type="text" name="title" id="title" value="{$backData.title}"/>   
						<input type="hidden" name="parentid" value="0"/>
						<input type="hidden" name="lead" value=""/>
						<input type="hidden" name="position" value="0"/>
						
						{*<input type="hidden" name="categoryid" value="0"/> *}            
					</li>
					<li class="diff offset">                     
						<label for="slug">Slug:</label> <input type="text" name="slug" id="slug" value="{$backData.slug}"/> {include file="common/slug_info"}
					</li>
					
					{*<li>
						<input type="hidden" name="MAX_FILE_SIZE" value="524288"/>
						<label for="thefile">Obrazek <span>(.jpg|.png)</span></label>
						<input type="file" id="thefile" name="thefile" size="47"/>
					</li>*}
					
					{*<li>
						<label for="lead">Lead:</label>
						<textarea rows="8" cols="90" id="lead" name="lead">{$backData.lead}</textarea>                  
					</li> *}        
					<li class="diff">
						<label for="content-a">Treść:</label>
						<textarea rows="6" cols="90" id="content-a" name="content">{$backData.content}</textarea>                  
					</li>
					{*<li class="diff">                     
						<label for="position">Pozycja:</label> <input type="text" name="position" id="position" value="{$backData.position|default:1}" class="short-input"/> 
					</li>	 *}              
				</ul>
				
				{*<ul class="form-items1">
					<li>
						<input type="checkbox" {if $backData.is_active}checked="checked"{/if} name="is_active" id="is_active" value="1"/> <label for="is_active">Czy pokazać na stronie?</label>               
					</li>
				</ul>
				
				<h3 class="group-section">Meta Tagi</h3>
				<ul class="form-items">
					<li>                    
						<label for="metaTitle">Title:</label> <input type="text" name="metaTitle" id="metaTitle" value="{$backData.metaTitle}"/>                     
					</li>
					<li class="diff">
						<label class="type1" for="metaKeywords">Keywords:</label> <textarea rows="3" cols="80" id="metaKeywords" name="metaKeywords">{$backData.metaKeywords}</textarea>            
					</li>
					<li>
						<label class="type1" for="metaDescription">Description:</label> <textarea rows="3" cols="80" id="metaDescription" name="metaDescription">{$backData.metaDescription}</textarea>            
					</li>                               
				</ul> *}
				{include file="common/add_buttons.tpl"}
			</form>         
		</div>         
{include file="footer.tpl"}