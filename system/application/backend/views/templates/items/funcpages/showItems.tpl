{include file="header.tpl"}
		<h1>
			{if !$itemsList|@count}
			No address found
			{else}
			All main sections addresses ({$allItemsCount})
			{/if}
		</h1>
		{if $subheader}
		<h2>{$subheader}</h2>
		{/if}
		<div class="medium">
			<div id="filterBox">
				<form action="{$link}/filter/{$item->id}" method="post">
					<p class="field-a">
						<input type="text" id="searchFilterTxt" name="searchFilterTxt" value="{$filter.searchFilterTxt}" title="Search"/>
						{*<select name="categoryFilter">
							<option value="-1">--Wszystkie kategorie--</option>
							{if $itemsTree}
								{foreach from=$itemsTree item=item key=key}
								{if $item->id eq $filter.categoryFilter}
								<option value="{$item->id}" selected="selected" class="level-0">
								{else}
								<option value="{$item->id}" class="level-0">
								{/if}
									{$item->name}
								</option>
								{if $item->children}
									{include file="common/category_tree_recursion.tpl" element=$item->children level=1 currentItem=$filter.categoryFilter}
								{/if}
								{/foreach}
							{/if}
						</select>*}
					</p>
					<p class="btn section-a">
						<button type="submit">
							Search
						</button>
					</p>
				</form>
			</div>

		{if $itemsList}
			{include file='pagination.tpl'}
			{include file='perPage.tpl'}
			<table class="item-table">
				<thead>
					<tr>
						<table class="item-table">
						<th class="toleft">Name</th>
						<th class="toleft">Address</th>
						{*<th class="toleft">Treść</th>
						<th class="toleft">Ilość obrazków</th>
						<th class="toleft">Ilość filmów</th>*}
						<th colspan="1">Options</th>
					</tr>
				</thead>
				<tbody>
				{foreach from=$itemsList item=item}
					<tr{cycle values=' class="diff",'}>
						<td>{$item->id}</td>
						<td class="toleft"><a href="{$link}/edit/{$item->id}">{$item->title}</a></td>
						<td class="toleft">{$item->slug}.html</td>
						{*<td class="toleft">
							{if $item->parents}
								{foreach from=$item->parents item=parent key=key name=loop1}
									{$parent->name} {if !$smarty.foreach.loop1.last}-> {/if}
								{/foreach}
							{/if}
						</td>
						<td class="toleft">{$item->pictures_num}</td>
						<td class="toleft">{$item->videos_num}</td>
						<td class="toleft"><a href="itemspictures/{$mainName}/chooseitem/{$item->id}">Zarządzaj obrazkami</a></td>
						<td class="toleft"><a href="itemsvideos/{$mainName}/chooseitem/{$item->id}">Zarządzaj video</a></td>*}
						<td><a href="{$link}/edit/{$item->id}">Edit</a></td>
						{*<td><a href="{$link}/removeItem/{$item->id}{if $offset}/{$offset}{/if}" class="del-opt" title="'{$item->title}'">Delete</a></td>*}
					</tr>
				{/foreach}
				</tbody>
			</table>
			{include file='pagination.tpl'}
		{/if}
		</div>
		<div id="dialog" title="Delete address?">
			<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
				<span style="display:block; padding-left: 25px;">Address <em></em> will be deleted.<br/> Are you sure?</span>
			</p>
		</div>
{include file="footer.tpl"}
