{include file="header.tpl"}
		<h1>
			{if !$itemsList|@count}
			No "Why Mobile Touch" items found
			{else}
			All "Why Mobile Touch" items ({$allItemsCount})
			{/if}
		</h1>
		{if $subheader}
		<h2>{$subheader}</h2>
		{/if}
		<div class="medium">
			<div id="filterBox">
				<form action="{$link}/filter/{$item->id}" method="post">
					<p class="field-a">
						<input type="text" id="searchFilterTxt" name="searchFilterTxt" value="{$filter.searchFilterTxt}" title="Search in title and description"/>
						{*<select name="categoryFilter">
							<option value="-1">--Wszystkie kategorie--</option>
							{if $itemsTree}
								{foreach from=$itemsTree item=item key=key}
								{if $item->id eq $filter.categoryFilter}
								<option value="{$item->id}" selected="selected" class="level-0">
								{else}
								<option value="{$item->id}" class="level-0">
								{/if}
									{$item->name}
								</option>
								{if $item->children}
									{include file="common/category_tree_recursion.tpl" element=$item->children level=1 currentItem=$filter.categoryFilter}
								{/if}
								{/foreach}
							{/if}
						</select>*}
					</p>
					<p class="btn section-a">
						<button type="submit">
							Search
						</button>
					</p>
				</form>
			</div>

		{if $itemsList}
			{include file='pagination.tpl'}
			{include file='perPage.tpl'}
			<table class="item-table">
				<thead>
					<tr>
						<th class="toleft">ID</th>
						<th class="toleft">Title</th>
						<th class="toleft">Description</th>
						<th colspan="2">Options</th>
					</tr>
				</thead>
				<tbody>
				{foreach from=$itemsList item=item}
					<tr{cycle values=' class="diff",'}>
						<td>{$item->id}</td>
						<td class="toleft"><a href="{$link}/edit/{$item->id}">{$item->title} {$item->text_data_2}</a></td>
						<td class="toleft">{$item->content|strip_tags|truncate:120:"&hellip;":false}</td>
						{*<td class="toleft">
							{if $item->parents}
								{foreach from=$item->parents item=parent key=key name=loop1}
									<a href="{$categoryLink}/edit/{$parent->id}">{$parent->name}</a> {if !$smarty.foreach.loop1.last}-> {/if}
								{/foreach}
							{/if}
						</td>*}
						<td><a href="{$link}/edit/{$item->id}">Edit</a></td>
						<td><a rel="dialog-teamleaders" href="{$link}/removeItem/{$item->id}{if $offset}/{$offset}{/if}" class="del-opt" title="'{$item->title}'">Delete</a></td>
					</tr>
				{/foreach}
				</tbody>
			</table>
			{include file='pagination.tpl'}
			{include file="common/importexport.tpl"}
		{/if}
		</div>
		<div class="dialog-box" id="dialog-teamleaders" title="Delete item?">
			<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
				<span style="display:block; padding-left: 25px;">Item <em></em> will be deleted.<br/> Are you sure?</span>
			</p>
		</div>
{include file="footer.tpl"}
