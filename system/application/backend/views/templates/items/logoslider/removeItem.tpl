{include file="header.tpl"}      
		<h1>{$header}: <em>{$itemName}</em></h1>
      <div class="tab-content">         
         <form action="{$link}/removeItem/{$itemId}" method="post" class="form-b">
            {if $errors}
            <h3 class="warning">{$errorHeader}</h3>
            <ul class="warning">
            {foreach from=$errors item=error}
               <li>{$error}</li>
            {/foreach}
            </ul>
            {/if}         
            		  
				<ul class="form-items1">
               <li>
                  <label for="deleteAll"><input type="radio" name="deleteOptions" value="1" id="deleteAll"/> Delete all videos item: <strong>{$itemName}</strong></label>               
               </li>	
					<li>
                  <label for="moveAll"><input type="radio" name="deleteOptions" value="2" id="moveAll" checked="checked"/> Move videos to other item:</label>               
               </li>					
            </ul>
				<ul class="form-items">                  
					<li class="diff" id="select-container">					
						<label for="parentid">Choose page:</label>
						<select name="parentid" id="parentid">						
							{if $itemsTree}								
								{include file="common/menu_tree.tpl" scope=parent}
								{call menu data=$itemsTree currentItem=$backData.categoryid forbiddenItem=$itemId}
							{/if}
						</select>					
					</li>
            </ul>		
				<p class="btn">				
					<button type="submit">
						Delete item and confirm options above &gt;
					</button>
				</p>		
         </form>         
      </div>         
{include file="footer.tpl"}