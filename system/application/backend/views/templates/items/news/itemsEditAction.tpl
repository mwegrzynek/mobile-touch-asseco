{include file="header.tpl"}
		<h1>{$header}: <em>{$itemTitle}</em></h1>
		{if $subheader}<h2>{$subheader}</h2>{/if}             
	  
		{if $errors}
		<h3 class="warning">{$errorHeader}</h3>
		<ul class="warning">
		{foreach from=$errors item=error}
			<li>{$error}</li>
		{/foreach}
		</ul>
		{/if}		
		<form action="{$link}/edit/{$itemid}" method="post" class="form-a fa-a" enctype="multipart/form-data">			
			{if !$noImages}
				{assign var=path value=$picturePath|cat:'/'|cat:$imageBackData.main_image}
				{if $path|is_file}
					{assign var=isFile value=true}
					<div id="pictureContainer">
						<a href="{$path}" class="fancy"><img src="{$path|thumb_name}" alt="" width="89"/></a>
						<p class="imagesubtitle">
							<a href="{$link}/removeImage/{$itemid}">Usuń obrazek</a>
						</p>
					</div>
				{/if}
			{/if}
			<div{if $isFile} class="withImage2"{/if} id="independentSection">			
				{include file="common/menu_tree.tpl" scope=parent}
				<ul class="form-items">				
					{*<li class="diff">
						<label for="categoryid">Kategoria <em>*</em>:</label> 
						<select name="categoryid" id="categoryid">
							<option value="-1">-- Bez kategorii --</option>
							{call menu data=$categoryTree currentItem=$backData.categoryid forbiddenItem=$forbiddenItem}
						</select>
					</li>*}
					<li>
						<input type="hidden" name="MAX_FILE_SIZE" value="524288"/>
						<label for="thefile">Images <span>(.jpg|.png)</span></label>
						<input type="file" id="thefile" name="thefile" size="47"/>						
					</li>	
					<li class="diff">
						<label for="add_date">Add date:</label>
						<input type="date" id="add_date" name="add_date" value="{$backData.add_date|date_format:"%Y-%m-%d"}" />						
					</li>			
				</ul>			
				<p class="btn">				
					<button type="submit">
						Change settings &gt;
					</button>
				</p>
			</div>
		</form>	
		
		{*<h2>Language dependent data:</h2>*}
		{if $backData.languages|@count > 1}
		<div class="tabs-container">            
			<ul class="tabs">            
				{foreach from=$backData.languages item=lang}
				<li><a href="#{$lang.lang_code}"><span>{$lang.name}</span></a></li>
				{/foreach}                
			</ul>
		</div>      
		{/if}
		<div class="tab-content">                  
			{foreach from=$backData.languages item=lang name=langLoop key=key}
			<div id="{$lang.lang_code}" class="ui-tabs-panel">
				<form action="{$link}/edit/{$itemid}#{$lang.lang_code}" method="post" class="form-a">      
					{if $subErrors[$lang.id]}
					<h3 class="warning">{$errorHeader[$lang.id]}</h3>
					<ul class="warning">
					{foreach from=$subErrors[$lang.id] item=error}
						<li>{$error}</li>
					{/foreach}
					</ul>
					{/if}               
					
					<ul class="form-items">                
						<li>
							<label for="title{$lang.id}">Title <em>*</em>:</label> <input type="text" name="title" id="title{$lang.id}" value="{$lang.itemInfo.title}"/>
						</li>
						<li class="diff">                     
							<label for="slug{$lang.id}">Slug:</label> <input type="text" name="slug" id="slug{$lang.id}" value="{$lang.itemInfo.slug}"/> {include file="common/slug_info.tpl"}
						</li>	
						<li>
							<label for="lead{$lang.id}">Lead:</label>
							<textarea rows="8" cols="90" id="lead{$lang.id}" name="lead">{$lang.itemInfo.lead}</textarea>                  
						</li>
						<li class="diff">
							<label for="content{$lang.id}">Content:</label>
							<textarea rows="16" cols="90" id="content{$lang.id}" name="content" class="mceEditor">{$lang.itemInfo.content}</textarea>                     
						</li>
						<li class="diff offset">                     
							<label for="position{$lang.id}">Pozycja:</label> <input type="text" name="position" id="position{$lang.id}" value="{$lang.itemInfo.position}" class="short-input"/> 
						</li>						
					</ul>	
					<ul class="form-items1">
						<li>
							<input type="checkbox" {if $lang.itemInfo.is_active}checked="checked"{/if} name="is_active" id="is_active{$lang.id}" value="1"/> <label for="is_active{$lang.id}">Is active?</label>               
						</li>
					</ul>				
									
					{include	file="common/regular_meta.tpl"}
					{include	file="common/submit_buttons.tpl"}
				</form>
			</div>
			{/foreach} 
		</div>
			
{include file="footer.tpl"}