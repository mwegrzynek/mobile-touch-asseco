{include file="header.tpl"}
		<h1>{$header}</h1>
		{if $subheader}<h2>{$subheader}</h2>{/if}

		<div class="tab-content">         
			<form action="{$link}/addnew" method="post" class="form-a" enctype="multipart/form-data">
				{if $errors}
				<h3 class="warning">{$errorHeader}</h3>
				<ul class="warning">
				{foreach from=$errors item=error}
					<li>{$error}</li>
				{/foreach}
				</ul>
				{/if}
				
				{include file="common/menu_tree.tpl" scope=parent}				
				<ul class="form-items"> 					
					{*<li>
						<input type="hidden" name="MAX_FILE_SIZE" value="524288"/>
						<label for="thefile">Image <span>(.jpg|.png)</span></label>
						<input type="file" id="thefile" name="thefile" size="47"/>
					</li>	*}								
					<li class="diff">
						<label for="title">Title <em>*</em>:</label> <input type="text" name="title" id="title" value="{$backData.title}"/>   
						{*<input type="hidden" name="parentid" value="0"/>*}          						
						{*<input type="hidden" name="categoryid" value="0"/>*}           
					</li>
					<li class="offset">                     
						<label for="slug">Slug:</label> <input type="text" name="slug" id="slug" value="{$backData.slug}"/> {include file="common/slug_info.tpl"}
					</li>
					{*<li class="diff">
						<label for="lead">Lead:</label>
						<textarea rows="8" cols="90" id="lead" name="lead">{$backData.lead}</textarea>                  
					</li>*}       
					<li>
						<label for="text_data_1">Icon link:</label>
						<input type="text" name="text_data_1" id="text_data_1" value="{$backData.text_data_1}"/>
					</li>
					<li class="diff">
						<label for="content-a">Content:</label>
						<textarea rows="16" cols="90" id="content-a" name="content" class="mceEditor">{$backData.content}</textarea>                  
					</li>
					<li>
						<label for="file_data_1">Visual file:</label> <input type="text" readonly="readonly" class="filemanager" name="file_data_1" id="file_data_1" value="{$backData.file_data_1}"/> <img src="images/picture.png" width="16" height="16" class="filemanager-ico" alt="Picture"/>
					</li>
					<li class="diff">                     
						<label for="position">Position:</label> <input type="text" name="position" id="position" value="{$backData.position|default:1}" class="short-input"/> 
					</li>	               
				</ul>
				
				<ul class="form-items1">
					<li>
						<input type="checkbox" {if $backData.is_active}checked="checked"{/if} name="is_active" id="is_active" value="1"/> <label for="is_active">Is active?</label>               
					</li>
				</ul>
				
				{include	file="common/add_regular_meta.tpl"}
				{include file="common/add_buttons.tpl"}
			</form>         
		</div>         
{include file="footer.tpl"}