{include file="header.tpl"}
		<h1>
			{if !$itemsList|count}
			No concepts pages found
			{else}
			All concepts pages ({$allItemsCount})
			{/if}
		</h1>

		{if !empty($subheader)}
		<h2>{$subheader}</h2>
		{/if}

		<div class="medium">
			<div id="filterBox">
				<form action="{$link}/filter" method="post">
					{include file="common/menu_tree.tpl" scope=parent}
					<p class="field-a">
						<input type="text" id="searchFilterTxt" name="searchFilterTxt" value="{$filter.searchFilterTxt}" title="Search in title and content"/>
						{*<select name="categoryFilter">
							<option value="-1">--All categories--</option>
							{call menu data=$categoryTree currentItem=$filter.categoryFilter forbiddenItem=$forbiddenItem}
						</select>*}

						{*<select name="parentFilter">
							<option value="-1">--All pages--</option>
							{call menu data=$itemsTree currentItem=$filter.parentFilter forbiddenItem=$forbiddenItem}
						</select>*}
					</p>
					<p class="btn section-a">
						<button type="submit">
							Search
						</button>
					</p>
				</form>
			</div>

		{if $itemsList}
			{include file='pagination.tpl'}
			{include file='perPage.tpl'}
			<table class="item-table">
				<thead>
					<tr>
						<th class="toleft">ID</th>
						<th class="toleft">Title</th>
						<th class="toleft">Content</th>
						<th colspan="2">Options</th>
					</tr>
				</thead>
				<tbody>
				{foreach from=$itemsList item="item"}
					<tr{cycle values=' class="diff",'}>
						<td>{$item->id}</td>
						<td class="toleft"><a href="{$link}/edit/{$item->id}">{$item->title}</a></td>
						<td class="toleft">{$item->content|strip_tags|truncate:120:"&hellip;":false}</td>
						<td><a href="{$link}/edit/{$item->id}">Edit</a></td>
						<td><a rel="dialog-pages" href="{$link}/removeItem/{$item->id}{if $offset}/{$offset}{/if}" class="del-opt" title="'{$item->title}'">Delete</a></td>
					</tr>
				{/foreach}
				</tbody>
			</table>
			{include file='pagination.tpl'}
			{include file="common/importexport.tpl"}
		{/if}
		</div>
		<div class="dialog-box" id="dialog-pages" title="Delete page?">
			<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
				<span style="display:block; padding-left: 25px;">Page <em></em> will be delteted.<br/> Are you sure?</span>
			</p>
		</div>
{include file="footer.tpl"}
