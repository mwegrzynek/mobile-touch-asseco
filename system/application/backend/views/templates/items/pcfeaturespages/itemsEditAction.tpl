{include file="header.tpl"}
		<h1>{$header}: <em>{$itemTitle}</em></h1>
		{if $subheader}<h2>{$subheader}</h2>{/if}

		{if $errors}
		<h3 class="warning">{$errorHeader}</h3>
		<ul class="warning">
		{foreach from=$errors item=error}
			<li>{$error}</li>
		{/foreach}
		</ul>
		{/if}

		<form action="{$link}/edit/{$itemid}" method="post" class="form-a fa-a" enctype="multipart/form-data">
			{if !$noImages}
				{assign var=path value=$picturePath|cat:'/'|cat:$imageBackData.main_image}
				{if $path|is_file}
					{assign var=isFile value=true}
					<div id="pictureContainer">
						<a href="{$path}" class="fancy"><img src="{$path}" alt="" width="175"/></a>
						<p class="imagesubtitle">
							<a href="{$link}/removeImage/{$itemid}">Remove image</a>
						</p>
					</div>
				{/if}
			{/if}
			<div{if $isFile} class="withImage"{/if} id="independentSection">			
				
				{include file="common/menu_tree.tpl" scope=parent}
				<ul class="form-items">					
					{if is_array($backData.category_id)}
						{foreach $backData.category_id as $category_id}
							{if $category_id neq -1}								
							{assign var="loopCats" value="loop"}							
							<li class="category_container">                     
								<label for="categoryid{$category_id@index}">Category:</label> 
								<select name="category_id[]" id="categoryid{$category_id@index}">							
									<option value="-1">-- No category --</option>
									{call menu data=$categoryTree currentItem=$category_id forbiddenItem=$forbiddenItem}
								</select>	               
							</li>	
							{/if}
						{/foreach}						
					{/if}
					{if !is_array($backData.category_id) or !$loopCats}
						<li class="category_container">                     
							<label for="categoryid">Category:</label> 
							<select name="category_id[]" id="categoryid">							
								<option value="-1">-- No category --</option>
								{call menu data=$categoryTree currentItem=$backData.category_id forbiddenItem=$forbiddenItem}
							</select>	               
						</li>	
					{/if}	
					
					<li class="diff">
						<label for="parentid">Parent page:</label>
						<select name="parent_id" id="parentid">
							<option value="-1">-- No parent page --</option>
							{call menu data=$itemsTree currentItem=$backData.parent_id forbiddenItem=$forbiddenItem}
						</select>
					</li>
					{if !$backData.parent_id}
					{*<li>
						<input type="hidden" name="MAX_FILE_SIZE" value="524288"/>
						<label for="thefile">Image <span>(.jpg|.png)</span></label>
						<input type="file" id="thefile" name="thefile" size="47"/>						
					</li>*}
					{/if}				
				</ul>
				<p class="btn">
					<button type="submit">
						Change &gt;
					</button>
				</p>
			</div>
		</form>

		{*<h2>Text data:</h2>*}
		{if $backData.languages|@count > 1}
		<div class="tabs-container">
			<ul class="tabs">
				{foreach from=$backData.languages item=lang}
				<li><a href="#{$lang.lang_code}"><span>{$lang.name}</span></a></li>
				{/foreach}
			</ul>
		{/if}
			<div class="tab-content">
				{foreach from=$backData.languages item=lang name=langLoop key=key}
				<div id="{$lang.lang_code}" class="ui-tabs-panel">
					<form action="{$link}/edit/{$itemid}#{$lang.lang_code}" method="post" class="form-a">
						{if $subErrors[$lang.id]}
						<h3 class="warning">{$errorHeader[$lang.id]}</h3>
						<ul class="warning">
						{foreach from=$subErrors[$lang.id] item=error}
							<li>{$error}</li>
						{/foreach}
						</ul>
						{/if}

						<ul class="form-items">
							<li>
								<label for="title{$lang.id}">Title <em>*</em>:</label> <input type="text" name="title" id="title{$lang.id}" value="{$lang.itemInfo.title}"/>
							</li>
							{if !$backData.parent_id}							 	
								<li class="diff">
									<label for="slug{$lang.id}">Slug:</label> <input type="text" name="slug" id="slug{$lang.id}" value="{$lang.itemInfo.slug}"/> {include file="common/slug_info.tpl"}
									{if $parents[$lang.id]}
										<p class="text-a">
											<span class="helper-c">full path:</span>
											<span class="helper-b">{foreach from=$parents[$lang.id] item=item key=key name=loop1}{$item[0]->slug}/{/foreach}{$lang.itemInfo.slug}.html</span>
										</p>
									{/if}
								</li>
								<li>
									<label for="lead{$lang.id}">Lead:</label>
									<textarea rows="8" cols="90" id="lead{$lang.id}" name="lead">{$lang.itemInfo.lead}</textarea>
								</li>
								<li class="diff">
									<label for="file_data_1{$lang.id}">Visual file:</label> <input type="text" readonly="readonly" class="filemanager" name="file_data_1" id="file_data_1{$lang.id}" value="{$lang.itemInfo.file_data_1}"/> <img src="images/picture.png" width="16" height="16" class="filemanager-ico" alt="Picture"/>
								</li>	
							{/if}
							{*<li class="diff">
								<label for="text_data_1{$lang.id}">Additional description:</label>
								<textarea rows="8" cols="90" id="text_data_1{$lang.id}" name="text_data_1">{$lang.itemInfo.text_data_1}</textarea>                  
							</li>*}     
							<li>
								<label for="content{$lang.id}">Content:</label>
								<textarea rows="16" cols="90" id="content{$lang.id}" name="content" class="mceEditor">{$lang.itemInfo.content}</textarea>
							</li>							
							<li class="diff">
								<label for="position{$lang.id}">Position:</label> <input type="text" name="position" id="position{$lang.id}" value="{$lang.itemInfo.position}" class="short-input"/>
							</li>							
						</ul>
						<ul class="form-items1">
							<li>
								<input type="checkbox" {if $lang.itemInfo.is_active}checked="checked"{/if} name="is_active" id="is_active{$lang.id}" value="1"/> <label for="is_active{$lang.id}">Is active?</label>
							</li>
						</ul>

						{if !$backData.parent_id}							 	
						{include	file="common/regular_meta.tpl"}
						{/if}
						{include	file="common/submit_buttons.tpl"}
					</form>
				</div>
			{/foreach}
		</div>
	{if $backData.languages|@count > 1}</div>{/if}
{include file="footer.tpl"}