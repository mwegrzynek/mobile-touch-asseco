{include file="header.tpl"}
		<h1>{$header}</h1>
		{if $subheader}<h2>{$subheader}</h2>{/if}

		<div class="tab-content">         
			<form action="{$link}/addnew" method="post" class="form-a" enctype="multipart/form-data">
				{if $errors}
				<h3 class="warning">{$errorHeader}</h3>
				<ul class="warning">
				{foreach from=$errors item=error}
					<li>{$error}</li>
				{/foreach}
				</ul>
				{/if}
				{include file="common/menu_tree.tpl" scope=parent}
				<ul class="form-items">					
					<li>
						<label for="title">Name <em>*</em>:</label> <input type="text" name="title" id="title" value="{$backData.title}"/>						
					</li>					
					<li class="diff offset">                     
						<label for="slug">Slug:</label> <input type="text" name="slug" id="slug" value="{$backData.slug}"/> {include file="common/slug_info.tpl"}
					</li>					
				</ul>
				
				<ul class="form-items1">
					<li>
						<input type="checkbox" {if $backData.is_active}checked="checked"{/if} name="is_active" id="is_active" value="1"/> <label for="is_active">Is active?</label>               
					</li>
				</ul>				
				{include file="common/add_buttons.tpl"}
			</form>         
		</div>         
{include file="footer.tpl"}