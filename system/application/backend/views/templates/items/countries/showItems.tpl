{include file="header.tpl"}
		<h1>
			{if !$itemsList|@count}
			No countries found
			{else}
			All countries ({$allItemsCount})
			{/if}
		</h1>
		{if $subheader}
		<h2>{$subheader}</h2>
		{/if}
		<div class="medium">


		{if $itemsList}
			{include file='pagination.tpl'}
			{include file='perPage.tpl'}
			<table class="item-table">
				<thead>
					<tr>
						<th class="toleft">ID</th>
						<th class="toleft">Name</th>
						<th colspan="2">Options</th>
					</tr>
				</thead>
				<tbody>
				{foreach from=$itemsList item=item}
					<tr{cycle values=' class="diff",'}>
						<td>{$item->id}</td>
						<td class="toleft"><a href="{$link}/edit/{$item->id}">{$item->title}</a></td>
						<td><a href="{$link}/edit/{$item->id}">Edit</a></td>
						<td><a rel="dialog-countries" href="{$link}/removeItem/{$item->id}{if $offset}/{$offset}{/if}" class="del-opt" title="'{$item->title}'">Delete</a></td>
					</tr>
				{/foreach}
				</tbody>
			</table>
			{include file='pagination.tpl'}
		{/if}
		</div>
		<div class="dialog-box" id="dialog-countries" title="Delete country?">
			<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
				<span style="display:block; padding-left: 25px;">Country <em></em> will be deleted.<br/> Are you sure?</span>
			</p>
		</div>
{include file="footer.tpl"}
