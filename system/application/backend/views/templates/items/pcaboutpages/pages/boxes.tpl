<ul class="form-items">
	<li>
		<label for="title{$lang.id}">Title <em>*</em>:</label> <input type="text" name="title" id="title{$lang.id}" value="{$lang.itemInfo.title}"/>
	</li>
	<li class="diff offset">
		<label for="slug{$lang.id}">Slug:</label> <input type="text" name="slug" id="slug{$lang.id}" value="{$lang.itemInfo.slug}"/> {include file="common/slug_info.tpl"}	
	</li>
	<li class="diff">
		<label for="lead{$lang.id}">Lead:</label>
		<textarea rows="8" cols="90" id="lead{$lang.id}" name="lead" class="mceEditorSimple">{$lang.itemInfo.lead}</textarea>		
	</li>	
</ul>