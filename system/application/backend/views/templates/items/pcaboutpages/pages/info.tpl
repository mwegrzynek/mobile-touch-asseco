<ul class="form-items">
	<li class="diff">
		<label for="title{$lang.id}">Name <em>*</em>:</label> <input type="text" name="title" id="title{$lang.id}" value="{$lang.itemInfo.title}"/>
	</li>
	<li class="diff offset">
		<label for="slug{$lang.id}">Slug:</label> <input type="text" name="slug" id="slug{$lang.id}" value="{$lang.itemInfo.slug}"/> {include file="common/slug_info.tpl"}
	</li>
	<li>
		<label for="lead{$lang.id}">Header:</label>
	<input type="text" name="lead" id="lead{$lang.id}" value="{$lang.itemInfo.lead}"/>
	</li>
	<li class="diff">
		<label for="content{$lang.id}">Content:</label>
		<textarea rows="8" cols="90" id="content{$lang.id}" name="content" class="mceEditorSimple">{$lang.itemInfo.content}</textarea>
	</li>
</ul>