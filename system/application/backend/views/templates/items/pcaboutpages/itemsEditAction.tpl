{include file="header.tpl"}
		<h1>{$header}: <em>{$itemTitle}</em></h1>
		{if $subheader}<h2>{$subheader}</h2>{/if}

		{if $errors}
		<h3 class="warning">{$errorHeader}</h3>
		<ul class="warning">
		{foreach from=$errors item=error}
			<li>{$error}</li>
		{/foreach}
		</ul>
		{/if}

		{*<h2>Text data:</h2>*}
		{if $backData.languages|@count > 1}
		<div class="tabs-container">
			<ul class="tabs">
				{foreach from=$backData.languages item=lang}
				<li><a href="#{$lang.lang_code}"><span>{$lang.name}</span></a></li>
				{/foreach}
			</ul>
		{/if}
			<div class="tab-content">
				{foreach from=$backData.languages item=lang name=langLoop key=key}
				<div id="{$lang.lang_code}" class="ui-tabs-panel">
					<form action="{$link}/edit/{$itemid}#{$lang.lang_code}" method="post" class="form-a">
						{if $subErrors[$lang.id]}
						<h3 class="warning">{$errorHeader[$lang.id]}</h3>
						<ul class="warning">
						{foreach from=$subErrors[$lang.id] item=error}
							<li>{$error}</li>
						{/foreach}
						</ul>
						{/if}

						{*{elseif $itemid eq 955 or $itemid eq 952  or $itemid eq 953 or $itemid eq 954 or $itemid eq 954}*}

						{if $itemid eq 888 or $itemid eq 889 or $itemid eq 957 or $itemid eq 958}
							{include file="items/pcaboutpages/pages/boxes.tpl"}
						{else}
							{include file="items/pcaboutpages/pages/info.tpl"}
							{*{include file="items/pcaboutpages/pages/tabs.tpl"}*}
						{/if}

						{include	file="common/submit_buttons.tpl"}
					</form>
				</div>
			{/foreach}
		</div>
	{if $backData.languages|@count > 1}</div>{/if}
{include file="footer.tpl"}