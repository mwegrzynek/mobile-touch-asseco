{include file="header.tpl"}
		<h1>{$header}</h1>
		{if $subheader}<h2>{$subheader}</h2>{/if}

		<div class="tab-content">         
			<form action="{$link}/addnew" method="post" class="form-a" enctype="multipart/form-data">
				{if $errors}
				<h3 class="warning">{$errorHeader}</h3>
				<ul class="warning">
				{foreach from=$errors item=error}
					<li>{$error}</li>
				{/foreach}
				</ul>
				{/if}
				{include file="common/menu_tree.tpl" scope=parent}
				<ul class="form-items"> 	
					{*<li class="diff">                     
						<label for="categoryid">Kategoria:</label> 
						<select name="categoryid" id="categoryid">							
							<option value="-1">-- Bez kategorii --</option>
							{call menu data=$categoryTree currentItem=$backData.categoryid forbiddenItem=$forbiddenItem}
						</select>	               
					</li>	*}			
					<li>
						<label for="title">Name <em>*</em>:</label> <input type="text" name="title" id="title" value="{$backData.title}"/>   
						<input type="hidden" name="parentid" value="0"/>
						{*<input type="hidden" name="position" value="1"/>*}
						{*<input type="hidden" name="lead" value=""/>*}
						
						<input type="hidden" name="categoryid" value="0"/>
					</li>
					<li class="diff offset">                     
						<label for="slug">Slug:</label> <input type="text" name="slug" id="slug" value="{$backData.slug}"/> <span>If empty, generated automatically.</span>                  
					</li>
					
					<li class="diff">
						<label for="li_text_data_1">Lang code <em>*</em>:</label> <input type="text" name="li_text_data_1" id="li_text_data_1" value="{$backData.li_text_data_1}"/> <span>2 letters (Code ISO 639-1 see <a href="http://www.loc.gov/standards/iso639-2/php/code_list.php">ISO document</a>)</span>
					</li>
					
					<li>
						<input type="hidden" name="MAX_FILE_SIZE" value="524288"/>
						<label for="thefile">Flag Icon <span>(.jpg|.png)</span></label>
						<input type="file" id="thefile" name="thefile" size="47"/>
					</li>
					<li class="diff">
						<span><strong>Flag Icon:</strong> Size exactly 16x11px, max 51KB</span>
					</li>	
									
					<li class="offset">
						<label for="content-a">Content:</label>
						<textarea rows="16" cols="90" id="content-a" name="content">{$backData.content}</textarea>                  
					</li>				
					<li class="offset">                     
						<label for="position">Position:</label> <input type="text" name="position" id="position" value="{$backData.position|default:1}" class="short-input"/> 
					</li>
				</ul>
				
				<ul class="form-items1 offset">
					<li>
						<input type="checkbox" {if $backData.is_active}checked="checked"{/if} name="is_active" id="is_active" value="1"/> <label for="is_active">Is active?</label>               
					</li>
				</ul>
								
				<p class="btn">					
					<button type="submit">
						Add &gt;
					</button>
					
					{if $languages|@count > 1}
					<input type="hidden" name="next" value="{$languages[1]->lang_code}"/>
					<button class="btn-a" type="submit" name="submit-and-edit-next" value="1">
						Add and edit next language &gt;
					</button>
					
					<button class="btn-a" type="submit" name="submit-and-fill-langs" value="1">
						Add to this and all other languages &gt;
					</button>
					{/if}
				</p>
			</form>         
		</div>         
{include file="footer.tpl"}