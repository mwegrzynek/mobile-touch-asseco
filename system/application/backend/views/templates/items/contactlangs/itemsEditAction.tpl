{include file="header.tpl"}
		<h1>{$header}: <em>{$itemTitle}</em></h1>
		{if $subheader}<h2>{$subheader}</h2>{/if}             
	  
		{if $errors}
		<h3 class="warning">{$errorHeader}</h3>
		<ul class="warning">
		{foreach from=$errors item=error}
			<li>{$error}</li>
		{/foreach}
		</ul>
		{/if}		
		<form action="{$link}/edit/{$itemid}" method="post" class="form-a fa-a" enctype="multipart/form-data">			
			{if !$noImages}
				{assign var=path value=$picturePath|cat:'/'|cat:$imageBackData.main_image}
				{if $path|is_file}
					{assign var=isFile value=true}
					<div id="pictureContainer">
						{*<a href="{$path}" class="fancy">*}<img src="{$path}" alt=""/>{*</a>*}
						{*<p class="imagesubtitle">
							<a href="{$link}/removeImage/{$itemid}">Remove image</a>
						</p>*}
					</div>
				{/if}
			{/if}
			<div{if $isFile} class="withImage3"{/if} id="independentSection">			
				{include file="common/menu_tree.tpl" scope=parent}
				<ul class="form-items">				
					{*<li class="diff">
						<label for="categoryid">Kategoria <em>*</em>:</label> 
						<select name="categoryid" id="categoryid">
							<option value="-1">-- Bez kategorii --</option>
							{call menu data=$categoryTree currentItem=$backData.categoryid forbiddenItem=$forbiddenItem}
						</select>
					</li>*}
					<li class="diff">
						<input type="hidden" name="MAX_FILE_SIZE" value="524288"/>
						<label for="thefile">Flag Icon <span>(.jpg|.png)</span></label>
						<input type="file" id="thefile" name="thefile" size="47"/>
					</li>
					<li class="diff">
						<span><strong>Flag Icon:</strong> Size exactly 16x11px, max 51KB</span>
					</li>					
					<li>
						<label for="li_text_data_1">Lang code <em>*</em>:</label> <input type="text" name="li_text_data_1" id="li_text_data_1" value="{$backData.li_text_data_1}"/> <span>2 letters (Code ISO 639-1 see <a href="http://www.loc.gov/standards/iso639-2/php/code_list.php">ISO document</a>)</span>
					</li>					
				</ul>			
				<p class="btn">
					<input type="hidden" name="lang_independent" value="1"/>
					<button type="submit">
						Change settings &gt;
					</button>
				</p>
			</div>
		</form>	
		
		<h2>Language dependent data:</h2>
		{if $backData.languages|@count > 1}
		<div class="tabs-container">            
			<ul class="tabs">            
				{foreach from=$backData.languages item=lang}
				<li><a href="#{$lang.lang_code}"><span>{$lang.name}</span></a></li>
				{/foreach}                
			</ul>
		</div>      
		{/if}
		<div class="tab-content">                  
			{foreach from=$backData.languages item=lang name=langLoop key=key}
			<div id="{$lang.lang_code}" class="ui-tabs-panel">
				<form action="{$link}/edit/{$itemid}#{$lang.lang_code}" method="post" class="form-a">      
					{if $subErrors[$lang.id]}
					<h3 class="warning">{$errorHeader[$lang.id]}</h3>
					<ul class="warning">
					{foreach from=$subErrors[$lang.id] item=error}
						<li>{$error}</li>
					{/foreach}
					</ul>
					{/if}               
					
					<ul class="form-items">                
						<li>
							<label for="title{$lang.id}">Name <em>*</em>:</label> <input type="text" name="title" id="title{$lang.id}" value="{$lang.itemInfo.title}"/>
						</li>
						<li class="diff offset">                     
							<label for="slug{$lang.id}">Slug:</label> <input type="text" name="slug" id="slug{$lang.id}" value="{$lang.itemInfo.slug}"/> <span>If empty, generated automatically.</span>                     
						</li>	
						<li class="offset">
							<label for="lead{$lang.id}">Lead:</label>
							<textarea rows="8" cols="90" id="lead{$lang.id}" name="lead">{$lang.itemInfo.lead}</textarea>                  
						</li>
						<li class="diff offset">
							<label for="content{$lang.id}">Content:</label>
							<textarea rows="16" cols="90" id="content{$lang.id}" name="content">{$lang.itemInfo.content}</textarea>                     
						</li>
						<li class="diff offset">                     
							<label for="position{$lang.id}">Position:</label> <input type="text" name="position" id="position{$lang.id}" value="{$lang.itemInfo.position}" class="short-input"/> 
						</li>						
					</ul>	
					<ul class="form-items1 offset">
						<li>
							<input type="checkbox" {if $lang.itemInfo.is_active}checked="checked"{/if} name="is_active" id="is_active{$lang.id}" value="1"/> <label for="is_active{$lang.id}">Is active in this language?</label>               
						</li>
					</ul>				
									
					{*<h3 class="group-section">Meta Tags</h3>
					<ul class="form-items">
						<li>                    
							<label for="metaTitle{$lang.id}">Title:</label> <input type="text" name="metaTitle" id="metaTitle{$lang.id}" value="{$lang.metaInfo.title}"/>                     
						</li>
						<li class="diff">
							<label class="type1" for="metaKeywords{$lang.id}">Keywords:</label> <textarea rows="3" cols="80" id="metaKeywords{$lang.id}" name="metaKeywords">{$lang.metaInfo.keywords}</textarea>            
						</li>
						<li>
							<label class="type1" for="metaDescription{$lang.id}">Description:</label> <textarea rows="3" cols="80" id="metaDescription{$lang.id}" name="metaDescription">{$lang.metaInfo.description}</textarea>            
						</li>                               
					</ul> *}              
					<p class="btn">						
						<input type="hidden" name="langid" value="{$lang.id}"/>
						<input type="hidden" name="next" value="{$lang.next_lang_code}"/>

						<button type="submit">
							Submit &gt;
						</button>

						{if $backData.languages|@count > 1}
						<button class="btn-a" type="submit" name="submit-and-next" value="1">
							Submit and edit next language &gt;
						</button>
						{/if}
					</p>
				</form>
			</div>
			{/foreach} 
		</div>
			
{include file="footer.tpl"}