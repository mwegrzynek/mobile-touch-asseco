{include file="header.tpl"}
		<h1>{$header}</h1>
		{if $subheader}<h2>{$subheader}</h2>{/if}

		<div class="tab-content">         
			<form action="{$link}/addnew" method="post" class="form-a" enctype="multipart/form-data">
				{if $errors}
				<h3 class="warning">{$errorHeader}</h3>
				<ul class="warning">
				{foreach from=$errors item=error}
					<li>{$error}</li>
				{/foreach}
				</ul>
				{/if}
				
				<ul class="form-items">									
					<li class="diff">
						<label for="title">Title <em>*</em>:</label> <input type="text" name="title" id="title" value="{$backData.title}"/>   
						<input type="hidden" name="parentid" value="0"/>         						
						<input type="hidden" name="categoryid" value="0"/>          
					</li>
					<li class="offset">                     
						<label for="slug">Slug:</label> <input type="text" name="slug" id="slug" value="{$backData.slug}"/> {include file="common/slug_info.tpl"}
					</li>		
					<li>
						<label for="content-1">Content <em>*</em>:</label> 
						<textarea rows="8" cols="90" id="content-1" name="content" class="mceEditorSimple">{$backData.content}</textarea>		
					</li>	
					<li class="diff">
						<label for="text_data_1">Button label:</label> <input type="text" name="text_data_1" id="text_data_1" value="{$backData.text_data_1}"/>
					</li>
					<li>
						<label for="text_data_2">Button link:</label> <input type="text" name="text_data_2" id="text_data_2" value="{$backData.text_data_2}"/>
					</li>									
					<li class="diff">                     
						<label for="position">Position:</label> <input type="text" name="position" id="position" value="{$backData.position|default:1}" class="short-input"/> 
					</li>	               
				</ul>
				
				<ul class="form-items1">
					<li>
						<input type="checkbox" {if $backData.is_active}checked="checked"{/if} name="is_active" id="is_active" value="1"/> <label for="is_active">Is active?</label>               
					</li>
				</ul>				
				{include file="common/add_buttons.tpl"}
			</form>         
		</div>         
{include file="footer.tpl"}