{include file="header.tpl"}
		<h1>{$header}: <em>{$itemTitle}</em></h1>
		{if $subheader}<h2>{$subheader}</h2>{/if}

		{if $errors}
		<h3 class="warning">{$errorHeader}</h3>
		<ul class="warning">
		{foreach from=$errors item=error}
			<li>{$error}</li>
		{/foreach}
		</ul>
		{/if}

		{*<h2>Text data:</h2>*}
		{if $backData.languages|@count > 1}
		<div class="tabs-container">
			<ul class="tabs">
				{foreach from=$backData.languages item=lang}
				<li><a href="#{$lang.lang_code}"><span>{$lang.name}</span></a></li>
				{/foreach}
			</ul>
		{/if}
			<div class="tab-content">
				{foreach from=$backData.languages item=lang name=langLoop key=key}
				<div id="{$lang.lang_code}" class="ui-tabs-panel">
					<form action="{$link}/edit/{$itemid}#{$lang.lang_code}" method="post" class="form-a">
						{if $subErrors[$lang.id]}
						<h3 class="warning">{$errorHeader[$lang.id]}</h3>
						<ul class="warning">
						{foreach from=$subErrors[$lang.id] item=error}
							<li>{$error}</li>
						{/foreach}
						</ul>
						{/if}

						<ul class="form-items">
							<li class="diff">
								<label for="title{$lang.id}">Title <em>*</em>:</label> <input type="text" name="title" id="title{$lang.id}" value="{$lang.itemInfo.title}"/>
							</li>
							<li class="diff offset">
								<label for="slug{$lang.id}">Slug:</label> <input type="text" name="slug" id="slug{$lang.id}" value="{$lang.itemInfo.slug}"/> {include file="common/slug_info.tpl"}								
							</li>	
							<li>
								<label for="content{$lang.id}">Content <em>*</em>:</label> 
								<textarea rows="8" cols="90" id="content{$lang.id}" name="content" class="mceEditorSimple">{$lang.itemInfo.content}</textarea>		
							</li>	
							<li class="diff">
								<label for="text_data_1{$lang.id}">Button label:</label> <input type="text" name="text_data_1" id="text_data_1{$lang.id}" value="{$lang.itemInfo.text_data_1}"/>
							</li>
							<li>
								<label for="text_data_2{$lang.id}">Button link:</label> <input type="text" name="text_data_2" id="text_data_2{$lang.id}" value="{$lang.itemInfo.text_data_2}"/>
							</li>						
							<li class="diff">
								<label for="position{$lang.id}">Position:</label> <input type="text" name="position" id="position{$lang.id}" value="{$lang.itemInfo.position}" class="short-input"/>
							</li>
						</ul>
						<ul class="form-items1">
							<li>
								<input type="checkbox" {if $lang.itemInfo.is_active}checked="checked"{/if} name="is_active" id="is_active{$lang.id}" value="1"/> <label for="is_active{$lang.id}">Is active?</label>
							</li>
						</ul>					
						{include	file="common/submit_buttons.tpl"}
					</form>
				</div>
			{/foreach}
		</div>
	{if $backData.languages|@count > 1}</div>{/if}
{include file="footer.tpl"}