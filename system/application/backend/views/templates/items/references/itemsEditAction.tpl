{include file="header.tpl"}
		<h1>{$header}: <em>{$itemTitle}</em></h1>
		{if $subheader}<h2>{$subheader}</h2>{/if}             
	  
		{if $errors}
		<h3 class="warning">{$errorHeader}</h3>
		<ul class="warning">
		{foreach from=$errors item=error}
			<li>{$error}</li>
		{/foreach}
		</ul>
		{/if}		
		<form action="{$link}/edit/{$itemid}" method="post" class="form-a fa-a" enctype="multipart/form-data">
			<div id="independentSection">			
				{include file="common/menu_tree.tpl" scope=parent}
				<ul class="form-items">				
					{*<li class="diff">
						<label for="categoryid">Kategoria <em>*</em>:</label> 
						<select name="categoryid" id="categoryid">
							<option value="-1">-- Bez kategorii --</option>
							{call menu data=$categoryTree currentItem=$backData.categoryid forbiddenItem=$forbiddenItem}
						</select>
					</li>*}
					
					<li class="diff">
						<label for="parentid">Client:</label>
						<select name="parent_id" id="parentid">							
							{call menu data=$connectedItems[5] currentItem=$backData.parent_id forbiddenItem=$forbiddenItem}
						</select>
					</li>
					
					<li>
						<input type="hidden" name="MAX_FILE_SIZE" value="524288"/>
						<label for="thefile">Attachment <span>(.pdf|.doc)</span></label>
						<input type="file" id="thefile" name="thefile" size="47"/>						
					</li>	
					<li>
					{if !$noImages}
						{assign var=path value=$picturePath|cat:'/'|cat:$imageBackData.main_image}
						{if $path|is_file}
							{assign var=isFile value=true}								
							<p>See attachment: <a href="{$path}">{$imageBackData.main_image}</a></p>
							<a href="{$link}/removeImage/{$itemid}">Remove attachment</a>
						{/if}
					{/if}						
					</li>					
				</ul>			
				<p class="btn">				
					<button type="submit">
						Change attachment &gt;
					</button>
				</p>
			</div>
		</form>	
		
		{*<h2>Language dependent data:</h2>*}
		{if $backData.languages|@count > 1}
		<div class="tabs-container">            
			<ul class="tabs">            
				{foreach from=$backData.languages item=lang}
				<li><a href="#{$lang.lang_code}"><span>{$lang.name}</span></a></li>
				{/foreach}                
			</ul>
		</div>      
		{/if}
		<div class="tab-content">                  
			{foreach from=$backData.languages item=lang name=langLoop key=key}
			<div id="{$lang.lang_code}" class="ui-tabs-panel">
				<form action="{$link}/edit/{$itemid}#{$lang.lang_code}" method="post" class="form-a">      
					{if $subErrors[$lang.id]}
					<h3 class="warning">{$errorHeader[$lang.id]}</h3>
					<ul class="warning">
					{foreach from=$subErrors[$lang.id] item=error}
						<li>{$error}</li>
					{/foreach}
					</ul>
					{/if}               
					
					<ul class="form-items">                
						<li class="diff">
							<label for="title{$lang.id}">Reference title <em>*</em>:</label> <input type="text" name="title" id="title{$lang.id}" value="{$lang.itemInfo.title}"/>
						</li>						
						<li class="diff offset">                     
							<label for="slug{$lang.id}">Slug:</label> <input type="text" name="slug" id="slug{$lang.id}" value="{$lang.itemInfo.slug}"/> {include file="common/slug_info.tpl"}
						</li>	
						<li>
							<label for="lead{$lang.id}">Lead:</label>
							<textarea rows="8" cols="90" id="lead{$lang.id}" name="lead">{$lang.itemInfo.lead}</textarea>                  
						</li>
						<li class="diff">
							<label for="content{$lang.id}">Content:</label>
							<textarea rows="16" cols="90" id="content{$lang.id}" name="content" class="mceEditor">{$lang.itemInfo.content}</textarea>                     
						</li>
						<li>                     
							<label for="position{$lang.id}">Position:</label> <input type="text" name="position" id="position{$lang.id}" value="{$lang.itemInfo.position}" class="short-input"/> 
						</li>						
					</ul>	
					<ul class="form-items1">
						<li>
							<input type="checkbox" {if $lang.itemInfo.is_active}checked="checked"{/if} name="is_active" id="is_active{$lang.id}" value="1"/> <label for="is_active{$lang.id}">Is active?</label>               
						</li>
					</ul>					
					{include	file="common/submit_buttons.tpl"}
				</form>
			</div>
			{/foreach} 
		</div>
			
{include file="footer.tpl"}