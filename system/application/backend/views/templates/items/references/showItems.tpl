{include file="header.tpl"}
		<h1>
			{if !$itemsList|@count}
			No references found
			{else}
			All references ({$allItemsCount})
			{/if}
		</h1>
		{if $subheader}
		<h2>{$subheader}</h2>
		{/if}
		<div class="medium">
			<div id="filterBox">
				{include file="common/menu_tree.tpl" scope=parent}
				<form action="{$link}/filter/{$item->id}" method="post">
					<p class="field-a">
						<input type="text" id="searchFilterTxt" name="searchFilterTxt" value="{$filter.searchFilterTxt}" title="Search in title and description"/>
						<select name="parentFilter">
							<option value="-1">--All clients--</option>
							{call menu data=$connectedItems[5] currentItem=$filter.parentFilter forbiddenItem=$forbiddenItem}
						</select>
					</p>
					<p class="btn section-a">
						<button type="submit">
							Search
						</button>
					</p>
				</form>
			</div>

		{if $itemsList}
			{include file='pagination.tpl'}
			{include file='perPage.tpl'}
			<table class="item-table">
				<thead>
					<tr>
						<th class="toleft">ID</th>
						<th class="toleft">Reference title</th>
						<th class="toleft">Client</th>
						<th class="toleft">Pictures count</th>
						<th class="toleft">Movies count</th>
						<th colspan="4">Options</th>
					</tr>
				</thead>
				<tbody>
				{foreach from=$itemsList item=item}
					<tr{cycle values=' class="diff",'}>
						<td>{$item->id}</td>
						<td class="toleft"><a href="{$link}/edit/{$item->id}">{$item->title}</a></td>
						<td class="toleft">
							{if $connectedItems[5]}
								{foreach $connectedItems[5] as $connection}
									{if $connection->id eq $item->parent_id}
										<a href="items//edit/{$connection->id}">{$connection->title}</a>
									{/if}
								{/foreach}
							{/if}
						</td>
						<td class="toleft">{$item->pictures_num}</td>
						<td class="toleft">{$item->videos_num}</td>
						<td class="toleft"><a href="itemspictures/{$mainName}/chooseitem/{$item->id}">Manage pictures</a></td>
						<td class="toleft"><a href="itemsvideos/{$mainName}/chooseitem/{$item->id}">Manage movies</a></td>
						<td><a href="{$link}/edit/{$item->id}">Edit</a></td>
						<td><a rel="dialog-testimonials" href="{$link}/removeItem/{$item->id}{if $offset}/{$offset}{/if}" class="del-opt" title="'{$item->title}'">Delete</a></td>
					</tr>
				{/foreach}
				</tbody>
			</table>
			{include file='pagination.tpl'}
			{include file="common/importexport.tpl"}
		{/if}
		</div>
		<div class="dialog-box" id="dialog-testimonials" title="Delete news?">
			<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
				<span style="display:block; padding-left: 25px;">Team leader <em></em> will be deleted.<br/> Are you sure?</span>
			</p>
		</div>
{include file="footer.tpl"}
