<ul class="form-items">
	<li class="diff">
		<label for="title{$lang.id}">Title <em>*</em>:</label> <input type="text" name="title" id="title{$lang.id}" value="{$lang.itemInfo.title}"/>
	</li>
	<li class="diff offset">
		<label for="slug{$lang.id}">Slug:</label> <input type="text" name="slug" id="slug{$lang.id}" value="{$lang.itemInfo.slug}"/> {include file="common/slug_info.tpl"}		
	</li>
	<li>
		<label for="lead{$lang.id}">Lead:</label>
		<textarea rows="8" cols="90" id="lead{$lang.id}" name="lead">{$lang.itemInfo.lead}</textarea>
	</li>	
	<li class="diff">
		<label for="content{$lang.id}">Content:</label>
		<textarea rows="8" cols="90" id="content{$lang.id}" name="content">{$lang.itemInfo.content}</textarea>
	</li>		
	<li>
		<label for="text_data_1{$lang.id}">No more need to:</label>
		<textarea rows="8" cols="90" id="text_data_1{$lang.id}" name="text_data_1">{$lang.itemInfo.text_data_1}</textarea>                  
	</li>     
</ul>
