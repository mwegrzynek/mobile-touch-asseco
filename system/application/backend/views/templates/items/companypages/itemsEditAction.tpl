{include file="header.tpl"}
		<h1>{$header}: <em>{$itemTitle}</em></h1>
		{if $subheader}<h2>{$subheader}</h2>{/if}

		{if $errors}
		<h3 class="warning">{$errorHeader}</h3>
		<ul class="warning">
		{foreach from=$errors item=error}
			<li>{$error}</li>
		{/foreach}
		</ul>
		{/if}

		{*<h2>Text data:</h2>*}
		{if $backData.languages|@count > 1}
		<div class="tabs-container">
			<ul class="tabs">
				{foreach from=$backData.languages item=lang}
				<li><a href="#{$lang.lang_code}"><span>{$lang.name}</span></a></li>
				{/foreach}
			</ul>
		{/if}
			<div class="tab-content">
				{foreach from=$backData.languages item=lang name=langLoop key=key}
				<div id="{$lang.lang_code}" class="ui-tabs-panel">
					<form action="{$link}/edit/{$itemid}#{$lang.lang_code}" method="post" class="form-a">
						{if $subErrors[$lang.id]}
						<h3 class="warning">{$errorHeader[$lang.id]}</h3>
						<ul class="warning">
						{foreach from=$subErrors[$lang.id] item=error}
							<li>{$error}</li>
						{/foreach}
						</ul>
						{/if}

						<ul class="form-items">
							<li>
								<label for="title{$lang.id}">Title <em>*</em>:</label> <input type="text" name="title" id="title{$lang.id}" value="{$lang.itemInfo.title}"/>
							</li>
							<li class="diff">
								<label for="slug{$lang.id}">Slug:</label> <input type="text" name="slug" id="slug{$lang.id}" value="{$lang.itemInfo.slug}"/> {include file="common/slug_info.tpl"}
								{if $parents[$lang.id]}
									<p class="text-a">
										<span class="helper-c">full path:</span>
										<span class="helper-b">{foreach from=$parents[$lang.id] item=item key=key name=loop1}{$item[0]->slug}/{/foreach}{$lang.itemInfo.slug}.html</span>
									</p>
								{/if}
							</li>
							<li>
								<label for="lead{$lang.id}">Lead:</label>
								<textarea rows="8" cols="90" id="lead{$lang.id}" name="lead">{$lang.itemInfo.lead}</textarea>
							</li>
							<li class="diff">
								<label for="content{$lang.id}">Content:</label>
								<textarea rows="8" cols="90" id="content{$lang.id}" name="content" class="mceEditorSimple">{$lang.itemInfo.content}</textarea>
							</li>
							<li class="diff">
								<label for="text_data_1{$lang.id}">Content 1:</label>
								<textarea rows="8" cols="90" id="text_data_1{$lang.id}" name="text_data_1" class="mceEditorSimple">{$lang.itemInfo.text_data_1}</textarea>
							</li>
							<li>
								<label for="text_data_2{$lang.id}">Mobile section title:</label> <input type="text" name="text_data_2" id="text_data_2{$lang.id}" value="{$lang.itemInfo.text_data_2}"/>
							</li>
							<li class="diff">
								<label for="position{$lang.id}">Position:</label> <input type="text" name="position" id="position{$lang.id}" value="{$lang.itemInfo.position}" class="short-input"/>
							</li>
						</ul>
						<ul class="form-items1">
							<li>
								<input type="checkbox" {if $lang.itemInfo.is_active}checked="checked"{/if} name="is_active" id="is_active{$lang.id}" value="1"/> <label for="is_active{$lang.id}">Is active?</label>
							</li>
						</ul>
						{include	file="common/submit_buttons.tpl"}
					</form>
				</div>
			{/foreach}
		</div>
	{if $backData.languages|@count > 1}</div>{/if}
{include file="footer.tpl"}