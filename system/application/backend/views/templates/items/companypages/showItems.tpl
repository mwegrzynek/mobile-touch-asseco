{include file="header.tpl"}
		<h1>
			{if !$itemsList|count}
			No company pages found
			{else}
			All company pages ({$allItemsCount})
			{/if}
		</h1>

		{if !empty($subheader)}
		<h2>{$subheader}</h2>
		{/if}

		<div class="medium">
		{if $itemsList}
			{include file='pagination.tpl'}
			{include file='perPage.tpl'}
			<table class="item-table">
				<thead>
					<tr>
						<th class="toleft">ID</th>
						<th class="toleft">Title</th>
						<th class="toleft">Content</th>
						<th colspan="1">Options</th>
					</tr>
				</thead>
				<tbody>
				{foreach from=$itemsList item="item"}
					<tr{cycle values=' class="diff",'}>
						<td>{$item->id}</td>
						<td class="toleft"><a href="{$link}/edit/{$item->id}">{$item->title}</a></td>
						<td class="toleft">{$item->content|strip_tags|truncate:120:"&hellip;":false}</td>
						<td><a href="{$link}/edit/{$item->id}">Edit</a></td>
						{*<td><a rel="dialog-pages" href="{$link}/removeItem/{$item->id}{if $offset}/{$offset}{/if}" class="del-opt" title="'{$item->title}'">Delete</a></td>*}
					</tr>
				{/foreach}
				</tbody>
			</table>
			{include file='pagination.tpl'}
			{include file="common/importexport.tpl"}
		{/if}
		</div>
		<div class="dialog-box" id="dialog-pages" title="Delete page?">
			<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
				<span style="display:block; padding-left: 25px;">Page <em></em> will be delteted.<br/> Are you sure?</span>
			</p>
		</div>
{include file="footer.tpl"}
