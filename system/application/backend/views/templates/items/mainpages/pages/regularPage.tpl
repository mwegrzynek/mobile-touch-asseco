<ul class="form-items">
	<li class="diff">
		<label for="title{$lang.id}">Title <em>*</em>:</label> <input type="text" name="title" id="title{$lang.id}" value="{$lang.itemInfo.title}"/>
	</li>
	<li>
		<label for="slug{$lang.id}">Slug:</label> <input type="text" name="slug" id="slug{$lang.id}" value="{$lang.itemInfo.slug}"/> <span>If empty, generated automatically.</span>								
	</li>
	{*<li>
		<label for="lead{$lang.id}">Lead:</label>
		<textarea rows="8" cols="90" id="lead{$lang.id}" name="lead">{$lang.itemInfo.lead}</textarea>
	</li>
	<li class="diff">
		<label for="content{$lang.id}">Content:</label>
		<textarea rows="16" cols="90" id="content{$lang.id}" name="content" class="mceEditor">{$lang.itemInfo.content}</textarea>
	</li>	
	<li class="offset">
		<label for="position{$lang.id}">Pozycja w menu podstron:</label> <input type="text" name="position" id="position{$lang.id}" value="{$lang.itemInfo.position}" class="short-input"/>
	</li>*}
</ul>
<ul class="form-items1 offset">
	<li>
		<input type="checkbox" {if $lang.itemInfo.is_active}checked="checked"{/if} name="is_active" id="is_active{$lang.id}" value="1"/> <label for="is_active{$lang.id}">Is active?</label>
	</li>
</ul>