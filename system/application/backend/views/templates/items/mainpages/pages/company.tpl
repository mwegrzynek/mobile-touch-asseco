<ul class="form-items">
	<li class="diff">
		<label for="title{$lang.id}">Title <em>*</em>:</label> <input type="text" name="title" id="title{$lang.id}" value="{$lang.itemInfo.title}"/>
	</li>
	
	<li>
		<label for="slug{$lang.id}">Slug:</label> <input type="text" name="slug" id="slug{$lang.id}" value="{$lang.itemInfo.slug}"/> <span>If empty, generated automatically.</span>								
	</li>
	
	<li class="diff">
		<label for="text_data_3{$lang.id}">Submenu name:</label> <input type="text" name="text_data_3" id="text_data_3{$lang.id}" value="{$lang.itemInfo.text_data_3}"/>
	</li>
	
	<li>
		<label for="text_data_2{$lang.id}">Main header <em>*</em>:</label> 
		<textarea rows="8" cols="90" id="text_data_2{$lang.id}" name="text_data_2" class="mceEditorSimple">{$lang.itemInfo.text_data_2}</textarea>		
	</li>
	
	<li class="diff">
		<label for="lead{$lang.id}">Lead part 1:</label>
		<textarea rows="6" cols="90" id="lead{$lang.id}" name="lead" class="mceEditorSimple">{$lang.itemInfo.lead}</textarea>
	</li>
	
	<li class="diff">
		<label for="text_data_4{$lang.id}">Lead part 2:</label>
		<textarea rows="6" cols="90" id="text_data_4{$lang.id}" name="text_data_4">{$lang.itemInfo.text_data_4}</textarea>
	</li>
	
	<li>
		<label for="text_data_1{$lang.id}">Boxes header:</label> 
		<textarea rows="8" cols="90" id="text_data_1{$lang.id}" name="text_data_1" class="mceEditorSimple">{$lang.itemInfo.text_data_1}</textarea>		
	</li>
	
	<li class="diff">
		<label for="content{$lang.id}">Main content:</label>
		<textarea rows="32" cols="90" id="content{$lang.id}" name="content" class="mceEditorWide">{$lang.itemInfo.content}</textarea>
	</li>		
</ul>