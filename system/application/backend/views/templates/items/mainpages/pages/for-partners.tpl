<ul class="form-items">
	<li class="diff">
		<label for="title{$lang.id}">Title <em>*</em>:</label> <input type="text" name="title" id="title{$lang.id}" value="{$lang.itemInfo.title}"/>
	</li>

	<li>
		<label for="slug{$lang.id}">Slug:</label> <input type="text" name="slug" id="slug{$lang.id}" value="{$lang.itemInfo.slug}"/> <span>If empty, generated automatically.</span>
	</li>

	<li class="diff">
		<label for="lead{$lang.id}">Main header <em>*</em>:</label>
		<textarea rows="3" cols="90" id="lead{$lang.id}" name="lead">{$lang.itemInfo.lead}</textarea>
	</li>

	<li>
		<label for="text_data_1{$lang.id}">Lead <em>*</em>:</label>
		<textarea rows="5" cols="90" id="text_data_1{$lang.id}" name="text_data_1">{$lang.itemInfo.text_data_1}</textarea>
	</li>

	<li class="diff">
		<label for="file_data_1{$lang.id}">Button label:</label> <input type="text" name="file_data_1" id="file_data_1{$lang.id}" value="{$lang.itemInfo.file_data_1}"/>
	</li>

	<li>
		<label for="content{$lang.id}">Section 1 Content:</label>
		<textarea rows="32" cols="90" id="content{$lang.id}" name="content" class="mceEditorWide">{$lang.itemInfo.content}</textarea>
	</li>

	<li class="diff">
		<label for="text_data_2{$lang.id}">Testimonials Header:</label> <input type="text" name="text_data_2" id="text_data_2{$lang.id}" value="{$lang.itemInfo.text_data_2}"/>
	</li>

	<li>
		<label for="text_data_3{$lang.id}">Section 2 Content:</label>
		<textarea rows="32" cols="90" id="text_data_3{$lang.id}" name="text_data_3" class="mceEditorWide">{$lang.itemInfo.text_data_3}</textarea>
	</li>

	<li class="diff">
		<label for="text_data_4{$lang.id}">Section 3 Content:</label>
		<textarea rows="32" cols="90" id="text_data_4{$lang.id}" name="text_data_4" class="mceEditorWide">{$lang.itemInfo.text_data_4}</textarea>
	</li>
</ul>