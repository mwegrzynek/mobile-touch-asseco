<ul class="form-items">
	<li class="diff">
		<label for="title{$lang.id}">Title <em>*</em>:</label> <input type="text" name="title" id="title{$lang.id}" value="{$lang.itemInfo.title}"/>
	</li>

	<li class="offset">
		<label for="slug{$lang.id}">Slug:</label> <input type="text" name="slug" id="slug{$lang.id}" value="{$lang.itemInfo.slug}"/> <span>If empty, generated automatically.</span>
	</li>

	<li class="diff">
		<label for="content{$lang.id}">Table Section Content:</label>
		<textarea rows="32" cols="90" id="content{$lang.id}" name="content" class="mceEditorWide">{$lang.itemInfo.content}</textarea>
	</li>

	<li>
		<label for="text_data_3{$lang.id}">Section 4 Content:</label>
		<textarea rows="32" cols="90" id="text_data_3{$lang.id}" name="text_data_3" class="mceEditorWide">{$lang.itemInfo.text_data_3}</textarea>
	</li>

	<li class="diff">
		<label for="text_data_2{$lang.id}">Button label <em>*</em>:</label> <input type="text" name="text_data_2" id="text_data_2{$lang.id}" value="{$lang.itemInfo.text_data_2}"/>
	</li>
</ul>