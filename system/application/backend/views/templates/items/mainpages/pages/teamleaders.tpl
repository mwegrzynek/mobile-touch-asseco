<ul class="form-items">
	<li class="diff">
		<label for="title{$lang.id}">Title <em>*</em>:</label> <input type="text" name="title" id="title{$lang.id}" value="{$lang.itemInfo.title}"/>
	</li>
	
	<li>
		<label for="slug{$lang.id}">Slug:</label> <input type="text" name="slug" id="slug{$lang.id}" value="{$lang.itemInfo.slug}"/> <span>If empty, generated automatically.</span>								
	</li>	
	
	<li class="diff">
		<label for="text_data_2{$lang.id}">Main header <em>*</em>:</label> 
		<textarea rows="8" cols="90" id="text_data_2{$lang.id}" name="text_data_2" class="mceEditorSimple">{$lang.itemInfo.text_data_2}</textarea>		
	</li>	
	
</ul>