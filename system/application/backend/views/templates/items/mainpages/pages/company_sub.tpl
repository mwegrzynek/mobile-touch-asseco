<ul class="form-items">
	<li class="diff">
		<label for="title{$lang.id}">Title <em>*</em>:</label> <input type="text" name="title" id="title{$lang.id}" value="{$lang.itemInfo.title}"/>
	</li>
	
	<li class="offset">
		<label for="slug{$lang.id}">Slug:</label> <input type="text" name="slug" id="slug{$lang.id}" value="{$lang.itemInfo.slug}"/> <span>If empty, generated automatically.</span>								
	</li>	
	
	<li class="diff">
		<label for="text_data_1{$lang.id}">Mobile Section Header:</label> <input type="text" name="text_data_1" id="text_data_1{$lang.id}" value="{$lang.itemInfo.text_data_1}"/>
	</li>
	
	<li>
		<label for="lead{$lang.id}">Box header:</label> 
		<textarea rows="8" cols="90" id="lead{$lang.id}" name="lead" class="mceEditorSimple">{$lang.itemInfo.lead}</textarea>		
	</li>
	
	<li class="diff">
		<label for="content{$lang.id}">Content:</label>
		<textarea rows="6" cols="90" id="content{$lang.id}" name="content">{$lang.itemInfo.content}</textarea>
	</li>
</ul>
<ul class="form-items1 offset">
	<li>
		<input type="checkbox" {if $lang.itemInfo.is_active}checked="checked"{/if} name="is_active" id="is_active{$lang.id}" value="1"/> <label for="is_active{$lang.id}">Is active?</label>
	</li>
</ul>