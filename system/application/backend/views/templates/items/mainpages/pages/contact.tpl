<ul class="form-items">
	<li class="diff">
		<label for="title{$lang.id}">Title <em>*</em>:</label> <input type="text" name="title" id="title{$lang.id}" value="{$lang.itemInfo.title}"/>
	</li>
	<li class="diff">
		<label for="slug{$lang.id}">Slug:</label> <input type="text" name="slug" id="slug{$lang.id}" value="{$lang.itemInfo.slug}"/> <span>If empty, generated automatically.</span>								
	</li>
	<li>
		<label for="lead{$lang.id}">Lead:</label>
		<textarea rows="2" cols="90" id="lead{$lang.id}" name="lead">{$lang.itemInfo.lead}</textarea>
	</li>
	<li class="diff">
		<label for="content{$lang.id}">Contact Info:</label>
		<textarea rows="16" cols="90" id="content{$lang.id}" name="content" class="mceEditorSimple">{$lang.itemInfo.content}</textarea>
	</li>		
	<li class="diff">
		<label for="text_data_1{$lang.id}">Contact Info:</label>
		<textarea rows="16" cols="90" id="text_data_1{$lang.id}" name="text_data_1" class="mceEditorSimple">{$lang.itemInfo.text_data_1}</textarea>
	</li>		
</ul>
<ul class="form-items1 offset">
	<li>
		<input type="checkbox" {if $lang.itemInfo.is_active}checked="checked"{/if} name="is_active" id="is_active{$lang.id}" value="1"/> <label for="is_active{$lang.id}">Is active?</label>
	</li>
</ul>