{include file="header.tpl"}
		<h1>{$header}: <em>{$itemTitle}</em></h1>
		{if $subheader}<h2>{$subheader}</h2>{/if}

		{if $errors}
		<h3 class="warning">{$errorHeader}</h3>
		<ul class="warning">
		{foreach from=$errors item=error}
			<li>{$error}</li>
		{/foreach}
		</ul>
		{/if}


		{if $itemid eq 636 or $itemid eq 634 or $itemid eq 691 or $itemid eq 692 or $itemid eq 635 or $itemid eq 607 or $itemid eq 759 or $itemid eq 760}
		<form action="{$link}/edit/{$itemid}" method="post" class="form-a fa-a" enctype="multipart/form-data">
			{if !$noImages}
				{assign var=path value=$picturePath|cat:'/'|cat:$imageBackData.main_image}
				{if $path|is_file}
					{assign var=isFile value=true}
					<div id="pictureContainer">
						<a href="{$path}" class="fancy"><img src="{$path}" alt="" width="100"/></a>
						<p class="imagesubtitle">
							<a href="{$link}/removeImage/{$itemid}">Remove image</a>
						</p>
					</div>
				{/if}
			{/if}
			<div{if $isFile} class="withImage2"{/if} id="independentSection">
				{include file="common/menu_tree.tpl" scope=parent}
				<ul class="form-items">
					<li>
						<input type="hidden" name="MAX_FILE_SIZE" value="524288"/>
						<label for="thefile">Images <span>(.jpg|.png)</span></label>
						<input type="file" id="thefile" name="thefile" size="47"/>
					</li>
				</ul>
				<p class="btn">
					<button type="submit">
						Change settings &gt;
					</button>
				</p>
			</div>
		</form>
		{/if}

		<h2>Language dependent data:</h2>
		{if $backData.languages|@count > 1}
		<div class="tabs-container">
			<ul class="tabs">
				{foreach from=$backData.languages item=lang}
				<li><a href="#{$lang.lang_code}"><span>{$lang.name}</span></a></li>
				{/foreach}
			</ul>
		{/if}
			<div class="tab-content">
				{foreach from=$backData.languages item=lang name=langLoop key=key}
				<div id="{$lang.lang_code}" class="ui-tabs-panel">
					<form action="{$link}/edit/{$itemid}#{$lang.lang_code}" method="post" class="form-a">
						{if $subErrors[$lang.id]}
						<h3 class="warning">{$errorHeader[$lang.id]}</h3>
						<ul class="warning">
						{foreach from=$subErrors[$lang.id] item=error}
							<li>{$error}</li>
						{/foreach}
						</ul>
						{/if}

						{if $itemid eq 213}
							{include file="items/mainpages/pages/404Page.tpl"}
							{include	file="common/regular_meta.tpl"}
						{elseif $itemid eq 219}
							{include file="items/mainpages/pages/contact.tpl"}
							{include	file="common/regular_meta.tpl"}
						{elseif $itemid eq 943}
							{include file="items/mainpages/pages/for-partners.tpl"}
							{include	file="common/regular_meta.tpl"}
						{elseif $itemid eq 944}
							{include file="items/mainpages/pages/for-partners-2.tpl"}
						{elseif $itemid eq 636}
							{include file="items/mainpages/pages/company.tpl"}
							{include	file="common/regular_meta.tpl"}
						{elseif $itemid eq 688 or $itemid eq 689 or $itemid eq 690}
							{include file="items/mainpages/pages/company_sub.tpl"}
						{elseif $itemid eq 634}
							{include file="items/mainpages/pages/resources.tpl"}
						{include	file="common/regular_meta.tpl"}
						{elseif $itemid eq 691 or $itemid eq 692 or $itemid eq 759 or $itemid eq 760}
							{include file="items/mainpages/pages/resources_sub.tpl"}
						{elseif $itemid eq 635}
							{include file="items/mainpages/pages/references.tpl"}
							{include	file="common/regular_meta.tpl"}
						{elseif $itemid eq 607}
							{include file="items/mainpages/pages/teamleaders.tpl"}
							{include	file="common/regular_meta.tpl"}
						{else}
							{include file="items/mainpages/pages/regularPage.tpl"}
							{include	file="common/regular_meta.tpl"}
						{/if}
						{include	file="common/submit_buttons.tpl"}
					</form>
				</div>
			{/foreach}
		</div>
	{if $backData.languages|@count > 1}</div>{/if}
{include file="footer.tpl"}