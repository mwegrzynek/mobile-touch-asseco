{include file="header.tpl"}
		<h1>{$header}</h1>
		{if $subheader}<h2>{$subheader}</h2>{/if}

		<div class="tab-content">         
			<form action="{$link}/addnew" method="post" class="form-a" enctype="multipart/form-data">
				{if $errors}
				<h3 class="warning">{$errorHeader}</h3>
				<ul class="warning">
				{foreach from=$errors item=error}
					<li>{$error}</li>
				{/foreach}
				</ul>
				{/if}
			
				<ul class="form-items"> 					
					
					{*<li>
						<input type="hidden" name="MAX_FILE_SIZE" value="524288"/>
						<label for="thefile">Image <span>(.jpg|.png)</span></label>
						<input type="file" id="thefile" name="thefile" size="47"/>
					</li>
					<li>
						<span><strong>Image:</strong> Side exactly 630x144px, max 512KB</span>
					</li>*}
									
					<li class="diff">
						<label for="title">Title <em>*</em>:</label> <input type="text" name="title" id="title" value="{$backData.title}"/>   
						<input type="hidden" name="parentid" value="0"/>          						
						<input type="hidden" name="categoryid" value="0"/>           
					</li>
					<li>                     
						<label for="slug">Slug:</label> <input type="text" name="slug" id="slug" value="{$backData.slug}"/> <span>If empty, generated automatically.</span>                  
					</li>
					<li class="diff">
						<label for="lead">Lead:</label>
						<textarea rows="8" cols="90" id="lead" name="lead">{$backData.lead}</textarea>                  
					</li>         
					<li>
						<label for="content-a">Content:</label>
						<textarea rows="16" cols="90" id="content-a" name="content" class="mceEditor">{$backData.content}</textarea>                  
					</li>
					{*<li>
						<label for="text_data_1">Treść dodatkowa (opcjonalna, nie wszędzie będzie pokazana):</label>
						<textarea rows="16" cols="90" id="text_data_1" name="text_data_1" class="mceEditor">{$backData.content}</textarea>                  
					</li>*}
					<li class="diff offset">                     
						<label for="position">Position:</label> <input type="text" name="position" id="position" value="{$backData.position|default:1}" class="short-input"/> 
					</li>	               
				</ul>
				
				<ul class="form-items1">
					<li>
						<input type="checkbox" {if $backData.is_active}checked="checked"{/if} name="is_active" id="is_active" value="1"/> <label for="is_active">Is active?</label>               
					</li>
				</ul>
				
				<h3 class="group-section">Meta Tags</h3>
				<ul class="form-items">
					<li>                    
						<label for="metaTitle">Title:</label> <input type="text" name="metaTitle" id="metaTitle" value="{$backData.metaTitle}"/>                     
					</li>
					<li class="diff">
						<label class="type1" for="metaKeywords">Keywords:</label> <textarea rows="3" cols="80" id="metaKeywords" name="metaKeywords">{$backData.metaKeywords}</textarea>            
					</li>
					<li>
						<label class="type1" for="metaDescription">Description:</label> <textarea rows="3" cols="80" id="metaDescription" name="metaDescription">{$backData.metaDescription}</textarea>            
					</li>                               
				</ul>    
				<p class="btn">					
					<button type="submit">
						Add &gt;
					</button>
					
					{if $languages|@count > 1}
					<input type="hidden" name="next" value="{$languages[1]->lang_code}"/>
					<button class="btn-a" type="submit" name="submit-and-edit-next" value="1">
						Add and edit next language &gt;
					</button>
					
					<button class="btn-a" type="submit" name="submit-and-fill-langs" value="1">
						Add to this and all other languages &gt;
					</button>
					{/if}
				</p>
			</form>         
		</div>         
{include file="footer.tpl"}