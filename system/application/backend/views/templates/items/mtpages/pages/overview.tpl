<ul class="form-items">
	<li class="diff">
		<label for="title{$lang.id}">Title <em>*</em>:</label> <input type="text" name="title" id="title{$lang.id}" value="{$lang.itemInfo.title}"/>
	</li>

	<li>
		<label for="slug{$lang.id}">Slug:</label> <input type="text" name="slug" id="slug{$lang.id}" value="{$lang.itemInfo.slug}"/> <span>If empty, generated automatically.</span>
	</li>

	<li class="diff">
		<label for="text_data_3{$lang.id}">Submenu name:</label> <input type="text" name="text_data_3" id="text_data_3{$lang.id}" value="{$lang.itemInfo.text_data_3}"/>
	</li>

	<li>
		<label for="text_data_2{$lang.id}">Main header <em>*</em>:</label>
		<textarea rows="8" cols="90" id="text_data_2{$lang.id}" name="text_data_2" class="mceEditorSimple">{$lang.itemInfo.text_data_2}</textarea>
	</li>

	<li class="diff">
		<label for="lead{$lang.id}">Lead:</label>
		<textarea rows="6" cols="90" id="lead{$lang.id}" name="lead">{$lang.itemInfo.lead}</textarea>
	</li>

	<li>
		<label for="file_data_2{$lang.id}">Main Visual file:</label> <input type="text" class="filemanager" name="file_data_2" id="file_data_2{$lang.id}" value="{$lang.itemInfo.file_data_2}"/> <img src="images/picture.png" width="16" height="16" class="filemanager-ico" alt="Picture"/>
	</li>

	<li class="diff">
		<label for="file_data_1{$lang.id}">Visual file:</label> <input type="text" class="filemanager" name="file_data_1" id="file_data_1{$lang.id}" value="{$lang.itemInfo.file_data_1}"/> <img src="images/picture.png" width="16" height="16" class="filemanager-ico" alt="Picture"/>
	</li>

	<li>
		<label for="file_data_3{$lang.id}">PDF Folder File:</label> <input type="text" class="filemanager" name="file_data_3" id="file_data_3{$lang.id}" value="{$lang.itemInfo.file_data_3}"/> <img src="images/picture.png" width="16" height="16" class="filemanager-ico" alt="Picture"/>
	</li>

	<li>
		<label for="text_data_1{$lang.id}">Tabs header:</label>
		<textarea rows="8" cols="90" id="text_data_1{$lang.id}" name="text_data_1" class="mceEditorSimple">{$lang.itemInfo.text_data_1}</textarea>
	</li>
</ul>