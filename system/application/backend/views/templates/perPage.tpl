<div class="pages{if !$pagination} clearme{/if}">
   <p>Records per page:</p>
   <ul>
      {foreach from=$perPageItems item=item}
      <li><a {if $item eq $currentPerPage} class="current"{/if}href="{$perPageLink}/{$item}">{$item}</a></li>
      {/foreach}
   </ul>
</div>