{include file="header.tpl"}
		<h1>{$header}{if $subheader} -&gt; <em>{$subheader}</em>{/if}</h1>
		<form action="language/setdefault" method="post" id="form-a">
			{if $errors}
			<h3 class="warning">{$errorHeader}</h3>
			<ul class="warning">
			{foreach from=$errors item=error}
				<li>{$error}</li>
			{/foreach}
			</ul>
			{/if}
			<div id="mainDataSection">				
				<ul class="form-items1">
					{foreach from=$backData item=item key=key name=loop1}
					<li>
						<input type="radio" {if $item->id eq $defaultLang}checked="checked"{/if} name="langid" id="langid{$item->id}" value="{$item->id}"/> <label for="langid{$item->id}">{$item->name}</label>
					</li>
					{/foreach}
				</ul>
				<p class="btn">
					<button type="submit">
						Zatwierdź &gt;
					</button>
				</p>
			</div>		
		</form>
{include file="footer.tpl"}