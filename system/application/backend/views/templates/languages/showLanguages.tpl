{include file="header.tpl"}
      <h1>
      {if $norecords}
      No language found
      {else}
      All languages ({$itemsList|@count})
      {/if}
      </h1>
      {if $subheader}
      <h2>{$subheader}</h2>
      {/if}
      <div class="medium">

      {if $itemsList}
         <table class="item-table">
            <thead>
               <tr>
                  <th class="toleft">Language name</th>
                  <th class="toleft">Language code</th>
                  <th class="toleft">Domain/ subdomain</th>
                  <th>Is visible</th>
                  <th>Is active</th>
                  <th colspan="2">Options</th>
               </tr>
            </thead>
            <tbody>
            {foreach from=$itemsList item=item}
               <tr{cycle values=' class="diff",'}>
                  <td class="toleft"><a href="language/edit/{$item->id}">{$item->name}</a></td>
                  <td class="toleft">{$item->lang_code}</td>
                  {*
                  {assign var=path value=../images/flags/`$item->lang_code`.png}
                  <td class="toleft">{if $path|is_file}<img src="{$path}" alt=""/>{/if}</td>
                  *}
                  <td class="toleft">{$item->subdomain}</td>
                  <td><a href="language/status/visibility/{$item->id}" title="Change status">{if $item->is_visible}Visible{else}Not visible{/if}</a></td>
                  <td><a href="language/status/active/{$item->id}" title="Change status">{if $item->is_active}Active{else}Not active{/if}</a></td>
                  <td><a href="language/edit/{$item->id}">Edit</a></td>
                  <td><a href="language/removelang/{$item->id}" class="del-opt" rel="dialog-lang">Delete</a></td>
               </tr>
            {/foreach}
            </tbody>
         </table>
      {/if}
      </div><!-- .medium -->
      <div class="dialog-box" id="dialog-lang" title="Delete language? (not recommended). We recommend deactivate it.">
			<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
				<span style="display:block; padding-left: 25px;">Language <em></em> Will be deleted. All categories, menu items pages entries and other data in this language will be removed<br/> Are you sure?</span>
			</p>
		</div>
{include file="footer.tpl"}