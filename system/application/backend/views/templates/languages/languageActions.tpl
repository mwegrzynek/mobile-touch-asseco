{include file="header.tpl"}
		<h1>{$header}{if $subheader} -&gt; <em>{$subheader}</em>{/if}</h1>
		<form action="language/{$formLink}" method="post" id="form-a" enctype="multipart/form-data">
			{if $errors}
			<h3 class="warning">{$errorHeader}</h3>
			<ul class="warning">
			{foreach from=$errors item=error}
				<li>{$error}</li>
			{/foreach}
			</ul>
			{/if}
			<div id="mainDataSection">
				<ul class="form-items">
					{if $add}
					<li>
						<label for="copy_lang_id">Copy items from <em>*</em>:</label>
						<select name="copy_lang_id" id="copy_lang_id">
							{foreach $langs as $lang}
							<option value="{$lang->id}">{$lang->name}</option>
							{/foreach}
						</select>
					</li>
					{/if}
					<li class="diff">
						<label for="langName">Language name <em>*</em>:</label> <input type="text" name="langName" id="langName" value="{$backData.langName}"/>
					</li>
					<li>
						{assign var=path value="../images/flags/"|cat:$backData.langCode|cat:".png"}
						<label for="langCode">Language code <em>*</em>:</label>
						<input type="text" name="langCode" id="langCode" value="{$backData.langCode}" class="medium-input"/>
						<span>{if $path|is_file}<img src="{$path}" alt=""/>{/if}  2 letters (Code ISO 639-1 see <a href="http://www.loc.gov/standards/iso639-2/php/code_list.php">ISO document</a>)</span>
					</li>
					<li class="diff">
						<label for="subDomain">Domain / subdomain <em>*</em>:</label> <input type="text" name="subDomain" id="subDomain" value="{$backData.subDomain}"/>
					</li>
					<li>
						{assign var=path value="../userfiles/languages/"|cat:$backData.langCode|cat:".png"}
						<input type="hidden" name="MAX_FILE_SIZE" value="524288"/>
						<label for="thefile">Flag <span>(.png)</span> {if $path|is_file}<img src="{$path}" alt=""/>{/if}</label>
						<input type="file" id="thefile" name="thefile" size="47"/>
					</li>
					<li>
						<span><strong>Flag:</strong> Size exactly 14x11px, max 100KB</span>
					</li>
				</ul>
				<ul class="form-items1">
					<li>
						<input type="checkbox" {if $backData.is_visible}checked="checked"{/if} name="is_visible" id="is_visible" value="1"/> <label for="is_visible">Is visible</label>
					</li>
					<li>
						<input type="checkbox" {if $backData.isLangActive}checked="checked"{/if} name="isLangActive" id="isLangActive" value="1"/> <label for="isLangActive">Is active</label>
					</li>
				</ul>
				<p class="btn">
					<button type="submit">
						Submit &gt;
					</button>
				</p>
			</div>
		</form>
{include file="footer.tpl"}