{include file="header.tpl"}
      <h1>{$header}{if $subheader} -&gt; <em>{$subheader}</em>{/if}</h1>
      <form action="" method="post" id="stepForm">
         {if $errors}
         <h3 class="warning">{$errorHeader}</h3>
         <ul class="warning">
         {foreach from=$errors item=error}
            <li>{$error}</li>
         {/foreach}
         </ul>
         {/if}

         
         <div id="tabContentContainer">
            <ul class="formItems">                  
               <li class="diff">
                  <label for="adCode">Ad Code:</label>  
						<textarea rows="10" cols="60" id="adCode" name="adCode">{$backData.adCode}</textarea>                  
               </li>    
					
					<li class="diff">
                  <label for="adminMail">List position <br/>(0 = not show):</label> <input class="short-input" type="text" name="position" id="position" value="{$backData.position|default:'0'}"/>                   
               </li>                           
            </ul>                  
         </div><!--end tab container-->
         <p><input type="image" alt="Submit" src="images/submit-btn.gif"/></p>
      </form>
{include file="footer.tpl"}