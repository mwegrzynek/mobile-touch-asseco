{include file="header.tpl"}
		<h1>{$header}{if $subheader} -&gt; <em>{$subheader}</em>{/if}</h1>
		<form action="{$link}" method="post" class="form-a">
			{if $errors}
			<h3 class="warning">{$errorHeader}</h3>
			<ul class="warning">
			{foreach from=$errors item=error}
				<li>{$error}</li>
			{/foreach}
			</ul>
			{/if}         
			<div class="tab-content">
				<ul class="form-items">
					<li class="diff">
						<label for="adminMail">Produkt <em>*</em>:</label> 
						<select name="itemid" id="itemid">						
							{if $itemList}
								{foreach from=$itemList item=item key=key}																
								<option value="{$item->id}"{if $item->id eq $backData.itemid} selected="selected"{/if}>
									{$item->title}
								</option>		
								{/foreach}
							{/if}
						</select>					
					</li>
				</ul>
			</div><!--end tab container-->
			<p class="btn">
				<button type="submit">
					Bestätige &gt;
				</button>
			</p>
		</form>
{include file="footer.tpl"}