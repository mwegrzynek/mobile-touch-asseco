{include file="header.tpl"}
      <h1>{$header}{if $subheader} -&gt; <em>{$subheader}</em>{/if}</h1>
      <form action="" method="post" id="stepForm">
         {if $errors}
         <h3 class="warning">{$errorHeader}</h3>
         <ul class="warning">
         {foreach from=$errors item=error}
            <li>{$error}</li>
         {/foreach}
         </ul>
         {/if}
         <div id="tabContentContainer">
            <ul class="formItems">                  
               <li class="diff">
                  <label for="topCode">Top Banner Code:</label>  
						<textarea rows="10" cols="60" id="topCode" name="topCode">{$backData.topCode}</textarea>                  
               </li>
					<li>
                  <label for="sideCode">Side Banner Code:</label>  
						<textarea rows="10" cols="60" id="sideCode" name="sideCode">{$backData.sideCode}</textarea>                  
               </li>					                         
            </ul>                  
         </div>
         <p><input type="image" alt="Submit" src="images/submit-btn.gif"/></p>
      </form>
{include file="footer.tpl"}