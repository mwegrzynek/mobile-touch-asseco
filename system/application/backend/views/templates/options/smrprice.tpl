{include file="header.tpl"}
		<h1>{$header}{if $subheader} -&gt; <em>{$subheader}</em>{/if}</h1>
		<form action="smrprice" method="post" class="form-a">
			{if $errors}
			<h3 class="warning">{$errorHeader}</h3>
			<ul class="warning">
			{foreach from=$errors item=error}
				<li>{$error}</li>
			{/foreach}
			</ul>
			{/if}         
			<div class="tab-content">
				<ul class="form-items">
					<li class="diff">
						<label for="price">Cena brutto SMR <em>*</em>:</label> <input type="text" name="price" id="price" value="{$backData.price}"/>
					</li>
				</ul>
				<p class="btn">
					<button type="submit">
						Zatwierdź &gt;
					</button>
				</p>
			</div><!--end tab container-->
		</form>
{include file="footer.tpl"}


