{include file="header.tpl"}
		<h1>{$header}{if $subheader} -&gt; <em>{$subheader}</em>{/if}</h1>
		<form action="{$link}" method="post" class="form-a" enctype="multipart/form-data">
			{if $errors}
			<h3 class="warning">{$errorHeader}</h3>
			<ul class="warning">
			{foreach from=$errors item=error}
				<li>{$error}</li>
			{/foreach}
			</ul>
			{/if}         
			<div class="tab-content">
				<ul class="form-items">
					<li class="diff">
						<input type="hidden" name="MAX_FILE_SIZE" value="8512000"/>
						<label for="thefile">Herunterlade den neuen Katalog</span></label>
						<input type="file" id="thefile" name="thefile" size="47"/>
						<div class="text-box-a">
							<p>max 7MB</p>
						</div>
					</li>
				</ul>
			</div><!--end tab container-->
			<p class="btn">
				<button type="submit">
					Bestätige &gt;
				</button>
			</p>
		</form>
{include file="footer.tpl"}