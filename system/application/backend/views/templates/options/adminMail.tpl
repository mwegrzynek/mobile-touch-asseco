﻿{include file="header.tpl"}
		<h1>{$header}{if $subheader} -&gt; <em>{$subheader}</em>{/if}</h1>
		<form action="adminmail" method="post" class="form-a">
			{if $errors}
			<h3 class="warning">{$errorHeader}</h3>
			<ul class="warning">
			{foreach from=$errors item=error}
				<li>{$error}</li>
			{/foreach}
			</ul>
			{/if}         
			<div class="tab-content">
				<ul class="form-items">
					<li class="diff">
						<label for="adminMail">Mail <em>*</em>:</label> <input type="text" name="adminMail" id="adminMail" value="{$backData.adminMail}"/>
					</li>
				</ul>
				<p class="btn">
					<button type="submit">
						Zatwierdź &gt;
					</button>
				</p>
			</div><!--end tab container-->
		</form>
{include file="footer.tpl"}


