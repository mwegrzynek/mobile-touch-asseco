{if $pagination}
   <ul class="pagination">
      {if $pagination.first}
         <li class="first">
            <a href="{$pagination.first.url}">{$pagination.first.link|default:'&lsaquo; First'}</a>
         </li>
      {/if}
      
      {if $pagination.previous}
         <li>
            <a href="{$pagination.previous.url}">{$pagination.previous.link|default:'&lt;'}</a>
         </li>
      {/if}
      
      {if $pagination.pages}
      {foreach from=$pagination.pages item="pageitem"}
      <li>
         {if $pageitem.url eq ''}
         <span>{$pageitem.link}</span>
         {else}
         <a href="{$pageitem.url}">{$pageitem.link}</a>
         {/if}
      </li>
      {/foreach}
      {/if}            
      
      {if $pagination.next}
         <li>
            <a href="{$pagination.next.url}">{$pagination.next.link|default:'&gt;'}</a>
         </li>
      {/if}
      
      {if $pagination.last}
         <li class="last">
            <a href="{$pagination.last.url}">{$pagination.last.link|default:'Last &rsaquo;'}</a>
         </li>
      {/if}
   </ul>
{/if}
