<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pl" lang="pl">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>{$noticeData.title}</title>
	<style type="text/css">
		p { margin-bottom: 5px; padding: 0; }
	</style>
</head>
<body>
	<div>
		<h2>http://{$smarty.server.HTTP_HOST}</h2>
		<p>
			<hr/>
			{$noticeData.notificationHeader}
			<hr/>
		</p>
		<p>{$noticeData.message}</p>		
		<p>
			{$noticeData.additionalMessage}
			<a href="{$noticeData.link}">{$noticeData.link}			
		</p>
		<p><hr/></p>	
	</div>
</body>
</html>	