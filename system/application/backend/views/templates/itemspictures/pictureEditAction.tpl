{include file="header.tpl"}
		<h1>{$header}</h1>
		{if $subheader}<h2 class="helper">{$subheader}</h2>{/if}             
	  
		{if $errors}
		<h3 class="warning">{$errorHeader}</h3>
		<ul class="warning">
		{foreach from=$errors item=error}
			<li>{$error}</li>
		{/foreach}
		</ul>
		{/if}				
		<form action="{$link}/edit/{$backData.pictureid}" method="post" class="form-b" enctype="multipart/form-data">			
			<ul class="form-items">				
				{assign var=path value=$picturePath|cat:'/'|cat:$backData.picture_thumb}		
				{if $backData.picture AND $path|is_file}						
					{assign var=isFile value=true}
				{/if}
				
				<li class="diff">					
					<label for="itemid">Change gallery</label>
					<select name="itemid" id="itemid">
						{if $itemsTree}
							{foreach from=$itemsTree item=item key=key}                        
							{if $item->id eq $itemId}
							<option value="{$item->id}" selected="selected" class="level-0">
							{else}
							<option value="{$item->id}" class="level-0">
							{/if}
								{$item->title}
							</option>
							{if $item->children}
								{include file="common/items_tree_recursion.tpl" element=$item->children level=1 currentItem=$backData.parentid}
							{/if}
							{/foreach}
						{/if}
					</select>					
				</li>
				
				<li>
					{if $isFile}
					<div class="image-box">
						<p><a href="{$picturePath}/{$backData.picture}" class="fancy"><img src="{$picturePath}/{$backData.picture_thumb}" alt="" width="100"/></a></p>					
					</div>
					{/if}
					
					<input type="hidden" name="MAX_FILE_SIZE" value="1512000"/>
					<label for="thefile">{if $isFile}Change {else}Add {/if} picture <span>(.jpg)</span></label>
					<input type="file" id="thefile" name="thefile" size="47"/>
					<div class="text-box-a">
						<p>max 1500KB</p>
					</div>					
				</li>		
				<li class="diff">
					<label for="position">Position:</label> <input type="text" name="position" id="position" value="{$backData.position}" class="short-input"/>                  
				</li>	
			</ul>
			<p class="btn">				
				<button type="submit">
					Change picture options &gt;
				</button>
			</p>
		</form>			
			 
		{if $backData.languages|@count > 1}
		<div class="tabs-container">            
			<ul class="tabs">            
				{foreach from=$backData.languages item=lang}
				<li><a href="#{$lang.lang_code}"><span>{$lang.name}</span></a></li>
				{/foreach}                
			</ul>
		</div>      
		{/if}
		<div class="tab-content">                  
			{foreach from=$backData.languages item=lang name=langLoop key=key}
			<div id="{$lang.lang_code}" class="ui-tabs-panel">
				<form action="{$link}/edit/{$backData.pictureid}#{$lang.lang_code}" method="post" class="form-a">      
					{if $subErrors[$lang.id]}
					<h3 class="warning">{$errorHeader[$lang.id]}</h3>
					<ul class="warning">
					{foreach from=$subErrors[$lang.id] item=error}
						<li>{$error}</li>
					{/foreach}
					</ul>
					{/if}               
					
					<h3 class="group-section">Image meta data:</h3>
					<ul class="form-items">
						<li>
							<label for="metaTitle{$lang.id}">Title:</label> <input type="text" name="meta[title]" id="metaTitle{$lang.id}" value="{$lang.metaInfo.title}"/>
						</li>
						<li class="diff">
							<label class="type1" for="metaKeywords{$lang.id}">Atl tag:</label> <textarea rows="3" cols="80" id="metaKeywords{$lang.id}" name="meta[keywords]">{$lang.metaInfo.keywords}</textarea>
						</li>
						{*<li>
							<label class="type1" for="metaDescription{$lang.id}">Description:</label> <textarea rows="3" cols="80" id="metaDescription{$lang.id}" name="meta[description]">{$lang.metaInfo.description}</textarea>
						</li>*}
					</ul>   
					{include	file="common/submit_buttons.tpl"}
				</form>
			</div>
			{/foreach} 
		</div>    
			
{include file="footer.tpl"}