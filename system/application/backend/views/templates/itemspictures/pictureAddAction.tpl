{include file="header.tpl"}
		<h1>{$header}</h1>
		{if $subheader}<h2>{$subheader}</h2>{/if}

		<div class="tab-content">         
			<form action="{$link}/addnew" method="post" class="form-a" enctype="multipart/form-data">
				{if $errors}
				<h3 class="warning">{$errorHeader}</h3>
				<ul class="warning">
				{foreach from=$errors item=error}
					<li>{$error}</li>
				{/foreach}
				</ul>
				{/if}
			
				<ul class="form-items">
					{assign var=path value=$picturePath|cat:'/'|cat:$imageBackData.picture_thumb}
					{if $backData->picture AND $path|is_file}						
						{assign var=isFile value=true}
					{/if}
					
					<li>
						<input type="hidden" name="MAX_FILE_SIZE" value="1512000"/>
						<label for="thefile">{if $isFile}Change {else}Add {/if} picture <span>(.jpg)</span></label>
						<input type="file" id="thefile" name="thefile" size="47"/>
						<div class="text-box-a">
							<p>max 1500KB</p>
						</div>
						{if $isFile}
						<div class="text-box">
							<p><a href="{$picturePath}/{$backData->picture_thumb}" class="fancy"><img src="{$picturePath}/{$backData->picture}" alt=""/></a></p>
							<p><a href="newspictures/removePicture/{$backData->id}/1">Remove picture</a></p>
						</div>
						{/if}
					</li>		
					<li class="diff">
						<label for="position">Position:</label> <input type="text" name="position" id="position" value="{$backData.position}" class="short-input"/>                  
					</li>		
				</ul>     
				
				<h3 class="group-section">Image meta data:</h3>
				<ul class="form-items">					
					<li>
						<label for="metaTitle">Title:</label> <input type="text" name="meta[title]" id="metaTitle" value="{$backData.metaInfo.title}"/>                     
					</li>
					<li class="diff">
						<label class="type1" for="metaKeywords">Alt tag:</label> <textarea rows="3" cols="80" id="metaKeywords" name="meta[keywords]">{$backData.metaInfo.keywords}</textarea>            
					</li>
					{*<li>
						<label class="type1" for="metaDescription">Description:</label> <textarea rows="3" cols="80" id="metaDescription" name="meta[description]">{$backData.metaInfo.description}</textarea>            
					</li>*}                               
				</ul> 
				<p class="btn">					
					<button type="submit">
						Submit &gt;
					</button>
				</p>
			</form>         
		</div>         
{include file="footer.tpl"}