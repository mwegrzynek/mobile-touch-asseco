{include file="header.tpl"}
      {if $itemsTree}
			{if !$itemId}
			<h2>Choose gallery first</h2>
			{/if}
			{include file="common/menu_tree.tpl" scope=parent}
			<form action="{$link}/chooseitem" method="post" class="form-b">			
				<ul class="form-items">
					<li class="diff">					
						<label for="itemid">Choose gallery:</label>
						<select name="itemid" id="itemid" class="auto-submit">						
							{if !$itemId}
							<option value="0">--- Choose gallery ---</option>
							{/if}
							{if $itemsTree}
								{call menu data=$itemsTree currentItem=$itemId forbiddenItem=$forbiddenItem}
							{/if}
						</select>  
						{*<button type="submit">
							Wybierz &gt;
						</button>*}
					</li>
				</ul>				
			</form>
		
			{if $itemId}
				<h1>
					{if !$picturesList|count}
					There are no pictures for gallery <em><a href="items/{$itemMainName}/edit/{$itemId}">{$itemName}</a></em> <br/>
					<a href="{$link}/addnew">Add new</a>
					{else}
					All pictures ({$allPicturesCount}) for gallery <em><a href="items/{$itemMainName}/edit/{$itemId}">{$itemName}</a></em>
					{/if}
				</h1>
				{if $subheader}
				<h2>{$subheader}</h2>
				{/if}
				
				{if $picturesList}
				<div class="tools-left">
					{include file='perPage.tpl'}
					{include file='pagination.tpl'}
				</div>
				
				<ul class="image-list">
					{foreach from=$picturesList item=picture}
					<li>
						<a href="{$picturePath}/{$picture->file_name}" class="fancy"><img src="{$picturePath}/{$picture->file_name|thumb_name}" alt="" width="100"/></a>
						<p class="edit"><a href="{$link}/edit/{$picture->id}">Edit</a></p>
						<p class="delete" title="Usuń"><a href="{$link}/removeImage/{$picture->id}" class="del-opt" rel="dialog-pictures" title="'{$picture->file_name}'">Del</a></p>
					</li>	
					{/foreach}
				</ul>
				
				<div class="tools-left">
					{include file='pagination.tpl'}					
				</div>
				{*<div class="dialog-box" id="dialog-pictures" title="Delete picture?">
					<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
						<span style="display:block; padding-left: 25px;">Picture <em></em> will be deleted<br/> Are you sure?</span>
					</p>			
				</div>*}
				{/if}
			{/if}
		{else}
			<h1>There's no gallery. <a href="items/{$itemMainName}/addnew">Add some gallery first</a></h1>
		{/if}
		<div class="dialog-box" id="dialog-pictures" title="Delete picture?">
			<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
				<span style="display:block; padding-left: 25px;">Picture <em></em> will be deleted<br/> Are you sure?</span>
			</p>			
		</div>
{include file="footer.tpl"}
