<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="{$langcode}" lang="{$langcode}">
   <head>                                      
      <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
      <meta http-equiv="content-language" content="{$langcode}"/>
      <base href="{$baseurl}"/>
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.1/jquery.min.js"></script>
		<script type="text/javascript" src="javascript/logintools.js"></script>
      <meta http-equiv="content-style-type" content="text/css"/>      
      <link rel="stylesheet" media="all" href="styles/login.css" type="text/css"/>      
      <title>Mobile Touch - Admin Panel</title>      
   </head>
   <body>      
      <div id="root">
         <div id="content">            
            <div id="login-header">
               <h1><img src="images/logo.png" alt=""/></h1>          
            </div>
            <div id="login-container" {if $forgottenPass}class="hidden"{/if}>
					<form class="login-form" action="{$baseurl}auth/login" method="post">               
	               <div>
	                  {if $errors}
	                     <ul class="warnings">
	                        {foreach from=$errors item=error}
	                        <li>{$error}</li>
	                        {/foreach}
	                     </ul>                  
	                  {/if}
	                  <ul>
	                     <li>
	                        <label for="email">E-mail:</label>
	                        <input type="text" id="email" name="email" value=""/>
	                     </li>
	                     
	                     <li>
	                        <label for="password">Password:</label>
	                        <input type="password" id="password" name="password" value=""/>                        
	                     </li>
	                  </ul>
							{*<p class="type1">
								<input type="checkbox" id="remember" name="remember"/>
								<label for="rememberme">Zapamiętaj mnie na tym komputerze</label>
							</p>*}
	                  <p class="btn">                     
	                     <button type="submit">
									Login &gt;
								</button>
	                  </p>
	               </div>
	            </form>  
					<p class="link-container"><a href="#">Forgotten password?</a></p>
				</div>
				<div id="forgotten-pass" {if !$forgottenPass}class="hidden"{/if}>
					<form class="login-form" action="{$baseurl}auth/forgot_password" method="post"> 	               
						<div>
							<p>
								{if !$forgottenPassSuccess}
								Enter Your login/email. New password will be sent at given email.
								{else}
								New, temporary password has been sent. Please login and change Your temporary password as soon as You can
								{/if}
							</p>
	                  {if $errors1}
	                     <ul class="warnings">
	                        {foreach from=$errors1 item=error}
	                        <li>{$error}</li>
	                        {/foreach}
	                     </ul>                  
	                  {/if}
	                  <ul>
	                     <li>
	                        <label for="email1">E-mail:</label>
	                        <input type="text" id="email1" name="email" value=""/>
	                     </li>	
							</ul>
	                  <p class="btn">                     
	                     <button type="submit">
									Send &gt;
								</button>
	                  </p>
	               </div>
	            </form>  
					<p class="link-container"><a href="#">Back to login</a>.</p>
				</div>
         </div>
      </div>
   </body>
</html>