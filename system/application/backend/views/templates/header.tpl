<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="{$langcode}" lang="{$langcode}">
<head>
	<meta http-equiv="content-style-type" content="text/css" />
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<base href="{$baseurl}"/>
	<meta http-equiv="content-style-type" content="text/css"/>
	<link rel="stylesheet" media="all" href="styles/screen.css" type="text/css"/> 
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
	
	
<script type="text/javascript" src="plupload/js/plupload.js"></script>
<script type="text/javascript" src="plupload/js/plupload.gears.js"></script>
<script type="text/javascript" src="plupload/js/plupload.silverlight.js"></script>
<script type="text/javascript" src="plupload/js/plupload.flash.js"></script>
<script type="text/javascript" src="plupload/js/plupload.browserplus.js"></script>
<script type="text/javascript" src="plupload/js/plupload.html4.js"></script>
<script type="text/javascript" src="plupload/js/plupload.html5.js"></script>
<script type="text/javascript" src="plupload/js/jquery.plupload.queue/jquery.plupload.queue.js"></script>
<link rel="stylesheet" media="all" href="plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css" type="text/css"/> 

	
	{*<script type="text/javascript" src="javascript/swfupload/swfupload.queue.js"></script>
	<script type="text/javascript" src="javascript/swfupload/handlers.js"></script>*}
	<script type="text/javascript" src="tinymce/plugins/ezfilemanager/js/ez_tinyMCE.js"></script>
	<script type="text/javascript" src="tinymce/plugins/ezfilemanager/js/ez_plugin.js"></script>
	<script type="text/javascript" src="javascript/scripts.js"></script>
	
	

	{if $outerscripts}
		{foreach from=$outerscripts item=scriptItem}
		<script type="text/javascript" src="{$scriptItem.src}/{$scriptItem.name}"></script>
		{/foreach}
	{/if}
	
	{if $scripts}
		{foreach from=$scripts item=scriptItem}
		<script type="text/javascript" src="javascript/{$scriptItem}"></script>
		{/foreach}
	{/if}

	{if $css}
		{foreach from=$css item=cssItem}
		<link rel="stylesheet" media="all" href="styles/{$cssItem}" type="text/css"/>
		{/foreach}
	{/if}
	
	<!--[if lt IE 7]>
		<link rel="stylesheet" media="all" href="styles/ielt7.css" type="text/css"/>
	<![endif]-->
	<!--[if (lt IE 8)&(gte IE 7)]>         
		<link rel="stylesheet" media="all" href="styles/ielt8.css" type="text/css"/>
	<![endif]-->
	
	<title>Mobile Touch - Admin panel</title>
</head>
<body>
	<div id="top">
		<p id="logout-container">Hello {$userData.username}  <a href="auth/logout">[Log out]</a></p> 
		<p id="logo"><a href="{$baseurl}"><img src="images/logo1.png" alt=""/></a></p>                 
		<div id="nav">
			<ul>
				{foreach from=$mainNavig item=navigItem key=key name=navigloop}
				<li{if $navigItem.startSection} class="start-section"{/if}{if $navigItem.endSection} class="end-section"{/if}>
					<a href="{$navigItem.link}"{if $key eq $activeItem} class="current"{/if}><span {if $navigItem.submenu}class="has-dropdown"{/if}>{$navigItem.name}</span></a>
					{if $navigItem.submenu}
						<ul>
						{foreach from=$navigItem.submenu item=submenu}
						<li><a{if $submenu.class} class="{$submenu.class}"{/if} href="{$submenu.link}">{$submenu.name}</a></li>
						{/foreach}
						</ul>
					{/if}               
				</li>
				{/foreach}
			</ul>	
			<div class="clear"></div>
		</div>
	</div>
	{if $secondmenu}
	<div id="nav-secondary">
		<ul>
			{foreach from=$secondmenu item=menuitem key=key name=secondMenu}               
				<li{if $key eq 0} class="show"{elseif $key eq 1} class="add"{elseif $key eq 2 or $key eq 99} class="edit"{/if}><a href="{$menuitem.link}" {if $key eq $activeSecondItem} class="current"{/if}>{$menuitem.name|htmlspecialchars|truncate:70}</a></li>              
			{/foreach}            
		</ul>		
	</div>
	{/if}
	<div id="content">         