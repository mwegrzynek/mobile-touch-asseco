{include file="header.tpl"}
      {if $itemsTree}
      	{if !$itemId}			
      	<h2>Choose gallery</h2>
			{/if}
			{include file="common/menu_tree.tpl" scope=parent}
			<form action="{$link}/chooseitem" method="post" class="form-b">			
				<ul class="form-items">
					<li class="diff">					
						<label for="itemid">Choose gallery:</label>
						<select name="itemid" id="itemid" class="auto-submit">						
							{if !$itemId}
							<option value="0">--- Choose gallery ---</option>
							{/if}
							{if $itemsTree}
								{call menu data=$itemsTree currentItem=$itemId forbiddenItem=$forbiddenItem}
							{/if}
						</select>  
						{*<button type="submit">
							Wybierz &gt;
						</button>*}
					</li>
				</ul>				
			</form>
      
			{if $itemId}			
				<h1>
					{if !$videosList|count}
					There's no video for: <em><a href="items/{$itemMainName}/edit/{$itemId}">{$itemName}</a></em> <br/>
					<a href="{$link}/addnew">Add new</a>
					{else}
					Video for: ({$allVideosCount}) <em><a href="items/{$itemMainName}/edit/{$itemId}">{$itemName}</a></em>
					{/if}
				</h1>
				{if $subheader}
				<h2>{$subheader}</h2>
				{/if}
				
				{if $videosList}
				<div class="tools-left">
					{include file='perPage.tpl'}
					{include file='pagination.tpl'}
				</div>
				
				<ul class="image-list">
					{foreach from=$videosList item=video}
					
					{if $video->type_id eq 1}
						{* youtube *}
						{assign var=id value=$video->link|yt_id}
						{assign var=movielink value="http://www.youtube.com/v/$id"}
					{else}
						{* vimeo *}
						{assign var=movielink value=$video->link}
					{/if}
					<li>
						<a href="{$movielink}" title="{$video->title}" class="{if $video->type_id eq 1}fancy-video-yt{else}fancy-video-vm{/if}">
							{if $video->type_id eq 1}
								<img src="http://i3.ytimg.com/vi/{$video->link|yt_id}/default.jpg" width="100" height="83" alt=""/>								
							{else}
								<img src="{$video->thumbnail_url}" width="100" height="83" alt=""/>
							{/if}
							
						</a>
						<p class="edit"><a href="{$link}/edit/{$video->id}">Edit</a></p>
						<p class="delete" title="Usuń"><a href="{$link}/removeVideo/{$video->id}" class="del-opt" rel="dialog-video" title="'{$video->title}'">Del</a></p>
					</li>	
					{/foreach}
				</ul>
				
				<div class="tools-left">
					{include file='pagination.tpl'}					
				</div>				
				{/if}
			{/if}
		{else}
			<h1>There's no gallery <a href="items/{$itemMainName}/addnew">Add some gallery first</a></h1>
		{/if}
		<div class="dialog-box" id="dialog-video" title="Delete video?">
			<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
				<span style="display:block; padding-left: 25px;">Video <em></em> will be deleted<br/> Are you sure?</span>
			</p>			
		</div>
{include file="footer.tpl"}
