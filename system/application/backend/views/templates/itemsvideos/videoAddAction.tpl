{include file="header.tpl"}
		<h1>{$header}</h1>
		{if $subheader}<h2>{$subheader}</h2>{/if}

		<div class="tab-content">         
			<form action="{$link}/addnew" method="post" class="form-a" enctype="multipart/form-data">
				{if $errors}
				<h3 class="warning">{$errorHeader}</h3>
				<ul class="warning">
				{foreach from=$errors item=error}
					<li>{$error}</li>
				{/foreach}
				</ul>
				{/if}
			
				<ul class="form-items">					
					<li>						               
						<label for="link">Link to video <em>*</em>:</label> <input type="text" name="link" id="link" value="{$backData.link}"/>   
						<span>example.: http://www.youtube.com/watch?v=FBLbrJxGtro or http://vimeo.com/22382541</span>
					</li>					
					<li class="diff">
						<label for="position">Position:</label> <input type="text" name="position" id="position" value="{$backData.position|default:1}" class="short-input"/>                  
					</li>						
				</ul>     
				
				<h3 class="group-section">Movie description:</h3>
				<ul class="form-items">              
					<li>                    
						<label for="metaTitle">Title:</label> <input type="text" name="meta[title]" id="metaTitle" value="{$backData.metaInfo.title}"/>                     
						<span>If empty, title will be taken from YouTube or Vimeo.</span>
					</li>
					{*
					<li class="diff">
						<label class="type1" for="metaKeywords">Krótki opis:</label> <textarea rows="3" cols="80" id="metaKeywords" name="metaKeywords">{$backData.metaKeywords}</textarea>
					</li>
					<li>
						<label class="type1" for="metaDescription">Description:</label> <textarea rows="3" cols="80" id="metaDescription" name="metaDescription">{$backData.metaDescription}</textarea>            
					</li>
					*}
				</ul>    
				<p class="btn">					
					<button type="submit">
						Submit &gt;
					</button>
				</p>
			</form>         
		</div>         
{include file="footer.tpl"}