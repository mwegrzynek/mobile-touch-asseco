{include file="header.tpl"}
		<h1>{$header}</h1>
		{if $subheader}<h2 class="helper">{$subheader}</h2>{/if}             
	  
		{if $errors}
		<h3 class="warning">{$errorHeader}</h3>
		<ul class="warning">
		{foreach from=$errors item=error}
			<li>{$error}</li>
		{/foreach}
		</ul>
		{/if}
		
		<form action="{$link}/edit/{$backData.videoid}" method="post" class="form-b" enctype="multipart/form-data">			
			<ul class="form-items">
				
				{include file="common/menu_tree.tpl" scope=parent}
				<li class="diff">					
					<label for="itemid">Change gallery</label>
					<select name="itemid" id="itemid">
						{call menu data=$itemsTree currentItem=$itemId forbiddenItem=$forbiddenItem}						
					</select>					
				</li>
				
				<li>										               
					<label for="link">Link to video <em>*</em>:</label> <input type="text" name="link" id="link" value="{$backData.link}" class="wide-input-a"/> 	
					<span>example.: http://www.youtube.com/watch?v=FBLbrJxGtro or http://vimeo.com/22382541</span>
					<div class="text-box-a">
						{if $video->type_id eq 1}
							{* youtube *}
							{assign var=id value=$video->link|yt_id}
							{assign var=movielink value="http://www.youtube.com/v/$id"}
						{else}
							{* vimeo *}
							{assign var=movielink value=$video->link}
						{/if}						
						<p><a href="{$movielink}" title="{$video->title}" class="{if $video->type_id eq 1}fancy-video-yt{else}fancy-video-vm{/if}">Watch video</a></p>
					</div>					
				</li>			
				<li class="diff">
					<label for="position">Position:</label> <input type="text" name="position" id="position" value="{$backData.position}" class="short-input"/>                  
				</li>				
			</ul>
			<p class="btn">				
				<button type="submit">
					Submit &gt;
				</button>
			</p>
		</form>			
		
		{if $backData.languages|@count > 1}
		<div class="tabs-container">            
			<ul class="tabs">            
				{foreach from=$backData.languages item=lang}
				<li><a href="#{$lang.lang_code}"><span>{$lang.name}</span></a></li>
				{/foreach}                
			</ul>
		</div>      
		{/if}
		<div class="tab-content">                  
			{foreach from=$backData.languages item=lang name=langLoop key=key}
			<div id="{$lang.lang_code}" class="ui-tabs-panel">
				<form action="{$link}/edit/{$backData.videoid}#{$lang.lang_code}" method="post" class="form-a">      
					{if $subErrors[$lang.id]}
					<h3 class="warning">{$errorHeader[$lang.id]}</h3>
					<ul class="warning">
					{foreach from=$subErrors[$lang.id] item=error}
						<li>{$error}</li>
					{/foreach}
					</ul>
					{/if}                              
				  
					<ul class="form-items">
						<li>
							<label for="metaTitle{$lang.id}">Title:</label> <input type="text" name="meta[title]" id="metaTitle{$lang.id}" value="{$lang.metaInfo.title}"/>
						</li>
						{*<li class="diff">
							<label class="type1" for="metaKeywords{$lang.id}">Keywords:</label> <textarea rows="3" cols="80" id="metaKeywords{$lang.id}" name="meta[keywords]">{$lang.metaInfo.keywords}</textarea>
						</li>
						<li>
							<label class="type1" for="metaDescription{$lang.id}">Description:</label> <textarea rows="3" cols="80" id="metaDescription{$lang.id}" name="meta[description]">{$lang.metaInfo.description}</textarea>
						</li>*}
					</ul>
				  {include	file="common/submit_buttons.tpl"}
				</form>
			</div>
			{/foreach} 
		</div>
			
{include file="footer.tpl"}