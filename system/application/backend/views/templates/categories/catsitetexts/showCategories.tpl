{include file="header.tpl"}
      <h1>
         {if !$itemsList|@count}
         Nie znaleziono żadnych kategorii
         {else}
         Wszystkie kategorie ({$allItems})
         {/if}
      </h1>
      {if $subheader}
      <h2>{$subheader}</h2>
      {/if}
      
		
		<div id="filterBox">
			<form action="{$link}/filter" method="post">
				<p>
					<input type="text" id="filter" name="filter" value="{$filter.textSearchFilter}" title="Szukaj"/>
					<select name="parentFilter">
						<option value="-1">--Wszystkie kategorie--</option>
						{if $itemsTree}
							{foreach from=$itemsTree item=item key=key}                        
							{if $item->id eq $filter.parentFilter}
							<option value="{$item->id}" selected="selected" class="level-0">
							{else}
							<option value="{$item->id}" class="level-0">
							{/if}
								{$item->name}
							</option>
							{if $item->children}
								{include file="common/category_tree_recursion.tpl" element=$item->children level=1 currentItem=$filter.parentFilter}
							{/if}
							{/foreach}
						{/if}						
					</select>
				</p> 
				<p class="btn">
					<button type="submit">
						Szukaj &gt;
					</button>						
				</p>
			</form>             
		</div>
		
		
		
      {if $itemsList}		
		<div class="medium">
			{include file='pagination.tpl'}
         {include file='perPage.tpl'}
         <table class="item-table">
            <thead>
               <tr>
                  <th class="toleft">Nazwa</th>                  
                  <th class="toleft">Kategorie nadrzędne</th>                  
                  <th colspan="2">Opcje</th>
               </tr>               
            </thead>
            <tbody>
            {foreach from=$itemsList item=item}
               <tr{cycle values=' class="diff",'}>                  
                  <td class="toleft"><a href="{$link}/edit/{$item->category_id}">{$item->name}</a></td>                                                              
                  <td class="toleft">
                  	{if $item->parents}
								{foreach from=$item->parents item=parent key=key name=loop1}
									<a href="{$link}/edit/{$parent->id}">{$parent->name}</a> {if !$smarty.foreach.loop1.last}-> {/if}
								{/foreach}
							{/if}                  	
                  </td>                                                              
                  <td><a href="{$link}/edit/{$item->category_id}">Edit</a></td>                  
                  <td><a href="{$link}/removeCategory/{$item->category_id}" rel="dialog-category" class="del-opt">Delete</a></td>  
               </tr>               
            {/foreach}
            </tbody>
         </table>
			{include file='pagination.tpl'}
		</div>
      {/if}     
      <div class="dialog-box" id="dialog-category" title="Usunąć Kategorię?">
			<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
				<span style="display:block; padding-left: 25px;">Kategoria <em></em> Zostanie usunięta.<br/> Czy jesteś pewien?</span>
			</p>
			<p>W nastepnym kroku będziesz mógł wybrać gdzie przenieść elementy z tej kategorii.</p>
		</div> 
{include file="footer.tpl"}