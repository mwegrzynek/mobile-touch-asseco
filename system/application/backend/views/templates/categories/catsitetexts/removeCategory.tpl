{include file="header.tpl"}

		<h1>Wybierz kategorię do której będą przeniesione elementy z tej kategorii: <em>{$categoryName}</em></h1>
		<div id="tabContentContainer">         
			<form action="{$link}/removeCategory/{$categoryid}" method="post" id="stepForm">
				{if $errors}
				<h3 class="warning">{$errorHeader}</h3>
				<ul class="warning">
				{foreach from=$errors item=error}
					<li>{$error}</li>
				{/foreach}
				</ul>
				{/if}
			
				<ul class="form-items">                  
					{if !$articleCategory}       
					<li>                     
						<label for="parentid">Wybierz kategorię:</label> 
						<select name="parent_id" id="parentid">
						 	<option value="0">Root</option>
							{if $itemsTree}
								{foreach from=$itemsTree item=item key=key}                        
								{if $item->id neq $categoryid}
								<option value="{$item->id}" class="level-0">								
									{$item->name}
								</option>
								{/if}
								{if $item->children}
									{include file="common/category_tree_recursion.tpl" element=$item->children level=1 forbiddenItem=$categoryid}
								{/if}
								{/foreach}
							{/if}
						</select>                  
					</li>
					{/if}
				  </ul>
				  {*				  
					<ul class="form-items1">
						<li>
							<input type="checkbox" name="deleteAll" id="deleteAll"/> <label for="deleteAll">Or delete all posts in this category: <strong>{$categoryName}</strong></label>               
						</li>						
					</ul>
					*}
				<p class="btn">				
					<button type="submit">
						Usuń kategorię i zatwierdź powyższe opcje &gt;
					</button>
				</p>		
			</form>
		</div>
			
{include file="footer.tpl"}