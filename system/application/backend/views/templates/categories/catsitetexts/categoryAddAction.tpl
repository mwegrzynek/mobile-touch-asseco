{include file="header.tpl"}
		<h1>{$header}</h1>
		{if $subheader}<h2 class="helper">{$subheader}</h2>{/if}

		<div id="tabContentContainer">         
			<form action="{$link}/addnew" method="post" id="stepForm" enctype="multipart/form-data">
				{if $errors}
				<h3 class="warning">{$errorHeader}</h3>
				<ul class="warning">
				{foreach from=$errors item=error}
					<li>{$error}</li>
				{/foreach}
				</ul>
				{/if}
			
				<ul class="form-items">                
						 
					<li>                     
						<label for="parentid">Kategoria nadrzędna:</label> 
						<select name="parentid" id="parentid">
							<option value="0"{if !$backData.parentid} selected="selected"{/if}>Root</option>
							{if $itemsTree}
								{foreach from=$itemsTree item=item key=key}                        
									{if $item->id eq $backData.parentid}
									<option value="{$item->id}" selected="selected" class="level-0">
									{else}
									<option value="{$item->id}" class="level-0">
									{/if}
										{$item->name}
									</option>
									{if $item->children}
										{include file="common/category_tree_recursion.tpl" element=$item->children level=1 currentItem=$backData.parentid}
									{/if}
								{/foreach}
							{/if}				
						</select>	               
					</li>
					
					
					
					<li class="diff">
						<label for="name">Nazwa <em>*</em>:</label> <input type="text" name="name" id="name" value="{$backData.name}"/>     
						{*<input type="hidden" name="parentid" value="0" id="parentid"/>*}
						{*<input type="hidden" name="position" value="1" id="position"/>*}
					</li>
					
					<li class="offset">                     
						<label for="slug">Slug:</label> <input type="text" name="slug" id="slug" value="{$backData.slug}"/> <span>Jeśli pusty, generowany automatycznie</span>                  
					</li>
					
					{*<li class="diff">
						<input type="hidden" name="MAX_FILE_SIZE" value="524288"/>
						<label for="thefile">Obrazek <span>(.jpg|.png)</span></label>
						<input type="file" id="thefile" name="thefile" size="47"/>
					</li>
					
					<li>                     
						<label for="position">Pozycja w menu:</label> <input type="text" name="position" id="position" value="{$backData.content|default:1}" class="short-input"/> 
					</li>	   
					
					<li class="diff">
						<label for="description">Opis kategorii:</label>
						<textarea rows="12" cols="70" id="description" name="description" class="mceEditor">{$backData.description|htmlspecialchars}</textarea>						
					</li>	*}					
				</ul>  
				{*    
				<ul class="form-items1">
					<li>
						<input type="checkbox" {if $backData.isActive}checked="checked"{/if} name="isActive" id="isActive" value="1"/> <label for="isActive">Czy aktywna</label>               
					</li>
				</ul>   
				
				<h3 class="group-section">Meta Tagi</h3>
				<ul class="form-items">
					<li>                    
						<label for="metaTitle">Title:</label> <input type="text" name="metaTitle" id="metaTitle" value="{$backData.metaTitle}"/>                     
					</li>
					<li class="diff">
						<label class="type1" for="metaKeywords">Keywords:</label> <textarea rows="3" cols="80" id="metaKeywords" name="metaKeywords">{$backData.metaKeywords}</textarea>            
					</li>
					<li>
						<label class="type1" for="metaDescription">Description:</label> <textarea rows="3" cols="80" id="metaDescription" name="metaDescription">{$backData.metaDescription}</textarea>            
					</li>                               
				</ul>  *}  
				<p class="btn">					
					<button type="submit">
						Dodaj &gt;
					</button>
				</p>
			</form>         
		</div>
			
{include file="footer.tpl"}