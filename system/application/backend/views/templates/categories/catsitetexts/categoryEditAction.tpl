{include file="header.tpl"}
		<h1>{$header}: <em>{$backData.categoryName}</em></h1>
		{if $subheader}<h2 class="helper">{$subheader}</h2>{/if}
		
		<form action="{$link}/edit/{$backData.categoryid}" method="post" class="form-a" enctype="multipart/form-data">
			{if $errors}
			<h3 class="warning">{$errorHeader}</h3>
			<ul class="warning">
			{foreach from=$errors item=error}
				<li>{$error}</li>
			{/foreach}
			</ul>
			{/if}

			{if !$noImages}
				{assign var=path value=$picturePath|cat:'/'|cat:$imageBackData.main_image}
				{if $path|is_file}
					{assign var=isFile value=true}
					<div id="pictureContainer">
						<a href="{$path}" class="fancy"><img src="{$path}" alt="" width="175"/></a>
						<p class="imagesubtitle">
							<a href="{$link}/removeImage/{$backData.categoryid}">Usuń obrazek</a>
						</p>
					</div>
				{/if}
			{/if}
			<div{if $isFile} class="withImage"{/if} id="independentSection">
				<ul class="form-items">
					<li>
						<label for="parentid">Parent category:</label>
						<select name="parentid" id="parentid">
							<option value="0"{if !$backData.parentid} selected="selected"{/if}>Root</option>
							{if $itemsTree}								
								{include file="common/menu_tree.tpl" scope=parent}
								{call menu data=$itemsTree currentItem=$backData.parentid forbiddenItem=$forbiddenItem}
							{/if}				
						</select>
					</li>
					{*<li class="diff">
						<input type="hidden" name="MAX_FILE_SIZE" value="524288"/>
						<label for="thefile">Obrazek <span>(.jpg|.png)</span></label>
						<input type="file" id="thefile" name="thefile" size="47"/>						          
					</li>	*}			
				</ul>				
				<p class="btn">
					<button type="submit">
						Change data &gt;
					</button>
	         </p>
			</div>
		</form>	

		<h2>Główne dane:</h2>
		{if $backData.languages|@count > 1}
		<div class="tabs-container">            
			<ul class="tabs">            
				{foreach from=$backData.languages item=lang}
				<li><a href="#{$lang.lang_code}"><span>{$lang.name}</span></a></li>
				{/foreach}                
			</ul>		 
		{/if}	

		<div class="tab-content">
			{foreach from=$backData.languages item=lang name=langLoop key=key}
			<div id="{$lang.lang_code}" class="ui-tabs-panel">
				<form action="{$link}/edit/{$backData.categoryid}#{$lang.lang_code}" method="post" class="stepForm">
					{if $subErrors[$lang.id]}
					<h3 class="warning">{$errorHeader[$lang.id]}</h3>
					<ul class="warning">
					{foreach from=$subErrors[$lang.id] item=error}
						<li>{$error}</li>
					{/foreach}
					</ul>
					{/if}					
					<ul class="form-items">
						<li>
							<label for="name{$lang.id}">Name <em>*</em>:</label> 
							<input type="text" name="name" id="name{$lang.id}" value="{$lang.categoryInfo.name}"/>
							{*<input type="hidden" name="slug" value="{$lang.categoryInfo.slug}" id="slug{$lang.id}"/>*}
							{*<input type="hidden" name="position" value="{$lang.categoryInfo.position}" id="position{$lang.id}"/>*}
						</li>
						<li class="diff">
							<label for="slug{$lang.id}">Slug:</label> <input type="text" name="slug" id="slug{$lang.id}" value="{$lang.categoryInfo.slug}"/> <span>If empty, generated automatically.</span>							
						</li>
						
						<li>
							<label for="position{$lang.id}">Position:</label> <input type="text" name="position" id="position{$lang.id}" value="{$lang.categoryInfo.position}" class="short-input"/>							
						</li>						
						
					{*	<li>
							<label for="position{$lang.id}">Pozycja w menu:</label> <input type="text" name="position" id="position{$lang.id}" value="{$lang.categoryInfo.position}" class="short-input"/>							
						</li>
						
						<li class="diff">
	                  <label for="description{$lang.id}">Opis kategorii:</label>
	                  <textarea rows="12" cols="70" id="description{$lang.id}" name="description" class="mceEditor">{$lang.categoryInfo.description|htmlspecialchars}</textarea>
	               </li>*}
					</ul>
					{*<ul class="form-items1">
						<li>
							<input type="checkbox" {if $lang.categoryInfo.isActive}checked="checked"{/if} name="isActive" id="isActive{$lang.id}" value="1"/> <label for="isActive{$lang.id}">Czy aktywna?</label>               
						</li>
					</ul>				

					<h3 class="group-section">Meta Tagi</h3>
					<ul class="form-items">
						<li>
							<label for="metaTitle{$lang.id}">Title:</label> <input type="text" name="metaTitle" id="metaTitle{$lang.id}" value="{$lang.metaInfo.title}"/>
						</li>
						<li class="diff">
							<label class="type1" for="metaKeywords{$lang.id}">Keywords:</label> <textarea rows="3" cols="80" id="metaKeywords{$lang.id}" name="metaKeywords">{$lang.metaInfo.keywords}</textarea>
						</li>
						<li>
							<label class="type1" for="metaDescription{$lang.id}">Description:</label> <textarea rows="3" cols="80" id="metaDescription{$lang.id}" name="metaDescription">{$lang.metaInfo.description}</textarea>
						</li>
					</ul>*}
					<p class="btn">
                  <input type="hidden" name="langid" value="{$lang.id}"/>
                  <input type="hidden" name="next" value="{$lang.next_lang_code}"/>					
						
						<button type="submit">
							Zmień &gt;
						</button>
						
						{if $backData.languages|@count > 1}
						<button class="btn-a" type="submit" name="submit-and-next" value="1">
							Submit and edit next language &gt;
						</button>     
						{/if}
               </p>
				</form>
			</div>
			{/foreach}
		</div>

{include file="footer.tpl"}