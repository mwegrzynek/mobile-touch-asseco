{include file="header.tpl"}
      <h1>
         {if !$itemsList|@count}
         No industry found
         {else}
         All industries ({$allItems})
         {/if}
      </h1>
      {if $subheader}
      <h2>{$subheader}</h2>
      {/if}
      
		
		<div id="filterBox">
			<form action="{$link}/filter" method="post">
				<p>
					<input type="text" id="filter" name="filter" value="{$filter.textSearchFilter}" title="Search"/>
					{*<select name="parentFilter">
						<option value="-1">-- All trades --</option>						
						{if $itemsTree}
							{include file="common/menu_tree.tpl" scope=parent}
							{call menu data=$itemsTree currentItem=$filter.parentFilter forbiddenItem=$forbiddenItem}
						{/if}						
					</select>*}
				</p> 
				<p class="btn">
					<button type="submit">
						Search &gt;
					</button>						
				</p>
			</form>             
		</div>
		
		
		
      {if $itemsList}		
		<div class="medium">
			{include file='pagination.tpl'}
         {include file='perPage.tpl'}
         <table class="item-table">
            <thead>
               <tr>
                  <th class="toleft">ID</th>                  
                  <th class="toleft">Name</th>                  
                  <th class="toleft">Is default</th>
                  <th colspan="2">Options</th>
               </tr>               
            </thead>
            <tbody>
            {foreach from=$itemsList item=item}
               <tr{cycle values=' class="diff",'}>                  
                  <td class="toleft">{$item->id}</td>                                                              
                  <td class="toleft"><a href="{$link}/edit/{$item->category_id}">{$item->name}</a></td>                                                              
                  {*<td class="toleft">
                  	{if $item->parents}
								{foreach from=$item->parents item=parent key=key name=loop1}
									<a href="{$link}/edit/{$parent->id}">{$parent->name}</a> {if !$smarty.foreach.loop1.last}-> {/if}
								{/foreach}
							{/if}                  								
                  </td>*} 
                  <td>{if $item->is_default}Yes{else}No{/if}</td>                                                            
                  <td><a href="{$link}/edit/{$item->category_id}">Edit</a></td>                  
                  <td><a rel="dialog-category" href="{$link}/removeCategory/{$item->category_id}" class="del-opt">Delete</a></td>  
               </tr>               
            {/foreach}
            </tbody>
         </table>
			{include file='pagination.tpl'}
		</div>
      {/if}     
      <div class="dialog-box" id="dialog-category" title="Delete category?">
			<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
				<span style="display:block; padding-left: 25px;">Industry <em></em> will be deleted<br/> Are You sure?</span>
			</p>
			<p>In next step You can move dependent elements.</p>
		</div> 
{include file="footer.tpl"}