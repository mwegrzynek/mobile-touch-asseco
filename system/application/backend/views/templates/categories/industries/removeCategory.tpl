{include file="header.tpl"}

		<h1>Choose category where all categories from current category will be moved: <em>{$categoryName}</em></h1>
		<div id="tabContentContainer">         
			<form action="{$link}/removeCategory/{$categoryid}" method="post" id="stepForm">
				{if $errors}
				<h3 class="warning">{$errorHeader}</h3>
				<ul class="warning">
				{foreach from=$errors item=error}
					<li>{$error}</li>
				{/foreach}
				</ul>
				{/if}
			
				<ul class="form-items">                  
					{if !$articleCategory}       
					<li>                     
						<label for="parent_id">Choose category:</label> 
						<select name="parent_id" id="parent_id">
						 	<option value="0">Root</option>
							{if $itemsTree}								
								{include file="common/menu_tree.tpl" scope=parent}
								{call menu data=$itemsTree currentItem=$filter.parentFilter forbiddenItem=$categoryid}
							{/if}
						</select>                  
					</li>
					{/if}
				  </ul>
				  {*				  
					<ul class="form-items1">
						<li>
							<input type="checkbox" name="deleteAll" id="deleteAll"/> <label for="deleteAll">Or delete all posts in this category: <strong>{$categoryName}</strong></label>               
						</li>						
					</ul>
					*}
				<p class="btn">				
					<button type="submit">
						Delete category and confirm this options &gt;
					</button>
				</p>		
			</form>
		</div>
			
{include file="footer.tpl"}