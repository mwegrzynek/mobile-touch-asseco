{include file="header.tpl"}
		<h1>{$header}: <em>{$backData.categoryName}</em></h1>
		{if $subheader}<h2 class="helper">{$subheader}</h2>{/if}
		
		<form action="{$link}/edit/{$backData.categoryid}" method="post" class="form-a" enctype="multipart/form-data">
			{if $errors}
			<h3 class="warning">{$errorHeader}</h3>
			<ul class="warning">
			{foreach from=$errors item=error}
				<li>{$error}</li>
			{/foreach}
			</ul>
			{/if}
			
			<div{if $isFile} class="withImage3"{/if} id="independentSection">				
				<ul class="form-items1">
					<li>
						<input type="checkbox" {if $backData.is_default}checked="checked"{/if} name="is_default" id="is_default" value="1"/> <label for="is_default">Is default?</label> 
						<input type="hidden" name="parent_id" value="0"/>              
					</li>					
				</ul>				
				<p class="btn">
					<button type="submit">
						Change data &gt;
					</button>
	         </p>
			</div>
		</form>

		<h2>Main data:</h2>
		{if $backData.languages|@count > 1}
		<div class="tabs-container">            
			<ul class="tabs">            
				{foreach from=$backData.languages item=lang}
				<li><a href="#{$lang.lang_code}"><span>{$lang.name}</span></a></li>
				{/foreach}                
			</ul>		 
		{/if}
		<div class="tab-content">
			{foreach from=$backData.languages item=lang name=langLoop key=key}
			<div id="{$lang.lang_code}" class="ui-tabs-panel">
				<form action="{$link}/edit/{$backData.categoryid}#{$lang.lang_code}" method="post" class="stepForm">
					{if $subErrors[$lang.id]}
					<h3 class="warning">{$errorHeader[$lang.id]}</h3>
					<ul class="warning">
					{foreach from=$subErrors[$lang.id] item=error}
						<li>{$error}</li>
					{/foreach}
					</ul>
					{/if}					
					<ul class="form-items">
						<li>
							<label for="name{$lang.id}">Name <em>*</em>:</label> 
							<input type="text" name="name" id="name{$lang.id}" value="{$lang.categoryInfo.name}"/>
							{*<input type="hidden" name="slug" value="{$lang.categoryInfo.slug}" id="slug{$lang.id}"/>*}
							{*<input type="hidden" name="position" value="{$lang.categoryInfo.position}" id="position{$lang.id}"/>*}
						</li>
						<li class="diff">
							<label for="slug{$lang.id}">Slug:</label> <input type="text" name="slug" id="slug{$lang.id}" value="{$lang.categoryInfo.slug}"/> <span>If empty, generated automatically.</span>							
						</li>
						
						<li>
							<label for="position{$lang.id}">Position:</label> <input type="text" name="position" id="position{$lang.id}" value="{$lang.categoryInfo.position}" class="short-input"/>							
						</li>						
						
						<li class="diff">
	                  <label for="description{$lang.id}">Description:</label>
	                  <textarea rows="12" cols="70" id="description{$lang.id}" name="description">{$lang.categoryInfo.description|htmlspecialchars}</textarea>
	               </li>
					</ul>
					<ul class="form-items1">
						<li>
							<input type="checkbox" {if $lang.categoryInfo.is_active}checked="checked"{/if} name="is_active" id="is_active{$lang.id}" value="1"/> <label for="is_active{$lang.id}">Is active?</label>               
						</li>						
					</ul>						
					{include	file="common/regular_meta.tpl"}
					
					{include	file="common/submit_buttons.tpl"}
				</form>
			</div>
			{/foreach}
		</div>
	{if $backData.languages|@count > 1}</div>{/if}
{include file="footer.tpl"}