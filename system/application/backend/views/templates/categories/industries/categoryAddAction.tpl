{include file="header.tpl"}
		<h1>{$header}</h1>
		{if $subheader}<h2 class="helper">{$subheader}</h2>{/if}

		<div id="tabContentContainer">         
			<form action="{$link}/addnew" method="post" id="stepForm" enctype="multipart/form-data">
				{if $errors}
				<h3 class="warning">{$errorHeader}</h3>
				<ul class="warning">
				{foreach from=$errors item=error}
					<li>{$error}</li>
				{/foreach}
				</ul>
				{/if}
			
				<ul class="form-items">                
						 
					{*<li>                     
						<label for="parentid">Kategoria nadrzędna:</label> 
						<select name="parent_id" id="parent_id">
							<option value="0"{if !$backData.parent_id} selected="selected"{/if}>Root</option>
							{if $itemsTree}
								{include file="common/menu_tree.tpl" scope=parent}								
								{call menu data=$itemsTree currentItem=$backData.categoryid forbiddenItem=$forbiddenItem}
							{/if}				
						</select>	               
					</li>*}
					
					<li class="diff">
						<label for="name">Nazwa <em>*</em>:</label> <input type="text" name="name" id="name" value="{$backData.name}"/>     
						<input type="hidden" name="parentid" value="0" id="parentid"/>
						{*<input type="hidden" name="position" value="1" id="position"/>*}
					</li>
					
					<li>                     
						<label for="slug">Slug:</label> <input type="text" name="slug" id="slug" value="{$backData.slug}"/> {include file="common/slug_info.tpl"}        
					</li>
					
					{*<li class="diff">
						<input type="hidden" name="MAX_FILE_SIZE" value="524288"/>
						<label for="thefile">Obrazek <span>(.jpg|.png)</span></label>
						<input type="file" id="thefile" name="thefile" size="47"/>
					</li>
					<li class="diff">
						<span><strong>Obrazek:</strong> Rozmiar 314x160px, max 512KB</span>
					</li>	*}
					
					<li>                     
						<label for="position">Position:</label> <input type="text" name="position" id="position" value="{$backData.content|default:1}" class="short-input"/> 
					</li>	     
					
					<li class="diff">
						<label for="description">Description:</label>
						<textarea rows="12" cols="70" id="description" name="description">{$backData.description|htmlspecialchars}</textarea>						
					</li>						
				</ul>      
				<ul class="form-items1">
					<li>
						<input type="checkbox" {if $backData.is_active}checked="checked"{/if} name="is_active" id="is_active" value="1"/> <label for="is_active">Is active</label>               
					</li>
					<li>
						<input type="checkbox" {if $backData.is_default}checked="checked"{/if} name="is_default" id="is_default" value="1"/> <label for="is_default">Is default</label>               
					</li>
				</ul>     
				
				{include	file="common/add_regular_meta.tpl"}
				{include file="common/add_buttons.tpl"}
			</form>         
		</div>
			
{include file="footer.tpl"}