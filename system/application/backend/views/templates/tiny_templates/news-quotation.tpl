{include file="tiny_templates/template-header.tpl"}
<section class="box-m mceTmpl">
	<article class="art-h">
		<blockquote>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
		</blockquote>
		<p class="cite"><cite>John Doe</cite> Company Position, Company Name</p>
	</article>	
</section><!-- .box-m -->
{include file="tiny_templates/template-footer.tpl"}