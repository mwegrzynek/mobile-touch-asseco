{include file="tiny_templates/template-header.tpl"}
	<section class="box-m format-c mceTmpl">
		<div class="cols-two-d">
			<h1 class="header-h hh-b">Sample video</h1>
			<div class="primary-td">
				<article class="text-area-a">
					<h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				</article>		
			</div>
			<div class="secondary-td">
				<p class="video-a va-a">
					<a class="fancybox-media" href="http://www.youtube.com/watch?v=8cbTGrmyi7k"> 
						<img src="http://{$smarty.server.HTTP_HOST}/images/templates/video-1.png" alt="" width="220" height="124" /> <span class="subtitle">Mobile Touch</span>					
					</a>				
				</p>
			</div>
		</div><!-- .cols-two-d -->	
	</section><!-- .box-m -->
{include file="tiny_templates/template-footer.tpl"}
