{include file="tiny_templates/template-header.tpl"}
<section class="box-w mceTmpl">
	<h2 class="header-k">Media Contact Details:</h2>
	<div class="cols-two-j">
		<div class="primary-tj">
			<article class="art-i">
				<h2>Agata Mierzwińska-Kurto</h2>
				<p>Asseco Business Solutions S.A.</p>
				<p>phone: 697 078 721</p>
				<p>e-mail: <a href="mailto:Agata.Kurto@assecobs.pl">Agata.Kurto@assecobs.pl</a></p>
			</article><!-- .art-h -->			
		</div>
		<div class="secondary-tj">
			<article class="art-i">
				<h2>Bartłomiej Pasternak</h2>
				<p>ITBC Communication</p>
				<p>phone 22 250 49 22, 608 678 654</p>
				<p>e-mail: <a href="mailto:Bartlomiej_Pasternak@itbc.pl">Bartlomiej_Pasternak@itbc.pl</a></p>
			</article><!-- .art-h -->			
		</div>
	</div><!-- .cols-two-j -->
</section><!-- .box-w -->
{include file="tiny_templates/template-footer.tpl"}