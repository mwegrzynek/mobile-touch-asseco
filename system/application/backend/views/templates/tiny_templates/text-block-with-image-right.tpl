{include file="tiny_templates/template-header.tpl"}
<div class="format-a format-d mceTmpl">
	<div class="cols-two-e">
		<div class="primary-te">
			<h1 class="header-h hh-b">Sample header</h1>
			<article class="text-area-a">
				<h1>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			</article>			
		</div>
		<div class="secondary-te">
			<p class="image"><img src="http://{$smarty.server.HTTP_HOST}/images/templates/sample-image-1.png" alt="" width="340" /></p>
		</div>
	</div><!-- .cols-two-e -->
</div>
{include file="tiny_templates/template-footer.tpl"}