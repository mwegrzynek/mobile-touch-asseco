<h3 class="group-section">Meta Tags</h3>
<ul class="form-items">
	<li>
		<label for="metaTitle{$lang.id}">Title:</label> <input type="text" name="meta[title]" id="metaTitle{$lang.id}" value="{$lang.metaInfo.title}"/>
	</li>
	<li class="diff">
		<label class="type1" for="metaKeywords{$lang.id}">Keywords:</label> <textarea rows="3" cols="80" id="metaKeywords{$lang.id}" name="meta[keywords]">{$lang.metaInfo.keywords}</textarea>
	</li>
	<li>
		<label class="type1" for="metaDescription{$lang.id}">Description:</label> <textarea rows="3" cols="80" id="metaDescription{$lang.id}" name="meta[description]">{$lang.metaInfo.description}</textarea>
	</li>
	{*<li class="diff">
		<label class="type1" for="metaOgTitle{$lang.id}">Facebook Title:</label> <textarea rows="3" cols="80" id="metaOgTitle{$lang.id}" name="meta[og_title]">{$lang.metaInfo.og_title}</textarea>
	</li>*}
</ul>