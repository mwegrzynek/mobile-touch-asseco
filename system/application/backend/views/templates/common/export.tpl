{include file="header.tpl"}
	<h1>Export items: {$config.showName}</h1>

	<div class="tab-content">
		<form action="{$config.mainLink}/export" method="post" class="form-a">
			{if $errors}
			<h3 class="warning">{$errorHeader}</h3>
			<ul class="warning">
			{foreach from=$errors item=error}
				<li>{$error}</li>
			{/foreach}
			</ul>
			{/if}

			<ul class="form-items">
				<li>
					<label for="language">Language <em>*</em>:</label>
					<select name="language" id="language">
						{foreach $languages as $item}
							<option value="{$item->id}">{$item->name}</option>
						{/foreach}
					</select>
				</li>
			</ul>

			<p class="btn">
				<button type="submit">
					Export &gt;
				</button>
			</p>
		</form>
	</div>
{include file="footer.tpl"}