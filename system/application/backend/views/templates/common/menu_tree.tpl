{function menu level=0 currentItem=-1 forbiddenItem=-1}
	{foreach $data as $entry}
		{if $entry->id neq $forbiddenItem}					
			{if isset($entry->children)}		
				<option value="{$entry->id}" {if $entry->id eq $currentItem}selected="selected"{/if} class="level-{$level}">{str_repeat string="-" count=$level} {if !empty($entry->universal_id)}{$entry->universal_id} [{{$entry->title}}]{elseif isset($entry->title)}{$entry->title}{else}{$entry->name}{/if}</option>					     
				{menu data=$entry->children level=$level+1 currentItem=$currentItem forbiddenItem=$forbiddenItem}
			{else}
				<option value="{$entry->id}" {if $entry->id eq $currentItem}selected="selected"{/if} class="level-{$level}">{str_repeat string="-" count=$level} {if !empty($entry->universal_id)}{$entry->universal_id} [{{$entry->title}}]{elseif isset($entry->title)}{$entry->title}{else}{$entry->name}{/if}</option>
			{/if}
		{/if} 	
	{/foreach}
{/function}