<p class="text-b">
	<span> [</span>
	<select name="user-native-lang" id="user-native-lang-{$id}" class="medium-input">
		{foreach from=$backData.languages item=sublang key=key}
		<option value="{$sublang.lang_code|strtolower}"{if $sublang.id eq $userLang} selected="selected"{/if}>{$sublang.lang_code}</option>									
		{/foreach}			
	</select>
	<span>to</span>
	<select name="translation-lang" id="translation-lang-{$id}" class="medium-input">
		{foreach from=$backData.languages item=sublang key=key}
		<option value="{$sublang.lang_code|strtolower}"{if	$sublang.id eq $currentLang} selected="selected"{/if}>{$sublang.lang_code}</option>									
		{/foreach}			
	</select>
	<a href="#" class="translate-button" data-field-id="{$field_id}{$currentLang}"><span> Translate now</span></a>
	<span>]</span>
</p>