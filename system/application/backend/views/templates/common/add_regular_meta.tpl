<h3 class="group-section">Meta Tags</h3>
<ul class="form-items">
	<li>
		<label for="metaTitle">Title:</label> <input type="text" name="meta[title]" id="metaTitle" value="{$backData.metaInfo.title}"/>                     
	</li>
	<li class="diff">
		<label class="type1" for="metaKeywords">Keywords:</label> <textarea rows="3" cols="80" id="metaKeywords" name="meta[keywords]">{$backData.metaInfo.keywords}</textarea>            
	</li>
	<li>
		<label class="type1" for="metaDescription">Description:</label> <textarea rows="3" cols="80" id="metaDescription" name="meta[description]">{$backData.metaInfo.description}</textarea>            
	</li>                               
</ul>    