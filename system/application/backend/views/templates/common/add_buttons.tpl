<p class="btn">					
	<button type="submit">
		Add &gt;
	</button>
	
	{if $languages|@count > 1}
	<input type="hidden" name="next" value="{$languages[1]->lang_code}"/>
	<button class="btn-a" type="submit" name="submit-and-edit-next" value="1">
		Add and edit next language &gt;
	</button>
	
	<button class="btn-a" type="submit" name="submit-and-fill-langs" value="1">
		Add to this and all other languages &gt;
	</button>
	{/if}
</p>