{foreach from=$element item="item" name="treeloop"}
   {if $item->id neq $forbiddenItem}
		{if $item->id eq $currentItem}
		<option value="{$item->id}" selected="selected" class="level-{$level}">
		{else}
		<option value="{$item->id}" class="level-{$level}">
		{/if}
			{if $smarty.foreach.treeloop.last}			
				{str_repeat string="-" count=$level}
			{else}	
				{str_repeat string="-" count=$level}
			{/if}
			{$item->title}
		</option>
	{/if}
	{if $item->children}
		{include file="common/items_tree_recursion.tpl" element=$item->children level=$level+1 currentItem=$currentItem forbiddenItem=$forbiddenItem}	
	{/if}
{/foreach}