{include file="header.tpl"}
		<h1>
			{if !$itemsList|@count}
			No contacts found
			{else}
			{if $filter}Found{else}All{/if} contacts: ({$allItems})
			{/if}
		</h1>
		{if $subheader}
		<h2>{$subheader}</h2>
		{/if}
			<div id="filterBox">
				<form action="contacts/filter" method="post">
					<p class="field-a">
						<input type="text" id="filter" name="filter" value="{$filter.textSearchFilter}" title="Search in mail, client name" />
						<input type="date" name="filterDateFrom" value="{$filter.filterDateFrom}" />
						<input type="date" name="filterDateTo" value="{$filter.filterDateTo}" />
						<select name="categoryFilter" id="categoryFilter">
							<option value="">-- All countries --</option>
							{if $countries}
								{foreach $countries as $country}
									{if $country->id eq $filter.categoryFilter}
									<option value="{$country->id}" selected="selected">
									{else}
									<option value="{$country->id}">
									{/if}
										{$country->title}
									</option>
								{/foreach}
							{/if}
						</select>
					</p>
					<p class="btn">
						<button type="submit">
							Search
						</button>
					</p>
				</form>
			</div>
		{if $itemsList}
			{include file='pagination.tpl'}
			{include file='perPage.tpl'}
			<form action="contacts/removeSelected" method="post" id="item-form">
				<table class="item-table">
					<thead>
						<tr>
							<th class="toleft">&nbsp;</th>
							<th class="toleft">Name and last name</th>
							<th class="toleft">Add date</th>
							<th class="toleft">Mail</th>
							<th class="toleft">Phone</th>
							<th class="toleft">Company</th>
							<th class="toleft">Country</th>
							<th class="toleft">Type</th>
							<th colspan="2">Options</th>
						</tr>
					</thead>
					<tbody>
					{foreach from=$itemsList item=item}
						<tr{cycle values=' class="diff",'}>
							{assign var=country_id value=$item->country}
							<td><input type="checkbox" name="id[]" value="{$item->id}" id="id-{$item->id}"/></td>
							<td class="toleft">{$item->client_name}</td>
							<td class="toleft">{$item->add_date}</td>
							<td class="toleft">{$item->email}</td>
							<td class="toleft">{$item->phone}</td>
							<td class="toleft">{$item->company}</td>
							<td class="toleft">{$countries[$country_id]->title|default:"N\A"}</td>
							<td class="toleft">{if $item->type eq 1}Gartner Form{else}Regular{/if}</td>

							<td><a href="{$link}/showfull/{$item->id}" title="Show all Info">Show contact details</a></td>
							<td><a rel="dialog-contact" href="{$link}/removeContact/{$item->id}{if $offset}/{$offset}{/if}" class="del-opt">Delete</a></td>
						</tr>
					{/foreach}
					</tbody>
				</table>
				<p class="btn">
					<button type="submit" class="del-opt" rel="dialog-contact">Remove selected</button>
					<a href="#item-form" id="mark-link">mark / unmark all</a>
				</p>
			</form>
			{include file='pagination.tpl'}
			<div class="dialog-box" id="dialog-contact" title="Delete contact?">
				<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
					<span style="display:block; padding-left: 25px;">Contact <em></em> will be deleted.<br/> Are You sure?</span>
				</p>
			</div>
		{/if}
{include file="footer.tpl"}