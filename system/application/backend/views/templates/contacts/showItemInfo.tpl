{include file="header.tpl"}
		<h1>{$header}{if $subheader} -&gt; <em>{$subheader}</em>{/if}</h1>
		<div class="tabs-container">
			<ul class="tabs">
				<li><a id="t1" href="#mainDataSection"><span>Main data</span></a></li>				
			</ul>
			<div class="tab-content">
				<div id="mainDataSection">
					<form action="comment/showfull/{$backData->id}" method="post" class="stepForm" enctype="multipart/form-data">
						{if $errors}
						<h3 class="warning">{$errorHeader}</h3>
						<ul class="warning">
						{foreach from=$errors item=error}
							<li>{$error}</li>
						{/foreach}
						</ul>
						{/if}
						<ul class="form-items f-items-a f-items-b">
							<li>
								<strong class="label">First and last name:</strong> <span class="content">{$backData->client_name}</span>
							</li>
							<li>
								<strong class="label"><a href="comment/removeContact/{$backData->id}" rel="dialog-contact" class="del-opt">Delete contact</a></strong>
							</li>											
							<li class="diff">
								<strong class="label">Mail:</strong> <span class="content">{$backData->email}</span>
							</li>							
							<li>
								<strong class="label">Phone:</strong> <span class="content">{$backData->phone}</span>
							</li>
							<li class="diff">
								<strong class="label">Add date:</strong> <span class="content">{$backData->add_date}</span>
							</li>
							<li>
								<strong class="label">Country:</strong> <span class="content">{$backData->country}</span>
							</li>
							<li class="diff">
								<strong class="label">Message:</strong> <span class="content">{$backData->message|nl2br}</span>
							</li>
						</ul>						
					</form>
				</div>				
		</div><!--end tab container-->
		<div class="dialog-box" id="dialog-contact" title="Delete contact?">
			<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
				<span style="display:block; padding-left: 25px;">Contact <em></em> will be deleted.<br/> Are You sure?</span>
			</p>
		</div>
{include file="footer.tpl"}