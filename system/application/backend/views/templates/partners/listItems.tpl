{include file="header.tpl"}
		<h1>
			{if !$itemsList|@count}
			No partners found
			{else}
			{if $filter}Found{else}All{/if} partners: ({$allItems})
			{/if}
		</h1>
		{if $subheader}
		<h2>{$subheader}</h2>
		{/if}
			<div id="filterBox">
				<form action="partners/filter" method="post">
					<p class="field-a">
						<input type="text" id="filter" name="filter" value="{$filter.textSearchFilter}" title="Search in mail, client name" />						
						<select name="categoryFilter" id="categoryFilter">
							<option value="">-- All languages --</option>
							{if $languages}
								{foreach $languages as $lang}
									{if $lang->id eq $filter.categoryFilter}
									<option value="{$lang->id}" selected="selected">
									{else}
									<option value="{$lang->id}">
									{/if}
										{$lang->name}
									</option>
								{/foreach}
							{/if}
						</select>
					</p>					
					<p class="btn">
						<button type="submit">
							Search
						</button>
					</p>				
				</form>        
			</div>
		{if $itemsList}
			{include file='pagination.tpl'}
			{include file='perPage.tpl'}			
				<table class="item-table">
					<thead>
						<tr>							
							<th class="toleft">Name</th>
							<th class="toleft">Country name</th>							
							<th class="toleft">Mail</th>
							<th class="toleft">Phone</th>
							<th class="toleft">Language</th>							
							<th colspan="2">Options</th>
						</tr>
					</thead>
					<tbody>
					{foreach from=$itemsList item=item}
						<tr{cycle values=' class="diff",'}>
							{assign var=country_id value=$item->country}							
							<td class="toleft">{$item->name}</td>						
							<td class="toleft">{$item->country_name}</td>
							<td class="toleft">{$item->mail}</td>
							<td class="toleft">{$item->phone}</td>														
							<td class="toleft">
								{foreach $languages as $lang}
									{if $lang->id eq $item->language_id}									
										{$lang->name}
									{/if}
								{/foreach}
								
							</td>
							<td><a href="{$link}/showfull/{$item->id}" title="Show all Info">Edit</a></td>						
							<td><a rel="dialog-contact" href="{$link}/removePartner/{$item->id}{if $offset}/{$offset}{/if}" class="del-opt">Delete</a></td>
						</tr>
					{/foreach}
					</tbody>
				</table>					
			{include file='pagination.tpl'}
			<div class="dialog-box" id="dialog-contact" title="Delete contact?">
				<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
					<span style="display:block; padding-left: 25px;">Partner <em></em> will be deleted.<br/> Are You sure?</span>
				</p>			
			</div>
		{/if}
{include file="footer.tpl"}