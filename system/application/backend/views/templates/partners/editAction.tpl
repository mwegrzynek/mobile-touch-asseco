{include file="header.tpl"}
		<h1>{$header}</h1>
		{if $subheader}<h2>{$subheader}</h2>{/if}

		<div class="tab-content">         
			<form action="{$link}/{$formLink}" method="post" class="form-a" enctype="multipart/form-data">
				{if $errors}
				<h3 class="warning">{$errorHeader}</h3>
				<ul class="warning">
				{foreach from=$errors item=error}
					<li>{$error}</li>
				{/foreach}
				</ul>
				{/if}
				
				<ul class="form-items">
					<li>	
						<label for="language_id">Language <em>*</em>:</label>
						<select name="language_id" id="language_id">							
							{if $languages}
								{foreach $languages as $lang}
									{if $lang->id eq $backData.language_id}
									<option value="{$lang->id}" selected="selected">
									{else}
									<option value="{$lang->id}">
									{/if}
										{$lang->name}
									</option>
								{/foreach}
							{/if}
						</select>											
					</li>
					
					<li class="diff">
						<label for="name">Name <em>*</em>:</label> <input type="text" name="name" id="name" value="{$backData.name}"/>
					</li>
					
					<li>
						<label for="person_position">Position:</label> <input type="text" name="person_position" id="person_position" value="{$backData.person_position}"/>
					</li>
					
					<li class="diff">
						<label for="company_name">Company name:</label> <input type="text" name="company_name" id="company_name" value="{$backData.company_name}"/>
					</li>
					
					<li>
						<label for="country_name">Country name:</label> <input type="text" name="country_name" id="country_name" value="{$backData.country_name}"/>
					</li>			
				
					<li class="diff">
						<label for="phone">Phone:</label> <input type="text" name="phone" id="phone" value="{$backData.phone}"/>
					</li>
					
					<li>
						<label for="mobile_phone">Mobile Phone:</label> <input type="text" name="mobile_phone" id="mobile_phone" value="{$backData.mobile_phone}"/>
					</li>
					
					<li class="diff">
						<label for="fax">Fax:</label> <input type="text" name="fax" id="fax" value="{$backData.fax}"/>
					</li>
					
					<li>
						<label for="mail">Mail:</label> <input type="text" name="mail" id="mail" value="{$backData.mail}"/>
					</li>
					
					<li class="diff">
						<label for="mail_personal">Personal Mail:</label> <input type="text" name="mail_personal" id="mail_personal" value="{$backData.mail_personal}"/>
					</li>
					
					<li>
						<label for="www">WWW address:</label> <input type="text" name="www" id="www" value="{$backData.www}"/>
					</li>
					
					<li class="diff">
						<label for="address_street">Street:</label> <input type="text" name="address_street" id="address_street" value="{$backData.address_street}"/>
					</li>
					
					<li>
						<label for="address_zipcode">Zipcode:</label> <input type="text" name="address_zipcode" id="address_zipcode" value="{$backData.address_zipcode}"/>
					</li>
					
					<li class="diff">
						<label for="address_city">City:</label> <input type="text" name="address_city" id="address_city" value="{$backData.address_city}"/>
					</li>
					
					{*<li class="diff">
						<label for="info">Info:</label>
						<textarea rows="16" cols="90" id="info" name="info" class="mceEditor">{$backData.info}</textarea>
					</li>*}
				</ul>
				
				<ul class="form-items1">
					<li>
						<input type="checkbox" {if $backData.is_default}checked="checked"{/if} name="is_default" id="is_default" value="1"/> <label for="is_default">Is default?</label>               
					</li>
				</ul>				
				
				<p class="btn">					
					<button type="submit">
						Submit &gt;
					</button>					
				</p>
				
			</form>         
		</div>         
{include file="footer.tpl"}