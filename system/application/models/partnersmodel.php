<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class PartnersModel extends CI_Model
	{
		function __construct()
		{
			parent::__construct();				
			$this->partnersTable = 'partners';
		}
		
		
		// --------------------------------------------------------------------
		

		function getCountPartners($filter = array())
		{
			//filters settings
			if(count($filter))
			{				
				$this->setFilters($filter);
			}

			$this->db->from($this->partnersTable);
			$output = $this->db->count_all_results();

			return $output;
		}
		
		
		// --------------------------------------------------------------------
		

		function getAllPartners($start, $offset, $sortRules, $filter = array())
		{
			//filters settings
			if(count($filter))
			{
				$this->setFilters($filter);
			}
			//end filters

			$query = $this->db->select()
			->from($this->partnersTable)			
			->order_by($sortRules)
			->limit($start, $offset)
			->get();
		
			$result = $query->result();
			return $result;
		}
		
		
		// --------------------------------------------------------------------
		

		function checkIfExists($id)
		{
			$this->db->from($this->partnersTable);
			$this->db->where('id', $id);			
			return $this->db->count_all_results();
		}	
		

		// --------------------------------------------------------------------
		

		function getPartner($id)
		{
			$this->db->where($this->partnersTable.'.id', $id);

			$query = $this->db->select()
			->from($this->partnersTable)
			->limit(1)
			->get();

			$result = $query->result();
			//var_dump($this->db->last_query());
			return $result;
		}
		
		
		// --------------------------------------------------------------------
		
		
		function addPartner($data)
		{			
         //$data['add_date'] = date('Y-m-d H:i:s');
			$this->db->insert($this->partnersTable, $data);
         return $this->db->insert_id();
		}
		
		
		// --------------------------------------------------------------------
		
		function updatePartner($data)
		{			
			$id = $data['id'];
			unset($data['id']);
			
			
			$this->db->where('id', $id);
         $this->db->update($this->partnersTable, $data);
			return $id;
		}
				
		// --------------------------------------------------------------------
		
		//DELETE, REMOVE METHODS
		function removePartner($partner_id)
		{
			$this->db->delete($this->partnersTable, array('id' => $partner_id));
		}		
		
		// --------------------------------------------------------------------
				
		function setFilters($filter)
		{
			$CI = & get_instance();
			$CI->load->library('db_filter');
			$this->db_filter->setFilters($filter);
		}
	}