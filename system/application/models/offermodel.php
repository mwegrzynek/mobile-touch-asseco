<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class OfferModel extends CI_Model
	{
		function __construct()
		{
			parent::__construct();				
			$this->offersTable    = 'questions_offers';
		}

		function getCountOffers($filter = array())
		{
			//filters settings
			if(count($filter))
			{				
				$this->setFilters($filter);
			}

			$this->db->from($this->offersTable);
			$output = $this->db->count_all_results();

			return $output;
		}

		function getAllOffers($start, $offset, $sortRules, $filter = array())
		{
			//filters settings
			if(count($filter))
			{
				$this->setFilters($filter);
			}
			//end filters

			$query = $this->db->select()
			->from($this->offersTable)			
			->order_by($sortRules)
			->limit($start, $offset)
			->get();
		
			$result = $query->result();
			return $result;
		}


		function checkIfExists($id)
		{
			$this->db->from($this->offersTable);
			$this->db->where('id', $id);			
			return $this->db->count_all_results();
		}	


		function getOffer($id)
		{
			$this->db->where($this->offersTable.'.id', $id);

			$query = $this->db->select()
			->from($this->offersTable)
			->limit(1)
			->get();

			$result = $query->result();
			//var_dump($this->db->last_query());
			return $result;
		}
		

		function addOffer($data)
		{			
         $data['add_date'] = date('Y-m-d H:i:s');
			$this->db->insert($this->offersTable, $data);
         return $this->db->insert_id();
		}
		
		
		function getStatus($id)
		{
			$query = $this->db->select('is_accepted')->from($this->offersTable)->where('id', $id)->get();
			$result = $query->result();
			return $result[0]->is_accepted;
		}


		function setStatus($id, $status)
		{
			$data = array(
					'is_closed' => $status
				);

			$this->db->where('id', $id);
			$this->db->update($this->offersTable, $data);

			return true;
		}
		
		
		/*function getSpamStatus($id)
		{
			$query = $this->db->select('spam_reported')->from($this->offersTable)->where('id', $id)->get();
			$result = $query->result();
			return $result[0]->spam_reported;
		}


		function setSpamStatus($id, $status)
		{
			$data = array(
					'spam_reported' => $status
				);

			$this->db->where('id', $id);
			$this->db->update($this->offersTable, $data);

			return true;
		}*/
			
		
		function updateOffer($data)
		{			
			$id = $data['id'];
			unset($data['id']);
			
			
			$this->db->where('id', $id);
         $this->db->update($this->offersTable, $data);
			return $id;
		}

		function checkIfUserOwner($offer_id, $user_id)
		{
			$this->db->where('user_id', $user_id);
		   $this->db->where('id', $offer_id);
		   $this->db->from($this->offersTable);
			$output = $this->db->count_all_results();
			return $output;
		}
		
		function getOfferUser($user_id)
		{
			$CI = & get_instance();		
			$result = $CI->UserModel->getUser($user_id);			
			return $result;
		}
		
		/*function getEntriesIdsByUserOffers($user_id)
		{
			$CI = & get_instance();
			$this->db->select('item_id, add_date');
			$this->db->where('user_id', $user_id);
			$this->db->group_by('item_id');		   
			$this->db->order_by('add_date desc');		   
		   $this->db->from($this->offersTable);
		   $query = $this->db->get();
			$result = $query->result();
			
			if(isset($result[0]))
			{
				return $result;	
			}
			return false;
		}*/		
		
		
		//DELETE, REMOVE METHODS
		function removeOffer($offer_id)
		{
			$this->db->delete($this->offersTable, array('id' => $offer_id));
		}	
		
		// --------------------------------------------------------------------
		
		function removeOfferByUserId($user_id)
		{
			$this->db->delete($this->offersTable, array('user_id' => $user_id));
		}	
		
		// --------------------------------------------------------------------
		
		function removeOfferByQuestionId($question_id)
		{
			$this->db->delete($this->offersTable, array('question_id' => $question_id));
		}	
		
		// --------------------------------------------------------------------
				
		function setFilters($filter)
		{
			$CI = & get_instance();
			$CI->load->library('db_filter');
			$this->db_filter->setFilters($filter);
		}
	}