<?php
	class OptionsModel extends CI_Model 
	{
		function __construct()
		{
			parent::__construct();
			$this->optionsTable = 'options';    
			$this->statesTable = 'states';
		}

		function getDefaultLanguageId()
		{
			$this->db->select('default_language_id');            
			$this->db->where('id', 1);            
			$query = $this->db->get($this->optionsTable);
			$result = $query->result();
			return $result[0]->default_language_id;
		}
		
		function setDefaultLanguage($langid)
		{
			$data = array(   
					'default_language_id' => $langid
				);
				
			$this->db->where('id', 1);
			$this->db->update($this->optionsTable, $data);
			return true;
		} 
		
		function getAdminMail()
		{
			$this->db->select('global_admin_mail');            
			$this->db->where('id', 1);            
			$query = $this->db->get($this->optionsTable);         
			return $query->result();
		}
		
		function setAdminMail($mail)
		{
			$data = array(   
					'global_admin_mail' => $mail        
				);
				
			$this->db->where('id', 1);
			$this->db->update($this->optionsTable, $data);
			return true;
		} 
		
		function getStates()
      {                
         $query = $this->db->order_by('id asc')->get($this->statesTable);         
         return $query->result();
      }
      
      function getPrice()
		{
			$this->db->select('smr_price');            
			$this->db->where('id', 1);            
			$query = $this->db->get($this->optionsTable);         
			return $query->result();
		}
		
		function setPrice($price)
		{
			$data = array(   
					'smr_price' => $price        
				);
				
			$this->db->where('id', 1);
			$this->db->update($this->optionsTable, $data);
			return true;
		} 
		
		function getKnowingAboutData()
		{
			$data[1] = "Google";
			$data[2] = "Facebook";
			$data[3] = "Od znajomego";
			$data[4] = "Prasa";
			$data[5] = "Radio";
			$data[6] = "TV";
			$data[7] = "inne";
			
			return $data;
		}
		
	}