<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class CommentsModel extends CI_Model
	{
		function __construct()
		{
			parent::__construct();				
			$this->commentsTable    = 'items_comments';
		}

		function getCountComments($filter = array())
		{
			//filters settings
			if(count($filter))
			{				
				$this->setFilters($filter);
			}

			$this->db->from($this->commentsTable);
			$output = $this->db->count_all_results();

			return $output;
		}

		function getAllComments($start, $offset, $sortRules, $filter = array())
		{
			//filters settings
			if(count($filter))
			{
				$this->setFilters($filter);
			}
			//end filters

			$query = $this->db->select()
			->from($this->commentsTable)			
			->order_by($sortRules)
			->limit($start, $offset)
			->get();
		
			$result = $query->result();
			return $result;
		}


		function checkIfExists($id)
		{
			$this->db->from($this->commentsTable);
			$this->db->where('id', $id);			
			return $this->db->count_all_results();
		}	


		function getComment($id)
		{
			$this->db->where($this->commentsTable.'.id', $id);

			$query = $this->db->select()
			->from($this->commentsTable)
			->limit(1)
			->get();

			$result = $query->result();
			//var_dump($this->db->last_query());
			return $result;
		}
		

		function addComment($data)
		{			
         $data['add_date'] = date('Y-m-d H:i:s');
			$this->db->insert($this->commentsTable, $data);
         return $this->db->insert_id();
		}
		
		
		function getStatus($id)
		{
			$query = $this->db->select('is_active')->from($this->commentsTable)->where('id', $id)->get();
			$result = $query->result();
			return $result[0]->is_active;
		}


		function setStatus($id, $status)
		{
			$data = array(
					'is_active' => $status
				);

			$this->db->where('id', $id);
			$this->db->update($this->commentsTable, $data);

			return true;
		}
		
		
		function getSpamStatus($id)
		{
			$query = $this->db->select('spam_reported')->from($this->commentsTable)->where('id', $id)->get();
			$result = $query->result();
			return $result[0]->spam_reported;
		}


		function setSpamStatus($id, $status)
		{
			$data = array(
					'spam_reported' => $status
				);

			$this->db->where('id', $id);
			$this->db->update($this->commentsTable, $data);

			return true;
		}
			
		
		function updateComment($data)
		{			
			$id = $data['id'];
			unset($data['id']);
			
			
			$this->db->where('id', $id);
         $this->db->update($this->commentsTable, $data);
			return $id;
		}

		function checkIfUserOwner($comment_id, $user_id)
		{
			$this->db->where('user_id', $user_id);
		   $this->db->where('id', $comment_id);
		   $this->db->from($this->commentsTable);
			$output = $this->db->count_all_results();
			return $output;
		}
		
		function getCommentUser($user_id)
		{
			$CI = & get_instance();		
			$result = $CI->UserModel->getUser($user_id);			
			return $result;
		}
		
		function getEntriesIdsByUserComments($user_id)
		{
			$CI = & get_instance();
			$this->db->select('item_id, add_date');
			$this->db->where('user_id', $user_id);
			$this->db->group_by('item_id');		   
			$this->db->order_by('add_date desc');		   
		   $this->db->from($this->commentsTable);
		   $query = $this->db->get();
			$result = $query->result();
			
			if(isset($result[0]))
			{
				return $result;	
			}
			return false;
		}		
		
		
		//DELETE, REMOVE METHODS
		function removeComment($comment_id)
		{
			$this->db->delete($this->commentsTable, array('id' => $comment_id));
		}	
		
		
		// --------------------------------------------------------------------
		
		function removeItemComments($item_id)
		{
			$this->db->delete($this->commentsTable, array('item_id' => $item_id));
		}
		
		// --------------------------------------------------------------------
				
		function setFilters($filter)
		{
			$CI = & get_instance();
			$CI->load->library('db_filter');
			$this->db_filter->setFilters($filter);
		}
	}