<?php

   class TransactionModel extends CI_Model
   {
      function __construct()
		{
			parent::__construct();
			$this->transactionTable = 'transactions';
      }

		function addTransaction($data)
		{
		   $data['add_date'] = date('Y-m-d H:i:s');
			
			$this->db->insert($this->transactionTable, $data);
			return $this->db->insert_id();
		}

		function ifTransactionExists($transactionPublicId)
		{
			$this->db->where('public_id', $transactionPublicId);
		   $this->db->from($this->transactionTable);
			$output = $this->db->count_all_results();
			return $output;
		}

		function getTransaction($transactionPublicId)
		{
			$this->db->where('public_id', $transactionPublicId);
		   $this->db->from($this->transactionTable);
			$this->db->limit(1);
			$query = $this->db->get();

			$result = $query->result();

			if(isset($result[0]))
			{
				return $result[0];
			}
			return false;
		}
		
		function setTransactionStatus($transactionPublicId, $data)
		{
			$this->db->where('public_id', $transactionPublicId);
			$this->db->update($this->transactionTable, $data);
		}


		function getCountTransactions($filter = array())
      {
        //filters settings
			if(count($filter))
			{				
				$this->setFilters($filter);
			}

         $this->db->from($this->transactionTable);
         $output = $this->db->count_all_results();
         return $output;
      }

      function getAllTransactions($start, $offset, $sortRules, $filter = array())
      {
         //filters settings
			if(count($filter))
			{				
				$this->setFilters($filter);
			}

         $query = $this->db->select($this->transactionTable.'.id, '.
                                    $this->transactionTable.'.public_id, '.
                                    $this->transactionTable.'.add_date, '.
                                    $this->transactionTable.'.type_id, '.
                                    $this->transactionTable.'.status_code, '.
                                    $this->transactionTable.'.status_message')
			->from($this->transactionTable)
         ->order_by($sortRules)
         ->limit($start, $offset)
			->get();

         $result = $query->result();

         //var_dump($this->db->last_query());

         return $result;
      }

		function deleteTransaction($id)
      {
         $tables = array($this->transactionTable);
         $this->db->where('id', $id);
         $this->db->delete($tables);
      }
      
      function setFilters($filter)
		{
			$CI = & get_instance();
			$CI->load->library('db_filter');
			$this->db_filter->setFilters($filter);  
		}
	}
?>