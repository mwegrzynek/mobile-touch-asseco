<?php

class VideosModel extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->itemsTable = 'items';
		$this->itemContentTable = 'items_info';

		$this->videoTable = 'items_video';
		$this->picturesTable = 'items_pictures';
	}	
	
	// --------------------------------------------------------------------

	function addVideo($data)
	{
		$data['id'] = '';
		$this->db->insert($this->videoTable, $data);
		return $this->db->insert_id();
	}
	
	// --------------------------------------------------------------------

	function getVideos($start, $offset, $sort, $itemid)
	{
		$this->db->where($this->videoTable.'.item_id', $itemid);

		$query = $this->db->select($this->videoTable.'.id, '.
											$this->videoTable.'.type_id, '.
											$this->videoTable.'.thumbnail_url, '.
											$this->videoTable.'.link')
		->from($this->videoTable)
		->order_by($sort)
		->limit($start, $offset)
		->get();

		$result = $query->result();

		//var_dump($this->db->last_query());

		return $result;
	}
	
	// --------------------------------------------------------------------

	function getVideo($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get($this->videoTable);
		return $query->result();
	}
	
	// --------------------------------------------------------------------

	function getCountVideos($itemid)
	{
		if($itemid != -1)
		{
			$this->db->where('item_id', $itemid);
			$this->db->from($this->videoTable);
			return $this->db->count_all_results();
		}
		return $this->db->count_all($this->videoTable);
	}
	
	// --------------------------------------------------------------------

	function removeVideo($id)
	{
		$tables = array($this->videoTable);
		$this->db->where('id', $id);
		$this->db->delete($tables);
	}
	
	// --------------------------------------------------------------------
	
	function removeItemVideos($itemid)
	{
		$tables = array($this->videoTable);
		$this->db->where('item_id', $itemid);
		$this->db->delete($tables);
	}
	
	// --------------------------------------------------------------------

	function checkVideo($id)
	{
		$this->db->where('id', $id);
		$this->db->from($this->videoTable);
		return $this->db->count_all_results();
	}
	
	// --------------------------------------------------------------------

	function changeVideo($data)
	{
		$this->db->where('id', $data['id']);
		$this->db->update($this->videoTable, $data);
		return true;
	}
}