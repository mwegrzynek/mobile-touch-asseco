<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class ContactsModel extends CI_Model
	{
		function __construct()
		{
			parent::__construct();
			$this->contactsTable = 'contacts';
		}


		// --------------------------------------------------------------------


		function getCountContacts($filter = array())
		{
			//filters settings
			if(count($filter))
			{
				$this->setFilters($filter);
			}

			$this->db->from($this->contactsTable);
			$output = $this->db->count_all_results();

			return $output;
		}


		// --------------------------------------------------------------------


		function getAllContacts($start, $offset, $sortRules, $filter = array())
		{
			//filters settings
			if(count($filter))
			{
				$this->setFilters($filter);
			}
			//end filters

			$query = $this->db->select()
			->from($this->contactsTable)
			->order_by($sortRules)
			->limit($start, $offset)
			->get();

			$result = $query->result();
			return $result;
		}


		// --------------------------------------------------------------------


		function checkIfExists($id)
		{
			$this->db->from($this->contactsTable);
			$this->db->where('id', $id);
			return $this->db->count_all_results();
		}


		// --------------------------------------------------------------------


		function getContact($id)
		{
			$this->db->where($this->contactsTable.'.id', $id);

			$query = $this->db->select()
			->from($this->contactsTable)
			->limit(1)
			->get();

			$result = $query->result();
			//var_dump($this->db->last_query());
			return $result;
		}


		// --------------------------------------------------------------------


		function addContact($data)
		{
         $data['add_date'] = date('Y-m-d H:i:s');

         $data['type'] = 0;
         if($data['typeof'] == 'gartner')
         {
         	$data['type'] = 1;
         }
         // elseif ($data['typeof'] == 'platformaconnector') {
         // 	$data['type'] = 2;
         // }

         unset($data['typeof']);

			$this->db->insert($this->contactsTable, $data);
         return $this->db->insert_id();
		}


		// --------------------------------------------------------------------

		function updateContact($data)
		{
			$id = $data['id'];
			unset($data['id']);


			$this->db->where('id', $id);
         $this->db->update($this->contactsTable, $data);
			return $id;
		}

		// --------------------------------------------------------------------

		//DELETE, REMOVE METHODS
		function removeContact($contact_id)
		{
			$this->db->delete($this->contactsTable, array('id' => $contact_id));
		}

		// --------------------------------------------------------------------

		function setFilters($filter)
		{
			$CI = & get_instance();
			$CI->load->library('db_filter');
			$this->db_filter->setFilters($filter);
		}
	}