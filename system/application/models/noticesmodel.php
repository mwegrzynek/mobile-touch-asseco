<?php

   class NoticesModel extends CI_Model
   {
      function __construct()
      {
         parent::__construct();
			$this->adminTable = 'admins';
			$this->permissionTable = 'admin_permission';
      }

		// --------------------------------------------------------------------

		function getSuperAdminsMails()
		{
		   $this->db->from($this->adminTable);
		   $this->db->where('group_id', 1);
			$query = $this->db->get();

			$result = $query->result();

			if(isset($result[0]))
			{
				foreach($result as $key => $value) 
				{
					$output[] = $value->email;
				}
				return $output;
			}
			return false;
		}

		// --------------------------------------------------------------------

		function getGroupsByPermission($permission)
		{
			$permArray = explode('-', $permission);
			
			$this->db->where('action_id', $permArray[1]);
			$this->db->where('type_identity', $permArray[0]);
		   $this->db->from($this->permissionTable);			
			$query = $this->db->get();
			$result = $query->result();
			if(isset($result[0]))
			{
				foreach($result as $key => $value) 
				{
					$groups[] = $value->group_id;
				}
				return $groups;
			}
			return false;
		}
		
		// --------------------------------------------------------------------
		
		function getAdminsMailsByGroups($groups)
		{
			$this->db->where_in('group_id', $groups);
		   $this->db->from($this->adminTable);			
			$query = $this->db->get();
			$result = $query->result();
			
			if(isset($result[0]))
			{
				foreach($result as $key => $value) 
				{
					$output[] = $value->email;
				}
				return $output;
			}
			return false;
		}
	}
?>