<?php
	class PicturesModel extends CI_Model
	{
		function __construct()
		{
			parent::__construct();
			$this->itemTable = 'items';
			$this->itemContentTable = 'items_info';					
			$this->picturesTable = 'items_pictures';
		}
				
		/* PICTURES */
		function makeThumbName($fileName, $counter = 0)
		{
			if($fileName != '')
			{
				if(!$counter) 
				{
					$counter = '';
				}
				else
				{
					$counter = '_'.$counter;
				}
				
				$path_parts = pathinfo($fileName);
				$file_extension = $path_parts['extension'];
				$thumbFileName = str_replace('.'.$file_extension, '', $fileName).$counter.'_thumb'.'.'.$file_extension;
				return $thumbFileName;
			}
			else
			{
				return false;
			}
		}

		function addPicture($data)
		{
			$data['id'] = '';
			$this->db->insert($this->picturesTable, $data);
			return $this->db->insert_id();
		}

		function getPictures($start, $offset, $sort, $galleryid)
		{
			$this->db->where($this->picturesTable.'.item_id', $galleryid);

			$query = $this->db->select($this->picturesTable.'.id, '.
												$this->picturesTable.'.file_name, '.
												$this->picturesTable.'.position')
			->from($this->picturesTable)
			->order_by($sort)
			->limit($start, $offset)
			->get();

			$result = $query->result();

			//var_dump($this->db->last_query());

			return $result;
		}

		function getPicture($id)
		{
			$this->db->where('id', $id);
			$query = $this->db->get($this->picturesTable);
			return $query->result();
		}

		function getCountPictures($galleryid)
		{
			if($galleryid != -1)
			{
				$this->db->where('item_id', $galleryid);
				$this->db->from($this->picturesTable);
				return $this->db->count_all_results();
			}
			return $this->db->count_all($this->picturesTable);
		}

		function removePicture($id)
		{
			$tables = array($this->picturesTable);
			$this->db->where('id', $id);
			$this->db->delete($tables);
		}

		function deletePicture($id, $thumbsConfig)
      {
         $info = $this->getPicture($id);

			$fileName = $info[0]->file_name;

         if($fileName != '')
         {
				$thumbsConfig['file_name'] = $fileName;														
				$thumbsConfig['old_file_name'] = $fileName;							
				$this->load->library('picturesactionslib');				
				$this->picturesactionslib->removeThumbs($thumbsConfig);
         }
      }

		function checkPicture($id)
		{
			//$this->db->where('id', $id);
			//$this->db->from($this->picturesTable);
			//return $this->db->count_all_results();
			
			$result = $this->getPicture($id);
			if(isset($result[0])) 
			{
				return $result[0];
			}
			return false;
		}

		function changePicture($data)
		{
			$this->db->where('id', $data['id']);
			$this->db->update($this->picturesTable, $data);
			return true;
		}
		
		function getRandomPictures($picturesNum, $itemid = 0)
		{
			$this->db->select_max('id');
			$query = $this->db->get($this->picturesTable);
			
			$result = $query->result();
			
			$items = $picturesNum * 100;			
			
			for($i = 0; $i < $items; $i++)
			{
				$randIds[] = rand(1, $result[0]->id);
			}	

			$randIds = array_unique($randIds);			
			shuffle($randIds);			
			$randIds = array_slice($randIds, 0, $picturesNum);
			$this->db->where_in('id', $randIds);	
			$this->db->limit($picturesNum);	
			$query = $this->db->get($this->picturesTable);			
			$result = $query->result();			
			return $result;			
		}
		
		
		function addFilename()
		{			
			$query = $this->db->get($this->picturesTable);
			
			$result = $query->result();
			
			foreach($result as $res)
			{
				$data['id'] = $res->id;
				$data['file_name'] = $res->id.'.jpg';
				$this->changePicture($data);				
			}					
		}		
	}	