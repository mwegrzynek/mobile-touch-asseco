<?php

	class ServiceModel extends CI_Model
	{
		function __construct()
		{
			parent::__construct();
			$this->userTable     = 'user';
			$this->serviceTable  = 'service';
			$this->stateTable    = 'states';
			$this->serviceMetaDataTable = 'service_meta_data';
			$this->serviceFilesTable = 'service_files';
			$this->currencyTable = 'service_currencies';
			
		}

		/*function getUserMails()
		{
			$this->db->select('email');
			$query = $this->db->get($this->serviceTable);
			return $query->result();
		} */


		function getCountServices($filter = array())
		{
			//filters settings
			if(count($filter))
			{				
				$this->setFilters($filter);
			}

			$this->db->from($this->serviceTable);
			$output = $this->db->count_all_results();

			return $output;
		}

		function getAllServices($start, $offset, $sortRules, $filter = array())
		{
			//filters settings
			if(count($filter))
			{
				$this->setFilters($filter);
			}
			//end filters

			$query = $this->db->select('*, '.$this->serviceTable.'.id as id, (UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(valid_thru)) as exp_stamp')
			->from($this->serviceTable)
			->join($this->serviceMetaDataTable, $this->serviceTable.'.id = '.$this->serviceMetaDataTable.'.service_id', 'left')			
			->order_by($sortRules)
			->limit($start, $offset)
			->get();
		
			$result = $query->result();
			return $result;
		}


		function checkIfServiceExists($id)
		{
			$this->db->from($this->serviceTable);
			$this->db->where('id', $id);
			$this->db->where('is_paid', 1);			
			return $this->db->count_all_results();
		}


		function getServiceName($id)
      {
         $query = $this->db->select('title')->where($this->serviceTable.'.id', $id)->limit(1)->get($this->serviceTable);
         $result = $query->result();

         if(count($result))
         {
            return $result[0]->title;
         }
         return false;
      }


		function getService($id)
		{
			$this->db->where($this->serviceTable.'.id', $id);

			$query = $this->db->select('*, (UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(valid_thru)) as exp_stamp')
			->from($this->serviceTable)
			->limit(1)
			->get();

			$result = $query->result();
			//var_dump($this->db->last_query());
			return $result;
		}


		function getServiceByPublicId($publicId)
      {
        	$query = $this->db->get_where($this->serviceTable, array('public_id' => $publicId));
			$result = $query->result();
			if(isset($result[0]))
			{
				return $result[0];
			}
			return false;
      }
      
     /* function parseProducts($productsString)
      {
      	$productsData = explode('#####', $productsString);
      	
      	if(isset($productsData[0])) 
      	{
      		foreach($productsData as $key => $value) 
      		{
      			$productInfo = explode('!!!!!', $value);
      			$productData[$key]['name'] = $productInfo[0];
      			$productData[$key]['price'] = $productInfo[1]; 
      			$productData[$key]['tax_value'] = $productInfo[2]; 
      			$productData[$key]['qty'] = $productInfo[3]; 
      			$productData[$key]['price_brutto'] = round($productData[$key]['price'] + $productData[$key]['price'] * $productData[$key]['tax_value'] / 100, 2);
      			$productData[$key]['subtotal_netto'] = $productData[$key]['price'] * $productData[$key]['qty']; 
      			$productData[$key]['subtotal_brutto'] = $productData[$key]['price_brutto'] * $productData[$key]['qty']; 
      			$productData[$key]['isbn'] = $productInfo[4];
      		}
      		return $productData;
      	}
      	return false;
      }*/

		/*function addService($data)
		{
         $this->db->insert($this->serviceTable, $data);
         return $this->db->insert_id();
		}*/

		/*function getRandomServices($count)
		{
			$query = $this->db->order_by('id', 'random')->limit($count)->get_where($this->serviceTable, array('is_active' => 1, 'is_published' => 1));
			$result = $query->result();

			foreach($result as $res)
			{
				$serviceResult = $this->getService($res->id);
				$output[] = $serviceResult[0];
			}

			return $output;
		}


		function getLastServices($count)
		{
			$query = $this->db->order_by('add_date', 'desc')->limit($count)->get_where($this->serviceTable, array('is_active' => 1, 'is_published' => 1));
			$result = $query->result();

			foreach($result as $res)
			{
				$serviceResult = $this->getService($res->id);
				$output[] = $serviceResult[0];
			}

			return $output;
		}*/


		function addService($data)
		{			
         $data['add_date'] = date('Y-m-d H:i:s');
			$this->db->insert($this->serviceTable, $data);
         return $this->db->insert_id();
		}
		
		function addServiceMetaData($data)
		{			
			$this->db->insert($this->serviceMetaDataTable, $data);
         return $this->db->insert_id();
		}
		
		function updateService($data)
		{			
			$id = $data['id'];
			unset($data['id']);
			
			$this->db->where('id', $id);
         $this->db->update($this->serviceTable, $data);
			return $id;
		}
		
		
		function updateServiceMetaData($data)
		{			
			$id = $data['service_id'];
			unset($data['service_id']);
			
			$this->db->where('service_id', $id);
         $this->db->update($this->serviceMetaDataTable, $data);
			return $id;
		}


		function getStatus($id)
		{
			$query = $this->db->select('is_paid')->from($this->serviceTable)->where('id', $id)->get();
			$result = $query->result();
			return $result[0]->is_paid;
		}


		function setStatus($id, $status)
		{
			$data = array(
					'is_paid' => $status
				);

			$this->db->where('id', $id);
			$this->db->update($this->serviceTable, $data);

			return true;
		}
		
		
		function checkIfUserOwner($service_id, $user_id)
		{
			$this->db->where('user_id', $user_id);
		   $this->db->where('id', $service_id);
		   $this->db->from($this->serviceTable);
			$output = $this->db->count_all_results();
			return $output;
		}
		
		function addUserFile($data)
		{
			$this->db->insert($this->serviceFilesTable, $data);
         return $this->db->insert_id();
		}
		
		
		function getUserFile($fileid)
		{
			$query = $this->db->get_where($this->serviceFilesTable, array('id' => $fileid));
			return $query->result();
		}
		
		function removeUserFile($fileid, $path)
		{
			$info = $this->getUserFile($fileid);
			
			if($info[0]->file_name != '')
			{
				unlink($path.'/'.$info[0]->file_name);         
				$tables = array($this->serviceFilesTable);
				$this->db->where('id', $fileid);
				$this->db->delete($tables);     
			}			
		}

		
		
		//SERVICE MESSAGES
		
		function getAllServiceMessages($serviceid, $ifGetUnreaded = false)
		{
			$data['service_id'] = $serviceid;
			if($ifGetUnreaded)
			{
				$data['is_read'] = 0;					
			}
			
			$query = $this->db->order_by('add_date', 'desc')->get_where($this->serviceMsgTable, $data);
			
			//$output = $this->db->count_all_results();
			$result = $query->result();

			return $result;
			//return false;
		}
		
		function addServiceMessages($data)
		{
			$data['add_date'] = date('Y-m-d H:i:s');
			$this->db->insert($this->serviceMsgTable, $data);
         return $this->db->insert_id();
		}
		
		function deleteAllServiceMessages($serviceid)
      {
      	$this->db->delete($this->serviceMsgTable, array('service_id' => $serviceid));
      }
      
      function markServiceMessagesAsReaded($serviceid)
		{
			$data['is_read'] = 1;
			$this->db->where('service_id', $serviceid);
			$this->db->update($this->serviceMsgTable, $data);
		}
		
		
		//DELETE, REMOVE METHODS
		function removeService($service_id, $fullPath = '')
		{
			// remove service files and invoice
			//$this->removeFiles($service_id);
			
			//remove all enclosed additional files
			//$this->removeAllServiceFiles($service_id);
			
			//remove all messages added to this service
			//$this->deleteAllServiceMessages($service_id);
			$CI = & get_instance();
			$CI->load->model('../../models/ItemsModel');
			$serviceMeta = $this->getServiceMetaData($service_id);
			if($sliderId = $serviceMeta[0]->entry_slider_id) 
			{
				$CI->ItemsModel->removeItem($sliderId);
			}
			
			$this->db->delete($this->serviceMetaDataTable, array('service_id' => $service_id));
			$this->db->delete($this->serviceTable, array('id' => $service_id));			
		}
		
		function getAllServiceFiles($serviceid)
		{
			
			$query = $this->db->order_by('add_date', 'desc')->get_where($this->serviceFilesTable, array('service_id' => $serviceid));
			
			//$output = $this->db->count_all_results();
			$result = $query->result();

			return $result;
			//return false;
		}
		
		function removeAllServiceFiles($serviceid, $path = '../upload/servicefilesplus')
      {
         $files = $this->getAllServiceFiles($serviceid);
      	
         if(isset($files[0]))
         {
         	foreach ($files as $key => $item) 
         	{
         		@unlink($path.'/'.$item->file_name);         		
         	}         	
         }         
         
         $this->deleteAllServiceFiles($serviceid);
      }	
      
      function deleteAllServiceFiles($serviceid)
      {
      	$this->db->delete($this->serviceFilesTable, array('service_id' => $serviceid));
      }		

		function removeFiles($id, $fullPath = '')
      {
         $info = $this->getService($id);
         
         $files[0] = $info[0]->fv_file;
         $files[1] = $info[0]->user_file;
         $files[2] = $info[0]->admin_file;
         
         $fields = array('fv_file', 'user_file', 'admin_file', 'fv_add_date', 'admin_file_add_date', 'user_file_add_date');        
         
         if($fullPath == '')
			{
				$path = '../upload/servicefiles/';
			}
			else
			{
				$path = $fullPath;
			}
			
			foreach ($files as $key => $item) 
			{
				if(isset($item) && $item != '')
	         {					
					unlink($path.$item);										
	         }
			}
			$this->changeImageFileName($id, $fields, '');
      }

		function changeImageFileName($id, $fields, $name)
      {
         foreach ($fields as $key => $item) 
         {
         	$data[$item] = $name;
			}
      	
         $this->db->where('id', $id);
         $this->db->update($this->serviceTable, $data);
         return true;
      }

		/*function makeThumbName($fileName)
		{
			if($fileName != '')
			{
				$path_parts = pathinfo($fileName);
				$file_extension = $path_parts['extension'];
				$thumbFileName = str_replace('.'.$file_extension, '', $fileName).'_thumb.'.$file_extension;
				return $thumbFileName;
			}
			else
			{
				return false;
			}
		}*/
		
		function getServiceMetaData($service_id)
		{
			$query = $this->db->select()
			->where('service_id', $service_id)
			->from($this->serviceMetaDataTable)
			->get();

			$result = $query->result();			
			return $result;
		}
		
		function getCurrencies()
		{
			$query = $this->db->select()
			->from($this->currencyTable)
			->get();

			$result = $query->result();
			
			if(isset($result[0])) 
			{
				foreach($result as $key => $value) 
				{
					$output[$value->id] = $value;
				}
				return $output;
			}
			return false;
		}
		
		function getServiceTypes()
		{
			$CI = & get_instance();
		
			$result = $CI->ItemsModel->getAllItems(10, 15, 0, 'title', 'asc');		
			
			if(isset($result[0])) 
			{
				foreach ($result as $key => $value) 
				{			
					$output[$value->id] = $value;
				}
				return $output;
			}
			return false;
		}
		
		function setFilters($filter)
		{
			$CI = & get_instance();
			$CI->load->library('db_filter');
			$this->db_filter->setFilters($filter);
		}
	}