<?php
	class LanguageModel extends CI_Model
	{
		function __construct()
		{
			parent::__construct();
			$this->languageTable = 'language';
		}

		// --------------------------------------------------------------------

		function getAllLanguages($activeOnly = false)
		{
			if($activeOnly)
			{
				$this->db->where('is_active', 1);
			}

			$query = $this->db->order_by('id', 'asc')->get('language');

			$result = $query->result();
			return $result;
		}

		// --------------------------------------------------------------------

		function addLanguage($name, $code, $subdomain, $filename, $is_visible, $is_active)
		{
			$data = array(
					'id'         => '',
					'name'       => $name,
					'lang_code'  => $code,
					'subdomain'  => $subdomain,
					'file_name'  => $filename,
					'is_visible' => $is_visible,
					'is_active'  => $is_active
				);

			$this->db->insert($this->languageTable, $data);
			return $this->db->insert_id();
		}

		// --------------------------------------------------------------------

		function changeLanguage($id, $name, $code, $subdomain, $filename, $is_visible, $is_active)
		{
			$data = array(
					'name'       => $name,
					'lang_code'  => $code,
					'subdomain'  => $subdomain,
					'file_name'  => $filename,
					'is_visible' => $is_visible,
					'is_active'  => $is_active
				);

			$this->db->where('id', $id);
			$this->db->update($this->languageTable, $data);
			return true;
		}

		// --------------------------------------------------------------------

		function checkLanguageName($name)
		{
			$this->db->where('name', $name);
			$this->db->from($this->languageTable);
			return $this->db->count_all_results();
		}

		// --------------------------------------------------------------------

		function checkLanguageCode($code)
		{
			/////http://www.loc.gov/standards/iso639-2/php/code_list.php
			$this->db->where('lang_code', $code);
			$this->db->from($this->languageTable);
			return $this->db->count_all_results();
		}

		// --------------------------------------------------------------------

		function getStatus($id, $statusType = 'is_active')
		{
			$result = $this->getLanguage($id);
			return $result[0]->$statusType;
		}

		// --------------------------------------------------------------------

		function setStatus($id, $statusType = 'is_active', $status)
		{
			$data = array(
					$statusType => $status
				);

			$this->db->where('id', $id);
			$this->db->update($this->languageTable, $data);
			return true;
		}

		// --------------------------------------------------------------------

		function deleteLang($id)
		{
			$tables = array('language');
			$this->db->where('id', $id);
			$this->db->delete($tables);


			$tables = array(
									'category_name',
									'menu_name',
									'items_info',
									'meta_data_info'
								);
			$this->db->where('language_id', $id);
			$this->db->delete($tables);
		}

		// --------------------------------------------------------------------

		function getLanguage($id)
		{
			$query = $this->db->from($this->languageTable)->where('id', $id)->limit(1)->get();
			return $query->result();
		}

		// --------------------------------------------------------------------

		function getLanguageBySubdomain($sub)
		{
			$query = $this->db->from($this->languageTable)->where('subdomain', $sub)->limit(1)->get();
			return $query->result();
		}

		// --------------------------------------------------------------------

		function removeLanguage($id)
		{
			$this->deleteLang($id);
			$tables = array($this->languageTable);
			$this->db->where('id', $id);
			$this->db->delete($tables);
		}

		// --------------------------------------------------------------------

		function copyLanguage($from, $to)
		{
			//items
			$this->copy_table($this->db->dbprefix.'items_info', 'id', $from, $to);

			//categories
			$this->copy_table($this->db->dbprefix.'category_name', 'id', $from, $to);

			//meta data
			$this->copy_table($this->db->dbprefix.'meta_data_info', 'id', $from, $to);

			//menus
			$this->copy_table($this->db->dbprefix.'menu_name', 'id', $from, $to);
		}

		// --------------------------------------------------------------------

		function copy_table($table='items_info', $id_field='id', $old_lang_id, $new_lang_id)
		{
			// load the original record into an array
			$this->db->select('*');
			$this->db->from($table);
			$this->db->where("language_id", $old_lang_id);
			$query = $this->db->get();

			foreach($query->result() as $original_record)
			{
				// insert the new record and get the new auto_increment id
				$sql_data = array('language_id' => 0);
				$this->db->insert($table, $sql_data);
				$newid = $this->db->insert_id();

				// generate the query to update the new record with the previous values
				$query = "UPDATE ".$table." SET ";
				foreach ($original_record as $key => $value)
				{
					if($key != $id_field)
					{
						if($key == 'language_id')
						{
							$value = $new_lang_id;
						}
						$query .= '`'.$key.'` = "'.str_replace('"','\"',$value).'", ';
					}
				}

				$query = rtrim($query, ', '); # lop off the extra trailing comma
				$query .= " WHERE {$id_field}={$newid}";
				$this->db->query($query);
			}
		}

		// --------------------------------------------------------------------


	}