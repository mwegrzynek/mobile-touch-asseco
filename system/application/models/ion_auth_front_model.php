<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Ion Auth Model
*
* Author:  Ben Edmunds
* 		   ben.edmunds@gmail.com
*	  	   @benedmunds
*
* Added Awesomeness: Phil Sturgeon
*
* Location: http://github.com/benedmunds/CodeIgniter-Ion-Auth
*
* Created:  10.01.2009
*
* Description:  Modified auth system based on redux_auth with extensive customization.  This is basically what Redux Auth 2 should be.
* Original Author name has been kept but that does not mean that the method has not been modified.
*
* Requirements: PHP5 or above
*
*/

//  CI 2.0 Compatibility
if(!class_exists('CI_Model')) { class CI_Model extends Model {} }

//get to front users from backend 

class Ion_auth_front_model extends Ion_auth_model
{	
	public function __construct()
	{
		parent::__construct();
		$this->getConfig();		
	}
	
	// --------------------------------------------------------------------
	
	public function getConfig()
	{		
		$this->load->config('../../../frontend/config/ion_auth');

		$this->tables  = $this->config->item('tables');		
		$this->columns = $this->config->item('columns');

		$this->identity_column = $this->config->item('identity');
		$this->store_salt      = $this->config->item('store_salt');
		$this->salt_length     = $this->config->item('salt_length');
		$this->meta_join       = $this->config->item('join');
	}
}