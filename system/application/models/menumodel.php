<?php
	class MenuModel extends CI_Model
	{
		function __construct()
		{
			parent::__construct();
			$this->menuTable = 'menu';
			$this->menuNamesTable = 'menu_name';
		}

		function setDefaultLanguage($id)
		{
			$this->defaultLanguage = $id;
		}

		function setTables($tables)
		{
			$this->menuTable = $tables['menuTable'];
			$this->menuNamesTable = $tables['menuNamesTable'];
		}

		function getMenuInfo($id, $languageId)
		{
			$this->db->where('menu_id', $id);
			$this->db->where('language_id', $languageId);
			$query = $this->db->get($this->menuNamesTable);
			return $query->result();
		}

		function getMenuSimpleInfo($id)
		{
			$this->db->where('id', $id);
			$query = $this->db->get($this->menuTable);
			return $query->result();
		}
		
		function getMenuFullInfo($menuid)
		{
			$query = $this->db->select($this->menuTable.'.id, '.
												$this->menuTable.'.parent_id, '.												
												$this->menuNamesTable.'.name, '.
												$this->menuNamesTable.'.link, '.
												$this->menuNamesTable.'.class, '.
												$this->menuNamesTable.'.position, '.                               
												$this->menuNamesTable.'.is_active, '.												
												$this->menuNamesTable.'.language_id, '.
												$this->menuTable.'.file_name')
			->from($this->menuTable)
			->join($this->menuNamesTable, $this->menuTable.'.id = '.$this->menuNamesTable.'.menu_id', 'left')
			->where($this->menuNamesTable.'.language_id', $this->defaultLanguage)
			->where($this->menuTable.'.id', $menuid)
			->limit(1)
			->get();

			$result = $query->result();
			return $result;
		}


		function getMenuName($id, $languageId)
		{
			$result = $this->getMenuInfo($id, $languageId);
			
			if(!empty($result))
			{
				return $result[0]->name;
			}
			
			return '';
		}

		function getMenuChildren($id)
		{
			if($id != 0)
			{
				$this->db->select('id');
				$this->db->where('parent_id', $id);
				$query = $this->db->get($this->menuTable);
				$result = $query->result();

				//get all children
				$aChildren = array();
				foreach($result as $res)
				{
					$aChildren[] = $res->id;

					//and children of children
					$aChildrenTemp = $this->getMenuChildren($res->id);
					if(isset($aChildrenTemp[0]))
					{
						$aChildren = array_merge($aChildren, $aChildrenTemp);
					}
				}
				return $aChildren;
			}
			return false;
		}


		function getParent($id)
		{
			$this->db->select('parent_id');
			$this->db->where('id', $id);

			$query = $this->db->get($this->menuTable);
			$result = $query->result();
			return $result[0]->parent_id;
		}


		function getMenuParents($id)
		{
			if($id)
			{
				$iCounter=0;
				while($id)
				{
					$id = $this->getParent($id);
					$aParents[] = $id;

					$iCounter++;
					if($iCounter>100)
					{
						print("loop error in getMenuParents()");
						exit();
					}
				}
				$aParents=array_reverse($aParents);
				return $aParents;
			}
			else
			{
				return 0;
			}
		}
		
		
		function getMenuParentsInfo($parentid)
		{
			if($parentid)
			{
				$iCounter=0;
				while($parentid)
				{
					$parentInfo = $this->getMenuFullInfo($parentid);
					$parentid = $parentInfo[0]->parent_id;
					$aParents[] = $parentInfo[0];

					$iCounter++;
					if($iCounter>100)
					{
						print("loop error in getMenuParents()");
						exit();
					}
				}
				$aParents=array_reverse($aParents);	
							
				return $aParents;
			}
			else
			{
				return 0;
			}
		}


		function getAllMenus($id=0)
		{
			//when $id is given function gets all categories that menu with given $id can be added to
			//it cannot be this menu itself and any of its children

			$aOutput = array();

			$this->db->select('id');
			$query = $this->db->get($this->menuTable);

			$result = $query->result();

			$forbiddenMenus = array();
			if($id)
			{
				$forbiddenMenus = $this->getMenuChildren($id);
				array_push($forbiddenMenus, $id);
			}


			foreach($result as $res)
			{
				if(!in_array($res->id, $forbiddenMenus))
				{
					$aOutput[] = $res->id;
				}
			}

			return $aOutput;
		}




		function getMenusInfo($aMenusId, $languageId, $getparents=false, $sortInfo=array())
		{
			if(is_array($aMenusId))
			{
				foreach($aMenusId as $key => $id)
				{
					if($id)
					{
						$result = $this->getMenuInfo($id, $languageId);
						$aOutput[$key]['id']          = $id;
						$aOutput[$key]['name']        = $result[0]->name;
						$aOutput[$key]['link']        = $result[0]->link;
						$aOutput[$key]['class']       = $result[0]->class;
						$aOutput[$key]['position']    = $result[0]->position;

						if($getparents)
						{
							$aOutput[$key]['parents'] = $this->getMenusInfo($this->getMenuParents($id), $languageId);
						}

						//if sorting is on
						if(count($sortInfo))
						{
							if($sortInfo['sortby'] != '')
							{
								$sorting[$key] = $aOutput[$key][$sortInfo['sortby']];
							}
						}
					}
					else
					{
						//for menu parents case
						$aOutput[$key]['id'] = $id;
						$aOutput[$key]['name'] = 'Root';
					}
				}

				//sorting
				if(count($sortInfo))
				{
					array_multisort($sorting, $sortInfo['sortOrder'], $sortInfo['sortType'], $aOutput);
				}

				return $aOutput;
			}
		}
		

		/*for faster backend*/

		function getCountMenus($typeid, $filter = array())
		{
			$this->db->where('type', $typeid);
			
			if(count($filter))
			{
				if(isset($filter['customFilter']))
				{
					foreach($filter['customFilter'] as $customFilter)
					{
						if(isset($customFilter['like']))
						{
							$this->db->like('lower(name)', $customFilter['value']);
						}
						elseif(isset($customFilter['in']))
						{
							$this->db->where_in($customFilter['field'], $customFilter['value']);
						}
						else
						{
							$this->db->where($customFilter['field'], $customFilter['value']);
						}
					}
				}
			}

			$query = $this->db->select($this->menuTable.'.id, '.
												$this->menuNamesTable.'.type, '.
												$this->menuNamesTable.'.name, '.
												$this->menuNamesTable.'.link, '.
												$this->menuTable.'.parent_id, ')
			->from($this->menuNamesTable)
			->join($this->menuTable, $this->menuTable.'.id = '.$this->menuNamesTable.'.menu_id', 'left')
			->where($this->menuNamesTable.'.language_id', $this->defaultLanguage);

			$output = $this->db->count_all_results();
			return $output;
		}

		function getMenus($typeid, $start, $offset, $sortRules, $filter = array())
		{
			$this->db->where('type', $typeid);
			
			if(count($filter))
			{
				if(isset($filter['customFilter']))
				{
					foreach($filter['customFilter'] as $customFilter)
					{
						if(isset($customFilter['like']))
						{
							$this->db->like('lower(name)', $customFilter['value']);
						}
						elseif(isset($customFilter['in']))
						{
							$this->db->where_in($customFilter['field'], $customFilter['value']);
						}
						elseif(isset($customFilter['notin']))
						{							
							$this->db->where_not_in($customFilter['notin']['field'], $customFilter['notin']['value']);
						}
						else
						{
							$this->db->where($customFilter['field'], $customFilter['value']);
						}
					}
				}
			}

			$query = $this->db->select($this->menuTable.'.id, '.
												$this->menuNamesTable.'.menu_id, '.
												$this->menuNamesTable.'.language_id, '.
												$this->menuNamesTable.'.name, '.
												$this->menuNamesTable.'.is_active, '.
												$this->menuTable.'.parent_id, '.
												$this->menuNamesTable.'.link, '.
												$this->menuNamesTable.'.class, '.
												$this->menuNamesTable.'.position, '.                               
												$this->menuNamesTable.'.description, '.                               
												$this->menuTable.'.file_name')
			->from($this->menuNamesTable)
			->join($this->menuTable, $this->menuTable.'.id = '.$this->menuNamesTable.'.menu_id', 'left')
			->where($this->menuNamesTable.'.language_id', $this->defaultLanguage)
			->order_by($sortRules)
			->limit($start, $offset)
			->get();

			$result = $query->result();
			return $result;
		}

		/*end for faster backend*/

		function getMenusInfoLang($aMenusId, $languageId, $defaultLangid, $sortInfo=array())
		{
			if(is_array($aMenusId))
			{
				//default lang
				if(is_array($aMenusId) && count($aMenusId))
				{
					$this->db->where_in('menu_id', $aMenusId);
				}

				$this->db->where('language_id', $defaultLangid);
				$this->db->from($this->menuNamesTable);
				$query = $this->db->get();
				$result = $query->result();


				foreach($result as $res)
				{
					$categories[$res->menu_id]['id']          = $res->menu_id;
					$categories[$res->menu_id]['name']        = $res->name;
					$categories[$res->menu_id]['link']        = $res->link;
					$categories[$res->menu_id]['position']    = $res->position;

					$position[$res->menu_id] = $res->position;
					$name[$res->menu_id] = $res->name;
				}

				//given lang
				if(is_array($aMenusId) && count($aMenusId))
				{
					$this->db->where_in('menu_id', $aMenusId);
				}

				$this->db->where('language_id', $languageId);
				$this->db->from($this->menuNamesTable);
				$query = $this->db->get();
				$result = $query->result();


				foreach($result as $res)
				{
					$categoriesGivenLng[$res->menu_id]['id']          = $res->menu_id;
					$categoriesGivenLng[$res->menu_id]['name']        = $res->name;
					$categoriesGivenLng[$res->menu_id]['link']        = $res->link;
					$categoriesGivenLng[$res->menu_id]['position']    = $res->position;
				}

				if(isset($categoriesGivenLng))
				{
					foreach($categories as $key => $cat)
					{
						if(isset($categoriesGivenLng[$key]))
						{
							$categories[$key]['name'] = $categoriesGivenLng[$key]['name'];
							$categories[$key]['link'] = $categoriesGivenLng[$key]['link'];

							$position[$key] = $categoriesGivenLng[$key]['position'];
							$name[$key] = $categoriesGivenLng[$key]['name'];
						}
					}
				}

				//array_multisort($position, SORT_DESC, SORT_NUMERIC, $name, SORT_ASC, SORT_STRING, $categories);
				array_multisort($name, SORT_ASC, SORT_STRING, $categories);
				return $categories;
			}
		}


		function addMenu($data)
		{
			$data1['id'] = '';
			
			$data1['type'] 		= $data['type'];        
			$data1['parent_id'] 	= $data['parent_id'];        
			$data1['file_name'] 	= $data['file_name'];

			$this->db->insert($this->menuTable, $data1);
			$menuid = $this->db->insert_id();

			$data2['id']            = '';
			$data2['menu_id']   = $menuid;
			$data2['language_id']   = $data['language_id'];
			$data2['name']          = $data['name'];
			$data2['link']          = $data['link'];
			$data2['class']         = $data['class'];
			$data2['position']      = $data['position'];
			$data2['description']   = $data['description'];
			$data2['is_active']		= $data['is_active'];
			$this->db->insert($this->menuNamesTable, $data2);
			
			return $menuid;
		}


		function getMenu($languageId, $defaultLanguage)
		{
			/*var_dump($languageId);
			var_dump($defaultLanguage);*/
			$root = $this->getStraightChildrenFullInfo(0, $languageId, $defaultLanguage);

			foreach($root as $key => $rootCat)
			{
				$root[$key]['children'] = $this->getStraightChildrenFullInfo($rootCat['menuid'], $languageId, $defaultLanguage);
			}
			
			return $root;
		}


		function getStraightChildrenFullInfo($menuid, $languageId, $defaultLanguage)
		{
			///default language
			$this->db->where('parent_id', $menuid);
			$this->db->where($this->menuNamesTable.'.language_id', $defaultLanguage);
			$query = $this->db->select($this->menuTable.'.id, '.
												$this->menuTable.'.parent_id, '.
												$this->menuTable.'.file_name, '.                                   
												$this->menuNamesTable.'.name, '.
												$this->menuNamesTable.'.link, '.
												$this->menuNamesTable.'.language_id, '.
												$this->menuNamesTable.'.position')
			->from($this->menuTable)

			->join($this->menuNamesTable, $this->menuNamesTable.'.menu_id = '.$this->menuTable.'.id', 'left')
			->get();
			$result = $query->result();

			$categories = array();

			if(count($result))
			{
				foreach($result as $key => $res)
				{
					$categories[$res->id]['menuid'] = $res->id;
					$categories[$res->id]['menu_name'] = htmlspecialchars($res->name);
					$categories[$res->id]['menu_link'] = $res->link;               
					$categories[$res->id]['file_name'] 		= $res->file_name;               

					$position[$res->id] = $res->position;
					$name[$res->id] = $res->name;
				}
			}

			//given language
			$this->db->where('parent_id', $menuid);
			$this->db->where($this->menuNamesTable.'.language_id', $languageId);
			$query = $this->db->select($this->menuTable.'.id, '.
												$this->menuTable.'.parent_id, '.
												$this->menuTable.'.file_name, '.                                   
												$this->menuNamesTable.'.name, '.
												$this->menuNamesTable.'.link, '.
												$this->menuNamesTable.'.language_id, '.
												$this->menuNamesTable.'.position')
			->from($this->menuTable)

			->join($this->menuNamesTable, $this->menuNamesTable.'.menu_id = '.$this->menuTable.'.id', 'left')
			->get();
			$result = $query->result();

			if(count($result))
			{
				foreach($result as $key => $res)
				{
					$categoriesGivenLng[$res->id]['menuid'] = $res->id;
					$categoriesGivenLng[$res->id]['menu_name'] = htmlspecialchars($res->name);
					$categoriesGivenLng[$res->id]['menu_link'] = $res->link;
					$categoriesGivenLng[$res->id]['file_name'] = $res->file_name;                
					$categoriesGivenLng[$res->id]['position']  = $res->position;
				}
			}


			if(isset($categoriesGivenLng))
			{
				foreach($categories as $key => $cat)
				{
					if(isset($categoriesGivenLng[$key]))
					{
						$categories[$key]['menu_name'] = $categoriesGivenLng[$key]['menu_name'];
						$categories[$key]['menu_link'] = $categoriesGivenLng[$key]['menu_link'];

						$position[$key] = $categoriesGivenLng[$key]['position'];						
						$name[$key] = $categoriesGivenLng[$key]['menu_name'];
					}
				}
			}

			if(count($categories))
			{
				array_multisort($position, SORT_ASC, SORT_NUMERIC, $name, SORT_ASC, SORT_STRING, $categories);
				//array_multisort($name, SORT_ASC, SORT_STRING, $categories);				
			}
			return $categories;
		}


		function changeParent($id, $parentid)
		{
			$data = array(
					'parent_id'  => $parentid
				);

			$this->db->where('id', $id);
			$this->db->update($this->menuTable, $data);
			return true;
		}


		function changeMainData($data)
		{
			$id = $data['id'];
			unset($data['id']);
			$this->db->where('id', $id);
			$this->db->update($this->menuTable, $data);
			return $id;
		}


		function changeMenu($data)
		{
			//check if data exists
			if($this->checkMenuData($data['id'], $data['language_id'])) //data exists edit them
			{
				$data1['name']          = $data['name'];
				$data1['link']          = $data['link'];
				$data1['class']         = $data['class'];
				$data1['position']      = $data['position'];
				$data1['is_active']     = $data['is_active'];
				$data1['description']   = $data['description'];

				$this->db->where('menu_id', $data['id']);
				$this->db->where('language_id', $data['language_id']);

				$this->db->update($this->menuNamesTable, $data1);
			}
			else //data doesn't exists add them
			{
				$data1['id']            = '';
				$data1['menu_id']   		= $data['id'];
				$data1['language_id']   = $data['language_id'];
				$data1['name']          = $data['name'];
				$data1['link']          = $data['link'];
				$data1['class']         = $data['class'];
				$data1['position']      = $data['position'];
				$data1['is_active']     = $data['is_active'];
				$data1['description']   = $data['description'];

				$this->db->insert($this->menuNamesTable, $data1);
			}

			return true;
		}

		function checkMenuData($menuid, $languageid)
		{
			$this->db->where('menu_id', $menuid);
			$this->db->where('language_id', $languageid);
			$this->db->from($this->menuNamesTable);

			return $this->db->count_all_results();
		}


		function removeMenu($id)
		{
			// main data
			$tables = array($this->menuTable);
			$this->db->where('id', $id);
			$this->db->delete($tables);

			// info data
			$tables = array($this->menuNamesTable);
			$this->db->where('menu_id', $id);
			$this->db->delete($tables);
		}


		function ifMenuExists($id)
		{
			$this->db->where('id', $id);
			$this->db->from($this->menuTable);
			return $this->db->count_all_results();
		}

		function remapMenus($parentid, $destinationParentId)
		{
			$data['parent_id'] = $destinationParentId;

			$this->db->where('parent_id', $parentid);
			$this->db->update($this->menuTable, $data);
		}


		function getMenuInfoLang($id, $langid, $defaultLangid)
		{
			$info = $this->getMenuInfo($id, $langid);
			if(!isset($info[0]) || empty($info[0]->name))
			{
				$info = $this->getMenuInfo($id, $defaultLangid);
			}

			if(empty($info))
			{
				return false;			
			}
			
			$simpleInfo = $this->getMenuSimpleInfo($id);
			$info[0]->simple_info = $simpleInfo;
			if($simpleInfo[0]->parent_id)
			{
				$info[0]->parent_info = $this->getMenuInfoLang($simpleInfo[0]->parent_id, $langid, $defaultLangid);
			}

			return $info;
		}
		
		
		function getMenuNameLang($id, $langid, $defaultLangid)
		{
			$info = $this->getMenuInfo($id, $langid);
			if(!isset($info[0]) || empty($info[0]->name))
			{
				$info = $this->getMenuInfo($id, $defaultLangid);
			}
			
			if(!isset($info[0]) || empty($info[0]->name))
			{
				return false;
			}
			return $info[0]->name;
		}

		function removeImage($id, $path)
		{
			$info = $this->getMenuSimpleInfo($id);
			//var_dump($info);
			if($info[0]->file_name != '')
			{
				unlink($path.'/'.$info[0]->file_name);
				if(is_file($path.'/'.$this->makeThumbName($info[0]->file_name)))
				{
					unlink($path.'/'.$this->makeThumbName($info[0]->file_name));
				}				
				$this->changeImageFileName($id, '');
			}         
		}

		function changeImageFileName($id, $name)
		{
			$data = array(               
					'file_name'  => $name         
				);
				
			$this->db->where('id', $id);
			$this->db->update($this->menuTable, $data);
			return true;
		}


		function makeThumbName($fileName)
		{
			if($fileName != '')
			{
				$path_parts = pathinfo($fileName);
				$file_extension = $path_parts['extension'];
				$thumbFileName = str_replace('.'.$file_extension, '', $fileName).'_thumb.'.$file_extension;
				return $thumbFileName;
			}
			else
			{
				return false;
			}
		}

		/* 
		
		*/
		function getTree($itemType, $sortRules, $filter = array())
		{
			//get all items
			$this->menuResult = $this->getMenus($itemType, 10000, 0, $sortRules, $filter);
			
			
			if(isset($this->menuResult[0])) 
			{
				foreach($this->menuResult as $key => $value) 
				{
					$res[$value->id] = $value;
				}
				
				foreach($res as $key => $value) 
				{
					if($value->parent_id != 0) 
					{
						if(isset($res[$value->parent_id])) 
						{
							$res[$value->parent_id]->children[] = $value;						
						}
						else
						{
							array_walk_recursive($res, array('MenuModel', 'check_tree'), $value);								
						}
						unset($res[$key]);
					}
				}	
				return $res;	
			}
			return false;			
		}
		
		function check_tree($item, $key, $value)
		{		
			if($item->id == $value->parent_id) 
			{
				$item->children[] = $value;
				return true;
			}
			if(isset($item->children))
			{
				array_walk_recursive($item->children, array('MenuModel', 'check_tree'), $value);
			}			
		}
		
		
		function getActiveElementsById($itemType, $url, $filter = array())
		{
			$ids = array();
			if(isset($this->menuResult)) 
			{
				$result = $this->menuResult;
				unset($this->menuResult);
			}
			else
			{
				$result = $this->getMenus($itemType, 10000, 0, 'id asc', $filter);					
			}			
			
			if(isset($result[0])) 
			{
				foreach($result as $key => $value) 
				{
					if(in_array(trim($value->link, '/'), $url))
					{
						$ids[] = $value->id;
						$parents = $this->getMenuParents($value->id);
						if(isset($parents[1])) 
						{
							$ids[] = $parents[1];
						}
						//return $ids;												
					}
				}
			}
			
			return $ids;
		}



		/*functions for link problems*/
		function getAllMenuInfo($langId)
		{
			$this->db->where('language_id', $langId);
			$query = $this->db->get($this->menuNamesTable);
			$result = $query->result();
			return $result;
		}

		function savelink($id, $link)
		{
			$data['link'] = $link;
			$this->db->where('id', $id);
			$this->db->update($this->menuNamesTable, $data);
		}
	}