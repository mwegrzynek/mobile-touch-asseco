<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class ItemsModel extends CI_Model
	{
		function __construct()
		{
			parent::__construct();

			$this->itemsTable = 'items';
			$this->itemContentTable = 'items_info';
			$this->itemExtendedTable = 'items_ex';

			$this->videoTable = 'items_video';
			$this->picturesTable = 'items_pictures';

			$this->itemsCategoriesTable = 'item_category';

			$this->itemsTableFields = $this->db->list_fields($this->itemsTable);
			$this->itemsContentTableFields = $this->db->list_fields($this->itemContentTable);

			//var_dump($this->getQueryFieldsString(array('UNIX_TIMESTAMP(add_date) as add_date_stamp')));
		}


		// --------------------------------------------------------------------


		function getTableFields()
		{
			return $this->itemsTableFields;
		}


		// --------------------------------------------------------------------


		function getContentTableFields()
		{
			return $this->itemsContentTableFields;
		}


		// --------------------------------------------------------------------


		function setDefaultLanguage($id)
		{
			$this->defaultLanguage = $id;
		}


		// --------------------------------------------------------------------


		function getDefaultLanguage()
		{
			return $this->defaultLanguage;
		}


		// --------------------------------------------------------------------


		function getItemInfo($id, $langid = FALSE)
		{
			$simpleInfo = $this->getItemSimpleInfo($id);


			if($langid === FALSE)
			{
				$langid = $this->defaultLanguage;
			}

			$this->db->where('item_id', $id);
			$this->db->where('language_id', $langid);
			$query = $this->db->get($this->itemContentTable);
			$result = $query->result();

			$output = array_merge($result, $simpleInfo);
			return $output;
		}


		// --------------------------------------------------------------------


		function getItemSimpleInfo($id)
		{
			$this->db->where('id', $id);
			$query = $this->db->get($this->itemsTable);
			return $query->result();
		}


		// --------------------------------------------------------------------


		function getItemExtendedInfo($id)
		{
			$this->db->where('item_id', $id);
			$query = $this->db->get($this->itemExtendedTable);

			$result = $query->result();

			if(isset($result[0]))
			{
				return get_object_vars($result[0]);
			}

			return false;
		}


		// --------------------------------------------------------------------


		function getItemTitle($id, $languageId)
		{
			/*$result = $this->getItemInfo($id, $languageId);
			return $result[0]->title;*/

			$this->db->where('item_id', $id);
			$this->db->where('language_id', $languageId);
			$query = $this->db->get($this->itemContentTable);

			$result = $query->result();
			if(!count($result))
			{
				return false;
			}
			return $result[0]->title;
		}


		// --------------------------------------------------------------------


		function getCountItems($typeid, $filter = array())
		{
			$this->db->where('type', $typeid);

			//filters settings
			if(count($filter))
			{
				$this->setFilters($filter);
			}

			$query = $this->db->select($this->itemsTable.'.id, '.
												$this->itemContentTable.'.title')
			->from($this->itemsTable)
			->join($this->itemContentTable, $this->itemsTable.'.id = '.$this->itemContentTable.'.item_id', 'left')
			->where($this->itemContentTable.'.language_id', $this->defaultLanguage)
			->count_all_results();

			return $query;
		}


		// --------------------------------------------------------------------


		function getAllItems($typeid, $start, $offset, $sortColumn, $sortOrder, $filter = array())
		{
			$this->db->where('type', $typeid);

			//filters settings
			if(count($filter))
			{
				$this->setFilters($filter);
			}

			$query = $this->db->select($this->getQueryFieldsString(array('UNIX_TIMESTAMP(add_date) as add_date_stamp', '(UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(exp_date)) as exp_stamp')))
			->from($this->itemsTable)
			->join($this->itemContentTable, $this->itemsTable.'.id = '.$this->itemContentTable.'.item_id', 'left')
			->where($this->itemContentTable.'.language_id', $this->defaultLanguage)
			->order_by($sortColumn, $sortOrder)
			->limit($start, $offset)
			->get();

			$result = $query->result();

			return $result;
		}

		function setFilters($filter)
		{
		   $CI = & get_instance();
			$CI->load->library('db_filter');
			$this->db_filter->setFilters($filter);
		}

		// --------------------------------------------------------------------

		function getItemBySlug($slug, $langid, $itemtype)
		{

			$this->db->where('type', $itemtype);
			$this->db->where('slug', $slug);
			$this->db->where('language_id', $langid);

			$query = $this->db->select($this->getQueryFieldsString(array('UNIX_TIMESTAMP(add_date) as add_date_stamp', '(UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(exp_date)) as exp_stamp')))
			->from($this->itemsTable)
			->join($this->itemContentTable, $this->itemsTable.'.id = '.$this->itemContentTable.'.item_id', 'left')
			->limit(1)
			->get();

			$result = $query->result();

			return $result;
		}


		// --------------------------------------------------------------------


		function getItemById($id, $langid)
		{
			$this->db->where($this->itemsTable.'.id', $id);
			$this->db->where('language_id', $langid);

			$query = $this->db->select($this->getQueryFieldsString(array('UNIX_TIMESTAMP(add_date) as add_date_stamp')))
			->from($this->itemsTable)
			->join($this->itemContentTable, $this->itemsTable.'.id = '.$this->itemContentTable.'.item_id', 'left')
			->limit(1)
			->get();

			$result = $query->result();

			return $result;
		}


		// --------------------------------------------------------------------


		function makeSlug($title, $langId, $itemType)
		{
			$this->load->library('slug');
			$slug = $this->slug->makeSlugs($title);
			$slug = $this->checkSlug($slug, $langId, $itemType);
			return $slug;
		}


		// --------------------------------------------------------------------


		function checkSlug($slug, $langId, $itemType)
		{
			if($slug == '')
			{
				return $slug = 'please-write-slug-manually';
			}

			$counter = 1;
			do
			{
				if($result = $this->checkIfSlugExists($slug, $langId, $itemType))
				{
					$slug = $slug.$counter;
				}
				$counter++;
				if($counter > 100)
				{
					show_error('_checkSlug() loop error');
					break;
				}
			}
			while($result);
			return $slug;
		}


		// --------------------------------------------------------------------


		function checkIfSlugExists($slug, $langid, $itemType)
		{
			$this->db->where('type', $itemType);
			$this->db->where('slug', $slug);
			$this->db->where('language_id', $langid);

			$query = $this->db->select($this->getQueryFieldsString(/*array('UNIX_TIMESTAMP(add_date) as add_date_stamp')*/))
			->from($this->itemsTable)
			->join($this->itemContentTable, $this->itemsTable.'.id = '.$this->itemContentTable.'.item_id', 'left')
			->limit(1)
			->get();

			$result = $query->result();
			return count($result);
		}


		// --------------------------------------------------------------------


		function getQueryFieldsString($additionalQueries = array())
		{
			$queryItems = array();
			foreach($this->itemsTableFields as $key => $value)
			{
				$queryItems[] = $this->itemsTable.'.'.$value;
			}

			foreach($this->itemsContentTableFields as $key => $value)
			{
				if($value != 'id')
				{
					$queryItems[] = $this->itemContentTable.'.'.$value;
				}
			}

			if(isset($additionalQueries[0]))
			{
				foreach($additionalQueries as $key => $value)
				{
					$queryItems[] = $value;
				}
			}

			$string = implode($queryItems, ', ');

			return $string;
		}


		// --------------------------------------------------------------------


		function addItem($data)
		{
			$this->load->library('getdatalib');

			$mainData = $this->getdatalib->fillTableData($this->itemsTableFields, $data);
			$mainData['id']  						= '';
			$mainData['last_edit_date']  		= date("Y-m-d H:i:s");
			$mainData['last_edit_user_id']  	= 0;
			$this->db->insert($this->itemsTable, $mainData);
			$itemid = $this->db->insert_id();

			$contentData = $this->getdatalib->fillTableData($this->itemsContentTableFields, $data);
			$contentData['id']  			= '';
			$contentData['item_id']  	= $itemid;
			$this->db->insert($this->itemContentTable, $contentData);

			return $itemid;
		}


		// --------------------------------------------------------------------

//obsolete
		function changeCategory($id, $categoryid)
		{
			$data = array(
							'category_id'  => $categoryid
				);

			$this->db->where('id', $id);
			$this->db->update($this->itemsTable, $data);
			return true;
		}


		// --------------------------------------------------------------------

//obsolete
		function changeSecondCategory($id, $categoryid)
		{
			$data = array(
							'category1_id'  => $categoryid
				);

			$this->db->where('id', $id);
			$this->db->update($this->itemsTable, $data);
			return true;
		}


		// --------------------------------------------------------------------


		function getItemsCategories($itemId)
		{
			$this->db->where('item_id', $itemId);
			$query = $this->db->get($this->itemsCategoriesTable);
			$result = $query->result();

			if(isset($result[0]))
			{
				foreach($result as $key => $value)
				{
					$output[] = $value->category_id;
				}
				return $output;
			}
			return false;
		}


		// Multi Categories
		// --------------------------------------------------------------------

		function updateItemCategories($itemId, $categories = NULL)
		{
			//delete all connections
			$this->deleteItemCategories($itemId);

			//add new connections
			if(isset($categories[0]))
			{
				foreach($categories as $key => $value)
				{
					$this->addItemCategoryRelation($itemId, $value);
				}
			}
		}

		// --------------------------------------------------------------------

		function deleteItemCategories($itemId)
		{
			$tables = array($this->itemsCategoriesTable);
			$this->db->where('item_id', $itemId);
			$this->db->delete($tables);
		}

		// --------------------------------------------------------------------

		function deleteRelationsByCategoryId($categoryid)
		{
			$tables = array($this->itemsCategoriesTable);
			$this->db->where('category_id', $categoryid);
			$this->db->delete($tables);
		}

		// --------------------------------------------------------------------

		function addItemCategoryRelation($itemId, $categoryId)
		{
			$data['item_id']  		= $itemId;
			$data['category_id']		= $categoryId;
			$this->db->insert($this->itemsCategoriesTable, $data);
			return true;
		}

		// --------------------------------------------------------------------

		function getItemsIdsByCategoryId($category_id)
		{
			$this->db->where('category_id', $category_id);
			$query = $this->db->get($this->itemsCategoriesTable);
			$result = $query->result();
			if(!count($result))
			{
				return false;
			}
			else
			{
				foreach($result as $key => $value)
				{
					$output[] = $value->item_id;
				}
			}
			return $output;
		}

		// --------------------------------------------------------------------

		function getItemsIdsByCategoriesIds($categoriesIds)
		{
			$this->db->where_in('category_id', $categoriesIds);
			$query = $this->db->get($this->itemsCategoriesTable);
			$result = $query->result();
			if(!count($result))
			{
				return false;
			}
			else
			{
				foreach($result as $key => $value)
				{
					$output[] = $value->item_id;
				}
			}
			return $output;
		}

		// --------------------------------------------------------------------

		function getCategoriesIdsByItemId($item_id)
		{
			$this->db->where('item_id', $item_id);
			$query = $this->db->get($this->itemsCategoriesTable);
			$result = $query->result();
			if(!count($result))
			{
				return false;
			}
			else
			{
				foreach($result as $key => $value)
				{
					$output[] = $value->category_id;
				}
			}
			return $output;
		}

		// --------------------------------------------------------------------

		function changeParent($id, $parentid)
		{
			$data = array(
							'parent_id'  => $parentid
				);

			$this->db->where('id', $id);
			$this->db->update($this->itemsTable, $data);
			return true;
		}

		// --------------------------------------------------------------------

		function changeMainPicture($id, $filename)
		{
			$data = array(
							'main_image'  => $filename
				);

			$this->db->where('id', $id);
			$this->db->update($this->itemsTable, $data);
			return true;
		}

		// --------------------------------------------------------------------

		function removeMainPicture($id, $thumbsConfig)
		{
			$info = $this->getItemSimpleInfo($id);
			//var_dump($info);
			if($info[0]->main_image != '')
			{
				$thumbsConfig['file_name'] = $info[0]->main_image;
				$thumbsConfig['old_file_name'] = $info[0]->main_image;
				$this->load->library('picturesactionslib');
				$this->picturesactionslib->removeThumbs($thumbsConfig);

				$this->changeMainPicture($id, '');
			}
		}

		// --------------------------------------------------------------------

		function changeDate($id, $date)
		{
			$data = array(
							'event_date'  => $date
				);

			$this->db->where('id', $id);
			$this->db->update($this->itemsTable, $data);
			return true;
		}

		// --------------------------------------------------------------------

		function changeAddDate($id, $date)
		{
			$data = array(
							'add_date'  => $date
				);

			$this->db->where('id', $id);
			$this->db->update($this->itemsTable, $data);
			return true;
		}

		// --------------------------------------------------------------------

		function changeItem($data)
		{
			//check if data exists
			if($this->checkItemData($data['id'], $data['language_id'])) //if data exists -> edit item
			{
				$data1 = $data;
				unset($data1['id']);
				unset($data1['language_id']);

				$data1['item_id'] = $data['id'];
				$this->db->where('item_id', $data['id']);
				$this->db->where('language_id', $data['language_id']);

				$this->db->update($this->itemContentTable, $data1);
			}
			else //data doesn't exists -> add item
			{
				$data1 = $data;
				$data1['id']            = '';
				$data1['item_id']    	= $data['id'];

				$this->db->insert($this->itemContentTable, $data1);
			}

			//update main data
			$data2['last_edit_date']  		= date("Y-m-d H:i:s");
			$data2['last_edit_user_id']  	= 0;

			$this->db->where('id', $data['id']);
			$this->db->update($this->itemsTable, $data2);

			return true;
		}

		// --------------------------------------------------------------------

		function updateItem($id, $data)
		{
			$this->db->where('id', $id);
			$this->db->update($this->itemsTable, $data);
			return true;
		}

		// --------------------------------------------------------------------

		function changeItemExtended($data)
		{
			// check if extended fields are present
			$result = $this->getItemExtendedInfo($data['item_id']);
			$this->db->where('item_id', $data['item_id']);
			if(!$result)
			{
				// if not add
				$data['id'] = '';
				$this->db->insert($this->itemExtendedTable, $data);
			}
			else
			{
				// if so, update
				$this->db->update($this->itemExtendedTable, $data);
			}
		}

		// --------------------------------------------------------------------

		function changeRelationData($data)
		{
			$this->db->where('id', $data['id']);
			$this->db->update($this->itemsTable, $data);
			return true;
		}

		// --------------------------------------------------------------------

		function checkItemData($itemid, $languageid)
		{
			$this->db->where('item_id', $itemid);
			$this->db->where('language_id', $languageid);
			$this->db->from($this->itemContentTable);

			return $this->db->count_all_results();
		}

		// --------------------------------------------------------------------

		function removeItem($id)
		{
			// main data
			$tables = array($this->itemsTable);
			$this->db->where('id', $id);
			$this->db->delete($tables);

			// info data
			$tables = array($this->itemContentTable);
			$this->db->where('item_id', $id);
			$this->db->delete($tables);

			//category relations
			$this->updateItemCategories($id);
		}

		// --------------------------------------------------------------------

		function ifItemExists($id)
		{
			$this->db->where('id', $id);
			$this->db->from($this->itemsTable);
			return $this->db->count_all_results();
		}

		// --------------------------------------------------------------------

		function ifItemOfTypeExists($id, $typeId)
		{
			$this->db->where('id', $id);
			$this->db->where('type', $typeId);
			$this->db->from($this->itemsTable);
			return $this->db->count_all_results();
		}

		// --------------------------------------------------------------------

		function remapItems($itemid, $destinationItem)
		{
		   $data['parent_id'] = $destinationItem;

         $this->db->where('parent_id', $itemid);
         $this->db->update($this->itemsTable, $data);
		}

		// --------------------------------------------------------------------

		function remapCategoryItems($categoryid, $destinationCategoryId)
		{
			if($destinationCategoryId)
			{
				$data['category_id'] = $destinationCategoryId;
         	$this->db->where('category_id', $categoryid);
         	$this->db->update($this->itemsCategoriesTable, $data);
			}
			else
			{
				$this->deleteRelationsByCategoryId($categoryid);
			}
		}

		// --------------------------------------------------------------------

		function remapCategory1Items($itemid, $destinationItem)
		{
		   $data['category1_id'] = $destinationItem;

         $this->db->where('category1_id', $itemid);
         $this->db->update($this->itemsTable, $data);
		}

		// --------------------------------------------------------------------

		function remapPictures($itemid, $newItemId)
		{
		   $data['item_id'] = $newItemId;

         $this->db->where('item_id', $itemid);
         $this->db->update($this->picturesTable, $data);
		}

		// --------------------------------------------------------------------

		function remapVideos($itemid, $newItemId)
		{
		   $data['item_id'] = $newItemId;

         $this->db->where('item_id', $itemid);
         $this->db->update($this->videoTable, $data);
		}

		// --------------------------------------------------------------------


		function makeThumbName($fileName)
		{
			if($fileName != '')
			{
				$path_parts = pathinfo($fileName);
				$file_extension = $path_parts['extension'];
				$thumbFileName = str_replace('.'.$file_extension, '', $fileName).'_thumb.'.$file_extension;
				return $thumbFileName;
			}
			else
			{
				return false;
			}
		}


		// --------------------------------------------------------------------


		// items relations
		function getItemParents($parentid, $lang = false)
		{
			if($parentid)
			{
				$iCounter=0;
				while($parentid)
				{
					$parentInfo = $this->getItemInfo($parentid, $lang);
					// var_dump($parentInfo);

					if(!isset($parentInfo[1]))
					{
						$parentid = 0;
					}
					else
					{
						$parentid = $parentInfo[1]->parent_id;
					}


					// $parentid = $parentInfo[1]->parent_id;
					$aParents[] = $parentInfo;

					$iCounter++;
					if($iCounter>100)
					{
						print("loop error in getItemsParents()");
						exit();
					}
				}
				$aParents = array_reverse($aParents);
				return $aParents;
			}
			else
			{
				return 0;
			}
		}

		// --------------------------------------------------------------------


		function getTree($itemType, $sortColumn, $sortOrder, $filter = array())
		{
			//get all items
			$result = $this->getAllItems($itemType, 10000, 0, $sortColumn, $sortOrder, $filter);


			$res = array();
			if(isset($result[0]))
			{
				foreach($result as $key => $value)
				{
					$res[$value->id] = $value;
				}


				foreach($res as $key => $value)
				{
					if($value->parent_id != 0 && $this->ifItemOfTypeExists($value->parent_id, $itemType))
					{
						if(isset($res[$value->parent_id]))
						{
							$res[$value->parent_id]->children[] = $value;
						}
						else
						{
							array_walk_recursive($res, array('ItemsModel', 'check_tree'), $value);
						}
						unset($res[$key]);
					}
				}
			}

			return $res;
		}


		// --------------------------------------------------------------------


		function check_tree($item, $key, $value)
		{
			if($item->id == $value->parent_id)
			{
				$item->children[] = $value;
				return true;
			}
			if(isset($item->children))
			{
				array_walk_recursive($item->children, array('ItemsModel', 'check_tree'), $value);
			}
		}


		// --------------------------------------------------------------------


		function getItemChildren($itemid)
		{
			if($itemid != 0)
			{
				$this->db->select('id');
				$this->db->where('parent_id', $itemid);
				$query = $this->db->get($this->itemsTable);
				$result = $query->result();

				//get all children
				$aChildren = array();
				foreach($result as $res)
				{
					$aChildren[] = $res->id;

					//and children of children
					$aChildrenTemp = $this->getItemChildren($res->id);
					if(isset($aChildrenTemp[0]))
					{
						$aChildren = array_merge($aChildren, $aChildrenTemp);
					}
				}
				return $aChildren;
			}
			return false;
		}


		// --------------------------------------------------------------------


		function fillTableDataWithPost($fieldsTable)
		{
			$CI = & get_instance();
			if(isset($fieldsTable[0]))
			{
				foreach($fieldsTable as $key => $value)
				{
					$val = $CI->input->post($value);
					if(!$val)
					{
						$val = '';
					}
					$output[$value] = $val;
				}
			}
			return $output;
		}


		// --------------------------------------------------------------------


		function unsetTableData($fieldsTable, $data)
		{
			if(isset($fieldsTable[0]))
			{
				foreach($fieldsTable as $key => $value)
				{
					if(isset($data[$value]))
					{
						unset($data[$value]);
					}
				}
			}
			return $data;
		}
	}