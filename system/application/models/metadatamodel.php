<?php
   class MetaDataModel extends CI_Model
   {
      function __construct()
      {
         parent::__construct();
         $this->metaTable = 'meta_data';
         $this->metaInfoTable = 'meta_data_info';

         //default type
         $this->typeid = 1;

         $this->metaTableFields = $this->db->list_fields($this->metaTable);
			$this->metaInfoTableFields = $this->db->list_fields($this->metaInfoTable);
      }


		function getMetaInfoTableFields($filter = false)
		{
			if($filter)
			{
				return $this->filterFields($this->metaInfoTableFields, $filter);
			}
			else
			{
				return $this->metaInfoTableFields;
			}
		}


		// --------------------------------------------------------------------


		function filterFields($tableFields, $fieldsArray)
		{
			$output = array();
			foreach($tableFields as $key => $value)
			{
				if(!in_array($value, $fieldsArray))
				{
					$output[] = $value;
				}
			}
			return $output;
		}

      /*Types: ($typeid)
      1 - 90 items
      99 - pictures
      98 - movies
      97 - maps
      100 - 200 categories
		*/

      function setType($typeid)
      {
         $this->typeid = $typeid;
      }

      function addMetaData($itemid, $languageid, $data)
      {
         //check if meta data exists
         $query = $this->db->from($this->metaTable)->where('type_id', $this->typeid)->where('item_id', $itemid)->limit(1)->get();
         $result = $query->result();

         if(count($result))
         {
            //check if meta info data exists
            $query = $this->db->from($this->metaInfoTable)->where('meta_data_id', $result[0]->id)->where('language_id', $languageid)->limit(1)->get();
            $result1 = $query->result();
            if(count($result1))
            {
            	$this->changeInfoData($result1[0]->id, $result[0]->id, $languageid, $data);
            }
            else
            {
               $this->addInfoData($result[0]->id, $languageid, $data);
            }
         }
         else
         {
            $metaid = $this->addMainData($itemid);
            $this->addInfoData($metaid, $languageid, $data);
         }
      }


      function addMainData($itemid)
      {
         $data = array(
                     'id'           => '',
                     'type_id'      => $this->typeid,
                     'item_id'      => $itemid
                  );

         $this->db->insert($this->metaTable, $data);
         return $this->db->insert_id();
      }


      function addInfoData($metaid, $languageid, $data)
      {
         $data['id']           = '';
         $data['meta_data_id'] = $metaid;
         $data['language_id']  = $languageid;

      	//$data = array_merge($maindata, $data);

         $this->db->insert($this->metaInfoTable, $data);
         return $this->db->insert_id();
      }


      // --------------------------------------------------------------------


      function changeMainData($id, $itemid)
      {
         $data = array(
                     'type_id'      => $this->typeid,
                     'item_id'      => $itemid
                  );

         $this->db->where('id', $id);
         $this->db->update($this->metaTable, $data);
      }


      // --------------------------------------------------------------------


      function changeInfoData($id, $metaid, $languageid, $data)
      {

      	$maindata = array(
                     'meta_data_id'    => $metaid,
                     'language_id'     => $languageid
                  );

         $data = array_merge($maindata, $data);
         $this->db->where('id', $id);
         $this->db->update($this->metaInfoTable, $data);

         return $this->db->insert_id();
      }


      // --------------------------------------------------------------------


      function getSimpleData($itemid)
      {
			$query = $this->db->from($this->metaTable)->where('type_id', $this->typeid)->where('item_id', $itemid)->limit(1)->get();
         return $query->result();
      }


      // --------------------------------------------------------------------


      function getMetaData($itemid, $languageid)
      {
         $result = $this->getSimpleData($itemid);
         if(count($result))
         {
            $query = $this->db->from($this->metaInfoTable)->where('meta_data_id', $result[0]->id)->where('language_id', $languageid)->limit(1)->get();
            return $query->result();
         }
         return $result;
      }


      // --------------------------------------------------------------------


		function getMetaToFront($id, $langid, $defaultLangid)
		{
		   $result = $this->getMetaData($id, $langid);
			if(!isset($result[0]->title))
			{
				$result = $this->getMetaData($id, $defaultLangid);
			}
			return $result;
		}


		// --------------------------------------------------------------------


      function removeMetaData($itemid)
      {
         $result = $this->getSimpleData($itemid);
         if(count($result))
         {
            $tables = array($this->metaInfoTable);
            $this->db->where('meta_data_id', $result[0]->id);
            $this->db->delete($tables);


            $tables = array($this->metaTable);
            $this->db->where('type_id', $this->typeid);
            $this->db->where('item_id', $itemid);
            $this->db->delete($tables);
         }
      }


      // --------------------------------------------------------------------


      function getEmptyStructure()
      {
         $struct = array();
      	$struct[0] = new stdClass();

      	foreach($this->metaInfoTableFields as $key => $value)
      	{
            if(!empty($value))
            {
               $struct[0]->$value = '';
            }
      	}

      	return $struct;
      }

   }
