<?php
	class MessagesModel extends CI_Model
	{
		function __construct()
		{
			parent::__construct();
			$this->msgTable         = 'messages';
			$this->msgStatusesTable = 'messages_statuses';
			$this->msgTopicsTable   = 'messages_topics';
		}
		
		function addTopic($data)
		{
			$this->db->insert($this->msgTopicsTable, $data);
			return $this->db->insert_id();
		}
		
		function updateTopic($data)
		{
			$id = $data['id'];
			unset($data['id']);
			$this->db->where('id', $id);
			$this->db->update($this->msgTopicsTable, $data);
		}
		
		function getAllUserTopics($userid, $filter = false)
		{
			if($filter) 
			{
				$this->db->where('status_id', $filter);
			}
			
			$query = $this->db->order_by('last_changed_date', 'desc')->get_where($this->msgTopicsTable, array('user_id' => $userid));
			
			//$output = $this->db->count_all_results();
			$result = $query->result();

			return $result;
			//return false;
		}
		
		function deleteTopic($topicid)
		{
      	$this->db->delete($this->msgTopicsTable, array('id' => $topicid));
      	$this->db->delete($this->msgTable, array('topic_id' => $topicid));
		}
		
		
		function getAllStatuses()
		{
			$query = $this->db->get($this->msgStatusesTable);
			
			
			//$output = $this->db->count_all_results();
			$result = $query->result();
			
			if(isset($result[0]))
			{
				foreach($result as $key => $value) 
				{
					$output[$value->id] = $value;
				}
				return $output;
			}
			return false;			
			//return false;
		}
		
	
		function getAllUserMessages($userid, $topicid = false)
		{
			if($topicid) 
			{
				$this->db->where('topic_id', $topicid);
			}			
			
			$query = $this->db->order_by('add_date', 'desc')->get_where($this->msgTable, array('user_id' => $userid));
			
			//$output = $this->db->count_all_results();
			$result = $query->result();
			return $result;
			//return false;
		}
		
		function getUnreadedUserMessages($userid)
		{
			//get unreaded messages connected straight with user
			$data['user_id'] = $userid;
			$data['is_read'] = 0;
			
			$query = $this->db->where($data)->from($this->userMsgTable);				
			$output = $this->db->count_all_results();
				
			
			//get unreaded messages connected with user services
			  //get user services
			$services = $this->getAllUserServices($userid);
			$serviveMsgs = 0;
			if(isset($services[0]))
			{
				foreach ($services as $key => $value) 
				{
					$serviceIds[] = $value->id;
				}
				//get messages
				$query1 = $this->db->where('is_read', 0)->where_in('service_id', $serviceIds)->from($this->userMsgTable);
				$serviveMsgs = $this->db->count_all_results();				
			}			
			
			$output = $output + $serviveMsgs;
				
			return $output;			
		}
		
		function addUserMessage($data)
		{
			$data['add_date'] = date('Y-m-d H:i:s');
			$this->db->insert($this->msgTable, $data);
			return $this->db->insert_id();
		}
		

		function markUserMessagesAsReaded($userid)
		{
			$data['is_read'] = 1;
			$this->db->where('user_id', $userid);
			$this->db->update($this->userMsgTable, $data);
		}
		
		function checkIfUserOwner($topic_id, $user_id)
		{
			$this->db->where('user_id', $user_id);
		   $this->db->where('id', $topic_id);
		   $this->db->from($this->msgTopicsTable);
			$output = $this->db->count_all_results();
			return $output;
		}
		
		//DELETE, REMOVE METHODS
		function removeMessage($message_id)
		{
			$this->db->delete($this->userMsgTable, array('id' => $message_id));
		}	
		
		function removeTopic($topic_id)
		{
			$this->db->delete($this->msgTopicsTable, array('id' => $topic_id));
		}	
		
	}
