<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class QuestionsModel extends CI_Model
	{
		function __construct()
		{
			parent::__construct();				
			$this->questionsTable = 'questions';
		}

		function getCountQuestions($filter = array())
		{
			//filters settings
			if(count($filter))
			{				
				$this->setFilters($filter);
			}

			$this->db->from($this->questionsTable);
			$output = $this->db->count_all_results();

			return $output;
		}

		function getAllQuestions($start, $offset, $sortRules, $filter = array())
		{
			//filters settings
			if(count($filter))
			{
				$this->setFilters($filter);
			}
			//end filters

			$query = $this->db->select()
			->from($this->questionsTable)			
			->order_by($sortRules)
			->limit($start, $offset)
			->get();
		
			$result = $query->result();
			return $result;
		}


		function checkIfExists($id)
		{
			$this->db->from($this->questionsTable);
			$this->db->where('id', $id);			
			return $this->db->count_all_results();
		}	


		function getQuestion($id)
		{
			$this->db->where($this->questionsTable.'.id', $id);

			$query = $this->db->select()
			->from($this->questionsTable)
			->limit(1)
			->get();

			$result = $query->result();
			//var_dump($this->db->last_query());
			return $result;
		}
		

		function addQuestion($data)
		{			
         $data['add_date'] = date('Y-m-d H:i:s');
			$this->db->insert($this->questionsTable, $data);
         return $this->db->insert_id();
		}
		
		
		function getStatus($id)
		{
			$query = $this->db->select('is_closed')->from($this->questionsTable)->where('id', $id)->get();
			$result = $query->result();
			return $result[0]->is_closed;
		}


		function setStatus($id, $status)
		{
			$data = array(
					'is_closed' => $status
				);

			$this->db->where('id', $id);
			$this->db->update($this->questionsTable, $data);

			return true;
		}
		
		
		/*function getSpamStatus($id)
		{
			$query = $this->db->select('spam_reported')->from($this->questionsTable)->where('id', $id)->get();
			$result = $query->result();
			return $result[0]->spam_reported;
		}


		function setSpamStatus($id, $status)
		{
			$data = array(
					'spam_reported' => $status
				);

			$this->db->where('id', $id);
			$this->db->update($this->questionsTable, $data);

			return true;
		}*/
			
		
		function updateQuestion($data)
		{			
			$id = $data['id'];
			unset($data['id']);
			
			
			$this->db->where('id', $id);
         $this->db->update($this->questionsTable, $data);
			return $id;
		}

		function checkIfUserOwner($question_id, $user_id)
		{
			$this->db->where('user_id', $user_id);
		   $this->db->where('id', $question_id);
		   $this->db->from($this->questionsTable);
			$output = $this->db->count_all_results();
			return $output;
		}
		
		function getQuestionUser($user_id)
		{
			$CI = & get_instance();		
			$result = $CI->UserModel->getUser($user_id);			
			return $result;
		}
		
		/*function getEntriesIdsByUserQuestions($user_id)
		{
			$CI = & get_instance();
			$this->db->select('item_id, add_date');
			$this->db->where('user_id', $user_id);
			$this->db->group_by('item_id');		   
			$this->db->order_by('add_date desc');		   
		   $this->db->from($this->questionsTable);
		   $query = $this->db->get();
			$result = $query->result();
			
			if(isset($result[0]))
			{
				return $result;	
			}
			return false;
		}*/		
		
		
		//DELETE, REMOVE METHODS
		function removeQuestion($question_id)
		{
			$this->db->delete($this->questionsTable, array('id' => $question_id));
		}	
		
		// --------------------------------------------------------------------
		
		function removeQuestionByUserId($user_id)
		{
			$this->db->delete($this->questionsTable, array('user_id' => $user_id));
		}	
		
		
		// --------------------------------------------------------------------
				
		function setFilters($filter)
		{
			$CI = & get_instance();
			$CI->load->library('db_filter');
			$this->db_filter->setFilters($filter);
		}
	}