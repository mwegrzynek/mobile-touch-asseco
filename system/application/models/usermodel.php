<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class UserModel extends CI_Model
	{
		function __construct()
		{
			parent::__construct();
			$this->userTable     	= 'users';
			$this->userDataTable   	= 'user_meta';   
			$this->offerTable    	= 'offer';			
			$this->serviceTable 		= 'service';
			$this->userMsgTable 		= 'messages';
		}		
		
		// --------------------------------------------------------------------
		
		function getCountUsers($filter = array())
		{			
			//filters settings
			if(count($filter))
			{				
				$this->setFilters($filter);
			}

			$query = $this->db->select()
			->from($this->userTable)
			->join($this->userDataTable, $this->userTable.'.id = '.$this->userDataTable.'.user_id', 'left');
			$output = $this->db->count_all_results();
			return $output;
		}
		
		// --------------------------------------------------------------------

		function getAllUsers($start, $offset, $sortRules, $filter = array())
		{		
			//filters settings
			if(count($filter))
			{				
				$this->setFilters($filter);
			}

			$query = $this->db->select()
			->from($this->userTable)
			->join($this->userDataTable, $this->userTable.'.id = '.$this->userDataTable.'.user_id', 'left')
			->order_by($sortRules)
			->limit($start, $offset)
			->get();

			$result = $query->result();

			//var_dump($this->db->last_query());

			return $result;
		}

		// --------------------------------------------------------------------	

		function getUser($id)
		{
			$query = $this->db->select()
			->from($this->userTable)
			->join($this->userDataTable, $this->userTable.'.id = '.$this->userDataTable.'.user_id', 'left')
			->where($this->userTable.'.id', $id)
			->limit(1)
			->get();

			$result = $query->result();

			//var_dump($this->db->last_query());

			return $result;
		}
		
		// --------------------------------------------------------------------
		
		function setFilters($filter)
		{
		   $CI = & get_instance();
			$CI->load->library('db_filter');
			$this->db_filter->setFilters($filter);
		}
		
		// --------------------------------------------------------------------

		function checkIfUserExists($id)
		{
			$this->db->from($this->userTable);
			$this->db->where('id', $id);
			$this->db->where('active', 1);
			return $this->db->count_all_results();
		}
		
		// --------------------------------------------------------------------
		
		function checkIfUserExistsByTaxId($taxid)
		{
			$this->db->from($this->userDataTable);
			$this->db->where('invoice_data_taxid', $taxid);			
			return $this->db->count_all_results();
		}
		
		// --------------------------------------------------------------------

		function checkIfUserExistsByMail($mail)
		{
			$this->db->from($this->userTable);
			$this->db->where('email', $mail);
			//$this->db->where('is_active', 1);
			return $this->db->count_all_results();
		}	
		
		// --------------------------------------------------------------------
		
		function getUserByMail($mail)
		{
			$query = $this->db->get_where('user', array('email' => $mail));
			return $query->result();
		}
		
		// --------------------------------------------------------------------
		
		function getUserName($id)
		{
			$query = $this->db->select('username')->where($this->userTable.'.id', $id)->limit(1)->get($this->userTable);
			$result = $query->result();

			if(count($result))
			{
				return $result[0]->username;
			}
			return false;
		}
		
		// --------------------------------------------------------------------
		
		function getUserMail($id)
		{
			$query = $this->db->select('email')->where($this->userTable.'.id', $id)->limit(1)->get($this->userTable);
			$result = $query->result();

			if(count($result))
			{
				return $result[0]->email;
			}
			return false;
		}
		
		// --------------------------------------------------------------------
		
		function getUserHash($id)
		{
			$query = $this->db->select('hash')->where($this->userTable.'.id', $id)->limit(1)->get($this->userTable);
			$result = $query->result();

			if(count($result))
			{
				return $result[0]->hash;
			}
			return false;
		}

		// --------------------------------------------------------------------
		
		function getCompanyName($id)
		{
			$query = $this->db->select('company_name')->where($this->userTable.'.id', $id)->limit(1)->get($this->userTable);
			$result = $query->result();

			if(count($result))
			{
				return $result[0]->company_name;
			}
			return false;
		}
		
		// --------------------------------------------------------------------

		function deleteUser($id)
		{
			$CI = & get_instance();
			
			$CI->load->model('../../models/UserModel');		
			$CI->load->model('../../models/ServiceModel');		
			$CI->load->model('../../models/MessagesModel');		
			$CI->load->model('../../models/CommentsModel');		
			$CI->load->model('../../models/ItemsModel');		
			
			//delete services
			$servicesInfo = $CI->UserModel->getAllUserServices($id);
			
			if(isset($servicesInfo[0]))
			{
				foreach($servicesInfo as $service)
				{
					//var_dump($offer->id);
					$CI->ServiceModel->removeService($service->id);
					log_message('info', 'Usunięto usługę o id = '.$service->id);
				}
			}
			
			//delete comments
			$filter['customFilter'][0] = array(
															'field' => 'user_id',
															'value' => $id
														);
			$commentsInfo = $CI->CommentsModel->getAllComments(10000, 0, 'add_date desc', $filter);
			if(isset($commentsInfo[0])) 
			{
				foreach($commentsInfo as $key => $value) 
				{
					$CI->CommentsModel->removeComment($value->id);
				}
			}
			
			//delete entries
			$filter['customFilter'][0] = array(
															'field' => 'li_num_data_1',
															'value' => $id
														);			
			$entriesInfo = $CI->ItemsModel->getAllItems(4, 10000, 0, 'add_date', 'desc', $filter);
			if(isset($entriesInfo[0])) 
			{
				foreach($entriesInfo as $key => $value) 
				{
					$CI->ItemsModel->removeItem($value->id);
				}
			}
			
			
			//delete messages
			$messages = $CI->MessagesModel->getAllUserMessages($id);
			if(isset($messages[0])) 
			{
				foreach($messages as $key => $value) 
				{
					$CI->MessagesModel->removeMessage($value->id);
				}
			}
			
			//delete topics
			$topics = $CI->MessagesModel->getAllUserTopics($id);
			if(isset($topics[0])) 
			{
				foreach($topics as $key => $value) 
				{
					$CI->MessagesModel->removeTopic($value->id);
				}
			}
			
			//delete MetaData
			$tables = array($this->userDataTable);
			$this->db->where('user_id', $id);
			$this->db->delete($tables);    
			
			//delete regular Data
			$tables = array($this->userTable);
			$this->db->where('id', $id);
			$this->db->delete($tables);           
		}
		
		// --------------------------------------------------------------------

		function addAccount($data)
		{
			$this->db->insert($this->userTable, $data);
			return $this->db->insert_id();
		}
		
		// --------------------------------------------------------------------
		
		function updateAccount($data)
		{
			$id = $data['id'];
			unset($data['id']);
			unset($data['passwd']);
			$this->db->where('id', $id);
			$this->db->update($this->userTable, $data);
			return $id;
		}
		
		// --------------------------------------------------------------------
		
		function updateAccountMeta($data)
		{
			$id = $data['user_id'];
			unset($data['user_id']);
			$this->db->where('user_id', $id);
			$this->db->update($this->userDataTable, $data);
			return $id;
		}
		
		// --------------------------------------------------------------------
		
		/*function setDiscount($data)
		{
			$id = $data['user_id'];
			unset($data['user_id']);
			$this->db->where('id', $id);
			$this->db->update($this->userTable, $data);
			return $id;
		}*/
		
		// --------------------------------------------------------------------

		function renameFile($old, $new)
		{
			if(!@rename($old, $new))
			{
				if(copy($old, $new))
				{
					unlink($old);
					return true;
				}
				return false;
			}
			return true;
		}
		
		// --------------------------------------------------------------------

		function changePassword($id, $passwd)
		{
			$data = array(
					'passwd'  => $passwd
				);

			$this->db->where('id', $id);
			$this->db->update($this->userTable, $data);
			return true;
		}

		// --------------------------------------------------------------------

		function removeLogo($id)
		{
			$info = $this->getUser($id);			
			
			if($info[0]->logo != '')
			{
				unlink('../pictures/user/logos/'.$info[0]->logo);
				$this->changeImageFileName($id, '');
			}
		}
		
		// --------------------------------------------------------------------

		function changeImageFileName($id, $name)
		{
			$data = array(
					'logo'  => $name
				);

			$this->db->where('id', $id);
			$this->db->update($this->userDataTable, $data);
			return true;
		}

		// --------------------------------------------------------------------

		function getAllUserMessages($userid)
		{
			
			$query = $this->db->order_by('add_date', 'desc')->get_where($this->userMsgTable, array('user_id' => $userid));
			
			//$output = $this->db->count_all_results();
			$result = $query->result();

			return $result;
			//return false;
		}
		
		// --------------------------------------------------------------------
		
		function getUnreadedUserMessages($userid)
		{
			//get unreaded messages connected straight with user
			$data['user_id'] = $userid;
			$data['is_read'] = 0;
			
			$query = $this->db->where($data)->from($this->userMsgTable);				
			$output = $this->db->count_all_results();
				
			
			//get unreaded messages connected with user services
			  //get user services
			$services = $this->getAllUserServices($userid);
			$serviveMsgs = 0;
			if(isset($services[0]))
			{
				foreach ($services as $key => $value) 
				{
					$serviceIds[] = $value->id;
				}
				//get messages
				$query1 = $this->db->where('is_read', 0)->where_in('service_id', $serviceIds)->from($this->userMsgTable);
				$serviveMsgs = $this->db->count_all_results();				
			}			
			
			$output = $output + $serviveMsgs;
				
			return $output;			
		}
		
		// --------------------------------------------------------------------
		
		function addUserMessages($data)
		{
			$data['add_date'] = date('Y-m-d H:i:s');
			$this->db->insert($this->userMsgTable, $data);
			return $this->db->insert_id();
		}
		
		// --------------------------------------------------------------------

		function markUserMessagesAsReaded($userid)
		{
			$data['is_read'] = 1;
			$this->db->where('user_id', $userid);
			$this->db->update($this->userMsgTable, $data);
		}
		
		// --------------------------------------------------------------------

		function getAllUserServices($userid)
		{			
			$query = $this->db->order_by('add_date', 'desc')->get_where($this->serviceTable, array('user_id' => $userid));			
			//$output = $this->db->count_all_results();
			$result = $query->result();

			return $result;
			//return false;
		}
	}