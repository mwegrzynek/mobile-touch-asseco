<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class CategoryModel extends CI_Model
	{
		function __construct()
		{
			parent::__construct();
			$this->categoryTable = 'category';
			$this->categoryNamesTable = 'category_name';
			$this->itemsCategoriesTable = 'item_category';
			
			$this->categoryTableFields = $this->db->list_fields($this->categoryTable);
			$this->categoryContentTableFields = $this->db->list_fields($this->categoryNamesTable);		
		}
		
		
		// --------------------------------------------------------------------
		
		
		function getTableFields()
		{
			return $this->categoryTableFields;
		}
		
		
		// --------------------------------------------------------------------
		
		
		function getContentTableFields()
		{
			return $this->categoryContentTableFields;
		}
		
		
		// --------------------------------------------------------------------


		function setDefaultLanguage($id)
		{
			$this->defaultLanguage = $id;
		}

		// --------------------------------------------------------------------

		
		function getDefaultLanguage()
		{
			return $this->defaultLanguage;
		}
		
		
		// --------------------------------------------------------------------
		
		// obsolete
		/*function setTables($tables)
		{
			$this->categoryTable = $tables['categoryTable'];
			$this->categoryNamesTable = $tables['categoryNamesTable'];
		}*/
		
		
		// --------------------------------------------------------------------
		

		function getCategoryInfo($id, $languageId)
		{
			$this->db->where('category_id', $id);
			$this->db->where('language_id', $languageId);
			$query = $this->db->get($this->categoryNamesTable);
			return $query->result();
		}
		
		
		// --------------------------------------------------------------------
		

		function getCategorySimpleInfo($id)
		{
			$this->db->where('id', $id);
			$query = $this->db->get($this->categoryTable);
			return $query->result();
		}
		
		
		// --------------------------------------------------------------------
		
		
		function getCategoryFullInfo($categoryid)
		{
			$query = $this->db->select(/*$this->categoryTable.'.id, '.
												$this->categoryTable.'.parent_id, '.
												$this->categoryTable.'.is_default, '.
												$this->categoryNamesTable.'.name, '.
												$this->categoryNamesTable.'.slug, '.
												$this->categoryNamesTable.'.position, '.                               
												$this->categoryNamesTable.'.is_active, '.												
												$this->categoryNamesTable.'.description, '.												
												$this->categoryNamesTable.'.language_id, '.
												$this->categoryTable.'.file_name'*/)
			->from($this->categoryTable)
			->join($this->categoryNamesTable, $this->categoryTable.'.id = '.$this->categoryNamesTable.'.category_id', 'left')
			->where($this->categoryNamesTable.'.language_id', $this->defaultLanguage)
			->where($this->categoryTable.'.id', $categoryid)
			->limit(1)
			->get();

			$result = $query->result();
			return $result;
		}
		
		
		// --------------------------------------------------------------------


		function getCategoryName($id, $languageId)
		{
			$result = $this->getCategoryInfo($id, $languageId);
			
			if(!empty($result))
			{
				return $result[0]->name;
			}
			
			return '';
		}
		
		
		// --------------------------------------------------------------------
		

		function getCategoryChildren($id)
		{
			if($id != 0)
			{
				$this->db->select('id');
				$this->db->where('parent_id', $id);
				$query = $this->db->get($this->categoryTable);
				$result = $query->result();

				//get all children
				$aChildren = array();
				foreach($result as $res)
				{
					$aChildren[] = $res->id;

					//and children of children
					$aChildrenTemp = $this->getCategoryChildren($res->id);
					if(isset($aChildrenTemp[0]))
					{
						$aChildren = array_merge($aChildren, $aChildrenTemp);
					}
				}
				return $aChildren;
			}
			return false;
		}
		
		
		// --------------------------------------------------------------------


		function getParent($id)
		{
			$this->db->select('parent_id');
			$this->db->where('id', $id);

			$query = $this->db->get($this->categoryTable);
			$result = $query->result();
			return $result[0]->parent_id;
		}
		
		
		// --------------------------------------------------------------------


		function getCategoryParents($id)
		{
			if($id)
			{
				$iCounter=0;
				while($id)
				{
					$id = $this->getParent($id);
					$aParents[] = $id;

					$iCounter++;
					if($iCounter>100)
					{
						print("loop error in getCategoryParents()");
						exit();
					}
				}
				$aParents=array_reverse($aParents);
				return $aParents;
			}
			else
			{
				return 0;
			}
		}
		
		
		// --------------------------------------------------------------------
		
		
		function getCategoryParentsInfo($parentid)
		{
			$aParents = array();			
			
			if($parentid)
			{
				$iCounter=0;
				while($parentid)
				{
					$parentInfo = $this->getCategoryFullInfo($parentid);
					
					$parentid = false;
					if(isset($parentInfo[0])) 
					{
						$parentid = $parentInfo[0]->parent_id;
						$aParents[] = $parentInfo[0];
					}					
					

					$iCounter++;
					if($iCounter>100)
					{
						print("loop error in getCategoryParents()");
						exit();
					}
				}
				$aParents=array_reverse($aParents);	
							
				return $aParents;
			}
			else
			{
				return 0;
			}
		}


		function getAllCategories($id=0)
		{
			//when $id is given function gets all categories that category with given $id can be added to
			//it cannot be this category itself and any of its children

			$aOutput = array();

			$this->db->select('id');
			$query = $this->db->get($this->categoryTable);

			$result = $query->result();

			$forbiddenCategories = array();
			if($id)
			{
				$forbiddenCategories = $this->getCategoryChildren($id);
				array_push($forbiddenCategories, $id);
			}


			foreach($result as $res)
			{
				if(!in_array($res->id, $forbiddenCategories))
				{
					$aOutput[] = $res->id;
				}
			}

			return $aOutput;
		}


		// --------------------------------------------------------------------
		
		

		/*function getCategoriesInfo($aCategoriesId, $languageId, $getparents=false, $sortInfo=array())
		{
			if(is_array($aCategoriesId))
			{
				foreach($aCategoriesId as $key => $id)
				{
					if($id)
					{
						$result = $this->getCategoryInfo($id, $languageId);
						$aOutput[$key]['id']          = $id;
						$aOutput[$key]['name']        = $result[0]->name;
						$aOutput[$key]['slug']        = $result[0]->slug;
						$aOutput[$key]['position']    = $result[0]->position;

						if($getparents)
						{
							$aOutput[$key]['parents'] = $this->getCategoriesInfo($this->getCategoryParents($id), $languageId);
						}

						//if sorting is on
						if(count($sortInfo))
						{
							if($sortInfo['sortby'] != '')
							{
								$sorting[$key] = $aOutput[$key][$sortInfo['sortby']];
							}
						}
					}
					else
					{
						//for category parents case
						$aOutput[$key]['id'] = $id;
						$aOutput[$key]['name'] = 'Root';
					}
				}

				//sorting
				if(count($sortInfo))
				{
					array_multisort($sorting, $sortInfo['sortOrder'], $sortInfo['sortType'], $aOutput);
				}

				return $aOutput;
			}
		}*/
		
		
		function getCategoryIdBySlug($slug, $langid)
		{
			$this->db->where('slug', $slug);
			$this->db->where('language_id', $langid);
			$this->db->select('category_id');
			$query = $this->db->get($this->categoryNamesTable);
			$result = $query->result();
			
			if(isset($result[0]))
			{
				return $result[0]->category_id;		
			}

			return false;	
		}
		
		function getCategoryBySlug($slug, $langid, $categoryType)
		{
			$this->db->where('type', $categoryType);			
			$this->db->where('slug', $slug);
			$this->db->where('language_id', $langid);
			
			$query = $this->db->select($this->categoryTable.'.id, '.
												$this->categoryTable.'.parent_id, '.
												$this->categoryTable.'.is_default, '.
												$this->categoryNamesTable.'.name, '.
												$this->categoryNamesTable.'.slug, '.
												$this->categoryNamesTable.'.position, '.                               
												$this->categoryNamesTable.'.is_active, '.												
												$this->categoryNamesTable.'.description, '.												
												$this->categoryNamesTable.'.language_id, '.
												$this->categoryTable.'.file_name')
			->from($this->categoryTable)
			->join($this->categoryNamesTable, $this->categoryTable.'.id = '.$this->categoryNamesTable.'.category_id', 'left')
			->limit(1)
			->get();

			$result = $query->result();
			return $result;
		}
		
		// --------------------------------------------------------------------
		
		function getCategoryById($id, $langid)
		{
			$this->db->where($this->categoryTable.'.id', $id);			
			$this->db->where('language_id', $langid);
			
			$query = $this->db->select($this->categoryTable.'.id, '.
												$this->categoryTable.'.parent_id, '.
												$this->categoryTable.'.is_default, '.
												$this->categoryNamesTable.'.name, '.
												$this->categoryNamesTable.'.slug, '.
												$this->categoryNamesTable.'.position, '.                               
												$this->categoryNamesTable.'.is_active, '.												
												$this->categoryNamesTable.'.description, '.												
												$this->categoryNamesTable.'.language_id, '.
												$this->categoryTable.'.file_name')
			->from($this->categoryTable)
			->join($this->categoryNamesTable, $this->categoryTable.'.id = '.$this->categoryNamesTable.'.category_id', 'left')
			->limit(1)
			->get();

			$result = $query->result();
			return $result;
		}


		/*for faster backend*/

		function getCountCategories($typeid, $filter = array())
		{
			$this->db->where('type', $typeid);
			
			if(count($filter))
			{
				$this->setFilters($filter);
			}

			$query = $this->db->select(/*$this->categoryTable.'.id, '.
												$this->categoryNamesTable.'.type, '.
												$this->categoryNamesTable.'.name, '.
												$this->categoryNamesTable.'.slug, '.
												$this->categoryTable.'.parent_id, '*/)
			->from($this->categoryNamesTable)
			->join($this->categoryTable, $this->categoryTable.'.id = '.$this->categoryNamesTable.'.category_id', 'left')
			->where($this->categoryNamesTable.'.language_id', $this->defaultLanguage);

			$output = $this->db->count_all_results();
			return $output;
		}
		
		
		// --------------------------------------------------------------------
		

		function getCategories($typeid, $start, $offset, $sortRules, $filter = array())
		{
			$this->db->where('type', $typeid);
			
			if(count($filter))
			{
				$this->setFilters($filter);
			}

			$query = $this->db->select(/*$this->categoryTable.'.id, '.
												$this->categoryNamesTable.'.category_id, '.
												$this->categoryNamesTable.'.language_id, '.
												$this->categoryNamesTable.'.name, '.
												$this->categoryNamesTable.'.is_active, '.
												$this->categoryTable.'.parent_id, '.
												$this->categoryTable.'.is_default, '.
												$this->categoryNamesTable.'.slug, '.
												$this->categoryNamesTable.'.position, '.                               
												$this->categoryNamesTable.'.description, '.                               
												$this->categoryTable.'.file_name'*/)
			->from($this->categoryNamesTable)
			->join($this->categoryTable, $this->categoryTable.'.id = '.$this->categoryNamesTable.'.category_id', 'left')
			->where($this->categoryNamesTable.'.language_id', $this->defaultLanguage)
			->order_by($sortRules)
			->limit($start, $offset)
			->get();

			$result = $query->result();
			return $result;
		}
		
		function setFilters($filter)
		{
		   $CI = & get_instance();
			$CI->load->library('db_filter');
			$this->db_filter->setFilters($filter);
		}

		/*end for faster backend*/

		/*function getCategoriesInfoLang($aCategoriesId, $languageId, $defaultLangid, $sortInfo=array())
		{
			if(is_array($aCategoriesId))
			{
				//default lang
				if(is_array($aCategoriesId) && count($aCategoriesId))
				{
					$this->db->where_in('category_id', $aCategoriesId);
				}

				$this->db->where('language_id', $defaultLangid);
				$this->db->from($this->categoryNamesTable);
				$query = $this->db->get();
				$result = $query->result();


				foreach($result as $res)
				{
					$categories[$res->category_id]['id']          = $res->category_id;
					$categories[$res->category_id]['name']        = $res->name;
					$categories[$res->category_id]['slug']        = $res->slug;
					$categories[$res->category_id]['position']    = $res->position;

					$position[$res->category_id] = $res->position;
					$name[$res->category_id] = $res->name;
				}

				//given lang
				if(is_array($aCategoriesId) && count($aCategoriesId))
				{
					$this->db->where_in('category_id', $aCategoriesId);
				}

				$this->db->where('language_id', $languageId);
				$this->db->from($this->categoryNamesTable);
				$query = $this->db->get();
				$result = $query->result();


				foreach($result as $res)
				{
					$categoriesGivenLng[$res->category_id]['id']          = $res->category_id;
					$categoriesGivenLng[$res->category_id]['name']        = $res->name;
					$categoriesGivenLng[$res->category_id]['slug']        = $res->slug;
					$categoriesGivenLng[$res->category_id]['position']    = $res->position;
				}

				if(isset($categoriesGivenLng))
				{
					foreach($categories as $key => $cat)
					{
						if(isset($categoriesGivenLng[$key]))
						{
							$categories[$key]['name'] = $categoriesGivenLng[$key]['name'];
							$categories[$key]['slug'] = $categoriesGivenLng[$key]['slug'];

							$position[$key] = $categoriesGivenLng[$key]['position'];
							$name[$key] = $categoriesGivenLng[$key]['name'];
						}
					}
				}

				//array_multisort($position, SORT_DESC, SORT_NUMERIC, $name, SORT_ASC, SORT_STRING, $categories);
				array_multisort($name, SORT_ASC, SORT_STRING, $categories);
				return $categories;
			}
		}*/		
		
		
		// --------------------------------------------------------------------
		
		
		function makeSlug($title, $langId, $typeid)
		{
			$this->load->library('slug');
			$slug = $this->slug->makeSlugs($title);
			$slug = $this->checkSlug($slug, $langId, $typeid);
			return $slug;
		}
		
		
		// --------------------------------------------------------------------


		function checkSlug($slug, $langId, $typeid)
		{
			if($slug == '')
			{			
				return $slug = 'please-write-slug-manually';			
			}

			$counter = 1;
			do
			{
				if($result = $this->checkIfSlugExists($slug, $langId, $typeid))
				{
					$slug = $slug.$counter;
				}
				$counter++;
				if($counter > 100)
				{
					show_error('_checkSlug() loop error');
					break;
				}
			}
			while($result);
			return $slug;
		}
		
		
		// --------------------------------------------------------------------
		
		
		function checkIfSlugExists($slug, $langid, $typeid)
		{
			$this->db->where('type', $typeid);			
			$this->db->where('slug', $slug);
			$this->db->where('language_id', $langid);
			
			$query = $this->db->select()
			->from($this->categoryTable)
			->join($this->categoryNamesTable, $this->categoryTable.'.id = '.$this->categoryNamesTable.'.category_id', 'left')			
			->limit(1)
			->get();
			
			$result = $query->result();
			return count($result);
		}
		

		// --------------------------------------------------------------------


		/*function checkSlug($slug, $langid, $typeid)
		{
			$query = $this->db->select($this->categoryTable.'.id, '.												
												$this->categoryTable.'.type, '.												
												$this->categoryNamesTable.'.slug, '.												
												$this->categoryNamesTable.'.language_id')
			->from($this->categoryTable)
			->join($this->categoryNamesTable, $this->categoryTable.'.id = '.$this->categoryNamesTable.'.category_id', 'left')
			->where($this->categoryNamesTable.'.language_id', $langid)
			->where($this->categoryNamesTable.'.slug', $slug)			
			->where($this->categoryTable.'.type', $typeid)			
			->get();			
			
			$result = $query->result();
			
			//var_dump($this->db->last_query());
			//var_dump($result);			
			
			return count($result);
		}*/
		
		
		// --------------------------------------------------------------------
		

		function addCategory($data)
		{
			$this->load->library('getdatalib');			
			
			$data1 = $this->getdatalib->fillTableData($this->categoryTableFields, $data);
			
			$data1['id'] = '';
			
			/*$data1['type'] 		= $data['type'];        
			$data1['parent_id'] 	= $data['parent_id'];        
			$data1['file_name'] 	= $data['file_name'];*/
			//$data1['is_default'] = 0;

			$this->db->insert($this->categoryTable, $data1);
			$categoryid = $this->db->insert_id();
			$data2 = $this->getdatalib->fillTableData($this->categoryContentTableFields, $data);

			$data2['id']            = '';
			$data2['category_id']   = $categoryid;
			$data2['language_id']   = $data['language_id'];
			/*$data2['name']          = $data['name'];
			$data2['slug']          = $data['slug'];
			$data2['position']      = $data['position'];
			$data2['description']   = $data['description'];
			$data2['is_active']		= $data['is_active'];*/
			$this->db->insert($this->categoryNamesTable, $data2);
			
			return $categoryid;
		}


		/*function getMenu($languageId, $defaultLanguage)
		{
			//var_dump($languageId);
			//var_dump($defaultLanguage);
			$root = $this->getStraightChildrenFullInfo(0, $languageId, $defaultLanguage);

			foreach($root as $key => $rootCat)
			{
				$root[$key]['children'] = $this->getStraightChildrenFullInfo($rootCat['categoryid'], $languageId, $defaultLanguage);
			}
			
			return $root;
		}


		function getStraightChildrenFullInfo($categoryid, $languageId, $defaultLanguage)
		{
			///default language
			$this->db->where('parent_id', $categoryid);
			$this->db->where($this->categoryNamesTable.'.language_id', $defaultLanguage);
			$query = $this->db->select($this->categoryTable.'.id, '.
												$this->categoryTable.'.parent_id, '.
												$this->categoryTable.'.is_default, '.
												$this->categoryTable.'.file_name, '.                                   
												$this->categoryNamesTable.'.name, '.
												$this->categoryNamesTable.'.slug, '.
												$this->categoryNamesTable.'.language_id, '.
												$this->categoryNamesTable.'.position')
			->from($this->categoryTable)

			->join($this->categoryNamesTable, $this->categoryNamesTable.'.category_id = '.$this->categoryTable.'.id', 'left')
			->get();
			$result = $query->result();

			$categories = array();

			if(count($result))
			{
				foreach($result as $key => $res)
				{
					$categories[$res->id]['categoryid'] = $res->id;
					$categories[$res->id]['category_name'] = htmlspecialchars($res->name);
					$categories[$res->id]['category_slug'] = $res->slug;               
					$categories[$res->id]['file_name'] 		= $res->file_name;               

					$position[$res->id] = $res->position;
					$name[$res->id] = $res->name;
				}
			}

			//given language
			$this->db->where('parent_id', $categoryid);
			$this->db->where($this->categoryNamesTable.'.language_id', $languageId);
			$query = $this->db->select($this->categoryTable.'.id, '.
												$this->categoryTable.'.parent_id, '.
												$this->categoryTable.'.is_default, '.
												$this->categoryTable.'.file_name, '.                                   
												$this->categoryNamesTable.'.name, '.
												$this->categoryNamesTable.'.slug, '.
												$this->categoryNamesTable.'.language_id, '.
												$this->categoryNamesTable.'.position')
			->from($this->categoryTable)

			->join($this->categoryNamesTable, $this->categoryNamesTable.'.category_id = '.$this->categoryTable.'.id', 'left')
			->get();
			$result = $query->result();

			if(count($result))
			{
				foreach($result as $key => $res)
				{
					$categoriesGivenLng[$res->id]['categoryid'] = $res->id;
					$categoriesGivenLng[$res->id]['category_name'] = htmlspecialchars($res->name);
					$categoriesGivenLng[$res->id]['category_slug'] = $res->slug;
					$categoriesGivenLng[$res->id]['file_name']	  = $res->file_name;                
					$categoriesGivenLng[$res->id]['position'] 	  = $res->position;
				}
			}


			if(isset($categoriesGivenLng))
			{
				foreach($categories as $key => $cat)
				{
					if(isset($categoriesGivenLng[$key]))
					{
						$categories[$key]['category_name'] = $categoriesGivenLng[$key]['category_name'];
						$categories[$key]['category_slug'] = $categoriesGivenLng[$key]['category_slug'];

						$position[$key] = $categoriesGivenLng[$key]['position'];						
						$name[$key] = $categoriesGivenLng[$key]['category_name'];
					}
				}
			}

			if(count($categories))
			{
				array_multisort($position, SORT_ASC, SORT_NUMERIC, $name, SORT_ASC, SORT_STRING, $categories);
				//array_multisort($name, SORT_ASC, SORT_STRING, $categories);				
			}
			return $categories;
		}*/


		function changeParent($id, $parentid)
		{
			$data = array(
					'parent_id'  => $parentid
				);

			$this->db->where('id', $id);
			$this->db->update($this->categoryTable, $data);
			return true;
		}


		function changeMainData($data)
		{
			$id = $data['id'];
			unset($data['id']);
			$this->db->where('id', $id);
			$this->db->update($this->categoryTable, $data);
			return $id;
		}


		function changeCategory($data)
		{
			//check if data exists
			if($this->checkCategoryData($data['category_id'], $data['language_id'])) //data exists edit them
			{				
				$data1 = $this->getdatalib->fillTableData($this->categoryContentTableFields, $data);				
				unset($data1['id']);
				
				$this->db->where('category_id', $data['category_id']);
				$this->db->where('language_id', $data['language_id']);


				$this->db->update($this->categoryNamesTable, $data1);
			}
			else //data doesn't exists add them
			{
				$data1 = $this->getdatalib->fillTableData($this->categoryContentTableFields, $data);
				$this->db->insert($this->categoryNamesTable, $data1);
			}

			return true;
		}

		function checkCategoryData($categoryid, $languageid)
		{
			$this->db->where('category_id', $categoryid);
			$this->db->where('language_id', $languageid);
			$this->db->from($this->categoryNamesTable);

			return $this->db->count_all_results();
		}


		function removeCategory($id)
		{
			// main data
			$tables = array($this->categoryTable);
			$this->db->where('id', $id);
			$this->db->delete($tables);

			// info data
			$tables = array($this->categoryNamesTable);
			$this->db->where('category_id', $id);
			$this->db->delete($tables);
		}


		function ifCategoryExists($id, $type = NULL)
		{
			$this->db->where('id', $id);
			
			if($type !== NULL) 
			{
				$this->db->where('type', $type);
			}
			
			$this->db->from($this->categoryTable);
			return $this->db->count_all_results();
		}

		function remapCategories($parentid, $destinationParentId)
		{
			$data['parent_id'] = $destinationParentId;

			$this->db->where('parent_id', $parentid);
			$this->db->update($this->categoryTable, $data);
		}


		function getCategoryInfoLang($id, $langid, $defaultLangid)
		{
			$info = $this->getCategoryInfo($id, $langid);
			if(!isset($info[0]) || empty($info[0]->name))
			{
				$info = $this->getCategoryInfo($id, $defaultLangid);
			}

			if(empty($info))
			{
				return false;			
			}
			
			$simpleInfo = $this->getCategorySimpleInfo($id);
			$info[0]->simple_info = $simpleInfo;
			if($simpleInfo[0]->parent_id)
			{
				$info[0]->parent_info = $this->getCategoryInfoLang($simpleInfo[0]->parent_id, $langid, $defaultLangid);
			}

			return $info;
		}
		
		
		function getCategoryNameLang($id, $langid, $defaultLangid)
		{
			$info = $this->getCategoryInfo($id, $langid);
			if(!isset($info[0]) || empty($info[0]->name))
			{
				$info = $this->getCategoryInfo($id, $defaultLangid);
			}
			
			if(!isset($info[0]) || empty($info[0]->name))
			{
				return false;
			}
			return $info[0]->name;
		}

		function removeImage($id, $path)
		{
			$info = $this->getCategorySimpleInfo($id);
			//var_dump($info);
			if($info[0]->file_name != '')
			{
				unlink($path.'/'.$info[0]->file_name);
				if(is_file($path.'/'.$this->makeThumbName($info[0]->file_name)))
				{
					unlink($path.'/'.$this->makeThumbName($info[0]->file_name));
				}				
				$this->changeImageFileName($id, '');
			}         
		}

		function changeImageFileName($id, $name)
		{
			$data = array(               
					'file_name'  => $name         
				);
				
			$this->db->where('id', $id);
			$this->db->update($this->categoryTable, $data);
			return true;
		}


		function makeThumbName($fileName)
		{
			if($fileName != '')
			{
				$path_parts = pathinfo($fileName);
				$file_extension = $path_parts['extension'];
				$thumbFileName = str_replace('.'.$file_extension, '', $fileName).'_thumb.'.$file_extension;
				return $thumbFileName;
			}
			else
			{
				return false;
			}
		}
		
		

		// --------------------------------------------------------------------
		
		
		function getTree($itemType, $sortRules, $filter = array())
		{
			//get all items
			$result = $this->getCategories($itemType, 10000, 0, $sortRules, $filter);
			
			
			if(isset($result[0])) 
			{
				foreach($result as $key => $value) 
				{
					$res[$value->id] = $value;
				}
				
				foreach($res as $key => $value) 
				{
					if($value->parent_id != 0) 
					{
						if(isset($res[$value->parent_id])) 
						{
							//$res[$value->parent_id]->children[] = $value;						
							$res[$value->parent_id]->children[$value->id] = $value;						
						}
						else
						{
							array_walk_recursive($res, array('CategoryModel', 'check_tree'), $value);								
						}
						unset($res[$key]);
					}
				}	
				return $res;	
			}
			return false;			
		}
		
		function check_tree($item, $key, $value)
		{		
			if($item->id == $value->parent_id) 
			{
				//$item->children[] = $value;
				$item->children[$value->id] = $value;
				return true;
			}
			if(isset($item->children))
			{
				array_walk_recursive($item->children, array('CategoryModel', 'check_tree'), $value);
			}			
		}
		
		
		function getTreeWithChildItems($itemType, $sortRules, $filter = array(), $childItemType, $childSortRules, $childSortOrder, $childFilter = array())
		{
			//get all items
			$result = $this->getCategories($itemType, 10000, 0, $sortRules, $filter);			
			
			if(isset($result[0])) 
			{				
				$CI = & get_instance();
				$CI->load->model('ItemsModel');				
				
				foreach($result as $key => $value) 
				{
					$res[$value->id] = $value;
					
					$res[$value->id]->childItems = false;
					
					
					$itemsIds = $CI->ItemsModel->setDefaultLanguage($this->defaultLanguage);					
					$itemsIds = $CI->ItemsModel->getItemsIdsByCategoryId($value->id);					
					
					
					$childFilter['customFilter'][0]['in'] = array(					
																'value' => $itemsIds,
																'field' => $CI->db->dbprefix.'items.id'
															);
					$items = $CI->ItemsModel->getAllItems($childItemType, 1000, 0, $childSortRules, $childSortOrder, $childFilter); 					
					
					if(isset($items[0]))
					{
						$res[$value->id]->childItems = $items;
					}					
				}
				
				foreach($res as $key => $value) 
				{
					if($value->parent_id != 0) 
					{
						if(isset($res[$value->parent_id])) 
						{
							$res[$value->parent_id]->children[] = $value;						
						}
						else
						{
							array_walk_recursive($res, array('CategoryModel', 'check_tree'), $value);								
						}
						unset($res[$key]);
					}
				}	
				return $res;	
			}
			return false;			
		}


		/**
		 * Get given category items count
		 *
		 * @access	public
		 * @param	integer $categoryId
		 * @return	integer $itemCount
		 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
		 *	@copyright Copyright (c) 2011, Maciej Węgrzynek
		 */
		function getCategoryItemsCount($categoryId)
		{
			$this->db->where('category_id', $categoryId);
			$query = $this->db->from($this->itemsCategoriesTable)->count_all_results();
						
			return $query;		
		}
		
	// --------------------------------------------------------------------


		/*functions for slug problems*/
		function getAllCategoryInfo($langId)
		{
			$this->db->where('language_id', $langId);
			$query = $this->db->get($this->categoryNamesTable);
			$result = $query->result();
			return $result;
		}

		function saveSlug($id, $slug)
		{
			$data['slug'] = $slug;
			$this->db->where('id', $id);
			$this->db->update($this->categoryNamesTable, $data);
		}
	}