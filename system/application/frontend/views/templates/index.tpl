{include file='header.tpl'}
	{include file="homepage/mainSlider.tpl"}
	{include file="homepage/videoList.tpl"}
	{include file="homepage/companyBox.tpl"}
	{if $currentLanguageInfo[0]->lang_code eq 'en' or $currentLanguageInfo[0]->lang_code eq 'de'}
		{include file="homepage/whySection.tpl"}
	{/if}
	{include file="homepage/testimonials.tpl"}
	{include file="homepage/contactBox.tpl" external=false}
	{include file="homepage/infoBox.tpl"}
	{include file="homepage/logosSlider.tpl"}
{include file='footer.tpl'}