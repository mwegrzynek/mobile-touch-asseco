{include file='header.tpl'}
<section class="box-l bl-a">
	<ul class="b-crumbs">
		<li><a href="{$base_url}">{$mainSites[609]->title}</a></li>
		<li>{$mainSites[219]->title}</li>
	</ul>

	<div class="cols-two-f">
		<div class="primary-tf">
			<section class="box-r">
				<h1 class="header-h hh-b">{$mainSites[219]->title}</h1>
				<p class="text-d td-a td-c">{$mainSites[219]->lead|nl2br}</p>
			</section>
			{if !$success}
				<ul class="list-m lstm-a tabs-a">
					<li class="item-4"><a href="#business">{$siteTexts[655]->content|strip_tags}</a></li>
					<li class="item-4 last"><a href="#request">{$siteTexts[654]->content|strip_tags}</a></li>
				</ul>
				<div class="panes-a pna-a">
					<section id="business" class="pane-a box-n bn-a">
						<form action="{$siteRoutes[219]->slug}.html" method="post" class="form-a fa-a" id="contact-form">
							{if $errors.full}
							<section class="info-box error">
								<h2>{$siteTexts[661]->content|strip_tags}</h2>
								<ul>
									{foreach $errors.full as $item}
										<li>{$item}</li>
									{/foreach}
								</ul>
							</section><!-- .error -->
							{/if}

							{include file="contact/shortFormFields.tpl" type='full'}
							<p class="field-a">
								<label for="phone">{$siteTexts[649]->content|strip_tags}:</label>
								<span class="f-container"><input type="text" name="phone" value="{$backData.full.phone}" id="phone" data-error-required="{$siteTexts[669]->content|strip_tags}"/></span>
							</p>
							<p class="field-a">
								<label for="company">{$siteTexts[650]->content|strip_tags}:</label>
								<span class="f-container"><input type="text" name="company" value="{$backData.full.company}" id="company" data-error-required="{$siteTexts[670]->content|strip_tags}"/></span>
							</p>
							<p class="field-a">
								<label for="country">{$siteTexts[651]->content|strip_tags}:</label>
								<span class="f-container">
									<select name="country" id="country">
										{foreach $countries as $item}
											<option value="{$item->id}">{$item->title}</option>
										{/foreach}
											<option value="0">{$siteTexts[843]->content|strip_tags}</option>
									</select>
								</span>
							</p>
							<p class="field-a">
								<label for="message">{$siteTexts[652]->content|strip_tags}:</label>
								<span class="f-container">
									<textarea id="message" name="message" cols="30" rows="10" data-error-required="{$siteTexts[671]->content|strip_tags}">{$backData.full.company}</textarea>
								</span>
							</p>
							<p class="btn">
								<input type="hidden" name="form-type" value="full" id="form-type"/>
								<button type="submit" class="link-a">{$siteTexts[653]->content|strip_tags}</button>
							</p>
						</form>
					</section>
					<section id="request" class="pane-a box-n bn-a">
						<form action="{$siteRoutes[219]->slug}.html" method="post" class="form-a fa-a no-ajax" id="request-contact-form">
							{if $errors.short}
							<section class="info-box error">
								<h2>{$siteTexts[661]->content|strip_tags}</h2>
								<ul>
									{foreach $errors.short as $item}
										<li>{$item}</li>
									{/foreach}
								</ul>
							</section><!-- .error -->
							{/if}
							{include file="contact/shortFormFields.tpl" counter=1  type='short'}
							<p class="btn">
								<input type="hidden" name="form-type" value="short" id="form-type"/>
								<input type="hidden" name="force-contact-redirect" value="1" id="force-contact-redirect"/>
								<button type="submit" class="link-a">{$siteTexts[653]->content|strip_tags}</button>
							</p>
						</form>
					</section>
				</div>
				<p class="text-k tk-a">{$siteTexts[922]->content|strip_tags|nl2br}</p>
			{else}
				<section class="box-y">
				<h1 class="header-e">{$siteTexts[675]->content|strip_tags}</h1>
				<p>{$siteTexts[676]->content|strip_tags}</p>
				<ul class="list-u">
					<li><a href="{$siteTexts[678]->content|strip_tags}" class="link-b lnkb-a lnkb-c">{$siteTexts[677]->content|strip_tags}</a></li>
					<li><a href="{$siteTexts[680]->content|strip_tags}" class="link-b lnkb-a lnkb-c">{$siteTexts[679]->content|strip_tags}</a></li>
					<li><a href="{$siteTexts[682]->content|strip_tags}" class="link-b lnkb-a lnkb-c">{$siteTexts[681]->content|strip_tags}</a></li>
				</ul>
				</section><!-- .box-y -->
			{/if}
		</div>
		<div class="secondary-tf">
			{if $partners}
				{foreach $partners as $item}
					<section class="box-x text-area-b vcard">
						<h2 class="header-l hl-a">{$siteTexts[668]->content|strip_tags}</h2>
						{if $item->name}
							<h3 class="header-m fn">{$item->name}</h3>
						{/if}

						{if $item->person_position}
							<p class="role">{$item->person_position}</p>
						{/if}
						<p>
							<span class="org">{$item->company_name}</span><br/>
							<span class="adr">
								<span class="street-address">{$item->address_street}</span>
								<span class="postal-code">{$item->address_zipcode}</span>
								<span class="locality">{$item->address_city}</span><br/>
								<span class="country-name">{$item->country_name}</span>
							</span>
						</p>
						<p class="text-e">
							{if $item->phone}<span class="tel">{$siteTexts[672]->content|strip_tags}. <span class="value">{$item->phone}</span></span><br/>{/if}
							{if $item->mobile_phone}<span class="tel">{$siteTexts[673]->content|strip_tags}. <span class="value">{$item->mobile_phone}</span></span><br/>{/if}
							{if $item->fax}<span class="tel"><span class="type">{$siteTexts[674]->content|strip_tags}</span> <span class="value">{$item->fax}</span></span>{/if}
						</p>
						<p class="text-e"><a href="mailto:{$item->mail_personal}">{$item->mail_personal}</a></p>
						<p class="text-e"><a class="url" href="http://{$item->www|replace:"http://":""}">{$item->www|replace:"http://":""}</a></p>
						{*<p class="text-e"><a class="url" onclick="_gaq.push(['_link',‘http://www.assecobs.pl’]); returnfalse;" href="https://wapro.pl">{$item->www|replace:"http://":""}</a></p>*}
					</section><!-- .box-x -->
				{/foreach}
			{/if}

			<section class="box-x text-area-b">
				<h2 class="header-l hl-b">{$siteTexts[667]->content|strip_tags}</h2>
				{$mainSites[219]->content}
			</section><!-- .box-x -->

			<section class="box-x text-area-b">
				<h2 class="header-l hl-b">{$siteTexts[758]->content|strip_tags}</h2>
				{$mainSites[219]->text_data_1}
			</section><!-- .box-x -->

		</div>
	</div><!-- .cols-two-f -->
</section><!-- .box-l -->
{include file='footer.tpl'}