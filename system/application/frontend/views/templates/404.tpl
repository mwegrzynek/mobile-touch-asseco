{include file='header.tpl'}
	<div class="box-404">
		<p class="image-j"><img src="images/404.png" width="879" height="421" alt="404"/></p>
		<section class="content">		
			<h1>{$mainSites[213]->text_data_1|strip_tags}</h1>			
			<h2>{$mainSites[213]->lead|strip_tags}</h2>			
			<p>{$mainSites[213]->content}</p>		
		</section><!-- .content -->
	</div><!-- .box-404 -->
{include file='footer.tpl'}