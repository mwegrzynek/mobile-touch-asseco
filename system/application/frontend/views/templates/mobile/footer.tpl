		<footer class="footer" data-role="footer">
			<div class="wrapper-a">
				<p class="copyright">Copyright by © Asseco BS</p>
				<p class="button-a"><a href="http://{$noMobileAddress}/forceversion" rel="external">{$siteTexts[860]->content|strip_tags}</a></p>
			</div><!-- .wrapper-a -->
   	</footer>
	</div> <!--! end of .container -->

	{literal}
	<!-- Google Tag Manager -->
	<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-HVLQ" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-HVLQ');</script>
	<!-- End Google Tag Manager -->
	{/literal}

	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script src="js/mobile/custom-scripting.js?v=1200939988678"></script>
	<script src="http://code.jquery.com/mobile/1.1.0/jquery.mobile-1.1.0.min.js"></script>
	<script src="js/mobile/libs/iscroll.js"></script>
	<script src="js/mobile/scripts.js?v=1.2"></script>
	<script src="js/mobile/mylibs/helper.js"></script>
	<script>MBP.scaleFix();</script>


  <!--<script>
  {$siteTexts[861]->content|strip_tags}
  </script>-->


</body>
</html>