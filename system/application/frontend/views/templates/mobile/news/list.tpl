{if $newsList}
	{foreach $newsList as $item}
		{assign var=url value="{$siteRoutes[207]->slug}/{$item->slug}.html"}
		<article class="art-g item">
			<a href="{$url}">
				<p class="date"><time datetime="{$item->add_date|date_format:"%Y-%m-%dT%H:%M"}">{$item->add_date|date_format:"%Y-%m-%d"}</time></p>
				<p class="text">{$item->title}</p>
			</a>
		</article>				
	{/foreach}
{/if}	