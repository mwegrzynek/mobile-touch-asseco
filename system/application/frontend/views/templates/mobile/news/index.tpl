{include file='mobile/header.tpl'}
<div class="main-content" role="main" data-role="content">
	<article class="art-f ontapchange">
		{assign var=url value="{$siteRoutes[207]->slug}/{$latest->slug}.html"}
		<a href="{$url}">
			{if $latest->main_image neq ''}
			<p class="image"><img src="{$baseurl}userfiles/news/{$latest->main_image}" width="98" alt=""/></p>						
			{else}
			<p class="image"><img src="{$baseurl}images/news_icon.png" width="98" alt=""/></p>	
			{/if}
			<div class="content">						
				<p class="date"><time datetime="{$latest->add_date|date_format:"%Y-%m-%dT%H:%M"}">{$latest->add_date|date_format:"%Y-%m-%d"}</time></p>
				<h1>{$latest->title}</h1>
				<p class="text">
					{if $latest->lead neq ''}					
						{assign var=desc value=$latest->lead}
					{else}
						{assign var=desc value=$latest->content|strip_tags|htmlspecialchars|truncate:160:"&hellip;":false}
					{/if}	
					{$desc}					
				</p>
			</div><!-- .content -->					
		</a>
	</article>
	<section id="items-container" class="ontapchange" data-items-count="{$allItems}">
		{include file="mobile/news/list.tpl"}
	</section>
	{if $showExtendBtn}<p class="more-e load-button"><a href="{$baseurl}news/extend/{$extendOffset|default:2}" data-extend-jump="{$extendJump}" class="link-d" data-ajax="false">{$siteTexts[858]->content|strip_tags}</a></p>{/if}
</div>
{include file='mobile/footer.tpl'}