{include file='mobile/header.tpl'}
{assign var=item value=$itemInfo}
<div class="main-content" role="main" data-role="content">
	{include file="mobile/news/nextPrevButtons.tpl"}
	<article class="art-f">
		<div class="art-container">
			{if $item->main_image neq ''}
			<p class="image"><img src="{$baseurl}userfiles/news/{$item->main_image}" width="98" alt=""/></p>						
			{else}
			<p class="image"><img src="{$baseurl}images/news_icon.png" width="98" alt=""/></p>	
			{/if}			
			<div class="content">						
				<p class="date"><time datetime="{$item->add_date|date_format:"%Y-%m-%dT%H:%M"}">{$item->add_date|date_format:"%Y-%m-%d"}</time></p>
				<h1>{$item->title}</h1>
				<p class="text">{$item->lead}</p>
			</div><!-- .content -->					
		</div>
	</article>
	<div class="text-container-a no-rewrite-src force-images-width">				
		{$item->content}
	</div><!-- .text-container-a -->
	
	{include file="mobile/news/nextPrevButtons.tpl"}
</div>
{include file='mobile/footer.tpl'}