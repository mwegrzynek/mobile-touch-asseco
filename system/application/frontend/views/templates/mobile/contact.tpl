{include file='mobile/header.tpl'}
{assign var=item value=$itemInfo}

<div class="main-content" role="main" data-role="content">
	<div data-role="collapsible">
		<h3 class="header-c">{$siteTexts[654]->content|strip_tags}</h3>
		<section class="collapsible-container-a box-a ba-2">
			<div class="box-c">
				<form action="index.html" method="post" class="form-a" id="request-contact-form" data-ajax="false">
					<p class="field-a"><input type="text" name="client_name" id="client_name" data-error-required="{$siteTexts[662]->content|strip_tags}"/></p>
					<p class="field-a"><input type="text" name="email" id="email" data-error-required="{$siteTexts[663]->content|strip_tags}" data-error-email="{$siteTexts[664]->content|strip_tags}"/></p>
					<p class="btn-a">
						<a href="index.html" id="submit-form" class="link-c" data-ajax="false">{$siteTexts[864]->content|strip_tags}</a>
					</p>
				</form>
				<p class="text-a">{$siteTexts[675]->content|strip_tags}</p>
			</div><!-- .box-c -->
		</section>
	</div>
	{if $partners}
		{foreach $partners as $item}
			<section class="box-x text-area-b vcard">
				<h2 class="header-l hl-a">{$siteTexts[668]->content|strip_tags}</h2>
				{if $item->name}
					<h3 class="header-m fn">{$item->name}</h3>
				{/if}

				{if $item->person_position}
					<p class="role">{$item->person_position}</p>
				{/if}
				<p>
					<span class="org">{$item->company_name}</span><br/>
					<span class="adr">
						<span class="street-address">{$item->address_street}</span>
						<span class="postal-code">{$item->address_zipcode}</span>
						<span class="locality">{$item->address_city}</span><br/>
						<span class="country-name">{$item->country_name}</span>
					</span>
				</p>
				<p class="text-e">
					{if $item->phone}<span class="tel">{$siteTexts[672]->content|strip_tags}. <span class="value">{$item->phone}</span></span><br/>{/if}
					{if $item->mobile_phone}<span class="tel">{$siteTexts[673]->content|strip_tags}. <span class="value">{$item->mobile_phone}</span></span><br/>{/if}
					{if $item->fax}<span class="tel"><span class="type">{$siteTexts[674]->content|strip_tags}</span> <span class="value">{$item->fax}</span></span>{/if}
				</p>
				<p class="text-e"><a href="mailto:{$item->mail_personal}">{$item->mail_personal}</a></p>
				<p class="text-e"><a class="url" href="http://{$item->www|replace:"http://":""}">{$item->www|replace:"http://":""}</a></p>
			</section><!-- .box-x -->
		{/foreach}
	{/if}

	<section class="box-x text-area-b">
		<h2 class="header-l hl-b">{$siteTexts[667]->content|strip_tags}</h2>
		{$mainSites[219]->content}
	</section><!-- .box-x -->

	<section class="box-x text-area-b">
		<h2 class="header-l hl-b">{$siteTexts[758]->content|strip_tags}</h2>
		{$mainSites[219]->text_data_1}
	</section><!-- .box-x -->

</div>
{include file='mobile/footer.tpl'}