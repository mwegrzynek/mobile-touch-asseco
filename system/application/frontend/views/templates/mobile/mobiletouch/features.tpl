{include file='mobile/header.tpl'}
{assign var=item value=$itemInfo}
<div class="main-content" role="main" data-role="content">
	<div class="text-container-a tca-a">				
		<p>{$item->text_data_2|strip_tags:"<strong>"}</p>
	</div><!-- .text-container-a -->
	
	<div class="box-c">
		<form action="{$baseurl}features/changeindustry" method="post" class="form-a autosubmit" data-ajax="false">
			<label for="industry">{$siteTexts[714]->content|strip_tags}:</label>
			<select name="industry" id="industry">
				{foreach $industries as $industryItem}
					<option{if $industryItem->id eq $currentCategory->id} selected="selected"{/if} value="{$industryItem->slug}">{$industryItem->name}</li>
				{/foreach}				
			</select>
			
			<p class="btn">
				<button type="submit">Submit</button>	
			</p>
		</form>
	</div><!-- .box-c -->			
	{if $pages}		
	<div>
		{foreach $pages as $page}			
			<section class="text-container-a box-d{if $page@last} bd-a{/if}">
				<h2 class="header-f">{$page->title}</h2>
				<p>{$page->lead}</p>
				{if $page->main_image}<p class="image-a"><img src="{$baseurl}userfiles/featurespages/{$page->main_image}" width="100%" alt=""/></p>{/if}
			</section>
		{/foreach}		
	</div>
	{/if}			
</div>
{include file='mobile/footer.tpl'}