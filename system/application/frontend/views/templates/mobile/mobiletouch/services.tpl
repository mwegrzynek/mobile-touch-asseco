{include file='mobile/header.tpl'}
{assign var=item value=$itemInfo}
<div class="main-content" role="main" data-role="content">
	<div class="text-container-a tca-a">				
		<p>{$item->text_data_2|strip_tags:"<strong>"}</p>
	</div><!-- .text-container-a -->
	
	<div class="no-rewrite-src">
		{foreach $services as $item}
		<div data-role="collapsible">
			<h3 class="header-c">{$item->title|strip_tags|htmlspecialchars}</h3>
			<section class="collapsible-container-a">						
				<div class="text-container-a text-area-a">
					<h2>{$item->lead|strip_tags|htmlspecialchars}</h2>
					{$item->content}
				</div>
			</section>
		</div>	
		{/foreach}		
	</div>	
			
</div>
{include file='mobile/footer.tpl'}