{include file='mobile/header.tpl'}
{assign var=item value=$itemInfo}
<div class="main-content" role="main" data-role="content">
	<div class="text-container-a tca-a">				
		<p>{$item->text_data_2|strip_tags:"<strong>"}</p>
	</div><!-- .text-container-a -->
	
	<div class="no-rewrite-src">	
		{if $concepts}			
			{foreach $concepts as $conceptItem}		
			<div data-role="collapsible">
				<h3 class="header-c">{$conceptItem->title}</h3>
				<section class="collapsible-container-a">						
					<div class="text-container-a" data-enhance="false">
						{$conceptItem->content}
					</div>
				</section>
			</div>
			{/foreach}
		{/if}		
	</div>
</div>
{include file='mobile/footer.tpl'}