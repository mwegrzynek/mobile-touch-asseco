{include file='mobile/header.tpl'}
{assign var=item value=$itemInfo}

<div class="main-content" role="main" data-role="content">
	<div class="text-container-a tca-a">				
		<p>{$item->text_data_2|strip_tags:"<strong>"}</p>
	</div><!-- .text-container-a -->
	
	{include file="mobile/common_areas/videosList.tpl"}		
			
	<div class="text-container-a">				
		<p>{$item->lead|strip_tags|htmlspecialchars}</p>
	</div><!-- .text-container-a -->
	{include file="mobile/mobiletouch/overviewBoxes.tpl"}
				
	{*<p class="image-b"><img src="images/visuals/overview-visual-2.png" width="100%" alt=""/></p>*}
	
	{if $item->file_data_1}<p class="image-b"><img src="{$item->file_data_1}" width="100%" alt=""/></p>{/if}							
	
	<h1 class="header-g">{$item->text_data_1|strip_tags:"<strong>"}</h1>
	
	{include file="mobile/mobiletouch/overviewTabs.tpl"}	
	
</div>
{include file='mobile/footer.tpl'}