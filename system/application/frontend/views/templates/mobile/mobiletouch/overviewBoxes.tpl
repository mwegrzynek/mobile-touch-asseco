{if $boxes}	
<div class="box-b bb-a" data-role="collapsible-set">			
	<div class="inner-box-b" data-role="collapsible">
		<h3 class="header-c">{$boxes[0]->title}</h3>
		<div class="text-container-a">				
			{$boxes[0]->lead}
		</div>
	</div>
	
	<div class="inner-box-b" data-role="collapsible">
		<h3 class="header-c">{$boxes[1]->title}</h3>
		<div class="text-container-a">
			{$boxes[1]->lead}
		</div>
	</div>				
</div>
{/if}