<div>	
	{foreach $tabBoxes as $tab}
	<div data-role="collapsible">
		<h3 class="header-c">{$tab->title}</h3>
		<section class="collapsible-container-a box-a ba-{$tab@iteration}">				
			<header>
				<p>{$tab->lead}</p>
			</header>
			<div class="text-container-a">
				<article class="art-a">
					<p>{$tab->content|nl2br}</p>
				</article>
				<article class="art-b">
					<h2 class="header-d">{$siteTexts[848]->content|strip_tags}</h2>
					<p>{$tab->text_data_1|nl2br}</p>
				</article>
			</div>
		</section>
	</div>
	{/foreach}
</div>