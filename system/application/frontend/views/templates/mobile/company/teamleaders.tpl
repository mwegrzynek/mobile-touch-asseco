{include file='mobile/header.tpl'}
{assign var=item value=$itemInfo}

<div class="main-content" role="main" data-role="content">
	<div class="text-container-a tca-a">				
		<p>{$item->text_data_2|strip_tags:"<strong>"}</p>
	</div><!-- .text-container-a -->
	
	<div class="text-container-a tca-b">				
		{foreach $teamleaders as $item}		
		<article class="art-e">
			<p class="image-b"><img src="{$baseurl}userfiles/teamleaders/{$item->main_image}" width="139" height="124" alt=""/></p>				
			<section class="content">							
				<h1>{$item->title} {$item->text_data_2}</h1>
				<p>{$item->text_data_1}</p>
			</section><!-- .text-area-a -->
		</article>	
		{/foreach}		
	</div><!-- .text-container-a -->
	
</div>
{include file='mobile/footer.tpl'}