{include file='mobile/header.tpl'}
{assign var=item value=$itemInfo}

<div class="main-content" role="main" data-role="content">
	<div class="text-container-a tca-a">				
		<p>{$item->text_data_2|strip_tags:"<strong>"}</p>
	</div><!-- .text-container-a -->
	
	<form action="{$baseurl}references/changeindustry" method="post" class="form-a autosubmit" data-ajax="false">
		<label for="industry">{$siteTexts[714]->content|strip_tags}:</label>
		<select name="industry" id="industry">
			{foreach $industries as $industryItem}
				<option{if $industryItem->id eq $currentCategory->id} selected="selected"{/if} value="{$industryItem->slug}">{$industryItem->name}</li>
			{/foreach}			
		</select>
		
		<p class="btn">
			<button type="submit">Submit</button>	
		</p>
	</form>
	
	<section class="text-container-a">
		<ul class="list-h">
		{foreach $extendedClients as $extendedClient}
			<li>{$extendedClient->title}</li>	
		{/foreach}		
		</ul>
	</section>
	
</div>
{include file='mobile/footer.tpl'}