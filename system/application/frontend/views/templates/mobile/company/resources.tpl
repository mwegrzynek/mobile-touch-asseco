{include file='mobile/header.tpl'}
{assign var=item value=$itemInfo}

<div class="main-content" role="main" data-role="content">
	<div class="text-container-a tca-a">				
		<p>{$item->text_data_2|strip_tags:"<strong>"}</p>
	</div><!-- .text-container-a -->
	
	{foreach $boxes as $item}		
		<article class="art-d">
			<header>
				{if $item->main_image}
					<p class="image-f"><img src="{$baseurl}userfiles/mainpages/{$item->main_image}" {$item->image_size} alt=""/></p>
				{/if}					
				<h1 class="header-h hh-b">{$item->text_data_1|strip_tags|htmlspecialchars}</h1>
				<p class="text-d td-a">{$item->lead|strip_tags|htmlspecialchars}</p>
			</header>
			<div class="text-area-a">							
				<p>{$item->content|strip_tags|htmlspecialchars}</p>
			</div><!-- .text-area-a -->
		</article>
	{/foreach}
	
</div>
{include file='mobile/footer.tpl'}