{include file='mobile/header.tpl'}
{assign var=item value=$itemInfo}

<div class="main-content" role="main" data-role="content">
	<div class="text-container-a tca-a">
		<p>{$item->text_data_2|strip_tags:"<strong>"}</p>
	</div><!-- .text-container-a -->

	{include file="mobile/common_areas/videosList.tpl"}

	<h1 class="header-g hg-b">{$pages[926]->title}</h1>
	<div class="text-container-a tca-c">
		<p>{$pages[926]->lead}</p>
	</div>

	<div class="box-y">
		<p class="link-container-a"><a href="http://www.gartner.com/reprints/asseco-business-solutions-s-a-?id=1-1LXBGZD&amp;ct=131017&amp;st=sg" rel="external" class="link-c lnkc-a" data-ajax="false" id="gartner-btn">{$siteTexts[935]->content|strip_tags}</a></p>

		<div class="text-container-a">
			<p>{$pages[926]->content|strip_tags}</p>
			<p>{$pages[926]->text_data_1|strip_tags}</p>
		</div>

		<p class="image image-c">
			<img src="images/mobile/vis/visual-subpage.png" width="254" height="192" alt="Vis 3"/>
		</p>
	</div>
	<div>
		<div data-role="collapsible">
			<h3 class="header-c">{$pages[929]->text_data_2|strip_tags}</h3>
			<section class="collapsible-container-a box-f">
				<header>
					<img src="images/mobile/icon-mobile.png" alt="" width="93" height="74" />
					<h1>{$pages[929]->content|strip_tags:"<strong><a><em><br>"}</h1>
				</header>
				<section class="text-container-a">
					{$pages[929]->text_data_1}
				</section>
			</section>
		</div>
		<div data-role="collapsible">
			<h3 class="header-c">{$pages[930]->text_data_2|strip_tags}</h3>
			<section class="collapsible-container-a box-f">
				<header>
					<img src="images/mobile/icon-sfa.png" alt="" width="71" height="72" />
					<h1>{$pages[930]->content|strip_tags:"<strong><a><em><br>"}</h1>
				</header>
				<section class="text-container-a">
					{$pages[930]->text_data_1}
				</section>
			</section>
		</div>
		<div data-role="collapsible">
			<h3 class="header-c">{$pages[931]->text_data_2|strip_tags}</h3>
			<section class="collapsible-container-a box-f">
				<header>
					<img src="images/mobile/icon-elite.png" alt="" width="82" height="65" />
					<h1>{$pages[931]->content|strip_tags:"<strong><a><em><br>"}</h1>
				</header>
				<section class="text-container-a">
					{$pages[931]->text_data_1}
				</section>
			</section>
		</div>
	</div>

	<div class="text-container-a">
		<p>{$item->lead|strip_tags|htmlspecialchars|nl2br}</p>
		<p>{$item->text_data_4|strip_tags|htmlspecialchars|nl2br}</p>
	</div><!-- .text-container-a -->

	<h1 class="header-g hg-a">{$item->text_data_1|strip_tags:"<strong><a>"}</h1>

	<div>
		{foreach $boxes as $item}
		<div data-role="collapsible">
			<h3 class="header-c">{$item->text_data_1|strip_tags}</h3>
			<section class="collapsible-container-a box-e be-{$item@iteration}">
				<header>
					<h1>{$item->lead|strip_tags:"<strong>"}</h1>
				</header>
				<section class="text-container-a">
					<p>{$item->content|strip_tags|nl2br}</p>
				</section>
			</section>
		</div>
		{/foreach}
	</div>

</div>
{include file='mobile/footer.tpl'}