{if $videosInfo}	
	<ul class="list-g">
		{foreach $videosInfo as $item}
		<li>
			{if $listType eq 'home'}
				{assign var=thumbSrc value="userfiles/sliders/{$item->main_image}"}			
			{elseif $item->type_id eq 1}
				{assign var=thumbSrc value="http://i3.ytimg.com/vi/{$item->link|yt_id}/hqdefault.jpg"}
			{else}			
				{assign var=thumbSrc value=$item->thumbnail_url}
			{/if}		
			<a class="fancybox-media" href="http://{$item->link|replace:"http://":""}">
				<p class="image">
					<span class="image-wrapper"><span class="image-container imc-a"><img src="{$thumbSrc}" width="220" alt=""{if $item->type_id eq 1} class="yt"{/if}/></span></span>
				</p>
				<p class="title">{$item->title|htmlspecialchars}</p>
			</a>					
		</li>
		{/foreach}
	</ul>
{/if}