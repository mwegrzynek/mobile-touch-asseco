{include file='mobile/header.tpl'}
<div class="main-content" role="main" data-role="content">
	<div class="scroll-container">
		<div id="wrapper">
		  <div id="scroller">
			 <ul id="thelist" class="list-c">
				<li data-index="1">
					<p class="image">
						<img src="images/mobile/vis/vis-1.png" width="266" height="192" alt="Vis 1"/>
					</p>
					<div class="text-container">
						<p class="text">{$mainSlider[0]->content|strip_tags:"<strong>"}</p>
					</div>
				</li>
				<li data-index="2">
					<p class="image">
						<img src="images/mobile/vis/vis-2.png" width="271" height="192" alt="Vis 2"/>
					</p>
					<div class="text-container">
						<p class="text">{$mainSlider[1]->content|strip_tags:"<strong>"}</p>
					</div>
				</li>
				<li data-index="3">
					<p class="image">
						<img src="images/mobile/vis/vis-4.png" width="254" height="192" alt="Vis 3"/>
					</p>
					<div class="text-container">
						<p class="text">{$mainSlider[3]->content|strip_tags:"<strong>"}</p>
					</div>
				</li>
			 </ul>
		  </div>
		</div>
		<ul class="list-f" id="scroller-nav">
			<li class="active" data-index="1"><a href="#">1</a></li>
			<li data-index="2"><a href="#">2</a></li>
			<li data-index="3"><a href="#">3</a></li>
		</ul>
	</div><!-- .scroll-container -->

	<nav>
		<ul class="list-e" data-role="listview">
			<li class="item-a"><a href="{$baseurl}{$siteRoutes[694]->slug}.html"><strong>{$mainSites[694]->title}</strong> {$siteTexts[852]->content|strip_tags}</a></li>
			<li class="item-b"><a href="{$baseurl}{$siteRoutes[636]->slug}.html"><strong>{$mainSites[636]->title}</strong> {$siteTexts[853]->content|strip_tags}</a></li>
			<li class="item-c"><a href="{$baseurl}{$siteRoutes[207]->slug}.html"><strong>{$mainSites[207]->title}</strong> {$siteTexts[854]->content|strip_tags}</a></li>
			<li class="item-d"><a href="{$baseurl}{$siteRoutes[219]->slug}.html"><strong>{$mainSites[219]->title}</strong> {$siteTexts[855]->content|strip_tags}</a></li>
			<li class="item-e"><a href="{$baseurl}movie-clips.html"><strong>{$siteTexts[856]->content|strip_tags}</strong> {$siteTexts[857]->content|strip_tags}</a></li>
		</ul>
	</nav>
</div>
{include file='mobile/footer.tpl'}