<!doctype html>
<!-- Conditional comment for mobile ie7 http://blogs.msdn.com/b/iemobile/ -->
<!--[if IEMobile 7 ]>    <html class="no-js iem7"> <![endif]-->
<!--[if (gt IEMobile 7)|!(IEMobile)]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<title>{$meta.title|htmlspecialchars}</title>
	<base href="{$baseurl}"/>
	<meta name="description" content="">
	<meta name="author" content="hitmo-studio.com">
	<meta name="HandheldFriendly" content="True">
  	<meta name="MobileOptimized" content="320">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/mobile/h/apple-touch-icon.png">  
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/mobile/m/apple-touch-icon.png">  
	<link rel="apple-touch-icon-precomposed" href="images/mobile/l/apple-touch-icon-precomposed.png">  
	<link rel="shortcut icon" href="images/mobile/l/apple-touch-icon.png">

	<!--iOS web app, deletable if not needed -->		
   <meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
   {literal}
	<script>(function(){var a;if(navigator.platform==="iPad"){a=window.orientation!==90||window.orientation===-90?"images/mobile/startup-tablet-landscape.png":"images/mobile/startup-tablet-portrait.png"}else{a=window.devicePixelRatio===2?"images/mobile/startup-retina.png":"images/mobile/startup.png"}document.write('<link rel="apple-touch-startup-image" href="'+a+'"/>')})()</script>
  	{/literal} 
	
	<meta http-equiv="cleartype" content="on">
	<link rel="stylesheet" href="styles/mobile/main.css?v=1.2">
	<link rel="stylesheet" href="styles/mobile/specific.css?v=1.2">
</head>

<body>
	<div class="container" data-role="page" id="{$mobile_pageid}"{if $mobile_cached} data-dom-cache="true"{/if}>
		<header data-role="header" class="bar-a">
			{if !$home}			
			<div class="h-box-a">				
				<h1 class="header-a">{$meta.title|htmlspecialchars}</h1>
				<p class="home"><a href="{$baseurl}{$mobile_backlink}" data-direction="reverse">{$siteTexts[859]->content|strip_tags}</a></p>	
				{if $mobile_submenu}					
				<h3 class="menu-trigger header-e"><span>Menu</span></h3>
				<nav class="main-menu nav-a">		
					{if $mobile_submenu eq 'mobiletouch'}
						{include file="mobile/mobiletouch/subnav.tpl"}
					{elseif $mobile_submenu eq 'company'}
						{include file="mobile/company/subnav.tpl"}
					{/if}
				</nav>				
				{/if}			
			</div><!-- .h-box-a -->
			{/if}			
			<div class="h-box-b">
				{if $home}
				<h2 class="header-b"><img src="{$baseurl}images/mobile/main-logo.png" width="214" height="69" alt="Mobile Touch"/></h2>
				{else}
				<h2 class="header-b"><a href="{$baseurl}index.html" data-direction="reverse"><img src="{$baseurl}images/mobile/main-logo-1.png" width="150" height="49" alt="Mobile Touch"/></a></h2>
				{/if}
			</div><!-- .h-box-b -->				
		</header>	