<!DOCTYPE html>
<html lang="{$currentLanguageInfo[0]->lang_code}">
<head>
	<meta charset="utf-8" />
	<title>{$meta.title|htmlspecialchars}</title>
	<meta name="keywords" content="{$meta.keywords|htmlspecialchars}" />
	<meta name="description" content="{$meta.description|htmlspecialchars}" />
	<base href="{$baseurl}"/>
	<link rel="stylesheet" type="text/css" href="styles/screen.css?v=4.3" media="screen" />
	<link rel="stylesheet" type="text/css" href="styles/print.css?v=1" media="print" />
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/icons/apple-touch-icon-114x114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/icons/apple-touch-icon-72x72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="images/icons/apple-touch-icon-precomposed.png">
	<script type="text/javascript">document.documentElement.className += " js";</script>
	<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	{*{if !$hideAnalyticsMarketingList && $currentLanguageInfo[0]->lang_code neq 'pl'}
		{include file='common_areas/analyticsMarketingList.tpl'}
	{/if}*}
	{*{if !$hideRegularAnalytics && $currentLanguageInfo[0]->lang_code neq 'pl'}
		{include file='common_areas/analytics.tpl'}
	{/if}*}
</head>

<body{if $home} class="home {$currentLanguageInfo[0]->lang_code}"{/if}>
	<!--[if lt IE 8]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> to experience this site.</p><![endif]-->
	<div class="root">
		<header id="top" role="banner">
			<div class="wrapper-a">
				<h1 id="logo"><a href="{$baseurl}" accesskey="h">
					{*<img src="images/logo.png" width="214" height="69" alt="Mobile Touch"/>*}
					{if $currentLanguageInfo[0]->lang_code neq 'pl'}
						<img src="images/logo-en.png" width="238" height="69" alt="Mobile Touch"/>
					{else}
						<img src="images/logo-pl.png" width="251" height="69" alt="Mobile Touch"/>
					{/if}
				</a></h1>
				<ul id="skip-links">
					<li><a href="#nav" accesskey="n">Skip to navigation [n]</a></li>
					<li><a href="#content" accesskey="c">Skip to content [c]</a></li>
					<li><a href="#footer" accesskey="f">Skip to footer [f]</a></li>
				</ul>

				{if $languages|count > 1}
				<div class="box-ag">
					<h2 class="offset">Choose language</h2>
					<ul class="list-y">
						{foreach $languages as $item}
							{if $item->is_visible}
							<li{if $item->id eq $currentLanguageInfo[0]->id} class="active"{/if}><a href="http://{$item->subdomain}" title="{$item->lang_code|strtoupper}"><img src="userfiles/languages/{$item->lang_code}.png" width="14" height="11" alt="{$item->lang_code|strtoupper}" /></a></li>
							{/if}
						{/foreach}
					</ul>
				</div><!-- .box-ag -->
				{*<div class="box-h">
					<h2 class="offset">Choose language</h2>
					<h3><span class="offset">Current language:</span> {$currentLanguageInfo[0]->lang_code|strtoupper}</h3>
					<ul class="list-a">
						{foreach $languages as $item}
							{if $item->id neq $currentLanguageInfo[0]->id}
								<li><a href="http://{$item->subdomain}">{$item->lang_code|strtoupper}</a></li>
							{/if}
						{/foreach}
					</ul>
				</div><!-- .box-h -->*}
				{/if}

				<h2 class="offset">Navigation</h2>
				<nav id="main-nav" role="navigation">
					{if $mainMenu}
						<ul>
						{foreach $mainMenu as $item}
							<li{if in_array($item->id,$activeIds)} class="active"{/if}>
								<a href="{$item->link}" accesskey="{$item@index}">{$item->name}</a><em> [{$item@index}]</em>
							</li>
						{/foreach}
						</ul>
					{/if}
				</nav>
			</div><!-- .wrapper-a -->
		</header><!-- #top -->
		<section id="content" role="main">