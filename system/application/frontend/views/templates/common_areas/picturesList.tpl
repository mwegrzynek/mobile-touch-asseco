{if $picturesInfo}	
	<ul class="list-l{if !$normalView} ll-b{/if}">
		{foreach from=$picturesInfo item=item key=key name=loop1}
		<li><a href="{$picturesPath}{$item->file_name}" class="fancy" rel="gallery"><img src="{$picturesPath}{$item->file_name|thumb_name}" width="100" height="100" alt="{$item->title|htmlspecialchars}"/></a></li>
		{/foreach}						
	</ul>
{/if}