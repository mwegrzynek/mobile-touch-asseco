{*main code*}
{if !$analyticsGoal1}
	{$siteTexts[785]->content}
{/if}

{*when contact form is sent*}
{if $analyticsGoal1}
	{if $analyticsGoal1 eq 1}
		{* form 1 *}
		{$siteTexts[786]->content}
	{else}
		{* form 2 *}
		{literal}
		<script type="text/javascript">
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-17829804-12']);
		_gaq.push(['_setDomainName', 'mobile-touch.eu']);
		_gaq.push(['_trackPageview', '/kontakt-z-nami-ok']);
		(function()
		{
		var ga = document.createElement('script'); ga.type = 'text/javascript';
		ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' :
		'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(ga, s);
		})();
		</script>
		{/literal}
	{/if}
{/if}

