{if $videosInfo}
	{foreach $videosInfo as $item}
	<p class="video-a{if $specialClass} {$specialClass}{/if}">
		{if $item->type_id eq 1}
			{assign var=thumbSrc value="http://i3.ytimg.com/vi/{$item->link|yt_id}/hqdefault.jpg"}
		{else}
			{assign var=thumbSrc value=$item->thumbnail_url}
		{/if}
		<a class="fancybox-media" data-index="{$item@index}" href="http://{$item->link|replace:"http://":""}">
			<span class="image-container"><img src="{$thumbSrc}" width="220" alt=""{if $item->type_id eq 1} class="yt"{/if}/></span>
			<span class="subtitle">{$item->title|htmlspecialchars}</span>
		</a>
	</p>
	{/foreach}
{/if}
