<h1 class="header-d hd-c">{$footerLinks->name}</h1>
<ul class="list-h">					
	{foreach $footerLinks->children as $item}
	<li>
		<a href="{$item->link}">{$item->name}</a>
	</li>
	{/foreach}				
</ul>