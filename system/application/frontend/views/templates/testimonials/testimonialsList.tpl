{if $testimonials}
	{if $testimonials|count > 1}
		<ul class="list-c lc-a">
			{foreach $testimonials as $item}
				<li><a href="#s-2-testimonial-{$item@index}">{$item->clientInfo->title}</a></li>
			{/foreach}
		</ul>
	{/if}
	<div class="box-k" id="slider-2">
		<ul class="list-e{if $type eq 'reference'} le-a{/if}">
			{foreach $testimonials as $item}
				<li id="#s-2-testimonial-{$item@index}">
					<blockquote>
						<p>{$item->lead|nl2br}</p>
					</blockquote>
					<p class="cite">
						<cite>{$item->title|strip_tags|htmlspecialchars}</cite>
						{if $item->text_data_1 neq ""}
							{$item->text_data_1|strip_tags|htmlspecialchars},
						{/if}
						{if $type neq 'reference'}{$item->clientInfo->title}{/if}</p>
					{if $type neq 'reference'}
						{assign var=path value='userfiles/clients/'|cat:$item->clientInfo->main_image}
						{if $item->clientInfo->main_image and $path|is_file}
							<p class="logo"><img src="userfiles/clients/{$item->clientInfo->main_image|thumb_name:1}" alt="{$item->clientInfo->title}"/></p>
						{/if}
					{/if}
				</li>
			{/foreach}
		</ul>
	</div>
{/if}