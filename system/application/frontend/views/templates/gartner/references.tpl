{include file='header.tpl'}
{assign var=item value=$itemInfo}
<section class="box-l">
	{include file="company/pagesHeader.tpl"}
	
	<ul class="b-crumbs">
		<li><a href="{$baseurl}">{$mainSites[609]->title}</a></li>		
		<li><a href="{$siteRoutes[636]->slug}.html">{$mainSites[636]->title}</a></li>
		<li>{$mainSites[635]->title}</li>
	</ul>
	
	{include file="company/industriesList.tpl" industries=$industries url="{$siteRoutes[636]->slug}/{$siteRoutes[635]->slug}" currentCategory=$currentCategory type='extended'}
	
	
	<ul class="list-r">
		{foreach $extendedClients as $extendedClient}
			<li>
				<a href="{$siteRoutes[636]->slug}/{$siteRoutes[635]->slug}/{$extendedClient->slug}.html" class="container">
					<p class="image">
						<img class="col" src="userfiles/clients/{$extendedClient->main_image|thumb_name}" alt=""/>
						<img class="bw" src="userfiles/clients/{$extendedClient->main_image|thumb_name:1}" alt=""/>
					</p>
					<p class="title">{$extendedClient->title}</p>
				</a><!-- .container -->
			</li>	
		{/foreach}		
	</ul>
	
	{if $regularClients}	
		{if $regularClients|count > 1}
			<div class="box-u">
				<p><span class="count">1</span> of <span class="total">{$regularClients|count}</span></p>
				<a class="cycle-c-nav prev" href="#">Prev</a>
				<a class="cycle-c-nav next" href="#">Next</a>
			</div><!-- .box-u -->		
		{/if}
		
		
		<h1 class="header-h hh-c">More clients</h1>
		
		<section class="box-t cycle-c">
			{foreach $regularClients as $regularItem}
			<section class="cols-two-g slide">					
				<div class="primary-tg">
					{if $regularItem[0]}
						<ul class="list-p">
							{foreach $regularItem[0] as $item}
								<li>{$item->title}</li>	
							{/foreach}						
						</ul>	
					{/if}	
				</div>
				<div class="secondary-tg">
					{if $regularItem[1]}
						<ul class="list-p">
							{foreach $regularItem[1] as $item}
								<li>{$item->title}</li>	
							{/foreach}						
						</ul>	
					{/if}	
				</div>
			</section><!-- .cols-two-g -->
			{/foreach}			
		</section><!-- .box-t -->
	{/if}
	
</section><!-- .box-l -->
{include file='footer.tpl'}