{if $industries and $industries|count > 1}
<div class="box-p{if $type eq 'short'} bp-a{/if}">					
	<section class="box-o">
		<h2>{$siteTexts[714]->content|strip_tags}:</h2>
		<h3><span class="offset">{$siteTexts[715]->content|strip_tags}:</span> {if !$currentCategory}{$siteTexts[716]->content|strip_tags}{else}{$currentCategory->name}{/if}</h3>				
	</section><!-- .box-o -->	
	<ul class="list-n">
		{if !$noDefault}<li><a href="{$siteRoutes[636]->slug}/{$siteRoutes[635]->slug}.html">{$siteTexts[716]->content|strip_tags}</a></li>{/if}
		{foreach $industries as $industryItem}
			<li{if $industryItem->id eq $currentCategory->id} class="active"{/if}><a href="{$url}/{$industryItem->slug}">{$industryItem->name}</a></li>
		{/foreach}	
	</ul>
</div><!-- .box-p -->
{/if}