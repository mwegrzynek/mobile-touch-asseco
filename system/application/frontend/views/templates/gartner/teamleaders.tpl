{include file='header.tpl'}
{assign var=item value=$itemInfo}
<section class="box-l">
	{include file="company/pagesHeader.tpl"}	
	
	<ul class="b-crumbs">
		<li><a href="{$baseurl}">{$mainSites[609]->title}</a></li>		
		<li><a href="{$siteRoutes[636]->slug}.html">{$mainSites[636]->title}</a></li>
		<li>{$mainSites[607]->title}</li>
	</ul>	
	{if $teamleaders}
	 	
	<ul class="list-v">
		{foreach $teamleaders as $item}			
		<li>
			<article class="container">
				<p class="image">
					<img src="userfiles/teamleaders/{$item->main_image}" width="139" height="124" alt=""/>
				</p>
				<div class="content">
					<h2>{$item->title} {$item->text_data_2}</h2>
					<p>{$item->text_data_1}</p>
				</div><!-- .content -->
				<div class="info">
					<div class="cols-two-k inner-content">
						<div class="primary-tk">
							<h2 class="header-b">{$siteTexts[868]->content|strip_tags}</h2>
							<p>{$item->content|strip_tags|htmlspecialchars}</p>
						</div>
						<div class="secondary-tk">
							<h2 class="header-b">{$siteTexts[869]->content|strip_tags} {$item->title}</h2>
							<blockquote>
								<p>{$item->lead|strip_tags|htmlspecialchars}</p>
							</blockquote>
						</div>
					</div><!-- .cols-two-k -->
				</div><!-- .info -->
			</article><!-- .container -->
		</li>		
		{/foreach}
	</ul>	
	{/if}
</section><!-- .box-l -->
{include file='footer.tpl'}