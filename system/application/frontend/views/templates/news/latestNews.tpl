{if $latestNews}
	<ul class="list-h">
	{foreach $latestNews as $item}
		<li>
			<a href="{$siteRoutes[207]->slug}/{$item->slug}.html">
				<p class="date"><time datetime="{$item->add_date|date_format:"%Y-%m-%dT%H:%M"}">{$item->add_date|date_format:"%d.%m.%Y"}</time></p>
				<p>{$item->title}</p>
			</a>
		</li>	
	{/foreach}
	</ul>
{/if}