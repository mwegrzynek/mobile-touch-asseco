{include file='header.tpl'}
<section class="box-l">				
	<ul class="b-crumbs">
		<li><a href="{$baseurl}">{$mainSites[609]->title}</a></li>
		<li>{$mainSites[207]->title}</li>					
	</ul>
	
	<section class="box-r">
		<h1 class="header-h hh-b">{$mainSites[207]->title}</h1>
		<article class="art-f">
			<div class="cols-two-i">
				{assign var=url value="{$siteRoutes[207]->slug}/{$latest->slug}.html"}
				<div class="primary-ti">
					<p class="date-a"><time datetime="{$latest->add_date|date_format:"%Y-%m-%dT%H:%M"}">{$latest->add_date|date_format:"%Y-%m-%d"}</time></p>
					<h1><a href="{$url}">{$latest->title}</a></h1>
					<p class="text-d td-a td-b">
						{if $latest->lead neq ''}					
							{assign var=desc value=$latest->lead}
						{else}
							{assign var=desc value=$latest->content|strip_tags|htmlspecialchars|truncate:160:"&hellip;":false}
						{/if}	
						{$desc}						
					</p>					
					<p class="btn-a"><a href="{$url}" class="link-a">{$siteTexts[687]->content|strip_tags}</a></p>
				</div>
				<div class="secondary-ti">					
					{if $latest->main_image neq ''}
					<p class="image"><img src="userfiles/news/{$latest->main_image}" alt=""/></p>						
					{else}
					<p class="image"><img src="images/news_icon.png" height="131" alt=""/></p>	
					{/if}					
				</div>
			</div><!-- .cols-two-i -->	
		</article>	
	</section>
	
	<section class="box-v" id="items-container" data-items-count="{$allItems}">
		{$newsListRendered}		
	</section><!-- .box-v -->				
	{if $showExtendBtn}<p class="more-e load-button"><a href="news/extend/{$extendOffset|default:2}" data-extend-jump="{$extendJump}" class="link-d">{$siteTexts[858]->content|strip_tags}</a></p>{/if}	
</section><!-- .box-l -->	
{include file='footer.tpl'}