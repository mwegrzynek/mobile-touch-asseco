{include file='header.tpl'}
{assign var=item value=$itemInfo}
	<section class="box-l bl-a">
		<ul class="b-crumbs">
			<li><a href="{$baseurl}">{$mainSites[609]->title}</a></li>
			<li><a href="{$siteRoutes[207]->slug}.html">{$mainSites[207]->title}</a></li>
			<li>{$item->title}</li>
		</ul>

		<div class="cols-two-f">
			<div class="primary-tf">
				<section class="box-r">
					<h1 class="header-h hh-b">{$mainSites[207]->title}</h1>
					<article class="art-f">
						<div class="cols-two-i cti-a">
							<div class="primary-ti">
								<p class="date-a"><time datetime="{$item->add_date|date_format:"%Y-%m-%dT%H:%M"}">{$item->add_date|date_format:"%Y-%m-%d"}</time></p>
								<h1>{$item->title}</h1>
								<p class="text-d td-a td-b">
									{$item->lead}
								</p>
							</div>
							<div class="secondary-ti">
								{if $item->main_image neq ''}
								<p class="image"><img src="userfiles/news/{$item->main_image}" alt=""/></p>
								{else}
								<p class="image"><img src="images/news_icon.png" height="131" alt=""/></p>
								{/if}
							</div>
						</div><!-- .cols-two-i -->
					</article>
				</section>
				<div class="content-container-a short-col">
					{$item->content}
				</div><!-- .content-container-a -->
			</div>
			<div class="secondary-tf">
				{if $latest}
					<ul class="list-o lo-b">
						{foreach $latest as $listItem}
							{assign var=url value="{$siteRoutes[207]->slug}/{$listItem->slug}.html"}
							<li class="{if $listItem->id eq $item->id}active{/if}{if $listItem@last} last{/if}"><a href="{$url}"><time datetime="{$listItem->add_date|date_format:"%Y-%m-%dT%H:%M"}">{$listItem->add_date|date_format:"%Y-%m-%d"}</time> {$listItem->title}</a></li>
						{/foreach}
					</ul>
				{/if}
			</div>
		</div><!-- .cols-two-f -->
	</section><!-- .box-l -->
{include file='footer.tpl'}