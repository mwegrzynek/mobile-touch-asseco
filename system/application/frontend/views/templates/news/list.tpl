{if $newsList}
	{foreach $newsList as $item}
		<article class="art-g item">
			{assign var=url value="{$siteRoutes[207]->slug}/{$item->slug}.html"}
			<p class="date-a"><time datetime="{$item->add_date|date_format:"%Y-%m-%dT%H:%M"}">{$item->add_date|date_format:"%Y-%m-%d"}</time></p>
			<h1><a href="{$url}" title="{$item->title}">{$item->title|truncate:100:"..."}</a></h1>
			<p class="text">
				{if $item->lead neq ''}					
					{assign var=desc value=$item->lead|truncate:280:"...":false}
				{else}
					{assign var=desc value=$item->content|strip_tags|htmlspecialchars|truncate:280:"&hellip;":false}
				{/if}	
				{$desc}			
			</p>
			<p class="btn-a"><a href="{$url}" class="link-b lnkb-a lnkb-c">{$siteTexts[687]->content|strip_tags}</a></p>
		</article>		
	{/foreach}
{/if}