<p class="field-a">
	<label for="client_name{$counter}">{$siteTexts[330]->content|strip_tags}:</label>
	<span class="f-container"><input type="text" name="client_name" value="{$backData[$type].client_name}" id="client_name{$counter}" data-error-required="{$siteTexts[662]->content|strip_tags}"/></span>
</p>
<p class="field-a">
	<label for="email{$counter}">{$siteTexts[648]->content|strip_tags}:</label>
	<span class="f-container"><input type="text" name="email" value="{$backData[$type].email}" id="email{$counter}" data-error-required="{$siteTexts[663]->content|strip_tags}" data-error-email="{$siteTexts[664]->content|strip_tags}"/></span>
</p>
<p class="field-b">
	<label for="company_second_name">Leave it empty:</label>
	<input id="company_second_name" type="text" data-error-required="Please write your company second name" value="" name="company_second_name">
</p>