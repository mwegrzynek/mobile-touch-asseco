{include file='header.tpl'}
{assign var=item value=$itemInfo}
<section class="box-l">
	{include file="company/pagesHeader.tpl"}	
	
	<ul class="b-crumbs">
		<li><a href="{$baseurl}">{$mainSites[609]->title}</a></li>		
		<li><a href="{$siteRoutes[636]->slug}.html">{$mainSites[636]->title}</a></li>
		<li>{$mainSites[634]->title}</li>
	</ul>
	
	<div class="wrapper-a wpa-a">
		{foreach $boxes as $item}		
			<article class="art-d equal-height">
				<header>
					{if $item->main_image}
						<p class="image-f"><img src="userfiles/mainpages/{$item->main_image}" {$item->image_size} alt=""/></p>
					{/if}					
					<h1 class="header-h hh-b">{$item->text_data_1|strip_tags|htmlspecialchars}</h1>
					<p class="text-d td-a">{$item->lead|strip_tags|htmlspecialchars}</p>
				</header>
				<div class="text-area-a">							
					<p>{$item->content|strip_tags|htmlspecialchars}</p>
				</div><!-- .text-area-a -->
			</article>
		{/foreach}		
	</div>
	
</section><!-- .box-l -->
{include file='footer.tpl'}