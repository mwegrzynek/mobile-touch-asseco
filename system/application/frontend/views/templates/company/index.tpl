{include file='header.tpl'}
{assign var=item value=$itemInfo}
<section class="box-l">
	{include file="company/pagesHeader.tpl"}

	<ul class="b-crumbs">
		<li><a href="{$baseurl}">{$mainSites[609]->title}</a></li>
		<li>{$mainSites[636]->title}</li>
	</ul>

{* start new section *}

	<div class="foo-a"></div>
	<section class="box-f request-contact-box bf-a bf-b no-mask">
		<div class="wrapper-a wpa-b">
			<header class="h-box-a h-box-a-a" id="gartner-btn">
				<h1 class="header-c hc-b">{$siteTexts[935]->content|strip_tags}</h1>
			</header>
			{*<section class="box-g request-contact-container">
				{if !$success}
					<form action="{$baseurl}" method="post" class="form-a request-contact-form" id="report-contact-form">
						<p class="text-b tb-a">{$siteTexts[936]->content|strip_tags}</p>

						{if $errors.short}
						<section class="info-box error">
							<h2>{$siteTexts[661]->content|strip_tags}</h2>
							<ul>
								{foreach $errors.short as $item}
									<li>{$item}</li>
								{/foreach}
							</ul>
						</section><!-- .error -->
						{/if}

						<p class="field-a">
							<label for="client_name{$counter}">{$siteTexts[330]->content|strip_tags}:</label>
							<span class="f-container"><input type="text" name="client_name" value="{$backData[$type].client_name}" id="client_name{$counter}" data-error-required="{$siteTexts[662]->content|strip_tags}"/></span>
						</p>

						<p class="field-b">
							<label for="company_second_name">Leave it empty:</label>
							<input id="company_second_name" type="text" data-error-required="Please write your company second name" value="" name="company_second_name">
						</p>

						<p class="field-a">
							<label for="company">{$siteTexts[650]->content|strip_tags}:</label>
							<span class="f-container"><input type="text" name="company" value="{$backData.full.company}" id="company" data-error-required="{$siteTexts[670]->content|strip_tags}"/></span>
						</p>

						<p class="field-a">
							<label for="email{$counter}">{$siteTexts[648]->content|strip_tags}:</label>
							<span class="f-container"><input type="text" name="email" value="{$backData[$type].email}" id="email{$counter}" data-error-required="{$siteTexts[663]->content|strip_tags}" data-error-email="{$siteTexts[664]->content|strip_tags}"/></span>
						</p>

						<p class="btn">
							<input type="hidden" name="typeof" value="gartner" />
							<button type="submit" class="link-a"{if $currentLanguageInfo[0]->lang_code eq 'pl'} onClick="_gaq.push(['_trackPageview','/Mobile_Touch_main_kontakt']);"{/if}>{$siteTexts[937]->content|strip_tags}</button>
						</p>
						<p class="text-k tk-a tka-a">{$pages[932]->lead|nl2br}</p>
						<p class="text-k tk-a tka-a">{$siteTexts[938]->content|strip_tags|nl2br}</p>
					</form>
				{/if}
			</section><!-- .box-g -->
			*}
		</div>
	</section><!-- .box-f -->

	<section class="box-m bm-b bm-h">
		<header class="hbox-b">
			<h1>{$pages[926]->title}</h1>
			<p>{$pages[926]->lead}</p>
		</header>
		<article class="box-ah">
			<header>{$pages[926]->content}</header>
			{$pages[926]->text_data_1}
			{if $currentLanguageInfo[0]->lang_code eq 'pl'}
				<img class="visual" src="images/gartner-vis-pl.png" alt="" width="492" height="496" />
			{else}
				<img class="visual" src="userfiles/user/other-vis/en/g-vis.png" alt="" width="492" height="496" />
			{/if}
		</article>
	</section>

	<section class="box-ai">
		<div class="cols-two-l">
			<div class="primary-tl">
				<ul class="list-f lf-a">
					{foreach $testimonials as $titem}
						<li><a href="#testimonial-{$titem@key}"><img src="userfiles/raporttestimonials/{$titem->main_image}" alt="" width="139" height="124" /></a></li>
					{/foreach}
				</ul>
			</div>
			<div class="secondary-tl">
				<ul class="list-g lg-a">
					{foreach $testimonials as $titem}
					<li>
						<article class="art-h ah-a">
							<blockquote>
								<p>{$titem->lead|nl2br}</p>
							</blockquote>
							<p class="cite">{$titem->title}</p>
						</article>
					</li>
					{/foreach}
				</ul>
			</div>
		</div><!-- .cols-two-e <-->
	</section>

	<section class="box-aj">
		<header>
			<p>{$pages[927]->lead|nl2br}</p>
		</header>
		<h2 class="header-o">{$pages[927]->title}</h2>
		<article class="art-j item-a">
			<div class="wrapper">
				<header>
					<h2>
						<img src="images/art-j-item-a.png" alt="" width="61" height="75" />
						{$pages[929]->content|strip_tags:"<strong><a><em><br>"}
					</h2>
				</header>
				<div class="desc text-area-e">
					{$pages[929]->text_data_1}
				</div>
			</div>
		</article>
		<article class="art-j item-b">
			<div class="wrapper">
				<header>
					<h2>
						<img src="images/art-j-item-b.png" alt="" width="58" height="75" />
						{$pages[930]->content|strip_tags:"<strong><a><em><br>"}
					</h2>
				</header>
				<div class="desc text-area-e">
					{$pages[930]->text_data_1}
				</div>
			</div>
		</article>
		<article class="art-j item-c">
			<div class="wrapper">
				<header>
					<h2>
						<img src="images/art-j-item-c.png" alt="" width="64" height="75" />
						{$pages[931]->content|strip_tags:"<strong><a><em><br>"}
					</h2>
				</header>
				<div class="desc text-area-e">
					{$pages[931]->text_data_1}
				</div>
			</div>
		</article>
	</section>

	<section class="box-m bm-b bm-i bm-j">
		<h2 class="header-h hh-b hh-b-a">{$pages[928]->title}</h2>
		<div class="text-area-d">
			<p>{$pages[928]->lead|nl2br}</p>
		</div>
		<hr />
		<p>{$pages[932]->lead|nl2br}</p>
	</section><!-- .box-m -->

	{*<section class="box-f request-contact-box bf-a bf-b no-mask">
		<div class="wrapper-a wpa-b">
			<header class="h-box-a h-box-a-a request-contact-trigger">
				<h1 class="header-c hc-b">{$siteTexts[935]->content|strip_tags}</h1>
			</header>
			<section class="box-g request-contact-container">
				{if !$success}
					<form action="{$baseurl}" method="post" class="form-a request-contact-form" id="report-contact-form">
						<p class="text-b tb-a">{$siteTexts[936]->content|strip_tags}</p>

						{if $errors.short}
						<section class="info-box error">
							<h2>{$siteTexts[661]->content|strip_tags}</h2>
							<ul>
								{foreach $errors.short as $item}
									<li>{$item}</li>
								{/foreach}
							</ul>
						</section><!-- .error -->
						{/if}

						<p class="field-a">
							<label for="client_name{$counter}">{$siteTexts[330]->content|strip_tags}:</label>
							<span class="f-container"><input type="text" name="client_name" value="{$backData[$type].client_name}" id="client_name{$counter}" data-error-required="{$siteTexts[662]->content|strip_tags}"/></span>
						</p>

						<p class="field-b">
							<label for="company_second_name">Leave it empty:</label>
							<input id="company_second_name" type="text" data-error-required="Please write your company second name" value="" name="company_second_name">
						</p>

						<p class="field-a">
							<label for="company">{$siteTexts[650]->content|strip_tags}:</label>
							<span class="f-container"><input type="text" name="company" value="{$backData.full.company}" id="company" data-error-required="{$siteTexts[670]->content|strip_tags}"/></span>
						</p>

						<p class="field-a">
							<label for="email{$counter}">{$siteTexts[648]->content|strip_tags}:</label>
							<span class="f-container"><input type="text" name="email" value="{$backData[$type].email}" id="email{$counter}" data-error-required="{$siteTexts[663]->content|strip_tags}" data-error-email="{$siteTexts[664]->content|strip_tags}"/></span>
						</p>

						<p class="btn">
							<input type="hidden" name="typeof" value="gartner" />
							<button type="submit" class="link-a"{if $currentLanguageInfo[0]->lang_code eq 'pl'} onClick="_gaq.push(['_trackPageview','/Mobile_Touch_main_kontakt']);"{/if}>{$siteTexts[937]->content|strip_tags}</button>
						</p>
						<p class="text-k tk-a tka-a">{$pages[932]->lead|nl2br}</p>
						<p class="text-k tk-a tka-a">{$siteTexts[938]->content|strip_tags|nl2br}</p>
					</form>
				{/if}
			</section><!-- .box-g -->
		</div>
	</section><!-- .box-f -->*}
{* end new section *}


	<section class="box-m bm-b">
		<h1 class="header-e he-b">
			{$item->lead|strip_tags:"<strong><a>"}
		</h1>
		<p class="text-h">{$item->text_data_4|strip_tags|htmlspecialchars|nl2br}</p>
	</section><!-- .box-m -->

	{if $currentLanguageInfo[0]->lang_code eq 'en' or $currentLanguageInfo[0]->lang_code eq 'de'}
		{include file="company/whyUs.tpl"}
	{/if}

{*
	<p class="image-i"><a href="http://www.asseco.com/"><img src="images/asseco-group-logo.png" width="979" height="104" alt="Asseco Group Logo"/></a></p>
	<p class="image-i"><img src="images/map-2.png" width="980" height="468" alt="Asseco World Map"/></p>
*}
	{*<p class="text-i"><a href="userfiles/user/docs/asseco-worldwide-offices.pdf" class="link-f lnkf-a">See the full list of offices</a></p>*}

	{if $videosInfo}
	<div class="video-box-b">
		<div class="vb-container">
			{include file="common_areas/videosList.tpl"}
		</div><!-- .vb-container -->
	</div><!-- .video-box-a -->
	{/if}

	<section class="box-d bd-a{if $videosInfo} bd-b{/if}">
		<div class="wrapper-a wpa-a">
			<h1 class="header-g hg-a">{$item->text_data_1|strip_tags:"<strong><a>"}</h1>
			<ul class="list-d no-anim">
				{include file="company/box.tpl" item=$boxes[0] itemClass='item-a' full=true}
				{include file="company/box.tpl" item=$boxes[1] itemClass='item-b' full=true}
				{include file="company/box.tpl" item=$boxes[2] itemClass='item-c' full=true}
			</ul>
		</div><!-- .wrapper-a -->
	</section><!-- .box-d -->

	{if $item->content}
		<div class="content-container-a cca-a wide-col">
			{$item->content}
		</div><!-- .content-container-a -->
	{/if}
</section><!-- .box-l -->
{include file='footer.tpl'}