{include file='header.tpl'}
{assign var=item value=$itemInfo}
<section class="box-l bl-a">
	{include file="company/pagesHeader.tpl"}
	
	<ul class="b-crumbs">
		<li><a href="{$baseurl}">{$mainSites[609]->title}</a></li>		
		<li><a href="{$siteRoutes[636]->slug}.html">{$mainSites[636]->title}</a></li>
		<li><a href="{$siteRoutes[636]->slug}/{$siteRoutes[635]->slug}.html">{$mainSites[635]->title}</a></li>
		<li>{$clientInfo->title}</li>		
	</ul>
	
	<div class="cols-two-f">
		<div class="primary-tf">						
			<section class="box-r">
				<div class="cols-two-h">
					<div class="primary-th">
						{if $clientInfo->main_image}
						<p class="image-g v-align-parent"><span><img src="userfiles/clients/{$clientInfo->main_image}" {$clientInfo->image_size} alt=""/></span></p>
						{/if}
					</div>
					<div class="secondary-th v-align-height-giver">
						<header>										
							<h1 class="header-j">{$clientInfo->title}</h1>
							{if $clientInfo->lead}
								<p class="text-d td-a">{$clientInfo->lead|strip_tags|htmlspecialchars}</p>
							{/if}
						</header>
						<div class="text-area-a">							
							<p>{$clientInfo->content|strip_tags|htmlspecialchars|nl2br}</p>							
						</div><!-- .text-area-a -->
					</div>
				</div><!-- .cols-two-h -->
			</section><!-- .box-m bm-b -->
			
			{if $testimonials}				
				<section class="box-j bj-a{if $testimonials|count < 2} bj-b{/if} sequence-container-b">							
					{include file="testimonials/testimonialsList.tpl" testimonials=$testimonials type='reference'}
				</section>
			{/if}
			
			
			{if $references}
				{foreach $references as $ref}
					<div class="content-container-a short-col">				
						{*{if $ref@first}							
							<h1 class="header-h hh-b">References</h1>				
						{/if}*}
						<section class="box-m {if $ref->pictures or $ref->main_image neq '' or $ref->videos}bm-a {/if}bm-e format-d">					
							<div class="text-area-a">
								<h2>{$ref->lead}</h2>
								{$ref->content}								
							</div><!-- .text-area-a -->
						</section><!-- .box-m -->
					</div><!-- .content-container-a -->
					
					{if $ref->pictures or $ref->main_image neq '' or $ref->videos}
						<div class="box-m bm-a bm-f wpa-a">
							{if $ref->pictures}							
							<section class="col-a cla-a">
								<h1 class="header-b hb-c">Pictures</h1>
								<ul class="list-s">
									{foreach $ref->pictures as $picture}
										<li><a class="fancy" href="userfiles/references/{$picture->file_name}"><img src="userfiles/references/{$picture->file_name|thumb_name}" width="100" height="100" alt="Gallery 1"/></a></li>	
									{/foreach}								
								</ul>
							</section><!-- .col-a -->
							{/if}
							
							{if $ref->main_image}							
							<section class="col-a cla-b">
								<h1 class="header-b hb-c">Attachment</h1>
								<ul class="list-t">
									<li><a href="userfiles/references/{$ref->main_image}">{$ref->main_image}</a></li>
								</ul>
							</section><!-- .col-a -->
							{/if}
							
							{if $ref->videos}							
							<section class="col-a cla-c">
								<h1 class="header-b hb-c">Video</h1>
								{include file="common_areas/videosList.tpl" videosInfo=$ref->videos}
							</section><!-- .col-a -->
							{/if}
						</div><!-- .box-m -->
					{/if}
				{/foreach}
			{/if}
			
			{if $caseStudies}
				{foreach $caseStudies as $cs}
					<div class="content-container-a short-col">				
						{*{if $cs@first}							
							<h1 class="header-h hh-b">Case Study</h1>				
						{/if}*}
						<section class="box-m {if $cs->pictures or $cs->main_image neq '' or $cs->videos}bm-a {/if}bm-e format-d">					
							<div class="text-area-a">
								<h2>{$cs->lead}</h2>
								{$cs->content}								
							</div><!-- .text-area-a -->
						</section><!-- .box-m -->
					</div><!-- .content-container-a -->
					
					{if $cs->pictures or $cs->file_data_1 neq '' or $cs->videos}
					<div class="box-m bm-a bm-f wpa-a">
						{if $cs->pictures}							
						<section class="col-a cla-a">
							<h1 class="header-b hb-c">Pictures</h1>
							<ul class="list-s">
								{foreach $cs->pictures as $picture}
									<li><a class="fancy" href="userfiles/casestudies/{$picture->file_name}"><img src="userfiles/casestudies/{$picture->file_name|thumb_name}" width="100" height="100" alt=""/></a></li>	
								{/foreach}								
							</ul>
						</section><!-- .col-a -->
						{/if}
						
						{*{if $cs->main_image}							
						<section class="col-a cla-b">
							<h1 class="header-b hb-c">Attachment</h1>
							<ul class="list-t">
								<li><a href="userfiles/casestudies/{$cs->main_image}">{$cs->main_image}</a></li>
							</ul>
						</section><!-- .col-a -->
						{/if}*}
						
						{if $cs->file_data_1}							
						<section class="col-a cla-b">
							<h1 class="header-b hb-c">Attachment</h1>
							<ul class="list-t">
								<li><a href="{$cs->file_data_1}">{$cs->file_name}</a></li>
							</ul>
						</section><!-- .col-a -->
						{/if}
						
						{if $cs->videos}							
						<section class="col-a cla-c">
							<h1 class="header-b hb-c">Video</h1>
							{include file="common_areas/videosList.tpl" videosInfo=$cs->videos specialClass='va-b'}
						</section><!-- .col-a -->
						{/if}
					</div><!-- .box-m -->
					{/if}
				{/foreach}
			{/if}
		</div>
		<div class="secondary-tf">			
			{include file="company/industriesList.tpl" industries=$industries url="{$siteRoutes[636]->slug}/{$siteRoutes[635]->slug}" currentCategory=$currentCategory type="short"}
			<ul class="list-o lo-a">
				{foreach $extendedClients as $extendedClient}					
				<li class="{if $extendedClient->id eq $clientInfo->id}active{/if}{if $extendedClient@last} last{/if}"><a href="{$siteRoutes[636]->slug}/{$siteRoutes[635]->slug}/{$extendedClient->slug}.html">{$extendedClient->title}</a></li>
				{/foreach}				
			</ul>						
			
		</div>
	</div><!-- .cols-two-f -->				
	
</section><!-- .box-l -->
{include file='footer.tpl'}