<li>
	<div class="item {$itemClass}"><p>{$item->lead|strip_tags:"<strong>"}</div>
	{if $full}		
	<div class="text">
		<p>{$item->content|strip_tags|nl2br}</p>
	</div><!-- .text -->								
	{/if}
</li>