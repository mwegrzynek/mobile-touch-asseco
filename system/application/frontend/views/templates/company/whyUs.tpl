<section class="box-ac bac-a" id="why-us">
	<h2 class="header-n hn-c">{$siteTexts[909]->content|strip_tags}</h2>
	<ul class="list-c lc-a lc-b sequence-container-c-nav">
		{foreach $whyUs as $item}
			<li><a href="#why-us-section-{$item@iteration}">{$item@iteration}</a></li>
		{/foreach}
	</ul>
	<div class="box-ae sequence-container-c">
		{foreach $whyUs as $item}
		<div class="tab-pane" id="#why-us-section-{$item@iteration}">
			<article class="box-af col-a">
				<h2>{$item[0]->title}</h2>
				<p>{$item[0]->content|nl2br}</p>
			</article>
			<article class="box-af col-b">
				<h2>{$item[1]->title}</h2>
				<p>{$item[1]->content|nl2br}</p>
			</article>
		</div>
		{/foreach}
	</div>
</section>