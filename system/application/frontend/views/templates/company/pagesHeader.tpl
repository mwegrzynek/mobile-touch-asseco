{include file="company/subnav.tpl"}
<article class="art-b{if $item->main_image} artb-b{else} artb-a{/if}">
	<h1 class="header-e">		
		{$item->text_data_2|strip_tags:"<strong><a>"}
	</h1>
	{if $item->main_image}
		<p class="image"><img src="userfiles/mainpages/{$item->main_image}" alt=""/></p>						
	{/if}		
</article><!-- .art-b -->