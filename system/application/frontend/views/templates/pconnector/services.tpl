{include file='header.tpl'}
{assign var=item value=$itemInfo}
<section class="box-l">
	{include file="pconnector/pagesHeader.tpl"}

	<ul class="b-crumbs">
		<li><a href="{$baseurl}">{$mainSites[609]->title}</a></li>
		<li><a href="{$siteRoutes[880]->slug}.html">{$mainSites[880]->title}</a></li>
		<li>{$mainSites[882]->title}</li>
	</ul>

	<div class="wrapper-a wpa-a">
		{foreach $services as $item}
			<article class="art-d equal-height">
				<header>
					<h1 class="header-h hh-a">
						{if $item->main_image}
							<img src="userfiles/pcservicespages/{$item->main_image}"alt=""/>
						{/if}
						<span>{$item->title|strip_tags|htmlspecialchars}</span>
					</h1>
					<p class="text-d td-a">{$item->lead|strip_tags|htmlspecialchars}</p>
				</header>
				<div class="text-area-a">
					{$item->content}
				</div><!-- .text-area-a -->
			</article>
		{/foreach}
	</div>
</section><!-- .box-l -->
{include file='footer.tpl'}