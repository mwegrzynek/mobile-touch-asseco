{*<section class="box-j sequence-container-b">
		<h1 class="header-b hb-a">{$siteTexts[709]->content|strip_tags}</h1>
		{include file="testimonials/testimonialsList.tpl" testimonials=$testimonials}
		<p class="btn-b"><a href="{$siteRoutes[636]->slug}/{$siteRoutes[635]->slug}.html" class="link-b lnkb-a">{$siteTexts[710]->content|strip_tags}</a></p>
</section>*}

<section class="box-j sequence-container-b">
	<h1 class="header-b hb-a">{$siteTexts[709]->content|strip_tags}</h1>

	<ul class="list-c lc-a">
		<li><a href="#s-2-testimonial-1">Hoop Polska</a></li>
		<li><a href="#s-2-testimonial-2">Jutrzenka Colian</a></li>
		<li><a href="#s-2-testimonial-3">Rieber Foods Polska</a></li>
		<li><a href="#s-2-testimonial-4">Fragment referencji od PTK Centertel</a></li>
	</ul>

	{if $currentLanguageInfo[0]->lang_code eq 'pl'}
		<div class="box-k" id="slider-2">
			<ul class="list-e">
				<li id="#s-2-testimonial-1">
					<blockquote>
						<p>Postanowiliśmy wdrożyć Platformę Wymiany Danych Connector, gdyż jest to narzędzie doskonale dopasowane do branży FMCG, dla której zarządzanie powiązaniami dystrybucyjnymi jest wyjątkowo ważne ze względu na charakter produkowanych towarów, tzw. dóbr szybkozbywalnych. Oprócz poprawy relacji z dystrybutorami, liczymy także na optymalizację kosztów związanych zarówno z produkcją, jak i magazynowaniem naszych napojów. Ponadto system oferuje nam szereg możliwości mających na celu zapewnienie jakości analizowanych informacji uzyskiwanych od naszych dystrybutorów. Są to np. usługi normalizacji, deduplikacji, geokodyfikacji czy mapowania danych</p>
					</blockquote>
					<p class="cite"><cite>Mateusz Jesiołowski</cite> Hoop Polska</p>
					{*{if $type neq 'reference'}
						{assign var=path value='userfiles/clients/'|cat:$item->clientInfo->main_image}
						{if $item->clientInfo->main_image and $path|is_file}
							<p class="logo"><img src="userfiles/clients/{$item->clientInfo->main_image|thumb_name:1}" alt="{$item->clientInfo->title}"/></p>
						{/if}
					{/if}*}
				</li>
				<li id="#s-2-testimonial-2">
					<blockquote>
						<p>Platforma Connector pozwoli naszej firmie na automatyzację procesu zamówień oraz umożliwi dostęp do dwóch rodzajów bardzo ważnych informacji. Po pierwsze, zapewni bieżący wgląd do stanów magazynowych produktów Jutrzenki Colian i ich kontrolę u poszczególnych dystrybutorów. Od tej pory przesłanie zamówienia faksem nie będzie konieczne, nasi pracownicy sami określą, kiedy należy uzupełnić towar i w jakiej ilości. (…)  Ponadto, platforma Connector umożliwi nam dostęp do danych na temat poziomu odsprzedaży do punktów detalicznych realizowanej przez sieć dystrybucyjną. Takie rozwiązanie pozwala skutecznie planować koszty i przychody oraz prowadzić działalność znacznie efektywniej</p>
					</blockquote>
					<p class="cite"><cite>Michał Mielcarek</cite> Jutrzenka Colian</p>
				</li>
				<li id="#s-2-testimonial-3">
					<blockquote>
						<p>Platforma wymiany danych Connector pomoże nam zacieśnić współpracę z partnerami handlowymi i ponadto przyniesie także konkretne korzyści biznesowe, zarówno naszej firnie jak i dystrybutorom. System pozwoli nam również na lepsze planowanie zasobów produkcyjnych oraz szybsze reagowanie na wszelkie zmiany związane ze zmieniającym się zapotrzebowaniem na nasze produkty przez sieć dystrybucyjną. Będziemy mogli również mieć bieżący obraz bazy punktów sprzedaży detalicznej, znać ich potrzeby oraz trendy rynkowe</p>
					</blockquote>
					<p class="cite"><cite>Jarosław Krysiński</cite> Sales Development Director, Rieber Foods Polska</p>
				</li>
				<li id="#s-2-testimonial-4">
					<blockquote>
						<p>Zaletą uruchomionego rozwiązania Connector  jest bieżące wsparcie naszych dystrybutorów ze strony Asseco Business Solutions, gdzie każdy z nich ma swojego opiekuna. W trakcie opieki nad systemem realizowany jest proces czyszczenia i deduplikacji punktów sprzedaży pochodzących z oddziałów dystrybucyjnych oraz budowa pełnej kartoteki punktów sprzedaży PTK Centertel. Wspomniana kartoteka to liczny zbiór unikalnych rekordów, wśród których znajdują się tradycyjne sklepy, salony prasowe, stacje benzynowe, bankomaty, strony internetowe - czyli każde miejsce, gdzie klient końcowy może kupić produkty PTK Centertel. Na bieżąco również modyfikowane i dostosowywane są raporty zgodnie z naszymi potrzebami</p>
					</blockquote>
					<p class="cite">Fragment referencji od PTK Centertel</p>
				</li>
			</ul>
		</div>
	{else}
		<div class="box-k" id="slider-2">
			<ul class="list-e">
				<li id="#s-2-testimonial-1">
					<blockquote>
						<p>We have decided to implement Connector Platform of Data Exchange because it perfectly fits the needs of the FMCG sector, where the management of distribution is extremely sensitive and important due to the nature of the so-called fast-moving goods. In addition to improving our relationships with distributors, we also aim to optimize the costs of production and storage of our beverages. What is more, the system reveals an array of options aimed to ensure the quality of analysed information collected from our distributors. Such services include standardization, deduplication, geocodification and data mapping.</p>
					</blockquote>
					<p class="cite"><cite>Mateusz Jesiołowski</cite> Hoop Polska</p>
					{*{if $type neq 'reference'}
						{assign var=path value='userfiles/clients/'|cat:$item->clientInfo->main_image}
						{if $item->clientInfo->main_image and $path|is_file}
							<p class="logo"><img src="userfiles/clients/{$item->clientInfo->main_image|thumb_name:1}" alt="{$item->clientInfo->title}"/></p>
						{/if}
					{/if}*}
				</li>
				<li id="#s-2-testimonial-2">
					<blockquote>
						<p>Connector Platform will enable our company to automate procurement and gain access to two types of very important information. <br/> First, it will enable the user to review current inventory of Jutrzenka Colian and monitor stocks at individual distributors. <br/> No more faxing is necessary, since our staff will know when and what volume of replenishment is needed. <br/> ...More than that, Connector Platform will offer access data on the level of resale to retailers completed by the distribution network. <br/> This kind of approach allows us to anticipate costs and revenues and operate more efficiently.</p>
					</blockquote>
					<p class="cite"><cite>Michał Mielcarek</cite> Jutrzenka Colian</p>
				</li>
				<li id="#s-2-testimonial-3">
					<blockquote>
						<p>Connector Platform will surely strengthen our cooperation with our business partners and is expected to bring measurable business benefits both to us and our distributors. <br/>The system should also help rationalize the planning of production resources and quickly respond to any changes in the demand for our products through the distribution network. <br/>We would also be able to follow the current status of the database of retail outlets, know their needs and respond to market trends.</p>
					</blockquote>
					<p class="cite"><cite>Jarosław Krysiński</cite> Sales Development Director, Rieber Foods Polska</p>
				</li>
				<li id="#s-2-testimonial-4">
					<blockquote>
						<p>The advantage of Connector Platform is the continuous support for our distributors from Asseco Business Solutions where each of them has been assigned its own consultant.<br/>As regards system maintenance, the process is put in place of cleansing and deduplicating the point of sale's data arriving from distributor branches, and of creating an exhaustive file of PTK Centertel's points of sale.<br/>This file is a voluminous collection of unique records, which include traditional shops, newsagent stores, gas stations, ATMs, websites – that is any place where the end customer can buy products from PTK Centertel.<br/>The system also modifies and customises reports, depending on what we need.</p>
					</blockquote>
					<p class="cite">An excerpt from the references from PTK Centertel</p>
				</li>
			</ul>
		</div>
	{/if}
</section>