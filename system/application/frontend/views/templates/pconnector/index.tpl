{include file='header.tpl'}
{assign var=item value=$itemInfo}
<section class="box-l">
	{include file="pconnector/pagesHeader.tpl"}

	<ul class="b-crumbs">
		<li><a href="{$baseurl}">{$mainSites[609]->title}</a></li>
		<li>{$mainSites[880]->title}</li>
	</ul>

	<section class="box-m bm-b">
		{if $videosInfo|count > 1}
		<section>
			<div class="video-box-b">
				<div class="vb-container">
					{include file="common_areas/videosList.tpl" specialClass='va-a'}
				</div><!-- .vb-container -->
			</div><!-- .video-box-a -->
			<h1 class="header-e he-a">
				{$item->lead|strip_tags|htmlspecialchars}
			</h1>
		</section><!-- .box-m -->
		{elseif $videosInfo|count eq 0}
			<h1 class="header-e he-a">
				{$item->lead|strip_tags|htmlspecialchars}
			</h1>
		{else}
		<div class="cols-two-d">
			<div class="primary-td">
				<h1 class="header-e he-a">
					{$item->lead|strip_tags|htmlspecialchars}
				</h1>
			</div>
			<div class="secondary-td">
				{include file="common_areas/videosList.tpl" specialClass='va-a'}
			</div>
		</div><!-- .cols-two-d -->
		{/if}
		{*<p class="td-c">{$item->text_data_1|strip_tags:"<strong>"|nl2br}</p>*}
	</section>

	<div class="wrapper-a wpa-a wpa-d">
		{if $info}
			{foreach $info as $textitem}
			{if $textitem}
					<article class="art-d artd-a equal-height">
						<header>
							<h1 class="header-h hh-d">
								<span>{$textitem->lead}</span>
							</h1>
						</header>
						<div class="text-area-a txa-a">
							<p>{$textitem->content|strip_tags:"<strong>"|nl2br}</p>
						</div><!-- .text-area-a -->
					</article>
				{/if}
			{/foreach}

		{*<article class="art-d ad-a equal-height">
			<header>
				<h1 class="header-h hh-d">
					<span>Narzędzie wsparcia biznesowego</span>
				</h1>
			</header>
			<div class="text-area-a">
				<p>Platforma Connector staje się coraz istotniejszym narzędziem wsparcia biznesowego dla wielu liderów rynku m.in. producentów z branży spożywczej, farmaceutycznej, budowlanej czy telekomunikacyjnej. Odbiorcami Platformy Connector są m.in. sieci handlowe i ich dostawcy, hurtownie oraz dystrybutorzy, a także producenci (wsparcie sił sprzedaży i zarządzania łańcuchem dostaw). Źródłem danych dla rozwiązania są zarówno systemy producenckie (np. zestawienia sell in vs sell out, sell in w celach targetowych), jak i różnego rodzaju programy zaimplementowane u dystrybutorów oraz w sieciach detalicznych.</p>
			</div><!-- .text-area-a -->
		</article>
		<article class="art-d ad-a equal-height">
			<header>
				<h1 class="header-h hh-d">
					<span>Obniżenie kosztów inicjalnych</span>
				</h1>
			</header>
			<div class="text-area-a">
				<p>Platforma Connector to narzędzie oferowane jako usługa (w modelu outsourcingowym), co w sposób znaczący obniża koszty inicjalne wdrożenia oraz gwarantuje najwyższy poziom bezpieczeństwa bardzo wrażliwych biznesowo danych. W porównaniu do stosowania tradycyjnych metod wymiany informacji handlowych, dzięki Platformie Connector wymiana danych odbywa się nieporównywalnie szybciej i efektywniej.</p>
			</div><!-- .text-area-a -->
		</article>
		<article class="art-d ad-a equal-height">
			<header>
				<h1 class="header-h hh-d">
					<span>32 miliony komunikatów</span>
				</h1>
			</header>
			<div class="text-area-a">
				<p>Obecnie przez platformę Connector przechodzi miesięcznie ponad 32 miliony komunikatów wymienianych pomiędzy kilkudziesięcioma producentami a siecią prawie dwóch i pół tysiąca oddziałów dystrybucyjnych, co świadczy o ogromnej skali działania i ilości zaangażowanych producentów oraz partnerów handlowych. Asseco BS może pochwalić się już kilkudziesięcioma wdrożeniami tego rozwiązania.</p>
			</div><!-- .text-area-a -->
		</article>*}
		{/if}
	</div>

	<div class="box-r br-a">
		{if $item->file_data_1}<p class="image"><img src="{$item->file_data_1}" {$item->file_data_1_info[3]} alt=""/></p>{/if}
	</div>

	<section class="box-m bm-b bm-a bma-a bm-g">
		{include file="pconnector/overviewBoxes.tpl"}
	</section><!-- .box-m -->

	{include file="homepage/contactBox.tpl" external=true}
	{*
	<p class="image-c"><img src="images/visuals/overview-visual-2.png" width="980" height="242" alt=""/></p>
	*}



	{*<section class="box-m bm-b bm-a bm-g">
		<h1 class="header-g">{$item->text_data_1|strip_tags:"<strong>"}</h1>
		{include file="pconnector/overviewTabs.tpl"}
	</section><!-- .box-m -->*}

	{include file="pconnector/testimonials.tpl"}
</section><!-- .box-l -->
{include file='footer.tpl'}