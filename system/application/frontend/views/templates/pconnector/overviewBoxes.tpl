{if $boxes}
<div class="cols-two-c">
	<article class="primary-tc">
		<h2 class="header-f{if $pagetype eq 'b2b'} hf-d{else} hf-c{/if}">{$boxes[0]->title|replace:"(b2b)":""}</h2>
		<div class="text-area-a">
			{$boxes[0]->lead}
		</div><!-- .text-area-a -->
	</article>
	<article class="secondary-tc">
		<h2 class="header-f hf-b">{$boxes[1]->title|replace:"(b2b)":""}</h2>
		<div class="text-area-a">
			{$boxes[1]->lead}
		</div>
	</article>
</div><!-- .cols-two-c -->
{/if}