{include file='header.tpl'}
{assign var=item value=$itemInfo}
<section class="box-l">
	{include file="pconnector/pagesHeader.tpl"}

	<ul class="b-crumbs">
		<li><a href="{$baseurl}">{$mainSites[609]->title}</a></li>
		<li>{$mainSites[880]->title}</li>
	</ul>

	<section class="box-m bm-b">
		{if $videosInfo|count > 1}
		<section>
			<div class="video-box-b">
				<div class="vb-container">
					{include file="common_areas/videosList.tpl" specialClass='va-a'}
				</div><!-- .vb-container -->
			</div><!-- .video-box-a -->
			<h1 class="header-e he-a">
				{$item->lead|strip_tags|htmlspecialchars}
			</h1>
		</section><!-- .box-m -->
		{elseif $videosInfo|count eq 0}
			<h1 class="header-e he-a">
				{$item->lead|strip_tags|htmlspecialchars}
			</h1>
		{else}
		<div class="cols-two-d">
			<div class="primary-td">
				<h1 class="header-e he-a">
					{$item->lead|strip_tags|htmlspecialchars}
				</h1>
			</div>
			<div class="secondary-td">
				{include file="common_areas/videosList.tpl" specialClass='va-a'}
			</div>
		</div><!-- .cols-two-d -->
		{/if}
		{*<p class="td-c">{$item->text_data_1|strip_tags:"<strong>"|nl2br}</p>*}
	</section>

	<div class="wrapper-a wpa-a wpa-d">
		{if $info}
			{foreach $info as $textitem}
			{if $textitem}
					<article class="art-d artd-a equal-height">
						<header>
							<h1 class="header-h hh-d">
								<span>{$textitem->lead}</span>
							</h1>
						</header>
						<div class="text-area-a txa-a">
							<p>{$textitem->content|strip_tags:"<strong>"|nl2br}</p>
						</div><!-- .text-area-a -->
					</article>
				{/if}
			{/foreach}
		{/if}
	</div>

	<div class="box-r br-a">
		{if $item->file_data_1}<p class="image"><img src="{$item->file_data_1}" {$item->file_data_1_info[3]} alt=""/></p>{/if}
	</div>

	<section class="box-m bm-b bm-a bma-a bm-g">
		{include file="pconnector/overviewBoxes.tpl" pagetype="b2b"}
	</section><!-- .box-m -->

	{include file="homepage/contactBox.tpl" external=true}
	{*
	<p class="image-c"><img src="images/visuals/overview-visual-2.png" width="980" height="242" alt=""/></p>
	*}



	{*<section class="box-m bm-b bm-a bm-g">
		<h1 class="header-g">{$item->text_data_1|strip_tags:"<strong>"}</h1>
		{include file="pconnector/overviewTabs.tpl"}
	</section><!-- .box-m -->*}

	{include file="pconnector/testimonials.tpl"}
</section><!-- .box-l -->
{include file='footer.tpl'}