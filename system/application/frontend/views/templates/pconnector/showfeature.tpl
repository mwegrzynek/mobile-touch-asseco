{include file='header.tpl'}
{assign var=item value=$itemInfo}
<section class="box-l bl-a">
	{include file="pconnector/pagesHeader.tpl"}
	
	<ul class="b-crumbs">
		<li><a href="{$baseurl}">{$mainSites[609]->title}</a></li>		
		<li><a href="{$siteRoutes[880]->slug}.html">{$mainSites[880]->title}</a></li>
		<li><a href="{$siteRoutes[880]->slug}/{$siteRoutes[881]->slug}.html">{$mainSites[881]->title}</a></li>
		<li>{$pageInfo->title}</li>
	</ul>
	
	<div class="cols-two-f">
		<div class="primary-tf">
			<section class="box-r">
				<h1 class="header-i">{$pageInfo->title}</h1>
				{if $pageInfo->lead}
					<p class="text-d">{$pageInfo->lead|strip_tags|htmlspecialchars}</p>
				{/if}	
				{if $pageInfo->file_data_1}<p class="image-e"><img src="{$pageInfo->file_data_1}" {$pageInfo->file_data_1_info[3]} alt=""/></p>{/if}				
			</section><!-- .box-r -->
			{if $subpages}
				{foreach $subpages as $item}
					{if $item->content}
					<div class="short-col">						
						<section class="box-m {if !$item@last}bm-c{/if} bm-b">						
							{$item->content}							
						</section>
					</div><!-- .short-col -->
					{/if}
				{/foreach}				
			{/if}
		</div>
		<div class="secondary-tf">			
			{include file="company/industriesList.tpl" industries=$industries url="{$siteRoutes[880]->slug}/{$siteRoutes[881]->slug}" currentCategory=$currentCategory type="short" noDefault=true}			
			
			{if $pages}				
				<ul class="list-o">
					{foreach $pages as $page}
						<li class="{if $page@last}last {/if}{if $pageInfo->id eq $page->id}active{/if}"><a href="{$siteRoutes[880]->slug}/{$siteRoutes[881]->slug}/{$page->slug}.html">{$page->title}</a></li>	
					{/foreach}			
				</ul>
			{/if}
			
			{if $testimonials}
				{assign var=testimonial value=$testimonials[0]}
				<section class="box-s">
					<h1 class="header-b hb-a">{$siteTexts[709]->content|strip_tags}</h1>					
					<article class="art-e">
						<blockquote>
							<p>{$testimonial->lead|nl2br}</p>
						</blockquote>
						{assign var=path value='userfiles/clients/'|cat:$testimonial->clientInfo->main_image}
						{if $testimonial->clientInfo->main_image and $path|is_file}
							<p class="logo"><img src="userfiles/clients/{$testimonial->clientInfo->main_image|thumb_name:1}" alt="{$testimonial->clientInfo->title}"/></p>	
						{/if}
						<p class="cite"><cite>{$testimonial->title|strip_tags|htmlspecialchars}</cite> {$testimonial->text_data_1|strip_tags|htmlspecialchars}</p>
					</article>									
				</section>
			{/if}
		</div>
	</div><!-- .cols-two-f -->
</section><!-- .box-l -->
{include file='footer.tpl'}