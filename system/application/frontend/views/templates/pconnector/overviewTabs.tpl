<ul class="list-m tabs-a">
	{foreach $tabBoxes as $tab}
		<li class="item-{$tab@iteration}{if $tab@last} last{/if}"><a href="#{$tab->slug}">{$tab->title}</a></li>	
	{/foreach}	
</ul>
<div class="panes-a">
	{foreach $tabBoxes as $tab}
	<section id="{$tab->slug}" class="pane-a box-n bn-{$tab@iteration}">
		<header>
			<h1>{$tab->title}</h1>
			<p>{$tab->lead}</p>
		</header>
		<div class="cols-two-c">
			<article class="primary-tc text-area-a">
				<p>{$tab->content|nl2br}</p>
			</article>
			<article class="secondary-tc">
				<h2 class="header-b hb-a">{$siteTexts[848]->content|strip_tags}</h2>
				<p class="text-c">{$tab->text_data_1|nl2br}</p>
			</article>
		</div><!-- .cols-two-c -->
	</section><!-- .pane-a -->
	{/foreach}
</div><!-- .panes-a -->