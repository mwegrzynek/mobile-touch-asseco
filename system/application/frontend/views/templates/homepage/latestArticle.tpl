<article class="art-a primary-ta">
	<header>
		{assign var=item value=$latestArticle}				
		{assign var=url value="artykuly/{$item->slug}.html"}				
		{if $item->lead neq ''}					
			{assign var=desc value=$item->lead}
		{else}
			{assign var=desc value=$item->content|strip_tags|htmlspecialchars|truncate:160:"&hellip;":false}
		{/if}
			
		<h1 class="header-a"><a href="{$url}">{$item->title}</a></h1>
		{if $item->categories}
			<h2 class="offset">Kategorie artykułu:</h2>
			{include file="common_areas/categoryList.tpl" categoryList=$item->categories}
		{/if}
		
		<aside class="additional-a">
			{assign var=month value=$item->add_date|date_format:"%m"*1}								
			<p class="date-a"><time datetime="{$item->add_date|date_format:"%Y-%m-%dT%H:%M"}"><span class="day">{$item->add_date|date_format:"%d"}</span> <span class="month">{$months[$month]|upper}</span>/<span class="year">{$item->add_date|date_format:"%Y"}</span></time></p>
			{include file="common_areas/addthisButtons.tpl" title=$item->title desc=$desc ur=$url}				
		</aside><!-- .additional -->
	</header>
	<section class="content-a">						
		<p>{$desc}</p>
		<p class="more-a"><a href="{$url}" class="link-b">Czytaj więcej</a></p>
	</section><!-- .content-a -->
	<footer>
		<h2 class="header-b">Autor:</h2> 
		<p><a href="kancelarie/{$item->author[0]->slug}.html">{$item->author[0]->title}</a></p>
		<p class="comments-a"><span>Komentarze</span> <a href="{$url}#comments">{$item->comments_count}</a></p>						
	</footer>
</article>