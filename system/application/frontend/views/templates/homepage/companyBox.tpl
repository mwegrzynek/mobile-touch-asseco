<section class="box-d">
	<div class="wrapper-a wpa-a">
		<article class="art-a">
			<h1 class="header-b">{$siteTexts[711]->content|strip_tags}</h1>
			<p>{$siteTexts[713]->content|strip_tags:'<strong><a>'}</p>
		</article>
		<ul class="list-d">
			{include file="company/box.tpl" item=$boxes[0] itemClass='item-a' full=false}
			{include file="company/box.tpl" item=$boxes[1] itemClass='item-b' full=false}
			{include file="company/box.tpl" item=$boxes[2] itemClass='item-c' full=false}
		</ul>
		<p class="btn-b"><a href="{$siteRoutes[636]->slug}.html" class="link-b">{$siteTexts[712]->content|strip_tags}</a></p>
	</div><!-- .wrapper-a -->
</section><!-- .box-d -->