{if $logoSlider}
<div class="box-aa">	
	<div class="wrapper-a wpa-c">
		<h1 class="header-b hb-a hb-b">{$siteTexts[876]->content|strip_tags}</h1>
	</div><!-- .wrapper-a -->
	
	<div class="wrapper-d">
		<p class="text-g"><a href="{$siteRoutes[636]->slug}/{$siteRoutes[635]->slug}.html" class="link-e">{$siteTexts[710]->content|strip_tags}</a></p>
		<div class="box-z logo-slider" id="logo-slider">
			{foreach $logoSlider as $item}
				<section class="slider-page">
					<ul class="list-w">
						{foreach $item as $subitem}
						<li><img src="userfiles/logoslider/{$subitem->main_image}" width="128" height="97" alt="{$subitem->title}"/></li>	
						{/foreach}
					</ul>
				</section><!-- .slider-page -->
			{/foreach}		
		</div><!-- .box-z -->
	</div><!-- .wrapper-a -->
</div><!-- .box-aa -->
{/if}