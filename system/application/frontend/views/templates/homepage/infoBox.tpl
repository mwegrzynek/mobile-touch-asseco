<section class="box-e be-a">
	<div class="cols-two-a">
		<div class="primary-ta">
			<h1 class="header-b hb-a hb-b">{$siteTexts[705]->content|strip_tags}</h1>
				
			<ul class="list-f">
				{foreach $teamleaders as $item}
					<li><a href="#teamleader{$item@iteration}"><img src="userfiles/teamleaders/{$item->main_image}" width="139" height="124" alt="{$item->title} {$item->text_data_2}"/></a></li>					
				{/foreach}
			</ul>
			
			<ul class="list-g">
				{foreach $teamleaders as $item}
				<li>
					<blockquote>
						<p>{$item->lead|strip_tags|htmlspecialchars}</p>
					</blockquote>
					<p class="cite"><cite>{$item->title} {$item->text_data_2}</cite> {$item->text_data_1}</p>							
				</li>
				{/foreach}			
			</ul>
			<p class="btn-b"><a href="{$siteRoutes[636]->slug}/{$siteRoutes[607]->slug}.html" class="link-b lnkb-a">{$siteTexts[706]->content|strip_tags}</a></p>
		</div>
		<div class="secondary-ta">
			<h1 class="header-b hb-a hb-b">{$siteTexts[707]->content|strip_tags}</h1>
			
			{include file="news/latestNews.tpl"}
			
			<p class="btn-b"><a href="{$siteRoutes[207]->slug}.html" class="link-b lnkb-a">{$siteTexts[708]->content|strip_tags}</a></p>
		</div>
	</div><!-- .cols-two-a -->
</section><!-- .box-e -->			