{if $videoSlider}
<section class="box-b carousel-main-container">
	<div class="wrapper-a wpa-a">
		<div class="wrapper-b">
			<h2 class="header-a">{$siteTexts[847]->content|strip_tags:"<strong><a>"}</h2>
		</div><!-- .wrapper-b -->
		<div class="box-c" id="slider-1">
			<ul class="list-k{if $videoSlider|count > 2} scrollable{else} lk-a{/if}" id="carousel-1">
				{foreach $videoSlider as $item}
				<li class="video-a">
					<a data-id="{$item@index}" class="{if $item->videoInfo->type_id eq 1}yt{else}vm{/if}" href="{if $item->videoInfo->type_id eq 1}http://www.youtube.com/embed/{$item->videoInfo->link|yt_id}?rel=0{else}http://player.vimeo.com/video/{$item->videoInfo->link|replace:"http://vimeo.com/":""}{/if}" data-index="{$item@index}">
						<img src="userfiles/sliders/{$item->main_image}" width="220" height="124" alt=""/>
						<span class="subtitle">{$item->title}</span>
					</a>
				</li>
				{/foreach}
			</ul>
		</div><!-- .box-c -->
	</div>
</section><!-- .box-b -->
{/if}