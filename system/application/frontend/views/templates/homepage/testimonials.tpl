<section class="box-e">				
	<div class="wrapper-a">
		<section class="box-j sequence-container-b">
			<h1 class="header-b hb-a">{$siteTexts[709]->content|strip_tags}</h1>
			{include file="testimonials/testimonialsList.tpl" testimonials=$testimonials}
			<p class="btn-b"><a href="{$siteRoutes[636]->slug}/{$siteRoutes[635]->slug}.html" class="link-b lnkb-a">{$siteTexts[710]->content|strip_tags}</a></p>
		</section>					
	</div><!-- .wrapper-a -->
</section><!-- .box-e -->