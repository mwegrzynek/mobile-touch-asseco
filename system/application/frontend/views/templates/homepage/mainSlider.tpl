<section class="box-a sequence-container-a">
	<ul class="list-c">
		<li class="active" data-index="0"><a href="#s-1-slide-1">Slide 1</a></li>
		<li data-index="1"><a href="#s-1-slide-2">Slide 2</a></li>
		<li data-index="2"><a href="#s-1-slide-3">Slide 3</a></li>
	</ul>
	<div class="slider-a" id="sequence-1">
		<ul class="list-b">
			<li id="s-1-slide-1">
				<div class="content slide-1-content">
					<p class="text">{$mainSlider[3]->content|strip_tags:"<strong>"}</p>
					<p class="btn"><a href="{$mainSlider[3]->text_data_2}" class="link-a">{$mainSlider[3]->text_data_1}</a></p>
				</div><!-- .content -->
				<div class="visual-conrainer-a">
					{if $currentLanguageInfo[0]->lang_code eq 'pl'}
						<img class="visual-a vis-a-a" src="images/sliders/pl/phone-1.png" width="111" height="200" alt=""/>
						<!-- <img class="visual-a vis-a-b" src="images/sliders/pl/tablet-1.png" width="290" height="379" alt=""/> -->
						<img class="visual-a vis-a-b" src="images/sliders/slide-grnt/tablet-1-pl.png" width="383" height="413" alt=""/>
					{else}
						<img class="visual-a vis-a-a" src="images/sliders/phone-1.png" width="111" height="200" alt=""/>
						<!-- <img class="visual-a vis-a-b" src="images/sliders/tablet-1.png" width="290" height="379" alt=""/> -->
						<img class="visual-a vis-a-b" src="images/sliders/slide-grnt/tablet-1-en.png" width="383" height="413" alt=""/>
					{/if}
					<img class="visual-a vis-a-c" src="images/sliders/vis-1-backlights.png" width="879" height="397" alt=""/>
				</div>
			</li>
			<li id="s-1-slide-2">
				<div class="content slide-2-content">
					<p class="text">{$mainSlider[1]->content|strip_tags:"<strong>"}</p>
					<p class="btn"><a href="{$mainSlider[1]->text_data_2}" class="link-a">{$mainSlider[1]->text_data_1}</a></p>
				</div>
				{if $currentLanguageInfo[0]->lang_code eq 'pl'}
					<img class="visual-a vis-b-a" src="images/sliders/pl/tablet-2.png" width="486" height="424" alt=""/>
				{else}
					<img class="visual-a vis-b-a" src="images/sliders/tablet-2.png" width="486" height="424" alt=""/>
				{/if}
				<img class="visual-a vis-b-b" src="images/sliders/vis-2-backlights.png" width="672" height="448" alt=""/>
			</li>
			<li id="s-1-slide-3">
				<div class="content slide-3-content">
					<p class="text">{$mainSlider[2]->content|strip_tags:"<strong>"}</p>
					<p class="btn"><a href="{$mainSlider[2]->text_data_2}" class="link-a">{$mainSlider[2]->text_data_1}</a></p>
				</div>

				<img class="visual-a vis-c-a" src="images/sliders/slide-3-new/hand.png" width="173" height="229" alt=""/>
				<img class="visual-a vis-c-b" src="images/sliders/slide-3-new/glow.png" width="395" height="395" alt=""/>
				<img class="visual-a vis-c-c" src="images/sliders/slide-3-new/circle-inner.png" width="88" height="88" alt=""/>
				<img class="visual-a vis-c-d" src="images/sliders/slide-3-new/circle-outer.png" width="144" height="144" alt=""/>
				<img class="visual-a vis-c-e" src="images/sliders/slide-3-new/ico-1.png" width="100" height="101" alt=""/>
				<img class="visual-a vis-c-f" src="images/sliders/slide-3-new/ico-2.png" width="77" height="78" alt=""/>
				<img class="visual-a vis-c-g" src="images/sliders/slide-3-new/ico-3.png" width="100" height="101" alt=""/>
				<img class="visual-a vis-c-h" src="images/sliders/slide-3-new/ico-4.png" width="77" height="78" alt=""/>
				<img class="visual-a vis-c-i" src="images/sliders/slide-3-new/ico-5.png" width="100" height="101" alt=""/>
				<img class="visual-a vis-c-j" src="images/sliders/slide-3-new/ico-6.png" width="76" height="78" alt=""/>

				<span class="visual-a vis-c-l">
					<img class="visual-a vis-c-k" src="images/sliders/slide-3-new/line-1.png" width="61" height="61" alt=""/></span>
				<span class="visual-a vis-c-m">
					<img class="visual-a vis-c-k" src="images/sliders/slide-3-new/line-2.png" width="105" height="2" alt=""/></span>
				<span class="visual-a vis-c-n">
					<img class="visual-a vis-c-k" src="images/sliders/slide-3-new/line-3.png" width="61" height="61" alt=""/></span>
				<span class="visual-a vis-c-o">
					<img class="visual-a vis-c-k" src="images/sliders/slide-3-new/line-4.png" width="2" height="102" alt=""/></span>
				<span class="visual-a vis-c-p">
					<img class="visual-a vis-c-k" src="images/sliders/slide-3-new/line-5.png" width="93" height="93" alt=""/></span>
				<span class="visual-a vis-c-r">
					<img class="visual-a vis-c-k" src="images/sliders/slide-3-new/line-6.png" width="104" height="2" alt=""/></span>
			</li>
		</ul>
	</div><!-- #sequence-1 -->
</section><!-- .sequence-container -->