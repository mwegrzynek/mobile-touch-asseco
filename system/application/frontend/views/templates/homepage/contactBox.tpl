<section class="box-f request-contact-box{if $external} bf-a no-mask{/if}">
	<div class="wrapper-a wpa-b">
		<header class="h-box-a request-contact-trigger{if $success} sent collapsed{/if}">
			{if !$success}
			<h1 class="header-c">{$siteTexts[654]->content|strip_tags}</h1>
			{if $external}
				<p class="info-text">{$siteTexts[923]->content|strip_tags}</p>
			{else}
				<p class="info-text">{$siteTexts[656]->content|strip_tags}</p>
			{/if}
			<p><a href="#" class="link-c only-js">Expand</a></p>
			{else}
			<h1 class="header-c">{$siteTexts[658]->content|strip_tags}</h1>
			<p class="info-text">{$siteTexts[659]->content|strip_tags}</p>
			{/if}
		</header>
		<section class="box-g request-contact-container">
			{if !$success}
				<form action="{$baseurl}" method="post" class="form-a request-contact-form" id="request-contact-form">
					{*<p class="text-b">{$siteTexts[657]->content|strip_tags}</p>*}

					{if $errors.short}
					<section class="info-box error">
						<h2>{$siteTexts[661]->content|strip_tags}</h2>
						<ul>
							{foreach $errors.short as $item}
								<li>{$item}</li>
							{/foreach}
						</ul>
					</section><!-- .error -->
					{/if}

					{include file="contact/shortFormFields.tpl" type='short'}

					<p class="btn">
						{if $external}
							<input type="hidden" name="typeof" value="platformaconnector" />
						{else}
							<input type="hidden" name="typeof" value="regular" />
						{/if}
						<button type="submit" class="link-a"{if $currentLanguageInfo[0]->lang_code eq 'pl'} onClick="_gaq.push(['_trackPageview','/Mobile_Touch_main_kontakt']);"{/if}>{$siteTexts[653]->content|strip_tags}</button>
					</p>
					<p class="text-k">{$siteTexts[922]->content|strip_tags|nl2br}</p>
				</form>
				<p class="btn-c"><a href="#" class="link-b lnkb-a lnkb-b request-contact-trigger only-js">{$siteTexts[660]->content|strip_tags}</a></p>
			{/if}
		</section><!-- .box-g -->
	</div>
</section><!-- .box-f -->