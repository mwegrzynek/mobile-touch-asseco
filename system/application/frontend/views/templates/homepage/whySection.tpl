<section class="box-ac">
	<div class="wrapper-e">
		<article class="box-ad">
			<h2 class="header-n">{$siteTexts[909]->content|strip_tags}</h2>
			<div class="content">
				<ul class="list-x">
					{if $whyUs}
						{foreach $whyUs as $item}
							<li>{$item->title}</li>
						{/foreach}
					{/if}
				</ul>
				<p class="button-a"><a href="{$siteRoutes[636]->slug}.html#why-us" class="link-g">More</a></p>
			</div>
		</article>

		<article class="box-ad">
			<h2 class="header-n hn-a">{$siteTexts[910]->content|strip_tags}</h2>
			<div class="content">
				<ul class="list-x lx-a">
					{if $whyMobileTouch}
						{foreach $whyMobileTouch as $item}
							<li>{$item->title}</li>
						{/foreach}
					{/if}
				</ul>
				<p class="button-a"><a href="{$siteRoutes[694]->slug}.html#why-mobile-touch" class="link-g lnkg-a">More</a></p>
			</div>
		</article>
	</div>
</section>