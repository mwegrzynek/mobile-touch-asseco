		</section><!-- #content -->
	</div><!-- .root -->

	<footer id="footer" role="contentinfo">
		<div class="cols-two-b ctb-a">
			<h2 class="header-d">Asseco Business Solutions</h2>
			<div class="primary-tb">
				{if $footerMenu}
				<ul class="list-i">
					{foreach $footerMenu as $item}
						<li{if in_array($item->id,$activeIds)} class="active"{/if}>
							<a href="{$item->link}">{$item->name}</a>
						</li>
					{/foreach}
				</ul>
				{/if}
				<p class="text-l"><a href="https://www.linkedin.com/company/mobile-touch---sales-force-automation-system?trk=biz-brand-tree-co-name" rel="external"><img src="images/linkedin-logo.png" alt="LinkedIn" width="100" height="24" /></a></p>
			</div>
			<div class="secondary-tb">
				<dl class="list-j">
					<dt>Tel.</dt>
					<dd>{$siteTexts[899]->content|strip_tags}</dd>

					<dt class="item-a">Email</dt>
					<dd><a href="mailto:{$siteTexts[883]->content|strip_tags}">{$siteTexts[883]->content|strip_tags}</a></dd>
				</dl>
			</div>
		</div><!-- .cols-two-b -->

		<div class="cols-two-b">
			<div class="primary-tb">
				<p class="text-a">Copyright by © Asseco BS</p>
			</div>
			<div class="secondary-tb">
				<p class="text-a ta-a"><span>Designed and developed by</span> <a href="http://hitmo-studio.com" class="author">hitmo</a></p>
			</div>
		</div><!-- .cols-two-b -->

		<p class="text-j">{$siteTexts[907]->content|strip_tags:'<strong><a>'|nl2br}</p>

	</footer><!-- #footer -->

	<div class="modal video-box-a" id="modal-box-1">
		<a href="#close" class="modal-close">Close</a>
		<div class="wrapper">
			<div class="content">
			</div><!-- .content -->
		</div><!-- .wrapper -->
	</div><!-- .modal video-box-a -->

	<p id="top-button" class="top-button"><a href="#content" class="link-c">go to top</a></p>

	{literal}
	<!-- Google Tag Manager -->
	<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-HVLQ" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-HVLQ');</script>
	<!-- End Google Tag Manager -->
	{/literal}

	{literal}
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="js/jquery-1.9.1.min.js"><\/script>')</script>
	<script src="js/scripts.js?v=2.8"></script>
	{/literal}
</body>
</html>