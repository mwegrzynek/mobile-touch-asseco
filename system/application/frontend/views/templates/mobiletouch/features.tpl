{include file='header.tpl'}
{assign var=item value=$itemInfo}
<section class="box-l">
	{include file="mobiletouch/pagesHeader.tpl"}
	
	<ul class="b-crumbs">
		<li><a href="{$baseurl}">{$mainSites[609]->title}</a></li>		
		<li><a href="{$siteRoutes[694]->slug}.html">{$mainSites[694]->title}</a></li>
		<li>{$mainSites[215]->title}</li>
	</ul>
	
	{include file="company/industriesList.tpl" industries=$industries url="{$siteRoutes[694]->slug}/{$siteRoutes[215]->slug}" currentCategory=$currentCategory type='extended' noDefault=true}
	
	{if $pages}		
		<div class="wrapper-c">	
			{foreach $pages as $page}			
				<section class="box-r">
					<h1 class="header-i">{$page->title}</h1>
					<p class="text-d">{$page->lead}</p>	
					<p class="btn-b btnb-a"><a href="{$siteRoutes[694]->slug}/{$siteRoutes[215]->slug}/{$page->slug}.html" class="link-a lnka-a">{$siteTexts[849]->content|strip_tags}</a></p>
					{if $page->file_data_1}<p class="image-e"><img src="{$page->file_data_1}" {$page->file_data_1_info[3]} alt=""/></p>{/if}
				</section><!-- .box-m -->
			{/foreach}		
		</div><!-- .wrapper-c -->
		{include file="mobiletouch/testimonials.tpl"}
	{/if}
	
</section><!-- .box-l -->
{include file='footer.tpl'}