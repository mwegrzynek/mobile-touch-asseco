{include file="mobiletouch/subnav.tpl"}
<article class="art-b{if $item->main_image eq ''} artb-a{/if}">
	{if $item->main_image neq '' and $item->file_data_3 neq ''}<div class="box-ab">{/if}
		<h1 class="header-e">
			{$item->text_data_2|strip_tags:"<strong>"}
		</h1>

		{if $item->file_data_3}
			<p class="btn"><a href="{$item->file_data_3}" class="link-a">{$siteTexts[885]->content|strip_tags}</a></p>
		{/if}
		{*{if $item->main_image}
			<p class="image"><img src="userfiles/mtpages/{$item->main_image}" alt=""/></p>
		{/if}*}
	{if $item->main_image neq '' and $item->file_data_3 neq ''}</div><!-- .box-ab -->{/if}

	{if $item->file_data_2}<p class="image"><img src="{$item->file_data_2}" {$item->file_data_2_info[3]} alt=""/></p>{/if}
</article><!-- .art-b -->