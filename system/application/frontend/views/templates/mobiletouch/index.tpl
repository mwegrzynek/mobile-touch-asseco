{include file='header.tpl'}
{assign var=item value=$itemInfo}
<section class="box-l">
	{include file="mobiletouch/pagesHeader.tpl"}

	<ul class="b-crumbs">
		<li><a href="{$baseurl}">{$mainSites[609]->title}</a></li>
		<li>{$mainSites[694]->title}</li>
	</ul>

	<section class="box-m bm-b">
		{if $videosInfo|count > 1}
		<section>
			<h1 class="header-e he-a">
				{$item->lead|strip_tags|htmlspecialchars}
			</h1>
			<div class="video-box-b">
				<div class="vb-container">
					{include file="common_areas/videosList.tpl" specialClass='va-a'}
				</div><!-- .vb-container -->
			</div><!-- .video-box-a -->
		</section><!-- .box-m -->
		{else}
		<div class="cols-two-d">
			<div class="primary-td">
				<h1 class="header-e he-a">
					{$item->lead|strip_tags|htmlspecialchars}
				</h1>
			</div>
			<div class="secondary-td">
				{include file="common_areas/videosList.tpl" specialClass='va-a'}
			</div>
		</div><!-- .cols-two-d -->
		{/if}
		{include file="mobiletouch/overviewBoxes.tpl"}
	</section><!-- .box-m -->

	{*
	<p class="image-c"><img src="images/visuals/overview-visual-2.png" width="980" height="242" alt=""/></p>
	*}

	<div class="box-r br-a">
	{if $item->file_data_1}<p class="image"><img src="{$item->file_data_1}" {$item->file_data_1_info[3]} alt=""/></p>{/if}
	</div>

	<section class="box-m bm-b">
		<h1 class="header-g">{$item->text_data_1|strip_tags:"<strong>"}</h1>
		{include file="mobiletouch/overviewTabs.tpl"}
	</section><!-- .box-m -->

	{if $currentLanguageInfo[0]->lang_code eq 'en' or $currentLanguageInfo[0]->lang_code eq 'de'}
		{include file="mobiletouch/whyMobile.tpl"}
	{/if}

	{include file="mobiletouch/testimonials.tpl"}
</section><!-- .box-l -->
{include file='footer.tpl'}