<section class="box-ac bac-b" id="why-mobile-touch">
	<h2 class="header-n hn-a hn-c">{$siteTexts[910]->content|strip_tags}</h2>
	<ul class="list-c lc-a lc-b sequence-container-c-nav">
		{foreach $whyMobileTouch as $item}
			<li><a href="#why-mobile-section-{$item@iteration}">{$item@iteration}</a></li>
		{/foreach}
	</ul>
	<div class="box-ae sequence-container-c">
		{foreach $whyMobileTouch as $item}
		<div class="tab-pane" id="#why-mobile-section-{$item@iteration}">
			<article class="box-af baf-a col-a">
				<h2>{$item[0]->title}</h2>
				<p>{$item[0]->content|nl2br}</p>
			</article>
			<article class="box-af baf-a col-b">
				<h2>{$item[1]->title}</h2>
				<p>{$item[1]->content|nl2br}</p>
			</article>
		</div>
		{/foreach}
	</div>
</section>