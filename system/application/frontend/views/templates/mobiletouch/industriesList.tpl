{if $industries}
<div class="box-p{if $type eq 'short'} bp-a{/if}">					
	<section class="box-o">
		<h2>Choose industry:</h2>
		<h3><span class="offset">Current industry:</span> {if !$currentCategory}All Industries{else}{$currentCategory->name}{/if}</h3>				
	</section><!-- .box-o -->	
	<ul class="list-n">
		<li><a href="{$siteRoutes[636]->slug}/{$siteRoutes[635]->slug}.html">All Industries</a></li>
		{foreach $industries as $industryItem}
			<li{if $industryItem->id eq $currentCategory->id} class="active"{/if}><a href="{$url}/{$industryItem->slug}">{$industryItem->name}</a></li>
		{/foreach}	
	</ul>
</div><!-- .box-p -->
{/if}