{include file='header.tpl'}
{assign var=item value=$itemInfo}
<section class="box-l">
	{include file="mobiletouch/pagesHeader.tpl"}	
	
	<ul class="b-crumbs">
		<li><a href="{$baseurl}">{$mainSites[609]->title}</a></li>		
		<li><a href="{$siteRoutes[694]->slug}.html">{$mainSites[694]->title}</a></li>
		<li>{$mainSites[2]->title}</li>
	</ul>
	
	<div class="wrapper-c">				
		{if $concepts}			
			{foreach $concepts as $conceptItem}
				<section class="box-m bm-c bm-b {cycle values="format-a,format-b"}">
					<div class="cols-two-e">
						<h1 class="header-h">							
							{if $conceptItem->text_data_1}
								<img src="{$conceptItem->text_data_1}" alt=""/>{$conceptItem->title}
							{/if}
						</h1>
						<div class="primary-te short-col">
							{$conceptItem->content}
						</div>
						<div class="secondary-te">
							{if $conceptItem->file_data_1}<p class="image"><img src="{$conceptItem->file_data_1}" {$conceptItem->file_data_1_info[3]} alt=""/></p>{/if}							
						</div>
					</div><!-- .cols-two-d -->					
				</section><!-- .box-m -->
			{/foreach}			
		{/if}
	</div><!-- .wrapper-c -->
	
	{include file="mobiletouch/testimonials.tpl"}	
</section><!-- .box-l -->
{include file='footer.tpl'}