{if $boxes}			
<div class="cols-two-c">
	<article class="primary-tc">
		<h2 class="header-f hf-a">{$boxes[0]->title}</h2>
		<div class="text-area-a">								
			{$boxes[0]->lead}
		</div><!-- .text-area-a -->
	</article>
	<article class="secondary-tc">
		<h2 class="header-f hf-b">{$boxes[1]->title}</h2>
		<div class="text-area-a">					
			{$boxes[1]->lead}
		</div>
	</article>
</div><!-- .cols-two-c -->
{/if}