{include file='header.tpl'}
	<section class="box-l bl-a">
		<div class="box-ak">
			<h1>{$item->lead|nl2br}</h1>
			<p class="lead">{$item->text_data_1|nl2br}</p>
			<p>
				<a class="link-a" href="contact">{$item->file_data_1}</a>
			</p>
		</div>
		<ul class="b-crumbs">
			<li><a href="{$baseurl}">{$mainSites[609]->title}</a></li>
			<li>{$item->title}</li>
		</ul>
		<div class="box-m bm-c bm-b">
			{$item->content}

			<h2 class="header-p">{$item->text_data_2}</h2>
			<section class="box-ai bai-a">
				<div class="cols-two-l">
					<div class="primary-tl">
						<ul class="list-f lf-a">
							{foreach $testimonials as $titem}
								<li><a href="#testimonial-{$titem@key}"><img src="userfiles/partnerstestimonials/{$titem->main_image}" alt="" width="139" height="124" /></a></li>
							{/foreach}
						</ul>
					</div>
					<div class="secondary-tl">
						<ul class="list-g lg-a">
							{foreach $testimonials as $titem}
							<li>
								<article class="art-h ah-a">
									<blockquote>
										<p>{$titem->lead|nl2br}</p>
									</blockquote>
									<p class="cite">{$titem->title}</p>
								</article>
							</li>
							{/foreach}
						</ul>
					</div>
				</div><!-- .cols-two-e <-->
			</section>
		</div><!-- .box-m -->
		<div class="box-m bm-c bm-b">
			{$item->text_data_3}

			{$item->text_data_4}
		</div>
		<div class="box-m bm-c bm-b">
			{$suplementItem->content}
		</div>
		<div class="box-m bm-b">
			{$suplementItem->text_data_3}
			{*<div class="cols-two-m ctm-a">
				<h2 class="header-p">Contact details</h2>
				<div class="primary-tm">
					<p>If you have more questions or plan to join us as our Mobile Touch Partner, fiil in the contact form or send me the message directly.</p>
				</div>
				<div class="secondary-tm">
					<article class="art-k">
						<p class="image"><img src="temp/potrzyszcz.jpg" width="139" height="124" alt="" /></p>
						<div class="content">
							<h2>Łukasz Potrzyszcz</h2>
							<p class="position">International Business Development Director</p>
							<p class="mail">e-mail: <a href="mailto:partners@assecobs.pl">partners@assecobs.pl</a></p>
						</div>
					</article>
				</div>
			</div><!-- .cols-two-m -->*}
		</div>
		<p class="button-c-container">
			<a class="button-c" href="contact"><span>{$suplementItem->text_data_2}</span></a>
		</p>
	</section><!-- .box-l -->
{include file='footer.tpl'}