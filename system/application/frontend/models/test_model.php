<?php

   class Test_model extends Model 
   {
      function __construct()
      {
         parent::Model();
      }
      
      function getTestValues()
      {         
         $query = $this->db->get('sample');
         return $query;
      }
      
      function generateTestValues()
      {
         $letters = range('a', 'z');
         for($i=0; $i<20; $i++)
         {
            $sampleName = '';
            for($j=0; $j<8; $j++)
            {
               $sampleName .= $letters[array_rand($letters)];
            }
            
            $data = array(               
               'name' => $sampleName                
            );

            $this->db->insert('sample', $data);            
         }
         return true;
      }
     
   }