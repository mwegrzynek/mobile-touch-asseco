<?php

	class Hooks
	{
		function Hooks()
		{
			$this->Engine = & get_instance();
			$this->Engine->mysmarty->assign('baseurl', base_url());
			//$this->Engine->mysmarty->clear_all_cache();
			$this->Engine->load->model('../../models/LanguageModel');			
			
			$this->Engine->load->library('form_validation');
			$this->Engine->load->library('langlib');
			$this->Engine->form_validation->set_error_delimiters('', '###');
			
			$this->Engine->load->library('detectdevice');
			$this->Engine->load->library('urllib');
			//$this->Engine->config->set_item('enable_query_strings', TRUE);			
		}

		// --------------------------------------------------------------------		

		function getLanguage()
		{
			
			
			//set default language
			if(!$this->Engine->session->userdata('defaultLanguageId'))
			{
				$this->Engine->load->model('../../models/OptionsModel');
				$this->Engine->session->set_userdata('defaultLanguageId', $this->Engine->OptionsModel->getDefaultLanguageId());
			}					
				
			$address = str_replace(array('http://', '/', 'index.php', 'www.'), '', $this->Engine->config->site_url());	
			$address = preg_replace('/^m\./', '', $address);
			
			$this->Engine->mysmarty->assign('noMobileAddress', $address);
					
			$langInfo = $this->Engine->LanguageModel->getLanguageBySubdomain($address);

			if(count($langInfo))
			{
				$this->Engine->session->set_userdata('languageid', $langInfo[0]->id);
			}
			else
			{
				$this->Engine->session->set_userdata('languageid', $this->Engine->session->userdata('defaultLanguageId'));
			}
			
			$this->Engine->mysmarty->assign('currentLanguage', $this->Engine->session->userdata('languageid'));

			$languages = $this->Engine->LanguageModel->getAllLanguages($activeOnly = true);		
			
			$this->Engine->mysmarty->assign('languages', $languages);
			$this->Engine->mysmarty->assign('currentLanguageInfo', $langInfo);
			
			$this->detectDevice();
			
		}

		// --------------------------------------------------------------------

		function getDefaultUrl()
		{
			$languageInfo = $this->Engine->LanguageModel->getLanguage(3);

			//var_dump($languageInfo[0]->subdomain);

			$this->Engine->mysmarty->assign('defaulturl', 'http://'.$languageInfo[0]->subdomain.'/');
			//var_dump($tanslationTable);
		}
				
		// --------------------------------------------------------------------
		
		function detectDevice()
		{			
			$segments = $this->Engine->uri->segment_array();
			if(isset($segments[1]) && $segments[1] == 'forceversion') 
			{
				return true;
			}
			
			$checkAddress = $this->Engine->urllib->checkIfUrlIsMobile();			
			$checkDevice  = $this->Engine->detectdevice->isMobile();			
			
			$is_version_forced = $this->Engine->session->userdata('forceversion');
			
			if($checkAddress)
			{				
				if(!$checkDevice && !$is_version_forced) 
				{
					$this->Engine->urllib->redirectToRegular();
				}				
				define('MOBILE', true);				
			}
			else
			{				
				if($checkDevice && !$is_version_forced) 
				{					
					$this->Engine->urllib->redirectToMobile();
				}
				define('MOBILE', false);
			}
		}
	}
