<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Router_Hook
{
		/**
			* Loads routes from database.
			*
			* @access public
			* @params array : hostname, username, password, database, db_prefix
			* @return void
			*/
			
			
	function get_routes($params)
	{
		
		$siteConfig = get_config();
		$address = str_replace(array('http://', '/', 'index.php', 'www.'), '', $siteConfig['base_url']);				
		$address = preg_replace('/^m\./', '', $address);
		
		
						
		global $DB_ROUTES;		
		
		mysql_connect($params[0], $params[1], $params[2]);		
		mysql_select_db($params[3]);
		
		//GET LANG ID
		$sql = "SELECT id FROM {$params[4]}language where subdomain = '".$address."' limit 1";
		$query = mysql_query($sql);
		
		$langs = array();
		while ($langs = mysql_fetch_array($query, MYSQL_ASSOC)) 
		{
			$langid = $langs['id'];			
		}
		mysql_free_result($query);
		
		if(!$langid) 
		{
			die('Language domain error. No language found.');
		}
		
		$sql = "SELECT `".$params[4]."items`.`id`, `".$params[4]."items_info`.`slug`
				FROM (`".$params[4]."items`)
				LEFT JOIN `".$params[4]."items_info` ON `".$params[4]."items`.`id` = `".$params[4]."items_info`.`item_id`
				WHERE (`type` = 16 OR `type` = 2 OR `type` = 22)
				AND `".$params[4]."items_info`.`language_id` = '".$langid."'
				ORDER BY `position` asc
				LIMIT 50";	
		
		
		//$sql = "SELECT * FROM {$params[4]}routes";
		$query = mysql_query($sql);
		
		$routes = array();
		while ($route = mysql_fetch_array($query, MYSQL_ASSOC)) 
		{
			$routes[$route['id']] = $route['slug'];
		}		
		//exit();
		mysql_free_result($query);
		mysql_close();
		$DB_ROUTES = $routes;		
	}	
}
/* End of file Router_Hook.php */
/* Location: ./system/application/hooks/Router_Hook.php */
