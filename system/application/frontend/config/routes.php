<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
| 	www.your-site.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['scaffolding_trigger'] = 'scaffolding';
|
| This route lets you set a "secret" word that will trigger the
| scaffolding feature for added security. Note: Scaffolding must be
| enabled in the controller in which you intend to use it.   The reserved
| routes must come before any wildcard or regular expression routes.
|
*/
	global $DB_ROUTES;


	if(is_array($DB_ROUTES))
	{
		//var_dump($DB_ROUTES);

		$contactRoute     = $DB_ROUTES[219];
		$companyRoute     = $DB_ROUTES[636];
		$resourcesRoute   = $DB_ROUTES[634];
		$referencesRoute  = $DB_ROUTES[635];
		$leadersRoute     = $DB_ROUTES[607];
		$newsRoute        = $DB_ROUTES[207];

		$mobileTouchRoute = $DB_ROUTES[694];
		$servicesRoute    = $DB_ROUTES[216];
		$conceptsRoute    = $DB_ROUTES[2];
		$featuresRoute    = $DB_ROUTES[215];

		$platformConnectorRoute = $DB_ROUTES[880];
		$pcServicesRoute        = $DB_ROUTES[882];
		$pcFeaturesRoute        = $DB_ROUTES[881];
		$pcB2BRoute             = $DB_ROUTES[956];

		$forPartnersRoute = $DB_ROUTES[943];
	}
	else
	{
		die('No routes from database, error');
	}

	$route['default_controller'] = "main";
	$route['scaffolding_trigger'] = "";


	$route['forceversion'] 									= "forceversion";
	$route['index.html'] 									= "main/index";
	$route['ajaxsendcontact'] 								= "main/ajaxaction";
	$route['movie-clips.html'] 							= "main/index/movieclips";
	$route['gartner.html'] 									= "gartner/index";
	$route[$contactRoute.'.html'] 						= "contact";

	$route[$newsRoute.'.html'] 							= "news";
	$route[$newsRoute.'/(:any).html'] 					= "news/show";
	$route['news/extend/(:any)'] 							= "news/extend/$1";
	$route['news/ajaxgetitems/(:num)/(:num)']       = "news/ajaxgetitems/$1/$2";
	$route[$newsRoute.'/(:any)'] 							= "news";

	$route['references/changeindustry']               = "company/changeindustry";
	$route[$companyRoute.'.html']                     = "company";
	$route[$companyRoute.'.html/new']                 = "company";
	$route[$companyRoute.'/'.$resourcesRoute.'.html'] = "company/resources";

	$route[$companyRoute.'/'.$referencesRoute.'/(:any).html'] = "company/showclient";
	$route[$companyRoute.'/'.$referencesRoute.'/(:any)']      = "company/references/$1";
	$route[$companyRoute.'/'.$referencesRoute.'.html']        = "company/references";

	$route[$companyRoute.'/'.$leadersRoute.'.html']           = "company/teamleaders";

	$route['features/changeindustry']                           = "mobiletouch/changeindustry";
	$route[$mobileTouchRoute.'.html']                           = "mobiletouch";
	$route[$mobileTouchRoute.'/'.$servicesRoute.'.html']        = "mobiletouch/services";
	$route[$mobileTouchRoute.'/'.$conceptsRoute.'.html']        = "mobiletouch/concepts";
	$route[$mobileTouchRoute.'/'.$featuresRoute.'/(:any).html'] = "mobiletouch/showfeature";
	$route[$mobileTouchRoute.'/'.$featuresRoute.'/(:any)']      = "mobiletouch/features/$1";
	$route[$mobileTouchRoute.'/'.$featuresRoute.'.html']        = "mobiletouch/features";


	$route['pcfeatures/changeindustry']                                 = "pconnector/changeindustry";
	$route[$platformConnectorRoute.'.html']                             = "pconnector";
	$route[$platformConnectorRoute.'/'.$pcServicesRoute.'.html']        = "pconnector/services";
	$route[$platformConnectorRoute.'/'.$pcFeaturesRoute.'/(:any).html'] = "pconnector/showfeature";
	$route[$platformConnectorRoute.'/'.$pcFeaturesRoute.'/(:any)']      = "pconnector/features/$1";
	$route[$platformConnectorRoute.'/'.$pcFeaturesRoute.'.html']        = "pconnector/features";
	$route[$platformConnectorRoute.'/'.$pcB2BRoute.'.html']             = "pconnector/b2b";


	$route[$forPartnersRoute.'.html']   = "forpartners";

	// Manual Routes --------------------------------------------------------------------

	$route['referencje'] = "manualroute";
	$route['Gartner']    = "manualroute";
	$route['contact']    = "manualroute";


	// Statisctic Routes --------------------------------------------------------------------

	$route['utm_source=(:any)'] = "main/index";
	$route['url=(:any)']        = "main/index";
	$route['gclid=(:any)']      = "main/index";
	/*$route['(:any).html'] 							= "entry";
	$route['(:any)'] 									= "category";
	$route['(:any)/(:any)'] 						= "category";*/


/* End of file routes.php */
/* Location: ./system/application/config/routes.php */
