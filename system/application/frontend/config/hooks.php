<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
*/   
	
	require_once(BASEPATH."application/frontend/config/database.php");
		

	$hook['pre_system'] = array(
                                'class'    => 'Router_Hook',
                                'function' => 'get_routes',
                                'filename' => 'Router_Hook.php',
                                'filepath' => 'hooks',
                                'params'   => array(
                                    $db['default']['hostname'],
                                    $db['default']['username'],
                                    $db['default']['password'],
                                    $db['default']['database'],
                                    $db['default']['dbprefix'],
                                    )
						         );



	$hook['post_controller_constructor'][] = array(
                                                'class'    => 'Hooks',
                                                'function' => 'getLanguage',
                                                'filename' => 'hooks.php',
                                                'filepath' => 'hooks'                                                
                                             );  
   
                                             
                                             
   /*$hook['post_controller_constructor'][] = array(
                                                'class'    => 'Hooks',
                                                'function' => 'setPaginationSettings',
                                                'filename' => 'hooks.php',
                                                'filepath' => 'hooks'                                                
		                                             );*/

/* End of file hooks.php */
/* Location: ./system/application/config/hooks.php */