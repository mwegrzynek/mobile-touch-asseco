<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		
		$this->load->model('../../models/ItemsModel');
		$this->load->model('../../models/OptionsModel');	
		$this->load->model('../../models/MenuModel');	
		$this->load->model('../../models/VideosModel');	
		$this->load->model('../../models/PicturesModel');	
		$this->load->model('../../models/CategoryModel');
		$this->load->model('../../models/UserModel');		
		
		$this->load->library('homepagelib');
		$this->load->library('commonareas');
		$this->load->library('categorylib');			
		$this->load->library('newslib');		
				
		$this->jump          = 4;
		$this->defaultExtend = 4;		
		$this->itemType      = 7;		
	}
	
	function index()
	{		
		//$this->output->enable_profiler(TRUE);
		
		$this->langSettings();
		$this->siteRoutes = $this->commonareas->getSiteRoutes();
		$this-> _getCommonParts();
		
		if(!$extend = $this->session->userdata('newsExtend')) 
		{
			$extend = $this->defaultExtend;			
		}
		$this->newslib->getNews($extend, 1);		
		
		$btnValue = true;
		$allItems = $this->newslib->getCountNews();
		if($extend >= $allItems-1)
		{
			$btnValue = false;
		}
		
		$latest = $this->newslib->getLatest();
		$this->mysmarty->assign('latest', $latest[0]);
		
		$this->mysmarty->assign('allItems', $allItems);		
		$this->mysmarty->assign('extendJump', $this->jump);		
		$this->mysmarty->assign('showExtendBtn', $btnValue);
		$this->mysmarty->assign('extendOffset', $extend + $this->jump);
		
		//META
		$this->commonareas->getMetaData(16, 207, $this->siteRoutes[207]->title, $this->languageid);	
		
		if(MOBILE) 
		{		
			$this->mysmarty->assign('mobile_cached', false);
			$this->mysmarty->assign('mobile_pageid', 'news');
			$this->mysmarty->assign('mobile_backlink', 'index.html');
			$this->mysmarty->assign('mobile_submenu', false);
			$this->mysmarty->display('mobile/news/index.tpl');
		}
		else
		{			
			$this->mysmarty->display('news/index.tpl');
		}
	}
	
	// --------------------------------------------------------------------

	function show()
	{	
		$this->langSettings();
		$this->siteRoutes = $this->commonareas->getSiteRoutes();
		$this-> _getCommonParts($additionalUrls = array($this->siteRoutes[207]->slug.'.html'));
		
		$this->segments = $this->uri->segment_array();
		$slug = end($this->segments);
		
		
		$slug = str_replace(array('.html', '_html'), '', $slug);		
		$itemInfo = $this->ItemsModel->getItemBySlug($slug, $this->languageid, $this->itemType);
		
		//check if item exists
		if(isset($itemInfo[0]) && $itemInfo[0]->is_active)
		{			
			$itemid = $itemInfo[0]->id;			
						
			//META
			$this->commonareas->getMetaData($this->itemType, $itemid, $itemInfo[0]->title, $this->languageid);
			
			//PICTURES
			$this->commonareas->getPictures($itemid, 'pictures/news/');
		
			//VIDEO
			$this->commonareas->getVideos($itemid);			
			
			$latest = $this->newslib->getLatest(10);
			$this->mysmarty->assign('latest', $latest);
			
			$this->mysmarty->assign('itemInfo', $itemInfo[0]);
			
			if(MOBILE) 
			{			
				$filter['customFilter'][0] = array(
												'field' => 'is_active',
												'value' => 1
											);
				$this->commonareas->getPrevNext($this->itemType, $itemid, $filter);
				$this->mysmarty->assign('mobile_cached', false);
				$this->mysmarty->assign('mobile_pageid', 'news-'.$itemid);
				$this->mysmarty->assign('mobile_backlink', $this->siteRoutes[207]->slug.'.html');
				$this->mysmarty->assign('mobile_submenu', false);
				$this->mysmarty->display('mobile/news/show.tpl');
			}
			else
			{			
				$this->mysmarty->display('news/show.tpl');
			}
		}
		else
		{
			//$this->router->show_404();
			exit();
		}	
	}
	
	// --------------------------------------------------------------------	
	
	function _getCommonParts($additionalUrls = array())
	{
		$mainMenu         = $this->commonareas->getMainMenu($additionalUrls);
		$footerMenu       = $this->commonareas->getFooterMenu();
		$topMenu          = $this->commonareas->getTopMenu();		
		$siteTexts        = $this->commonareas->getSiteTexts();				
		$mainSites        = $this->commonareas->getMainSites();	
	}
	
	// --------------------------------------------------------------------
	
	private function langSettings()
	{
		$this->languageid = $this->session->userdata('languageid');
		$this->defaultLanguageid = $this->session->userdata('defaultLanguageId');
		$this->ItemsModel->setDefaultLanguage($this->languageid);
		$this->MenuModel->setDefaultLanguage($this->languageid);
		$this->CategoryModel->setDefaultLanguage($this->languageid);
	}
	
	// --------------------------------------------------------------------
	
	function extend($count = false)
	{		
		$this->langSettings();
		$siteRoutes = $this->commonareas->getSiteRoutes();
		
		if(!$count) 
		{
			$extend = $this->defaultExtend;
		}
		else
		{
			$extend = $count;
		}
		
		$this->session->set_userdata('newsExtend', $extend);
		redirect($siteRoutes[207]->slug.'.html');
	}
	
	// --------------------------------------------------------------------
	// AJAX ACTIONS
	// --------------------------------------------------------------------
	
	function ajaxgetitems($offset, $count)
	{
		$this->langSettings();
		$siteTexts = $this->commonareas->getSiteTexts();
		$mainSites = $this->commonareas->getMainSites();
		$siteRoutes = $this->commonareas->getSiteRoutes();	
		
		$offset = $offset+1;
		$this->langSettings();
		
		if(MOBILE) 
		{
			$template = "mobile/news/list.tpl";
		}
		else
		{
			$template = "news/list.tpl";		
		}
		
		$result = $this->newslib->getNews($count, $offset, $template);
		$extend = $count + $offset;
		$this->session->set_userdata('newsExtend', $extend);
		echo $result;		
	}
	
}
