<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mobiletouch extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->model('../../models/ItemsModel');
		$this->load->model('../../models/OptionsModel');
		$this->load->model('../../models/MenuModel');
		$this->load->model('../../models/VideosModel');
		$this->load->model('../../models/PicturesModel');
		$this->load->model('../../models/CategoryModel');
		$this->load->model('../../models/UserModel');

		$this->load->library('commonareas');
		$this->load->library('categorylib');
		$this->load->library('mobiletouchlib');
		$this->load->library('clientslib');
		$this->load->library('homepagelib');

		$this->itemType          = 2;
		$this->mainItemId        = 694;
		$this->conceptsItemId    = 2;
		$this->featuresItemId    = 215;
		$this->featuresPagesType = 3;
		$this->servicesItemId    = 216;
	}

	function index()
	{
		$this->langSettings();
		$this->siteRoutes = $this->commonareas->getSiteRoutes();
		$this->_getCommonParts();

		$itemInfo = $this->mainSites[$this->mainItemId];

		//BOXES
		$boxes = $this->mobiletouchlib->getBoxes();

		//TABS
		$tabBoxes = $this->mobiletouchlib->getTabsBoxes();
		$this->mysmarty->assign('boxes', $boxes);
		$this->mysmarty->assign('tabBoxes', $tabBoxes);

		//TESTIMONIALS
		$testimonials = $this->clientslib->getRandomTestimonials();
		$this->mysmarty->assign('testimonials', $testimonials);

		//META
		$this->commonareas->getMetaData($this->itemType, $this->mainItemId, $this->siteRoutes[$this->mainItemId]->title, $this->languageid);

		//PICTURES
		$this->commonareas->getPictures($this->mainItemId, 'pictures/mtpages/');

		//VISUALS
		if(isset($itemInfo->file_data_1) && $itemInfo->file_data_1 != '')
		{
			$itemInfo->file_data_1_info = @getimagesize($itemInfo->file_data_1);
		}

		if(isset($itemInfo->file_data_2) && $itemInfo->file_data_2 != '')
		{
			$itemInfo->file_data_2_info = @getimagesize($itemInfo->file_data_2);
		}

		$this->mysmarty->assign('itemInfo', $itemInfo);

		//VIDEO
		$videos = $this->commonareas->getVideos($this->mainItemId, $noSendToTemplate = true);

//var_dump($videos);

		if(is_array($videos))
		{
			$videosInfo = array();
			foreach($videos as $key => $value)
			{
				if(isset($value->title) && $value->title  != '')
				{
				 	$videosInfo[] = $value;
				}
			}
			$this->mysmarty->assign('videosInfo', $videosInfo);
		}

		$this->homepagelib->getWhyMobileTouch(true);

		if(MOBILE)
		{
			$this->mysmarty->assign('mobile_cached', false);
			$this->mysmarty->assign('mobile_pageid', 'mobiletouch');
			$this->mysmarty->assign('mobile_backlink', 'index.html');
			$this->mysmarty->assign('mobile_submenu', 'mobiletouch');
			$this->mysmarty->display('mobile/mobiletouch/index.tpl');
		}
		else
		{
			$this->mysmarty->display('mobiletouch/index.tpl');
		}

	}

	// --------------------------------------------------------------------

	function services()
	{
		$this->langSettings();
		$this->siteRoutes = $this->commonareas->getSiteRoutes();
		$this->_getCommonParts($additionalUrls = array($this->siteRoutes[$this->mainItemId]->slug.'.html'));

		$itemInfo = $this->mainSites[$this->servicesItemId];
		$this->mysmarty->assign('itemInfo', $itemInfo);

		$services = $this->mobiletouchlib->getServicePages();
		$this->mysmarty->assign('services', $services);

		//META
		$this->commonareas->getMetaData($this->itemType, $this->servicesItemId, $this->siteRoutes[$this->servicesItemId]->title, $this->languageid);

		//PICTURES
		//$this->commonareas->getPictures($this->servicesItemId, 'userfiles/services/');

		//VIDEO
		//$this->commonareas->getVideos($this->servicesItemId);

		if(MOBILE)
		{
			$this->mysmarty->assign('mobile_cached', false);
			$this->mysmarty->assign('mobile_pageid', 'services');
			$this->mysmarty->assign('mobile_backlink', $this->siteRoutes[$this->mainItemId]->slug.'.html');
			$this->mysmarty->assign('mobile_submenu', 'mobiletouch');
			$this->mysmarty->display('mobile/mobiletouch/services.tpl');
		}
		else
		{
			$this->mysmarty->display('mobiletouch/services.tpl');
		}
	}

	// --------------------------------------------------------------------

	function concepts()
	{
		$this->langSettings();
		$this->siteRoutes = $this->commonareas->getSiteRoutes();
		$this->_getCommonParts($additionalUrls = array($this->siteRoutes[$this->mainItemId]->slug.'.html'));

		$itemInfo = $this->mainSites[$this->conceptsItemId];
		$this->mysmarty->assign('itemInfo', $itemInfo);

		//CONCEPTS
		$concepts = $this->mobiletouchlib->getConceptsPages();
		$this->mysmarty->assign('concepts', $concepts);

		//TESTIMONIALS
		$testimonials = $this->clientslib->getRandomTestimonials();
		$this->mysmarty->assign('testimonials', $testimonials);

		//META
		$this->commonareas->getMetaData($this->itemType, $this->conceptsItemId, $this->siteRoutes[$this->conceptsItemId]->title, $this->languageid);

		//PICTURES
		//$this->commonareas->getPictures($this->leadersItemId, 'pictures/news/');

		//VIDEO
		//$this->commonareas->getVideos($this->leadersItemId);

		if(MOBILE)
		{
			$this->mysmarty->assign('mobile_cached', false);
			$this->mysmarty->assign('mobile_pageid', 'concepts');
			$this->mysmarty->assign('mobile_backlink', $this->siteRoutes[$this->mainItemId]->slug.'.html');
			$this->mysmarty->assign('mobile_submenu', 'mobiletouch');
			$this->mysmarty->display('mobile/mobiletouch/concepts.tpl');
		}
		else
		{
			$this->mysmarty->display('mobiletouch/concepts.tpl');
		}
	}

	// --------------------------------------------------------------------

	function features($categorySlug = false)
	{
		//$this->output->enable_profiler(TRUE);
		$this->langSettings();
		$this->siteRoutes = $this->commonareas->getSiteRoutes();
		$this->_getCommonParts($additionalUrls = array($this->siteRoutes[$this->mainItemId]->slug.'.html'));

		$this->categorylib->getCategories();

		$itemInfo = $this->mainSites[$this->featuresItemId];
		$this->mysmarty->assign('itemInfo', $itemInfo);

		$this->mysmarty->assign('categorySlug', $categorySlug);

		$filter = array();
		$currentCategory = false;
		if($categorySlug)
		{
			$currentCategory  = $this->categorylib->getCategoryBySlug($categorySlug, 1);
			if(!$currentCategory)
			{
				redirect($this->siteRoutes[$this->mainItemId]->slug.'/'.$this->siteRoutes[215]->slug.'.html');
			}
		}
		else
		{
			$currentCategory  = $this->categorylib->getDefaultCategory();
		}
		$this->mysmarty->assign('currentCategory', $currentCategory);

		$categoryId = $currentCategory->id;
		$categoryInfo = $this->getCategoryInfo($categoryId);


		$pages = $this->mobiletouchlib->getFeaturesPages(0, $categoryInfo['filter']);
		$this->mysmarty->assign('pages', $pages);

		//TESTIMONIALS
		$testimonials = $this->clientslib->getRandomTestimonialsByCategory(3, $categoryId);
		$this->mysmarty->assign('testimonials', $testimonials);

		//META
		$this->commonareas->getMetaData($this->itemType, $this->featuresItemId, $this->siteRoutes[$this->featuresItemId]->title, $this->languageid);

		if(MOBILE)
		{
			$this->mysmarty->assign('mobile_cached', false);
			$this->mysmarty->assign('mobile_pageid', 'features');
			$this->mysmarty->assign('mobile_backlink', $this->siteRoutes[$this->mainItemId]->slug.'.html');
			$this->mysmarty->assign('mobile_submenu', 'mobiletouch');
			$this->mysmarty->display('mobile/mobiletouch/features.tpl');
		}
		else
		{
			$this->mysmarty->display('mobiletouch/features.tpl');
		}
	}

	// --------------------------------------------------------------------

	function showfeature()
	{
		$this->langSettings();
		$this->siteRoutes = $this->commonareas->getSiteRoutes();
		$this->_getCommonParts($additionalUrls = array($this->siteRoutes[$this->mainItemId]->slug.'.html'));

		$itemInfo = $this->mainSites[$this->featuresItemId];
		$this->mysmarty->assign('itemInfo', $itemInfo);

		$this->categorylib->getCategories();

		$this->segments = $this->uri->segment_array();
		$slug = end($this->segments);
		$slug = str_replace(array('.html', '_html'), '', $slug);
		$itemInfo = $this->ItemsModel->getItemBySlug($slug, $this->languageid, $this->featuresPagesType);

		//check if item exists
		if(isset($itemInfo[0]) && $itemInfo[0]->is_active)
		{
			$page = $itemInfo[0];
			$itemid = $page->id;

			//META
			$this->commonareas->getMetaData($this->featuresPagesType, $itemid, $page->title, $this->languageid);

			//GET CATEGORY ID
			$categories = $this->categorylib->getCategoriesByItemId($itemid);
			$currentCategory = $this->categorylib->getCategoryById($categories[0]);

			$this->mysmarty->assign('currentCategory', $currentCategory);
			$categoryId = $currentCategory->id;
			$categoryInfo = $this->getCategoryInfo($categoryId);

			//MAIN IMAGE
			if(isset($page->file_data_1) && $page->file_data_1 != '')
			{
				$page->file_data_1_info = @getimagesize($page->file_data_1);
			}

			//TESTIMONIALS
			$testimonials = $this->clientslib->getRandomTestimonialsByCategory(3, $categoryId);
			$this->mysmarty->assign('testimonials', $testimonials);

			//GETSUBPAGES
			$subpages = $this->mobiletouchlib->getFeaturesPages($itemid, $categoryInfo['filter']);
			$this->mysmarty->assign('subpages', $subpages);

			//GET PAGES
			$pages = $this->mobiletouchlib->getFeaturesPages(0, $categoryInfo['filter']);
			$this->mysmarty->assign('pages', $pages);

			$this->mysmarty->assign('pageInfo', $page);
			$this->mysmarty->display('mobiletouch/showfeature.tpl');
		}
		else
		{
			$this->router->show_404();
		}
	}

	// --------------------------------------------------------------------

	function getCategoryInfo($categoryId)
	{
		$pagesIds  = $this->mobiletouchlib->getPagesIdsByCategoryId($categoryId);
		$filter['customFilter'][0]['in'] = array(
														'value' => $pagesIds,
														'field' => $this->db->dbprefix.'items.id'
													);

		return array(
			'filter' => $filter
		);
	}

	// --------------------------------------------------------------------

	function _getCommonParts($additionalUrls = array())
	{
		$mainMenu         = $this->commonareas->getMainMenu($additionalUrls);
		$footerMenu       = $this->commonareas->getFooterMenu();
		$topMenu          = $this->commonareas->getTopMenu();
		$siteTexts        = $this->commonareas->getSiteTexts();
		$this->mainSites  = $this->commonareas->getMainSites();
	}

	// --------------------------------------------------------------------

	private function langSettings()
	{
		$this->languageid = $this->session->userdata('languageid');
		$this->defaultLanguageid = $this->session->userdata('defaultLanguageId');
		$this->ItemsModel->setDefaultLanguage($this->languageid);
		$this->MenuModel->setDefaultLanguage($this->languageid);
		$this->CategoryModel->setDefaultLanguage($this->languageid);
	}

	// --------------------------------------------------------------------

	function changeindustry()
	{
		$this->langSettings();
		$this->siteRoutes = $this->commonareas->getSiteRoutes();

		$industry = $this->input->post('industry');
		if($industry)
		{
			redirect($this->siteRoutes[$this->mainItemId]->slug.'/'.$this->siteRoutes[215]->slug.'/'.$industry);
		}
		else
		{
			redirect($this->siteRoutes[$this->mainItemId]->slug.'.html');
		}
	}
}
