<?php

class forceversion extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();		
		$this->load->library('urllib');
	}
	
	// --------------------------------------------------------------------

	function index()
	{		
		$this->session->set_userdata('forceversion', true);
		$this->urllib->redirectToRegular();
	}	
	
}
