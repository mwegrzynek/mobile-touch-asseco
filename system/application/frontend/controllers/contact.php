<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* @property CI_Loader $load
* @property CI_Form_validation $form_validation
* @property CI_Input $input
* @property CI_Email $email
* @property CI_Session $session
*
*/

class Contact extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->model('../../models/ItemsModel');
		$this->load->model('../../models/OptionsModel');
		$this->load->model('../../models/MenuModel');
		$this->load->model('../../models/CategoryModel');
	}


	function index()
	{
		$this->languageid = $this->session->userdata('languageid');
		$this->defaultLanguageid = $this->session->userdata('defaultLanguageId');
		$this->ItemsModel->setDefaultLanguage($this->languageid);
		$this->MenuModel->setDefaultLanguage($this->languageid);
		$this->CategoryModel->setDefaultLanguage($this->languageid);

		$this->_getCommonParts();

		$this->contactlib->getCountries();

		if($this->session->flashdata('messageSent'))
		{
			$this->mysmarty->assign('success', 1);
			$this->mysmarty->assign('analyticsGoal1', $this->session->flashdata('messageSent'));
		}
		else
		{
			if($type = $this->input->post('form-type'))
			{
				$this->contactlib->addContactAction($type);
			}
		}

		//PARTNERS
		$partners = $this->contactlib->getAllPartnersByLanguage();
		$this->mysmarty->assign('partners', $partners);

		//META
		$this->commonareas->getMetaData(16, 219, $this->mainSites[219]->title, $this->languageid);

		if(MOBILE)
		{
			$this->mysmarty->assign('mobile_cached', false);
			$this->mysmarty->assign('mobile_pageid', 'contact');
			$this->mysmarty->assign('mobile_backlink', 'index.html');
			$this->mysmarty->assign('mobile_submenu', false);
			$this->mysmarty->display('mobile/contact.tpl');
		}
		else
		{
			$this->mysmarty->display('contact.tpl');
		}
	}

	function _getCommonParts($additionalUrls = array())
	{
		/*
		 *
		 * Common Areas
		 *
		 ****************/
		$this->load->library('commonareas');
		$this->load->library('contactlib');

		//GET ALL SITES (for menu) AND CUSTOM TEXTS
		$mainMenu   = $this->commonareas->getMainMenu();
		$footerMenu = $this->commonareas->getFooterMenu();
		$this->mainSites  = $this->commonareas->getMainSites();

		$this->siteTexts  = $this->commonareas->getSiteTexts();
		$this->siteRoutes = $this->commonareas->getSiteRoutes();
	}
}