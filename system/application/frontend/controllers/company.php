<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Company extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->model('../../models/ItemsModel');
		$this->load->model('../../models/OptionsModel');
		$this->load->model('../../models/MenuModel');
		$this->load->model('../../models/VideosModel');
		$this->load->model('../../models/PicturesModel');
		$this->load->model('../../models/CategoryModel');
		$this->load->model('../../models/UserModel');

		$this->load->library('commonareas');
		$this->load->library('categorylib');
		$this->load->library('companylib');
		$this->load->library('clientslib');
		$this->load->library('teamleaderslib');
		$this->load->library('homepagelib');
		$this->load->library('itemslib');

		$this->itemType               = 16;
		$this->mainItemId             = 636;
		$this->resourcesItemId        = 634;
		$this->referencesItemId       = 635;
		$this->leadersItemId          = 607;
		$this->clientsType            = 5;
		$this->companyPagesType       = 28;
		$this->raportTestimonialsType = 29;
	}

	function index()
	{
		$this->langSettings();
		$this->siteRoutes = $this->commonareas->getSiteRoutes();
		$this->_getCommonParts();

		$itemInfo = $this->mainSites[$this->mainItemId];

		$this->mysmarty->assign('itemInfo', $itemInfo);

		// boxes
		$boxes = $this->companylib->getBoxes();
		$this->mysmarty->assign('boxes', $boxes);

		// sections
		$pages = $this->companylib->getCompanyPages();
		$this->mysmarty->assign('pages', $pages);

		//testimonials
		$testimonials = $this->companylib->getCompanyTestimonials();
		$this->mysmarty->assign('testimonials', $testimonials);

		$isNew = $this->uri->segment(2, 0);
		$this->mysmarty->assign('isnew', $isNew);

		//META
		$this->commonareas->getMetaData($this->itemType, $this->mainItemId, $this->siteRoutes[$this->mainItemId]->title, $this->languageid);

		//PICTURES
		$this->commonareas->getPictures($this->mainItemId, 'pictures/news/');

		//VIDEO
		$this->commonareas->getVideos($this->mainItemId);

		$this->homepagelib->getWhyUs(true);

		if(MOBILE)
		{
			$this->mysmarty->assign('mobile_cached', false);
			$this->mysmarty->assign('mobile_pageid', 'company');
			$this->mysmarty->assign('mobile_backlink', 'index.html');
			$this->mysmarty->assign('mobile_submenu', 'company');
			$this->mysmarty->display('mobile/company/index.tpl');
		}
		else
		{
			$this->mysmarty->display('company/index.tpl');
		}
	}

	// --------------------------------------------------------------------

	function resources()
	{
		$this->langSettings();
		$this->siteRoutes = $this->commonareas->getSiteRoutes();
		$this->_getCommonParts($additionalUrls = array($this->siteRoutes[$this->mainItemId]->slug.'.html'));

		$itemInfo = $this->mainSites[$this->resourcesItemId];
		$this->mysmarty->assign('itemInfo', $itemInfo);

		$boxes = $this->companylib->getResourceBoxes();

		foreach($boxes as $key => $value)
		{
			if($value->main_image != '')
			{
				$filename = 'userfiles/mainpages/'.$value->main_image;
				if(is_file($filename))
				{
					$size = getimagesize($filename);
					$boxes[$key]->image_size = $size[3];
				}
			}
		}

		$this->mysmarty->assign('boxes', $boxes);

		//META
		$this->commonareas->getMetaData(16, $this->resourcesItemId, $this->siteRoutes[$this->resourcesItemId]->title, $this->languageid);

		//PICTURES
		$this->commonareas->getPictures($this->resourcesItemId, 'pictures/news/');

		//VIDEO
		$this->commonareas->getVideos($this->resourcesItemId);

		if(MOBILE)
		{
			$this->mysmarty->assign('mobile_cached', false);
			$this->mysmarty->assign('mobile_pageid', 'resources');
			$this->mysmarty->assign('mobile_backlink', $this->siteRoutes[$this->mainItemId]->slug.'.html');
			$this->mysmarty->assign('mobile_submenu', 'company');
			$this->mysmarty->display('mobile/company/resources.tpl');
		}
		else
		{
			$this->mysmarty->display('company/resources.tpl');
		}

	}

	// --------------------------------------------------------------------

	function references($categorySlug = false)
	{
		$this->langSettings();
		$this->siteRoutes = $this->commonareas->getSiteRoutes();
		$this->_getCommonParts($additionalUrls = array($this->siteRoutes[$this->mainItemId]->slug.'.html'));

		$this->categorylib->getCategories();

		$itemInfo = $this->mainSites[$this->referencesItemId];
		$this->mysmarty->assign('itemInfo', $itemInfo);

		$this->mysmarty->assign('categorySlug', $categorySlug);

		$filter = array();
		$currentCategory = false;
		if($categorySlug)
		{
			$currentCategory = $this->categorylib->getCategoryBySlug($categorySlug, 1);

			$currentCategory  = $this->categorylib->getCategoryBySlug($categorySlug, 1);
			if(!$currentCategory)
			{
				redirect($this->siteRoutes[$this->mainItemId]->slug.'/'.$this->siteRoutes[635]->slug.'.html');
			}


			$categoryId      = $currentCategory->id;
			$clientsIds      = $this->clientslib->getClientsIdsByCategoryId($categoryId);
			$filter['customFilter'][0]['in'] = array(
															'value' => $clientsIds,
															'field' => $this->db->dbprefix.'items.id'
														);

			/*$this->session->set_userdata('categoryInfo', array(
				'filter'     => $filter,
				'categoryId' => $categoryId
			));*/
		}
		else
		{
			//$this->session->unset_userdata('categoryInfo');
		}

		$regularClients  = $this->clientslib->getRegularClients($filter);
		$extendedClients = $this->clientslib->getExtendedClients($filter);

		$this->mysmarty->assign('regularClients', $regularClients);
		$this->mysmarty->assign('extendedClients', $extendedClients);
		$this->mysmarty->assign('currentCategory', $currentCategory);

		//META
		$this->commonareas->getMetaData($this->itemType, $this->referencesItemId, $this->siteRoutes[$this->referencesItemId]->title, $this->languageid);

		if(MOBILE)
		{
			$this->mysmarty->assign('mobile_cached', false);
			$this->mysmarty->assign('mobile_pageid', 'references');
			$this->mysmarty->assign('mobile_backlink', $this->siteRoutes[$this->mainItemId]->slug.'.html');
			$this->mysmarty->assign('mobile_submenu', 'company');
			$this->mysmarty->display('mobile/company/references.tpl');
		}
		else
		{
			$this->mysmarty->display('company/references.tpl');
		}
	}

	// --------------------------------------------------------------------

	function showclient()
	{
		$this->langSettings();
		$this->siteRoutes = $this->commonareas->getSiteRoutes();
		$this->_getCommonParts($additionalUrls = array($this->siteRoutes[$this->mainItemId]->slug.'.html'));

		$itemInfo = $this->mainSites[$this->referencesItemId];
		$this->mysmarty->assign('itemInfo', $itemInfo);

		$this->categorylib->getCategories();

		$this->segments = $this->uri->segment_array();
		$slug = end($this->segments);
		$slug = str_replace(array('.html', '_html'), '', $slug);
		$itemInfo = $this->ItemsModel->getItemBySlug($slug, $this->languageid, $this->clientsType);

		//check if item exists
		if(isset($itemInfo[0]) && $itemInfo[0]->is_active)
		{
			$client = $itemInfo[0];
			$itemid = $client->id;

			//META
			$this->commonareas->getMetaData($this->clientsType, $itemid, $client->title, $this->languageid);

			//GET CATEGORY ID
			/*$filter = array();
			$currentCategory = false;
			if($categoryInfo = $this->session->userdata('categoryInfo'))
			{
				$currentCategory = $this->categorylib->getCategoryById($categoryInfo['categoryId']);
				$this->mysmarty->assign('currentCategory', $currentCategory);
				$filter = $categoryInfo['filter'];
			}
			$extendedClients = $this->clientslib->getExtendedClients($filter);
			$this->mysmarty->assign('extendedClients', $extendedClients);*/

			//GET CATEGORY ID
			$categories = $this->categorylib->getCategoriesByItemId($itemid);
			if(!$currentCategory = $this->categorylib->getCategoryById($categories[0]))
			{
				$currentCategory  = $this->categorylib->getDefaultCategory();
			}

			$this->mysmarty->assign('currentCategory', $currentCategory);
			$categoryId = $currentCategory->id;
			$categoryInfo = $this->getCategoryInfo($categoryId);

			$extendedClients = $this->clientslib->getExtendedClients($categoryInfo['filter']);
			$this->mysmarty->assign('extendedClients', $extendedClients);

			if($client->main_image != '')
			{
				$filename = 'userfiles/clients/'.$client->main_image;
				if(is_file($filename))
				{
					$size = getimagesize($filename);
					$client->image_size = $size[3];
				}
			}

			//TESTIMONIALS
			//$testimonials = $this->clientslib->getTestimonialsByClientId($itemid);
			//$this->mysmarty->assign('testimonials', $testimonials);

			//REFERENCES
			$references = $this->clientslib->getReferencesByClientId($itemid);
			$this->mysmarty->assign('references', $references);

			//CASE STUDIES
			$caseStudies = $this->clientslib->getCaseStudiesByClientId($itemid);
			$this->mysmarty->assign('caseStudies', $caseStudies);

			//PICTURES
			//$this->commonareas->getPictures($itemid, 'pictures/news/');

			//VIDEO
			//$this->commonareas->getVideos($itemid);

			$this->mysmarty->assign('clientInfo', $client);
			$this->mysmarty->display('company/showclient.tpl');
		}
		else
		{
			$this->router->show_404();
			//exit();
		}
	}

	// --------------------------------------------------------------------

	function teamleaders()
	{
		$this->langSettings();
		$this->siteRoutes = $this->commonareas->getSiteRoutes();
		$this->_getCommonParts($additionalUrls = array($this->siteRoutes[$this->mainItemId]->slug.'.html'));

		$itemInfo = $this->mainSites[$this->leadersItemId];
		$this->mysmarty->assign('itemInfo', $itemInfo);

		//META
		$this->commonareas->getMetaData($this->itemType, $this->leadersItemId, $this->siteRoutes[$this->leadersItemId]->title, $this->languageid);

		//GET TEAM LEADERS
		$filter['customFilter'][0] = array(
												'field' => 'is_active',
												'value' => 1
											);
		$result = $this->teamleaderslib->getTeamLeaders($filter);
		$this->mysmarty->assign('teamleaders', $result);

		//VIDEO
		//$this->commonareas->getVideos($this->leadersItemId);

		if(MOBILE)
		{
			$this->mysmarty->assign('mobile_cached', false);
			$this->mysmarty->assign('mobile_pageid', 'teamleaders');
			$this->mysmarty->assign('mobile_backlink', $this->siteRoutes[$this->mainItemId]->slug.'.html');
			$this->mysmarty->assign('mobile_submenu', 'company');
			$this->mysmarty->display('mobile/company/teamleaders.tpl');
		}
		else
		{
			$this->mysmarty->display('company/teamleaders.tpl');
		}
	}

	// --------------------------------------------------------------------


	function _getCommonParts($additionalUrls = array())
	{
		$mainMenu         = $this->commonareas->getMainMenu($additionalUrls);
		$footerMenu       = $this->commonareas->getFooterMenu();
		$topMenu          = $this->commonareas->getTopMenu();
		$siteTexts        = $this->commonareas->getSiteTexts();
		$this->mainSites  = $this->commonareas->getMainSites();
	}

	// --------------------------------------------------------------------

	private function langSettings()
	{
		$this->languageid = $this->session->userdata('languageid');
		$this->defaultLanguageid = $this->session->userdata('defaultLanguageId');
		$this->ItemsModel->setDefaultLanguage($this->languageid);
		$this->MenuModel->setDefaultLanguage($this->languageid);
		$this->CategoryModel->setDefaultLanguage($this->languageid);
	}

	// --------------------------------------------------------------------

	function getCategoryInfo($categoryId)
	{
		$itemsIds  = $this->clientslib->getClientsIdsByCategoryId($categoryId);
		$filter['customFilter'][0]['in'] = array(
														'value' => $itemsIds,
														'field' => $this->db->dbprefix.'items.id'
													);

		return array(
			'filter' => $filter
		);
	}

	// --------------------------------------------------------------------

	function changeindustry()
	{
		$this->langSettings();
		$this->siteRoutes = $this->commonareas->getSiteRoutes();

		$industry = $this->input->post('industry');
		if($industry)
		{
			redirect($this->siteRoutes[$this->mainItemId]->slug.'/'.$this->siteRoutes[635]->slug.'/'.$industry);
		}
		else
		{
			redirect($this->siteRoutes[$this->mainItemId]->slug.'.html');
		}
	}
}
