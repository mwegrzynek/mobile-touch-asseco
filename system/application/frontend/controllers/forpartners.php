<?php

class forpartners extends CI_Controller
{

	function __construct()
	{
		parent::__construct();

		$this->load->model('../../models/ItemsModel');
		$this->load->model('../../models/OptionsModel');
		$this->load->model('../../models/MetaDataModel');
		$this->load->model('../../models/CategoryModel');
		$this->load->model('../../models/VideosModel');
		$this->load->model('../../models/MenuModel');

		$this->mainItemId = 943;
		$this->suplementItemId = 944;
	}

	// --------------------------------------------------------------------

	function index()
	{
		//$this->output->enable_profiler(TRUE);
		$this->languageid = $this->session->userdata('languageid');
		$this->ItemsModel->setDefaultLanguage($this->languageid);
		$this->MenuModel->setDefaultLanguage($this->languageid);
		$this->CategoryModel->setDefaultLanguage($this->languageid);

		/*
		 *
		 * Common Areas
		 *
		 ****************/

		$this->load->library('commonareas');
		$this->load->library('companylib');
		$this->load->library('itemslib');

		$mainMenu   = $this->commonareas->getMainMenu();
		$footerMenu = $this->commonareas->getFooterMenu();
		$mainSites  = $this->commonareas->getMainSites();

		$this->siteTexts  = $this->commonareas->getSiteTexts();
		$this->siteRoutes = $this->commonareas->getSiteRoutes();

		$this->commonareas->getMetaData(16, $this->mainItemId, 'Mobile Touch', $this->languageid);

		$this->mysmarty->assign('item', $mainSites[$this->mainItemId]);
		$this->mysmarty->assign('suplementItem', $mainSites[$this->suplementItemId]);

		$testimonials = $this->companylib->getPartnersTestimonials();
		$this->mysmarty->assign('testimonials', $testimonials);

		$this->mysmarty->display('pages/for-partners.tpl');

	}
}
