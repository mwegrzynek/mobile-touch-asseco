<?php
class Error extends CI_Controller {
 
	function error_404()
	{
		$CI =& get_instance();
		$CI->output->set_status_header('404');
		
		
		//var_dump('404');		
		
		$CI->load->model('../../models/ItemsModel');
		$CI->load->model('../../models/OptionsModel');		
		$CI->load->model('../../models/MetaDataModel');		
		$CI->load->model('../../models/CategoryModel');		
		$CI->load->model('../../models/VideosModel');		
		$CI->load->model('../../models/MenuModel');
		
		$CI->languageid = $CI->session->userdata('languageid');
		$CI->defaultLanguageid = $CI->session->userdata('defaultLanguageId');
		$CI->ItemsModel->setDefaultLanguage($CI->languageid);
		$CI->MenuModel->setDefaultLanguage($CI->languageid);		
		$CI->CategoryModel->setDefaultLanguage($CI->languageid);		
		
		$CI->load->library('commonareas');  
		
		//GET ALL SITES (for menu) AND CUSTOM TEXTS				
		$mainMenu   = $CI->commonareas->getMainMenu();
		$footerMenu = $CI->commonareas->getFooterMenu();		
		$mainSites  = $CI->commonareas->getMainSites();
		
		$CI->siteTexts  = $CI->commonareas->getSiteTexts();		
		$CI->siteRoutes = $CI->commonareas->getSiteRoutes();			
			
		$mainSites = $CI->commonareas->getMainSites();
		
		/*$pathInfo['pathCrumbs'][] = array('title' => $siteTexts[172]->content, 'slug' => false);
		$CI->mysmarty->assign('breadCrumbs', $pathInfo['pathCrumbs']);	*/
		
		//META
		$CI->commonareas->getMetaData(16, 213, $mainSites[213]->title, $CI->languageid);
		
		
		$CI->mysmarty->display('404.tpl');
	}
}