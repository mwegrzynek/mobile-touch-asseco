<?php

class Main extends CI_Controller
{

	function __construct()
	{
		parent::__construct();

		$this->load->model('../../models/ItemsModel');
		$this->load->model('../../models/OptionsModel');
		$this->load->model('../../models/MetaDataModel');
		$this->load->model('../../models/CategoryModel');
		$this->load->model('../../models/VideosModel');
		$this->load->model('../../models/MenuModel');
	}

	// --------------------------------------------------------------------

	function index($pagetype = false)
	{
		//$this->output->enable_profiler(TRUE);
		$this->languageid = $this->session->userdata('languageid');
		$this->ItemsModel->setDefaultLanguage($this->languageid);
		$this->MenuModel->setDefaultLanguage($this->languageid);
		$this->CategoryModel->setDefaultLanguage($this->languageid);

		/*
		 *
		 * Common Areas
		 *
		 ****************/
		$this->load->library('homepagelib');
		$this->load->library('commonareas');
		$this->load->library('newslib');
		$this->load->library('companylib');
		$this->load->library('clientslib');
		$this->load->library('contactlib');
		$this->load->library('teamleaderslib');

		$mainMenu   = $this->commonareas->getMainMenu();
		$footerMenu = $this->commonareas->getFooterMenu();
		$mainSites  = $this->commonareas->getMainSites();

		$this->siteTexts  = $this->commonareas->getSiteTexts();
		$this->siteRoutes = $this->commonareas->getSiteRoutes();

		$this->commonareas->getMetaData(16, 609, 'Mobile Touch', $this->languageid);

		/*
		 *
		 * Home Page specific
		 *
		 ****************/
		//$this->categorylib->getCategories();

		if($this->session->flashdata('messageSent'))
		{
			$this->mysmarty->assign('success', 1);
		}
		else
		{
			$this->contactlib->addContactAction($type = 'short');
		}

		$this->homepagelib->getMainSlider();
		$this->homepagelib->getVideoSlider();
		$this->homepagelib->getWhyUs();
		$this->homepagelib->getWhyMobileTouch();

		$this->mysmarty->assign('home', true);

		if(MOBILE)
		{
			if($pagetype === 'movieclips')
			{
				$this->mysmarty->assign('home', false);
				$this->mysmarty->assign('mobile_cached', false);
				$this->mysmarty->assign('mobile_pageid', $type);
				$this->mysmarty->assign('mobile_backlink', 'index.html');
				$this->mysmarty->assign('mobile_submenu', false);
				$this->mysmarty->display('mobile/movieclips.tpl');
			}
			else
			{
				$segments = $this->uri->segment_array();
				if(isset($segments[1]) && $segments[1] == 'index.html')
				{

				}
				else
				{
					redirect('index.html');
				}

				$this->mysmarty->assign('mobile_cached', true);
				$this->mysmarty->assign('mobile_pageid', 'homepage');
				$this->mysmarty->display('mobile/index.tpl');
			}
		}
		else
		{
			$this->homepagelib->getLogoSlider();
			$latestNews = $this->newslib->getLatest(3);
			$this->mysmarty->assign('latestNews', $latestNews);

			$testimonials = $this->clientslib->getHomePageTestimonials();
			$this->mysmarty->assign('testimonials', $testimonials);

			//team leaders
			$filter['customFilter'][0] = array(
												'field' => 'is_active',
												'value' => 1
											);
			$teamleaders = $this->teamleaderslib->getRandomTeamLeaders(3, $filter);
			$this->mysmarty->assign('teamleaders', $teamleaders);

			//company boxes
			$boxes = $this->companylib->getBoxes();
			$this->mysmarty->assign('boxes', $boxes);

			$this->mysmarty->display('index.tpl');
		}
	}

	// --------------------------------------------------------------------

	function ajaxaction()
	{

		$this->languageid = $this->session->userdata('languageid');
		$this->ItemsModel->setDefaultLanguage($this->languageid);

		$this->load->library('contactlib');
		$this->load->library('commonareas');

		$this->siteTexts  = $this->commonareas->getSiteTexts();
		$this->siteRoutes = $this->commonareas->getSiteRoutes();

		if(isset($_POST['client_name']))
		{
			$data['email']       = $this->input->post('email');
			$data['client_name'] = $this->input->post('client_name');
			$data['company']     = $this->input->post('company');
			$data['typeof']      = $this->input->post('typeof');

			$this->contactlib->sendData($data);
			$output   = array(
				'header' => $this->siteTexts[658]->content,
				'text'   => $this->siteTexts[659]->content
			);
			echo json_encode($output);
		}
		else
		{
			echo 0;
		}
	}
}
