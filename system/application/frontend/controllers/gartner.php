<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gartner extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->model('../../models/ItemsModel');
		$this->load->model('../../models/OptionsModel');
		$this->load->model('../../models/MenuModel');
		$this->load->model('../../models/VideosModel');
		$this->load->model('../../models/PicturesModel');
		$this->load->model('../../models/CategoryModel');
		$this->load->model('../../models/UserModel');

		$this->load->library('commonareas');
		$this->load->library('categorylib');
		$this->load->library('companylib');
		$this->load->library('clientslib');
		$this->load->library('teamleaderslib');
		$this->load->library('homepagelib');
		$this->load->library('itemslib');

		$this->itemType               = 16;
		$this->mainItemId             = 636;
		$this->resourcesItemId        = 634;
		$this->referencesItemId       = 635;
		$this->leadersItemId          = 607;
		$this->clientsType            = 5;
		$this->companyPagesType       = 28;
		$this->raportTestimonialsType = 29;
	}

	function index()
	{
		$this->langSettings();
		$this->siteRoutes = $this->commonareas->getSiteRoutes();
		$this->_getCommonParts();

		$itemInfo = $this->mainSites[$this->mainItemId];

		$this->mysmarty->assign('itemInfo', $itemInfo);

		// boxes
		$boxes = $this->companylib->getBoxes();
		$this->mysmarty->assign('boxes', $boxes);

		// sections
		$pages = $this->companylib->getCompanyPages();
		$this->mysmarty->assign('pages', $pages);

		//testimonials
		$testimonials = $this->companylib->getCompanyTestimonials();
		$this->mysmarty->assign('testimonials', $testimonials);

		$isNew = $this->uri->segment(2, 0);
		$this->mysmarty->assign('isnew', $isNew);

		//META
		$this->commonareas->getMetaData($this->itemType, $this->mainItemId, $this->siteRoutes[$this->mainItemId]->title, $this->languageid);

		//PICTURES
		$this->commonareas->getPictures($this->mainItemId, 'pictures/news/');

		//VIDEO
		$this->commonareas->getVideos($this->mainItemId);

		$this->homepagelib->getWhyUs(true);

		if(MOBILE)
		{
			$this->mysmarty->assign('mobile_cached', false);
			$this->mysmarty->assign('mobile_pageid', 'company');
			$this->mysmarty->assign('mobile_backlink', 'index.html');
			$this->mysmarty->assign('mobile_submenu', 'company');
			$this->mysmarty->display('mobile/company/index.tpl');
		}
		else
		{
			$this->mysmarty->display('gartner/index.tpl');
		}
	}

	// --------------------------------------------------------------------

	function _getCommonParts($additionalUrls = array())
	{
		$mainMenu         = $this->commonareas->getMainMenu($additionalUrls);
		$footerMenu       = $this->commonareas->getFooterMenu();
		$topMenu          = $this->commonareas->getTopMenu();
		$siteTexts        = $this->commonareas->getSiteTexts();
		$this->mainSites  = $this->commonareas->getMainSites();
	}

	// --------------------------------------------------------------------

	private function langSettings()
	{
		$this->languageid = $this->session->userdata('languageid');
		$this->defaultLanguageid = $this->session->userdata('defaultLanguageId');
		$this->ItemsModel->setDefaultLanguage($this->languageid);
		$this->MenuModel->setDefaultLanguage($this->languageid);
		$this->CategoryModel->setDefaultLanguage($this->languageid);
	}

	// --------------------------------------------------------------------

	function getCategoryInfo($categoryId)
	{
		$itemsIds  = $this->clientslib->getClientsIdsByCategoryId($categoryId);
		$filter['customFilter'][0]['in'] = array(
														'value' => $itemsIds,
														'field' => $this->db->dbprefix.'items.id'
													);

		return array(
			'filter' => $filter
		);
	}

	// --------------------------------------------------------------------

	function changeindustry()
	{
		$this->langSettings();
		$this->siteRoutes = $this->commonareas->getSiteRoutes();

		$industry = $this->input->post('industry');
		if($industry)
		{
			redirect($this->siteRoutes[$this->mainItemId]->slug.'/'.$this->siteRoutes[635]->slug.'/'.$industry);
		}
		else
		{
			redirect($this->siteRoutes[$this->mainItemId]->slug.'.html');
		}
	}
}
