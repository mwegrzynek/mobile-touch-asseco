<?php

class manualroute extends CI_Controller
{

	function __construct()
	{
		parent::__construct();

		$this->load->model('../../models/ItemsModel');
		$this->load->model('../../models/OptionsModel');
		$this->load->model('../../models/MenuModel');
		$this->load->model('../../models/VideosModel');
		$this->load->model('../../models/PicturesModel');
		$this->load->model('../../models/CategoryModel');
		$this->load->model('../../models/UserModel');

		$this->load->library('commonareas');
	}

	// --------------------------------------------------------------------

	function index()
	{
		$this->langSettings();
		$uriArray = $this->uri->segment_array();

		$this->siteRoutes = $this->commonareas->getSiteRoutes();

		switch ($uriArray[1]) {
			case 'referencje':
				redirect('o-firmie/references.html', 'location', 301);
				break;

			case 'Gartner':
				redirect($this->siteRoutes[636]->slug.'.html', 'location', 301);
				break;

			case 'contact':
				redirect($this->siteRoutes[219]->slug.'.html', 'location', 301);
				break;

			default:
				// to home page
				redirect();
				break;
		}
	}

	private function langSettings()
	{
		$this->languageid = $this->session->userdata('languageid');
		$this->defaultLanguageid = $this->session->userdata('defaultLanguageId');
		$this->ItemsModel->setDefaultLanguage($this->languageid);
		$this->MenuModel->setDefaultLanguage($this->languageid);
		$this->CategoryModel->setDefaultLanguage($this->languageid);
	}

}
