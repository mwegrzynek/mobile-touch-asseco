<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pconnector extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->model('../../models/ItemsModel');
		$this->load->model('../../models/OptionsModel');
		$this->load->model('../../models/MenuModel');
		$this->load->model('../../models/VideosModel');
		$this->load->model('../../models/PicturesModel');
		$this->load->model('../../models/CategoryModel');
		$this->load->model('../../models/UserModel');

		$this->load->library('commonareas');
		$this->load->library('categorylib');
		$this->load->library('pconnectorlib');
		$this->load->library('clientslib');

		$this->itemType          = 22;
		$this->mainItemId        = 880;
		//$this->conceptsItemId  = 2;
		$this->featuresItemId    = 881;
		$this->featuresPagesType = 23;
		$this->servicesItemId    = 882;
		$this->b2bItemId         = 956;
	}

	// --------------------------------------------------------------------

	function index()
	{
		$this->langSettings();
		$this->siteRoutes = $this->commonareas->getSiteRoutes();
		$this->_getCommonParts();

		$itemInfo = $this->mainSites[$this->mainItemId];

		//BOXES
		$boxes = $this->pconnectorlib->getBoxes(array(888, 889));


		//INFO
		$info = $this->pconnectorlib->getBoxes(array(952, 953, 954, 955));

		//TABS
		$tabBoxes = $this->pconnectorlib->getTabsBoxes();
		$this->mysmarty->assign('boxes', $boxes);
		$this->mysmarty->assign('tabBoxes', $tabBoxes);
		$this->mysmarty->assign('info', $info);

		//TESTIMONIALS
		$testimonials = $this->clientslib->getRandomTestimonials();
		$this->mysmarty->assign('testimonials', $testimonials);

		//META
		$this->commonareas->getMetaData($this->itemType, $this->mainItemId, $this->siteRoutes[$this->mainItemId]->title, $this->languageid);

		//PICTURES
		$this->commonareas->getPictures($this->mainItemId, 'pictures/pconnectorpages/');

		//VISUALS
		if(isset($itemInfo->file_data_1) && $itemInfo->file_data_1 != '')
		{
			$itemInfo->file_data_1_info = @getimagesize($itemInfo->file_data_1);
		}

		$this->mysmarty->assign('itemInfo', $itemInfo);

		//VIDEO
		$videos = $this->commonareas->getVideos($this->mainItemId, $noSendToTemplate = true);

//var_dump($videos);

		if(is_array($videos))
		{
			$videosInfo = array();
			foreach($videos as $key => $value)
			{
				if(isset($value->title) && $value->title  != '')
				{
				 	$videosInfo[] = $value;
				}
			}
			$this->mysmarty->assign('videosInfo', $videosInfo);
		}


		if(MOBILE)
		{
			$this->mysmarty->assign('mobile_cached', false);
			$this->mysmarty->assign('mobile_pageid', 'pconnector');
			$this->mysmarty->assign('mobile_backlink', 'index.html');
			$this->mysmarty->assign('mobile_submenu', 'pconnector');
			$this->mysmarty->display('mobile/pconnector/index.tpl');
		}
		else
		{
			$this->mysmarty->display('pconnector/index.tpl');
		}

	}

	// --------------------------------------------------------------------

	function services()
	{
		$this->langSettings();
		$this->siteRoutes = $this->commonareas->getSiteRoutes();
		$this->_getCommonParts($additionalUrls = array($this->siteRoutes[$this->mainItemId]->slug.'.html'));

		$itemInfo = $this->mainSites[$this->servicesItemId];
		$this->mysmarty->assign('itemInfo', $itemInfo);

		$services = $this->pconnectorlib->getServicePages();
		$this->mysmarty->assign('services', $services);

		//META
		$this->commonareas->getMetaData($this->itemType, $this->servicesItemId, $this->siteRoutes[$this->servicesItemId]->title, $this->languageid);

		//PICTURES
		//$this->commonareas->getPictures($this->servicesItemId, 'userfiles/services/');

		//VIDEO
		//$this->commonareas->getVideos($this->servicesItemId);

		if(MOBILE)
		{
			$this->mysmarty->assign('mobile_cached', false);
			$this->mysmarty->assign('mobile_pageid', 'services');
			$this->mysmarty->assign('mobile_backlink', $this->siteRoutes[$this->mainItemId]->slug.'.html');
			$this->mysmarty->assign('mobile_submenu', 'pconnector');
			$this->mysmarty->display('mobile/pconnector/services.tpl');
		}
		else
		{
			$this->mysmarty->display('pconnector/services.tpl');
		}
	}

	// --------------------------------------------------------------------

	/*function concepts()
	{
		$this->langSettings();
		$this->siteRoutes = $this->commonareas->getSiteRoutes();
		$this->_getCommonParts($additionalUrls = array($this->siteRoutes[$this->mainItemId]->slug.'.html'));

		$itemInfo = $this->mainSites[$this->conceptsItemId];
		$this->mysmarty->assign('itemInfo', $itemInfo);

		//CONCEPTS
		$concepts = $this->pconnectorlib->getConceptsPages();
		$this->mysmarty->assign('concepts', $concepts);

		//TESTIMONIALS
		$testimonials = $this->clientslib->getRandomTestimonials();
		$this->mysmarty->assign('testimonials', $testimonials);

		//META
		$this->commonareas->getMetaData($this->itemType, $this->conceptsItemId, $this->siteRoutes[$this->conceptsItemId]->title, $this->languageid);

		//PICTURES
		//$this->commonareas->getPictures($this->leadersItemId, 'pictures/news/');

		//VIDEO
		//$this->commonareas->getVideos($this->leadersItemId);

		if(MOBILE)
		{
			$this->mysmarty->assign('mobile_cached', false);
			$this->mysmarty->assign('mobile_pageid', 'concepts');
			$this->mysmarty->assign('mobile_backlink', $this->siteRoutes[$this->mainItemId]->slug.'.html');
			$this->mysmarty->assign('mobile_submenu', 'pconnector');
			$this->mysmarty->display('mobile/pconnector/concepts.tpl');
		}
		else
		{
			$this->mysmarty->display('pconnector/concepts.tpl');
		}
	}*/

	// --------------------------------------------------------------------

	function features($categorySlug = false)
	{
		//$this->output->enable_profiler(TRUE);
		$this->langSettings();
		$this->siteRoutes = $this->commonareas->getSiteRoutes();
		$this->_getCommonParts($additionalUrls = array($this->siteRoutes[$this->mainItemId]->slug.'.html'));

		$this->categorylib->getCategories();

		$itemInfo = $this->mainSites[$this->featuresItemId];
		$this->mysmarty->assign('itemInfo', $itemInfo);

		$this->mysmarty->assign('categorySlug', $categorySlug);

		$filter = array();
		$currentCategory = false;
		if($categorySlug)
		{
			$currentCategory  = $this->categorylib->getCategoryBySlug($categorySlug, 1);
			if(!$currentCategory)
			{
				redirect($this->siteRoutes[$this->mainItemId]->slug.'/'.$this->siteRoutes[215]->slug.'.html');
			}
		}
		else
		{
			$currentCategory  = $this->categorylib->getDefaultCategory();
		}
		$this->mysmarty->assign('currentCategory', $currentCategory);

		$categoryId = $currentCategory->id;
		$categoryInfo = $this->getCategoryInfo($categoryId);


		$pages = $this->pconnectorlib->getFeaturesPages(0, $categoryInfo['filter']);
		$this->mysmarty->assign('pages', $pages);

		//TESTIMONIALS
		$testimonials = $this->clientslib->getRandomTestimonialsByCategory(3, $categoryId);
		$this->mysmarty->assign('testimonials', $testimonials);

		//META
		$this->commonareas->getMetaData($this->itemType, $this->featuresItemId, $this->siteRoutes[$this->featuresItemId]->title, $this->languageid);

		if(MOBILE)
		{
			$this->mysmarty->assign('mobile_cached', false);
			$this->mysmarty->assign('mobile_pageid', 'features');
			$this->mysmarty->assign('mobile_backlink', $this->siteRoutes[$this->mainItemId]->slug.'.html');
			$this->mysmarty->assign('mobile_submenu', 'pconnector');
			$this->mysmarty->display('mobile/pconnector/features.tpl');
		}
		else
		{
			$this->mysmarty->display('pconnector/features.tpl');
		}
	}

	// --------------------------------------------------------------------

	function showfeature()
	{
		$this->langSettings();
		$this->siteRoutes = $this->commonareas->getSiteRoutes();
		$this->_getCommonParts($additionalUrls = array($this->siteRoutes[$this->mainItemId]->slug.'.html'));

		$itemInfo = $this->mainSites[$this->featuresItemId];
		$this->mysmarty->assign('itemInfo', $itemInfo);

		$this->categorylib->getCategories();

		$this->segments = $this->uri->segment_array();
		$slug = end($this->segments);
		$slug = str_replace(array('.html', '_html'), '', $slug);
		$itemInfo = $this->ItemsModel->getItemBySlug($slug, $this->languageid, $this->featuresPagesType);

		//check if item exists
		if(isset($itemInfo[0]) && $itemInfo[0]->is_active)
		{
			$page = $itemInfo[0];
			$itemid = $page->id;

			//META
			$this->commonareas->getMetaData($this->featuresPagesType, $itemid, $page->title, $this->languageid);

			//GET CATEGORY ID
			$categories = $this->categorylib->getCategoriesByItemId($itemid);
			$currentCategory = $this->categorylib->getCategoryById($categories[0]);

			$this->mysmarty->assign('currentCategory', $currentCategory);
			$categoryId = $currentCategory->id;
			$categoryInfo = $this->getCategoryInfo($categoryId);

			//MAIN IMAGE
			if(isset($page->file_data_1) && $page->file_data_1 != '')
			{
				$page->file_data_1_info = @getimagesize($page->file_data_1);
			}

			//TESTIMONIALS
			$testimonials = $this->clientslib->getRandomTestimonialsByCategory(3, $categoryId);
			$this->mysmarty->assign('testimonials', $testimonials);

			//GETSUBPAGES
			$subpages = $this->pconnectorlib->getFeaturesPages($itemid, $categoryInfo['filter']);
			$this->mysmarty->assign('subpages', $subpages);

			//GET PAGES
			$pages = $this->pconnectorlib->getFeaturesPages(0, $categoryInfo['filter']);
			$this->mysmarty->assign('pages', $pages);

			$this->mysmarty->assign('pageInfo', $page);
			$this->mysmarty->display('pconnector/showfeature.tpl');
		}
		else
		{
			$this->router->show_404();
		}
	}

	// --------------------------------------------------------------------

	function b2b()
	{
		$this->langSettings();
		$this->siteRoutes = $this->commonareas->getSiteRoutes();
		$this->_getCommonParts();

		$itemInfo = $this->mainSites[$this->b2bItemId];

		//BOXES
		$boxes = $this->pconnectorlib->getBoxes(array(957, 958));


		//INFO
		$info = $this->pconnectorlib->getBoxes(array(959, 960, 961, 962));

		//TABS
		$tabBoxes = $this->pconnectorlib->getTabsBoxes();
		$this->mysmarty->assign('boxes', $boxes);
		$this->mysmarty->assign('tabBoxes', $tabBoxes);
		$this->mysmarty->assign('info', $info);

		//TESTIMONIALS
		$testimonials = $this->clientslib->getRandomTestimonials();
		$this->mysmarty->assign('testimonials', $testimonials);

		//META
		$this->commonareas->getMetaData($this->itemType, $this->b2bItemId, $this->siteRoutes[$this->b2bItemId]->title, $this->languageid);

		//PICTURES
		$this->commonareas->getPictures($this->b2bItemId, 'pictures/pconnectorpages/');

		//VISUALS
		if(isset($itemInfo->file_data_1) && $itemInfo->file_data_1 != '')
		{
			$itemInfo->file_data_1_info = @getimagesize($itemInfo->file_data_1);
		}

		$this->mysmarty->assign('itemInfo', $itemInfo);

		//VIDEO
		$videos = $this->commonareas->getVideos($this->b2bItemId, $noSendToTemplate = true);



		if(is_array($videos))
		{
			$videosInfo = array();
			foreach($videos as $key => $value)
			{
				if(isset($value->title) && $value->title  != '')
				{
				 	$videosInfo[] = $value;
				}
			}
			$this->mysmarty->assign('videosInfo', $videosInfo);
		}


		if(MOBILE)
		{
			$this->mysmarty->assign('mobile_cached', false);
			$this->mysmarty->assign('mobile_pageid', 'pconnector');
			$this->mysmarty->assign('mobile_backlink', 'index.html');
			$this->mysmarty->assign('mobile_submenu', 'pconnector');
			$this->mysmarty->display('mobile/pconnector/index.tpl');
		}
		else
		{
			$this->mysmarty->display('pconnector/b2b.tpl');
		}

	}

	// --------------------------------------------------------------------

	function getCategoryInfo($categoryId)
	{
		$pagesIds  = $this->pconnectorlib->getPagesIdsByCategoryId($categoryId);
		$filter['customFilter'][0]['in'] = array(
														'value' => $pagesIds,
														'field' => $this->db->dbprefix.'items.id'
													);

		return array(
			'filter' => $filter
		);
	}

	// --------------------------------------------------------------------

	function _getCommonParts($additionalUrls = array())
	{
		$mainMenu         = $this->commonareas->getMainMenu($additionalUrls);
		$footerMenu       = $this->commonareas->getFooterMenu();
		$topMenu          = $this->commonareas->getTopMenu();
		$siteTexts        = $this->commonareas->getSiteTexts();
		$this->mainSites  = $this->commonareas->getMainSites();
	}

	// --------------------------------------------------------------------

	private function langSettings()
	{
		$this->languageid = $this->session->userdata('languageid');
		$this->defaultLanguageid = $this->session->userdata('defaultLanguageId');
		$this->ItemsModel->setDefaultLanguage($this->languageid);
		$this->MenuModel->setDefaultLanguage($this->languageid);
		$this->CategoryModel->setDefaultLanguage($this->languageid);
	}

	// --------------------------------------------------------------------

	function changeindustry()
	{
		$this->langSettings();
		$this->siteRoutes = $this->commonareas->getSiteRoutes();

		$industry = $this->input->post('industry');
		if($industry)
		{
			redirect($this->siteRoutes[$this->mainItemId]->slug.'/'.$this->siteRoutes[215]->slug.'/'.$industry);
		}
		else
		{
			redirect($this->siteRoutes[$this->mainItemId]->slug.'.html');
		}
	}
}
