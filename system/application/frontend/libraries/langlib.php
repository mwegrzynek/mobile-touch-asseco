<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class langlib {


	function getAllLanguages()
	{
		$CI = & get_instance();
		$CI->load->model('../../models/LanguageModel');
		$languages = $CI->LanguageModel->getAllLanguages();

		return $languages;
	}

	// --------------------------------------------------------------------

	function getAllLanguagesButCurrent()
	{
		$CI = & get_instance();
		$output = array();
		$languages = $this->getAllLanguages();
		$currentLangId = $CI->session->userdata('languageid');

		foreach($languages as $key => $value)
		{
			if($value->id != $currentLangId)
			{
				$output[] = $value;
			}
		}
		return $output;
	}

	// --------------------------------------------------------------------


	function getCurrentLanguage()
	{
		$CI = & get_instance();
		$id = $CI->session->userdata('languageid');
		$language = $CI->LanguageModel->getLanguage($id);

		return $language;
	}


}
