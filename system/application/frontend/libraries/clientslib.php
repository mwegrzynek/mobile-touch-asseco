<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class clientslib 
{	
	function __construct()
	{
		$this->clientsType      = 5;	
		$this->testimonialsType = 6;	
		$this->caseStudiesType  = 8;
		$this->referencesType   = 18;
	}
	
	/**
	 * Get testimonials marked as "show on homepage"
	 *
	 * @access	public
	 * @param	integer $userId
	 * @return	array $comments
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2011, Maciej Węgrzynek
	 */
	function getHomePageTestimonials()
	{
		$filter['customFilter'][0] = array(					
											'value' => 1,
											'field' => 'li_num_data_1'
										);			
										
		$result = $this->getTestimonials($filter);
		
		if(isset($result[0])) 
		{
			foreach($result as $key => $value) 
			{
				$result[$key]->clientInfo = $this->getClientById($value->parent_id);
			}
		}
		
		return $result;		
	}
	
	// --------------------------------------------------------------------
	
	function getTestimonialsByClientId($id)
	{	
		$filter['customFilter'][0] = array(					
											'value' => $id,
											'field' => 'parent_id'
										);			
		
		$result = $this->getTestimonials($filter);
		
		return $result;
	}
	
	// --------------------------------------------------------------------
	
	function getTestimonials($filter = array())
	{	
		$CI = & get_instance();
		
		$filterCount = 0;
		if(isset($filter['customFilter'])) 
		{
			$filterCount = count($filter['customFilter']);
		}
		
		$filter['customFilter'][$filterCount] = array(
													'field' => 'is_active',
													'value' => 1
												);
		
		$result = $CI->ItemsModel->getAllItems($this->testimonialsType, 100, 0, 'position', 'asc', $filter);	
		
		return $result;
	}
	
	// --------------------------------------------------------------------
	
	function getRandomTestimonials($itemsCount = 3)
	{
		$output = array();
		$result = $this->getTestimonials();	
		$randKeys = array_rand($result, $itemsCount);
		
		if(!is_array($randKeys))
		{
			$randKeys = array($randKeys);
		}		
		
		foreach($randKeys as $key => $value) 
		{
			$output[$key] = $result[$value];
			$output[$key]->clientInfo = $this->getClientById($output[$key]->parent_id);
		}
		
		return $output;
	}
	
	// --------------------------------------------------------------------
	
	function getRandomTestimonialsByCategory($itemsCount, $category_id)
	{
		$ids = $this->getClientsIdsByCategoryId($category_id);
		
		$filter['customFilter'][0]['in'] = array(					
														'value' => $ids,
														'field' => 'parent_id'
													);	
		
		$result = $this->getTestimonials($filter);
		
		$resCount = count($result);
		
		if($resCount) 
		{
			foreach($result as $key => $value) 
			{
				$result[$key]->clientInfo = $this->getClientById($value->parent_id);
			}
		}
		else
		{
			return false;			
		}
		
		
		if($resCount >= $itemsCount) 
		{
			shuffle($result);
			return array_slice($result, 0, 3);
		}
		else
		{
			//var_dump($result);
			//var_dump($resCount);			
			$supplement = $this->getRandomTestimonials($itemsCount - $resCount);				
			$result = array_merge($result, $supplement);
			shuffle($result);			
			return $result;
		}
	}
	
	// --------------------------------------------------------------------
	
	function getClients($filter = array())
	{	
		$CI = & get_instance();
		
		$filterCount = 0;
		if(isset($filter['customFilter'])) 
		{
			$filterCount = count($filter['customFilter']);
		}
		
		$filter['customFilter'][$filterCount] = array(
													'field' => 'is_active',
													'value' => 1
												);

												
		$result = $CI->ItemsModel->getAllItems($this->clientsType, 1000, 0, 'position', 'asc', $filter);		
		return $result;
	}
	
	// --------------------------------------------------------------------
	
	function getRegularClients($filter = array())
	{
		$filterCount = 0;
		if(isset($filter['customFilter'])) 
		{
			$filterCount = count($filter['customFilter']);
		}
		
		$filter['customFilter'][$filterCount] = array(
													'field' => 'li_num_data_1',
													'value' => 0
												);																								
												
		$clients = $this->getClients($filter);		
		return $this->splitItemsList($clients);
	}
	
	// --------------------------------------------------------------------
	
	function getExtendedClients($filter = array())
	{
		$filterCount = 0;
		if(isset($filter['customFilter'])) 
		{
			$filterCount = count($filter['customFilter']);
		}
		
		$filter['customFilter'][$filterCount] = array(
													'field' => 'li_num_data_1',
													'value' => 1
												);
		return $this->getClients($filter);
		
	}
	
	// --------------------------------------------------------------------
	
	function getClientById($id)
	{				
		$filter['customFilter'][0] = array(					
											'value' => $id,
											'field' => 'item_id'
										);			
										
		$result = $this->getClients($filter);
		if(isset($result[0])) 
		{			
			return $result[0];
		}

		return false;
	}
	
	// --------------------------------------------------------------------
	
	function getClientsIdsByCategoryId($category_id)
	{
		$CI = & get_instance();
		
		$ids = $CI->ItemsModel->getItemsIdsByCategoryId($category_id);
		
		return $ids;
	}
	
	// --------------------------------------------------------------------

	function splitItemsList($array)
	{
		if(is_array($array)) 
		{
			$pageItems = 38;
			$mainArray = array_chunk($array, $pageItems);
			foreach($mainArray as $key => $value) 
			{
				$mainArray[$key] = array_chunk($value, round($pageItems/2));
			}
			
			return $mainArray;
		}
		
		return false;
	}
	

	// --------------------------------------------------------------------
	
	function getReferences($filter = array())
	{	
		$CI = & get_instance();
		$result = $CI->ItemsModel->getAllItems($this->referencesType, 100, 0, 'position', 'asc', $filter);	
		
		return $result;
	}
	
	// --------------------------------------------------------------------
	
	function getReferencesByClientId($id)
	{	
		$CI = & get_instance();
		$filter['customFilter'][0] = array(					
											'value' => $id,
											'field' => 'parent_id'
										);			
		
		$result = $this->getReferences($filter);
		
		if(isset($result[0])) 
		{
			foreach($result as $key => $value) 
			{
				$result[$key]->videos   = $CI->commonareas->getVideos($value->id, $noSendToTemplate = true);
				$result[$key]->pictures = $CI->commonareas->getPictures($value->id, '', $noSendToTemplate = true);
			}
		}
		
		return $result;
	}
	
	// --------------------------------------------------------------------
	
	function getCaseStudies($filter = array())
	{	
		$CI = & get_instance();
		$result = $CI->ItemsModel->getAllItems($this->caseStudiesType, 100, 0, 'position', 'asc', $filter);	
		
		return $result;
	}
	
	// --------------------------------------------------------------------
	
	function getCaseStudiesByClientId($id)
	{	
		$CI = & get_instance();
		$filter['customFilter'][0] = array(					
											'value' => $id,
											'field' => 'parent_id'
										);			
		
		$result = $this->getCaseStudies($filter);
		
		if(isset($result[0])) 
		{
			foreach($result as $key => $value) 
			{
				$result[$key]->videos   = $CI->commonareas->getVideos($value->id, $noSendToTemplate = true);
				$result[$key]->pictures = $CI->commonareas->getPictures($value->id, '', $noSendToTemplate = true);
				
				if($value->file_data_1 != '') 
				{
					$result[$key]->file_name = pathinfo($value->file_data_1, PATHINFO_FILENAME);
				}
			}
		}
		
		return $result;
	}
}
