<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Itemslib {

	function __construct()
	{
		$this->itemType = 0;
		$this->itemSort = 'position asc';
	}

	/**
	 * Get items
	 *
	 * @access	public
	 * @return	array items
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2012, Maciej Węgrzynek
	 */
	function getItems($count, $offset, $filter = array())
	{
		$CI = & get_instance();

		$filterCount = 0;
		$filterCount = count($filter);

		$filter['customFilter'][$filterCount+1] = array(
													'field' => 'is_active',
													'value' => 1
												);
		$result = $CI->ItemsModel->getAllItems($this->itemType, $count, $offset, $this->itemSort, '', $filter);

		$output = array();
		foreach($result as $key => $value)
		{
			$output[$value->id] = $value;
		}

		return $output;
	}

	// --------------------------------------------------------------------

	/**
	 * Get count items.
	 *
	 * @access	public
	 * @return	int articles count
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2012, Maciej Węgrzynek
	 */
	function getCountItems($filter = array())
	{
		$CI = & get_instance();

		$filterCount = 0;
		if(isset($filter['customFilter']))
		{
			$filterCount = count($filter['customFilter']);
		}


		$filter['customFilter'][$filterCount+1] = array(
												'field' => 'is_active',
												'value' => 1
											);

		return $CI->ItemsModel->getCountItems($this->itemType, $filter);
	}

	// --------------------------------------------------------------------

	public function setType($type)
	{
		$this->itemType = $type;
		return true;
	}

	// --------------------------------------------------------------------

	public function setSort($sort)
	{
		$this->itemSort = $sort;
		return true;
	}

	// --------------------------------------------------------------------

	public function getPictures($itemId, $picPath, $noSendToTemplate = false)
	{
		$CI = & get_instance();

		$picturesInfo = $CI->PicturesModel->getPictures(1000, 0, 'position asc', $itemId);
		if(isset($picturesInfo[0]))
		{
			$CI->MetaDataModel->setType(99);
			foreach($picturesInfo as $key => $res)
			{
				$meta = $CI->MetaDataModel->getMetaData($res->id, $CI->defaultLanguageid);
				//var_dump($meta);
				if(isset($meta[0]))
				{
					$picturesInfo[$key]->title = $meta[0]->title;
				}
			}

			if(!$noSendToTemplate)
			{
				$CI->mysmarty->assign('picturesInfo', $picturesInfo);
				$CI->mysmarty->assign('picturesPath', $picPath);
			}
		}
		return $picturesInfo;
	}

	// --------------------------------------------------------------------

	public function getVideos($itemId, $noSendToTemplate = false)
	{
		$CI = & get_instance();

		$videosInfo = $CI->VideosModel->getVideos(1000, 0, 'position asc', $itemId);
		if(isset($videosInfo[0]))
		{
			$CI->MetaDataModel->setType(99);
			foreach($videosInfo as $key => $res)
			{
				$meta = $CI->MetaDataModel->getMetaData($res->id, $CI->languageid);
				if(isset($meta[0]))
				{
					$videosInfo[$key]->title = $meta[0]->title;
				}
			}

			if(!$noSendToTemplate)
			{
				$CI->mysmarty->assign('videosInfo', $videosInfo);
			}
		}

		return $videosInfo;
	}

	// --------------------------------------------------------------------

	public function getImageInfo($img, $path)
 	{
 		$path = $path.$img;
 		if(is_file($path))
 		{
 			$output['path'] = $path;
 			$info = getimagesize($path);
			$output['width']   = $info[0];
			$output['height']  = $info[1];
			$output['imginfo'] = $info[3];
 			return $output;
 		}

 		return false;
 	}

 	// --------------------------------------------------------------------

 	function getPrevNext($itemType, $itemId, $filter = array())
	{
		$CI = & get_instance();

		$result = $CI->ItemsModel->getAllItems($itemType, 1000, 0, 'position', 'asc', $filter);

		if(isset($result[0]))
		{
			foreach($result as $key => $value)
			{
				if($value->id == $itemId)
				{
					$myKey = $key;
				}
			}

			$prevKey = $myKey - 1;
			if($prevKey > -1)
			{
				$CI->mysmarty->assign('prevInfo', $result[$prevKey]);
			}

			$nextKey = $myKey + 1;
			if(isset($result[$nextKey]))
			{
				$CI->mysmarty->assign('nextInfo', $result[$nextKey]);
			}
		}
	}

}