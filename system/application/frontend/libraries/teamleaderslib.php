<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class teamleaderslib 
{	
	function __construct()
	{
		$this->teamLeadersType = 17;	
		
	}
	
	function getTeamLeaders($filter = array())
	{	
		$CI = & get_instance();
		$result = $CI->ItemsModel->getAllItems($this->teamLeadersType, 100, 0, 'position', 'asc', $filter);	
		
		return $result;
	}
	
	// --------------------------------------------------------------------
	
	function getRandomTeamLeaders($itemsCount = 3, $filter = array())
	{
		$output = array();
		$result = $this->getTeamLeaders($filter);	
		$randKeys = array_rand($result, $itemsCount);
		
		if(!is_array($randKeys))
		{
			$randKeys = array($randKeys);
		}		
		
		foreach($randKeys as $key => $value) 
		{
			$output[$key] = $result[$value];		
		}
		
		return $output;
	}	
	
}
