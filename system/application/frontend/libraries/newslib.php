<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newslib {

	function __construct()
	{
		$this->itemType = 7;	
	}

	// --------------------------------------------------------------------
	
	/**
	 * Get news.
	 * Cache result
	 *
	 * @access	public
	 * @param   integer $newsCount
	 * @return	true
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2011, Maciej Węgrzynek
	 */
	function getNews($count, $offset, $template = "news/list.tpl")
	{
		$CI = & get_instance();
		
		$CI->mysmarty->setCaching(Smarty::CACHING_LIFETIME_SAVED);
		$CI->mysmarty->setCacheLifetime(3600);
		$CI->mysmarty->setCaching(Smarty::CACHING_OFF);		
		
		if(!$CI->mysmarty->isCached($template, 'items|'.$CI->languageid.'|News|'.$count.'|'.$offset))
		{			
			//set months											
			$CI->commonareas->setMonths();
			
			//get news
			$result = $this->getItems($count, $offset);
			
			$CI->mysmarty->assign('newsList', $result);
			$rendered = $CI->mysmarty->fetch($template, 'items|'.$CI->languageid.'|News|'.$count.'|'.$offset);
			$CI->mysmarty->assign('newsListRendered', $rendered);
		}
		else
		{
			$rendered = $CI->mysmarty->fetch($template, 'items|'.$CI->languageid.'|News|'.$count.'|'.$offset);
			$CI->mysmarty->assign('newsListRendered', $rendered);
		}	

		$CI->mysmarty->setCaching(Smarty::CACHING_OFF);
		return $rendered;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Get count news.
	 *
	 * @access	public
	 * @return	int news count
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2012, Maciej Węgrzynek
	 */
	function getCountNews()
	{
		$CI = & get_instance();
		$filter['customFilter'][0] = array(
												'field' => 'is_active',
												'value' => 1
											);		
		
		return $CI->ItemsModel->getCountItems($this->itemType, $filter);	
	}
	
	// --------------------------------------------------------------------
	
	function getLatest($count = 1)
	{
		return $this->getItems($count, 0);
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Get items
	 *
	 * @access	public
	 * @return	array news items
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2012, Maciej Węgrzynek
	 */
	function getItems($count, $offset, $filter = array())
	{
		$CI = & get_instance();
		
		$filterCount = 0;
		$filterCount = count($filter);
		
		$filter['customFilter'][$filterCount+1] = array(
													'field' => 'is_active',
													'value' => 1
												);		

		$result = $CI->ItemsModel->getAllItems($this->itemType, $count, $offset, 'add_date', 'desc', $filter);	
		
		return $result;
	}
	
	// --------------------------------------------------------------------	
	
}
