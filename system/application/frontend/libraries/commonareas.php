<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Commonareas {

	function getSites()
	{
		$CI = & get_instance();
		
		$result = $CI->ItemsModel->getAllItems(1, 1000, 0, 'title', 'asc');		
		
		foreach ($result as $key => $value) 
		{			
			$idsResult[$value->id] = $value;
		}
		
		if(isset($result[0])) 
		{
			$CI->mysmarty->assign('sites', $idsResult);		
			return $idsResult;
		}
		return false;
	}
	
	// --------------------------------------------------------------------
	
	function getMainSites()
	{
		$CI = & get_instance();
		
		$result = $CI->ItemsModel->getAllItems(16, 50, 0, 'title', 'asc');
		$result1 = $CI->ItemsModel->getAllItems(2, 50, 0, 'title', 'asc');
		$result2 = $CI->ItemsModel->getAllItems(22, 50, 0, 'title', 'asc');
		$result = array_merge($result, $result1, $result2);
		foreach ($result as $key => $value) 
		{			
			$idsResult[$value->id] = $value;
		}
		
		if(isset($result[0])) 
		{
			$CI->mysmarty->assign('mainSites', $idsResult);		
			return $idsResult;
		}
		return false;
	}
	
	// --------------------------------------------------------------------
	
	function getSiteTexts()
	{
		$CI = & get_instance();
		
		$result = $CI->ItemsModel->getAllItems(12, 1000, 0, 'title', 'asc');
		foreach ($result as $key => $value) 
		{			
			$idsResult[$value->id] = $value;
		}
		
		if(isset($result[0])) 
		{
			$CI->mysmarty->assign('siteTexts', $idsResult);		
			return $idsResult;
		}
	}
	
	// --------------------------------------------------------------------
	
	function getSiteRoutes()
	{
		$CI = & get_instance();
		
		$result = $CI->ItemsModel->getAllItems(16, 50, 0, 'title', 'asc');
		$result1 = $CI->ItemsModel->getAllItems(2, 50, 0, 'title', 'asc');
		$result2 = $CI->ItemsModel->getAllItems(22, 50, 0, 'title', 'asc');
		$result = array_merge($result, $result1, $result2);
		foreach ($result as $key => $value) 
		{			
			$idsResult[$value->id] = $value;
		}
				
		if(isset($result[0])) 
		{
			$this->siteRoutes = $idsResult;
			$CI->mysmarty->assign('siteRoutes', $idsResult);			
			return $idsResult;
		}
	}
	
	// --------------------------------------------------------------------
	
	function getNotificationTexts()
	{
		$CI = & get_instance();
		
		$result = $CI->ItemsModel->getAllItems(14, 1000, 0, 'title', 'asc');
		foreach ($result as $key => $value) 
		{			
			$idsResult[$value->id] = $value;
		}
		
		if(isset($result[0])) 
		{
			$CI->mysmarty->assign('notificationTexts', $idsResult);		
			return $idsResult;
		}
	}
	
	/**
	 * Get all active banners, send them to smarty and to array where keys are ids
	 *
	 * @access	public
	 * @return	array $idsResult
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2011, Maciej Węgrzynek
	 */
	function getBanners()
	{
		$CI = & get_instance();
		
		$filter['customFilter'][0] = array(
												'field' => 'is_active',
												'value' => 1
											);
		
		$result = $CI->ItemsModel->getAllItems(2, 50, 0, 'title', 'asc', $filter);
		foreach($result as $key => $value) 
		{			
			$idsResult[$value->id] = $value;
		}
		
		if(isset($result[0])) 
		{
			$CI->mysmarty->assign('siteBanners', $idsResult);		
			return $idsResult;
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Get all contact languages send them to smarty and to array where keys are ids or codes (en, pl, da) etc.
	 *
	 * @access	public
	 * @return	array $contactLanguages
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2011, Maciej Węgrzynek
	 */
	function getContactLanguages($keysAreCodes = false)
	{
		$CI = & get_instance();
		
		$result = $CI->ItemsModel->getAllItems(5, 50, 0, 'title', 'asc');
		foreach($result as $key => $value) 
		{			
			if($keysAreCodes) 
			{
				$idsResult[$value->li_text_data_1] = $value;
			}
			else
			{
				$idsResult[$value->id] = $value;
			}
		}
		
		if(isset($result[0])) 
		{
			$CI->mysmarty->assign('contactLanguages', $idsResult);		
			return $idsResult;
		}
	}
	
	// --------------------------------------------------------------------		
	
	function getMainMenu($url = array())
	{
		$CI = & get_instance();
		
		$filter['customFilter'][0] = array(					
														'value' => 1,
														'field' => 'is_active'
													);	
		
		$this->menuTree = $CI->MenuModel->getTree(1, 'position asc, name asc', $filter);		
		$CI->mysmarty->assign('mainMenu', $this->menuTree);
		
		$CI->load->helper('url');
		$currentUrl = trim(uri_string(), '/');
		$url[] = $currentUrl;
		
		
		$activeIds = $CI->MenuModel->getActiveElementsById(1, $url, $filter);		
		
		$CI->mysmarty->assign('activeIds', $activeIds);
		//var_dump($activeIds);		
		return $this->menuTree;
	}
	
	// --------------------------------------------------------------------
	
	function getOfferMenu()
	{
		$CI = & get_instance();
		
		$filter['customFilter'][0] = array(					
														'value' => 1,
														'field' => 'is_active'
													);	
		$filter['customFilter'][1] = array(					
														'value' => 7,
														'field' => 'category_id'
													);
		
		$offerTree = $CI->ItemsModel->getTree(1, 'position asc, title asc', '', $filter);		
		$CI->mysmarty->assign('offerMenu', $offerTree);		
		
		//var_dump($offerTree);
		
		return $offerTree;
	}
	
	// --------------------------------------------------------------------
	
	function getFooterMenu()
	{
		$CI = & get_instance();
		
		$filter['customFilter'][0] = array(					
														'value' => 1,
														'field' => 'is_active'
													);	
		
		$menuTree = $CI->MenuModel->getTree(2, 'position asc, name asc', $filter);	
		$CI->mysmarty->assign('footerMenu', $menuTree);
		//var_dump($menuTree);
		
		return $menuTree;
	}
	
	// --------------------------------------------------------------------
	
	function getTopMenu()
	{
		$CI = & get_instance();
		
		$filter['customFilter'][0] = array(					
														'value' => 1,
														'field' => 'is_active'
													);	
		
		$menuTree = $CI->MenuModel->getTree(3, 'position asc, name asc', $filter);	
		$CI->mysmarty->assign('topMenu', $menuTree);
		//var_dump($menuTree);
		
		return $menuTree;
	}
	
	// --------------------------------------------------------------------
	
	function getPictures($itemId, $picPath, $noSendToTemplate = false)
	{
		$CI = & get_instance();
		
		$picturesInfo = $CI->PicturesModel->getPictures(1000, 0, 'position asc', $itemId);
		if(isset($picturesInfo[0])) 
		{
			$CI->MetaDataModel->setType(99);
			foreach($picturesInfo as $key => $res)
			{
				$meta = $CI->MetaDataModel->getMetaData($res->id, $CI->defaultLanguageid);
				//var_dump($meta);
				if(isset($meta[0]))
				{
					$picturesInfo[$key]->title = $meta[0]->title;
				}
			}
			
			if(!$noSendToTemplate) 
			{
				$CI->mysmarty->assign('picturesInfo', $picturesInfo);
				$CI->mysmarty->assign('picturesPath', $picPath);
			}
		}
		return $picturesInfo;
	}
	
	// --------------------------------------------------------------------
	
	function getVideos($itemId, $noSendToTemplate = false)
	{
		$CI = & get_instance();
		
		$videosInfo = $CI->VideosModel->getVideos(1000, 0, 'position asc', $itemId);			
		if(isset($videosInfo[0])) 
		{
			$CI->MetaDataModel->setType(99);
			foreach($videosInfo as $key => $res)
			{
				$meta = $CI->MetaDataModel->getMetaData($res->id, $CI->languageid);				
				if(isset($meta[0]))
				{
					$videosInfo[$key]->title = $meta[0]->title;
				}
			}
			
			if(!$noSendToTemplate) 
			{				
				$CI->mysmarty->assign('videosInfo', $videosInfo);
			}
		}
		
	
		
		
		return $videosInfo;
	}
	
	// --------------------------------------------------------------------
	
	function getPrevNext($itemType, $itemId, $filter)
	{
		$CI = & get_instance();
		
		$result = $CI->ItemsModel->getAllItems($itemType, 1000, 0, 'position', 'asc', $filter);		
		
		if(isset($result[0])) 
		{
			foreach($result as $key => $value) 
			{
				if($value->id == $itemId) 
				{
					$myKey = $key;								
				}
			}
			
			$prevKey = $myKey - 1;			
			if($prevKey > -1) 
			{
				$CI->mysmarty->assign('prevInfo', $result[$prevKey]);
			}
			
			$nextKey = $myKey + 1;
			if(isset($result[$nextKey])) 
			{
				$CI->mysmarty->assign('nextInfo', $result[$nextKey]);
			}
		}
	}
	
	// --------------------------------------------------------------------
	
	function _getCategoryParentPath($categoryid)
	{
		if(isset($this->pathCache[$categoryid])) 
		{
			return $this->pathCache[$categoryid];
		}
		
		$CI = & get_instance();
		
		$parentsInfo = $CI->CategoryModel->getCategoryParentsInfo($categoryid);		
		$path = '';		
		
		if(isset($parentsInfo[0])) 
		{
			foreach($parentsInfo as $key => $value) 
			{
				if($value->is_active) 
				{
					$path .= $value->slug.'/';
				}
			}				
		}
		
		$path = $this->siteRoutes[82]->slug.'/'.$path;
		
		$this->pathCache[$categoryid] = $path;
		
		return $path;
	}
	
	// --------------------------------------------------------------------
	
	function getSubMenu()	
	{
		$CI = & get_instance();
		
		$activeIds = $CI->mysmarty->getTemplateVars('activeIds');
		$mainMenu = $CI->mysmarty->getTemplateVars('mainMenu');
		
		foreach($mainMenu as $key => $value) 
		{
			if(in_array($value->id, $activeIds) && isset($value->children)) 
			{
				return $value->children;
			}
		}
		return false;
	}
	
	// --------------------------------------------------------------------
	
	function getMetaData($itemTypeId, $itemId, $defaultTitle, $langid)
	{
		$CI = & get_instance();
		$CI->load->model('../../models/MetaDataModel');
		$CI->MetaDataModel->setType($itemTypeId);
		$metaInfo = $CI->MetaDataModel->getMetaToFront($itemId, $langid, $langid);			
				
		$meta['title'] = '';
		if(!empty($metaInfo))
		{
			$meta['title']			= $metaInfo[0]->title;
			$meta['keywords']		= $metaInfo[0]->keywords;
			$meta['description']	= $metaInfo[0]->description;
		}
		if($meta['title'] == '')
		{
			$meta['title'] = $defaultTitle;
		}
					
		$CI->mysmarty->assign('meta', $meta);
		
		return $meta;
	}
	
	// --------------------------------------------------------------------	
	
	function setMonths($full = 0)
	{
		$CI = & get_instance();	
		$months = array(
			1 => 'Styczeń',
			2 => 'Luty',
			3 => 'Marzec',
			4 => 'Kwiecień',
			5 => 'Maj',
			6 => 'Czerwiec',
			7 => 'Lipiec',
			8 => 'Sierpień',
			9 => 'Wrzesień',
			10 => 'Październik',
			11 => 'Listopad',
			12 => 'Grudzień'
		);
		
		if(!$full) 
		{
			foreach($months as $key => $value) 
			{
				$months[$key] = substr($value, 0, 3);
			}
			$template = 'months';
		}
		else
		{
			$template = 'monthsFull';
		}
		
		$CI->mysmarty->assign($template, $months);		
		return $months;
	}	
}