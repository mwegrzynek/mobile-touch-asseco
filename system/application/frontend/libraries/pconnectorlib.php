<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pconnectorlib {

	/**
	 * Get item by id
	 *
	 * @access	public
	 * @return	array articles items
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2012, Maciej Węgrzynek
	 */
	function getItemById($id)
	{
		$CI = & get_instance();

		$result = $CI->ItemsModel->getItemById($id, $CI->languageid);

		if(isset($result[0]))
		{
			return $result[0];
		}
		return false;
	}

	// --------------------------------------------------------------------

	function getBoxes($boxes = array(695, 696))
	{
		foreach($boxes as $key => $value)
		{
			$boxes[$key] = $this->getItemById($value);
		}
		return $boxes;
	}

	// --------------------------------------------------------------------

	function getTabsBoxes()
	{
		$boxes = array(697, 698, 699, 700);
		return $this->getBoxes($boxes);
	}

	// --------------------------------------------------------------------

	function getServicePages()
	{
		$CI = & get_instance();

		$result = $CI->ItemsModel->getAllItems(24, 1000, 0, 'position', 'asc');

		foreach ($result as $key => $value)
		{
			$idsResult[$value->id] = $value;
		}

		if(isset($result[0]))
		{
			return $idsResult;
		}
		return false;
	}

	// --------------------------------------------------------------------

	/*function getConceptsPages()
	{
		$CI = & get_instance();

		$result = $CI->ItemsModel->getAllItems(15, 1000, 0, 'position', 'asc');

		foreach ($result as $key => $value)
		{
			$idsResult[$value->id] = $value;
			if($value->file_data_1 != '')
			{
				$idsResult[$value->id]->file_data_1_info = @getimagesize($value->file_data_1);
			}
		}

		if(isset($result[0]))
		{
			return $idsResult;
		}
		return false;
	}*/

	// --------------------------------------------------------------------

	function getFeaturesPages($parentId = 0, $filter = array())
	{
		$CI = & get_instance();

		$filterCount = 0;
		if(isset($filter['customFilter']))
		{
			$filterCount = count($filter['customFilter']);
		}

		$filter['customFilter'][$filterCount] = array(
												'field' => 'parent_id',
												'value' => $parentId
											);
		$filter['customFilter'][] = array(
												'field' => 'is_active',
												'value' => 1
											);

		$result = $CI->ItemsModel->getAllItems(23, 1000, 0, 'position', 'asc', $filter);

		foreach ($result as $key => $value)
		{
			$idsResult[$value->id] = $value;
			if($value->file_data_1 != '')
			{
				$idsResult[$value->id]->file_data_1_info = @getimagesize($value->file_data_1);
			}
		}

		if(isset($result[0]))
		{
			return $idsResult;
		}
		return false;
	}

	// --------------------------------------------------------------------

	function getPagesIdsByCategoryId($category_id)
	{
		$CI = & get_instance();

		$ids = $CI->ItemsModel->getItemsIdsByCategoryId($category_id);

		return $ids;
	}

}
