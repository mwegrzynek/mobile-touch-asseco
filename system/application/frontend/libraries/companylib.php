<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Companylib {

	/**
	 * Get item by id
	 *
	 * @access	public
	 * @return	array articles items
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2012, Maciej Węgrzynek
	 */
	function getItemById($id)
	{
		$CI = & get_instance();

		$result = $CI->ItemsModel->getItemById($id, $CI->languageid);

		return $result[0];
	}

	// --------------------------------------------------------------------

	function getBoxes($boxes = false)
	{
		if(!$boxes)
		{
			$boxes = array(688, 689, 690);
		}

		foreach($boxes as $key => $value)
		{
			$boxes[$key] = $this->getItemById($value);
		}
		return $boxes;
	}

	// --------------------------------------------------------------------

	function getResourceBoxes()
	{
		$boxes = array(691, 692, 759, 760);
		return $this->getBoxes($boxes);
	}

	// --------------------------------------------------------------------

	function getCompanyPages()
	{
		$CI = & get_instance();

		$CI->itemslib->setType(28);
		$result = $CI->itemslib->getItems(100, 0);

		return $result;
	}

	// --------------------------------------------------------------------

	function getCompanyTestimonials()
	{
		$CI = & get_instance();

		$CI->itemslib->setType(29);
		$result = $CI->itemslib->getItems(100, 0);

		return $result;
	}

	// --------------------------------------------------------------------

	function getPartnersTestimonials()
	{
		$CI = & get_instance();

		$CI->itemslib->setType(30);
		$result = $CI->itemslib->getItems(100, 0);

		return $result;
	}

	// --------------------------------------------------------------------

}
