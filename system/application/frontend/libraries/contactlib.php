<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class contactlib {

	function __construct()
	{
		$this->formType = 'full';
		$this->countriesType = 9;
	}

	// --------------------------------------------------------------------

	/**
	 * Adds contact data
	 *
	 */
	function addContactData($data)
	{
		$CI = & get_instance();
		$CI->load->model('../../models/ContactsModel');

		unset($data['form-type']);
		unset($data['force-contact-redirect']);
		// unset($data['typeof']);
		unset($data['company_second_name']);

		$dataId = $CI->ContactsModel->addContact($data);
		return $dataId;
	}

	// --------------------------------------------------------------------

	function sentNotification($data)
	{
			//send notification to admins
		$CI = & get_instance();
		$CI->load->library('Notices');
		$CI->load->library('langlib');
		$noticeConfig['noticeType']      = 'newContactFormMessage';
		$noticeConfig['data']            = $data;
		$noticeConfig['permission']      = 'getsystemnotices-7';
		$noticeConfig['topic']           = 'New message has been sent by contact form';
		if(isset($data['typeof']) && $data['typeof'] == 'platformaconnector')
		{
			$noticeConfig['topic']  = 'New message has been sent by Platforma Connector contact form';
			// $noticeConfig['additionalMails'] = $this->getPartnersMails();
			$noticeConfig['additionalMails'] = array_merge(array('pawel.petrusewicz@assecobs.pl'), $this->getPartnersMails());
		}
		elseif(isset($data['typeof']) && $data['typeof'] == 'gartner')
		{
			$noticeConfig['topic']  = 'New message has been sent by Gartner contact form';
			$noticeConfig['additionalMails']      = array(
				'dorota.powroznik@assecobs.pl',
				'pawel.petrusewicz@assecobs.pl',
				'agata.kurto@assecobs.pl'
			);
			$noticeConfig['forceAdditionalMails'] = true;
		}
		else
		{
			// $noticeConfig['additionalMails'] = $this->getPartnersMails();
			$noticeConfig['additionalMails'] = array_merge(array('pawel.petrusewicz@assecobs.pl'), $this->getPartnersMails());
		}

// var_dump($noticeConfig);


		$currentLanguage = $CI->langlib->getCurrentLanguage();
		$noticeConfig['topic'] .= '('.$currentLanguage[0]->lang_code.')';

		$CI->notices->SendAdminNotice($noticeConfig);
		//echo $this->email->print_debugger();
		//exit;
		return true;
	}

	// --------------------------------------------------------------------

	function addContactAction($type = 'full')
	{
		$CI = & get_instance();

		$this->setFormType($type);

		$config = $this->getConfig();

		$CI->form_validation->set_rules($config);
		$CI->form_validation->set_message('required', $CI->siteTexts[666]->content);
		$CI->form_validation->set_message('valid_email', $CI->siteTexts[664]->content);

		$CI->load->helper('post_data');

		// antispam check
		if($CI->input->post('company_second_name') != '')
		{
			redirect($CI->siteRoutes[219]->slug.'.html');
		}

		if($CI->form_validation->run()) // Validation Passed
		{
			$data = getPostDataToArray();
			$this->sendData($data);

			$sendFormType = 2;
			if($this->formType == 'full')
			{
				$sendFormType = 1;
			}

			$CI->session->set_flashdata('messageSent', $sendFormType);
			// not put form again when page reload

			if($this->formType == 'full' || $CI->input->post('force-contact-redirect'))
			{
				redirect($CI->siteRoutes[219]->slug.'.html');
			}
			else
			{
				redirect();
			}
		}
		elseif(validation_errors() != '') // Validation Failed
		{
			//prepare for smarty as array
			$CI->mysmarty->assign('errorHeader', $CI->siteTexts[231]->content);

			$aErrors = explode("###", substr(trim(validation_errors()), 0, -3));
			if(isset($aErrors[0]) && $aErrors[0]!='')
			{
				$CI->mysmarty->assign('errors', array(
					$this->formType => $aErrors
				));
			}

			$data = getPostDataToArray();
			$CI->mysmarty->assign('backData', array(
				$this->formType => $data
			));
		}
	}

	// --------------------------------------------------------------------

	function sendData($data)
	{
		$this->addContactData($data);
		$this->sentNotification($data);
		return true;
	}

	// --------------------------------------------------------------------

	function getConfig()
	{
		$CI = & get_instance();

		if($this->formType == 'short')
		{
			$config = array(
				array(
						'field'   => 'client_name',
						'label'   => $CI->siteTexts[330]->content,
						'rules'   => 'trim|required|strip_tags'
					),
				array(
						'field'   => 'email',
						'label'   => $CI->siteTexts[648]->content,
						'rules'   => 'trim|required|valid_email'
					)
			);
		}
		else
		{
			$config = array(
					array(
							'field'   => 'client_name',
							'label'   => $CI->siteTexts[330]->content,
							'rules'   => 'trim|required|strip_tags'
						),
					array(
							'field'   => 'email',
							'label'   => $CI->siteTexts[648]->content,
							'rules'   => 'trim|required|valid_email'
						),
					array(
							'field'   => 'phone',
							'label'   => $CI->siteTexts[649]->content,
							'rules'   => 'trim|required|strip_tags'
						),
					array(
							'field'   => 'company',
							'label'   => $CI->siteTexts[650]->content,
							'rules'   => 'trim|required|strip_tags'
						),
					array(
							'field'   => 'message',
							'label'   => $CI->siteTexts[652]->content,
							'rules'   => 'trim|required|strip_tags'
						)
				);
		}
		return $config;
	}

	// --------------------------------------------------------------------

	function setFormType($type)
	{
		$this->formType = $type;
	}

	// --------------------------------------------------------------------

	function getCountries()
	{
		$CI = & get_instance();

		$result = $CI->ItemsModel->getAllItems($this->countriesType, 100, 0, 'title', 'asc');

		foreach ($result as $key => $value)
		{
			$idsResult[$value->id] = $value;
		}

		if(isset($result[0]))
		{
			$CI->mysmarty->assign('countries', $idsResult);
			return $idsResult;
		}
		return false;
	}

	// --------------------------------------------------------------------

	function getAllPartnersByLanguage($withDefault = true)
	{
		$CI = & get_instance();
		$filter['customFilter'][0] = array(
												'field' => 'language_id',
												'value' => $CI->languageid
											);

		$partners =  $this->getPartners($filter);

		$output = array();
		if(isset($partners[0]))
		{
			foreach($partners as $key => $value)
			{
				$output[$value->id] = $value;
			}

			if($withDefault)
			{
				if($default = $this->getDefaultPartners())
				{
					foreach($default as $key => $value)
					{
						$output[$key] = $value;
					}
				}
			}

			return $output;
		}
		return false;
	}

	// --------------------------------------------------------------------

	function getPartnersMails()
	{
		$output = array();

		$partners = $this->getAllPartnersByLanguage();

		if(is_array($partners))
		{
			foreach($partners as $key => $value)
			{
				if($value->mail != '')
				{
					$output[] = $value->mail;
				}
				elseif($value->personal_mail != '')
				{
					$output[] = $value->personal_mail;
				}
			}
		}

		return $output;
	}

	// --------------------------------------------------------------------

	function getDefaultPartners()
	{
		$filter['customFilter'][0] = array(
												'field' => 'is_default',
												'value' => 1
											);

		$partners =  $this->getPartners($filter);

		$output = array();
		if(isset($partners[0]))
		{
			foreach($partners as $key => $value)
			{
				$output[$value->id] = $value;
			}
			return $output;
		}

		return false;
	}

	// --------------------------------------------------------------------

	function getPartners($filter = array())
	{
		$CI = & get_instance();

		$CI->load->model('../../models/PartnersModel');

		return  $CI->PartnersModel->getAllPartners(20, 0, 'name asc', $filter);
	}
}
