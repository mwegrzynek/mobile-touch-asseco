<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class urllib {

	function __construct()
	{
		$this->urlInfo = $this->getUrlInfo();
	}

	// --------------------------------------------------------------------

	function getUrlInfo()
	{
		return parse_url(base_url());
	}

	// --------------------------------------------------------------------

	function getHost()
	{
		// return str_replace('www.', '', $this->urlInfo['host']);
		return str_replace('www.', '', $this->urlInfo['host'].$_SERVER['REQUEST_URI']);
	}

	// --------------------------------------------------------------------

	function checkIfUrlIsMobile()
	{
		$currentUrl = $this->getHost();

		$prefix = 'm.';
		$pos = strpos($currentUrl, $prefix);

		if ($pos === 0)
		{
			return true;
		}
		return false;
	}

	// --------------------------------------------------------------------

	function redirectToMobile()
	{

		$location = $this->getHost();
		$location = 'http://m.'.$location;

		$this->redirect($location);
	}

	// --------------------------------------------------------------------

	function redirectToRegular()
	{
		$location = $this->getHost();
		$location = preg_replace('/^m\./', '', $location);
		$location = 'http://'.$location;

		$this->redirect($location);
	}

	// --------------------------------------------------------------------

	function redirect($location)
	{
		header("Location: ".$location);
		exit;
	}
}