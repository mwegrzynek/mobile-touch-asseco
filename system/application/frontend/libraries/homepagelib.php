<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Homepagelib {


	function getVideoSlider()
	{
		$CI = & get_instance();

		$filter['customFilter'][0] = array(
											'field' => 'is_active',
											'value' => 1
										);

		$result = $CI->ItemsModel->getAllItems(10, 20, 0, 'position', 'asc', $filter);

		foreach($result as $key => $value)
		{
			$videoSlider[$value->id] = $value;
			$videoSlider[$value->id]->videoInfo = $this->getVideoInfo($value->id);
		}

		$CI->mysmarty->assign('videoSlider', $videoSlider);

		return true;
	}

	// --------------------------------------------------------------------

	function getVideoInfo($itemId)
	{
		$CI = & get_instance();

		$videoInfo = $CI->VideosModel->getVideos(1, 0, 'position asc', $itemId);
		return $videoInfo[0];
	}

	// --------------------------------------------------------------------

	function getMainSlider()
	{
		$CI = & get_instance();

		$result = $CI->ItemsModel->getAllItems(20, 4, 0, 'position', 'asc');


		$CI->mysmarty->assign('mainSlider', $result);

		return $result;
	}

	// --------------------------------------------------------------------

	function getLogoSlider()
	{
		$CI = & get_instance();

		$filter['customFilter'][0] = array(
											'field' => 'is_active',
											'value' => 1
										);

		$result = $CI->ItemsModel->getAllItems(21, 20, 0, 'position', 'asc', $filter);

		if(isset($result[0]))
		{
			$result = array_chunk($result, 6);
			$CI->mysmarty->assign('logoSlider', $result);
			return $result;
		}


		return false;
	}

	// --------------------------------------------------------------------

	function getWhyUs($ifChunked = false)
	{
		$CI = & get_instance();

		$result = $CI->ItemsModel->getAllItems(26, 100, 0, 'position', 'asc');

		if($ifChunked)
		{
			$result = array_chunk($result, 2);
		}

		$CI->mysmarty->assign('whyUs', $result);

		return $result;
	}

	// --------------------------------------------------------------------

	function getWhyMobileTouch($ifChunked = false)
	{
		$CI = & get_instance();

		$result = $CI->ItemsModel->getAllItems(27, 100, 0, 'position', 'asc');

		if($ifChunked)
		{
			$result = array_chunk($result, 2);
		}

		$CI->mysmarty->assign('whyMobileTouch', $result);

		return $result;
	}
}