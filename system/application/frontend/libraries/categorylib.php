<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categorylib {	
	
	
	function getCategoryById($id)
	{
		$CI = & get_instance();		
		$category = $CI->CategoryModel->getCategoryById($id, $CI->languageid);		
		
		if(isset($category[0])) 
		{
			return $category[0];
		}		
		return false;
	}
	
	// --------------------------------------------------------------------
	
	function getCategoryBySlug($slug, $categoryType)
	{
		$CI = & get_instance();		
		$category = $CI->CategoryModel->getCategoryBySlug($slug, $CI->languageid, $categoryType);	
		if(isset($category[0])) 
		{
			return $category[0];
		}	
		return false;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Check if all parent of this category are active
	 *
	 * @access	public
	 * @param	integer $id
	 * @return	bool true/false
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2011, Maciej Węgrzynek
	 */
	function checkParentsActive($id)
	{
		$CI = & get_instance();
		$CI->parentsInfo = $CI->CategoryModel->getCategoryParentsInfo($id);		
		
		if(isset($CI->parentsInfo[0])) 
		{
			foreach($CI->parentsInfo as $key => $value) 
			{
				if(!$value->is_active) 
				{
					return false;
				}
			}
		}
		return true;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Check if path is valid. 
	 * some/category is valid, but /category is not
	 * 
	 *
	 * @access	public
	 * @param	integer $parentid
	 * @param	string $slug
	 * @return	bool true/false
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2011, Maciej Węgrzynek
	 */
	function checkPathValid($parentid, $slug)
	{
		$CI = & get_instance();		
		
		if(!isset($CI->parentsInfo)) 
		{
			$CI->parentsInfo = $CI->CategoryModel->getCategoryParentsInfo($parentid);
		}
		
		$path = '';
		if(isset($CI->parentsInfo[0])) 
		{
			foreach($CI->parentsInfo as $key => $value) 
			{
				if($value->is_active) 
				{
					$path .= $value->slug.'/';
				}
			}			
		}
		$path .= $slug;	
				
		$urlPath = implode('/', $CI->segments);
		//$urlPath = str_replace($CI->siteRoutes[82]->slug.'/', '', $urlPath);
		
		if($path == $urlPath) 
		{
			return true;
		}
		
		return false;
	}
	
	// --------------------------------------------------------------------
	
	
	/**
	 * Get category path
	 *
	 * @access	public
	 * @param	integer $categoryid
	 * @return	array $pathinfo
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2011, Maciej Węgrzynek
	 */
	function getCategoryParentPath($categoryid)
	{
		$CI = & get_instance();
		
		$parentsInfo = $CI->CategoryModel->getCategoryParentsInfo($categoryid);
		$pathInfo = array('path' => '', 'pathCrumbs' => array());
		$path = '';		
		
		if(isset($parentsInfo[0])) 
		{
			foreach($parentsInfo as $key => $value) 
			{
				if($value->is_active) 
				{
					$pathArray[$key]['title'] = $value->name;
					$pathArray[$key]['slug'] = $value->slug;
					$pathArray[$key]['nohtml'] = true;
					$path .= $value->slug.'/';
				}
			}
			$pathInfo['path'] = $path;			
			$pathInfo['pathCrumbs'] = $pathArray;			
		}
		
		//$pathInfo['path'] = $CI->siteRoutes[82]->slug.'/'.$pathInfo['path'];			
		//$pathInfo['path'] = $CI->siteRoutes[82]->slug.'/'.$pathInfo['path'];			
		//$firstElement = array('title' => $CI->siteTexts[112]->content, 'slug' => $CI->siteRoutes[82]->slug);	
		//array_unshift($pathInfo['pathCrumbs'], $firstElement);
		
		return $pathInfo;
	}
		
	// --------------------------------------------------------------------
	
	/**
	 * Get active categories order by popular and name
	 *
	 * @access	public
	 * @param	integer $categoryid
	 * @return	array $categories
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2011, Maciej Węgrzynek
	 */
	function getCategories($type = 1, $templateVar = 'industries')
	{
		$CI = & get_instance();	
		$filter['customFilter'][0] = array(
													'field' => 'is_active',
													'value' => 1
											);
													
		$categories = $CI->CategoryModel->getCategories($type, 1000, 0, 'slug asc', $filter);
		
		$CI->mysmarty->assign($templateVar, $categories);
		return $categories;	
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Get subcategories of given category
	 *
	 * @access	public
	 * @param	integer $categoryid
	 * @return	array $categories
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2011, Maciej Węgrzynek
	 */
	function getSubCategories($categoryid)
	{
		$CI = & get_instance();	
		$filter['customFilter'][0] = array(
													'field' => 'is_active',
													'value' => 1
											);
												
		$filter['customFilter'][1] = array(
												'field' => 'parent_id',
												'value' => $categoryid
												);											
											
		$categories = $CI->CategoryModel->getCategories(1, 1000, 0, 'items_count desc, slug asc', $filter);
		return $categories;	
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Get subcategories IDS of given category
	 *
	 * @access	public
	 * @param	integer $categoryid
	 * @return	array $categoriesIds
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2011, Maciej Węgrzynek
	 */
	function getSubCategoriesIds($categoryid)
	{
		$categoriesIds = array();
		$categories = $this->getSubCategories($categoryid);
		if(isset($categories[0])) 
		{
			foreach($categories as $key => $value) 
			{
				$categoriesIds[] = $value->id;
			}		
		}
		return $categoriesIds;	
	}
	
	// --------------------------------------------------------------------
		
	/**
	 * Set pagination setting and send it to smarty
	 *
	 * @access	public
	 * @param	integer $itemsCount
	 * @return	integer $offset
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2011, Maciej Węgrzynek
	 */
	function setPagination($itemsCount)
	{
		$offset = $this->offset;
		$CI = & get_instance();
		
		$CI->load->library('pagination2');

		$urlString = implode('/', $CI->segments);
		
		$CI->paginationConfig['config']['base_url'] = base_url().$urlString.'/page/';
		$CI->paginationConfig['config']['uri_segment'] = $this->uri_segment; //set by $this->checkSegments()
		$CI->paginationConfig['config']['total_rows'] = $itemsCount;
		
		$CI->paginationConfig['config']['per_page'] = 10;

		$CI->pagination2->initialize($CI->paginationConfig['config']);
		$pagination = $CI->pagination2->create_links();
		$CI->mysmarty->assign('pagination', $pagination);			
		
				
		$CI->mysmarty->assign('currentPerPage', $CI->paginationConfig['config']['per_page']);		
		
		return $offset;			
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Get offset from address. Return address segments without "page/x"
	 *
	 * @access	public
	 * @param	array $segments
	 * @return	integer $offset
	 * @return	array $segments
	 * @author	Maciej Węgrzynek <maciej@hitmo.pl>
	 *	@copyright Copyright (c) 2011, Maciej Węgrzynek
	 */
	function checkSegments($segments)
	{
		$this->offset = 0;
		$this->uri_segment = count($segments);
		if($this->uri_segment > 2) 
		{
			$offset = end($segments);
			$toCheck = prev($segments);
			if($toCheck == 'page') 
			{
				$this->offset = $offset;				
				$segments = array_slice($segments, 0, ($this->uri_segment-2));
			}
		}
		return $segments;
	}
	
	// --------------------------------------------------------------------
	
	function getDefaultCategory()
	{
		$CI = & get_instance();
		
		$filter['customFilter'][0] = array(					
											'value' => 1,
											'field' => 'is_default'
										);			
				
		$category = $CI->CategoryModel->getCategories(1, 1, 0, 'position asc', $filter);
		return $category[0];
	}
	
	// --------------------------------------------------------------------	
	
	function getCategoriesByItemId($id)
	{
		$CI = & get_instance();
		return $CI->ItemsModel->getCategoriesIdsByItemId($id);		
	}
}