/*
 * Scripts
 *
 */
jQuery(function($) {
 
	var Engine = {
		utils : {
			links : function(){
				$('a[rel*=external]').click(function(e){
					e.preventDefault();
					window.open($(this).attr('href'));						  
				});
			},
			mails : function(){
				$('a[href^=mailto:]').each(function(){
					var mail = $(this).attr('href').replace('mailto:','');
					var replaced = mail.replace('/at/','@');
					$(this).attr('href','mailto:'+replaced);
					if($(this).text() == mail) {
						$(this).text(replaced);
					}
				});
			}
		},
		ui : {
			loginTypeVisibilityToggle : function () {
				$(".link-container a").click(function () {
					if($('#forgotten-pass').is(':hidden'))
					{
						/*$('#login-container').hide();
						$('#forgotten-pass').show('normal');*/
						
						$('#login-container').hide();
						$('#forgotten-pass').fadeIn('normal');							
					}
					else
					{
						$('#forgotten-pass').hide();			
						$('#login-container').fadeIn('normal');		
					}					
					return false;					
			    });	
			}
		}
	};

	Engine.utils.links();
	Engine.utils.mails();
	Engine.ui.loginTypeVisibilityToggle();
	
});