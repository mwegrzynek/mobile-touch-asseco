var tinyLang = "en";

var tinyConfigMax = {
	// General options
		language : tinyLang,
		body_id : "regularTiny",
		body_class : "short-col",
		mode : "specific_textareas",
		editor_selector : "mceEditor",
		theme : "advanced",
		entity_encoding : "raw",
		plugins : "safari,pagebreak,style,layer,table,save,advhr,ezfilemanager,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups",

		// Theme options
		//theme_advanced_styles : "image, some, tralala",
		//,styleselect
		theme_advanced_styles : "Header Type 1=header-e,Suppelmental text=text-f",
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,styleselect,|,visualchars,nonbreaking,template",
		theme_advanced_buttons2 : "pastetext,pasteword,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,image,cleanup,help,code",
		theme_advanced_buttons3 : "hr,|,removeformat,|,sub,sup,|,charmap,iespell,media,|,fullscreen,|,ezfilemanager",

		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

      //paste_auto_cleanup_on_paste : true,

		// Example word content CSS (should be your site CSS) this one removes paragraph margins
		content_css : "styles/screen.css?v=4.2",
		width: "760",

		// Replace values for the template plugin
		/*template_replace_values : {
			username : "Some User",
			staffid : "991234"
		},*/

		template_popup_width : 800,

		template_templates : [
			{
				title : "News quotation",
				src : "/admin/tinytemplates/news-quotation",
				description : "Input quotation"
			},
			{
				title : "News contact",
				src : "/admin/tinytemplates/news-contact",
				description : "Footer contact for news"
			},
			{
				title : "Text with video",
				src : "/admin/tinytemplates/text-with-video",
				description : "Text with video at right"
			},
			{
				title : "Team Leader Testimonial",
				src : "/admin/tinytemplates/person-testimonial",
				description : "Team Leader Testimonial"
			},
			{
				title : "Text with image to left",
				src : "/admin/tinytemplates/text-block-with-image-left",
				description : "Text with image to left"
			},
			{
				title : "Text with image to right",
				src : "/admin/tinytemplates/text-block-with-image-right",
				description : "Text with image to right"
			},
			{
				title : "Text with image on top",
				src : "/admin/tinytemplates/text-block-with-image-top",
				description : "Text with image on top"
			},
			{
				title : "Text with image on bottom",
				src : "/admin/tinytemplates/text-block-with-image-bottom",
				description : "Text with image on bottom"
			}
      ],


		extended_valid_elements : "@[id|class|data*],article,aside,audio,canvas,command,datalist,details,embed,figure,footer,header,hgroup,keygen,mark,meter,nav,output,progress,rp,rt,ruby,section,source,time,video",

		class_filter : function(cls, rule) {
        // Block the someClass
        if (cls == 'image')
            return 'image';


        // Pass though the rest
        return false;
    	},

		//
		document_base_url : document.location.protocol+'//'+document.location.hostname+"/",

		file_browser_callback : "CustomFileBrowser"
		//tinymce/plugins/ezfilemanager/ezfilemanager.php
		//config tinymce/plugins/ezfilemanager/includes/config.inc.php
};

tinyMCE.init(tinyConfigMax);

var tinyConfigWide = tinyConfigMax;
tinyConfigWide.width = "980";
tinyConfigWide.body_id = "wideTiny";
tinyConfigWide.body_class = "wide-col";
tinyConfigWide.editor_selector = "mceEditorWide";
tinyMCE.init(tinyConfigWide);

tinyMCE.init({
		// General options
		body_id : "simpleTiny",
		language : tinyLang,
		mode : "specific_textareas",
		editor_selector : "mceEditorSimple",
		theme : "advanced",
		plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups",

		// Theme options
		//theme_advanced_styles : "image, some, tralala",
		//,styleselect
		//theme_advanced_styles : "Bez marginesu=no-margin",
		theme_advanced_buttons1 : "pastetext,pasteword,|,bold,italic,underline,|,bullist,numlist,|,link,unlink,|,undo,redo,code",
		theme_advanced_buttons2 : "",
		theme_advanced_buttons3 : "",

		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,

		// Example word content CSS (should be your site CSS) this one removes paragraph margins
		content_css : "styles/screen.css",
		width: "760",
		entity_encoding : "raw",
		document_base_url : document.location.protocol+'//'+document.location.hostname+"/"
});