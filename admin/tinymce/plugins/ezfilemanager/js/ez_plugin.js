$(document).ready(function() {
	
	function ez_showFileManager(obj) {
		var dir = obj.data("dir");
		if (typeof dir != 'undefined' && dir != '') {
			var querystr= "?tmce=2&p="+dir;       
		}
		else {
	    	var querystr= "?tmce=2";    
		}
		var windowname = obj.attr("id");
		var newWindow = window.open("http://"+document.domain+"/admin/tinymce/plugins/ezfilemanager/index.php"+querystr,windowname,"scrollbars=yes,status=yes,menubar=no,toolbar=no,resizeable=no,height=680,width=720");
	}
	
	$("input.filemanager").dblclick(function() {
		ez_showFileManager($(this));
	});
	
	$(".filemanager-ico").click(function() {
		var obj = $(this).parent().find('input');
		ez_showFileManager(obj);
	});
	
});