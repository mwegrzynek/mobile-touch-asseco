/* TEAM */
	hitmo: hitmo-studio.com
	Twitter: @hitmostudio
	FB: http://www.facebook.com/hitmostudio
	
	UI developer, Backend Developer: Maciej Węgrzynek	
	From: Koszalin, Poland

	Web designer: Paweł Opozda
	From: Koszalin, Poland

/* SITE */
  Standards: HTML5, CSS3
  Language: English
  Components: jQuery, CodeIgniter, Smarty3, Hitmo CMS
  Doctype: HTML5
  Software: E Texteditor, Notepad++, FileZilla, Photoshop