/*!
 * Project:   Mobile Touch
 * Date:      2012/05/04
 * Author:    Maciej Węgrzynek <maciej@hitmo-studio.com>, hitmo-studio.com
/* ---------------------------------------- */

/*!
 * jQuery Browser Support - v1.0.0 - 16.01.2013
 * https://github.com/SerafimArts/jquery.browser.js
 * Copyright 2013 Serafim Arts, Inc. and other contributors; Licensed MIT
 */

if(parseFloat(/[0-9]\.[0-9]/.exec($.fn.jquery)[0]) >= 1.9){
	(function(jQuery){
		jQuery.uaMatch = function( ua ) {
			ua = ua.toLowerCase();

			var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
				/(webkit)[ \/]([\w.]+)/.exec( ua ) ||
				/(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
				/(msie) ([\w.]+)/.exec( ua ) ||
				ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
				[];

			return {
				browser: match[ 1 ] || "",
				version: match[ 2 ] || "0"
			};
		};

		matched = jQuery.uaMatch( navigator.userAgent );
		browser = {};

		if ( matched.browser ) {
			browser[ matched.browser ] = true;
			browser.version = matched.version;
		}

		/*/ Chrome is Webkit, but Webkit is also Safari. */
		if ( browser.chrome ) {
			browser.webkit = true;
		} else if ( browser.webkit ) {
			browser.safari = true;
		}
		jQuery.browser = browser;
	})(jQuery);
}

/*cycle plugin*/
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}(';(6($,D){"3u 3v";7 E=\'2.4x.5\';4($.1Q===D){$.1Q={18:!($.2q.3w)}}6 1D(s){4($.F.8.1D)1i(s)}6 1i(){4(4y.2S&&2S.1i)2S.1i(\'[8] \'+4z.4A.4B.3x(4C,\' \'))}$.4D[\':\'].2T=6(a){M a.1k};$.F.8=6(g,h){7 o={s:K.3y,c:K.4E};4(K.S===0&&g!=\'2r\'){4(!$.3z&&o.s){1i(\'3A 1l 3B, 4F 2s\');$(6(){$(o.s,o.c).8(g,h)});M K}1i(\'2t; 4G 1v 2u 4H 3y\'+($.3z?\'\':\' (3A 1l 3B)\'));M K}M K.1f(6(){7 a=3C(K,g,h);4(a===N)M;a.1R=a.1R||$.F.8.1R;4(K.1b)1S(K.1b);K.1b=K.1k=0;K.1I=0;7 b=$(K);7 c=a.2U?$(a.2U,K):b.3D();7 d=c.4I();4(d.S<2){1i(\'2t; 4J 4K 4L: \'+d.S);M}7 e=3E(b,c,d,a,o);4(e===N)M;7 f=e.2h?10:2V(d[e.11],d[e.V],e,!e.1o);4(f){f+=(e.3F||0);4(f<10)f=10;1D(\'4M 1d: \'+f);K.1b=2v(6(){1J(d,e,0,!a.1o)},f)}})};6 1M(a,b,c){7 d=$(a).1K(\'8.1T\');7 e=!!a.1k;4(e&&d.2T)d.2T(a,d,b,c);Q 4(!e&&d.3G)d.3G(a,d,b,c)}6 3C(e,f,g){4(e.1I===D)e.1I=0;4(f===D||f===Z)f={};4(f.2W==3H){4N(f){1U\'27\':1U\'2r\':7 h=$(e).1K(\'8.1T\');4(!h)M N;e.1I++;4(e.1b)1S(e.1b);e.1b=0;4(h.1v)$(h.1v).2r();$(e).4O(\'8.1T\');4(f==\'27\')27(e,h);M N;1U\'4P\':e.1k=(e.1k===1)?0:1;2X(e.1k,g,e);1M(e);M N;1U\'2Y\':e.1k=1;1M(e);M N;1U\'3I\':e.1k=0;2X(N,g,e);1M(e);M N;1U\'1N\':1U\'14\':h=$(e).1K(\'8.1T\');4(!h){1i(\'2Z 1l 2u, "1N/14" 4Q\');M N}$.F.8[f](h);M N;4R:f={1e:f}}M f}Q 4(f.2W==4S){7 i=f;f=$(e).1K(\'8.1T\');4(!f){1i(\'2Z 1l 2u, 3J 1l 29 31\');M N}4(i<0||i>=f.1v.S){1i(\'4T 31 2i: \'+i);M N}f.V=i;4(e.1b){1S(e.1b);e.1b=0}4(32 g==\'4U\')f.2w=g;1J(f.1v,f,1,i>=f.11);M N}M f;6 2X(a,b,c){4(!a&&b===L){7 d=$(c).1K(\'8.1T\');4(!d){1i(\'2Z 1l 2u, 3J 1l 3I\');M N}4(c.1b){1S(c.1b);c.1b=0}1J(d.1v,d,1,!d.1o)}}}6 33(a,b){4(!$.1Q.18&&b.2j&&a.3K.3L){4V{a.3K.4W(\'3L\')}4X(4Y){}}}6 27(a,b){4(b.14)$(b.14).2x(b.2k);4(b.1N)$(b.1N).2x(b.2k);4(b.1L||b.1V)$.1f(b.2l||[],6(){K.2x().4Z()});b.2l=Z;$(a).2x(\'35.8 3M.8\');4(b.27)b.27(b)}6 3E(f,g,k,l,o){7 m;7 n=$.1g({},$.F.8.3N,l||{},$.3O?f.3O():$.50?f.1K():{});7 p=$.1E(f.1K)?f.1K(n.3P):Z;4(p)n=$.1g(n,p);4(n.36)n.38=n.39||k.S;7 q=f[0];f.1K(\'8.1T\',n);n.$1W=f;n.2y=q.1I;n.1v=k;n.U=n.U?[n.U]:[];n.1m=n.1m?[n.1m]:[];4(!$.1Q.18&&n.2j)n.1m.R(6(){33(K,n)});4(n.2h)n.1m.R(6(){1J(k,n,0,!n.1o)});3Q(n);4(!$.1Q.18&&n.2j&&!n.3a)3b(g);4(f.T(\'2z\')==\'51\')f.T(\'2z\',\'52\');4(n.I)f.I(n.I);4(n.H&&n.H!=\'2A\')f.H(n.H);4(n.1c!==D){n.1c=1j(n.1c,10);4(n.1c>=k.S||n.53<0)n.1c=0;Q m=L}Q 4(n.1o)n.1c=k.S-1;Q n.1c=0;4(n.1p){n.1w=[];1B(7 i=0;i<k.S;i++)n.1w.R(i);n.1w.3c(6(a,b){M 1X.1p()-0.5});4(m){1B(7 r=0;r<k.S;r++){4(n.1c==n.1w[r]){n.1n=r}}}Q{n.1n=1;n.1c=n.1w[1]}}Q 4(n.1c>=k.S)n.1c=0;n.11=n.1c||0;7 s=n.1c;g.T({2z:\'3R\',J:0,G:0}).1Y().1f(6(i){7 z;4(n.1o)z=s?i<=s?k.S+(i-s):s-i:k.S-i;Q z=s?i>=s?k.S-(i-s):s-i:k.S-i;$(K).T(\'z-2i\',z)});$(k[s]).T(\'18\',1).2B();33(k[s],n);4(n.1F){4(!n.2m){4(n.I)g.I(n.I);4(n.H&&n.H!=\'2A\')g.H(n.H)}Q{g.1f(6(){7 a=$(K);7 b=(n.2m===L)?a.I()/a.H():n.2m;4(n.I&&a.I()!=n.I){a.I(n.I);a.H(n.I/b)}4(n.H&&a.H()<n.H){a.H(n.H);a.I(n.H*b)}})}}4(n.3d&&((!n.1F)||n.2m)){g.1f(6(){7 a=$(K);a.T({"2C-G":n.I?((n.I-a.I())/2)+"1a":0,"2C-J":n.H?((n.H-a.H())/2)+"1a":0})})}4(n.3d&&!n.1F&&!n.2D){g.1f(6(){7 a=$(K);a.T({"2C-G":n.I?((n.I-a.I())/2)+"1a":0,"2C-J":n.H?((n.H-a.H())/2)+"1a":0})})}7 t=n.3S&&!f.54();4(t){7 u=0,2a=0;1B(7 j=0;j<k.S;j++){7 v=$(k[j]),e=v[0],w=v.55(),h=v.56();4(!w)w=e.3T||e.I||v.2E(\'I\');4(!h)h=e.3U||e.H||v.2E(\'H\');u=w>u?w:u;2a=h>2a?h:2a}4(u>0&&2a>0)f.T({I:u+\'1a\',H:2a+\'1a\'})}7 x=N;4(n.2Y)f.2b(\'35.8\',6(){x=L;K.1k++;1M(q,L)}).2b(\'3M.8\',6(){4(x)K.1k--;1M(q,L)});4(3V(n)===N)M N;7 y=N;l.2F=l.2F||0;g.1f(6(){7 a=$(K);K.W=(n.1F&&n.H)?n.H:(a.H()||K.3U||K.H||a.2E(\'H\')||0);K.X=(n.1F&&n.I)?n.I:(a.I()||K.3T||K.I||a.2E(\'I\')||0);4(a.57(\'3W\')){7 b=($.2q.3w&&K.X==28&&K.W==30&&!K.2G);7 c=($.2q.58&&K.X==34&&K.W==19&&!K.2G);7 d=($.2q.59&&((K.X==42&&K.W==19)||(K.X==37&&K.W==17))&&!K.2G);7 e=(K.W===0&&K.X===0&&!K.2G);4(b||c||d||e){4(o.s&&n.3X&&++l.2F<5a){1i(l.2F,\' - 3W 31 1l 5b, 5c 2s: \',K.3Y,K.X,K.W);2v(6(){$(o.s,o.c).8(l)},n.3Z);y=L;M N}Q{1i(\'5d 1l 5e 5f 5g 5h: \'+K.3Y,K.X,K.W)}}}M L});4(y)M N;n.9=n.9||{};n.1s=n.1s||{};n.1t=n.1t||{};n.P=n.P||{};n.O=n.O||{};g.1l(\':2H(\'+s+\')\').T(n.9);$(g[s]).T(n.1t);4(n.1d){n.1d=1j(n.1d,10);4(n.1h.2W==3H)n.1h=$.1e.5i[n.1h]||1j(n.1h,10);4(!n.2I)n.1h=n.1h/2;7 A=n.1e==\'2n\'?0:n.1e==\'1Z\'?5j:3e;40((n.1d-n.1h)<A)n.1d+=n.1h}4(n.3f)n.21=n.22=n.3f;4(!n.2c)n.2c=n.1h;4(!n.1G)n.1G=n.1h;n.1C=k.S;n.11=n.3g=s;4(n.1p){4(++n.1n==k.S)n.1n=0;n.V=n.1w[n.1n]}Q 4(n.1o)n.V=n.1c===0?(k.S-1):n.1c-1;Q n.V=n.1c>=(k.S-1)?0:n.1c+1;4(!n.2d){7 B=$.F.8.Y[n.1e];4($.1E(B))B(f,g,n);Q 4(n.1e!=\'3h\'&&!n.2d){1i(\'41 2J: \'+n.1e,\'; 2s 2t\');M N}}7 C=g[s];4(!n.43){4(n.U.S)n.U[0].2K(C,[C,C,n,L]);4(n.1m.S)n.1m[0].2K(C,[C,C,n,L])}4(n.14)$(n.14).2b(n.2k,6(){M 29(n,1)});4(n.1N)$(n.1N).2b(n.2k,6(){M 29(n,0)});4(n.1L||n.1V)44(k,n);45(n,k);M n}6 3Q(a){a.1u={U:[],1m:[]};a.1u.9=$.1g({},a.9);a.1u.1s=$.1g({},a.1s);a.1u.P=$.1g({},a.P);a.1u.O=$.1g({},a.O);$.1f(a.U,6(){a.1u.U.R(K)});$.1f(a.1m,6(){a.1u.1m.R(K)})}6 3V(a){7 i,1O,23=$.F.8.Y;4(a.1e.46(\',\')>0){a.2d=L;a.1q=a.1e.5k(/\\s*/g,\'\').5l(\',\');1B(i=0;i<a.1q.S;i++){7 b=a.1q[i];1O=23[b];4(!1O||!23.3i(b)||!$.1E(1O)){1i(\'5m 41 2J: \',b);a.1q.47(i,1);i--}}4(!a.1q.S){1i(\'5n 5o Y 5p; 2s 2t.\');M N}}Q 4(a.1e==\'5q\'){a.2d=L;a.1q=[];1B(7 p 48 23){4(23.3i(p)){1O=23[p];4(23.3i(p)&&$.1E(1O))a.1q.R(p)}}}4(a.2d&&a.49){7 c=1X.4a(1X.1p()*20)+30;1B(i=0;i<c;i++){7 d=1X.4a(1X.1p()*a.1q.S);a.1q.R(a.1q.47(d,1)[0])}1D(\'5r 1e 5s: \',a.1q)}M L}6 45(f,g){f.5t=6(c,d){7 e=$(c),s=e[0];4(!f.39)f.38++;g[d?\'3j\':\'R\'](s);4(f.1x)f.1x[d?\'3j\':\'R\'](s);f.1C=g.S;4(f.1p){f.1w.R(f.1C-1);f.1w.3c(6(a,b){M 1X.1p()-0.5})}e.T(\'2z\',\'3R\');e[d?\'5u\':\'4b\'](f.$1W);4(d){f.11++;f.V++}4(!$.1Q.18&&f.2j&&!f.3a)3b(e);4(f.1F&&f.I)e.I(f.I);4(f.1F&&f.H&&f.H!=\'2A\')e.H(f.H);s.W=(f.1F&&f.H)?f.H:e.H();s.X=(f.1F&&f.I)?f.I:e.I();e.T(f.9);4(f.1L||f.1V)$.F.8.3k(g.S-1,s,$(f.1L),g,f);4($.1E(f.4c))f.4c(e);Q e.1Y()}}$.F.8.4d=6(a,b){b=b||a.1e;a.U=[];a.1m=[];a.9=$.1g({},a.1u.9);a.1s=$.1g({},a.1u.1s);a.P=$.1g({},a.1u.P);a.O=$.1g({},a.1u.O);a.2e=Z;$.1f(a.1u.U,6(){a.U.R(K)});$.1f(a.1u.1m,6(){a.1m.R(K)});7 c=$.F.8.Y[b];4($.1E(c))c(a.$1W,$(a.1v),a)};6 1J(c,d,e,f){7 p=d.$1W[0],1y=c[d.11],14=c[d.V];4(e&&d.2o&&d.3l){1D(\'3l 48 1J(), 5v 4e 2J\');$(c).2r(L,L);d.2o=0;1S(p.1b)}4(d.2o){1D(\'2J 4e, 5w 5x 1O 5y\');M}4(p.1I!=d.2y||p.1b===0&&!e)M;4(!e&&!p.1k&&!d.3m&&((d.36&&(--d.38<=0))||(d.2L&&!d.1p&&d.V<d.11))){4(d.3n)d.3n(d);M}7 g=N;4((e||!p.1k)&&(d.V!=d.11)){g=L;7 h=d.1e;1y.W=1y.W||$(1y).H();1y.X=1y.X||$(1y).I();14.W=14.W||$(14).H();14.X=14.X||$(14).I();4(d.2d){4(f&&(d.24===D||++d.24>=d.1q.S))d.24=0;Q 4(!f&&(d.24===D||--d.24<0))d.24=d.1q.S-1;h=d.1q[d.24]}4(d.2w){h=d.2w;d.2w=Z}$.F.8.4d(d,h);4(d.U.S)$.1f(d.U,6(i,o){4(p.1I!=d.2y)M;o.2K(14,[1y,14,d,f])});7 j=6(){d.2o=0;$.1f(d.1m,6(i,o){4(p.1I!=d.2y)M;o.2K(14,[1y,14,d,f])});4(!p.1I){3o()}};1D(\'1O 5z(\'+h+\'); 11: \'+d.11+\'; V: \'+d.V);d.2o=1;4(d.2e)d.2e(1y,14,d,j,f,e&&d.2M);Q 4($.1E($.F.8[d.1e]))$.F.8[d.1e](1y,14,d,j,f,e&&d.2M);Q $.F.8.3h(1y,14,d,j,f,e&&d.2M)}Q{3o()}4(g||d.V==d.11){7 k;d.3g=d.11;4(d.1p){d.11=d.V;4(++d.1n==c.S){d.1n=0;d.1w.3c(6(a,b){M 1X.1p()-0.5})}d.V=d.1w[d.1n];4(d.V==d.11)d.V=(d.11==d.1C-1)?0:d.11+1}Q 4(d.1o){k=(d.V-1)<0;4(k&&d.3m){d.1o=!d.1o;d.V=1;d.11=0}Q{d.V=k?(c.S-1):d.V-1;d.11=k?0:d.V+1}}Q{k=(d.V+1)==c.S;4(k&&d.3m){d.1o=!d.1o;d.V=c.S-2;d.11=c.S-1}Q{d.V=k?0:d.V+1;d.11=k?c.S-1:d.V-1}}}4(g&&d.1L)d.1R(d.1L,d.11,d.3p);6 3o(){7 a=0,1d=d.1d;4(d.1d&&!d.2h){a=2V(c[d.11],c[d.V],d,f);4(d.1e==\'1Z\')a-=d.1G}Q 4(d.2h&&p.1k)a=10;4(a>0)p.1b=2v(6(){1J(c,d,0,!d.1o)},a)}}$.F.8.1R=6(a,b,c){$(a).1f(6(){$(K).3D().5A(c).2H(b).5B(c)})};6 2V(a,b,c,d){4(c.3q){7 t=c.3q.3x(a,a,b,c,d);40(c.1e!=\'2n\'&&(t-c.1h)<3e)t+=c.1h;1D(\'5C 1d: \'+t+\'; 1h: \'+c.1h);4(t!==N)M t}M c.1d}$.F.8.14=6(a){29(a,1)};$.F.8.1N=6(a){29(a,0)};6 29(a,b){7 c=b?1:-1;7 d=a.1v;7 p=a.$1W[0],1d=p.1b;4(1d){1S(1d);p.1b=0}4(a.1p&&c<0){a.1n--;4(--a.1n==-2)a.1n=d.S-2;Q 4(a.1n==-1)a.1n=d.S-1;a.V=a.1w[a.1n]}Q 4(a.1p){a.V=a.1w[a.1n]}Q{a.V=a.11+c;4(a.V<0){4(a.2L)M N;a.V=d.S-1}Q 4(a.V>=d.S){4(a.2L)M N;a.V=0}}7 e=a.4f||a.5D;4($.1E(e))e(c>0,a.V,d[a.V]);1J(d,a,1,b);M N}6 44(a,b){7 c=$(b.1L);$.1f(a,6(i,o){$.F.8.3k(i,o,c,a,b)});b.1R(b.1L,b.1c,b.3p)}$.F.8.3k=6(i,b,c,d,f){7 a;4($.1E(f.1V)){a=f.1V(i,b);1D(\'1V(\'+i+\', 5E) 5F: \'+a)}Q a=\'<a 5G="#">\'+(i+1)+\'</a>\';4(!a)M;7 g=$(a);4(g.5H(\'5I\').S===0){7 h=[];4(c.S>1){c.1f(6(){7 a=g.5J(L);$(K).5K(a);h.R(a[0])});g=$(h)}Q{g.4b(c)}}f.2l=f.2l||[];f.2l.R(g);7 j=6(e){e.5L();f.V=i;7 p=f.$1W[0],1d=p.1b;4(1d){1S(1d);p.1b=0}7 a=f.4g||f.5M;4($.1E(a))a(f.V,d[f.V]);1J(d,f,1,f.11<i)};4(/35|5N/i.25(f.2N)){g.4h(j,6(){})}Q{g.2b(f.2N,j)}4(!/^2O/.25(f.2N)&&!f.4i)g.2b(\'2O.8\',6(){M N});7 k=f.$1W[0];7 l=N;4(f.4j){g.4h(6(){l=L;k.1k++;1M(k,L,L)},6(){4(l)k.1k--;1M(k,L,L)})}};$.F.8.4k=6(a,b){7 d,l=a.3g,c=a.11;4(b)d=c>l?c-l:a.1C-l;Q d=c<l?l-c:l+a.1C-c;M d};6 3b(b){1D(\'5O 5P 3r-3s 5Q\');6 2P(s){s=1j(s,10).5R(16);M s.S<2?\'0\'+s:s}6 4l(e){1B(;e&&e.5S.5T()!=\'5U\';e=e.5V){7 v=$.T(e,\'3r-3s\');4(v&&v.46(\'5W\')>=0){7 a=v.4m(/\\d+/g);M\'#\'+2P(a[0])+2P(a[1])+2P(a[2])}4(v&&v!=\'5X\')M v}M\'#5Y\'}b.1f(6(){$(K).T(\'3r-3s\',4l(K))})}$.F.8.12=6(a,b,c,w,h,d){$(c.1v).1l(a).1Y();4(32 c.9.18==\'5Z\')c.9.18=1;c.9.1P=\'2f\';4(c.2D&&w!==N&&b.X>0)c.9.I=b.X;4(c.2D&&h!==N&&b.W>0)c.9.H=b.W;c.1s=c.1s||{};c.1s.1P=\'2n\';$(a).T(\'2p\',c.1C+(d===L?1:0));$(b).T(\'2p\',c.1C+(d===L?0:1))};$.F.8.3h=6(a,b,c,d,e,f){7 g=$(a),$n=$(b);7 h=c.2c,1G=c.1G,21=c.21,22=c.22;$n.T(c.9);4(f){4(32 f==\'60\')h=1G=f;Q h=1G=1;21=22=Z}7 i=6(){$n.2Q(c.P,h,21,6(){d()})};g.2Q(c.O,1G,22,6(){g.T(c.1s);4(!c.2I)i()});4(c.2I)i()};$.F.8.Y={4n:6(d,e,f){e.1l(\':2H(\'+f.11+\')\').T(\'18\',0);f.U.R(6(a,b,c){$.F.8.12(a,b,c);c.9.18=0});f.P={18:1};f.O={18:0};f.9={J:0,G:0}}};$.F.8.61=6(){M E};$.F.8.3N={3p:\'62\',1m:Z,4i:N,P:Z,O:Z,2m:N,36:0,39:0,1o:N,U:Z,3d:Z,2j:!$.1Q.18,3a:N,3S:1,2h:0,1s:Z,9:Z,3F:0,21:Z,22:Z,3f:Z,3n:Z,2M:0,1F:0,1e:\'4n\',2e:Z,H:\'2A\',3l:L,3P:\'8\',14:Z,2L:0,4g:Z,4f:Z,1L:Z,1V:Z,2N:\'2O.8\',2Y:0,4j:0,1N:Z,2k:\'2O.8\',1p:0,49:1,3X:L,3Z:3e,2R:0,1Z:Z,43:N,2U:Z,2D:1,1h:63,2c:Z,1G:Z,1c:D,2I:1,1d:64,3q:Z,1R:Z,I:Z}})(4o);(6($){"3u 3v";$.F.8.Y.2n=6(e,f,g){g.2e=6(a,b,c,d){$(b).2B();$(a).1Y();d()}};$.F.8.Y.65=6(e,f,g){f.1l(\':2H(\'+g.11+\')\').T({1P:\'2f\',\'18\':1});g.U.R(6(a,b,c,w,h,d){$(a).T(\'2p\',c.1C+(d!==L?1:0));$(b).T(\'2p\',c.1C+(d!==L?0:1))});g.P.18=1;g.O.18=0;g.9.18=1;g.9.1P=\'2f\';g.1s.2p=0};$.F.8.Y.66=6(a,b,c){a.T(\'1r\',\'1z\');c.U.R($.F.8.12);7 h=a.H();c.9.J=h;c.9.G=0;c.1t.J=0;c.P.J=0;c.O.J=-h};$.F.8.Y.67=6(a,b,c){a.T(\'1r\',\'1z\');c.U.R($.F.8.12);7 h=a.H();c.1t.J=0;c.9.J=-h;c.9.G=0;c.P.J=0;c.O.J=h};$.F.8.Y.68=6(a,b,c){a.T(\'1r\',\'1z\');c.U.R($.F.8.12);7 w=a.I();c.1t.G=0;c.9.G=w;c.9.J=0;c.P.G=0;c.O.G=0-w};$.F.8.Y.69=6(a,b,c){a.T(\'1r\',\'1z\');c.U.R($.F.8.12);7 w=a.I();c.1t.G=0;c.9.G=-w;c.9.J=0;c.P.G=0;c.O.G=w};$.F.8.Y.6a=6(e,f,g){e.T(\'1r\',\'1z\').I();g.U.R(6(a,b,c,d){4(c.2R)d=!d;$.F.8.12(a,b,c);c.9.G=d?(b.X-1):(1-b.X);c.O.G=d?-a.X:a.X});g.1t.G=0;g.9.J=0;g.P.G=0;g.O.J=0};$.F.8.Y.6b=6(e,f,g){e.T(\'1r\',\'1z\');g.U.R(6(a,b,c,d){4(c.2R)d=!d;$.F.8.12(a,b,c);c.9.J=d?(1-b.W):(b.W-1);c.O.J=d?a.W:-a.W});g.1t.J=0;g.9.G=0;g.P.J=0;g.O.G=0};$.F.8.Y.6c=6(d,e,f){f.U.R(6(a,b,c){$(c.1v).1l(a).1Y();$.F.8.12(a,b,c,N,L);c.P.I=b.X});f.9.G=0;f.9.J=0;f.9.I=0;f.P.I=\'2B\';f.O.I=0};$.F.8.Y.6d=6(d,e,f){f.U.R(6(a,b,c){$(c.1v).1l(a).1Y();$.F.8.12(a,b,c,L,N);c.P.H=b.W});f.9.G=0;f.9.J=0;f.9.H=0;f.P.H=\'2B\';f.O.H=0};$.F.8.Y.1Z=6(j,l,m){7 i,w=j.T(\'1r\',\'4p\').I();l.T({G:0,J:0});m.U.R(6(a,b,c){$.F.8.12(a,b,c,L,L,L)});4(!m.4q){m.1h=m.1h/2;m.4q=L}m.1p=0;m.1Z=m.1Z||{G:-w,J:15};m.1x=[];1B(i=0;i<l.S;i++)m.1x.R(l[i]);1B(i=0;i<m.11;i++)m.1x.R(m.1x.4r());m.2e=6(b,c,d,e,f){4(d.2R)f=!f;7 g=f?$(b):$(c);$(c).T(d.9);7 h=d.1C;g.2Q(d.1Z,d.2c,d.21,6(){7 a=$.F.8.4k(d,f);1B(7 k=0;k<a;k++){4(f)d.1x.R(d.1x.4r());Q d.1x.3j(d.1x.6e())}4(f){1B(7 i=0,3t=d.1x.S;i<3t;i++)$(d.1x[i]).T(\'z-2i\',3t-i+h)}Q{7 z=$(b).T(\'z-2i\');g.T(\'z-2i\',1j(z,10)+1+h)}g.2Q({G:0,J:0},d.1G,d.22,6(){$(f?K:b).1Y();4(e)e()})})};$.1g(m.9,{1P:\'2f\',18:1,J:0,G:0})};$.F.8.Y.6f=6(d,e,f){f.U.R(6(a,b,c){$.F.8.12(a,b,c,L,N);c.9.J=b.W;c.P.H=b.W;c.O.I=b.X});f.1t.J=0;f.9.G=0;f.9.H=0;f.P.J=0;f.O.H=0};$.F.8.Y.6g=6(d,e,f){f.U.R(6(a,b,c){$.F.8.12(a,b,c,L,N);c.P.H=b.W;c.O.J=a.W});f.1t.J=0;f.9.G=0;f.9.J=0;f.9.H=0;f.O.H=0};$.F.8.Y.6h=6(d,e,f){f.U.R(6(a,b,c){$.F.8.12(a,b,c,N,L);c.9.G=b.X;c.P.I=b.X});f.9.J=0;f.9.I=0;f.P.G=0;f.O.I=0};$.F.8.Y.6i=6(d,e,f){f.U.R(6(a,b,c){$.F.8.12(a,b,c,N,L);c.P.I=b.X;c.O.G=a.X});$.1g(f.9,{J:0,G:0,I:0});f.P.G=0;f.O.I=0};$.F.8.Y.4s=6(d,e,f){f.U.R(6(a,b,c){$.F.8.12(a,b,c,N,N,L);c.9.J=b.W/2;c.9.G=b.X/2;$.1g(c.P,{J:0,G:0,I:b.X,H:b.W});$.1g(c.O,{I:0,H:0,J:a.W/2,G:a.X/2})});f.1t.J=0;f.1t.G=0;f.9.I=0;f.9.H=0};$.F.8.Y.6j=6(d,e,f){f.U.R(6(a,b,c){$.F.8.12(a,b,c,N,N);c.9.G=b.X/2;c.9.J=b.W/2;$.1g(c.P,{J:0,G:0,I:b.X,H:b.W})});f.9.I=0;f.9.H=0;f.O.18=0};$.F.8.Y.6k=6(d,e,f){7 w=d.T(\'1r\',\'1z\').I();f.U.R(6(a,b,c){$.F.8.12(a,b,c);c.P.I=b.X;c.O.G=a.X});f.9.G=w;f.9.J=0;f.P.G=0;f.O.G=w};$.F.8.Y.6l=6(d,e,f){7 h=d.T(\'1r\',\'1z\').H();f.U.R(6(a,b,c){$.F.8.12(a,b,c);c.P.H=b.W;c.O.J=a.W});f.9.J=h;f.9.G=0;f.P.J=0;f.O.J=h};$.F.8.Y.6m=6(d,e,f){7 h=d.T(\'1r\',\'1z\').H();7 w=d.I();f.U.R(6(a,b,c){$.F.8.12(a,b,c);c.P.H=b.W;c.O.J=a.W});f.9.J=h;f.9.G=w;f.P.J=0;f.P.G=0;f.O.J=h;f.O.G=w};$.F.8.Y.6n=6(d,e,f){f.U.R(6(a,b,c){$.F.8.12(a,b,c,N,L);c.9.G=K.X/2;c.P.G=0;c.P.I=K.X;c.O.G=0});f.9.J=0;f.9.I=0};$.F.8.Y.6o=6(d,e,f){f.U.R(6(a,b,c){$.F.8.12(a,b,c,L,N);c.9.J=K.W/2;c.P.J=0;c.P.H=K.W;c.O.J=0});f.9.H=0;f.9.G=0};$.F.8.Y.6p=6(d,e,f){f.U.R(6(a,b,c){$.F.8.12(a,b,c,N,L,L);c.9.G=b.X/2;c.P.G=0;c.P.I=K.X;c.O.G=a.X/2;c.O.I=0});f.9.J=0;f.9.I=0};$.F.8.Y.6q=6(d,e,f){f.U.R(6(a,b,c){$.F.8.12(a,b,c,L,N,L);c.9.J=b.W/2;c.P.J=0;c.P.H=b.W;c.O.J=a.W/2;c.O.H=0});f.9.H=0;f.9.G=0};$.F.8.Y.6r=6(e,f,g){7 d=g.4t||\'G\';7 w=e.T(\'1r\',\'1z\').I();7 h=e.H();g.U.R(6(a,b,c){$.F.8.12(a,b,c);4(d==\'4u\')c.9.G=-w;Q 4(d==\'4v\')c.9.J=h;Q 4(d==\'4w\')c.9.J=-h;Q c.9.G=w});g.P.G=0;g.P.J=0;g.9.J=0;g.9.G=0};$.F.8.Y.6s=6(e,f,g){7 d=g.4t||\'G\';7 w=e.T(\'1r\',\'1z\').I();7 h=e.H();g.U.R(6(a,b,c){$.F.8.12(a,b,c,L,L,L);4(d==\'4u\')c.O.G=w;Q 4(d==\'4v\')c.O.J=-h;Q 4(d==\'4w\')c.O.J=h;Q c.O.G=-w});g.P.G=0;g.P.J=0;g.9.J=0;g.9.G=0};$.F.8.Y.6t=6(d,e,f){7 w=d.T(\'1r\',\'4p\').I();7 h=d.H();f.U.R(6(a,b,c){$.F.8.12(a,b,c,L,L,L);4(!c.O.G&&!c.O.J)$.1g(c.O,{G:w*2,J:-h/2,18:0});Q c.O.18=0});f.9.G=0;f.9.J=0;f.P.G=0};$.F.8.Y.6u=6(n,o,p){7 w=n.T(\'1r\',\'1z\').I();7 h=n.H();p.9=p.9||{};7 q;4(p.1H){4(/6v/.25(p.1H))q=\'26(1A 1A \'+h+\'1a 1A)\';Q 4(/6w/.25(p.1H))q=\'26(1A \'+w+\'1a \'+h+\'1a \'+w+\'1a)\';Q 4(/6x/.25(p.1H))q=\'26(1A \'+w+\'1a 1A 1A)\';Q 4(/6y/.25(p.1H))q=\'26(\'+h+\'1a \'+w+\'1a \'+h+\'1a 1A)\';Q 4(/4s/.25(p.1H)){7 s=1j(h/2,10);7 u=1j(w/2,10);q=\'26(\'+s+\'1a \'+u+\'1a \'+s+\'1a \'+u+\'1a)\'}}p.9.1H=p.9.1H||q||\'26(1A 1A 1A 1A)\';7 d=p.9.1H.4m(/(\\d+)/g);7 t=1j(d[0],10),r=1j(d[1],10),b=1j(d[2],10),l=1j(d[3],10);p.U.R(6(g,i,j){4(g==i)M;7 k=$(g),$14=$(i);$.F.8.12(g,i,j,L,L,N);j.1s.1P=\'2f\';7 m=1,2g=1j((j.2c/13),10)-1;(6 f(){7 a=t?t-1j(m*(t/2g),10):0;7 c=l?l-1j(m*(l/2g),10):0;7 d=b<h?b+1j(m*((h-b)/2g||1),10):h;7 e=r<w?r+1j(m*((w-r)/2g||1),10):w;$14.T({1H:\'26(\'+a+\'1a \'+e+\'1a \'+d+\'1a \'+c+\'1a)\'});(m++<=2g)?2v(f,13):k.T(\'1P\',\'2n\')})()});$.1g(p.9,{1P:\'2f\',18:1,J:0,G:0});p.P={G:0};p.O={G:0}}})(4o);',62,407,'||||if||function|var|cycle|cssBefore||||||||||||||||||||||||||||||||fn|left|height|width|top|this|true|return|false|animOut|animIn|else|push|length|css|before|nextSlide|cycleH|cycleW|transitions|null||currSlide|commonReset||next||||opacity||px|cycleTimeout|startingSlide|timeout|fx|each|extend|speed|log|parseInt|cyclePause|not|after|randomIndex|backwards|random|fxs|overflow|cssAfter|cssFirst|original|elements|randomMap|els|curr|hidden|0px|for|slideCount|debug|isFunction|fit|speedOut|clip|cycleStop|go|data|pager|triggerPause|prev|tx|display|support|updateActivePagerLink|clearTimeout|opts|case|pagerAnchorBuilder|cont|Math|hide|shuffle||easeIn|easeOut|txs|lastFx|test|rect|destroy||advance|maxh|bind|speedIn|multiFx|fxFn|block|count|continuous|index|cleartype|prevNextEvent|pagerAnchors|aspect|none|busy|zIndex|browser|stop|slideshow|terminating|found|setTimeout|oneTimeFx|unbind|stopCount|position|auto|show|margin|slideResize|attr|requeueAttempts|complete|eq|sync|transition|apply|nowrap|fastOnEvent|pagerEvent|click|hex|animate|rev|console|paused|slideExpr|getTimeout|constructor|checkInstantResume|pause|options||slide|typeof|removeFilter||mouseenter|autostop||countdown|autostopCount|cleartypeNoBg|clearTypeFix|sort|center|250|easing|lastSlide|custom|hasOwnProperty|unshift|createPagerAnchor|manualTrump|bounce|end|queueNext|activePagerClass|timeoutFn|background|color|len|use|strict|msie|call|selector|isReady|DOM|ready|handleArguments|children|buildOptions|delay|resumed|String|resume|can|style|filter|mouseleave|defaults|metadata|metaAttr|saveOriginalOpts|absolute|containerResize|offsetWidth|offsetHeight|supportMultiTransitions|img|requeueOnImageNotLoaded|src|requeueTimeout|while|unknown||skipInitializationCallbacks|buildPager|exposeAddSlide|indexOf|splice|in|randomizeEffects|floor|appendTo|onAddSlide|resetState|active|onPrevNextEvent|onPagerEvent|hover|allowPagerClickBubble|pauseOnPagerHover|hopsFromLast|getBg|match|fade|jQuery|visible|speedAdjusted|shift|zoom|direction|right|up|down|9999|window|Array|prototype|join|arguments|expr|context|queuing|zero|by|get|too|few|slides|first|switch|removeData|toggle|ignored|default|Number|invalid|string|try|removeAttribute|catch|smother|remove|meta|static|relative|startSlide|innerHeight|outerWidth|outerHeight|is|mozilla|opera|100|loaded|requeuing|could|determine|size|of|image|speeds|500|replace|split|discarding|No|valid|named|all|randomized|sequence|addSlide|prependTo|stopping|ignoring|new|request|firing|removeClass|addClass|calculated|prevNextClick|el|returned|href|parents|body|clone|append|preventDefault|pagerClick|mouseover|applying|clearType|hack|toString|nodeName|toLowerCase|html|parentNode|rgb|transparent|ffffff|undefined|number|ver|activeSlide|1000|4000|fadeout|scrollUp|scrollDown|scrollLeft|scrollRight|scrollHorz|scrollVert|slideX|slideY|pop|turnUp|turnDown|turnLeft|turnRight|fadeZoom|blindX|blindY|blindZ|growX|growY|curtainX|curtainY|cover|uncover|toss|wipe|l2r|r2l|t2b|b2t'.split('|'),0,{}));

/*
 * Pixastic - JavaScript Image Processing Library
 * Copyright (c) 2008 Jacob Seidelin, jseidelin@nihilogic.dk, http://blog.nihilogic.dk/
 * MIT License [http://www.pixastic.com/lib/license.txt]
 */
var Pixastic=(function(){function addEvent(el,event,handler){if(el.addEventListener)
el.addEventListener(event,handler,false);else if(el.attachEvent)
el.attachEvent("on"+event,handler);}
function onready(handler){var handlerDone=false;var execHandler=function(){if(!handlerDone){handlerDone=true;handler();}}
document.write("<"+"script defer src=\"//:\" id=\"__onload_ie_pixastic__\"></"+"script>");var script=document.getElementById("__onload_ie_pixastic__");script.onreadystatechange=function(){if(script.readyState=="complete"){script.parentNode.removeChild(script);execHandler();}}
if(document.addEventListener)
document.addEventListener("DOMContentLoaded",execHandler,false);addEvent(window,"load",execHandler);}
function init(){var imgEls=getElementsByClass("pixastic",null,"img");var canvasEls=getElementsByClass("pixastic",null,"canvas");var elements=imgEls.concat(canvasEls);for(var i=0;i<elements.length;i++){(function(){var el=elements[i];var actions=[];var classes=el.className.split(" ");for(var c=0;c<classes.length;c++){var cls=classes[c];if(cls.substring(0,9)=="pixastic-"){var actionName=cls.substring(9);if(actionName!="")
actions.push(actionName);}}
if(actions.length){if(el.tagName.toLowerCase()=="img"){var dataImg=new Image();dataImg.src=el.src;if(dataImg.complete){for(var a=0;a<actions.length;a++){var res=Pixastic.applyAction(el,el,actions[a],null);if(res)
el=res;}}else{dataImg.onload=function(){for(var a=0;a<actions.length;a++){var res=Pixastic.applyAction(el,el,actions[a],null)
if(res)
el=res;}}}}else{setTimeout(function(){for(var a=0;a<actions.length;a++){var res=Pixastic.applyAction(el,el,actions[a],null);if(res)
el=res;}},1);}}})();}}
if(typeof pixastic_parseonload!="undefined"&&pixastic_parseonload)
onready(init);function getElementsByClass(searchClass,node,tag){var classElements=new Array();if(node==null)
node=document;if(tag==null)
tag='*';var els=node.getElementsByTagName(tag);var elsLen=els.length;var pattern=new RegExp("(^|\\s)"+searchClass+"(\\s|$)");for(i=0,j=0;i<elsLen;i++){if(pattern.test(els[i].className)){classElements[j]=els[i];j++;}}
return classElements;}
var debugElement;function writeDebug(text,level){if(!Pixastic.debug)return;try{switch(level){case"warn":console.warn("Pixastic:",text);break;case"error":console.error("Pixastic:",text);break;default:console.log("Pixastic:",text);}}catch(e){}
if(!debugElement){}}
var hasCanvas=(function(){var c=document.createElement("canvas");var val=false;try{val=!!((typeof c.getContext=="function")&&c.getContext("2d"));}catch(e){}
return function(){return val;}})();var hasCanvasImageData=(function(){var c=document.createElement("canvas");var val=false;var ctx;try{if(typeof c.getContext=="function"&&(ctx=c.getContext("2d"))){val=(typeof ctx.getImageData=="function");}}catch(e){}
return function(){return val;}})();var hasGlobalAlpha=(function(){var hasAlpha=false;var red=document.createElement("canvas");if(hasCanvas()&&hasCanvasImageData()){red.width=red.height=1;var redctx=red.getContext("2d");redctx.fillStyle="rgb(255,0,0)";redctx.fillRect(0,0,1,1);var blue=document.createElement("canvas");blue.width=blue.height=1;var bluectx=blue.getContext("2d");bluectx.fillStyle="rgb(0,0,255)";bluectx.fillRect(0,0,1,1);redctx.globalAlpha=0.5;redctx.drawImage(blue,0,0);var reddata=redctx.getImageData(0,0,1,1).data;hasAlpha=(reddata[2]!=255);}
return function(){return hasAlpha;}})();return{parseOnLoad:false,debug:false,applyAction:function(img,dataImg,actionName,options){options=options||{};var imageIsCanvas=(img.tagName.toLowerCase()=="canvas");if(imageIsCanvas&&Pixastic.Client.isIE()){if(Pixastic.debug)writeDebug("Tried to process a canvas element but browser is IE.");return false;}
var canvas,ctx;var hasOutputCanvas=false;if(Pixastic.Client.hasCanvas()){hasOutputCanvas=!!options.resultCanvas;canvas=options.resultCanvas||document.createElement("canvas");ctx=canvas.getContext("2d");}
var w=img.offsetWidth;var h=img.offsetHeight;if(imageIsCanvas){w=img.width;h=img.height;}
if(w==0||h==0){if(img.parentNode==null){var oldpos=img.style.position;var oldleft=img.style.left;img.style.position="absolute";img.style.left="-9999px";document.body.appendChild(img);w=img.offsetWidth;h=img.offsetHeight;document.body.removeChild(img);img.style.position=oldpos;img.style.left=oldleft;}else{if(Pixastic.debug)writeDebug("Image has 0 width and/or height.");return;}}
if(actionName.indexOf("(")>-1){var tmp=actionName;actionName=tmp.substr(0,tmp.indexOf("("));var arg=tmp.match(/\((.*?)\)/);if(arg[1]){arg=arg[1].split(";");for(var a=0;a<arg.length;a++){thisArg=arg[a].split("=");if(thisArg.length==2){if(thisArg[0]=="rect"){var rectVal=thisArg[1].split(",");options[thisArg[0]]={left:parseInt(rectVal[0],10)||0,top:parseInt(rectVal[1],10)||0,width:parseInt(rectVal[2],10)||0,height:parseInt(rectVal[3],10)||0}}else{options[thisArg[0]]=thisArg[1];}}}}}
if(!options.rect){options.rect={left:0,top:0,width:w,height:h};}else{options.rect.left=Math.round(options.rect.left);options.rect.top=Math.round(options.rect.top);options.rect.width=Math.round(options.rect.width);options.rect.height=Math.round(options.rect.height);}
var validAction=false;if(Pixastic.Actions[actionName]&&typeof Pixastic.Actions[actionName].process=="function"){validAction=true;}
if(!validAction){if(Pixastic.debug)writeDebug("Invalid action \""+actionName+"\". Maybe file not included?");return false;}
if(!Pixastic.Actions[actionName].checkSupport()){if(Pixastic.debug)writeDebug("Action \""+actionName+"\" not supported by this browser.");return false;}
if(Pixastic.Client.hasCanvas()){if(canvas!==img){canvas.width=w;canvas.height=h;}
if(!hasOutputCanvas){canvas.style.width=w+"px";canvas.style.height=h+"px";}
ctx.drawImage(dataImg,0,0,w,h);if(!img.__pixastic_org_image){canvas.__pixastic_org_image=img;canvas.__pixastic_org_width=w;canvas.__pixastic_org_height=h;}else{canvas.__pixastic_org_image=img.__pixastic_org_image;canvas.__pixastic_org_width=img.__pixastic_org_width;canvas.__pixastic_org_height=img.__pixastic_org_height;}}else if(Pixastic.Client.isIE()&&typeof img.__pixastic_org_style=="undefined"){img.__pixastic_org_style=img.style.cssText;}
var params={image:img,canvas:canvas,width:w,height:h,useData:true,options:options}
var res=Pixastic.Actions[actionName].process(params);if(!res){return false;}
if(Pixastic.Client.hasCanvas()){if(params.useData){if(Pixastic.Client.hasCanvasImageData()){canvas.getContext("2d").putImageData(params.canvasData,options.rect.left,options.rect.top);canvas.getContext("2d").fillRect(0,0,0,0);}}
if(!options.leaveDOM){canvas.title=img.title;canvas.imgsrc=img.imgsrc;if(!imageIsCanvas)canvas.alt=img.alt;if(!imageIsCanvas)canvas.imgsrc=img.src;canvas.className=img.className;canvas.style.cssText=img.style.cssText;canvas.name=img.name;canvas.tabIndex=img.tabIndex;canvas.id=img.id;if(img.parentNode&&img.parentNode.replaceChild){img.parentNode.replaceChild(canvas,img);}}
options.resultCanvas=canvas;return canvas;}
return img;},prepareData:function(params,getCopy){var ctx=params.canvas.getContext("2d");var rect=params.options.rect;var dataDesc=ctx.getImageData(rect.left,rect.top,rect.width,rect.height);var data=dataDesc.data;if(!getCopy)params.canvasData=dataDesc;return data;},process:function(img,actionName,options,callback){if(img.tagName.toLowerCase()=="img"){var dataImg=new Image();dataImg.src=img.src;if(dataImg.complete){var res=Pixastic.applyAction(img,dataImg,actionName,options);if(callback)callback(res);return res;}else{dataImg.onload=function(){var res=Pixastic.applyAction(img,dataImg,actionName,options)
if(callback)callback(res);}}}
if(img.tagName.toLowerCase()=="canvas"){var res=Pixastic.applyAction(img,img,actionName,options);if(callback)callback(res);return res;}},revert:function(img){if(Pixastic.Client.hasCanvas()){if(img.tagName.toLowerCase()=="canvas"&&img.__pixastic_org_image){img.width=img.__pixastic_org_width;img.height=img.__pixastic_org_height;img.getContext("2d").drawImage(img.__pixastic_org_image,0,0);if(img.parentNode&&img.parentNode.replaceChild){img.parentNode.replaceChild(img.__pixastic_org_image,img);}
return img;}}else if(Pixastic.Client.isIE()){if(typeof img.__pixastic_org_style!="undefined")
img.style.cssText=img.__pixastic_org_style;}},Client:{hasCanvas:hasCanvas,hasCanvasImageData:hasCanvasImageData,hasGlobalAlpha:hasGlobalAlpha,isIE:function(){return!!document.all&&!!window.attachEvent&&!window.opera;}},Actions:{}}})();if(typeof jQuery!="undefined"&&jQuery&&jQuery.fn){jQuery.fn.pixastic=function(action,options){var newElements=[];this.each(function(){if(this.tagName.toLowerCase()=="img"&&!this.complete){return;}
var res=Pixastic.process(this,action,options);if(res){newElements.push(res);}});if(newElements.length>0)
return jQuery(newElements);else
return this;};};Pixastic.Actions.desaturate={process:function(params){var useAverage=!!(params.options.average&&params.options.average!="false");if(Pixastic.Client.hasCanvasImageData()){var data=Pixastic.prepareData(params);var rect=params.options.rect;var w=rect.width;var h=rect.height;var p=w*h;var pix=p*4,pix1,pix2;if(useAverage){while(p--)
data[pix-=4]=data[pix1=pix+1]=data[pix2=pix+2]=(data[pix]+data[pix1]+data[pix2])/3}else{while(p--)
data[pix-=4]=data[pix1=pix+1]=data[pix2=pix+2]=(data[pix]*0.3+data[pix1]*0.59+data[pix2]*0.11);}
return true;}else if(Pixastic.Client.isIE()){params.image.style.filter+=" gray";return true;}},checkSupport:function(){return(Pixastic.Client.hasCanvasImageData()||Pixastic.Client.isIE());}}
Pixastic.Actions.hsl={process:function(params){var hue=parseInt(params.options.hue,10)||0;var saturation=(parseInt(params.options.saturation,10)||0)/100;var lightness=(parseInt(params.options.lightness,10)||0)/100;if(saturation<0){var satMul=1+saturation;}else{var satMul=1+saturation*2;}
hue=(hue%360)/360;var hue6=hue*6;var rgbDiv=1/255;var light255=lightness*255;var lightp1=1+lightness;var lightm1=1-lightness;if(Pixastic.Client.hasCanvasImageData()){var data=Pixastic.prepareData(params);var rect=params.options.rect;var p=rect.width*rect.height;var pix=p*4,pix1=pix+1,pix2=pix+2,pix3=pix+3;while(p--){var r=data[pix-=4];var g=data[pix1=pix+1];var b=data[pix2=pix+2];if(hue!=0||saturation!=0){var vs=r;if(g>vs)vs=g;if(b>vs)vs=b;var ms=r;if(g<ms)ms=g;if(b<ms)ms=b;var vm=(vs-ms);var l=(ms+vs)/510;if(l>0){if(vm>0){if(l<=0.5){var s=vm/(vs+ms)*satMul;if(s>1)s=1;var v=(l*(1+s));}else{var s=vm/(510-vs-ms)*satMul;if(s>1)s=1;var v=(l+s-l*s);}
if(r==vs){if(g==ms)
var h=5+((vs-b)/vm)+hue6;else
var h=1-((vs-g)/vm)+hue6;}else if(g==vs){if(b==ms)
var h=1+((vs-r)/vm)+hue6;else
var h=3-((vs-b)/vm)+hue6;}else{if(r==ms)
var h=3+((vs-g)/vm)+hue6;else
var h=5-((vs-r)/vm)+hue6;}
if(h<0)h+=6;if(h>=6)h-=6;var m=(l+l-v);var sextant=h>>0;if(sextant==0){r=v*255;g=(m+((v-m)*(h-sextant)))*255;b=m*255;}else if(sextant==1){r=(v-((v-m)*(h-sextant)))*255;g=v*255;b=m*255;}else if(sextant==2){r=m*255;g=v*255;b=(m+((v-m)*(h-sextant)))*255;}else if(sextant==3){r=m*255;g=(v-((v-m)*(h-sextant)))*255;b=v*255;}else if(sextant==4){r=(m+((v-m)*(h-sextant)))*255;g=m*255;b=v*255;}else if(sextant==5){r=v*255;g=m*255;b=(v-((v-m)*(h-sextant)))*255;}}}}
if(lightness<0){r*=lightp1;g*=lightp1;b*=lightp1;}else if(lightness>0){r=r*lightm1+light255;g=g*lightm1+light255;b=b*lightm1+light255;}
if(r<0)
data[pix]=0
else if(r>255)
data[pix]=255
else
data[pix]=r;if(g<0)
data[pix1]=0
else if(g>255)
data[pix1]=255
else
data[pix1]=g;if(b<0)
data[pix2]=0
else if(b>255)
data[pix2]=255
else
data[pix2]=b;}
return true;}},checkSupport:function(){return Pixastic.Client.hasCanvasImageData();}}

/**
 * jGestures: a jQuery plugin for gesture events
 * Copyright 2010-2011 Neue Digitale / Razorfish GmbH
 * Copyright 2011-2012, Razorfish GmbH
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * @copyright Razorfish GmbH
 * @author martin.krause@razorfish.de
 * @version 0.90-shake
 * @requires jQuery JavaScript Library v1.4.2 - http://jquery.com/- Copyright 2010, John Resig- Dual licensed under the MIT or GPL Version 2 licenses. http://jquery.org/license
 */
;(function(c){c.jGestures={};c.jGestures.defaults={};c.jGestures.defaults.thresholdShake={requiredShakes:10,freezeShakes:100,frontback:{sensitivity:10},leftright:{sensitivity:10},updown:{sensitivity:10}};c.jGestures.defaults.thresholdPinchopen=0.05;c.jGestures.defaults.thresholdPinchmove=0.05;c.jGestures.defaults.thresholdPinch=0.05;c.jGestures.defaults.thresholdPinchclose=0.05;c.jGestures.defaults.thresholdRotatecw=5;c.jGestures.defaults.thresholdRotateccw=5;c.jGestures.defaults.thresholdMove=20;c.jGestures.defaults.thresholdSwipe=100;c.jGestures.data={};c.jGestures.data.capableDevicesInUserAgentString=["iPad","iPhone","iPod","Mobile Safari"];c.jGestures.data.hasGestures=(function(){var k;for(k=0;k<c.jGestures.data.capableDevicesInUserAgentString.length;k++){if(navigator.userAgent.indexOf(c.jGestures.data.capableDevicesInUserAgentString[k])!==-1){return true}}return false})();c.hasGestures=c.jGestures.data.hasGestures;c.jGestures.events={touchstart:"jGestures.touchstart",touchendStart:"jGestures.touchend;start",touchendProcessed:"jGestures.touchend;processed",gesturestart:"jGestures.gesturestart",gestureendStart:"jGestures.gestureend;start",gestureendProcessed:"jGestures.gestureend;processed"};jQuery.each({orientationchange_orientationchange01:"orientationchange",gestureend_pinchopen01:"pinchopen",gestureend_pinchclose01:"pinchclose",gestureend_rotatecw01:"rotatecw",gestureend_rotateccw01:"rotateccw",gesturechange_pinch01:"pinch",gesturechange_rotate01:"rotate",touchstart_swipe13:"swipemove",touchstart_swipe01:"swipeone",touchstart_swipe02:"swipetwo",touchstart_swipe03:"swipethree",touchstart_swipe04:"swipefour",touchstart_swipe05:"swipeup",touchstart_swipe06:"swiperightup",touchstart_swipe07:"swiperight",touchstart_swipe08:"swiperightdown",touchstart_swipe09:"swipedown",touchstart_swipe10:"swipeleftdown",touchstart_swipe11:"swipeleft",touchstart_swipe12:"swipeleftup",touchstart_tap01:"tapone",touchstart_tap02:"taptwo",touchstart_tap03:"tapthree",touchstart_tap04:"tapfour",devicemotion_shake01:"shake",devicemotion_shake02:"shakefrontback",devicemotion_shake03:"shakeleftright",devicemotion_shake04:"shakeupdown"},function(l,k){jQuery.event.special[k]={setup:function(){var r=l.split("_");var o=r[0];var m=r[1].slice(0,r[1].length-2);var p=jQuery(this);var q;var n;if(!p.data("ojQueryGestures")||!p.data("ojQueryGestures")[o]){q=p.data("ojQueryGestures")||{};n={};n[o]=true;c.extend(true,q,n);p.data("ojQueryGestures",q);if(c.hasGestures){switch(m){case"orientationchange":p.get(0).addEventListener("orientationchange",a,false);break;case"shake":case"shakefrontback":case"shakeleftright":case"shakeupdown":case"tilt":p.get(0).addEventListener("devicemotion",b,false);break;case"tap":case"swipe":case"swipeup":case"swiperightup":case"swiperight":case"swiperightdown":case"swipedown":case"swipeleftdown":case"swipeleft":p.get(0).addEventListener("touchstart",h,false);break;case"pinchopen":case"pinchclose":case"rotatecw":case"rotateccw":p.get(0).addEventListener("gesturestart",e,false);p.get(0).addEventListener("gestureend",i,false);break;case"pinch":case"rotate":p.get(0).addEventListener("gesturestart",e,false);p.get(0).addEventListener("gesturechange",f,false);break}}else{switch(m){case"tap":case"swipe":p.bind("mousedown",h);break;case"orientationchange":case"pinchopen":case"pinchclose":case"rotatecw":case"rotateccw":case"pinch":case"rotate":case"shake":case"tilt":break}}}return false},add:function(n){var m=jQuery(this);var o=m.data("ojQueryGestures");o[n.type]={originalType:n.type};return false},remove:function(n){var m=jQuery(this);var o=m.data("ojQueryGestures");o[n.type]=false;m.data("ojQueryGestures",o);return false},teardown:function(){var r=l.split("_");var o=r[0];var m=r[1].slice(0,r[1].length-2);var p=jQuery(this);var q;var n;if(!p.data("ojQueryGestures")||!p.data("ojQueryGestures")[o]){q=p.data("ojQueryGestures")||{};n={};n[o]=false;c.extend(true,q,n);p.data("ojQueryGestures",q);if(c.hasGestures){switch(m){case"orientationchange":p.get(0).removeEventListener("orientationchange",a,false);break;case"shake":case"shakefrontback":case"shakeleftright":case"shakeupdown":case"tilt":p.get(0).removeEventListener("devicemotion",b,false);break;case"tap":case"swipe":case"swipeup":case"swiperightup":case"swiperight":case"swiperightdown":case"swipedown":case"swipeleftdown":case"swipeleft":case"swipeleftup":p.get(0).removeEventListener("touchstart",h,false);p.get(0).removeEventListener("touchmove",g,false);p.get(0).removeEventListener("touchend",j,false);break;case"pinchopen":case"pinchclose":case"rotatecw":case"rotateccw":p.get(0).removeEventListener("gesturestart",e,false);p.get(0).removeEventListener("gestureend",i,false);break;case"pinch":case"rotate":p.get(0).removeEventListener("gesturestart",e,false);p.get(0).removeEventListener("gesturechange",f,false);break}}else{switch(m){case"tap":case"swipe":p.unbind("mousedown",h);p.unbind("mousemove",g);p.unbind("mouseup",j);break;case"orientationchange":case"pinchopen":case"pinchclose":case"rotatecw":case"rotateccw":case"pinch":case"rotate":case"shake":case"tilt":break}}}return false}}});function d(k){k.startMove=(k.startMove)?k.startMove:{startX:null,startY:null,timestamp:null};var l=new Date().getTime();var m;var n;if(k.touches){n=[{lastX:k.deltaX,lastY:k.deltaY,moved:null,startX:k.screenX-k.startMove.screenX,startY:k.screenY-k.startMove.screenY}];m={vector:k.vector||null,orientation:window.orientation||null,lastX:((n[0].lastX>0)?+1:((n[0].lastX<0)?-1:0)),lastY:((n[0].lastY>0)?+1:((n[0].lastY<0)?-1:0)),startX:((n[0].startX>0)?+1:((n[0].startX<0)?-1:0)),startY:((n[0].startY>0)?+1:((n[0].startY<0)?-1:0))};n[0].moved=Math.sqrt(Math.pow(Math.abs(n[0].startX),2)+Math.pow(Math.abs(n[0].startY),2))}return{type:k.type||null,originalEvent:k.event||null,delta:n||null,direction:m||{orientation:window.orientation||null,vector:k.vector||null},duration:(k.duration)?k.duration:(k.startMove.timestamp)?l-k.timestamp:null,rotation:k.rotation||null,scale:k.scale||null,description:k.description||[k.type,":",k.touches,":",((n[0].lastX!=0)?((n[0].lastX>0)?"right":"left"):"steady"),":",((n[0].lastY!=0)?((n[0].lastY>0)?"down":"up"):"steady")].join("")}}function a(l){var k=["landscape:clockwise:","portrait:default:","landscape:counterclockwise:","portrait:upsidedown:"];c(window).triggerHandler("orientationchange",{direction:{orientation:window.orientation},description:["orientationchange:",k[((window.orientation/90)+1)],window.orientation].join("")})}function b(r){var k;var t=jQuery(window);var o=t.data("ojQueryGestures");var m=c.jGestures.defaults.thresholdShake;var n=o.oDeviceMotionLastDevicePosition||{accelerationIncludingGravity:{x:0,y:0,z:0},shake:{eventCount:0,intervalsPassed:0,intervalsFreeze:0},shakeleftright:{eventCount:0,intervalsPassed:0,intervalsFreeze:0},shakefrontback:{eventCount:0,intervalsPassed:0,intervalsFreeze:0},shakeupdown:{eventCount:0,intervalsPassed:0,intervalsFreeze:0}};var q={accelerationIncludingGravity:{x:r.accelerationIncludingGravity.x,y:r.accelerationIncludingGravity.y,z:r.accelerationIncludingGravity.z},shake:{eventCount:n.shake.eventCount,intervalsPassed:n.shake.intervalsPassed,intervalsFreeze:n.shake.intervalsFreeze},shakeleftright:{eventCount:n.shakeleftright.eventCount,intervalsPassed:n.shakeleftright.intervalsPassed,intervalsFreeze:n.shakeleftright.intervalsFreeze},shakefrontback:{eventCount:n.shakefrontback.eventCount,intervalsPassed:n.shakefrontback.intervalsPassed,intervalsFreeze:n.shakefrontback.intervalsFreeze},shakeupdown:{eventCount:n.shakeupdown.eventCount,intervalsPassed:n.shakeupdown.intervalsPassed,intervalsFreeze:n.shakeupdown.intervalsFreeze}};var p;var s;var l;for(k in o){switch(k){case"shake":case"shakeleftright":case"shakefrontback":case"shakeupdown":p=[];s=[];p.push(k);if(++q[k].intervalsFreeze>m.freezeShakes&&q[k].intervalsFreeze<(2*m.freezeShakes)){break}q[k].intervalsFreeze=0;q[k].intervalsPassed++;if((k==="shake"||k==="shakeleftright")&&(q.accelerationIncludingGravity.x>m.leftright.sensitivity||q.accelerationIncludingGravity.x<(-1*m.leftright.sensitivity))){p.push("leftright");p.push("x-axis")}if((k==="shake"||k==="shakefrontback")&&(q.accelerationIncludingGravity.y>m.frontback.sensitivity||q.accelerationIncludingGravity.y<(-1*m.frontback.sensitivity))){p.push("frontback");p.push("y-axis")}if((k==="shake"||k==="shakeupdown")&&(q.accelerationIncludingGravity.z+9.81>m.updown.sensitivity||q.accelerationIncludingGravity.z+9.81<(-1*m.updown.sensitivity))){p.push("updown");p.push("z-axis")}if(p.length>1){if(++q[k].eventCount==m.requiredShakes&&(q[k].intervalsPassed)<m.freezeShakes){t.triggerHandler(k,d({type:k,description:p.join(":"),event:r,duration:q[k].intervalsPassed*5}));q[k].eventCount=0;q[k].intervalsPassed=0;q[k].intervalsFreeze=m.freezeShakes+1}else{if(q[k].eventCount==m.requiredShakes&&(q[k].intervalsPassed)>m.freezeShakes){q[k].eventCount=0;q[k].intervalsPassed=0}}}break}l={};l.oDeviceMotionLastDevicePosition=q;t.data("ojQueryGestures",c.extend(true,o,l))}}function h(l){var k=jQuery(l.currentTarget);k.triggerHandler(c.jGestures.events.touchstart,l);if(c.hasGestures){l.currentTarget.addEventListener("touchmove",g,false);l.currentTarget.addEventListener("touchend",j,false)}else{k.bind("mousemove",g);k.bind("mouseup",j)}var n=k.data("ojQueryGestures");var m=(l.touches)?l.touches[0]:l;var o={};o.oLastSwipemove={screenX:m.screenX,screenY:m.screenY,timestamp:new Date().getTime()};o.oStartTouch={screenX:m.screenX,screenY:m.screenY,timestamp:new Date().getTime()};k.data("ojQueryGestures",c.extend(true,n,o))}function g(t){var v=jQuery(t.currentTarget);var s=v.data("ojQueryGestures");var q=!!t.touches;var l=(q)?t.changedTouches[0].screenX:t.screenX;var k=(q)?t.changedTouches[0].screenY:t.screenY;var r=s.oLastSwipemove;var o=l-r.screenX;var n=k-r.screenY;var u;if(!!s.oLastSwipemove){u=d({type:"swipemove",touches:(q)?t.touches.length:"1",screenY:k,screenX:l,deltaY:n,deltaX:o,startMove:r,event:t,timestamp:r.timestamp});v.triggerHandler(u.type,u)}var m={};var p=(t.touches)?t.touches[0]:t;m.oLastSwipemove={screenX:p.screenX,screenY:p.screenY,timestamp:new Date().getTime()};v.data("ojQueryGestures",c.extend(true,s,m))}function j(r){var v=jQuery(r.currentTarget);var x=!!r.changedTouches;var u=(x)?r.changedTouches.length:"1";var p=(x)?r.changedTouches[0].screenX:r.screenX;var o=(x)?r.changedTouches[0].screenY:r.screenY;v.triggerHandler(c.jGestures.events.touchendStart,r);if(c.hasGestures){r.currentTarget.removeEventListener("touchmove",g,false);r.currentTarget.removeEventListener("touchend",j,false)}else{v.unbind("mousemove",g);v.unbind("mouseup",j)}var m=v.data("ojQueryGestures");var y=(Math.abs(m.oStartTouch.screenX-p)>c.jGestures.defaults.thresholdMove||Math.abs(m.oStartTouch.screenY-o)>c.jGestures.defaults.thresholdMove)?true:false;var B=(Math.abs(m.oStartTouch.screenX-p)>c.jGestures.defaults.thresholdSwipe||Math.abs(m.oStartTouch.screenY-o)>c.jGestures.defaults.thresholdSwipe)?true:false;var A;var t;var n;var l;var k;var q;var w=["zero","one","two","three","four"];var s;for(A in m){t=m.oStartTouch;n={};p=(x)?r.changedTouches[0].screenX:r.screenX;o=(x)?r.changedTouches[0].screenY:r.screenY;l=p-t.screenX;k=o-t.screenY;q=d({type:"swipe",touches:u,screenY:o,screenX:p,deltaY:k,deltaX:l,startMove:t,event:r,timestamp:t.timestamp});s=false;switch(A){case"swipeone":if(x===false&&u==1&&y===false){break}if(x===false||(u==1&&y===true&&B===true)){s=true;q.type=["swipe",w[u]].join("");v.triggerHandler(q.type,q)}break;case"swipetwo":if((x&&u==2&&y===true&&B===true)){s=true;q.type=["swipe",w[u]].join("");v.triggerHandler(q.type,q)}break;case"swipethree":if((x&&u==3&&y===true&&B===true)){s=true;q.type=["swipe",w[u]].join("");v.triggerHandler(q.type,q)}break;case"swipefour":if((x&&u==4&&y===true&&B===true)){s=true;q.type=["swipe",w[u]].join("");v.triggerHandler(q.type,q)}break;case"swipeup":case"swiperightup":case"swiperight":case"swiperightdown":case"swipedown":case"swipeleftdown":case"swipeleft":case"swipeleftup":if(x&&y===true&&B===true){s=true;q.type=["swipe",((q.delta[0].lastX!=0)?((q.delta[0].lastX>0)?"right":"left"):""),((q.delta[0].lastY!=0)?((q.delta[0].lastY>0)?"down":"up"):"")].join("");v.triggerHandler(q.type,q)}break;case"tapone":case"taptwo":case"tapthree":case"tapfour":if((y!==true&&s!==true)&&(w[u]==A.slice(3))){q.description=["tap",w[u]].join("");q.type=["tap",w[u]].join("");v.triggerHandler(q.type,q)}break}var z={};v.data("ojQueryGestures",c.extend(true,m,z));v.data("ojQueryGestures",c.extend(true,m,z))}v.triggerHandler(c.jGestures.events.touchendProcessed,r)}function e(l){var k=jQuery(l.currentTarget);k.triggerHandler(c.jGestures.events.gesturestart,l);var m=k.data("ojQueryGestures");var n={};n.oStartTouch={timestamp:new Date().getTime()};k.data("ojQueryGestures",c.extend(true,m,n))}function f(l){var k=jQuery(l.currentTarget);var p,m,r,o;var q=k.data("ojQueryGestures");var n;for(n in q){switch(n){case"pinch":p=l.scale;if(((p<1)&&(p%1)<(1-c.jGestures.defaults.thresholdPinchclose))||((p>1)&&(p%1)>(c.jGestures.defaults.thresholdPinchopen))){m=(p<1)?-1:+1;o=d({type:"pinch",scale:p,touches:null,startMove:q.oStartTouch,event:l,timestamp:q.oStartTouch.timestamp,vector:m,description:["pinch:",m,":",((p<1)?"close":"open")].join("")});k.triggerHandler(o.type,o)}break;case"rotate":p=l.rotation;if(((p<1)&&(-1*(p)>c.jGestures.defaults.thresholdRotateccw))||((p>1)&&(p>c.jGestures.defaults.thresholdRotatecw))){m=(p<1)?-1:+1;o=d({type:"rotate",rotation:p,touches:null,startMove:q.oStartTouch,event:l,timestamp:q.oStartTouch.timestamp,vector:m,description:["rotate:",m,":",((p<1)?"counterclockwise":"clockwise")].join("")});k.triggerHandler(o.type,o)}break}}}function i(l){var k=jQuery(l.currentTarget);k.triggerHandler(c.jGestures.events.gestureendStart,l);var n;var o=k.data("ojQueryGestures");var m;for(m in o){switch(m){case"pinchclose":n=l.scale;if((n<1)&&(n%1)<(1-c.jGestures.defaults.thresholdPinchclose)){k.triggerHandler("pinchclose",d({type:"pinchclose",scale:n,vector:-1,touches:null,startMove:o.oStartTouch,event:l,timestamp:o.oStartTouch.timestamp,description:"pinch:-1:close"}))}break;case"pinchopen":n=l.scale;if((n>1)&&(n%1)>(c.jGestures.defaults.thresholdPinchopen)){k.triggerHandler("pinchopen",d({type:"pinchopen",scale:n,vector:+1,touches:null,startMove:o.oStartTouch,event:l,timestamp:o.oStartTouch.timestamp,description:"pinch:+1:open"}))}break;case"rotatecw":n=l.rotation;if((n>1)&&(n>c.jGestures.defaults.thresholdRotatecw)){k.triggerHandler("rotatecw",d({type:"rotatecw",rotation:n,vector:+1,touches:null,startMove:o.oStartTouch,event:l,timestamp:o.oStartTouch.timestamp,description:"rotate:+1:clockwise"}))}break;case"rotateccw":n=l.rotation;if((n<1)&&(-1*(n)>c.jGestures.defaults.thresholdRotateccw)){k.triggerHandler("rotateccw",d({type:"rotateccw",rotation:n,vector:-1,touches:null,startMove:o.oStartTouch,event:l,timestamp:o.oStartTouch.timestamp,description:"rotate:-1:counterclockwise"}))}break}}k.triggerHandler(c.jGestures.events.gestureendProcessed,l)}})(jQuery);


/**
 *  Plugin which is applied on a list of img objects and calls
 *  the specified callback function, only when all of them are loaded (or errored).
 *  @author:  H. Yankov (hristo.yankov at gmail dot com)
 *  @version: 1.0.0 (Feb/22/2010)
 *	http://yankov.us
 */
;(function($) {
$.fn.batchImageLoad = function(options) {
	var images = $(this);
	var originalTotalImagesCount = images.size();

	//window.console && console.log('images count: '+originalTotalImagesCount);

	var totalImagesCount = originalTotalImagesCount;
	var elementsLoaded = 0;

	// Init
	$.fn.batchImageLoad.defaults = {
		loadingCompleteCallback: null,
		imageLoadedCallback: null
	}
    var opts = $.extend({}, $.fn.batchImageLoad.defaults, options);

	// Start
	images.each(function(i) {
	//window.console && console.log('inside'+i);
		// The image has already been loaded (cached)
		if ($(this)[0].complete) {
			totalImagesCount--;
			if (opts.imageLoadedCallback) opts.imageLoadedCallback(elementsLoaded, originalTotalImagesCount);
		// The image is loading, so attach the listener
		} else {
			$(this).load(function() {
				elementsLoaded++;

				if (opts.imageLoadedCallback) opts.imageLoadedCallback(elementsLoaded, originalTotalImagesCount);

				// An image has been loaded
				if (elementsLoaded >= totalImagesCount)
					if (opts.loadingCompleteCallback) opts.loadingCompleteCallback();
			});
			$(this).error(function() {
				elementsLoaded++;

				if (opts.imageLoadedCallback) opts.imageLoadedCallback(elementsLoaded, originalTotalImagesCount);

				// The image has errored
				if (elementsLoaded >= totalImagesCount)
					if (opts.loadingCompleteCallback) opts.loadingCompleteCallback();
			});
		}
	});

	// There are no unloaded images
	if (totalImagesCount <= 0)
		window.console && console.log('total count '+totalImagesCount);
		if (opts.loadingCompleteCallback) opts.loadingCompleteCallback();
};
})(jQuery);

/*!
 * jQuery Tools v1.2.7 - The missing UI library for the Web
 *
 * toolbox/toolbox.expose.js
 *
 * NO COPYRIGHTS OR LICENSES. DO WHAT YOU LIKE.
 *
 * http://flowplayer.org/tools/
 *
 */
(function(a){a.tools=a.tools||{version:"v1.2.7"};var b;b=a.tools.expose={conf:{maskId:"exposeMask",loadSpeed:"slow",closeSpeed:"fast",closeOnClick:!0,closeOnEsc:!0,zIndex:9998,opacity:.8,startOpacity:0,color:"#fff",onLoad:null,onClose:null}};function c(){if(a.browser.msie){var b=a(document).height(),c=a(window).height();return[window.innerWidth||document.documentElement.clientWidth||document.body.clientWidth,b-c<20?c:b]}return[a(document).width(),a(document).height()]}function d(b){if(b)return b.call(a.mask)}var e,f,g,h,i;a.mask={load:function(j,k){if(g)return this;typeof j=="string"&&(j={color:j}),j=j||h,h=j=a.extend(a.extend({},b.conf),j),e=a("#"+j.maskId),e.length||(e=a("<div/>").attr("id",j.maskId),a("body").append(e));var l=c();e.css({position:"absolute",top:0,left:0,width:l[0],height:l[1],display:"none",opacity:j.startOpacity,zIndex:j.zIndex}),j.color&&e.css("backgroundColor",j.color);if(d(j.onBeforeLoad)===!1)return this;j.closeOnEsc&&a(document).on("keydown.mask",function(b){b.keyCode==27&&a.mask.close(b)}),j.closeOnClick&&e.on("click.mask",function(b){a.mask.close(b)}),a(window).on("resize.mask",function(){a.mask.fit()}),k&&k.length&&(i=k.eq(0).css("zIndex"),a.each(k,function(){var b=a(this);/relative|absolute|fixed/i.test(b.css("position"))||b.css("position","relative")}),f=k.css({zIndex:Math.max(j.zIndex+1,i=="auto"?0:i)})),e.css({display:"block"}).fadeTo(j.loadSpeed,j.opacity,function(){a.mask.fit(),d(j.onLoad),g="full"}),g=!0;return this},close:function(){if(g){if(d(h.onBeforeClose)===!1)return this;e.fadeOut(h.closeSpeed,function(){d(h.onClose),f&&f.css({zIndex:i}),g=!1}),a(document).off("keydown.mask"),e.off("click.mask"),a(window).off("resize.mask")}return this},fit:function(){if(g){var a=c();e.css({width:a[0],height:a[1]})}},getMask:function(){return e},isLoaded:function(a){return a?g=="full":g},getConf:function(){return h},getExposed:function(){return f}},a.fn.mask=function(b){a.mask.load(b);return this},a.fn.expose=function(b){a.mask.load(b,this);return this}})(jQuery);
(function(a){a.tools=a.tools||{version:"v1.2.7"},a.tools.tabs={conf:{tabs:"a",current:"current",onBeforeClick:null,onClick:null,effect:"default",initialEffect:!1,initialIndex:0,event:"click",rotate:!1,slideUpSpeed:400,slideDownSpeed:400,history:!1},addEffect:function(a,c){b[a]=c}};var b={"default":function(a,b){this.getPanes().hide().eq(a).show(),b.call()},fade:function(a,b){var c=this.getConf(),d=c.fadeOutSpeed,e=this.getPanes();d?e.fadeOut(d):e.hide(),e.eq(a).fadeIn(c.fadeInSpeed,b)},slide:function(a,b){var c=this.getConf();this.getPanes().slideUp(c.slideUpSpeed),this.getPanes().eq(a).slideDown(c.slideDownSpeed,b)},ajax:function(a,b){this.getPanes().eq(0).load(this.getTabs().eq(a).attr("href"),b)}},c,d;a.tools.tabs.addEffect("horizontal",function(b,e){if(!c){var f=this.getPanes().eq(b),g=this.getCurrentPane();d||(d=this.getPanes().eq(0).width()),c=!0,f.show(),g.animate({width:0},{step:function(a){f.css("width",d-a)},complete:function(){a(this).hide(),e.call(),c=!1}}),g.length||(e.call(),c=!1)}});function e(c,d,e){var f=this,g=c.add(this),h=c.find(e.tabs),i=d.jquery?d:c.children(d),j;h.length||(h=c.children()),i.length||(i=c.parent().find(d)),i.length||(i=a(d)),a.extend(this,{click:function(d,i){var k=h.eq(d),l=!c.data("tabs");typeof d=="string"&&d.replace("#","")&&(k=h.filter("[href*=\""+d.replace("#","")+"\"]"),d=Math.max(h.index(k),0));if(e.rotate){var m=h.length-1;if(d<0)return f.click(m,i);if(d>m)return f.click(0,i)}if(!k.length){if(j>=0)return f;d=e.initialIndex,k=h.eq(d)}if(d===j)return f;i=i||a.Event(),i.type="onBeforeClick",g.trigger(i,[d]);if(!i.isDefaultPrevented()){var n=l?e.initialEffect&&e.effect||"default":e.effect;b[n].call(f,d,function(){j=d,i.type="onClick",g.trigger(i,[d])}),h.removeClass(e.current),k.addClass(e.current);return f}},getConf:function(){return e},getTabs:function(){return h},getPanes:function(){return i},getCurrentPane:function(){return i.eq(j)},getCurrentTab:function(){return h.eq(j)},getIndex:function(){return j},next:function(){return f.click(j+1)},prev:function(){return f.click(j-1)},destroy:function(){h.off(e.event).removeClass(e.current),i.find("a[href^=\"#\"]").off("click.T");return f}}),a.each("onBeforeClick,onClick".split(","),function(b,c){a.isFunction(e[c])&&a(f).on(c,e[c]),f[c]=function(b){b&&a(f).on(c,b);return f}}),e.history&&a.fn.history&&(a.tools.history.init(h),e.event="history"),h.each(function(b){a(this).on(e.event,function(a){f.click(b,a);return a.preventDefault()})}),i.find("a[href^=\"#\"]").on("click.T",function(b){f.click(a(this).attr("href"),b)}),location.hash&&e.tabs=="a"&&c.find("[href=\""+location.hash+"\"]").length?f.click(location.hash):(e.initialIndex===0||e.initialIndex>0)&&f.click(e.initialIndex)}a.fn.tabs=function(b,c){var d=this.data("tabs");d&&(d.destroy(),this.removeData("tabs")),a.isFunction(c)&&(c={onBeforeClick:c}),c=a.extend({},a.tools.tabs.conf,c),this.each(function(){d=new e(a(this),b,c),a(this).data("tabs",d)});return c.api?d:this}})(jQuery);

/*!
 * jCarousel - Riding carousels with jQuery
 *   http://sorgalla.com/jcarousel/
 *
 * Copyright (c) 2006 Jan Sorgalla (http://sorgalla.com)
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 *
 * Built on top of the jQuery library
 *   http://jquery.com
 *
 * Inspired by the "Carousel Component" by Bill Scott
 *   http://billwscott.com/carousel/
 */

(function(g){var q={vertical:!1,rtl:!1,start:1,offset:1,size:null,scroll:3,visible:null,animation:"normal",easing:"swing",auto:0,wrap:null,initCallback:null,setupCallback:null,reloadCallback:null,itemLoadCallback:null,itemFirstInCallback:null,itemFirstOutCallback:null,itemLastInCallback:null,itemLastOutCallback:null,itemVisibleInCallback:null,itemVisibleOutCallback:null,animationStepCallback:null,buttonNextHTML:"<div></div>",buttonPrevHTML:"<div></div>",buttonNextEvent:"click",buttonPrevEvent:"click", buttonNextCallback:null,buttonPrevCallback:null,itemFallbackDimension:null},m=!1;g(window).bind("load.jcarousel",function(){m=!0});g.jcarousel=function(a,c){this.options=g.extend({},q,c||{});this.autoStopped=this.locked=!1;this.buttonPrevState=this.buttonNextState=this.buttonPrev=this.buttonNext=this.list=this.clip=this.container=null;if(!c||c.rtl===void 0)this.options.rtl=(g(a).attr("dir")||g("html").attr("dir")||"").toLowerCase()=="rtl";this.wh=!this.options.vertical?"width":"height";this.lt=!this.options.vertical? this.options.rtl?"right":"left":"top";for(var b="",d=a.className.split(" "),f=0;f<d.length;f++)if(d[f].indexOf("jcarousel-skin")!=-1){g(a).removeClass(d[f]);b=d[f];break}a.nodeName.toUpperCase()=="UL"||a.nodeName.toUpperCase()=="OL"?(this.list=g(a),this.clip=this.list.parents(".jcarousel-clip"),this.container=this.list.parents(".jcarousel-container")):(this.container=g(a),this.list=this.container.find("ul,ol").eq(0),this.clip=this.container.find(".jcarousel-clip"));if(this.clip.size()===0)this.clip= this.list.wrap("<div></div>").parent();if(this.container.size()===0)this.container=this.clip.wrap("<div></div>").parent();b!==""&&this.container.parent()[0].className.indexOf("jcarousel-skin")==-1&&this.container.wrap('<div class=" '+b+'"></div>');this.buttonPrev=g(".jcarousel-prev",this.container);if(this.buttonPrev.size()===0&&this.options.buttonPrevHTML!==null)this.buttonPrev=g(this.options.buttonPrevHTML).appendTo(this.container);this.buttonPrev.addClass(this.className("jcarousel-prev"));this.buttonNext= g(".jcarousel-next",this.container);if(this.buttonNext.size()===0&&this.options.buttonNextHTML!==null)this.buttonNext=g(this.options.buttonNextHTML).appendTo(this.container);this.buttonNext.addClass(this.className("jcarousel-next"));this.clip.addClass(this.className("jcarousel-clip")).css({position:"relative"});this.list.addClass(this.className("jcarousel-list")).css({overflow:"hidden",position:"relative",top:0,margin:0,padding:0}).css(this.options.rtl?"right":"left",0);this.container.addClass(this.className("jcarousel-container")).css({position:"relative"}); !this.options.vertical&&this.options.rtl&&this.container.addClass("jcarousel-direction-rtl").attr("dir","rtl");var j=this.options.visible!==null?Math.ceil(this.clipping()/this.options.visible):null,b=this.list.children("li"),e=this;if(b.size()>0){var h=0,i=this.options.offset;b.each(function(){e.format(this,i++);h+=e.dimension(this,j)});this.list.css(this.wh,h+100+"px");if(!c||c.size===void 0)this.options.size=b.size()}this.container.css("display","block");this.buttonNext.css("display","block");this.buttonPrev.css("display", "block");this.funcNext=function(){e.next()};this.funcPrev=function(){e.prev()};this.funcResize=function(){e.resizeTimer&&clearTimeout(e.resizeTimer);e.resizeTimer=setTimeout(function(){e.reload()},100)};this.options.initCallback!==null&&this.options.initCallback(this,"init");!m&&g.browser.safari?(this.buttons(!1,!1),g(window).bind("load.jcarousel",function(){e.setup()})):this.setup()};var f=g.jcarousel;f.fn=f.prototype={jcarousel:"0.2.8"};f.fn.extend=f.extend=g.extend;f.fn.extend({setup:function(){this.prevLast= this.prevFirst=this.last=this.first=null;this.animating=!1;this.tail=this.resizeTimer=this.timer=null;this.inTail=!1;if(!this.locked){this.list.css(this.lt,this.pos(this.options.offset)+"px");var a=this.pos(this.options.start,!0);this.prevFirst=this.prevLast=null;this.animate(a,!1);g(window).unbind("resize.jcarousel",this.funcResize).bind("resize.jcarousel",this.funcResize);this.options.setupCallback!==null&&this.options.setupCallback(this)}},reset:function(){this.list.empty();this.list.css(this.lt, "0px");this.list.css(this.wh,"10px");this.options.initCallback!==null&&this.options.initCallback(this,"reset");this.setup()},reload:function(){this.tail!==null&&this.inTail&&this.list.css(this.lt,f.intval(this.list.css(this.lt))+this.tail);this.tail=null;this.inTail=!1;this.options.reloadCallback!==null&&this.options.reloadCallback(this);if(this.options.visible!==null){var a=this,c=Math.ceil(this.clipping()/this.options.visible),b=0,d=0;this.list.children("li").each(function(f){b+=a.dimension(this, c);f+1<a.first&&(d=b)});this.list.css(this.wh,b+"px");this.list.css(this.lt,-d+"px")}this.scroll(this.first,!1)},lock:function(){this.locked=!0;this.buttons()},unlock:function(){this.locked=!1;this.buttons()},size:function(a){if(a!==void 0)this.options.size=a,this.locked||this.buttons();return this.options.size},has:function(a,c){if(c===void 0||!c)c=a;if(this.options.size!==null&&c>this.options.size)c=this.options.size;for(var b=a;b<=c;b++){var d=this.get(b);if(!d.length||d.hasClass("jcarousel-item-placeholder"))return!1}return!0}, get:function(a){return g(">.jcarousel-item-"+a,this.list)},add:function(a,c){var b=this.get(a),d=0,p=g(c);if(b.length===0)for(var j,e=f.intval(a),b=this.create(a);;){if(j=this.get(--e),e<=0||j.length){e<=0?this.list.prepend(b):j.after(b);break}}else d=this.dimension(b);p.get(0).nodeName.toUpperCase()=="LI"?(b.replaceWith(p),b=p):b.empty().append(c);this.format(b.removeClass(this.className("jcarousel-item-placeholder")),a);p=this.options.visible!==null?Math.ceil(this.clipping()/this.options.visible): null;d=this.dimension(b,p)-d;a>0&&a<this.first&&this.list.css(this.lt,f.intval(this.list.css(this.lt))-d+"px");this.list.css(this.wh,f.intval(this.list.css(this.wh))+d+"px");return b},remove:function(a){var c=this.get(a);if(c.length&&!(a>=this.first&&a<=this.last)){var b=this.dimension(c);a<this.first&&this.list.css(this.lt,f.intval(this.list.css(this.lt))+b+"px");c.remove();this.list.css(this.wh,f.intval(this.list.css(this.wh))-b+"px")}},next:function(){this.tail!==null&&!this.inTail?this.scrollTail(!1): this.scroll((this.options.wrap=="both"||this.options.wrap=="last")&&this.options.size!==null&&this.last==this.options.size?1:this.first+this.options.scroll)},prev:function(){this.tail!==null&&this.inTail?this.scrollTail(!0):this.scroll((this.options.wrap=="both"||this.options.wrap=="first")&&this.options.size!==null&&this.first==1?this.options.size:this.first-this.options.scroll)},scrollTail:function(a){if(!this.locked&&!this.animating&&this.tail){this.pauseAuto();var c=f.intval(this.list.css(this.lt)), c=!a?c-this.tail:c+this.tail;this.inTail=!a;this.prevFirst=this.first;this.prevLast=this.last;this.animate(c)}},scroll:function(a,c){!this.locked&&!this.animating&&(this.pauseAuto(),this.animate(this.pos(a),c))},pos:function(a,c){var b=f.intval(this.list.css(this.lt));if(this.locked||this.animating)return b;this.options.wrap!="circular"&&(a=a<1?1:this.options.size&&a>this.options.size?this.options.size:a);for(var d=this.first>a,g=this.options.wrap!="circular"&&this.first<=1?1:this.first,j=d?this.get(g): this.get(this.last),e=d?g:g-1,h=null,i=0,k=!1,l=0;d?--e>=a:++e<a;){h=this.get(e);k=!h.length;if(h.length===0&&(h=this.create(e).addClass(this.className("jcarousel-item-placeholder")),j[d?"before":"after"](h),this.first!==null&&this.options.wrap=="circular"&&this.options.size!==null&&(e<=0||e>this.options.size)))j=this.get(this.index(e)),j.length&&(h=this.add(e,j.clone(!0)));j=h;l=this.dimension(h);k&&(i+=l);if(this.first!==null&&(this.options.wrap=="circular"||e>=1&&(this.options.size===null||e<= this.options.size)))b=d?b+l:b-l}for(var g=this.clipping(),m=[],o=0,n=0,j=this.get(a-1),e=a;++o;){h=this.get(e);k=!h.length;if(h.length===0){h=this.create(e).addClass(this.className("jcarousel-item-placeholder"));if(j.length===0)this.list.prepend(h);else j[d?"before":"after"](h);if(this.first!==null&&this.options.wrap=="circular"&&this.options.size!==null&&(e<=0||e>this.options.size))j=this.get(this.index(e)),j.length&&(h=this.add(e,j.clone(!0)))}j=h;l=this.dimension(h);if(l===0)throw Error("jCarousel: No width/height set for items. This will cause an infinite loop. Aborting..."); this.options.wrap!="circular"&&this.options.size!==null&&e>this.options.size?m.push(h):k&&(i+=l);n+=l;if(n>=g)break;e++}for(h=0;h<m.length;h++)m[h].remove();i>0&&(this.list.css(this.wh,this.dimension(this.list)+i+"px"),d&&(b-=i,this.list.css(this.lt,f.intval(this.list.css(this.lt))-i+"px")));i=a+o-1;if(this.options.wrap!="circular"&&this.options.size&&i>this.options.size)i=this.options.size;if(e>i){o=0;e=i;for(n=0;++o;){h=this.get(e--);if(!h.length)break;n+=this.dimension(h);if(n>=g)break}}e=i-o+ 1;this.options.wrap!="circular"&&e<1&&(e=1);if(this.inTail&&d)b+=this.tail,this.inTail=!1;this.tail=null;if(this.options.wrap!="circular"&&i==this.options.size&&i-o+1>=1&&(d=f.intval(this.get(i).css(!this.options.vertical?"marginRight":"marginBottom")),n-d>g))this.tail=n-g-d;if(c&&a===this.options.size&&this.tail)b-=this.tail,this.inTail=!0;for(;a-- >e;)b+=this.dimension(this.get(a));this.prevFirst=this.first;this.prevLast=this.last;this.first=e;this.last=i;return b},animate:function(a,c){if(!this.locked&& !this.animating){this.animating=!0;var b=this,d=function(){b.animating=!1;a===0&&b.list.css(b.lt,0);!b.autoStopped&&(b.options.wrap=="circular"||b.options.wrap=="both"||b.options.wrap=="last"||b.options.size===null||b.last<b.options.size||b.last==b.options.size&&b.tail!==null&&!b.inTail)&&b.startAuto();b.buttons();b.notify("onAfterAnimation");if(b.options.wrap=="circular"&&b.options.size!==null)for(var c=b.prevFirst;c<=b.prevLast;c++)c!==null&&!(c>=b.first&&c<=b.last)&&(c<1||c>b.options.size)&&b.remove(c)}; this.notify("onBeforeAnimation");if(!this.options.animation||c===!1)this.list.css(this.lt,a+"px"),d();else{var f=!this.options.vertical?this.options.rtl?{right:a}:{left:a}:{top:a},d={duration:this.options.animation,easing:this.options.easing,complete:d};if(g.isFunction(this.options.animationStepCallback))d.step=this.options.animationStepCallback;this.list.animate(f,d)}}},startAuto:function(a){if(a!==void 0)this.options.auto=a;if(this.options.auto===0)return this.stopAuto();if(this.timer===null){this.autoStopped= !1;var c=this;this.timer=window.setTimeout(function(){c.next()},this.options.auto*1E3)}},stopAuto:function(){this.pauseAuto();this.autoStopped=!0},pauseAuto:function(){if(this.timer!==null)window.clearTimeout(this.timer),this.timer=null},buttons:function(a,c){if(a==null&&(a=!this.locked&&this.options.size!==0&&(this.options.wrap&&this.options.wrap!="first"||this.options.size===null||this.last<this.options.size),!this.locked&&(!this.options.wrap||this.options.wrap=="first")&&this.options.size!==null&& this.last>=this.options.size))a=this.tail!==null&&!this.inTail;if(c==null&&(c=!this.locked&&this.options.size!==0&&(this.options.wrap&&this.options.wrap!="last"||this.first>1),!this.locked&&(!this.options.wrap||this.options.wrap=="last")&&this.options.size!==null&&this.first==1))c=this.tail!==null&&this.inTail;var b=this;this.buttonNext.size()>0?(this.buttonNext.unbind(this.options.buttonNextEvent+".jcarousel",this.funcNext),a&&this.buttonNext.bind(this.options.buttonNextEvent+".jcarousel",this.funcNext), this.buttonNext[a?"removeClass":"addClass"](this.className("jcarousel-next-disabled")).attr("disabled",a?!1:!0),this.options.buttonNextCallback!==null&&this.buttonNext.data("jcarouselstate")!=a&&this.buttonNext.each(function(){b.options.buttonNextCallback(b,this,a)}).data("jcarouselstate",a)):this.options.buttonNextCallback!==null&&this.buttonNextState!=a&&this.options.buttonNextCallback(b,null,a);this.buttonPrev.size()>0?(this.buttonPrev.unbind(this.options.buttonPrevEvent+".jcarousel",this.funcPrev), c&&this.buttonPrev.bind(this.options.buttonPrevEvent+".jcarousel",this.funcPrev),this.buttonPrev[c?"removeClass":"addClass"](this.className("jcarousel-prev-disabled")).attr("disabled",c?!1:!0),this.options.buttonPrevCallback!==null&&this.buttonPrev.data("jcarouselstate")!=c&&this.buttonPrev.each(function(){b.options.buttonPrevCallback(b,this,c)}).data("jcarouselstate",c)):this.options.buttonPrevCallback!==null&&this.buttonPrevState!=c&&this.options.buttonPrevCallback(b,null,c);this.buttonNextState= a;this.buttonPrevState=c},notify:function(a){var c=this.prevFirst===null?"init":this.prevFirst<this.first?"next":"prev";this.callback("itemLoadCallback",a,c);this.prevFirst!==this.first&&(this.callback("itemFirstInCallback",a,c,this.first),this.callback("itemFirstOutCallback",a,c,this.prevFirst));this.prevLast!==this.last&&(this.callback("itemLastInCallback",a,c,this.last),this.callback("itemLastOutCallback",a,c,this.prevLast));this.callback("itemVisibleInCallback",a,c,this.first,this.last,this.prevFirst, this.prevLast);this.callback("itemVisibleOutCallback",a,c,this.prevFirst,this.prevLast,this.first,this.last)},callback:function(a,c,b,d,f,j,e){if(!(this.options[a]==null||typeof this.options[a]!="object"&&c!="onAfterAnimation")){var h=typeof this.options[a]=="object"?this.options[a][c]:this.options[a];if(g.isFunction(h)){var i=this;if(d===void 0)h(i,b,c);else if(f===void 0)this.get(d).each(function(){h(i,this,d,b,c)});else for(var a=function(a){i.get(a).each(function(){h(i,this,a,b,c)})},k=d;k<=f;k++)k!== null&&!(k>=j&&k<=e)&&a(k)}}},create:function(a){return this.format("<li></li>",a)},format:function(a,c){for(var a=g(a),b=a.get(0).className.split(" "),d=0;d<b.length;d++)b[d].indexOf("jcarousel-")!=-1&&a.removeClass(b[d]);a.addClass(this.className("jcarousel-item")).addClass(this.className("jcarousel-item-"+c)).css({"float":this.options.rtl?"right":"left","list-style":"none"}).attr("jcarouselindex",c);return a},className:function(a){return a+" "+a+(!this.options.vertical?"-horizontal":"-vertical")}, dimension:function(a,c){var b=g(a);if(c==null)return!this.options.vertical?b.outerWidth(!0)||f.intval(this.options.itemFallbackDimension):b.outerHeight(!0)||f.intval(this.options.itemFallbackDimension);else{var d=!this.options.vertical?c-f.intval(b.css("marginLeft"))-f.intval(b.css("marginRight")):c-f.intval(b.css("marginTop"))-f.intval(b.css("marginBottom"));g(b).css(this.wh,d+"px");return this.dimension(b)}},clipping:function(){return!this.options.vertical?this.clip[0].offsetWidth-f.intval(this.clip.css("borderLeftWidth"))- f.intval(this.clip.css("borderRightWidth")):this.clip[0].offsetHeight-f.intval(this.clip.css("borderTopWidth"))-f.intval(this.clip.css("borderBottomWidth"))},index:function(a,c){if(c==null)c=this.options.size;return Math.round(((a-1)/c-Math.floor((a-1)/c))*c)+1}});f.extend({defaults:function(a){return g.extend(q,a||{})},intval:function(a){a=parseInt(a,10);return isNaN(a)?0:a},windowLoaded:function(){m=!0}});g.fn.jcarousel=function(a){if(typeof a=="string"){var c=g(this).data("jcarousel"),b=Array.prototype.slice.call(arguments, 1);return c[a].apply(c,b)}else return this.each(function(){var b=g(this).data("jcarousel");b?(a&&g.extend(b.options,a),b.reload()):g(this).data("jcarousel",new f(this,a))})}})(jQuery);

// VERTICALLY ALIGN FUNCTION
$.fn.vAlign=function(){return this.each(function(i){var a=$(this).height();var b=$(this).parent().height();var c=Math.ceil((b-a)/2);$(this).css('padding-top',c-2);$(this).css('height',b-c-2)})};

/**
 * Copyright (c) 2007-2012 Ariel Flesler - aflesler(at)gmail(dot)com | http://flesler.blogspot.com
 * Dual licensed under MIT and GPL.
 * @author Ariel Flesler
 * @version 1.4.3.1
 */
;(function($){var h=$.scrollTo=function(a,b,c){$(window).scrollTo(a,b,c)};h.defaults={axis:'xy',duration:parseFloat($.fn.jquery)>=1.3?0:1,limit:true};h.window=function(a){return $(window)._scrollable()};$.fn._scrollable=function(){return this.map(function(){var a=this,isWin=!a.nodeName||$.inArray(a.nodeName.toLowerCase(),['iframe','#document','html','body'])!=-1;if(!isWin)return a;var b=(a.contentWindow||a).document||a.ownerDocument||a;return/webkit/i.test(navigator.userAgent)||b.compatMode=='BackCompat'?b.body:b.documentElement})};$.fn.scrollTo=function(e,f,g){if(typeof f=='object'){g=f;f=0}if(typeof g=='function')g={onAfter:g};if(e=='max')e=9e9;g=$.extend({},h.defaults,g);f=f||g.duration;g.queue=g.queue&&g.axis.length>1;if(g.queue)f/=2;g.offset=both(g.offset);g.over=both(g.over);return this._scrollable().each(function(){if(e==null)return;var d=this,$elem=$(d),targ=e,toff,attr={},win=$elem.is('html,body');switch(typeof targ){case'number':case'string':if(/^([+-]=)?\d+(\.\d+)?(px|%)?$/.test(targ)){targ=both(targ);break}targ=$(targ,this);if(!targ.length)return;case'object':if(targ.is||targ.style)toff=(targ=$(targ)).offset()}$.each(g.axis.split(''),function(i,a){var b=a=='x'?'Left':'Top',pos=b.toLowerCase(),key='scroll'+b,old=d[key],max=h.max(d,a);if(toff){attr[key]=toff[pos]+(win?0:old-$elem.offset()[pos]);if(g.margin){attr[key]-=parseInt(targ.css('margin'+b))||0;attr[key]-=parseInt(targ.css('border'+b+'Width'))||0}attr[key]+=g.offset[pos]||0;if(g.over[pos])attr[key]+=targ[a=='x'?'width':'height']()*g.over[pos]}else{var c=targ[pos];attr[key]=c.slice&&c.slice(-1)=='%'?parseFloat(c)/100*max:c}if(g.limit&&/^\d+$/.test(attr[key]))attr[key]=attr[key]<=0?0:Math.min(attr[key],max);if(!i&&g.queue){if(old!=attr[key])animate(g.onAfterFirst);delete attr[key]}});animate(g.onAfter);function animate(a){$elem.animate(attr,f,g.easing,a&&function(){a.call(this,e,g)})}}).end()};h.max=function(a,b){var c=b=='x'?'Width':'Height',scroll='scroll'+c;if(!$(a).is('html,body'))return a[scroll]-$(a)[c.toLowerCase()]();var d='client'+c,html=a.ownerDocument.documentElement,body=a.ownerDocument.body;return Math.max(html[scroll],body[scroll])-Math.min(html[d],body[d])};function both(a){return typeof a=='object'?a:{top:a,left:a}}})(jQuery);


/*
CSS Browser Selector v0.4.0 (Nov 02, 2010)
Rafael Lima (http://rafael.adm.br)
http://rafael.adm.br/css_browser_selector
License: http://creativecommons.org/licenses/by/2.5/
Contributors: http://rafael.adm.br/css_browser_selector#contributors
*/
function css_browser_selector(u){var ua=u.toLowerCase(),is=function(t){return ua.indexOf(t)>-1},g='gecko',w='webkit',s='safari',o='opera',m='mobile',h=document.documentElement,b=[(!(/opera|webtv/i.test(ua))&&/msie\s(\d)/.test(ua))?('ie ie'+RegExp.$1):is('firefox/2')?g+' ff2':is('firefox/3.5')?g+' ff3 ff3_5':is('firefox/3.6')?g+' ff3 ff3_6':is('firefox/3')?g+' ff3':is('gecko/')?g:is('opera')?o+(/version\/(\d+)/.test(ua)?' '+o+RegExp.$1:(/opera(\s|\/)(\d+)/.test(ua)?' '+o+RegExp.$2:'')):is('konqueror')?'konqueror':is('blackberry')?m+' blackberry':is('android')?m+' android':is('chrome')?w+' chrome':is('iron')?w+' iron':is('applewebkit/')?w+' '+s+(/version\/(\d+)/.test(ua)?' '+s+RegExp.$1:''):is('mozilla/')?g:'',is('j2me')?m+' j2me':is('iphone')?m+' iphone':is('ipod')?m+' ipod':is('ipad')?m+' ipad':is('mac')?'mac':is('darwin')?'mac':is('webtv')?'webtv':is('win')?'win'+(is('windows nt 6.0')?' vista':''):is('freebsd')?'freebsd':(is('x11')||is('linux'))?'linux':'','js']; c = b.join(' '); h.className += ' '+c; return c;}; css_browser_selector(navigator.userAgent);
var ua = $.browser;
if(ua.msie && ua.version < 9)  { document.documentElement.className += " ielt9" }

/**
 * Equal Heights Plugin
 * Equalize the heights of elements. Great for columns or any elements
 * that need to be the same size (floats, etc).
 *
 * Version 1.0
 * Updated 12/10/2008
 *
 * Copyright (c) 2008 Rob Glazebrook (cssnewbie.com)
 *
 * Usage: $(object).equalHeights([minHeight], [maxHeight]);
 *
 * Example 1: $(".cols").equalHeights(); Sets all columns to the same height.
 * Example 2: $(".cols").equalHeights(400); Sets all cols to at least 400px tall.
 * Example 3: $(".cols").equalHeights(100,300); Cols are at least 100 but no more
 * than 300 pixels tall. Elements with too much content will gain a scrollbar.
 *
 */

(function($) {
	$.fn.equalHeights = function(minHeight, maxHeight) {
		tallest = (minHeight) ? minHeight : 0;
		this.each(function() {
			if($(this).height() > tallest) {
				tallest = $(this).height();
			}
		});
		if((maxHeight) && tallest > maxHeight) tallest = maxHeight;
		return this.each(function() {
			$(this).height(tallest).css("overflow","auto");
		});
	}
})(jQuery);

/*
	name : chunk
	file : jquery.chunk.js
	author : gregory tomlinson
	Dual licensed under the MIT and GPL licenses.
	///////////////////////////
	///////////////////////////
	dependencies : jQuery 1.3.2+
	///////////////////////////
	///////////////////////////

*/

(function($) {

	$.chunk = function( array, chunkSize ) {
	   var base = [], i, size = chunkSize || 5;
	   for(i=0; i<array.length; i+=size ) { base.push( array.slice( i, i+size ) ); }
	   return base;
	}

})(jQuery);

/*
 * Fade Slider Toggle plugin
 *
 * Copyright(c) 2009, Cedric Dugas
 * http://www.position-relative.net
 *
 * A sliderToggle() with opacity
 * Licenced under the MIT Licence
 */
 jQuery.fn.fadeSliderToggle = function(settings) {
 	/* Damn you jQuery opacity:'toggle' that dosen't work!~!!!*/
 	 settings = jQuery.extend({
		speed: 300,
		easing : "swing",
		onComplete : false,
		step : false
	}, settings)

	var caller = this
 	if($(caller).css("display") == "none"){
 		$(caller).animate({
 			opacity: 1,
 			height: 'toggle'
 		},
		{ duration : settings.speed,
		  easing : settings.easing,
		  complete : settings.onComplete,
		  step : settings.step});
	}else{
		$(caller).animate({
 			opacity: 0,
 			height: 'toggle'
 		},
		{ duration : settings.speed,
		  easing : settings.easing,
		  complete : settings.onComplete,
		  step : settings.step});
	}
};

/* easing */
// t: current time, b: begInnIng value, c: change In value, d: duration
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('h.i[\'1a\']=h.i[\'z\'];h.O(h.i,{y:\'D\',z:9(x,t,b,c,d){6 h.i[h.i.y](x,t,b,c,d)},17:9(x,t,b,c,d){6 c*(t/=d)*t+b},D:9(x,t,b,c,d){6-c*(t/=d)*(t-2)+b},13:9(x,t,b,c,d){e((t/=d/2)<1)6 c/2*t*t+b;6-c/2*((--t)*(t-2)-1)+b},X:9(x,t,b,c,d){6 c*(t/=d)*t*t+b},U:9(x,t,b,c,d){6 c*((t=t/d-1)*t*t+1)+b},R:9(x,t,b,c,d){e((t/=d/2)<1)6 c/2*t*t*t+b;6 c/2*((t-=2)*t*t+2)+b},N:9(x,t,b,c,d){6 c*(t/=d)*t*t*t+b},M:9(x,t,b,c,d){6-c*((t=t/d-1)*t*t*t-1)+b},L:9(x,t,b,c,d){e((t/=d/2)<1)6 c/2*t*t*t*t+b;6-c/2*((t-=2)*t*t*t-2)+b},K:9(x,t,b,c,d){6 c*(t/=d)*t*t*t*t+b},J:9(x,t,b,c,d){6 c*((t=t/d-1)*t*t*t*t+1)+b},I:9(x,t,b,c,d){e((t/=d/2)<1)6 c/2*t*t*t*t*t+b;6 c/2*((t-=2)*t*t*t*t+2)+b},G:9(x,t,b,c,d){6-c*8.C(t/d*(8.g/2))+c+b},15:9(x,t,b,c,d){6 c*8.n(t/d*(8.g/2))+b},12:9(x,t,b,c,d){6-c/2*(8.C(8.g*t/d)-1)+b},Z:9(x,t,b,c,d){6(t==0)?b:c*8.j(2,10*(t/d-1))+b},Y:9(x,t,b,c,d){6(t==d)?b+c:c*(-8.j(2,-10*t/d)+1)+b},W:9(x,t,b,c,d){e(t==0)6 b;e(t==d)6 b+c;e((t/=d/2)<1)6 c/2*8.j(2,10*(t-1))+b;6 c/2*(-8.j(2,-10*--t)+2)+b},V:9(x,t,b,c,d){6-c*(8.o(1-(t/=d)*t)-1)+b},S:9(x,t,b,c,d){6 c*8.o(1-(t=t/d-1)*t)+b},Q:9(x,t,b,c,d){e((t/=d/2)<1)6-c/2*(8.o(1-t*t)-1)+b;6 c/2*(8.o(1-(t-=2)*t)+1)+b},P:9(x,t,b,c,d){f s=1.l;f p=0;f a=c;e(t==0)6 b;e((t/=d)==1)6 b+c;e(!p)p=d*.3;e(a<8.w(c)){a=c;f s=p/4}m f s=p/(2*8.g)*8.r(c/a);6-(a*8.j(2,10*(t-=1))*8.n((t*d-s)*(2*8.g)/p))+b},H:9(x,t,b,c,d){f s=1.l;f p=0;f a=c;e(t==0)6 b;e((t/=d)==1)6 b+c;e(!p)p=d*.3;e(a<8.w(c)){a=c;f s=p/4}m f s=p/(2*8.g)*8.r(c/a);6 a*8.j(2,-10*t)*8.n((t*d-s)*(2*8.g)/p)+c+b},T:9(x,t,b,c,d){f s=1.l;f p=0;f a=c;e(t==0)6 b;e((t/=d/2)==2)6 b+c;e(!p)p=d*(.3*1.5);e(a<8.w(c)){a=c;f s=p/4}m f s=p/(2*8.g)*8.r(c/a);e(t<1)6-.5*(a*8.j(2,10*(t-=1))*8.n((t*d-s)*(2*8.g)/p))+b;6 a*8.j(2,-10*(t-=1))*8.n((t*d-s)*(2*8.g)/p)*.5+c+b},F:9(x,t,b,c,d,s){e(s==u)s=1.l;6 c*(t/=d)*t*((s+1)*t-s)+b},E:9(x,t,b,c,d,s){e(s==u)s=1.l;6 c*((t=t/d-1)*t*((s+1)*t+s)+1)+b},16:9(x,t,b,c,d,s){e(s==u)s=1.l;e((t/=d/2)<1)6 c/2*(t*t*(((s*=(1.B))+1)*t-s))+b;6 c/2*((t-=2)*t*(((s*=(1.B))+1)*t+s)+2)+b},A:9(x,t,b,c,d){6 c-h.i.v(x,d-t,0,c,d)+b},v:9(x,t,b,c,d){e((t/=d)<(1/2.k)){6 c*(7.q*t*t)+b}m e(t<(2/2.k)){6 c*(7.q*(t-=(1.5/2.k))*t+.k)+b}m e(t<(2.5/2.k)){6 c*(7.q*(t-=(2.14/2.k))*t+.11)+b}m{6 c*(7.q*(t-=(2.18/2.k))*t+.19)+b}},1b:9(x,t,b,c,d){e(t<d/2)6 h.i.A(x,t*2,0,c,d)*.5+b;6 h.i.v(x,t*2-d,0,c,d)*.5+c*.5+b}});',62,74,'||||||return||Math|function|||||if|var|PI|jQuery|easing|pow|75|70158|else|sin|sqrt||5625|asin|||undefined|easeOutBounce|abs||def|swing|easeInBounce|525|cos|easeOutQuad|easeOutBack|easeInBack|easeInSine|easeOutElastic|easeInOutQuint|easeOutQuint|easeInQuint|easeInOutQuart|easeOutQuart|easeInQuart|extend|easeInElastic|easeInOutCirc|easeInOutCubic|easeOutCirc|easeInOutElastic|easeOutCubic|easeInCirc|easeInOutExpo|easeInCubic|easeOutExpo|easeInExpo||9375|easeInOutSine|easeInOutQuad|25|easeOutSine|easeInOutBack|easeInQuad|625|984375|jswing|easeInOutBounce'.split('|'),0,{}))

/*! fancyBox v2.0.6 fancyapps.com | fancyapps.com/fancybox/#license */
;(function(s,l,d,t){var m=d(s),q=d(l),a=d.fancybox=function(){a.open.apply(this,arguments)},u=!1,k=l.createTouch!==t,o=function(a){return"string"===d.type(a)},n=function(b,c){c&&o(b)&&0<b.indexOf("%")&&(b=a.getViewport()[c]/100*parseInt(b,10));return Math.round(b)+"px"};d.extend(a,{version:"2.0.5",defaults:{padding:15,margin:20,width:800,height:600,minWidth:100,minHeight:100,maxWidth:9999,maxHeight:9999,autoSize:!0,autoResize:!k,autoCenter:!k,fitToView:!0,aspectRatio:!1,topRatio:0.5,fixed:!1,scrolling:"auto",
wrapCSS:"",arrows:!0,closeBtn:!0,closeClick:!1,nextClick:!1,mouseWheel:!0,autoPlay:!1,playSpeed:3E3,preload:3,modal:!1,loop:!0,ajax:{dataType:"html",headers:{"X-fancyBox":!0}},keys:{next:[13,32,34,39,40],prev:[8,33,37,38],close:[27]},tpl:{wrap:'<div class="fancybox-wrap"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>',image:'<img class="fancybox-image" src="{href}" alt="" />',iframe:'<iframe class="fancybox-iframe" name="fancybox-frame{rnd}" frameborder="0" hspace="0"'+
(d.browser.msie?' allowtransparency="true"':"")+"></iframe>",swf:'<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="100%" height="100%"><param name="wmode" value="transparent" /><param name="allowfullscreen" value="true" /><param name="allowscriptaccess" value="always" /><param name="movie" value="{href}" /><embed src="{href}" type="application/x-shockwave-flash" allowfullscreen="true" allowscriptaccess="always" width="100%" height="100%" wmode="transparent"></embed></object>',error:'<p class="fancybox-error">The requested content cannot be loaded.<br/>Please try again later.</p>',
closeBtn:'<div title="Close" class="fancybox-item fancybox-close"></div>',next:'<a title="Next" class="fancybox-nav fancybox-next"><span></span></a>',prev:'<a title="Previous" class="fancybox-nav fancybox-prev"><span></span></a>'},openEffect:"fade",openSpeed:300,openEasing:"swing",openOpacity:!0,openMethod:"zoomIn",closeEffect:"fade",closeSpeed:300,closeEasing:"swing",closeOpacity:!0,closeMethod:"zoomOut",nextEffect:"elastic",nextSpeed:300,nextEasing:"swing",nextMethod:"changeIn",prevEffect:"elastic",
prevSpeed:300,prevEasing:"swing",prevMethod:"changeOut",helpers:{overlay:{speedIn:0,speedOut:300,opacity:0.8,css:{cursor:"pointer"},closeClick:!0},title:{type:"float"}}},group:{},opts:{},coming:null,current:null,isOpen:!1,isOpened:!1,player:{timer:null,isActive:!1},ajaxLoad:null,imgPreload:null,transitions:{},helpers:{},open:function(b,c){a.close(!0);b&&!d.isArray(b)&&(b=b instanceof d?d(b).get():[b]);a.isActive=!0;a.opts=d.extend(!0,{},a.defaults,c);d.isPlainObject(c)&&c.keys!==t&&(a.opts.keys=c.keys?
d.extend({},a.defaults.keys,c.keys):!1);a.group=b;a._start(a.opts.index||0)},cancel:function(){a.coming&&!1===a.trigger("onCancel")||(a.coming=null,a.hideLoading(),a.ajaxLoad&&a.ajaxLoad.abort(),a.ajaxLoad=null,a.imgPreload&&(a.imgPreload.onload=a.imgPreload.onabort=a.imgPreload.onerror=null))},close:function(b){a.cancel();a.current&&!1!==a.trigger("beforeClose")&&(a.unbindEvents(),!a.isOpen||b&&!0===b[0]?(d(".fancybox-wrap").stop().trigger("onReset").remove(),a._afterZoomOut()):(a.isOpen=a.isOpened=
!1,d(".fancybox-item, .fancybox-nav").remove(),a.wrap.stop(!0).removeClass("fancybox-opened"),a.inner.css("overflow","hidden"),a.transitions[a.current.closeMethod]()))},play:function(b){var c=function(){clearTimeout(a.player.timer)},e=function(){c();a.current&&a.player.isActive&&(a.player.timer=setTimeout(a.next,a.current.playSpeed))},f=function(){c();d("body").unbind(".player");a.player.isActive=!1;a.trigger("onPlayEnd")};if(a.player.isActive||b&&!1===b[0])f();else if(a.current&&(a.current.loop||
a.current.index<a.group.length-1))a.player.isActive=!0,d("body").bind({"afterShow.player onUpdate.player":e,"onCancel.player beforeClose.player":f,"beforeLoad.player":c}),e(),a.trigger("onPlayStart")},next:function(){a.current&&a.jumpto(a.current.index+1)},prev:function(){a.current&&a.jumpto(a.current.index-1)},jumpto:function(b){a.current&&(b=parseInt(b,10),1<a.group.length&&a.current.loop&&(b>=a.group.length?b=0:0>b&&(b=a.group.length-1)),a.group[b]!==t&&(a.cancel(),a._start(b)))},reposition:function(b,
c){var e;a.isOpen&&(e=a._getPosition(c),b&&"scroll"===b.type?(delete e.position,a.wrap.stop(!0,!0).animate(e,200)):a.wrap.css(e))},update:function(b){a.isOpen&&(u||setTimeout(function(){var c=a.current,e=!b||b&&"orientationchange"===b.type;if(u&&(u=!1,c)){if(!b||"scroll"!==b.type||e)c.autoSize&&"iframe"!==c.type&&(a.inner.height("auto"),c.height=a.inner.height()),(c.autoResize||e)&&a._setDimension(),c.canGrow&&"iframe"!==c.type&&a.inner.height("auto");(c.autoCenter||e)&&a.reposition(b);a.trigger("onUpdate")}},
200),u=!0)},toggle:function(){a.isOpen&&(a.current.fitToView=!a.current.fitToView,a.update())},hideLoading:function(){q.unbind("keypress.fb");d("#fancybox-loading").remove()},showLoading:function(){a.hideLoading();q.bind("keypress.fb",function(b){27===b.keyCode&&(b.preventDefault(),a.cancel())});d('<div id="fancybox-loading"><div></div></div>').click(a.cancel).appendTo("body")},getViewport:function(){return{x:m.scrollLeft(),y:m.scrollTop(),w:k&&s.innerWidth?s.innerWidth:m.width(),h:k&&s.innerHeight?
s.innerHeight:m.height()}},unbindEvents:function(){a.wrap&&a.wrap.unbind(".fb");q.unbind(".fb");m.unbind(".fb")},bindEvents:function(){var b=a.current,c=b.keys;b&&(m.bind("resize.fb orientationchange.fb"+(b.autoCenter&&!b.fixed?" scroll.fb":""),a.update),c&&q.bind("keydown.fb",function(b){var f;f=b.target||b.srcElement;if(!b.ctrlKey&&!b.altKey&&!b.shiftKey&&!b.metaKey&&(!f||!f.type&&!d(f).is("[contenteditable]")))f=b.keyCode,-1<d.inArray(f,c.close)?(a.close(),b.preventDefault()):-1<d.inArray(f,c.next)?
(a.next(),b.preventDefault()):-1<d.inArray(f,c.prev)&&(a.prev(),b.preventDefault())}),d.fn.mousewheel&&b.mouseWheel&&1<a.group.length&&a.wrap.bind("mousewheel.fb",function(b,c){var d=b.target||null;if(0!==c&&(!d||0===d.clientHeight||d.scrollHeight===d.clientHeight&&d.scrollWidth===d.clientWidth))b.preventDefault(),a[0<c?"prev":"next"]()}))},trigger:function(b,c){var e,f=c||a[-1<d.inArray(b,["onCancel","beforeLoad","afterLoad"])?"coming":"current"];if(f){d.isFunction(f[b])&&(e=f[b].apply(f,Array.prototype.slice.call(arguments,
1)));if(!1===e)return!1;f.helpers&&d.each(f.helpers,function(c,e){if(e&&d.isPlainObject(a.helpers[c])&&d.isFunction(a.helpers[c][b]))a.helpers[c][b](e,f)});d.event.trigger(b+".fb")}},isImage:function(a){return o(a)&&a.match(/\.(jpe?g|gif|png|bmp)((\?|#).*)?$/i)},isSWF:function(a){return o(a)&&a.match(/\.(swf)((\?|#).*)?$/i)},_start:function(b){var c={},e=a.group[b]||null,f,g,i;if(e&&(e.nodeType||e instanceof d))f=!0,d.metadata&&(c=d(e).metadata());c=d.extend(!0,{},a.opts,{index:b,element:e},d.isPlainObject(e)?
e:c);d.each(["href","title","content","type"],function(b,g){c[g]=a.opts[g]||f&&d(e).attr(g)||c[g]||null});"number"===typeof c.margin&&(c.margin=[c.margin,c.margin,c.margin,c.margin]);c.modal&&d.extend(!0,c,{closeBtn:!1,closeClick:!1,nextClick:!1,arrows:!1,mouseWheel:!1,keys:null,helpers:{overlay:{css:{cursor:"auto"},closeClick:!1}}});a.coming=c;if(!1===a.trigger("beforeLoad"))a.coming=null;else{g=c.type;b=c.href||e;g||(f&&(g=d(e).data("fancybox-type"),g||(g=(g=e.className.match(/fancybox\.(\w+)/))?
g[1]:null)),!g&&o(b)&&(a.isImage(b)?g="image":a.isSWF(b)?g="swf":b.match(/^#/)&&(g="inline")),g||(g=f?"inline":"html"),c.type=g);if("inline"===g||"html"===g){if(c.content||(c.content="inline"===g?d(o(b)?b.replace(/.*(?=#[^\s]+$)/,""):b):e),!c.content||!c.content.length)g=null}else b||(g=null);"ajax"===g&&o(b)&&(i=b.split(/\s+/,2),b=i.shift(),c.selector=i.shift());c.href=b;c.group=a.group;c.isDom=f;switch(g){case "image":a._loadImage();break;case "ajax":a._loadAjax();break;case "inline":case "iframe":case "swf":case "html":a._afterLoad();
break;default:a._error("type")}}},_error:function(b){a.hideLoading();d.extend(a.coming,{type:"html",autoSize:!0,minWidth:0,minHeight:0,padding:15,hasError:b,content:a.coming.tpl.error});a._afterLoad()},_loadImage:function(){var b=a.imgPreload=new Image;b.onload=function(){this.onload=this.onerror=null;a.coming.width=this.width;a.coming.height=this.height;a._afterLoad()};b.onerror=function(){this.onload=this.onerror=null;a._error("image")};b.src=a.coming.href;(b.complete===t||!b.complete)&&a.showLoading()},
_loadAjax:function(){a.showLoading();a.ajaxLoad=d.ajax(d.extend({},a.coming.ajax,{url:a.coming.href,error:function(b,c){a.coming&&"abort"!==c?a._error("ajax",b):a.hideLoading()},success:function(b,c){"success"===c&&(a.coming.content=b,a._afterLoad())}}))},_preloadImages:function(){var b=a.group,c=a.current,e=b.length,f,g,i,h=Math.min(c.preload,e-1);if(c.preload&&!(2>b.length))for(i=1;i<=h;i+=1)if(f=b[(c.index+i)%e],g=f.href||d(f).attr("href")||f,"image"===f.type||a.isImage(g))(new Image).src=g},_afterLoad:function(){a.hideLoading();
!a.coming||!1===a.trigger("afterLoad",a.current)?a.coming=!1:(a.isOpened?(d(".fancybox-item, .fancybox-nav").remove(),a.wrap.stop(!0).removeClass("fancybox-opened"),a.inner.css("overflow","hidden"),a.transitions[a.current.prevMethod]()):(d(".fancybox-wrap").stop().trigger("onReset").remove(),a.trigger("afterClose")),a.unbindEvents(),a.isOpen=!1,a.current=a.coming,a.wrap=d(a.current.tpl.wrap).addClass("fancybox-"+(k?"mobile":"desktop")+" fancybox-type-"+a.current.type+" fancybox-tmp "+a.current.wrapCSS).appendTo("body"),
a.skin=d(".fancybox-skin",a.wrap).css("padding",n(a.current.padding)),a.outer=d(".fancybox-outer",a.wrap),a.inner=d(".fancybox-inner",a.wrap),a._setContent())},_setContent:function(){var b=a.current,c=b.content,e=b.type,f=b.minWidth,g=b.minHeight,i=b.maxWidth,h=b.maxHeight;switch(e){case "inline":case "ajax":case "html":b.selector?c=d("<div>").html(c).find(b.selector):c instanceof d&&(c.parent().hasClass("fancybox-inner")&&c.parents(".fancybox-wrap").unbind("onReset"),c=c.show().detach(),d(a.wrap).bind("onReset",
function(){c.appendTo("body").hide()}));b.autoSize&&(f=d('<div class="fancybox-wrap '+a.current.wrapCSS+' fancybox-tmp"></div>').appendTo("body").css({minWidth:n(f,"w"),minHeight:n(g,"h"),maxWidth:n(i,"w"),maxHeight:n(h,"h")}).append(c),b.width=f.width(),b.height=f.height(),f.width(a.current.width),f.height()>b.height&&(f.width(b.width+1),b.width=f.width(),b.height=f.height()),c=f.contents().detach(),f.remove());break;case "image":c=b.tpl.image.replace("{href}",b.href);b.aspectRatio=!0;break;case "swf":c=
b.tpl.swf.replace(/\{width\}/g,b.width).replace(/\{height\}/g,b.height).replace(/\{href\}/g,b.href);break;case "iframe":c=d(b.tpl.iframe.replace("{rnd}",(new Date).getTime())).attr("scrolling",b.scrolling).attr("src",b.href),b.scrolling=k?"scroll":"auto"}if("image"===e||"swf"===e)b.autoSize=!1,b.scrolling="visible";"iframe"===e&&b.autoSize?(a.showLoading(),a._setDimension(),a.inner.css("overflow",b.scrolling),c.bind({onCancel:function(){d(this).unbind();a._afterZoomOut()},load:function(){a.hideLoading();
try{this.contentWindow.document.location&&(a.current.height=d(this).contents().find("body").height())}catch(b){a.current.autoSize=!1}a[a.isOpen?"_afterZoomIn":"_beforeShow"]()}}).appendTo(a.inner)):(a.inner.append(c),a._beforeShow())},_beforeShow:function(){a.coming=null;a.trigger("beforeShow");a._setDimension();a.wrap.hide().removeClass("fancybox-tmp");a.bindEvents();a._preloadImages();a.transitions[a.isOpened?a.current.nextMethod:a.current.openMethod]()},_setDimension:function(){var b=a.wrap,c=
a.inner,e=a.current,f=a.getViewport(),g=e.margin,i=2*e.padding,h=e.width,j=e.height,r=e.maxWidth+i,k=e.maxHeight+i,l=e.minWidth+i,m=e.minHeight+i,p;f.w-=g[1]+g[3];f.h-=g[0]+g[2];o(h)&&0<h.indexOf("%")&&(h=(f.w-i)*parseFloat(h)/100);o(j)&&0<j.indexOf("%")&&(j=(f.h-i)*parseFloat(j)/100);g=h/j;h+=i;j+=i;e.fitToView&&(r=Math.min(f.w,r),k=Math.min(f.h,k));if(e.aspectRatio){if(h>r&&(h=r,j=(h-i)/g+i),j>k&&(j=k,h=(j-i)*g+i),h<l&&(h=l,j=(h-i)/g+i),j<m)j=m,h=(j-i)*g+i}else h=Math.max(l,Math.min(h,r)),j=Math.max(m,
Math.min(j,k));h=Math.round(h);j=Math.round(j);d(b.add(c)).width("auto").height("auto");c.width(h-i).height(j-i);b.width(h);p=b.height();if(h>r||p>k)for(;(h>r||p>k)&&h>l&&p>m;)j-=10,e.aspectRatio?(h=Math.round((j-i)*g+i),h<l&&(h=l,j=(h-i)/g+i)):h-=10,c.width(h-i).height(j-i),b.width(h),p=b.height();e.dim={width:n(h),height:n(p)};e.canGrow=e.autoSize&&j>m&&j<k;e.canShrink=!1;e.canExpand=!1;if(h-i<e.width||j-i<e.height)e.canExpand=!0;else if((h>f.w||p>f.h)&&h>l&&j>m)e.canShrink=!0;a.innerSpace=p-i-
c.height()},_getPosition:function(b){var c=a.current,e=a.getViewport(),f=c.margin,d=a.wrap.width()+f[1]+f[3],i=a.wrap.height()+f[0]+f[2],h={position:"absolute",top:f[0]+e.y,left:f[3]+e.x};c.autoCenter&&c.fixed&&!b&&i<=e.h&&d<=e.w&&(h={position:"fixed",top:f[0],left:f[3]});h.top=n(Math.max(h.top,h.top+(e.h-i)*c.topRatio));h.left=n(Math.max(h.left,h.left+0.5*(e.w-d)));return h},_afterZoomIn:function(){var b=a.current,c=b?b.scrolling:"no";if(b&&(a.isOpen=a.isOpened=!0,a.wrap.addClass("fancybox-opened"),
a.inner.css("overflow","yes"===c?"scroll":"no"===c?"hidden":c),a.trigger("afterShow"),a.update(),(b.closeClick||b.nextClick)&&a.inner.css("cursor","pointer").bind("click.fb",function(c){if(!d(c.target).is("a")&&!d(c.target).parent().is("a"))a[b.closeClick?"close":"next"]()}),b.closeBtn&&d(b.tpl.closeBtn).appendTo(a.skin).bind("click.fb",a.close),b.arrows&&1<a.group.length&&((b.loop||0<b.index)&&d(b.tpl.prev).appendTo(a.outer).bind("click.fb",a.prev),(b.loop||b.index<a.group.length-1)&&d(b.tpl.next).appendTo(a.outer).bind("click.fb",
a.next)),a.opts.autoPlay&&!a.player.isActive))a.opts.autoPlay=!1,a.play()},_afterZoomOut:function(){var b=a.current;a.wrap.trigger("onReset").remove();d.extend(a,{group:{},opts:{},current:null,isActive:!1,isOpened:!1,isOpen:!1,wrap:null,skin:null,outer:null,inner:null});a.trigger("afterClose",b)}});a.transitions={getOrigPosition:function(){var b=a.current,c=b.element,e=b.padding,f=d(b.orig),g={},i=50,h=50;!f.length&&b.isDom&&d(c).is(":visible")&&(f=d(c).find("img:first"),f.length||(f=d(c)));f.length?
(g=f.offset(),f.is("img")&&(i=f.outerWidth(),h=f.outerHeight())):(b=a.getViewport(),g.top=b.y+0.5*(b.h-h),g.left=b.x+0.5*(b.w-i));return g={top:n(g.top-e),left:n(g.left-e),width:n(i+2*e),height:n(h+2*e)}},step:function(b,c){var e=c.prop,d,g;if("width"===e||"height"===e)d=Math.ceil(b-2*a.current.padding),"height"===e&&(g=(b-c.start)/(c.end-c.start),c.start>c.end&&(g=1-g),d-=a.innerSpace*g),a.inner[e](d)},zoomIn:function(){var b=a.wrap,c=a.current,e=c.openEffect,f="elastic"===e,g=d.extend({},c.dim,
a._getPosition(f)),i=d.extend({opacity:1},g);delete i.position;f?(g=this.getOrigPosition(),c.openOpacity&&(g.opacity=0),a.outer.add(a.inner).width("auto").height("auto")):"fade"===e&&(g.opacity=0);b.css(g).show().animate(i,{duration:"none"===e?0:c.openSpeed,easing:c.openEasing,step:f?this.step:null,complete:a._afterZoomIn})},zoomOut:function(){var b=a.wrap,c=a.current,d=c.openEffect,f="elastic"===d,g={opacity:0};f&&("fixed"===b.css("position")&&b.css(a._getPosition(!0)),g=this.getOrigPosition(),c.closeOpacity&&
(g.opacity=0));b.animate(g,{duration:"none"===d?0:c.closeSpeed,easing:c.closeEasing,step:f?this.step:null,complete:a._afterZoomOut})},changeIn:function(){var b=a.wrap,c=a.current,d=c.nextEffect,f="elastic"===d,g=a._getPosition(f),i={opacity:1};g.opacity=0;f&&(g.top=n(parseInt(g.top,10)-200),i.top="+=200px");b.css(g).show().animate(i,{duration:"none"===d?0:c.nextSpeed,easing:c.nextEasing,complete:a._afterZoomIn})},changeOut:function(){var b=a.wrap,c=a.current,e=c.prevEffect,f={opacity:0};b.removeClass("fancybox-opened");
"elastic"===e&&(f.top="+=200px");b.animate(f,{duration:"none"===e?0:c.prevSpeed,easing:c.prevEasing,complete:function(){d(this).trigger("onReset").remove()}})}};a.helpers.overlay={overlay:null,update:function(){var a,c;this.overlay.width("100%").height("100%");d.browser.msie||k?(a=Math.max(l.documentElement.scrollWidth,l.body.scrollWidth),c=Math.max(l.documentElement.offsetWidth,l.body.offsetWidth),a=a<c?m.width():a):a=q.width();this.overlay.width(a).height(q.height())},beforeShow:function(b){this.overlay||
(b=d.extend(!0,{},a.defaults.helpers.overlay,b),this.overlay=d('<div id="fancybox-overlay"></div>').css(b.css).appendTo("body"),b.closeClick&&this.overlay.bind("click.fb",a.close),a.current.fixed&&!k?this.overlay.addClass("overlay-fixed"):(this.update(),this.onUpdate=function(){this.update()}),this.overlay.fadeTo(b.speedIn,b.opacity))},afterClose:function(a){this.overlay&&this.overlay.fadeOut(a.speedOut||0,function(){d(this).remove()});this.overlay=null}};a.helpers.title={beforeShow:function(b){var c;
if(c=a.current.title)c=d('<div class="fancybox-title fancybox-title-'+b.type+'-wrap">'+c+"</div>").appendTo("body"),"float"===b.type&&(c.width(c.width()),c.wrapInner('<span class="child"></span>'),a.current.margin[2]+=Math.abs(parseInt(c.css("margin-bottom"),10))),c.appendTo("over"===b.type?a.inner:"outside"===b.type?a.wrap:a.skin)}};d.fn.fancybox=function(b){var c=d(this),e=this.selector||"",f,g=function(g){var h=this,j=f,k;!g.ctrlKey&&!g.altKey&&!g.shiftKey&&!g.metaKey&&!d(h).is(".fancybox-wrap")&&
(g.preventDefault(),g=b.groupAttr||"data-fancybox-group",k=d(h).attr(g),k||(g="rel",k=h[g]),k&&""!==k&&"nofollow"!==k&&(h=e.length?d(e):c,h=h.filter("["+g+'="'+k+'"]'),j=h.index(this)),b.index=j,a.open(h,b))},b=b||{};f=b.index||0;e?q.undelegate(e,"click.fb-start").delegate(e,"click.fb-start",g):c.unbind("click.fb-start").bind("click.fb-start",g);return this};d(l).ready(function(){a.defaults.fixed=d.support.fixedPosition||!(d.browser.msie&&6>=d.browser.version)&&!k})})(window,document,jQuery);

 /*!
 * Media helper for fancyBox
 * version: 1.0.0
 * @requires fancyBox v2.0 or later
 *
 * Usage:
 *     $(".fancybox").fancybox({
 *         media: {}
 *     });
 *
 *  Supports:
 *      Youtube
 *          http://www.youtube.com/watch?v=opj24KnzrWo
 *          http://youtu.be/opj24KnzrWo
 *      Vimeo
 *          http://vimeo.com/25634903
 *      Metacafe
 *          http://www.metacafe.com/watch/7635964/dr_seuss_the_lorax_movie_trailer/
 *          http://www.metacafe.com/watch/7635964/
 *      Dailymotion
 *          http://www.dailymotion.com/video/xoytqh_dr-seuss-the-lorax-premiere_people
 *      Twitvid
 *          http://twitvid.com/QY7MD
 *      Twitpic
 *          http://twitpic.com/7p93st
 *      Instagram
 *          http://instagr.am/p/IejkuUGxQn/
 *          http://instagram.com/p/IejkuUGxQn/
 *      Google maps
 *          http://maps.google.com/maps?q=Eiffel+Tower,+Avenue+Gustave+Eiffel,+Paris,+France&t=h&z=17
 *          http://maps.google.com/?ll=48.857995,2.294297&spn=0.007666,0.021136&t=m&z=16
 *          http://maps.google.com/?ll=48.859463,2.292626&spn=0.000965,0.002642&t=m&z=19&layer=c&cbll=48.859524,2.292532&panoid=YJ0lq28OOy3VT2IqIuVY0g&cbp=12,151.58,,0,-15.56
 */
(function ($) {
	//Shortcut for fancyBox object
	var F = $.fancybox;

	//Add helper object
	F.helpers.media = {
		beforeLoad : function(opts, obj) {
			var href = obj.href || '',
				type = false,
				rez;

			if ((rez = href.match(/(youtube\.com|youtu\.be)\/(v\/|u\/|embed\/|watch\?v=)?([^#\&\?]*).*/i))) {
				href = '//www.youtube.com/embed/' + rez[3] + '?autoplay=0&autohide=1&fs=1&rel=0&enablejsapi=1&wmode=opaque';
				type = 'iframe';

			} else if ((rez = href.match(/vimeo.com\/(\d+)\/?(.*)/))) {
				href = '//player.vimeo.com/video/' + rez[1] + '?hd=1&autoplay=0&show_title=1&show_byline=1&show_portrait=0&color=&fullscreen=1&wmode=opaque';
				type = 'iframe';

			} else if ((rez = href.match(/metacafe.com\/watch\/(\d+)\/?(.*)/))) {
				href = '//www.metacafe.com/fplayer/' + rez[1] + '/.swf?playerVars=autoPlay=yes';
				type = 'swf';

			} else if ((rez = href.match(/dailymotion.com\/video\/(.*)\/?(.*)/))) {
				href = '//www.dailymotion.com/swf/video/' + rez[1] + '?additionalInfos=0&autoStart=1';
				type = 'swf';

			} else if ((rez = href.match(/twitvid\.com\/([a-zA-Z0-9_\-\?\=]+)/i))) {
				href = '//www.twitvid.com/embed.php?autoplay=0&guid=' + rez[1];
				type = 'iframe';

			} else if ((rez = href.match(/twitpic\.com\/(?!(?:place|photos|events)\/)([a-zA-Z0-9\?\=\-]+)/i))) {
				href = '//twitpic.com/show/full/' + rez[1];
				type = 'image';

			} else if ((rez = href.match(/(instagr\.am|instagram\.com)\/p\/([a-zA-Z0-9_\-]+)\/?/i))) {
				href = '//' + rez[1] + '/p/' + rez[2] + '/media/?size=l';
				type = 'image';

			} else if ((rez = href.match(/maps\.google\.com\/(\?ll=|maps\/?\?q=)(.*)/i))) {
				href = '//maps.google.com/' + rez[1] + '' + rez[2] + '&output=' + (rez[2].indexOf('layer=c') ? 'svembed' : 'embed');
				type = 'iframe';
			}

			if (type) {
				obj.href = href;
				obj.type = type;
			}
		}
	}

}(jQuery));

/*! Copyright (c) 2011 Brandon Aaron (http://brandonaaron.net)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Thanks to: http://adomas.org/javascript-mouse-wheel/ for some pointers.
 * Thanks to: Mathias Bank(http://www.mathias-bank.de) for a scope bug fix.
 * Thanks to: Seamus Leahy for adding deltaX and deltaY
 *
 * Version: 3.0.6
 *
 * Requires: 1.2.2+
 */
(function(d){function e(a){var b=a||window.event,c=[].slice.call(arguments,1),f=0,e=0,g=0,a=d.event.fix(b);a.type="mousewheel";b.wheelDelta&&(f=b.wheelDelta/120);b.detail&&(f=-b.detail/3);g=f;b.axis!==void 0&&b.axis===b.HORIZONTAL_AXIS&&(g=0,e=-1*f);b.wheelDeltaY!==void 0&&(g=b.wheelDeltaY/120);b.wheelDeltaX!==void 0&&(e=-1*b.wheelDeltaX/120);c.unshift(a,f,e,g);return(d.event.dispatch||d.event.handle).apply(this,c)}var c=["DOMMouseScroll","mousewheel"];if(d.event.fixHooks)for(var h=c.length;h;)d.event.fixHooks[c[--h]]=
d.event.mouseHooks;d.event.special.mousewheel={setup:function(){if(this.addEventListener)for(var a=c.length;a;)this.addEventListener(c[--a],e,false);else this.onmousewheel=e},teardown:function(){if(this.removeEventListener)for(var a=c.length;a;)this.removeEventListener(c[--a],e,false);else this.onmousewheel=null}};d.fn.extend({mousewheel:function(a){return a?this.bind("mousewheel",a):this.trigger("mousewheel")},unmousewheel:function(a){return this.unbind("mousewheel",a)}})})(jQuery);

/**
 * jQuery Validation Plugin 1.8.1
 *
 * http://bassistance.de/jquery-plugins/jquery-plugin-validation/
 * http://docs.jquery.com/Plugins/Validation
 *
 * Copyright (c) 2006 - 2011 Jörn Zaefferer
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 */

//changed line 349
// bug http://plugins.jquery.com/content/validation-checkbox-group-causes-incorrect-registration-valid-and-invalid-elements
//changed line 319
// bug http://plugins.jquery.com/content/support-html-5-input-types-number-email-url
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('(7($){$.K($.2E,{16:7(c){l(!6.F){c&&c.28&&2F.1x&&1x.4F("3k 3l, 4G\'t 16, 4H 3k");8}p d=$.17(6[0],\'v\');l(d){8 d}d=29 $.v(c,6[0]);$.17(6[0],\'v\',d);l(d.q.3m){6.2G("1y, 3n").1z(".4I").2H(7(){d.2I=w});l(d.q.2J){6.2G("1y, 3n").1z(":2a").2H(7(){d.1O=6})}6.2a(7(b){l(d.q.28)b.4J();7 1P(){l(d.q.2J){l(d.1O){p a=$("<1y 12=\'4K\'/>").1j("u",d.1O.u).2K(d.1O.Y).4L(d.V)}d.q.2J.Z(d,d.V);l(d.1O){a.3o()}8 N}8 w}l(d.2I){d.2I=N;8 1P()}l(d.L()){l(d.1a){d.1k=w;8 N}8 1P()}W{d.2b();8 N}})}8 d},H:7(){l($(6[0]).2L(\'L\')){8 6.16().L()}W{p a=w;p b=$(6[0].L).16();6.P(7(){a&=b.I(6)});8 a}},4M:7(c){p d={},$I=6;$.P(c.1Q(/\\s/),7(a,b){d[b]=$I.1j(b);$I.4N(b)});8 d},1b:7(c,d){p e=6[0];l(c){p f=$.17(e.L,\'v\').q;p g=f.1b;p h=$.v.2M(e);2c(c){1c"1l":$.K(h,$.v.1R(d));g[e.u]=h;l(d.G)f.G[e.u]=$.K(f.G[e.u],d.G);2N;1c"3o":l(!d){Q g[e.u];8 h}p i={};$.P(d.1Q(/\\s/),7(a,b){i[b]=h[b];Q h[b]});8 i}}p j=$.v.3p($.K({},$.v.3q(e),$.v.3r(e),$.v.3s(e),$.v.2M(e)),e);l(j.13){p k=j.13;Q j.13;j=$.K({13:k},j)}8 j}});$.K($.4O[":"],{4P:7(a){8!$.1m(""+a.Y)},4Q:7(a){8!!$.1m(""+a.Y)},4R:7(a){8!a.3t}});$.v=7(a,b){6.q=$.K(w,{},$.v.2O,a);6.V=b;6.3u()};$.v.11=7(b,c){l(R.F==1)8 7(){p a=$.3v(R);a.4S(b);8 $.v.11.1S(6,a)};l(R.F>2&&c.2d!=3w){c=$.3v(R).4T(1)}l(c.2d!=3w){c=[c]}$.P(c,7(i,n){b=b.1A(29 3x("\\\\{"+i+"\\\\}","g"),n)});8 b};$.K($.v,{2O:{G:{},2e:{},1b:{},1d:"3y",2f:"H",2P:"4U",2b:w,3z:$([]),2Q:$([]),3m:w,2R:[],3A:N,4V:7(a){6.3B=a;l(6.q.4W&&!6.4X){6.q.1T&&6.q.1T.Z(6,a,6.q.1d,6.q.2f);6.2g(6.1U(a)).2S()}},4Y:7(a){l(!6.1n(a)&&(a.u S 6.1e||!6.J(a))){6.I(a)}},4Z:7(a){l(a.u S 6.1e||a==6.3C){6.I(a)}},50:7(a){l(a.u S 6.1e)6.I(a);W l(a.3D.u S 6.1e)6.I(a.3D)},2T:7(a,b,c){l(a.12===\'2h\'){6.1o(a.u).1p(b).1B(c)}W{$(a).1p(b).1B(c)}},1T:7(a,b,c){l(a.12===\'2h\'){6.1o(a.u).1B(b).1p(c)}W{$(a).1B(b).1p(c)}}},51:7(a){$.K($.v.2O,a)},G:{13:"52 3E 2L 13.",1q:"M 2U 6 3E.",1C:"M O a H 1C 53.",1r:"M O a H 54.",1D:"M O a H 1D.",2i:"M O a H 1D (55).",1E:"M O a H 1E.",1V:"M O 56 1V.",2j:"M O a H 57 58 1E.",2k:"M O 3F 59 Y 5a.",3G:"M O a Y 5b a H 5c.",18:$.v.11("M O 3H 5d 2V {0} 2W."),1F:$.v.11("M O 5e 5f {0} 2W."),2l:$.v.11("M O a Y 3I {0} 3J {1} 2W 5g."),2m:$.v.11("M O a Y 3I {0} 3J {1}."),1G:$.v.11("M O a Y 5h 2V 3K 3L 3M {0}."),1H:$.v.11("M O a Y 5i 2V 3K 3L 3M {0}.")},3N:N,5j:{3u:7(){6.2n=$(6.q.2Q);6.3O=6.2n.F&&6.2n||$(6.V);6.2o=$(6.q.3z).1l(6.q.2Q);6.1e={};6.5k={};6.1a=0;6.1f={};6.1g={};6.1W();p e=(6.2e={});$.P(6.q.2e,7(c,d){$.P(d.1Q(/\\s/),7(a,b){e[b]=c})});p f=6.q.1b;$.P(f,7(a,b){f[a]=$.v.1R(b)});7 2X(a){p b=$.17(6[0].L,"v"),2Y="5l"+a.12.1A(/^16/,"");b.q[2Y]&&b.q[2Y].Z(b,6[0])}$(6.V).2Z(":3P, [12=1C], [12=5m], [12=1r], :5n, :5o, 1X, 3Q","2p 30 5p",2X).2Z(":2h, :3R, 1X, 3S","2H",2X);l(6.q.3T)$(6.V).31("1g-L.16",6.q.3T)},L:7(){6.3U();$.K(6.1e,6.1I);6.1g=$.K({},6.1I);l(!6.H())$(6.V).3V("1g-L",[6]);6.1s();8 6.H()},3U:7(){6.32();T(p i=0,19=(6.2q=6.19());19[i];i++){6.2r(19[i])}8 6.H()},I:7(a){a=6.33(a);6.3C=a;6.34(a);6.2q=$(6.1n(a)?6.1o(a.u)[0]:a);p b=6.2r(a);l(b){Q 6.1g[a.u]}W{6.1g[a.u]=w}l(!6.3W()){6.14=6.14.1l(6.2o)}6.1s();8 b},1s:7(b){l(b){$.K(6.1I,b);6.U=[];T(p c S b){6.U.2s({1t:b[c],I:6.1o(c)[0]})}6.1u=$.3X(6.1u,7(a){8!(a.u S b)})}6.q.1s?6.q.1s.Z(6,6.1I,6.U):6.3Y()},35:7(){l($.2E.35)$(6.V).35();6.1e={};6.32();6.36();6.19().1B(6.q.1d)},3W:7(){8 6.2t(6.1g)},2t:7(a){p b=0;T(p i S a)b++;8 b},36:7(){6.2g(6.14).2S()},H:7(){8 6.3Z()==0},3Z:7(){8 6.U.F},2b:7(){l(6.q.2b){40{$(6.41()||6.U.F&&6.U[0].I||[]).1z(":5q").42().5r("2p")}43(e){}}},41:7(){p a=6.3B;8 a&&$.3X(6.U,7(n){8 n.I.u==a.u}).F==1&&a},19:7(){p a=6,37={};8 $(6.V).2G("1y, 1X, 3Q").1J(":2a, :1W, :5s, [5t]").1J(6.q.2R).1z(7(){!6.u&&a.q.28&&2F.1x&&1x.3y("%o 5u 3H u 5v",6);l(6.u S 37||!a.2t($(6).1b()))8 N;37[6.u]=w;8 w})},33:7(a){8 $(a)[0]},38:7(){8 $(6.q.2P+"."+6.q.1d,6.3O)},1W:7(){6.1u=[];6.U=[];6.1I={};6.1v=$([]);6.14=$([]);6.2q=$([])},32:7(){6.1W();6.14=6.38().1l(6.2o)},34:7(a){6.1W();6.14=6.1U(a)},2r:7(a){a=6.33(a);l(6.1n(a)){a=6.1o(a.u).1J(6.q.2R)[0]}p b=$(a).1b();p c=N;T(p d S b){p f={2u:d,2v:b[d]};40{p g=$.v.1Y[d].Z(6,a.Y.1A(/\\r/g,""),a,f.2v);l(g=="1Z-20"){c=w;5w}c=N;l(g=="1f"){6.14=6.14.1J(6.1U(a));8}l(!g){6.44(a,f);8 N}}43(e){6.q.28&&2F.1x&&1x.5x("5y 5z 5A 5B I "+a.45+", 2r 3F \'"+f.2u+"\' 2u",e);5C e;}}l(c)8;l(6.2t(b))6.1u.2s(a);8 w},46:7(a,b){l(!$.1K)8;p c=6.q.39?$(a).1K()[6.q.39]:$(a).1K();8 c&&c.G&&c.G[b]},47:7(a,b){p m=6.q.G[a];8 m&&(m.2d==48?m:m[b])},49:7(){T(p i=0;i<R.F;i++){l(R[i]!==21)8 R[i]}8 21},2w:7(a,b){8 6.49(6.47(a.u,b),6.46(a,b),!6.q.3A&&a.5D||21,$.v.G[b],"<4a>5E: 5F 1t 5G T "+a.u+"</4a>")},44:7(a,b){p c=6.2w(a,b.2u),3a=/\\$?\\{(\\d+)\\}/g;l(1h c=="7"){c=c.Z(6,b.2v,a)}W l(3a.15(c)){c=1L.11(c.1A(3a,\'{$1}\'),b.2v)}6.U.2s({1t:c,I:a});6.1I[a.u]=c;6.1e[a.u]=c},2g:7(a){l(6.q.2x)a=a.1l(a.4b(6.q.2x));8 a},3Y:7(){T(p i=0;6.U[i];i++){p a=6.U[i];6.q.2T&&6.q.2T.Z(6,a.I,6.q.1d,6.q.2f);6.3b(a.I,a.1t)}l(6.U.F){6.1v=6.1v.1l(6.2o)}l(6.q.1M){T(p i=0;6.1u[i];i++){6.3b(6.1u[i])}}l(6.q.1T){T(p i=0,19=6.4c();19[i];i++){6.q.1T.Z(6,19[i],6.q.1d,6.q.2f)}}6.14=6.14.1J(6.1v);6.36();6.2g(6.1v).4d()},4c:7(){8 6.2q.1J(6.4e())},4e:7(){8 $(6.U).4f(7(){8 6.I})},3b:7(a,b){p c=6.1U(a);l(c.F){c.1B().1p(6.q.1d);c.1j("4g")&&c.4h(b)}W{c=$("<"+6.q.2P+"/>").1j({"T":6.3c(a),4g:w}).1p(6.q.1d).4h(b||"");l(6.q.2x){c=c.2S().4d().5H("<"+6.q.2x+"/>").4b()}l(!6.2n.5I(c).F)6.q.4i?6.q.4i(c,$(a)):c.5J(a)}l(!b&&6.q.1M){c.3P("");1h 6.q.1M=="1N"?c.1p(6.q.1M):6.q.1M(c)}6.1v=6.1v.1l(c)},1U:7(a){p b=6.3c(a);8 6.38().1z(7(){8 $(6).1j(\'T\')==b})},3c:7(a){8 6.2e[a.u]||(6.1n(a)?a.u:a.45||a.u)},1n:7(a){8/2h|3R/i.15(a.12)},1o:7(c){p d=6.V;8 $(4j.5K(c)).4f(7(a,b){8 b.L==d&&b.u==c&&b||4k})},22:7(a,b){2c(b.4l.4m()){1c\'1X\':8 $("3S:3l",b).F;1c\'1y\':l(6.1n(b))8 6.1o(b.u).1z(\':3t\').F}8 a.F},4n:7(a,b){8 6.3d[1h a]?6.3d[1h a](a,b):w},3d:{"5L":7(a,b){8 a},"1N":7(a,b){8!!$(a,b.L).F},"7":7(a,b){8 a(b)}},J:7(a){8!$.v.1Y.13.Z(6,$.1m(a.Y),a)&&"1Z-20"},4o:7(a){l(!6.1f[a.u]){6.1a++;6.1f[a.u]=w}},4p:7(a,b){6.1a--;l(6.1a<0)6.1a=0;Q 6.1f[a.u];l(b&&6.1a==0&&6.1k&&6.L()){$(6.V).2a();6.1k=N}W l(!b&&6.1a==0&&6.1k){$(6.V).3V("1g-L",[6]);6.1k=N}},2y:7(a){8 $.17(a,"2y")||$.17(a,"2y",{3e:4k,H:w,1t:6.2w(a,"1q")})}},23:{13:{13:w},1C:{1C:w},1r:{1r:w},1D:{1D:w},2i:{2i:w},4q:{4q:w},1E:{1E:w},4r:{4r:w},1V:{1V:w},2j:{2j:w}},4s:7(a,b){a.2d==48?6.23[a]=b:$.K(6.23,a)},3r:7(a){p b={};p c=$(a).1j(\'5M\');c&&$.P(c.1Q(\' \'),7(){l(6 S $.v.23){$.K(b,$.v.23[6])}});8 b},3s:7(a){p b={};p c=$(a);T(p d S $.v.1Y){p e=c.1j(d);l(e){b[d]=e}}l(b.18&&/-1|5N|5O/.15(b.18)){Q b.18}8 b},3q:7(a){l(!$.1K)8{};p b=$.17(a.L,\'v\').q.39;8 b?$(a).1K()[b]:$(a).1K()},2M:7(a){p b={};p c=$.17(a.L,\'v\');l(c.q.1b){b=$.v.1R(c.q.1b[a.u])||{}}8 b},3p:7(d,e){$.P(d,7(a,b){l(b===N){Q d[a];8}l(b.3f||b.2z){p c=w;2c(1h b.2z){1c"1N":c=!!$(b.2z,e.L).F;2N;1c"7":c=b.2z.Z(e,e);2N}l(c){d[a]=b.3f!==21?b.3f:w}W{Q d[a]}}});$.P(d,7(a,b){d[a]=$.4t(b)?b(e):b});$.P([\'1F\',\'18\',\'1H\',\'1G\'],7(){l(d[6]){d[6]=3g(d[6])}});$.P([\'2l\',\'2m\'],7(){l(d[6]){d[6]=[3g(d[6][0]),3g(d[6][1])]}});l($.v.3N){l(d.1H&&d.1G){d.2m=[d.1H,d.1G];Q d.1H;Q d.1G}l(d.1F&&d.18){d.2l=[d.1F,d.18];Q d.1F;Q d.18}}l(d.G){Q d.G}8 d},1R:7(a){l(1h a=="1N"){p b={};$.P(a.1Q(/\\s/),7(){b[6]=w});a=b}8 a},5P:7(a,b,c){$.v.1Y[a]=b;$.v.G[a]=c!=21?c:$.v.G[a];l(b.F<3){$.v.4s(a,$.v.1R(a))}},1Y:{13:7(a,b,c){l(!6.4n(c,b))8"1Z-20";2c(b.4l.4m()){1c\'1X\':p d=$(b).2K();8 d&&d.F>0;1c\'1y\':l(6.1n(b))8 6.22(a,b)>0;5Q:8 $.1m(a).F>0}},1q:7(f,g,h){l(6.J(g))8"1Z-20";p i=6.2y(g);l(!6.q.G[g.u])6.q.G[g.u]={};i.4u=6.q.G[g.u].1q;6.q.G[g.u].1q=i.1t;h=1h h=="1N"&&{1r:h}||h;l(6.1f[g.u]){8"1f"}l(i.3e===f){8 i.H}i.3e=f;p j=6;6.4o(g);p k={};k[g.u]=f;$.3h($.K(w,{1r:h,2A:"24",1i:"16"+g.u,5R:"5S",17:k,1M:7(a){j.q.G[g.u].1q=i.4u;p b=a===w;l(b){p c=j.1k;j.34(g);j.1k=c;j.1u.2s(g);j.1s()}W{p d={};p e=a||j.2w(g,"1q");d[g.u]=i.1t=$.4t(e)?e(f):e;j.1s(d)}i.H=b;j.4p(g,b)}},h));8"1f"},1F:7(a,b,c){8 6.J(b)||6.22($.1m(a),b)>=c},18:7(a,b,c){8 6.J(b)||6.22($.1m(a),b)<=c},2l:7(a,b,c){p d=6.22($.1m(a),b);8 6.J(b)||(d>=c[0]&&d<=c[1])},1H:7(a,b,c){8 6.J(b)||a>=c},1G:7(a,b,c){8 6.J(b)||a<=c},2m:7(a,b,c){8 6.J(b)||(a>=c[0]&&a<=c[1])},1C:7(a,b){8 6.J(b)||/^((([a-z]|\\d|[!#\\$%&\'\\*\\+\\-\\/=\\?\\^X`{\\|}~]|[\\x-\\y\\A-\\B\\C-\\E])+(\\.([a-z]|\\d|[!#\\$%&\'\\*\\+\\-\\/=\\?\\^X`{\\|}~]|[\\x-\\y\\A-\\B\\C-\\E])+)*)|((\\4v)((((\\2B|\\26)*(\\3i\\4w))?(\\2B|\\26)+)?(([\\4x-\\5T\\4y\\4z\\5U-\\5V\\4A]|\\5W|[\\5X-\\5Y]|[\\5Z-\\60]|[\\x-\\y\\A-\\B\\C-\\E])|(\\\\([\\4x-\\26\\4y\\4z\\3i-\\4A]|[\\x-\\y\\A-\\B\\C-\\E]))))*(((\\2B|\\26)*(\\3i\\4w))?(\\2B|\\26)+)?(\\4v)))@((([a-z]|\\d|[\\x-\\y\\A-\\B\\C-\\E])|(([a-z]|\\d|[\\x-\\y\\A-\\B\\C-\\E])([a-z]|\\d|-|\\.|X|~|[\\x-\\y\\A-\\B\\C-\\E])*([a-z]|\\d|[\\x-\\y\\A-\\B\\C-\\E])))\\.)+(([a-z]|[\\x-\\y\\A-\\B\\C-\\E])|(([a-z]|[\\x-\\y\\A-\\B\\C-\\E])([a-z]|\\d|-|\\.|X|~|[\\x-\\y\\A-\\B\\C-\\E])*([a-z]|[\\x-\\y\\A-\\B\\C-\\E])))\\.?$/i.15(a)},1r:7(a,b){8 6.J(b)||/^(61?|62):\\/\\/(((([a-z]|\\d|-|\\.|X|~|[\\x-\\y\\A-\\B\\C-\\E])|(%[\\27-f]{2})|[!\\$&\'\\(\\)\\*\\+,;=]|:)*@)?(((\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5]))|((([a-z]|\\d|[\\x-\\y\\A-\\B\\C-\\E])|(([a-z]|\\d|[\\x-\\y\\A-\\B\\C-\\E])([a-z]|\\d|-|\\.|X|~|[\\x-\\y\\A-\\B\\C-\\E])*([a-z]|\\d|[\\x-\\y\\A-\\B\\C-\\E])))\\.)+(([a-z]|[\\x-\\y\\A-\\B\\C-\\E])|(([a-z]|[\\x-\\y\\A-\\B\\C-\\E])([a-z]|\\d|-|\\.|X|~|[\\x-\\y\\A-\\B\\C-\\E])*([a-z]|[\\x-\\y\\A-\\B\\C-\\E])))\\.?)(:\\d*)?)(\\/((([a-z]|\\d|-|\\.|X|~|[\\x-\\y\\A-\\B\\C-\\E])|(%[\\27-f]{2})|[!\\$&\'\\(\\)\\*\\+,;=]|:|@)+(\\/(([a-z]|\\d|-|\\.|X|~|[\\x-\\y\\A-\\B\\C-\\E])|(%[\\27-f]{2})|[!\\$&\'\\(\\)\\*\\+,;=]|:|@)*)*)?)?(\\?((([a-z]|\\d|-|\\.|X|~|[\\x-\\y\\A-\\B\\C-\\E])|(%[\\27-f]{2})|[!\\$&\'\\(\\)\\*\\+,;=]|:|@)|[\\63-\\64]|\\/|\\?)*)?(\\#((([a-z]|\\d|-|\\.|X|~|[\\x-\\y\\A-\\B\\C-\\E])|(%[\\27-f]{2})|[!\\$&\'\\(\\)\\*\\+,;=]|:|@)|\\/|\\?)*)?$/i.15(a)},1D:7(a,b){8 6.J(b)||!/65|66/.15(29 67(a))},2i:7(a,b){8 6.J(b)||/^\\d{4}[\\/-]\\d{1,2}[\\/-]\\d{1,2}$/.15(a)},1E:7(a,b){8 6.J(b)||/^-?(?:\\d+|\\d{1,3}(?:,\\d{3})+)(?:\\.\\d+)?$/.15(a)},1V:7(a,b){8 6.J(b)||/^\\d+$/.15(a)},2j:7(a,b){l(6.J(b))8"1Z-20";l(/[^0-9-]+/.15(a))8 N;p c=0,e=0,2C=N;a=a.1A(/\\D/g,"");T(p n=a.F-1;n>=0;n--){p d=a.68(n);p e=69(d,10);l(2C){l((e*=2)>9)e-=9}c+=e;2C=!2C}8(c%10)==0},3G:7(a,b,c){c=1h c=="1N"?c.1A(/,/g,\'|\'):"6a|6b?g|6c";8 6.J(b)||a.6d(29 3x(".("+c+")$","i"))},2k:7(a,b,c){p d=$(c).6e(".16-2k").31("4B.16-2k",7(){$(b).H()});8 a==d.2K()}}});$.11=$.v.11})(1L);(7($){p d={};l($.4C){$.4C(7(a,X,b){p c=a.1i;l(a.2A=="24"){l(d[c]){d[c].24()}d[c]=b}})}W{p e=$.3h;$.3h=7(a){p b=("2A"S a?a:$.4D).2A,1i=("1i"S a?a:$.4D).1i;l(b=="24"){l(d[1i]){d[1i].24()}8(d[1i]=e.1S(6,R))}8 e.1S(6,R)}}})(1L);(7($){l(!1L.1w.3j.2p&&!1L.1w.3j.30&&4j.4E){$.P({42:\'2p\',4B:\'30\'},7(a,b){$.1w.3j[b]={6f:7(){6.4E(a,2D,w)},6g:7(){6.6h(a,2D,w)},2D:7(e){R[0]=$.1w.2U(e);R[0].12=b;8 $.1w.1P.1S(6,R)}};7 2D(e){e=$.1w.2U(e);e.12=b;8 $.1w.1P.Z(6,e)}})};$.K($.2E,{2Z:7(c,d,e){8 6.31(d,7(a){p b=$(a.6i);l(b.2L(c)){8 e.1S(b,R)}})}})})(1L);',62,391,'||||||this|function|return|||||||||||||if||||var|settings||||name|validator|true|u00A0|uD7FF||uF900|uFDCF|uFDF0||uFFEF|length|messages|valid|element|optional|extend|form|Please|false|enter|each|delete|arguments|in|for|errorList|currentForm|else|_|value|call||format|type|required|toHide|test|validate|data|maxlength|elements|pendingRequest|rules|case|errorClass|submitted|pending|invalid|typeof|port|attr|formSubmitted|add|trim|checkable|findByName|addClass|remote|url|showErrors|message|successList|toShow|event|console|input|filter|replace|removeClass|email|date|number|minlength|max|min|errorMap|not|metadata|jQuery|success|string|submitButton|handle|split|normalizeRule|apply|unhighlight|errorsFor|digits|reset|select|methods|dependency|mismatch|undefined|getLength|classRuleSettings|abort||x09|da|debug|new|submit|focusInvalid|switch|constructor|groups|validClass|addWrapper|radio|dateISO|creditcard|equalTo|rangelength|range|labelContainer|containers|focusin|currentElements|check|push|objectLength|method|parameters|defaultMessage|wrapper|previousValue|depends|mode|x20|bEven|handler|fn|window|find|click|cancelSubmit|submitHandler|val|is|staticRules|break|defaults|errorElement|errorLabelContainer|ignore|hide|highlight|fix|than|characters|delegate|eventType|validateDelegate|focusout|bind|prepareForm|clean|prepareElement|resetForm|hideErrors|rulesCache|errors|meta|theregex|showLabel|idOrName|dependTypes|old|param|Number|ajax|x0d|special|nothing|selected|onsubmit|button|remove|normalizeRules|metadataRules|classRules|attributeRules|checked|init|makeArray|Array|RegExp|error|errorContainer|ignoreTitle|lastActive|lastElement|parentNode|field|the|accept|no|between|and|or|equal|to|autoCreateRanges|errorContext|text|textarea|checkbox|option|invalidHandler|checkForm|triggerHandler|numberOfInvalids|grep|defaultShowErrors|size|try|findLastActive|focus|catch|formatAndAdd|id|customMetaMessage|customMessage|String|findDefined|strong|parent|validElements|show|invalidElements|map|generated|html|errorPlacement|document|null|nodeName|toLowerCase|depend|startRequest|stopRequest|dateDE|numberDE|addClassRules|isFunction|originalMessage|x22|x0a|x01|x0b|x0c|x7f|blur|ajaxPrefilter|ajaxSettings|addEventListener|warn|can|returning|cancel|preventDefault|hidden|appendTo|removeAttrs|removeAttr|expr|blank|filled|unchecked|unshift|slice|label|onfocusin|focusCleanup|blockFocusCleanup|onfocusout|onkeyup|onclick|setDefaults|This|address|URL|ISO|only|credit|card|same|again|with|extension|more|at|least|long|less|greater|prototype|valueCache|on|tel|password|file|keyup|visible|trigger|image|disabled|has|assigned|continue|log|exception|occured|when|checking|throw|title|Warning|No|defined|wrap|append|insertAfter|getElementsByName|boolean|class|2147483647|524288|addMethod|default|dataType|json|x08|x0e|x1f|x21|x23|x5b|x5d|x7e|https|ftp|uE000|uF8FF|Invalid|NaN|Date|charAt|parseInt|png|jpe|gif|match|unbind|setup|teardown|removeEventListener|target'.split('|'),0,{}))

/*!
 * Placeholder plugin
 * http://mal.co.nz/code/jquery-placeholder/
 * A full featured jQuery plugin that emulates the HTML5 placeholder attribute for web browsers that do not natively support it. If the browser does support it, the plugin does nothing.
 */

eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('(5($){$.r({1:{f:{l:\'J\',7:\'1\',t:u,v:K},w:u,3:5(a){2(!$.1.w)6;a="[L] "+a;$.1.x?g.3(a):$.1.y?h.g.3(a):M(a)},x:"g"8 h&&"N"8 h.g,y:"g"8 h&&"3"8 h.g}});$.z.1=\'1\'8 O.P(\'A\');$.k.j=$.k.9;$.k.9=5(a){$.1.3(\'8 9\');2(4[0]){$.1.3(\'Q R S T\');e b=$(4[0]);2(a!=m){$.1.3(\'8 U\');e c=b.j();e d=$(4).j(a);2(b.n($.1.f.7)&&c==b.i(\'1\')){b.o($.1.f.7)}6 d}2(b.n($.1.f.7)&&b.j()==b.i(\'1\')){$.1.3(\'p V W X\\\'s a 1\');6\'\'}Y{$.1.3(\'p Z 9\');6 b.j()}}$.1.3(\'p m\');6 m};$(h).q(\'10.1\',5(){e a=$(\'A.\'+$.1.f.7);2(a.11>0)a.9(\'\').i(\'B\',\'C\')});$.k.1=5(c){c=$.r({},$.1.f,c);2(!c.t&&$.z.1)6 4;6 4.12(5(){e b=$(4);2(!b.D(\'[1]\'))6;2(b.D(\':13\'))6;2(c.v)b.i(\'B\',\'C\');b.q(\'E.1\',5(){e a=$(4);2(4.F==a.i(\'1\')&&a.n(c.7))a.9(\'\').o(c.7).G(c.l)});b.q(\'H.1\',5(){e a=$(4);a.o(c.l);2(4.F==\'\')a.9(a.i(\'1\')).G(c.7)});b.I(\'H\');b.14(\'15\').16(5(){b.I(\'E.1\')})})}})(17);',62,70,'|placeholder|if|log|this|function|return|activeClass|in|val|||||var|settings|console|window|attr|plVal|fn|focusClass|undefined|hasClass|removeClass|returning|bind|extend||overrideSupport|false|preventRefreshIssues|debug|hasFirebug|hasConsoleLog|support|input|autocomplete|off|is|focus|value|addClass|blur|triggerHandler|placeholderFocus|true|Placeholder|alert|firebug|document|createElement|have|found|an|element|setter|empty|because|it|else|original|beforeunload|length|each|password|parents|form|submit|jQuery'.split('|'),0,{}));

/* ============================================================
 * retina-replace.min.js v1.0
 * http://github.com/leonsmith/retina-replace-js
 * ============================================================
 * Author: Leon Smith
 * Twitter: @nullUK
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================ */
(function(a){var e=function(d,c){this.options=c;var b=a(d),g=b.is("img"),f=g?b.attr("src"):b.backgroundImageUrl(),f=this.options.generateUrl(b,f);a("<img/>").attr("src",f).load(function(){g?b.attr("src",a(this).attr("src")):(b.backgroundImageUrl(a(this).attr("src")),b.backgroundSize(a(this)[0].width,a(this)[0].height));b.attr("data-retina","complete")})};e.prototype={constructor:e};a.fn.retinaReplace=function(d){var c;c=void 0===window.devicePixelRatio?1:window.devicePixelRatio;return 1>=c?this:this.each(function(){var b=
a(this),c=b.data("retinaReplace"),f=a.extend({},a.fn.retinaReplace.defaults,b.data(),"object"==typeof d&&d);c||b.data("retinaReplace",c=new e(this,f));if("string"==typeof d)c[d]()})};a.fn.retinaReplace.defaults={suffix:"_2x",generateUrl:function(a,c){var b=c.lastIndexOf("."),e=c.substr(b+1);return c.substr(0,b)+this.suffix+"."+e}};a.fn.retinaReplace.Constructor=e;a.fn.backgroundImageUrl=function(d){return d?this.each(function(){a(this).css("background-image",'url("'+d+'")')}):a(this).css("background-image").replace(/url\(|\)|"|'/g,
"")};a.fn.backgroundSize=function(d,c){var b=Math.floor(d/2)+"px "+Math.floor(c/2)+"px";a(this).css("background-size",b);a(this).css("-webkit-background-size",b)};a(function(){a("[data-retina='true']").retinaReplace()})})(window.jQuery);

/*!
 * (v) hrefID jQuery extention
 * returns a valid #hash string from link href attribute in Internet Explorer
 */
(function($){$.fn.extend({hrefId:function(){return $(this).attr('href').substr($(this).attr('href').indexOf('#'));}});})(jQuery);

/*!
 * Scripts
 *
 */
jQuery(function($) {

	$.fn.reverse = [].reverse;

	var Engine = {
		utils : {
			links : function(){
				$('a[rel*="external"]').click(function(e){
					e.preventDefault();
					window.open($(this).attr('href'));
				});
			},
			mails : function(){
				$('a[href^="mailto:"]').each(function(){
					var mail = $(this).attr('href').replace('mailto:','');
					var replaced = mail.replace('/at/','@');
					$(this).attr('href','mailto:'+replaced);
					if($(this).text() == mail) {
						$(this).text(replaced);
					}
				});
			},
			placeholderInit : function () {
				$('input[placeholder]').placeholder();
				$('textarea[placeholder]').placeholder();
			},
			retinaCheck : function () {
				$('#logo img').retinaReplace();
			}
		},
		ui : {
			fancyInit : function () {
				var analytics = new Array(
					"<script type=\"text/javascript\"> var _gaq = _gaq || []; _gaq.push(['_setAccount', 'UA-17829804-12']); _gaq.push(['_setDomainName', 'mobile-touch.eu']); _gaq.push(['_trackPageview', '/mobile-touch-movie']); (function() { var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); })(); window.console && console.log('movie 3 analytics script loaded'); </script>",
					"<script type=\"text/javascript\"> var _gaq = _gaq || []; _gaq.push(['_setAccount', 'UA-17829804-12']); _gaq.push(['_setDomainName', 'mobile-touch.eu']); _gaq.push(['_trackPageview', '/mozliwosci-mobile-touch']); (function() { var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); })(); window.console && console.log('movie 4 analytics script loaded'); </script>"
				);

				$('.fancybox-media').fancybox({
					openEffect  : 'fade',
					closeEffect : 'fade',
					padding: 0,
					width : 960,
					height: 540,
					helpers : {
						media : {},
						overlay : {
							opacity  : 0.6,
						}
					},
					afterShow : function () {
						var el = $($.fancybox.current.element);
						if ($('html').attr('lang') == 'pl') {
							var index = el.data('index');
							var code = analytics[index];
							$('body').append(code);
						}
					}
				});
				$('.fancy').fancybox({ padding: 0 });
			},
			requestContactActions : {
				objLib : {
					form : $("#request-contact-form"),
					showHideTriggers : $(".request-contact-trigger"),
					requestBox : $(".request-contact-box"),
					requestContainer : $(".request-contact-container"),
					requestHeader : $(".request-contact-box .h-box-a"),
					init : 1,
					withMask : 1

				},
				init : function () {
					var self = this;
					self.checkVisibility(self.objLib.requestContainer)
					self.showHideAction();
				},
				showHideAction : function () {
					var self = this;
					self.objLib.requestBox.on('click', '.request-contact-trigger', function (e) {
						e.preventDefault();

						if ($(this).closest('.box-f').hasClass('no-mask')) {
							window.console && console.log('no-mask');
							self.objLib.withMask = 0;
						};

						if (!self.objLib.requestHeader.hasClass("sent")) {
							self.showHide();
						}
					});
				},
				showHide : function () {
					var self = this;
					self.objLib.requestContainer.fadeSliderToggle({speed: 300, easing : 'swing',
						step : function () {
							if (self.objLib.withMask) {
								self.changeMask();
							}
						},
						onComplete : function () {
							self.checkVisibility($(this));
						}
					});
				},
				checkVisibility : function (obj) {
					var self = this;
					if (obj.is(':visible')) {
						$('#content').css({ position : 'static' });
						self.objLib.requestHeader.removeClass('collapsed');
						if (self.objLib.withMask) {
							self.objLib.requestBox.expose({closeOnEsc: false, closeOnClick: false, color: "#234e80", opacity: 0.6 });
						};
						if (!self.objLib.init) {
							Engine.ui.mainSlider.pause();
							Engine.ui.testimonialSlider.pause();
						}
					}
					else
					{
						$('#content').css({ position : 'relative' });
						self.objLib.requestHeader.addClass('collapsed');
						$.mask.close();
						if (!self.objLib.init) {
							Engine.ui.mainSlider.play();
							Engine.ui.testimonialSlider.play();
						}
					}
					self.objLib.init = 0;
					self.objLib.withMask = 1;
				},
				changeMask : function () {
					var exposeMask   = $('#exposeMask');
					var footerHeight = $('#footer').outerHeight();
					var rootHeight   = $('.root').height();
					exposeMask.height(footerHeight+rootHeight);
				}
			},
			verticalAlignVideoSubtitles : function () {
				var subtitles = $('.video-a .subtitle');
				subtitles.wrapInner('<span/>');
				subtitles.find('span').vAlign();
			},
			carousel : {
				objLib : {
					carouselMainContainer : $(".carousel-main-container"),
					carousel : $("#carousel-1"),
					videoModal : $("#modal-box-1"),
					vmTemplate : '<iframe id="x123456" src="http://player.vimeo.com/video/12613824" width="960" height="540" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>',
					ytTemplate : '<iframe id="y9986875" width="960" height="540" src="http://www.youtube.com/embed/VG0I64zAgek" frameborder="0" allowfullscreen></iframe>',
					analytics : new Array(
						"<script type=\"text/javascript\"> var _gaq = _gaq || []; _gaq.push(['_setAccount', 'UA-17829804-12']); _gaq.push(['_setDomainName', 'mobile-touch.eu']); _gaq.push(['_trackPageview', '/dzingiel-mobile-touch']); (function() { var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); })(); window.console && console.log('movie 1 analytics script loaded'); </script>",
						"<script type=\"text/javascript\"> var _gaq = _gaq || []; _gaq.push(['_setAccount', 'UA-17829804-12']); _gaq.push(['_setDomainName', 'mobile-touch.eu']); _gaq.push(['_trackPageview', '/spot-mobile-touch']); (function() { var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); })(); window.console && console.log('movie 2 analytics script loaded'); </script>",
						"<script type=\"text/javascript\"> var _gaq = _gaq || []; _gaq.push(['_setAccount', 'UA-17829804-12']); _gaq.push(['_setDomainName', 'mobile-touch.eu']); _gaq.push(['_trackPageview', '/poznaj-mobile-touch']); (function() { var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); })(); window.console && console.log('movie 3 analytics script loaded'); </script>"
					)
				},
				init : function () {
					var self = this;
					self.carouselInit();
					self.overlayModalInit();
					self.closeOnMaskClick();
					self.closeButtonAction();
				},
				carouselInit : function () {
					var self = this;
					if (self.objLib.carousel.hasClass('scrollable')) {
						self.objLib.carousel.jcarousel({ buttonPrevHTML : '<span class="slider-2-nav prev">Previous</span>', buttonNextHTML : '<span class="slider-2-nav next">Next</span>'});
					}
				},
				overlayModalInit : function () {
					var self = this;
					self.objLib.carousel.find('a').on('click', function (e) {
						e.preventDefault();
						var src = $(this).attr('href');
						var id = $(this).data('id');
						var type = "yt";
						if ($(this).hasClass('vm')) {
							type = "vm";
						}
						self.setVideo(src, type, id);

						if (!$(this).hasClass('active')) {
							self.openModal();
							self.objLib.carousel.find('a').removeClass('active');
							$(this).addClass('active');
						}
						else
						{
							$(this).removeClass('active');
							self.closeModal();
						}
					})
				},
				openModal : function () {
					var self = this;
					//set top pos
					if (!self.objLib.videoModal.is(":visible")) {
						self.objLib.videoModal.css('top', self.getTopPos());
						self.objLib.videoModal.fadeIn(300);
						self.objLib.carouselMainContainer.expose({closeOnEsc: false, closeOnClick: false, color: "#234e80", opacity: 0.6 });
						self.objLib.carouselMainContainer.addClass('opened');
						self.animButtons(84);

						//stop main slider
						Engine.ui.mainSlider.pause();
						Engine.ui.testimonialSlider.pause();
					}
				},
				closeModal : function () {
					var self = this;
					self.objLib.videoModal.fadeOut(600);
					$.mask.close();
					//set empty video
					self.setVideo('','yt', -1);
					self.objLib.carouselMainContainer.removeClass('opened');
					self.animButtons(35);
					self.objLib.carousel.find('a').removeClass('active');

					//start again main slider
					Engine.ui.mainSlider.play();
					Engine.ui.testimonialSlider.play();
				},
				getTopPos : function () {
					var self = this;
					var offset = self.objLib.carousel.offset();
					var topPos = offset.top - 453;
					return topPos;
				},
				setVideo : function (src, type, id) {
					var self = this;
					var code = self.objLib.ytTemplate;
					if (type = "vm") {
						code = self.objLib.vmTemplate;
					}
					self.objLib.videoModal.find('.content').html(code);
					self.objLib.videoModal.find('iframe').attr('src', src);

					if (id >= 0) {
						self.setAnalyticsCode(id);
					}

					// window.console && console.log(src);
				},
				animButtons : function (posTop) {
					var buttons = $('.slider-2-nav');
					buttons.delay(1000).animate({ top: posTop }, { duration: 500, easing: 'easeOutExpo' });
				},
				closeOnMaskClick : function () {
					var self = this;
					$('body').on('click', '#exposeMask', function () {
						if(self.objLib.carouselMainContainer.hasClass('opened'))
						{
							self.closeModal();
						}
					});
				},
				closeButtonAction : function () {
					var self = this;
					self.objLib.videoModal.on('click', '.modal-close', function (e) {
						e.preventDefault();
						self.closeModal();
					});
				},
				setAnalyticsCode : function (id) {
					var self = this;
					var code = self.objLib.analytics[id];
					$('body').append(code);
				}
			},
			mainSlider : {
				objLib : {
					cycleNav : $(".sequence-container-a .list-c"),
					cycleObj : $("#sequence-1 ul"),
					cycleContainer : $(".slider-a"),
					init : 1,
					imagesArray : [
						'btn-2.png',
						'sprite-set-1.png',
						'main-bg.jpg'
					]
				},
				options : {
					timeout: 10000,
					speed: 400,
					delay: 400,
					fx: 'fade',
					init : 1,
					cleartype: true,
    				cleartypeNoBg: true,
					manualTrump: false,
					before : function (curr, next, opts, fwd) {
						if(opts.init === 1)
						{
							var index = 0;
							opts.init = 0;
						}
						else
						{
							var index = opts.nextSlide;
						}
						Engine.ui.mainSlider.cycleEffect($(next), fwd, index);
						Engine.ui.mainSlider.setNavActive(index);
					},
					after : function (curr, next, opts) {

					}
				},
				init : function () {
					var self = this;
					self.loadImagesInitAll();
					self.gesturesNav();
				},
				loadImagesInitAll : function () {
					var self = this;
					var imgs = Engine.ui.getImages(self.objLib.imagesArray);

					var allImgs = self.objLib.cycleObj.find('img').add(imgs);
					allImgs.batchImageLoad({
						loadingCompleteCallback: function () {
							//window.console && console.log('init state '+self.objLib.init);
							if (self.objLib.init) {
								self.setContainerLoaded();
								//self.objLib.cycleObj.show(0);
								self.cycleInitialize();
								self.setNavActions();
								self.objLib.init = 0;
							}
						},
						imageLoadedCallback: null
					});
				},
				setContainerLoaded : function () {
					var self = this;
					self.objLib.cycleContainer.addClass('loaded');
				},
				cycleInitialize : function () {
					var self = this;
					self.objLib.cycleObj.cycle(self.options);
				},
				pause : function () {
					var self = this;
					self.objLib.cycleObj.cycle("pause");
					//get active index

					var index = self.objLib.cycleNav.find('.active').data('index');
					if (index === 0) {
						self.effects.slide1.pause();
					}
					else if (index === 1) {
						self.effects.slide2.pause();
					}
					else if (index === 2) {
						self.effects.slide3.pause();
					}
				},
				play : function () {
					var self = this;
					self.objLib.cycleObj.cycle("resume");
				},
				goToFrame : function (index) {
					var self = this;
					self.objLib.cycleObj.cycle(index);
				},
				prev : function () {
					var self = this;
					self.objLib.cycleObj.cycle('prev');
				},
				next : function () {
					var self = this;
					self.objLib.cycleObj.cycle('next');
				},
				setNavActive : function (index) {
					var self = this;
					self.objLib.cycleNav.find('li').removeClass('active');
					self.objLib.cycleNav.find('li').eq(index).addClass('active');
				},
				setNavActions : function () {
					var self = this;
					self.objLib.cycleNav.find('li').on('click', 'a', function (e) {
						e.preventDefault();
						var itemToCheck = $(this).parent();
						if (!itemToCheck.hasClass('active')) {
							var listItems  = self.objLib.cycleNav.find('li');
							activeIndex = listItems.index(itemToCheck);
							self.goToFrame(activeIndex);
						}
					});
				},
				cycleEffect : function (obj, direction, index) {
					var self = this;
					// window.console && console.log('cycle effect, index: '+index);
					if (index === 0) {
						self.effects.slide1.start(obj, direction);
					}
					else if (index === 1) {
						self.effects.slide2.start(obj, direction);
					}
					else if (index === 2) {
						self.effects.slide3.start(obj, direction);
					}
				},
				effects : {
					slide1 : {
						slider1Timeout1 : null,
						slider1Timeout2 : null,
						slider1Timeout2a : null,
						slider1Timeout3 : null,
						slider1Timeout3a : null,
						slider1TextTimeout : null,
						emptyTime : 5000,
						glow : {
							obj : $('#sequence-1 .vis-a-c'),
							initSettings : { opacity : 1 }
						},
						tablet : {
							obj : $('#sequence-1 .vis-a-b'),
							initSettings : { top: 24, opacity : 1 }
						},
						phone : {
							obj : $('#sequence-1 .vis-a-a'),
							initSettings : { top: 235, opacity : 1 }
						},
						textBox : {
							obj : $('#sequence-1 .content').eq(0),
							initSettings : { left: 0, opacity : 1 }
						},

						start : function (obj, direction) {
							var self = this;
							self.setCss();
							self.stop();

							var glowAnimationTime = 600;
							var glowWaitingTime = 2100;
							var ua = $.browser;
							if(ua.msie && ua.version < 9)
							{
								glowWaitingTime = glowWaitingTime + glowAnimationTime;
								glowAnimationTime = 0;
							}

							self.slider1Timeout3a = setTimeout ( function () {
								self.glow.obj.animate(self.glow.initSettings, glowAnimationTime, 'swing', function () {
									self.slider1Timeout3 = setTimeout ( function () {
										self.glow.obj.animate({ opacity : 0 }, glowAnimationTime, 'swing', null);
									}, glowWaitingTime + self.emptyTime);
								});
							}, 1500);

							self.slider1Timeout2a = setTimeout ( function () {
								self.tablet.obj.animate(self.tablet.initSettings, 1000, 'swing', function () {
									self.slider1Timeout2 = setTimeout ( function () {
										self.tablet.obj.animate({ top: 500, opacity : 1 });
									}, 3400 + self.emptyTime);
								});
							}, 200 );

							self.phone.obj.animate(self.phone.initSettings, 800, 'swing', function () {
								self.slider1Timeout1 = setTimeout ( function () {
									self.phone.obj.animate({ top: 500, opacity : 1 });
								}, 4100 + self.emptyTime);
							});

							self.textBox.obj.animate(self.textBox.initSettings, 600, 'swing', function () {
								self.slider1TextTimeout = setTimeout ( function () {
									self.textBox.obj.animate({ left: 150, opacity : 0 }, 600, 'swing', null);
								}, 4400 + self.emptyTime);
							});
						},
						stop : function () {
							var self = this;
							clearTimeout(self.slider1Timeout1);
							clearTimeout(self.slider1Timeout2);
							clearTimeout(self.slider1Timeout2a);
							clearTimeout(self.slider1Timeout3);
							clearTimeout(self.slider1Timeout3a);
							clearTimeout(self.slider1TextTimeout);
							self.glow.obj.stop();
							self.tablet.obj.stop();
							self.phone.obj.stop();
							self.textBox.obj.stop();
						},
						setCss : function () {
							var self = this;
							self.glow.obj.css({ 'opacity' : 0 });
							self.tablet.obj.css({ 'top' : -450, 'opacity' : 1 });
							self.phone.obj.css({ 'top' : -250, 'opacity' : 1 });
							self.textBox.obj.css({ 'position' : 'absolute', 'left' : 150, 'opacity' : 0 });
						},
						pause : function () {
							var self = this;
							self.stop();
							self.glow.obj.css(self.glow.initSettings);
							self.tablet.obj.css(self.tablet.initSettings);
							self.phone.obj.css(self.phone.initSettings);
							self.textBox.obj.css(self.textBox.initSettings);
						}
					},
					slide2 : {
						slider2Timeout : null,
						slider2Timeout1 : null,
						slider2Timeout1a : null,
						slider2TextTimeout : null,
						emptyTime : 5000,
						hands : {
							obj : $('#sequence-1 .vis-b-a'),
							initSettings : { top: 47, left: 0, opacity : 1 }
						},
						glow : {
							obj : $('#sequence-1 .vis-b-b'),
							initSettings : { opacity : 1 }
						},
						textBox : {
							obj : $('#sequence-1 .content').eq(1),
							initSettings : { left: 0, opacity : 1 }
						},
						start : function (obj, direction) {
							var self = this;
							self.setCss();
							self.stop();

							var glowAnimationTime = 600;
							var glowWaitingTime = 3200;
							var ua = $.browser;
							if(ua.msie && ua.version < 9)
							{
								glowWaitingTime = glowWaitingTime + glowAnimationTime;
								glowAnimationTime = 0;
							}

							//tablet with hands animation
							self.hands.obj.animate(self.hands.initSettings, 800, 'swing', function () {
								self.slider2Timeout = setTimeout ( function () {
									self.hands.obj.animate({ left: -800, opacity : 1 }, 600, 'swing', null);
								}, 4400 + self.emptyTime);
							});

							//tablet glow animation
							self.slider2Timeout1a = setTimeout ( function () {
								self.glow.obj.animate(self.glow.initSettings, glowAnimationTime, 'swing', function () {
									self.slider2Timeout1 = setTimeout ( function () {
										self.glow.obj.animate({ opacity : 0 }, glowAnimationTime, 'swing', null);
									}, glowWaitingTime + self.emptyTime);
								});
							}, 800);

							//text box animation
							self.textBox.obj.animate(self.textBox.initSettings, 600, 'swing', function () {
								self.slider2TextTimeout = setTimeout ( function () {
									self.textBox.obj.animate({ left: 150, opacity : 0 }, 600, 'swing', null);
								}, 4400 + self.emptyTime);
							});
						},
						stop : function () {
							var self = this;
							clearTimeout(self.slider2Timeout);
							clearTimeout(self.slider2Timeout1);
							clearTimeout(self.slider2Timeout1a);
							clearTimeout(self.slider2TextTimeout);
							self.hands.obj.stop();
							self.glow.obj.stop();
							self.textBox.obj.stop();
						},
						setCss : function () {
							var self = this;
							self.hands.obj.css({ 'top' : 500, "left" : 0, 'opacity' : 1 });
							self.glow.obj.css({ 'opacity' : 0 });
							self.textBox.obj.css({ 'position' : 'absolute', 'left' : 150, 'opacity' : 0 });
						},
						pause : function () {
							var self = this;
							self.stop();
							self.glow.obj.css(self.glow.initSettings);
							self.hands.obj.css(self.hands.initSettings);
							self.textBox.obj.css(self.textBox.initSettings);
						}
					},
					slide3 : {
						slider3Timeout : null,
						slider3Timeout1 : null,
						slider3Timeout1a : null,
						slider3Timeout2 : [],
						slider3Timeout2a : null,
						slider3Timeout2b : null,
						slider3Timeout2c : [],
						slider3Timeout3 : [],
						slider3Timeout3a : null,
						slider3Timeout3b : null,
						slider3Timeout3c : [],
						slider3TextTimeout : null,
						emptyTime : 5000,
						icoLib : [],
						hand : {
							obj : $('#sequence-1 .vis-c-a'),
							initSettings : { left: 235, top: 245, opacity : 1 }
						},
						glow : {
							obj : $('#sequence-1 .vis-c-b'),
							initSettings : { opacity : 1 }
						},
						circleInner : {
							obj : $('#sequence-1 .vis-c-c'),
							initSettings : { opacity : 1 }
						},
						circleOuter : {
							obj : $('#sequence-1 .vis-c-d'),
							initSettings : { opacity : 1 }
						},
						ico1 : {
							obj : $('#sequence-1 .vis-c-e'),
							initSettings : { opacity : 1 }
						},
						ico2 : {
							obj : $('#sequence-1 .vis-c-f'),
							initSettings : { opacity : 1 }
						},
						ico3 : {
							obj : $('#sequence-1 .vis-c-g'),
							initSettings : { opacity : 1 }
						},
						ico4 : {
							obj : $('#sequence-1 .vis-c-h'),
							initSettings : { opacity : 1 }
						},
						ico5 : {
							obj : $('#sequence-1 .vis-c-i'),
							initSettings : { opacity : 1 }
						},
						ico6 : {
							obj : $('#sequence-1 .vis-c-j'),
							initSettings : { opacity : 1 }
						},
						line1 : {
							obj : $('#sequence-1 .vis-c-l'),
							initSettings : { opacity : 1 }
						},
						line2 : {
							obj : $('#sequence-1 .vis-c-m'),
							initSettings : { opacity : 1 }
						},
						line3 : {
							obj : $('#sequence-1 .vis-c-n'),
							initSettings : { opacity : 1 }
						},
						line4 : {
							obj : $('#sequence-1 .vis-c-o'),
							initSettings : { opacity : 1 }
						},
						line5 : {
							obj : $('#sequence-1 .vis-c-p'),
							initSettings : { opacity : 1 }
						},
						line6 : {
							obj : $('#sequence-1 .vis-c-r'),
							initSettings : { opacity : 1 }
						},
						textBox : {
							obj : $('#sequence-1 .content').eq(2),
							initSettings : { left: 0, opacity : 1 }
						},
						start : function (obj, direction) {
							var self = this;

							self.icoLib = [self.ico1, self.ico2, self.ico3, self.ico4, self.ico5, self.ico6];
							self.lineLib = [self.line1, self.line2, self.line3, self.line4, self.line5, self.line6];

							self.setCss();
							self.stop();

							var glowAnimationTime = 600;
							var glowWaitingTime   = 3200;
							var ua = $.browser;
							if(ua.msie && ua.version < 9)
							{
								glowWaitingTime = glowWaitingTime + glowAnimationTime;
								glowAnimationTime = 0;
							}

							//hand animation
							self.hand.obj.animate(self.hand.initSettings, 1200, 'swing', function () {
								self.hand.obj.animate({ 'font-size': 0.97 }, { duration : 100, easing : 'linear',
									complete : function () {
										// $(this).css({'font-size' : 1});
										$(this).css('transform','scale(1)');
									},
									step : function (now, fx) {
										if (fx.prop === 'fontSize') {
											if(now > 0)
											{
												$(this).css('transform','scale('+now+')');
											}
										}
									}}
								);

								self.slider3Timeout = setTimeout ( function () {
									self.hand.obj.animate({ 'font-size': 0.97 }, { duration : 100, easing : 'linear',
										complete : function () {
											// $(this).css({'font-size' : 1});
											$(this).css('transform','scale(1)');
										},
										step : function (now, fx) {
											if (fx.prop === 'fontSize') {
												if(now > 0)
												{
													$(this).css('transform','scale('+now+')');
												}
											}
										}}
									);

									self.slider3Timeout4 = setTimeout(function () {
										self.hand.obj.animate({ top: 500, opacity : 1 }, 600, 'swing', null);
									}, 200);

								}, 3000 + self.emptyTime);
							});

							//hand glow animation
							self.slider3Timeout1a = setTimeout ( function () {
								self.glow.obj.animate(self.glow.initSettings, glowAnimationTime, 'swing', function () {
									self.slider3Timeout1 = setTimeout ( function () {
										self.glow.obj.animate({ opacity : 0 }, glowAnimationTime, 'swing', null);
									}, glowWaitingTime + self.emptyTime);
								});
							}, 1300);

							//inner circle animation
							self.slider3Timeout4a = setTimeout ( function () {
								self.circleInner.obj.animate(self.circleInner.initSettings, 500, 'easeOutBounce', function () {
									self.slider3Timeout4 = setTimeout ( function () {
										self.circleInner.obj.animate({ opacity : 0 }, 900, 'easeOutBounce', null);
									}, glowWaitingTime + self.emptyTime - 700);
								});
							}, 1300);


							//outer circle animation
							self.slider3Timeout5a = setTimeout ( function () {
								self.circleOuter.obj.animate(self.circleOuter.initSettings, 500, 'easeOutBounce', function () {
									self.slider3Timeout5 = setTimeout ( function () {
										self.circleOuter.obj.animate({ opacity : 0 }, 900, 'easeOutBounce', null);
									}, glowWaitingTime + self.emptyTime - 700);
								});
							}, 1500);

							//text box animation
							self.textBox.obj.animate(self.textBox.initSettings, 600, 'swing', function () {
								self.slider3TextTimeout = setTimeout ( function () {
									self.textBox.obj.animate({ left: 150, opacity : 0 }, 600, 'swing', null);
								}, 4400 + self.emptyTime);
							});

							// icons animation
								// show
							self.slider3Timeout2a = setTimeout( function () {
								var counter = 0;
								$.each(self.icoLib, function(index, val) {
									self.slider3Timeout2[index] = setTimeout( function () {
										val.obj.animate({	opacity: 1 }, 500, function() { });
									}, 10 + counter);
									counter = counter + 100;
								});
							}, 1600);

								//hide
							self.slider3Timeout2b = setTimeout( function () {
								var reversedIcoLib = self.icoLib.reverse();
								var counter = 0;
								$.each(reversedIcoLib, function(index, val) {
									self.slider3Timeout2c[index] = setTimeout( function () {
										val.obj.animate({	opacity: 0 }, 300, function() { });
									}, 10 + counter);
									counter = counter + 100;
								});
							}, 4600 + self.emptyTime);


							// line animation
								// show
							self.slider3Timeout3a = setTimeout( function () {
								var counter = 0;
								$.each(self.lineLib, function(index, val) {
									self.slider3Timeout3[index] = setTimeout( function () {
										val.obj.animate({	opacity: 1 }, 500, function() { });
									}, 10 + counter);
									counter = counter + 100;
								});
							}, 1400);
								// hide
							self.slider3Timeout3b = setTimeout( function () {
								var reversedIcoLib = self.lineLib.reverse();
								var counter = 0;
								$.each(reversedIcoLib, function(index, val) {
									self.slider3Timeout3c[index] = setTimeout( function () {
										val.obj.animate({	opacity: 0 }, 300, function() { });
									}, 10 + counter);
									counter = counter + 100;
								});
							}, 4400 + self.emptyTime);

						},
						stop : function () {
							var self = this;
							clearTimeout(self.slider3Timeout);
							clearTimeout(self.slider3Timeout1);
							clearTimeout(self.slider3Timeout1a);
							clearTimeout(self.slider3TextTimeout);

							clearTimeout(self.slider3Timeout4);
							clearTimeout(self.slider3Timeout4a);

							clearTimeout(self.slider3Timeout5);
							clearTimeout(self.slider3Timeout5a);

							self.hand.obj.stop();
							self.glow.obj.stop();
							self.textBox.obj.stop();
							self.circleInner.obj.stop();
							self.circleOuter.obj.stop();

							$.each(self.icoLib, function(index, val) {
								self.icoLib[index].obj.stop();
								self.lineLib[index].obj.stop()
							});

							// window.console && console.log(self.icoLib[0].obj.css('opacity'));

							// window.console && console.log(self.slider3Timeout2);
							$.each(self.slider3Timeout2, function(index, val) {
								// window.console && console.log(self.slider3Timeout2[index]);
								clearTimeout(self.slider3Timeout2[index]);
								clearTimeout(self.slider3Timeout2c[index]);
								clearTimeout(self.slider3Timeout3[index]);
								clearTimeout(self.slider3Timeout3c[index]);
							});
							clearTimeout(self.slider3Timeout2a);
							clearTimeout(self.slider3Timeout2b);
							clearTimeout(self.slider3Timeout3a);
							clearTimeout(self.slider3Timeout3b);

						},
						setCss : function () {
							var self = this;
							self.glow.obj.css({ 'opacity' : 0 });
							self.hand.obj.css({ left : 235, top: 600, opacity : 1, 'font-size' : 1, 'transform' : 'scale(1)' });
							self.textBox.obj.css({ 'position' : 'absolute', 'left' : 150, 'opacity' : 0 });

							self.circleInner.obj.css({ opacity : 0 });
							self.circleOuter.obj.css({ opacity : 0 });

							$.each(self.icoLib, function(index, val) {
								// window.console && console.log(val);
								self.icoLib[index].obj.css({ opacity : 0 });
								self.lineLib[index].obj.css({ opacity : 0 });
							});
						},
						pause : function () {
							var self = this;
							self.stop();
							self.glow.obj.css(self.glow.initSettings);
							self.textBox.obj.css(self.textBox.initSettings);
							self.hand.obj.css(self.hand.initSettings);
							self.circleInner.obj.css(self.circleInner.initSettings);
							self.circleOuter.obj.css(self.circleInner.initSettings);

							$.each(self.icoLib, function(index, val) {
								// window.console && console.log(val);
								self.icoLib[index].obj.css(self.icoLib[index].initSettings);
								self.lineLib[index].obj.css(self.lineLib[index].initSettings);
							});
						}
					}
				},
				gesturesNav : function () {
					var self = this;
					var swipe = self.gesturesObj();
					swipe.on('swipeleft swipeleftup swipeleftdown', function (e) {
						//alert('swipeleft');
						self.prev();
					});
					swipe.bind('swiperight swiperightup swiperightdown', function () {
						//alert('swiperight');
						self.next();
					});
				},
				gesturesObj : function () {
					return $('<div id="swipe"/>').appendTo(".sequence-container-a");
				}
			},
			getImages : function (imagesArray) {
				var p = $('#preloader');
				if (!p.length) {
					var p = $('<p id="preloader"/>').appendTo('body');
				}

				for(var i in imagesArray) {
					var value = imagesArray[i];
					if(typeof(value) == 'string')
					{
						p.append('<img src="images/' + value +'"/>');
					}
				}
				return p;
			},
			testimonialSlider : {
				objLib : {
					cycleNav : $(".sequence-container-b .list-c"),
					cycleObj : $("#slider-2 ul")
				},
				options : {
					timeout: 0,
					speed: 1000,
					speedIn : 1000,
					speedOut : 500,
					fx: 'fade',
					init : 1,
					before : function (curr, next, opts, fwd) {
						if(opts.init === 1)
						{
							var index = 0;
							opts.init = 0;
							Engine.ui.testimonialSlider.setContainerHeight($(curr), 0);
						}
						else
						{
							var index = opts.nextSlide;
							Engine.ui.testimonialSlider.setContainerHeight($(next), 350);
						}
						Engine.ui.testimonialSlider.setNavActive(index);
						//Engine.ui.testimonialSlider.cycleEffect($(next), fwd, index);
					},
					after : function (curr, next, opts) {

					}
				},
				init : function () {
					var self = this;
					self.cycleInitialize();
					self.setNavActions();
					self.setQuotesObj();
				},
				cycleInitialize : function () {
					var self = this;
					self.objLib.cycleObj.cycle(self.options);
				},
				pause : function () {
					var self = this;
					self.objLib.cycleObj.cycle("pause");
				},
				play : function () {
					var self = this;
					self.objLib.cycleObj.cycle("resume");
				},
				goToFrame : function (index) {
					var self = this;
					self.objLib.cycleObj.cycle(index);
				},
				setNavActive : function (index) {
					var self = this;
					self.objLib.cycleNav.find('li').removeClass('active');
					self.objLib.cycleNav.find('li').eq(index).addClass('active');
				},
				setNavActions : function () {
					var self = this;
					window.console && console.log(self.objLib.cycleNav);
					self.objLib.cycleNav.find('li').on('click', 'a', function (e) {
						e.preventDefault();
						var listItems  = self.objLib.cycleNav.find('li');
						var itemToCheck = $(this).parent();
						activeIndex = listItems.index(itemToCheck);
						self.goToFrame(activeIndex);
					});
				},
				setContainerHeight : function (obj, duration) {
					var self = this;
					self.objLib.cycleObj.animate({height: obj.height()+50}, duration, 'swing', null);
				},
				setQuotesObj : function () {
					var self = this;
					self.objLib.cycleObj.find('li').append('<span class="quote start"></span><span class="quote end"></span>');
				}
			},
			whySectionSlider : {
				objLib : {
					cycleNav : $(".sequence-container-c-nav"),
					cycleObj : $(".sequence-container-c")
				},
				options : {
					timeout: 0,
					speed: 1000,
					speedIn : 1000,
					speedOut : 500,
					fx: 'fade',
					init : 1,
					before : function (curr, next, opts, fwd) {
						if(opts.init === 1)
						{
							var index = 0;
							opts.init = 0;
							Engine.ui.whySectionSlider.setContainerHeight($(curr), 0);
						}
						else
						{
							var index = opts.nextSlide;
							Engine.ui.whySectionSlider.setContainerHeight($(next), 350);
						}
						Engine.ui.whySectionSlider.setNavActive(index);
						//Engine.ui.testimonialSlider.cycleEffect($(next), fwd, index);
					},
					after : function (curr, next, opts) {

					}
				},
				init : function () {
					var self = this;
					self.cycleInitialize();
					self.setNavActions();
				},
				cycleInitialize : function () {
					var self = this;
					self.objLib.cycleObj.cycle(self.options);
				},
				pause : function () {
					var self = this;
					self.objLib.cycleObj.cycle("pause");
				},
				play : function () {
					var self = this;
					self.objLib.cycleObj.cycle("resume");
				},
				goToFrame : function (index) {
					var self = this;
					self.objLib.cycleObj.cycle(index);
				},
				setNavActive : function (index) {
					var self = this;
					window.console && console.log(self.objLib.cycleNav);
					self.objLib.cycleNav.find('li').removeClass('active');
					self.objLib.cycleNav.find('li').eq(index).addClass('active');
				},
				setNavActions : function () {
					var self = this;

					self.objLib.cycleNav.find('li').on('click', 'a', function (e) {
						e.preventDefault();
						var listItems  = self.objLib.cycleNav.find('li');
						var itemToCheck = $(this).parent();
						activeIndex = listItems.index(itemToCheck);
						self.goToFrame(activeIndex);
					});
				},
				setContainerHeight : function (obj, duration) {
					var self = this;
					self.objLib.cycleObj.animate({height: obj.height()}, duration, 'swing', null);
				}
			},
			logoSlider : {
				objLib : {
					cycleObj : $("#logo-slider")
				},
				options : {
					timeout: 7000,
					speed: 500,
					fx: 'scrollVert',
					init : 1,
					before : function (curr, next, opts, fwd) {
						if(opts.init === 1)
						{
							var index = 0;
							opts.init = 0;
						}
						else
						{
							var index = opts.nextSlide;
						}
					},
					after : function (curr, next, opts) {

					}
				},
				init : function () {
					var self = this;
					self.cycleInitialize();
				},
				cycleInitialize : function () {
					var self = this;
					self.objLib.cycleObj.cycle(self.options);
				},
				pause : function () {
					var self = this;
					self.objLib.cycleObj.cycle("pause");
				},
				play : function () {
					var self = this;
					self.objLib.cycleObj.cycle("resume");
				},
				goToFrame : function (index) {
					var self = this;
					self.objLib.cycleObj.cycle(index);
				}
			},
			teamLeadersSlider : {
				objLib : {
					cycleNav : $(".list-f"),
					cycleObj : $(".list-g"),
					minHeight : 0
				},
				options : {
					timeout: 0,
					speed: 1000,
					speedIn : 1000,
					speedOut : 500,
					fx: 'fade',
					init : 1,
					before : function (curr, next, opts, fwd) {
						if(opts.init === 1)
						{
							var index = 0;
							opts.init = 0;
							Engine.ui.teamLeadersSlider.setContainerHeight($(curr), 0);
						}
						else
						{
							var index = opts.nextSlide;
							Engine.ui.teamLeadersSlider.setContainerHeight($(next), 350);
						}
						Engine.ui.teamLeadersSlider.setNavActive(index);
					},
					after : function (curr, next, opts) {
						Engine.ui.teamLeadersSlider.setHeight($(next));
					}
				},
				init : function () {
					var self = this;
					self.setMinHeight();
					self.cycleInitialize();
					self.setNavActions();
					self.desaturateImagesStart();
				},
				cycleInitialize : function () {
					var self = this;
					self.objLib.cycleObj.cycle(self.options);
				},
				pause : function () {
					var self = this;
					self.objLib.cycleObj.cycle("pause");
				},
				play : function () {
					var self = this;
					self.objLib.cycleObj.cycle("resume");
				},
				goToFrame : function (index) {
					var self = this;
					self.objLib.cycleObj.cycle(index);
				},
				setNavActive : function (index) {
					var self = this;
					self.objLib.cycleNav.find('li').removeClass('active');
					self.objLib.cycleNav.find('li').eq(index).addClass('active');
				},
				setNavActions : function () {
					var self = this;
					self.objLib.cycleNav.find('li').on('click', 'a', function (e) {
						e.preventDefault();
						var listItems  = self.objLib.cycleNav.find('li');
						var itemToCheck = $(this).parent();
						activeIndex = listItems.index(itemToCheck);
						self.goToFrame(activeIndex);
					});
				},
				setContainerHeight : function (obj, duration) {
					var self = this;
					var height = self.setHeight(obj);
					window.console && console.log('setContainerHeight');
					self.objLib.cycleObj.animate({height: height}, duration, 'swing', null);
				},
				setHeight : function (obj) {
					var self = this;
					var height = obj.height();
					if (self.objLib.cycleNav.hasClass('lf-a')) {
						if (height < self.objLib.minHeight) {
							window.console && console.log(height);
							var top = (self.objLib.minHeight - height) / 2;
							height = self.objLib.minHeight;
							obj.css({ 'margin-top' : top })
							window.console && console.log(self.objLib.minHeight);
						};
					}
					return height;
				},
				desaturateImagesStart : function () {
					var self = this;
					Engine.ui.desaturateImages.settings.desaturateOnly = false; //brighter logos
					Engine.ui.desaturateImages.start(self.objLib.cycleNav.find('img'));
				},
				setMinHeight : function () {
					var self = this;
					if (self.objLib.cycleNav.hasClass('lf-a')) {
						self.objLib.minHeight = self.objLib.cycleNav.height();
					};
				}
			},
			referencesSlider : {
				objLib : {
					cycleNav : $(".box-u"),
					cycleObj : $(".cycle-c"),
					cyclePrevBtn : $(".cycle-c-nav.prev"),
					cycleNextBtn : $(".cycle-c-nav.next")
				},
				options : {
					timeout: 0,
					speed: 1000,
					autostop: 0,
					fx: 'scrollHorz',
					init : 1,
					before : function (curr, next, opts, fwd) {
						var self = Engine.ui.referencesSlider;
						if(opts.init === 1)
						{
							var index = 0;
							opts.init = 0;
							self.setContainerHeight($(curr), 0);
						}
						else
						{
							var index = opts.nextSlide;
							self.setContainerHeight($(next), 350);
						}
						self.setNavInfo(index);
						//Engine.ui.testimonialSlider.cycleEffect($(next), fwd, index);
					},
					after : function (curr, next, opts) {

					}
				},
				init : function () {
					var self = this;
					self.cycleInitialize();
					self.setNav();
				},
				cycleInitialize : function () {
					var self = this;
					self.objLib.cycleObj.cycle(self.options);
				},
				next : function () {
					var self = this;
					self.objLib.cycleObj.cycle("next");
				},
				prev : function () {
					var self = this;
					self.objLib.cycleObj.cycle("prev");
				},
				setNav : function () {
					var self = this;
					self.objLib.cycleNav.find('.total').html(self.objLib.cycleObj.find('.slide').length);
					self.objLib.cyclePrevBtn.click( function (e) {
						e.preventDefault();
						self.prev();
					});
					self.objLib.cycleNextBtn.click( function (e) {
						e.preventDefault();
						self.next();
					});
				},
				setNavInfo : function (index) {
					var self = this;
					self.objLib.cycleNav.find('.count').html(index+1);
				},
				setContainerHeight : function (obj, duration) {
					var self = this;
					self.objLib.cycleObj.animate({height: obj.height()+50}, duration, 'swing', null);
				}
			},
			referencesLogoMiddle : {
				objLib : {
					heightSource : $('.v-align-height-giver'),
					vAParent : $('.v-align-parent')
				},
				init : function () {
					var self = this;
					if (self.objLib.heightSource.length) {
						var height = self.objLib.heightSource.height();
						self.objLib.vAParent.height(height);
						self.objLib.vAParent.find('span').vAlign();
					}
				}
			},
			desaturateImages : {
				settings : {
					desaturateOnly : true
				},
				start : function (imagesCollection) {
					var self = this;
					imagesCollection.batchImageLoad({
						loadingCompleteCallback: function () {
							self.desaturate(imagesCollection);
						},
						imageLoadedCallback: null
					});
				},
				desaturate : function (imagesCollection) {
					var self = this;
					imagesCollection.each(function () {
						var parent = $(this).parent();
						if (!parent.find('.bw').length) {
							var desaturated = $(this).clone();
							desaturated.addClass('bw').appendTo(parent);
						}
					});
					var timeout = setTimeout ( function () {
						imagesCollection.parent().find('img.bw').each(function () {
							/*if (self.settings.desaturateOnly) {
								$(this).pixastic("desaturate", {average : false});
							}
							else
							{
								//brighter logos
								$(this).pixastic("desaturate", {average : false}).pixastic("hsl", {hue: 0, saturation: 0, lightness: 50});
							}*/
							$(this).pixastic("desaturate", {average : false}).pixastic("hsl", {hue: 0, saturation: 0, lightness: 50});
						});
					}, 10);
				}
			},
			companyBoxesAnim : {
				objLib : {
					boxesContainer : $(".list-d"),
					isAnimationDone : 0,
					containerPos : 0
				},
				init : function () {
					var self = this;
					if (self.objLib.boxesContainer.length && !self.objLib.boxesContainer.hasClass('no-anim')) {
						self.setBoxesInitPos();
						self.getContainerTopPos();
						self.checkPos();
						self.scrollAction();
					}
				},
				setBoxesInitPos : function () {
					var self = this;
					self.objLib.boxesContainer.addClass('with-anim');
				},
				boxesAnim : function () {
					var self = this;
					if (!self.isAnimationDone) {
						self.isAnimationDone = 1;
						var timeout = 0;
						self.objLib.boxesContainer.find('.item').each(function () {
							var item = $(this);
							setTimeout( function () {
								item.animate({ top: 0, opacity : 1 }, 900, 'easeOutExpo', null);
							}, timeout);
							timeout += 300;
						});
					}
				},
				scrollAction : function () {
					var self = this;
					$(window).bind('scroll', function(){
						self.checkPos();
					});
				},
				checkPos : function () {
					var self = this;
					var topPos = self.getTopPos() + 500;
					if (topPos >= self.objLib.containerPos) {
						self.boxesAnim();
					}
				},
				getTopPos : function () {
					return $(window).scrollTop();
				},
				getContainerTopPos : function () {
					var self = this;
					var offset = self.objLib.boxesContainer.offset();
					self.objLib.containerPos =  offset.top;
				}
			},
			industrySelectAction : {
				objLib : {
					menu : $(".box-p .list-n"),
					container : $(".box-p")
				},
				init : function () {
					var self = this;
					self.objLib.container.on('click', function () {
						self.objLib.menu.fadeSliderToggle({speed: 150, easing : 'swing'});
					});
				},
			},
			tabsActions : {
				objLib : {
					tabs : $(".tabs-a"),
					panesContainer : $(".panes-a"),
					panes : $(".panes-a > section"),
					api : false
				},
				init : function () {
					var self = this;
					self.tabsInit();
				},
				getMaxHeight : function () {
					var self = this;
				},
				tabsInit : function () {
					var self = this;
					if (self.objLib.tabs.length) {
						self.objLib.tabs.tabs(self.objLib.panes, {
							tabs : 'li',
							effect : 'fade',
							history : false,
							fadeInSpeed : 0,
							fadeOutSpeed : 0,
							onBeforeClick: function(event, tabIndex) {
								//window.console && console.log('tabIndex');
								var currentTab = this.getTabs().eq(tabIndex);
								var currentPane = this.getPanes().eq(tabIndex);
								var paneContainerHeight = currentPane.height();
								currentPane.css({'position' : 'absolute'});
							},
							onClick : function (event, tabIndex) {
								var currentPane = this.getPanes().eq(tabIndex);
								currentPane.css({'position' : 'relative'});
								//var height = currentPane.outerHeight();
								self.containerSetHeight();
							}
						});
						self.objLib.api = self.objLib.tabs.data("tabs");
						var conf = self.objLib.api.getConf();
						conf.fadeInSpeed = 500;
						conf.fadeOutSpeed = 500;
					}
				},
				containerSetHeight : function () {
					var self = this;
					var height = self.getActiveHeight();
					var animSettings = { height: height };
					self.objLib.panesContainer.stop().animate(animSettings, 300, 'swing', function () {
						//self.objLib.panesContainer.css('min-height', height);
					});
				},
				getActiveHeight : function () {
					var self = this;
					var height = self.objLib.panesContainer.find(':visible').height();
					return height;
				}
			},
			goToTop : {
				objLib : {
					topButton : $('#top-button')
				},
				init : function () {
					var self = this;
					self.goToTop();
				},
				goToTop : function () {
					var self = this;

					self.countTopPos();
					$(window).bind('scroll', function(){
						self.countTopPos();
					});

					self.objLib.topButton.on('click', 'a', function (e) {
						e.preventDefault();
						self.scrollTopPos();
					});
				},
				countTopPos : function () {
					var self = this;
					var pos = $(window).scrollTop();
					//window.console && console.log(pos);
					if (pos > 250)
					{
						self.objLib.topButton.fadeIn();
					}
					else
					{
						self.objLib.topButton.fadeOut();
					}
				},
				scrollTopPos : function () {
					var pos = $(window).scrollTop();
					if (pos > 250) {
						$.scrollTo(0, 800);
					}
				}
			},
			blockEqualHeights : function() {
				var elements = $('.equal-height');
				var elChunks = $.chunk(elements, 2);
				$.each(elChunks, function (key, value) {
					value.equalHeights();
				});
			},
			boxesEqualHeights : function () {
				$('.art-j .wrapper').equalHeights();
			},
			headerSetHeights : function () {
				var articleContainer = $('.art-b');
				if (articleContainer.length) {
					var box = articleContainer.find('.box-ab');
					if (!box.length) {
						box = articleContainer.find('.header-e');
					};

					var img = articleContainer.find('.image');

					if(box.outerHeight(true)  > img.height())
					{
						articleContainer.css({ position : 'relative' });
						img.css({ position : 'absolute', bottom: 0 });
					}
				}
			},
			referencesEffects : {
				objLib : {
					mainList : $('.list-r')
				},
				init : function () {
					var self = this;
					self.desaturate();
					self.addShadowObjects();
					self.animateActions();
				},
				desaturate : function () {
					var self = this;
					//Engine.ui.desaturateImages.settings.desaturateOnly = false; //brighter logos
					//Engine.ui.desaturateImages.start(self.objLib.mainList.find('img'));
				},
				addShadowObjects : function () {
					var self = this;
					self.objLib.mainList.find('li').each(function () {
						$(this).append('<div class="shadow shadow-1"></div><div class="shadow shadow-2"></div>');
					});
				},
				animateActions : function () {
					var self = this;
					self.objLib.mainList.on(
					{
						mouseenter: function(){
							var parent = $(this);
							var shadow1 = parent.find('.shadow-1');
							var shadow2 = parent.find('.shadow-2');
							var obj = parent.find('a');

							var animateTimeOn = 200;
							var animateTimeOff = 200;
							var objAnimSettingsOn = { top: 5, left: -5 };
							var objAnimSettingsOff = { top: 0, left: 0 };

							var shadow1AnimSettingsOn = { width: 221, height: 185 };
							var shadow1AnimSettingsOff = { width: 216, height: 180 };

							var shadow2AnimSettingsOn = { opacity: 1, width: 228, height: 193, left: -12 };
							var shadow2AnimSettingsOff = { opacity: 0, width: 216, height: 180, left: 0 };

							obj.stop().animate(objAnimSettingsOn, animateTimeOn, 'swing', function () {});
							shadow1.stop().animate(shadow1AnimSettingsOn, animateTimeOn, 'swing', function () {});
							shadow2.stop().animate(shadow2AnimSettingsOn, animateTimeOn, 'swing', function () {});
						},
						mouseleave: function(){
							var parent = $(this);
							var shadow1 = parent.find('.shadow-1');
							var shadow2 = parent.find('.shadow-2');
							var obj = parent.find('a');

							var animateTimeOn = 200;
							var animateTimeOff = 200;
							var objAnimSettingsOn = { top: 5, left: -5 };
							var objAnimSettingsOff = { top: 0, left: 0 };

							var shadow1AnimSettingsOn = { width: 221, height: 185 };
							var shadow1AnimSettingsOff = { width: 216, height: 180 };

							var shadow2AnimSettingsOn = { opacity: 1, width: 228, height: 193, left: -12 };
							var shadow2AnimSettingsOff = { opacity: 0, width: 216, height: 180, left: 0 };

							obj.stop().animate(objAnimSettingsOff, animateTimeOff, 'swing', function () {});
							shadow1.stop().animate(shadow1AnimSettingsOff, animateTimeOff, 'swing', function () {});
							shadow2.stop().animate(shadow2AnimSettingsOff, animateTimeOff, 'swing', function () {});
						}
					}, 'li' );
				}
			},
			teamEffects : {
				objLib : {
					mainList : $('.list-v'),
					lastIndex : 0,
					currentIndex : -1,
					direction : 1,
					totalCount : $('.list-v').find('li').length
				},
				init : function () {
					var self = this;
					self.desaturate();
					self.addShadowObjects();
					self.clickActions();
					self.animateActions();
					self.keyboardNavigation();
				},
				desaturate : function () {
					var self = this;
					//Engine.ui.desaturateImages.settings.desaturateOnly = false; //brighter logos
					Engine.ui.desaturateImages.start(self.objLib.mainList.find('img'));
				},
				addShadowObjects : function () {
					var self = this;
					self.objLib.mainList.find('li').each(function () {
						$(this).append('<div class="shadow shadow-1"></div><div class="shadow shadow-2"></div>');
					});
				},
				animateSettings : {
					objAnimSettingsOn : { top: 5, left: -5 },
					objAnimSettingsOff : { top: 0, left: 0 },
					shadow1AnimSettingsOn : { width: 292, height: 149 },
					shadow1AnimSettingsOff : { width: 287, height: 144 },
					shadow2AnimSettingsOn : { opacity: 1, width: 297, height: 157, left: -10 },
					shadow2AnimSettingsOff : { opacity: 0, width: 287, height: 144, left: 0 }
				},
				animateActions : function () {
					var self = this;
					self.objLib.mainList.on(
					{
						mouseenter : function() {
							var parent = $(this);
							var shadow1 = parent.find('.shadow-1');
							var shadow2 = parent.find('.shadow-2');
							var obj = parent.find('.container');

							if (!parent.hasClass('active')) {
								var animateTimeOn = 200;
								var animateTimeOff = 200;
								var objAnimSettingsOn = self.animateSettings.objAnimSettingsOn;
								var objAnimSettingsOff = self.animateSettings.objAnimSettingsOff;

								var shadow1AnimSettingsOn = self.animateSettings.shadow1AnimSettingsOn;
								var shadow1AnimSettingsOff = self.animateSettings.shadow1AnimSettingsOff;

								var shadow2AnimSettingsOn = self.animateSettings.shadow2AnimSettingsOn;
								var shadow2AnimSettingsOff = self.animateSettings.shadow2AnimSettingsOff;

								obj.stop().animate(objAnimSettingsOn, animateTimeOn, 'swing', function () {});
								shadow1.stop().animate(shadow1AnimSettingsOn, animateTimeOn, 'swing', function () {});
								shadow2.stop().animate(shadow2AnimSettingsOn, animateTimeOn, 'swing', function () {});
							}
						},
						mouseleave : function() {
							var parent = $(this);
							var shadow1 = parent.find('.shadow-1');
							var shadow2 = parent.find('.shadow-2');
							var obj = parent.find('.container');

							if (!parent.hasClass('active')) {
								var animateTimeOn = 200;
								var animateTimeOff = 200;
								var objAnimSettingsOn = self.animateSettings.objAnimSettingsOn;
								var objAnimSettingsOff = self.animateSettings.objAnimSettingsOff;

								var shadow1AnimSettingsOn = self.animateSettings.shadow1AnimSettingsOn;
								var shadow1AnimSettingsOff = self.animateSettings.shadow1AnimSettingsOff;

								var shadow2AnimSettingsOn = self.animateSettings.shadow2AnimSettingsOn;
								var shadow2AnimSettingsOff = self.animateSettings.shadow2AnimSettingsOff;

								obj.stop().animate(objAnimSettingsOff, animateTimeOff, 'swing', function () {});
								shadow1.stop().animate(shadow1AnimSettingsOff, animateTimeOff, 'swing', function () {});
								shadow2.stop().animate(shadow2AnimSettingsOff, animateTimeOff, 'swing', function () {});
							}
						}
					}, 'li');
				},
				clickActions : function () {
					var self = this;
					self.objLib.mainList.on('click', 'li', function (e) {
						var obj = $(this);
						if (!obj.hasClass('active') && !$('.info-box-a').is(':animated')) {
							self.objLib.mainList.find('li').removeClass('active');
							obj.addClass('active');

							var inner = obj.find('.container');
							var shadow1 = obj.find('.shadow-1');
							var shadow2 = obj.find('.shadow-2');

							inner.stop().animate(self.animateSettings.objAnimSettingsOff, 200, 'swing');
							shadow1.stop().animate(self.animateSettings.shadow1AnimSettingsOff, 200, 'swing');
							shadow2.stop().animate(self.animateSettings.shadow2AnimSettingsOff, 200, 'swing');

							self.insertBox(obj);
						}
					});
				},
				insertBox : function (obj) {
					var self = this;
					var insertPosition = self.countPosition(obj);
					var infoBox = $('.info-box-a');
					if(self.objLib.lastIndex === insertPosition)
					{
						self.changeBoxContent(infoBox, obj);
					}
					else
					{
						self.objLib.lastIndex = insertPosition;
						var theObject = obj;
						if (infoBox.length) {
							infoBox.fadeSliderToggle({speed: 200, easing : 'swing', onComplete: function () {
								$(this).remove();
								self.insertNewBox(insertPosition, theObject);
							}});
						}
						else
						{
							self.insertNewBox(insertPosition, theObject);
						}
					}
				},
				countPosition : function (obj) {
					var self = this;
					activeIndex = self.objLib.mainList.find('li').index(obj);

					//count direction
					if (activeIndex > self.objLib.currentIndex) {
						self.objLib.direction = -1;
					}
					else
					{
						self.objLib.direction = 1;
					}
					self.objLib.currentIndex = activeIndex;

					//window.console && console.log(self.objLib.direction);
					//window.console && console.log(self.objLib.currentIndex);

					var row = Math.ceil((activeIndex+1)/3);
					var lastInARow = row*3;

					if (lastInARow > self.objLib.totalCount) {
						lastInARow = self.objLib.totalCount;
					}
					var lastIndex = lastInARow - 1;
					return lastIndex;
				},
				insertNewBox : function (index, obj) {
					var self = this;
					var box = $('<div class="info-box-a"></div>').insertAfter(self.objLib.mainList.find('li').eq(index));
					box.html(self.getContent(obj));
					box.fadeSliderToggle({speed: 200, easing : 'swing'});
				},
				changeBoxContent : function (infoBox, obj) {
					var self = this;

					var boxHeight = infoBox.height();
					infoBox.height(boxHeight);

					var innerContent = infoBox.find('.inner-content');
					innerContent.css({ position : 'absolute', opacity : 1});
					innerContent.fadeOut(200, function () {
						$(this).remove();
					});

					var newContent = self.getContent(obj);
					// var nC = $(newContent).html();
					var newObj = $('<div class="cols-two-k inner-content"></div>').appendTo(infoBox);

					var distance = 1000;
					distance = distance * self.objLib.direction;

					newObj.css({ position: 'relative', left: distance, opacity: 0});
					newObj.html(newContent);
					newObj.animate({left: 0, opacity: 1}, 200, 'swing', function () {
						var height = $(this).height();
						infoBox.animate({height: height}, 200, 'swing');
					});
				},
				getContent : function (obj) {
					return obj.find('.info').html();
				},
				prev : function () {
					var self = this;
					var prevIndex = self.objLib.currentIndex - 1;
					if (prevIndex < 0) {
						prevIndex = self.objLib.totalCount - 1;
					}
					self.goToIndex(prevIndex);
				},
				next : function () {
					var self = this;
					var prevIndex = self.objLib.currentIndex + 1;
					if (prevIndex > (self.objLib.totalCount - 1)) {
						prevIndex = 0;
					}
					self.goToIndex(prevIndex);
				},
				goToIndex : function (index) {
					var self = this;
					self.objLib.mainList.find('li').eq(index).trigger('click');
				},
				keyboardNavigation : function () {
					var self = this;
					$(window).keydown(function(event){
						//Left
						if(event.keyCode == '37')
						{
							self.prev();
						}
						//Right
						if(event.keyCode == '39')
						{
							self.next();
						}
            		});
				}
			},
			setSpecialButton : function () {
				var self = this;
				// window.console && console.log('gartner init');
				// gartner button
				$('#gartner-btn').on('click', function(event) {
					event.preventDefault();
					Engine.ajax.addContact.insertGartnerCode();
					setTimeout(function () {
						window.console && console.log('location change');
						window.location = 'http://www.gartner.com/reprints/asseco-business-solutions-s-a-?id=1-1LXBGZD&ct=131017&amp;st=sg';
					}, 500);
				});
			}
		},
		ajax : {
			addContact : {
				objLib : {
					requestHeader : $(".request-contact-box .h-box-a"),
					requestForm : $(".request-contact-form"),
					analyticsCode : "<script type=\"text/javascript\"> var _gaq = _gaq || []; _gaq.push(['_setAccount', 'UA-17829804-12']); _gaq.push(['_setDomainName', 'mobile-touch.eu']); _gaq.push(['_trackPageview', '/kontakt-z-nami-ok']); (function() { var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google- analytics.com/ga.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); })(); window.console && console.log('contact analytics script loaded'); </script>",
					analyticsGartnerCode : "<script type=\"text/javascript\"> var _gaq = _gaq || []; _gaq.push(['_setAccount', 'UA-17829804-12']); _gaq.push(['_setDomainName', 'mobile-touch.eu']); _gaq.push(['_trackPageview', '/desktop_rap_gartner_ok']); (function() { var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google- analytics.com/ga.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); })(); window.console && console.log('gartner analytics script loaded'); </script>"
				},
				add : function (callback) {
					var self = this;
					var fields = self.getFormFields();
					$.ajax({
						url: 'ajaxsendcontact',
						type: "POST",
  						data: fields,
  						success: function(data) {
    						window.console && console.log(typeof callback);
    						if (typeof callback == 'undefined') {
    							self.setData(data);
    						}
    						else
    						{
    							callback(data);
    						}
	  					}
					});
				},
				changeHeaderText : function (text) {
					var self = this;
					//var text = 'Request contact has been sent.';
					self.objLib.requestHeader.find('h1').html(text);
				},
				changeInfoText : function (text) {
					var self = this;
					//var text = 'We will contact you as soon as possible.';
					self.objLib.requestHeader.find('.info-text').html(text);
				},
				setHeaderClass : function () {
					var self = this;
					self.objLib.requestHeader.addClass('sent');
				},
				getFormUrl : function () {
					var self = this;
					return self.objLib.requestForm.attr('action');
				},
				getFormFields : function () {
					var self = this;
					var form = self.objLib.requestForm;
					var fields = form.serialize()
					// var fields = {
					// 	client_name : form.find('#client_name').val(),
					// 	email : form.find('#email').val()
					// }
					return fields;
				},
				setData : function (data) {
					var self = this;
					var data = JSON.parse(data);
					self.changeHeaderText(data.header);
					self.changeInfoText(data.text);
					self.setHeaderClass();
					Engine.ui.requestContactActions.showHide();
					self.insertCode();
				},
				insertCode : function () {
					var self = this;
					var code = self.objLib.analyticsCode;
					$('body').append(code);
				},
				insertGartnerCode : function () {
					var self = this;
					var code = self.objLib.analyticsGartnerCode;
					$('body').append(code);
				}
			},
			loadItems : {
				objLib : {
					loadButton : $('.load-button'),
					itemsContainer : $('#items-container'),
					allItemsCount : $('#items-container').data('items-count')
				},
				init : function () {
					var self = this;
					self.clickAction();
				},
				clickAction : function () {
					var self = this;
					self.objLib.loadButton.on('click', 'a', function (e) {
						e.preventDefault();
						if (!self.objLib.loadButton.hasClass('loading')) {
							self.objLib.loadButton.addClass('loading');
							var url = $(this).attr('href');
							var itemsCount = $(this).data('extend-jump');
							var info = self.getInfoFromUrl(url);
							var url = self.prepareUrl(info, itemsCount);
							self.load(url);
						}
					});
				},
				load : function (url) {
					var self = this;
					$.ajax({
						url: url,
  						success: function(data) {
    						self.appendItems(data);
	  					}
					});
				},
				getInfoFromUrl : function (url) {
					var urlArray = url.split('/');
					return { addr : urlArray[0], action : urlArray[1], parentItemId : urlArray[2], offset : urlArray.pop() }
				},
				prepareUrl : function (info, count) {
					//var offset = info.offset - count;
					var self = this;
					var offset = self.objLib.itemsContainer.find('.item').length;
					if (info.action === 'extendcomments') {
						// Comments
						var url = info.addr+'/ajaxgetcomments/'+info.parentItemId+'/'+offset+'/'+count;
					}
					else
					{
						// Other items
						var url = info.addr+'/ajaxgetitems/'+offset+'/'+count;
					}
					return url;
				},
				appendItems : function (data) {
					var self = this;
					var obj = $('<div class="temp-container"/>').append(data);
					var items = obj.find('.item');
					self.objLib.itemsContainer.append(obj);
					obj.fadeSliderToggle({speed: 800, easing : 'swing',  onComplete : function () {
						self.finalize(items);
					}});
				},
				unwrapItems : function (items) {
					items.unwrap();
				},
				changeHref : function () {
					var self = this;
					var link = self.objLib.loadButton.find('a');
					var href = link.attr('href');
					var itemsCount = link.data('extend-jump');
					var hrefArray = href.split('/');
					var offset = hrefArray.pop();
					var newOffset = parseInt(offset)+parseInt(itemsCount)
					hrefArray.push(newOffset);
					var newHref = hrefArray.join('/');
					link.attr('href', newHref);
					//window.console && console.log(newHref);
					return newHref;
				},
				checkItemsCount : function () {
					var self = this;
					var items = self.objLib.itemsContainer.find('.item');
					var length = items.length + 1;
					if (length >= self.objLib.itemsContainer.attr('data-items-count')) {
						self.objLib.loadButton.fadeSliderToggle();
					}
				},
				finalize : function (items) {
					var self = this;
					self.unwrapItems(items);
					self.changeHref();
					self.checkItemsCount();
					self.objLib.loadButton.removeClass('loading');
				}
			}
		},
		forms : {
			validateFastContactForm : function () {
				var contactForm = $("#request-contact-form");
				var validator = contactForm.validate({
					rules: {
						client_name: "required",
						email: {
							required: true,
							email: true
						}
					},
					messages: {
						client_name : $('#client_name').data('error-required'),
						email : {
							email : $('#email').data('error-email'),
							required : $('#email').data('error-required')
						}
					},
  					highlight: function(element, errorClass, validClass) {
    					$(element).parent().parent().addClass(errorClass);
						$(element).addClass(errorClass).removeClass(validClass);
  					},
  					unhighlight: function(element, errorClass, validClass) {
    					$(element).parent().parent().removeClass(errorClass);
    					$(element).removeClass(errorClass).addClass(validClass);
					},
					errorPlacement: function(error, element) {
						if (element.is('select')) {
							if (!$('.set-a').find('label.error').length) {
								element.parent().parent().append(error);
							}
						}
						else
						{
							element.parent().parent().append(error);
						}
					},
					showErrors: function(errorMap, errorList) {
    					this.defaultShowErrors();
						Engine.ui.requestContactActions.changeMask();
						Engine.ui.tabsActions.containerSetHeight();
  					},
					submitHandler: function(form) {
						if (!$(form).hasClass('no-ajax')) {
							Engine.ajax.addContact.add();
							return false;
						}
						else
						{
							form.submit();
						}
   				}
				});
			},
			validateReportContactForm : function () {
				var contactForm = $("#report-contact-form");
				var lockFrom = false;
				var validator = contactForm.validate({
					rules: {
						client_name: "required",
						email: {
							required: true,
							email: true
						},
						company: "required",
					},
					messages: {
						client_name : $('#client_name').data('error-required'),
						email : {
							email : $('#email').data('error-email'),
							required : $('#email').data('error-required')
						},
						company : $('#company').data('error-required')
					},
  					highlight: function(element, errorClass, validClass) {
    					$(element).parent().parent().addClass(errorClass);
						$(element).addClass(errorClass).removeClass(validClass);
  					},
  					unhighlight: function(element, errorClass, validClass) {
    					$(element).parent().parent().removeClass(errorClass);
    					$(element).removeClass(errorClass).addClass(validClass);
					},
					errorPlacement: function(error, element) {
						if (element.is('select')) {
							if (!$('.set-a').find('label.error').length) {
								element.parent().parent().append(error);
							}
						}
						else
						{
							element.parent().parent().append(error);
						}
					},
					showErrors: function(errorMap, errorList) {
    					this.defaultShowErrors();
						Engine.ui.requestContactActions.changeMask();
						Engine.ui.tabsActions.containerSetHeight();
  					},
					submitHandler: function(form) {
						if (!lockFrom) {
							lockFrom = true;
							Engine.ajax.addContact.add(function () {
								Engine.ajax.addContact.insertGartnerCode();
								setTimeout(function () {
									window.location = 'http://www.gartner.com/reprints/asseco-business-solutions-s-a-?id=1-1LXBGZD&ct=131017&st=sg';
								}, 500)
							});
						};
						return false;
   				}
				});
			},
			validateContactForm : function () {
				var contactForm = $("#contact-form");
				var validator = contactForm.validate({
					rules: {
						client_name: "required",
						email: {
							required: true,
							email: true
						},
						phone: "required",
						company: "required",
						message: "required"
					},
					messages: {
						client_name : $('#client_name').data('error-required'),
						email : {
							email : $('#email').data('error-email'),
							required : $('#email').data('error-required')
						},
						phone : $('#phone').data('error-required'),
						company : $('#company').data('error-required'),
						message : $('#message').data('error-required')
					},
  					highlight: function(element, errorClass, validClass) {
    					$(element).parent().parent().addClass(errorClass);
						$(element).addClass(errorClass).removeClass(validClass);
  					},
  					unhighlight: function(element, errorClass, validClass) {
    					$(element).parent().parent().removeClass(errorClass);
    					$(element).removeClass(errorClass).addClass(validClass);
					},
					errorPlacement: function(error, element) {
						if (element.is('select')) {
							if (!$('.set-a').find('label.error').length) {
								element.parent().parent().append(error);
							}
						}
						else
						{
							element.parent().parent().append(error);
						}
					},
					showErrors: function(errorMap, errorList) {
    					this.defaultShowErrors();
						Engine.ui.tabsActions.containerSetHeight();
  					}
				});
			}
		}
	};

	Engine.utils.retinaCheck();
	Engine.utils.links();
	Engine.utils.mails();
	Engine.utils.placeholderInit();
	Engine.ui.mainSlider.init();
	Engine.ui.testimonialSlider.init();
	Engine.ui.teamLeadersSlider.init();
	Engine.forms.validateFastContactForm();
	Engine.forms.validateContactForm();
	Engine.forms.validateReportContactForm();
	Engine.ui.requestContactActions.init();
	Engine.ui.verticalAlignVideoSubtitles();
	Engine.ui.carousel.init();
	Engine.ui.companyBoxesAnim.init();
	Engine.ui.tabsActions.init();
	Engine.ui.fancyInit();
	Engine.ui.goToTop.init();
	Engine.ui.blockEqualHeights();
	Engine.ui.boxesEqualHeights();
	Engine.ui.industrySelectAction.init();
	Engine.ui.referencesEffects.init();
	Engine.ui.referencesSlider.init();
	Engine.ui.referencesLogoMiddle.init();
	Engine.ui.logoSlider.init();
	Engine.ui.teamEffects.init();
	Engine.ui.whySectionSlider.init();
	Engine.ui.headerSetHeights();
	Engine.ajax.loadItems.init();

	Engine.ui.setSpecialButton();

});