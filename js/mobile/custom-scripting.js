(function(){
        var wst = window.scrollTo;
        window.scrollTo = function(x,y){
            if ($.mobile.activePage){
                if ($.mobile.activePage.hasClass(".ui-page-pre-in")){
                    $.mobile.activePage.css("margin-top", "-" + y + "px");
                }else{
                    $.mobile.activePage.css("", "");
                }
            }
            $(document.body).height(y+$.mobile.getScreenHeight());
            setTimeout(function(){
                wst.call(window, x, y);
            }, 0);
        };
    })();


$(document).bind("mobileinit", function() {
  $.extend($.mobile , {    
    pageLoadErrorMessage: "Error loading page",    
	defaultPageTransition : 'slide'	
  });
  if (!!window.navigator.standalone) {
		/*$.extend($.mobile , {        
			defaultPageTransition : 'fade'	
  		});	*/		
	}
});
