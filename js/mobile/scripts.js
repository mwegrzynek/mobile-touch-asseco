/**
 * jQuery Validation Plugin 1.8.1
 *
 * http://bassistance.de/jquery-plugins/jquery-plugin-validation/
 * http://docs.jquery.com/Plugins/Validation
 *
 * Copyright (c) 2006 - 2011 Jörn Zaefferer
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 */

//changed line 349
// bug http://plugins.jquery.com/content/validation-checkbox-group-causes-incorrect-registration-valid-and-invalid-elements
//changed line 319
// bug http://plugins.jquery.com/content/support-html-5-input-types-number-email-url
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('(7($){$.K($.2E,{16:7(c){l(!6.F){c&&c.28&&2F.1x&&1x.4F("3k 3l, 4G\'t 16, 4H 3k");8}p d=$.17(6[0],\'v\');l(d){8 d}d=29 $.v(c,6[0]);$.17(6[0],\'v\',d);l(d.q.3m){6.2G("1y, 3n").1z(".4I").2H(7(){d.2I=w});l(d.q.2J){6.2G("1y, 3n").1z(":2a").2H(7(){d.1O=6})}6.2a(7(b){l(d.q.28)b.4J();7 1P(){l(d.q.2J){l(d.1O){p a=$("<1y 12=\'4K\'/>").1j("u",d.1O.u).2K(d.1O.Y).4L(d.V)}d.q.2J.Z(d,d.V);l(d.1O){a.3o()}8 N}8 w}l(d.2I){d.2I=N;8 1P()}l(d.L()){l(d.1a){d.1k=w;8 N}8 1P()}W{d.2b();8 N}})}8 d},H:7(){l($(6[0]).2L(\'L\')){8 6.16().L()}W{p a=w;p b=$(6[0].L).16();6.P(7(){a&=b.I(6)});8 a}},4M:7(c){p d={},$I=6;$.P(c.1Q(/\\s/),7(a,b){d[b]=$I.1j(b);$I.4N(b)});8 d},1b:7(c,d){p e=6[0];l(c){p f=$.17(e.L,\'v\').q;p g=f.1b;p h=$.v.2M(e);2c(c){1c"1l":$.K(h,$.v.1R(d));g[e.u]=h;l(d.G)f.G[e.u]=$.K(f.G[e.u],d.G);2N;1c"3o":l(!d){Q g[e.u];8 h}p i={};$.P(d.1Q(/\\s/),7(a,b){i[b]=h[b];Q h[b]});8 i}}p j=$.v.3p($.K({},$.v.3q(e),$.v.3r(e),$.v.3s(e),$.v.2M(e)),e);l(j.13){p k=j.13;Q j.13;j=$.K({13:k},j)}8 j}});$.K($.4O[":"],{4P:7(a){8!$.1m(""+a.Y)},4Q:7(a){8!!$.1m(""+a.Y)},4R:7(a){8!a.3t}});$.v=7(a,b){6.q=$.K(w,{},$.v.2O,a);6.V=b;6.3u()};$.v.11=7(b,c){l(R.F==1)8 7(){p a=$.3v(R);a.4S(b);8 $.v.11.1S(6,a)};l(R.F>2&&c.2d!=3w){c=$.3v(R).4T(1)}l(c.2d!=3w){c=[c]}$.P(c,7(i,n){b=b.1A(29 3x("\\\\{"+i+"\\\\}","g"),n)});8 b};$.K($.v,{2O:{G:{},2e:{},1b:{},1d:"3y",2f:"H",2P:"4U",2b:w,3z:$([]),2Q:$([]),3m:w,2R:[],3A:N,4V:7(a){6.3B=a;l(6.q.4W&&!6.4X){6.q.1T&&6.q.1T.Z(6,a,6.q.1d,6.q.2f);6.2g(6.1U(a)).2S()}},4Y:7(a){l(!6.1n(a)&&(a.u S 6.1e||!6.J(a))){6.I(a)}},4Z:7(a){l(a.u S 6.1e||a==6.3C){6.I(a)}},50:7(a){l(a.u S 6.1e)6.I(a);W l(a.3D.u S 6.1e)6.I(a.3D)},2T:7(a,b,c){l(a.12===\'2h\'){6.1o(a.u).1p(b).1B(c)}W{$(a).1p(b).1B(c)}},1T:7(a,b,c){l(a.12===\'2h\'){6.1o(a.u).1B(b).1p(c)}W{$(a).1B(b).1p(c)}}},51:7(a){$.K($.v.2O,a)},G:{13:"52 3E 2L 13.",1q:"M 2U 6 3E.",1C:"M O a H 1C 53.",1r:"M O a H 54.",1D:"M O a H 1D.",2i:"M O a H 1D (55).",1E:"M O a H 1E.",1V:"M O 56 1V.",2j:"M O a H 57 58 1E.",2k:"M O 3F 59 Y 5a.",3G:"M O a Y 5b a H 5c.",18:$.v.11("M O 3H 5d 2V {0} 2W."),1F:$.v.11("M O 5e 5f {0} 2W."),2l:$.v.11("M O a Y 3I {0} 3J {1} 2W 5g."),2m:$.v.11("M O a Y 3I {0} 3J {1}."),1G:$.v.11("M O a Y 5h 2V 3K 3L 3M {0}."),1H:$.v.11("M O a Y 5i 2V 3K 3L 3M {0}.")},3N:N,5j:{3u:7(){6.2n=$(6.q.2Q);6.3O=6.2n.F&&6.2n||$(6.V);6.2o=$(6.q.3z).1l(6.q.2Q);6.1e={};6.5k={};6.1a=0;6.1f={};6.1g={};6.1W();p e=(6.2e={});$.P(6.q.2e,7(c,d){$.P(d.1Q(/\\s/),7(a,b){e[b]=c})});p f=6.q.1b;$.P(f,7(a,b){f[a]=$.v.1R(b)});7 2X(a){p b=$.17(6[0].L,"v"),2Y="5l"+a.12.1A(/^16/,"");b.q[2Y]&&b.q[2Y].Z(b,6[0])}$(6.V).2Z(":3P, [12=1C], [12=5m], [12=1r], :5n, :5o, 1X, 3Q","2p 30 5p",2X).2Z(":2h, :3R, 1X, 3S","2H",2X);l(6.q.3T)$(6.V).31("1g-L.16",6.q.3T)},L:7(){6.3U();$.K(6.1e,6.1I);6.1g=$.K({},6.1I);l(!6.H())$(6.V).3V("1g-L",[6]);6.1s();8 6.H()},3U:7(){6.32();T(p i=0,19=(6.2q=6.19());19[i];i++){6.2r(19[i])}8 6.H()},I:7(a){a=6.33(a);6.3C=a;6.34(a);6.2q=$(6.1n(a)?6.1o(a.u)[0]:a);p b=6.2r(a);l(b){Q 6.1g[a.u]}W{6.1g[a.u]=w}l(!6.3W()){6.14=6.14.1l(6.2o)}6.1s();8 b},1s:7(b){l(b){$.K(6.1I,b);6.U=[];T(p c S b){6.U.2s({1t:b[c],I:6.1o(c)[0]})}6.1u=$.3X(6.1u,7(a){8!(a.u S b)})}6.q.1s?6.q.1s.Z(6,6.1I,6.U):6.3Y()},35:7(){l($.2E.35)$(6.V).35();6.1e={};6.32();6.36();6.19().1B(6.q.1d)},3W:7(){8 6.2t(6.1g)},2t:7(a){p b=0;T(p i S a)b++;8 b},36:7(){6.2g(6.14).2S()},H:7(){8 6.3Z()==0},3Z:7(){8 6.U.F},2b:7(){l(6.q.2b){40{$(6.41()||6.U.F&&6.U[0].I||[]).1z(":5q").42().5r("2p")}43(e){}}},41:7(){p a=6.3B;8 a&&$.3X(6.U,7(n){8 n.I.u==a.u}).F==1&&a},19:7(){p a=6,37={};8 $(6.V).2G("1y, 1X, 3Q").1J(":2a, :1W, :5s, [5t]").1J(6.q.2R).1z(7(){!6.u&&a.q.28&&2F.1x&&1x.3y("%o 5u 3H u 5v",6);l(6.u S 37||!a.2t($(6).1b()))8 N;37[6.u]=w;8 w})},33:7(a){8 $(a)[0]},38:7(){8 $(6.q.2P+"."+6.q.1d,6.3O)},1W:7(){6.1u=[];6.U=[];6.1I={};6.1v=$([]);6.14=$([]);6.2q=$([])},32:7(){6.1W();6.14=6.38().1l(6.2o)},34:7(a){6.1W();6.14=6.1U(a)},2r:7(a){a=6.33(a);l(6.1n(a)){a=6.1o(a.u).1J(6.q.2R)[0]}p b=$(a).1b();p c=N;T(p d S b){p f={2u:d,2v:b[d]};40{p g=$.v.1Y[d].Z(6,a.Y.1A(/\\r/g,""),a,f.2v);l(g=="1Z-20"){c=w;5w}c=N;l(g=="1f"){6.14=6.14.1J(6.1U(a));8}l(!g){6.44(a,f);8 N}}43(e){6.q.28&&2F.1x&&1x.5x("5y 5z 5A 5B I "+a.45+", 2r 3F \'"+f.2u+"\' 2u",e);5C e;}}l(c)8;l(6.2t(b))6.1u.2s(a);8 w},46:7(a,b){l(!$.1K)8;p c=6.q.39?$(a).1K()[6.q.39]:$(a).1K();8 c&&c.G&&c.G[b]},47:7(a,b){p m=6.q.G[a];8 m&&(m.2d==48?m:m[b])},49:7(){T(p i=0;i<R.F;i++){l(R[i]!==21)8 R[i]}8 21},2w:7(a,b){8 6.49(6.47(a.u,b),6.46(a,b),!6.q.3A&&a.5D||21,$.v.G[b],"<4a>5E: 5F 1t 5G T "+a.u+"</4a>")},44:7(a,b){p c=6.2w(a,b.2u),3a=/\\$?\\{(\\d+)\\}/g;l(1h c=="7"){c=c.Z(6,b.2v,a)}W l(3a.15(c)){c=1L.11(c.1A(3a,\'{$1}\'),b.2v)}6.U.2s({1t:c,I:a});6.1I[a.u]=c;6.1e[a.u]=c},2g:7(a){l(6.q.2x)a=a.1l(a.4b(6.q.2x));8 a},3Y:7(){T(p i=0;6.U[i];i++){p a=6.U[i];6.q.2T&&6.q.2T.Z(6,a.I,6.q.1d,6.q.2f);6.3b(a.I,a.1t)}l(6.U.F){6.1v=6.1v.1l(6.2o)}l(6.q.1M){T(p i=0;6.1u[i];i++){6.3b(6.1u[i])}}l(6.q.1T){T(p i=0,19=6.4c();19[i];i++){6.q.1T.Z(6,19[i],6.q.1d,6.q.2f)}}6.14=6.14.1J(6.1v);6.36();6.2g(6.1v).4d()},4c:7(){8 6.2q.1J(6.4e())},4e:7(){8 $(6.U).4f(7(){8 6.I})},3b:7(a,b){p c=6.1U(a);l(c.F){c.1B().1p(6.q.1d);c.1j("4g")&&c.4h(b)}W{c=$("<"+6.q.2P+"/>").1j({"T":6.3c(a),4g:w}).1p(6.q.1d).4h(b||"");l(6.q.2x){c=c.2S().4d().5H("<"+6.q.2x+"/>").4b()}l(!6.2n.5I(c).F)6.q.4i?6.q.4i(c,$(a)):c.5J(a)}l(!b&&6.q.1M){c.3P("");1h 6.q.1M=="1N"?c.1p(6.q.1M):6.q.1M(c)}6.1v=6.1v.1l(c)},1U:7(a){p b=6.3c(a);8 6.38().1z(7(){8 $(6).1j(\'T\')==b})},3c:7(a){8 6.2e[a.u]||(6.1n(a)?a.u:a.45||a.u)},1n:7(a){8/2h|3R/i.15(a.12)},1o:7(c){p d=6.V;8 $(4j.5K(c)).4f(7(a,b){8 b.L==d&&b.u==c&&b||4k})},22:7(a,b){2c(b.4l.4m()){1c\'1X\':8 $("3S:3l",b).F;1c\'1y\':l(6.1n(b))8 6.1o(b.u).1z(\':3t\').F}8 a.F},4n:7(a,b){8 6.3d[1h a]?6.3d[1h a](a,b):w},3d:{"5L":7(a,b){8 a},"1N":7(a,b){8!!$(a,b.L).F},"7":7(a,b){8 a(b)}},J:7(a){8!$.v.1Y.13.Z(6,$.1m(a.Y),a)&&"1Z-20"},4o:7(a){l(!6.1f[a.u]){6.1a++;6.1f[a.u]=w}},4p:7(a,b){6.1a--;l(6.1a<0)6.1a=0;Q 6.1f[a.u];l(b&&6.1a==0&&6.1k&&6.L()){$(6.V).2a();6.1k=N}W l(!b&&6.1a==0&&6.1k){$(6.V).3V("1g-L",[6]);6.1k=N}},2y:7(a){8 $.17(a,"2y")||$.17(a,"2y",{3e:4k,H:w,1t:6.2w(a,"1q")})}},23:{13:{13:w},1C:{1C:w},1r:{1r:w},1D:{1D:w},2i:{2i:w},4q:{4q:w},1E:{1E:w},4r:{4r:w},1V:{1V:w},2j:{2j:w}},4s:7(a,b){a.2d==48?6.23[a]=b:$.K(6.23,a)},3r:7(a){p b={};p c=$(a).1j(\'5M\');c&&$.P(c.1Q(\' \'),7(){l(6 S $.v.23){$.K(b,$.v.23[6])}});8 b},3s:7(a){p b={};p c=$(a);T(p d S $.v.1Y){p e=c.1j(d);l(e){b[d]=e}}l(b.18&&/-1|5N|5O/.15(b.18)){Q b.18}8 b},3q:7(a){l(!$.1K)8{};p b=$.17(a.L,\'v\').q.39;8 b?$(a).1K()[b]:$(a).1K()},2M:7(a){p b={};p c=$.17(a.L,\'v\');l(c.q.1b){b=$.v.1R(c.q.1b[a.u])||{}}8 b},3p:7(d,e){$.P(d,7(a,b){l(b===N){Q d[a];8}l(b.3f||b.2z){p c=w;2c(1h b.2z){1c"1N":c=!!$(b.2z,e.L).F;2N;1c"7":c=b.2z.Z(e,e);2N}l(c){d[a]=b.3f!==21?b.3f:w}W{Q d[a]}}});$.P(d,7(a,b){d[a]=$.4t(b)?b(e):b});$.P([\'1F\',\'18\',\'1H\',\'1G\'],7(){l(d[6]){d[6]=3g(d[6])}});$.P([\'2l\',\'2m\'],7(){l(d[6]){d[6]=[3g(d[6][0]),3g(d[6][1])]}});l($.v.3N){l(d.1H&&d.1G){d.2m=[d.1H,d.1G];Q d.1H;Q d.1G}l(d.1F&&d.18){d.2l=[d.1F,d.18];Q d.1F;Q d.18}}l(d.G){Q d.G}8 d},1R:7(a){l(1h a=="1N"){p b={};$.P(a.1Q(/\\s/),7(){b[6]=w});a=b}8 a},5P:7(a,b,c){$.v.1Y[a]=b;$.v.G[a]=c!=21?c:$.v.G[a];l(b.F<3){$.v.4s(a,$.v.1R(a))}},1Y:{13:7(a,b,c){l(!6.4n(c,b))8"1Z-20";2c(b.4l.4m()){1c\'1X\':p d=$(b).2K();8 d&&d.F>0;1c\'1y\':l(6.1n(b))8 6.22(a,b)>0;5Q:8 $.1m(a).F>0}},1q:7(f,g,h){l(6.J(g))8"1Z-20";p i=6.2y(g);l(!6.q.G[g.u])6.q.G[g.u]={};i.4u=6.q.G[g.u].1q;6.q.G[g.u].1q=i.1t;h=1h h=="1N"&&{1r:h}||h;l(6.1f[g.u]){8"1f"}l(i.3e===f){8 i.H}i.3e=f;p j=6;6.4o(g);p k={};k[g.u]=f;$.3h($.K(w,{1r:h,2A:"24",1i:"16"+g.u,5R:"5S",17:k,1M:7(a){j.q.G[g.u].1q=i.4u;p b=a===w;l(b){p c=j.1k;j.34(g);j.1k=c;j.1u.2s(g);j.1s()}W{p d={};p e=a||j.2w(g,"1q");d[g.u]=i.1t=$.4t(e)?e(f):e;j.1s(d)}i.H=b;j.4p(g,b)}},h));8"1f"},1F:7(a,b,c){8 6.J(b)||6.22($.1m(a),b)>=c},18:7(a,b,c){8 6.J(b)||6.22($.1m(a),b)<=c},2l:7(a,b,c){p d=6.22($.1m(a),b);8 6.J(b)||(d>=c[0]&&d<=c[1])},1H:7(a,b,c){8 6.J(b)||a>=c},1G:7(a,b,c){8 6.J(b)||a<=c},2m:7(a,b,c){8 6.J(b)||(a>=c[0]&&a<=c[1])},1C:7(a,b){8 6.J(b)||/^((([a-z]|\\d|[!#\\$%&\'\\*\\+\\-\\/=\\?\\^X`{\\|}~]|[\\x-\\y\\A-\\B\\C-\\E])+(\\.([a-z]|\\d|[!#\\$%&\'\\*\\+\\-\\/=\\?\\^X`{\\|}~]|[\\x-\\y\\A-\\B\\C-\\E])+)*)|((\\4v)((((\\2B|\\26)*(\\3i\\4w))?(\\2B|\\26)+)?(([\\4x-\\5T\\4y\\4z\\5U-\\5V\\4A]|\\5W|[\\5X-\\5Y]|[\\5Z-\\60]|[\\x-\\y\\A-\\B\\C-\\E])|(\\\\([\\4x-\\26\\4y\\4z\\3i-\\4A]|[\\x-\\y\\A-\\B\\C-\\E]))))*(((\\2B|\\26)*(\\3i\\4w))?(\\2B|\\26)+)?(\\4v)))@((([a-z]|\\d|[\\x-\\y\\A-\\B\\C-\\E])|(([a-z]|\\d|[\\x-\\y\\A-\\B\\C-\\E])([a-z]|\\d|-|\\.|X|~|[\\x-\\y\\A-\\B\\C-\\E])*([a-z]|\\d|[\\x-\\y\\A-\\B\\C-\\E])))\\.)+(([a-z]|[\\x-\\y\\A-\\B\\C-\\E])|(([a-z]|[\\x-\\y\\A-\\B\\C-\\E])([a-z]|\\d|-|\\.|X|~|[\\x-\\y\\A-\\B\\C-\\E])*([a-z]|[\\x-\\y\\A-\\B\\C-\\E])))\\.?$/i.15(a)},1r:7(a,b){8 6.J(b)||/^(61?|62):\\/\\/(((([a-z]|\\d|-|\\.|X|~|[\\x-\\y\\A-\\B\\C-\\E])|(%[\\27-f]{2})|[!\\$&\'\\(\\)\\*\\+,;=]|:)*@)?(((\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5])\\.(\\d|[1-9]\\d|1\\d\\d|2[0-4]\\d|25[0-5]))|((([a-z]|\\d|[\\x-\\y\\A-\\B\\C-\\E])|(([a-z]|\\d|[\\x-\\y\\A-\\B\\C-\\E])([a-z]|\\d|-|\\.|X|~|[\\x-\\y\\A-\\B\\C-\\E])*([a-z]|\\d|[\\x-\\y\\A-\\B\\C-\\E])))\\.)+(([a-z]|[\\x-\\y\\A-\\B\\C-\\E])|(([a-z]|[\\x-\\y\\A-\\B\\C-\\E])([a-z]|\\d|-|\\.|X|~|[\\x-\\y\\A-\\B\\C-\\E])*([a-z]|[\\x-\\y\\A-\\B\\C-\\E])))\\.?)(:\\d*)?)(\\/((([a-z]|\\d|-|\\.|X|~|[\\x-\\y\\A-\\B\\C-\\E])|(%[\\27-f]{2})|[!\\$&\'\\(\\)\\*\\+,;=]|:|@)+(\\/(([a-z]|\\d|-|\\.|X|~|[\\x-\\y\\A-\\B\\C-\\E])|(%[\\27-f]{2})|[!\\$&\'\\(\\)\\*\\+,;=]|:|@)*)*)?)?(\\?((([a-z]|\\d|-|\\.|X|~|[\\x-\\y\\A-\\B\\C-\\E])|(%[\\27-f]{2})|[!\\$&\'\\(\\)\\*\\+,;=]|:|@)|[\\63-\\64]|\\/|\\?)*)?(\\#((([a-z]|\\d|-|\\.|X|~|[\\x-\\y\\A-\\B\\C-\\E])|(%[\\27-f]{2})|[!\\$&\'\\(\\)\\*\\+,;=]|:|@)|\\/|\\?)*)?$/i.15(a)},1D:7(a,b){8 6.J(b)||!/65|66/.15(29 67(a))},2i:7(a,b){8 6.J(b)||/^\\d{4}[\\/-]\\d{1,2}[\\/-]\\d{1,2}$/.15(a)},1E:7(a,b){8 6.J(b)||/^-?(?:\\d+|\\d{1,3}(?:,\\d{3})+)(?:\\.\\d+)?$/.15(a)},1V:7(a,b){8 6.J(b)||/^\\d+$/.15(a)},2j:7(a,b){l(6.J(b))8"1Z-20";l(/[^0-9-]+/.15(a))8 N;p c=0,e=0,2C=N;a=a.1A(/\\D/g,"");T(p n=a.F-1;n>=0;n--){p d=a.68(n);p e=69(d,10);l(2C){l((e*=2)>9)e-=9}c+=e;2C=!2C}8(c%10)==0},3G:7(a,b,c){c=1h c=="1N"?c.1A(/,/g,\'|\'):"6a|6b?g|6c";8 6.J(b)||a.6d(29 3x(".("+c+")$","i"))},2k:7(a,b,c){p d=$(c).6e(".16-2k").31("4B.16-2k",7(){$(b).H()});8 a==d.2K()}}});$.11=$.v.11})(1L);(7($){p d={};l($.4C){$.4C(7(a,X,b){p c=a.1i;l(a.2A=="24"){l(d[c]){d[c].24()}d[c]=b}})}W{p e=$.3h;$.3h=7(a){p b=("2A"S a?a:$.4D).2A,1i=("1i"S a?a:$.4D).1i;l(b=="24"){l(d[1i]){d[1i].24()}8(d[1i]=e.1S(6,R))}8 e.1S(6,R)}}})(1L);(7($){l(!1L.1w.3j.2p&&!1L.1w.3j.30&&4j.4E){$.P({42:\'2p\',4B:\'30\'},7(a,b){$.1w.3j[b]={6f:7(){6.4E(a,2D,w)},6g:7(){6.6h(a,2D,w)},2D:7(e){R[0]=$.1w.2U(e);R[0].12=b;8 $.1w.1P.1S(6,R)}};7 2D(e){e=$.1w.2U(e);e.12=b;8 $.1w.1P.Z(6,e)}})};$.K($.2E,{2Z:7(c,d,e){8 6.31(d,7(a){p b=$(a.6i);l(b.2L(c)){8 e.1S(b,R)}})}})})(1L);',62,391,'||||||this|function|return|||||||||||||if||||var|settings||||name|validator|true|u00A0|uD7FF||uF900|uFDCF|uFDF0||uFFEF|length|messages|valid|element|optional|extend|form|Please|false|enter|each|delete|arguments|in|for|errorList|currentForm|else|_|value|call||format|type|required|toHide|test|validate|data|maxlength|elements|pendingRequest|rules|case|errorClass|submitted|pending|invalid|typeof|port|attr|formSubmitted|add|trim|checkable|findByName|addClass|remote|url|showErrors|message|successList|toShow|event|console|input|filter|replace|removeClass|email|date|number|minlength|max|min|errorMap|not|metadata|jQuery|success|string|submitButton|handle|split|normalizeRule|apply|unhighlight|errorsFor|digits|reset|select|methods|dependency|mismatch|undefined|getLength|classRuleSettings|abort||x09|da|debug|new|submit|focusInvalid|switch|constructor|groups|validClass|addWrapper|radio|dateISO|creditcard|equalTo|rangelength|range|labelContainer|containers|focusin|currentElements|check|push|objectLength|method|parameters|defaultMessage|wrapper|previousValue|depends|mode|x20|bEven|handler|fn|window|find|click|cancelSubmit|submitHandler|val|is|staticRules|break|defaults|errorElement|errorLabelContainer|ignore|hide|highlight|fix|than|characters|delegate|eventType|validateDelegate|focusout|bind|prepareForm|clean|prepareElement|resetForm|hideErrors|rulesCache|errors|meta|theregex|showLabel|idOrName|dependTypes|old|param|Number|ajax|x0d|special|nothing|selected|onsubmit|button|remove|normalizeRules|metadataRules|classRules|attributeRules|checked|init|makeArray|Array|RegExp|error|errorContainer|ignoreTitle|lastActive|lastElement|parentNode|field|the|accept|no|between|and|or|equal|to|autoCreateRanges|errorContext|text|textarea|checkbox|option|invalidHandler|checkForm|triggerHandler|numberOfInvalids|grep|defaultShowErrors|size|try|findLastActive|focus|catch|formatAndAdd|id|customMetaMessage|customMessage|String|findDefined|strong|parent|validElements|show|invalidElements|map|generated|html|errorPlacement|document|null|nodeName|toLowerCase|depend|startRequest|stopRequest|dateDE|numberDE|addClassRules|isFunction|originalMessage|x22|x0a|x01|x0b|x0c|x7f|blur|ajaxPrefilter|ajaxSettings|addEventListener|warn|can|returning|cancel|preventDefault|hidden|appendTo|removeAttrs|removeAttr|expr|blank|filled|unchecked|unshift|slice|label|onfocusin|focusCleanup|blockFocusCleanup|onfocusout|onkeyup|onclick|setDefaults|This|address|URL|ISO|only|credit|card|same|again|with|extension|more|at|least|long|less|greater|prototype|valueCache|on|tel|password|file|keyup|visible|trigger|image|disabled|has|assigned|continue|log|exception|occured|when|checking|throw|title|Warning|No|defined|wrap|append|insertAfter|getElementsByName|boolean|class|2147483647|524288|addMethod|default|dataType|json|x08|x0e|x1f|x21|x23|x5b|x5d|x7e|https|ftp|uE000|uF8FF|Invalid|NaN|Date|charAt|parseInt|png|jpe|gif|match|unbind|setup|teardown|removeEventListener|target'.split('|'),0,{}))


/*
 * Fade Slider Toggle plugin
 *
 * Copyright(c) 2009, Cedric Dugas
 * http://www.position-relative.net
 *
 * A sliderToggle() with opacity
 * Licenced under the MIT Licence
 */
 jQuery.fn.fadeSliderToggle = function(settings) {
 	/* Damn you jQuery opacity:'toggle' that dosen't work!~!!!*/
 	 settings = jQuery.extend({
		speed: 300,
		easing : "swing",
		onComplete : false,
		step : false
	}, settings)

	caller = this
 	if($(caller).css("display") == "none"){
 		$(caller).animate({
 			opacity: 1,
 			height: 'toggle'
 		},
		{ duration : settings.speed,
		  easing : settings.easing,
		  complete : settings.onComplete,
		  step : settings.step});
	}else{
		$(caller).animate({
 			opacity: 0,
 			height: 'toggle'
 		},
		{ duration : settings.speed,
		  easing : settings.easing,
		  complete : settings.onComplete,
		  step : settings.step});
	}
};

/*!
 * Scripts
 *
 */
jQuery(function($) {
	var Engine = {
		ui : {
			menuInit : function () {
				$('.menu-trigger').on('tap, click', function (e) {
					e.preventDefault();
					var menu = $('.main-menu');
					if (menu.is(':visible')) {
						//hide menu
						menu.slideUp(100);
						$(this).removeClass('active');
					}
					else
					{
						//show menu
						menu.slideDown(100);
						$(this).addClass('active');
					}
				});
			},
			hideMenu : function () {
				$('.main-menu').slideUp(0);
				$('.menu-trigger').removeClass('active');
			},
			checkIfHome : function () {
				if($.mobile.activePage !== undefined)
				{
					if($.mobile.activePage.attr('id') === 'homepage')
					{
						$('.home').fadeOut(200);
					}
					else
					{
						$('.home').fadeIn(200);
					}
				}
			},
			addRemoveTapped : function () {
				$('.home a, .list-a a, .ontapchange a').removeClass('tapped');
				$('.home a, .list-a a, .ontapchange a').bind('tap', function(event){
					$(this).addClass('tapped');
				});
			},
			removeTapped : function (delay) {
				setTimeout(function () {
					$('.home a, .list-a a, .ontapchange a').removeClass('tapped');
				}, delay);

			},
			iScrollInit :  {
				objLib : {
					wrapper : $('#wrapper'),
					scroller : $('#scroller'),
					nav : $('#scroller-nav'),
					scrollApi : false
				},
				init : function () {
					var self = this;
					if ($('#wrapper').length) {
						if (self.objLib.scrollApi === false) {
							self.setObjects();
							self.setSize();
							self.setScroll();
							self.setHeight(0, 0);
							self.resizeAction();
							self.setNavActions();
						}

					}
				},
				setScroll : function () {
					var self = this;
					self.objLib.scrollApi = new iScroll('wrapper', {
					    snap: true,
					    momentum: false,
					    hScrollbar: false,
					    vScrollbar: false,
					    onScrollEnd: function () {
							var currIndex = self.objLib.scrollApi.currPageX;
					    	//window.console && console.log(self.objLib.wrapper.find('li').eq(self.objLib.myScroll.currPageX).height());
							self.setNav(currIndex);
							self.setHeight(currIndex, 200);
					    }
				    });
				},
				setSize : function () {
					var self = this;
					var dWidth = $(document).width();
					var countItems = self.objLib.wrapper.find('li').length;
					self.objLib.wrapper.find('li').width(dWidth);
					self.objLib.wrapper.width(dWidth);
					self.objLib.scroller.width(dWidth * countItems);
				},
				setNav : function (index) {
					var self = this;
					self.objLib.nav.find('li').removeClass('active');
					self.objLib.nav.find('li').eq(index).addClass('active');
				},
				setNavActions : function (index) {
					var self = this;
					if (self.objLib.scrollApi !== false) {
						self.objLib.nav.find('li').on('click, tap', 'a', function (e) {
							e.preventDefault();
							var index = $(this).parent().data('index') - 1;
							window.console && console.log(index);
							self.objLib.scrollApi.scrollToPage(index, 0, 300);
						});
					}
				},
				setHeight : function (index, duration) {
					var self = this;
					var height = self.objLib.wrapper.find('li').eq(index).height();
					// window.console && console.log(height);
					//self.objLib.wrapper.find('ul').stop().animate({height: height}, duration);
					self.objLib.wrapper.find('ul').css({height: height});

				},
				reset : function () {
					var self = this;
					if (self.objLib.scrollApi !== false) {
						var currIndex = self.objLib.scrollApi.currPageX;
						self.setSize();
						self.objLib.scrollApi.refresh();
						self.objLib.scrollApi.scrollToPage(currIndex, 0, 0);
					}
				},
				resizeAction : function () {
					var self = this;
					//$(window).bind("resize", function() { self.reset(); });
				},
				setObjects : function () {
					var self = this;
					self.objLib.wrapper = $('#wrapper');
					self.objLib.scroller = $('#scroller');
					self.objLib.nav = $('#scroller-nav');
				}
			},
			noRewriteHref : function () {
				$('.no-rewrite-src img').each(function () {
					var src = $(this).attr('src');
					var pos = src.indexOf('http://');
					if(pos !== -1)
					{
						pos1 = src.indexOf('/userfiles/');
						if(pos1 !== -1)
						{
							src = src.substr(pos1);
							window.console && console.log(pos1);
							window.console && console.log(src);
							$(this).attr('src', src);
						}
					}
				});
			},
			setSpecialButton : function () {
				var self = this;
				// window.console && console.log('gartner init');
				// gartner button
				$('body').on('tap, click', '#gartner-btn', function(event) {
					window.console && console.log('body clicked');
					event.preventDefault();
					Engine.ajax.addContact.insertGartnerCode();
					setTimeout(function () {
						window.console && console.log('location change');
						window.location = 'http://www.gartner.com/reprints/asseco-business-solutions-s-a-?id=1-1LXBGZD&ct=131017&st=sg';
					}, 500);
				});
			}
		},
		ajax : {
			addContact : {
				objLib : {
					requestInfoBox : $(".text-a"),
					requestForm : $("#request-contact-form"),
					analyticsCode : "<script type=\"text/javascript\"> var _gaq = _gaq || []; _gaq.push(['_setAccount', 'UA-17829804-12']); _gaq.push(['_setDomainName', 'mobile-touch.eu']); _gaq.push(['_trackPageview', '/kontakt-z-nami-ok']); (function() { var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google- analytics.com/ga.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); })(); window.console && console.log('contact analytics script loaded'); </script>",
					analyticsGartnerCode : "<script type=\"text/javascript\"> var _gaq = _gaq || []; _gaq.push(['_setAccount', 'UA-17829804-12']); _gaq.push(['_setDomainName', 'mobile-touch.eu']); _gaq.push(['_trackPageview', '/mobile_rap_gartner_ok']); (function() { var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true; ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google- analytics.com/ga.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s); })(); window.console && console.log('gartner analytics script loaded'); </script>"
				},
				add : function () {
					var self = this;
					var fields = self.getFormFields();
					$.ajax({
						url: 'ajaxsendcontact',
						type: "POST",
  						data: { client_name : fields.client_name, email : fields.email },
  						success: function(data) {
    						window.console && console.log(data);
    						self.setData(data);
	  					}
					});
				},
				hideForm : function () {
					var self = this;
					self.objLib.requestForm.fadeSliderToggle({speed: 300, easing : "swing",	onComplete : function () {
						self.showInfoText();
					}});
				},
				showInfoText : function () {
					var self = this;
					self.objLib.requestInfoBox.fadeSliderToggle();
				},
				setInfoText : function (data) {
					var self = this;
					self.objLib.requestInfoBox.html(data.header+' '+data.text);
				},
				getFormUrl : function () {
					var self = this;
					return self.objLib.requestForm.attr('action');
				},
				getFormFields : function () {
					var self = this;
					var form = self.objLib.requestForm;
					var fields = {
						client_name : form.find('#client_name').val(),
						email : form.find('#email').val()
					}
					return fields;
				},
				setData : function (data) {
					var self = this;
					var data = JSON.parse(data);
					//window.console && console.log(data);
					self.setInfoText(data);
					self.hideForm();
					self.insertCode();
				},
				insertCode : function () {
					var self = this;
					var code = self.objLib.analyticsCode;
					$('body').append(code);
				},
				insertGartnerCode : function () {
					var self = this;
					var code = self.objLib.analyticsGartnerCode;
					$('body').append(code);
				}
			},
			loadItems : {
				objLib : {
					loadButton : $('.load-button'),
					itemsContainer : $('#items-container'),
					allItemsCount : $('#items-container').data('items-count')
				},
				init : function () {
					var self = this;
					self.clickAction();
				},
				clickAction : function () {
					var self = this;

					$('.load-button').on('tap, click', 'a', function (e) {
						e.preventDefault();
						self.setObjects();
						if (!self.objLib.loadButton.hasClass('loading')) {
							self.objLib.loadButton.addClass('loading');
							var url = $(this).attr('href');
							window.console && console.log(url);
							var itemsCount = $(this).data('extend-jump');
							var info = self.getInfoFromUrl(url);

							var url = self.prepareUrl(info, itemsCount);
							self.load(url);
						}
					});
				},
				load : function (url) {
					var self = this;
					$.ajax({
						url: url,
  						success: function(data) {
    						self.appendItems(data);
	  					}
					});
				},
				getInfoFromUrl : function (url) {
					var self = this;
					var url = url.replace(self.getBaseUrl(), '');
					var urlArray = url.split('/');
					return { addr : urlArray[0], action : urlArray[1], parentItemId : urlArray[2], offset : urlArray.pop() }
				},
				prepareUrl : function (info, count) {
					//var offset = info.offset - count;
					var self = this;
					var offset = self.objLib.itemsContainer.find('.item').length;
					if (info.action === 'extendcomments') {
						// Comments
						var url = info.addr+'/ajaxgetcomments/'+info.parentItemId+'/'+offset+'/'+count;
					}
					else
					{
						// Other items
						var url = info.addr+'/ajaxgetitems/'+offset+'/'+count;
					}
					return url;
				},
				appendItems : function (data) {
					var self = this;
					var obj = $('<div class="temp-container"/>').append(data);
					var items = obj.find('.item');
					self.objLib.itemsContainer.append(obj);
					obj.fadeSliderToggle({speed: 800, easing : 'swing',  onComplete : function () {
						self.finalize(items);
					}});
				},
				unwrapItems : function (items) {
					items.unwrap();
				},
				changeHref : function () {
					var self = this;
					var link = self.objLib.loadButton.find('a');
					var href = link.attr('href');
					var itemsCount = link.data('extend-jump');
					var hrefArray = href.split('/');
					var offset = hrefArray.pop();
					var newOffset = parseInt(offset)+parseInt(itemsCount)
					hrefArray.push(newOffset);
					var newHref = hrefArray.join('/');
					link.attr('href', newHref);
					//window.console && console.log(newHref);
					return newHref;
				},
				checkItemsCount : function () {
					var self = this;
					var items = self.objLib.itemsContainer.find('.item');
					var length = items.length + 1;
					if (length >= self.objLib.itemsContainer.attr('data-items-count')) {
						self.objLib.loadButton.fadeSliderToggle();
					}
				},
				finalize : function (items) {
					var self = this;
					self.unwrapItems(items);
					self.changeHref();
					self.checkItemsCount();
					self.objLib.loadButton.removeClass('loading');
					self.objLib.loadButton.removeClass('tapped');
				},
				setObjects : function () {
					var self = this;
					self.objLib.loadButton = $('.load-button');
					self.objLib.itemsContainer = $('#items-container');
					self.objLib.allItemsCount = $('#items-container').data('items-count');
				},
				getBaseUrl : function () {
					return document.location.protocol+'//'+document.location.hostname+"/";
				}
			}
		},
		forms : {
			validateFastContactForm : function () {
				var contactForm = $("#request-contact-form");

				$('#submit-form').on('tap, click', function (e) {
					e.preventDefault();
					contactForm.submit();
				});

				var validator = contactForm.validate({
					rules: {
						client_name: "required",
						email: {
							required: true,
							email: true
						}
					},
					messages: {
						client_name : $('#client_name').data('error-required'),
						email : {
							email : $('#email').data('error-email'),
							required : $('#email').data('error-required')
						}
					},
  					highlight: function(element, errorClass, validClass) {
    					$(element).parent().parent().addClass(errorClass);
						$(element).addClass(errorClass).removeClass(validClass);
  					},
  					unhighlight: function(element, errorClass, validClass) {
    					$(element).parent().parent().removeClass(errorClass);
    					$(element).removeClass(errorClass).addClass(validClass);
					},
					errorPlacement: function(error, element) {
						element.parent().append(error);
					},
					submitHandler: function(form) {
						Engine.ajax.addContact.add(form);
						return false;
   				}
				});
			},
			autoSubmitForm : function () {
				$('form.autosubmit select').on('change', function () {
					$(this).closest('form').submit();
				});
			}
		},
		events : {
			objLib : {
				//needed for scroller behavior on orientation change
				is_orientation_changed : 0
			},
			init : function () {
				var self = this;
				self.pageinit();
				self.pageshow();
				self.pagechange();
				self.orientationchange();
				self.pagehide();
				self.pageloadfailed();
				self.pagebeforehide();
			},
			pageinit : function () {
				$(document).on('pageinit', function(event) {
					Engine.ui.addRemoveTapped();
					Engine.ui.menuInit();
					Engine.forms.autoSubmitForm();
				});
			},
			pagechange : function () {
				$(document).on('pagechange', function(event) {
					Engine.ui.checkIfHome();
					Engine.ui.hideMenu();
					Engine.ui.removeTapped(0);
				});
			},
			pageshow : function () {
				var self = this;
				$(document).on('pageshow', function(event, data) {
					if (!self.objLib.is_orientation_changed) {
						Engine.ui.iScrollInit.init();
					}
					else
					{
						self.objLib.is_orientation_changed = 0;
						Engine.ui.iScrollInit.reset();
					}
					Engine.ui.noRewriteHref();
				});
			},
			orientationchange : function () {
				var self = this;
				$(document).on('orientationchange', function(event) {
					if($.mobile.activePage.attr('id') === 'homepage')
					{
						Engine.ui.iScrollInit.reset();
					}
					else
					{
						if (Engine.ui.iScrollInit.objLib.scrollApi) {
							self.objLib.is_orientation_changed = 1;
						}
					}
				});
			},
			pagehide : function () {
				$(document).on('pagehide', function(event, data) {
					data.nextPage.removeClass('ui-page-pre-in');
				});
			},
			pageloadfailed : function () {
				$(document).bind("pageloadfailed", function(event, data){
					Engine.ui.hideMenu();
					Engine.ui.removeTapped(1500);
				});
			},
			pagebeforehide : function () {
				$(document).bind("pagebeforehide", function(event, data){
					data.nextPage.addClass('ui-page-pre-in');

					if(data.nextPage.attr('id') === 'news')
					{
						Engine.ajax.loadItems.init();
					}

				});
			}
		},
		firstTimeLoaded : function () {
			Engine.ui.iScrollInit.init();
			Engine.ui.checkIfHome();
			Engine.ui.addRemoveTapped();
			Engine.ui.menuInit();
			Engine.forms.autoSubmitForm();
		}
	};

 	Engine.firstTimeLoaded();
 	Engine.events.init();

	//Engine.ui.menuInit();
	//Engine.ui.checkIfHome();
	//Engine.ui.iScrollInit.init();
	Engine.ajax.loadItems.init();
	Engine.forms.validateFastContactForm();
	Engine.ui.setSpecialButton();
});


